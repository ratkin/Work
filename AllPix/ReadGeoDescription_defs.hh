/**
 * Allpix
 * Author: John Idarraga <idarraga@cern.ch> , 2010
 */

// expected strings in the xml file
// forced by the dtd
#define __radial_on_S "radial_on"

#define __pixeldet_node_S "pixeldet"
#define __pixeldet_node__END_S "pixeldet"
#define __pixeldet_node_ATT_id_S "id"

#define __pixeldet_global_ATT_units_S "units"

#define __npix_x_S "npix_x"
#define __npix_y_S "npix_y"
#define __npix_z_S "npix_z"
#define __npix_r_S "npix_r"
#define __npix_phi_S "npix_phi"
#define __npix_phi2_S "npix_phi2"

#define __stereoangle_S "stereoangle"
#define __striplength_1_S "striplength_1"
#define __striplength_2_S "striplength_2"
#define __striplength_3_S "striplength_3"
#define __striplength_4_S "striplength_4"
#define __sensor_r_S "sensor_r"

#define __point_ax_S "point_ax"
#define __point_ay_S "point_ay"
#define __point_bx_S "point_bx"
#define __point_by_S "point_by"
#define __point_cx_S "point_cx"
#define __point_cy_S "point_cy"
#define __point_dx_S "point_dx"
#define __point_dy_S "point_dy"

#define __pixsize_x_S "pixsize_x"
#define __pixsize_y_S "pixsize_y"
#define __pixsize_z_S "pixsize_z"
#define __pixsize_r_S "pixsize_r"
#define __pixsize_phi_S "pixsize_phi"
#define __pixsize_phi2_S "pixsize_phi2"

#define __chip_hx_S "chip_hx"
#define __chip_hy_S "chip_hy"
#define __chip_hz_S "chip_hz"

#define __chip_rmin_S "chip_rmin"
#define __chip_rmax_S "chip_rmax" //ADDED
#define __chip_sphi_S "chip_sphi"
#define __chip_dphi_S "chip_dphi"

#define __chip_posx_S "chip_posx"
#define __chip_posy_S "chip_posy"
#define __chip_posz_S "chip_posz"

#define __chip_offsetx_S "chip_offsetx"
#define __chip_offsety_S "chip_offsety"
#define __chip_offsetz_S "chip_offsetz"

#define __sensor_hx_S "sensor_hx"
#define __sensor_hy_S "sensor_hy"
#define __sensor_hz_S "sensor_hz"

#define __sensor_rmin_S "sensor_rmin"
#define __sensor_rmax_S "sensor_rmax"
#define __sensor_sphi_S "sensor_sphi"
#define __sensor_dphi_S "sensor_dphi"

#define __coverlayer_hz_S "coverlayer_hz"
#define __coverlayer_mat_S "coverlayer_mat"

#define __sensor_posx_S "sensor_posx"
#define __sensor_posy_S "sensor_posy"
#define __sensor_posz_S "sensor_posz"

#define __sensor_gr_excess_htop_S      "sensor_gr_excess_htop"
#define __sensor_gr_excess_hbottom_S   "sensor_gr_excess_hbottom"
#define __sensor_gr_excess_hright_S    "sensor_gr_excess_hright"
#define __sensor_gr_excess_hleft_S     "sensor_gr_excess_hleft"


#define __Bump_Radius_S "bump_radius"
#define __Bump_Height_S "bump_height"
#define __Bump_OffsetX_S "bump_offset_x"
#define __Bump_OffsetY_S "bump_offset_y"
#define __Bump_OffsetR_S "bump_offset_r"
#define __Bump_OffsetPhi_S "bump_offset_phi"
#define __Bump_Dr_S "bump_dr"



#define __digitizer_S "digitizer"

#define __pcb_hx_S "pcb_hx"
#define __pcb_hy_S "pcb_hy"
#define __pcb_hz_S "pcb_hz"
#define __sensor_Resistivity "sensor_resistivity"
#define __MIP_Tot_S "MIP_Tot"
#define __MIP_Charge_S "MIP_Charge"
#define __Counter_Depth_S "Counter_Depth"
#define __Clock_Unit_S "Clock_Unit"
#define __Chip_Noise_S "Chip_Noise"
#define __Chip_Threshold_S "Chip_Threshold"
#define __Cross_Talk_S "Cross_Talk"
#define __Saturation_Energy_S "Saturation_Energy"



