/*
 * AllpixBumpsParameterization.cpp
 *
 *  Created on: 31 janv. 2014
 *      Author: mbenoit
 */

#include "AllPixBumpsParameterization.hh"

Allpix_BumpsParameterization::Allpix_BumpsParameterization(AllPixGeoDsc * geo) {
	this->fGeoPars = geo;
	/*hsensorX=fGeoPars->GetHalfSensorX();
	hsensorY=fGeoPars->GetHalfSensorY();
	hpixelX=fGeoPars->GetHalfPixelX();
	hpixelY=fGeoPars->GetHalfPixelY();

	npixelX = fGeoPars->GetNPixelsX();
	npixelY = fGeoPars->GetNPixelsY();*/
	//ADDED
	
	hsensorR=fGeoPars->GetSensorDR()/2;
	hsensorPhi=fGeoPars->GetSensorDPhi()/2;
	npixelR = fGeoPars->GetNPixelsR();
	npixelPhi = fGeoPars->GetNPixelsPhi();
	hpixelR=hsensorR/npixelR;
	hpixelPhi=hsensorPhi/npixelPhi;
	
}



void Allpix_BumpsParameterization::ComputeTransformation(G4int copyId,
		G4VPhysicalVolume* Bump) const {

  /*G4double XPos = posX(copyId) + fGeoPars->GetBumpOffsetX();
    G4double YPos = posY(copyId) + fGeoPars->GetBumpOffsetY();*/
	G4double ZPos = 0;

	
	G4double RPos= fGeoPars->GetSensorRmin()+posR(copyId)+fGeoPars->GetBumpOffsetR();
	G4double PhiPos= fGeoPars->GetSensorSPhi() +posPhi(copyId)+fGeoPars->GetBumpOffsetPhi();
	G4double XPos = RPos*cos(PhiPos)+fGeoPars->GetSensorXOffset();
	G4double YPos = RPos*sin(PhiPos)+fGeoPars->GetSensorYOffset();
	

	//G4cout << "[PArameterization] placing bump : " << copyId << endl;
	Bump->SetTranslation(G4ThreeVector(XPos,YPos,ZPos));
	Bump->SetRotation(0);

}
/*
double Allpix_BumpsParameterization::posX(int id) const {

	G4int X =  id%npixelX;
	//G4cout << "STRANGE: " << id<< ", "<< X <<", "<< X*hpixelX*2 + hpixelX - hsensorX << G4endl;
	return X*hpixelX*2 + hpixelX - hsensorX;
}

double Allpix_BumpsParameterization::posY(int id) const {

  //G4int Y = (id-(id%npixelX))/npixelY;
	G4int Y = (id-(id%npixelX))/npixelX;       
	return Y*hpixelY*2 + hpixelY - hsensorY;
	}*/

double Allpix_BumpsParameterization::posR(int id) const {

	//G4int R =  id%npixelR;
	G4int R = (id-(id%npixelPhi))/(npixelPhi);
	return R*hpixelR*2 + hpixelR;
}

double Allpix_BumpsParameterization::posPhi(int id) const {

	//G4int Y = (id-(id%npixelR))/npixelPhi 
	G4int Y = id%npixelPhi;
	return Y*hpixelPhi*2 + hpixelPhi;
}

