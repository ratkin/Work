import os
import sys
import numpy as np

filepath_readA="run_s1/physVols.txt"
#filepath_write='physVol_SCT.txt'
#filepath_write2='physVol_modules.txt'
filepath_write3='physVol_modules_basics.txt'

 
#route=os.listdir(folder)
f=open(filepath_readA,'r')
A=f.readlines()
f.close()
#W=open(filepath_write,'w')
#W2=open(filepath_write2,'w')
W3=open(filepath_write3,'w')

i=0
last2=len(A)
print('length of PhysVol file: ',last2)
#last2=100
#W.write(A[0])
ABC_count = 0
HCC_count = 0

module_list = []

while i<last2:
    #print(F[i].find('###'))
    found=0
    line=A[i].split('\t')
    line2=A[i]
    loc = line[0].find("::")
    if i==0:
        #W2.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t'+line[3]+'\t'+line[7]+'\t'+line[8]+'\t'+line[9]+'\t'+line[10]+'\t'+line[11]+'\t'+line[12]+'\t'+line[13]+'\t'+line[14]+'\n \n')
        W3.write('{:50} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10}\n \n'.format(line[0],line[9],line[10],line[11],line[12],line[13],line[14]))
    i+=1
    #print(line)
    if "SCT::" in line[2]:
        #W.write(line2)
        if line[0][loc+2:] in module_list:
            continue
        if "Sensor" in line[2]:
            module_list.append(line[0][loc+2:])
            #W2.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t'+line[3]+'\t'+line[7]+'\t'+line[8]+'\t'+line[9]+'\t'+line[10]+'\t'+line[11]+'\t'+line[12]+'\t'+line[13]+'\t'+line[14]+'\n')
            W3.write('{:50} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10}\n'.format(line[0][loc+2:],line[9],line[10],line[11],line[12],line[13],line[14]))
        elif "Chip" in line[2]:
            module_list.append(line[0][loc+2:])
            #W2.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t'+line[3]+'\t'+line[7]+'\t'+line[8]+'\t'+line[9]+'\t'+line[10]+'\t'+line[11]+'\t'+line[12]+'\t'+line[13]+'\t'+line[14]+'\n')
            if "ABC" in line[2] and ABC_count==0:
                W3.write('{:50} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10}\n'.format(line[0][loc+2:],line[9],line[10],line[11],line[12],line[13],line[14]))
                ABC_count=1
            elif "HCC" in line[2] and HCC_count==0:
                W3.write('{:50} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10}\n'.format(line[0][loc+2:],line[9],line[10],line[11],line[12],line[13],line[14]))
                HCC_count=1
        elif "Hybrid" in line[2]:
            module_list.append(line[0][loc+2:])
            #W2.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t'+line[3]+'\t'+line[7]+'\t'+line[8]+'\t'+line[9]+'\t'+line[10]+'\t'+line[11]+'\t'+line[12]+'\t'+line[13]+'\t'+line[14]+'\n')
            W3.write('{:50} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10}\n'.format(line[0][loc+2:],line[9],line[10],line[11],line[12],line[13],line[14]))
        elif "DCDC" in line[2]:
            module_list.append(line[0][loc+2:])
            #W2.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t'+line[3]+'\t'+line[7]+'\t'+line[8]+'\t'+line[9]+'\t'+line[10]+'\t'+line[11]+'\t'+line[12]+'\t'+line[13]+'\t'+line[14]+'\n')
            W3.write('{:50} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10} \t {:10}\n'.format(line[0][loc+2:],line[9],line[10],line[11],line[12],line[13],line[14]))

