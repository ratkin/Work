import os
import sys
import ROOT
import collections
import math
import copy
import re
import ctypes
import numpy as np

sys.argv.append("-b")
print ("Opening files")

workDir = "./inputs/SMVHVZ_2022_MVA_mc16ade_30J3_STXS"
fileName = "quick.root"
#directory = os.fsencode(workDir)

for file in os.listdir(workDir): #directory):
    hist_file = ROOT.TFile(workDir+"/"+file, "UPDATE")
    
    hist = hist_file.Get("stopWtDS")

    try:
        h = hist.Clone()
    except:
        print("Doesn't exist for {}".format(file))
        continue

    h.Write("stopWt")
    ROOT.gDirectory.Delete("stopWtDS;1")
    hist_file.Close()
