import os
import sys
import numpy as np
from collections import OrderedDict

# python readSignificances.py 1 output/<WS1> output/<WS2>   # where WS2 is the nominal WS

# to rerun failed jobs, edit "submit" file in e.g., Reader_1L_33-05_d_Resolved_D_D/submit, 
# make sure "queue in" only has the numbers numbers printed out from fails 
# then from in e.g., Reader_1L_33-05_d_Resolved_D_D/submit, run "condor_submit submit" to run those jobs

doReco = int(sys.argv[1])

if doReco:
    print("Doing reco")

directory = sys.argv[2]+"/logs/significance.log"
f=open(directory,'r')
A=f.readlines()
f.close()
print("Looking in "+directory)
directory_nom = sys.argv[3]+"/logs/significance.log"
f=open(directory_nom,'r')
B=f.readlines()
f.close()

POIs = OrderedDict([
    ("SigXsecOverSMWHxGT75PTVx0J" , ["WH, $p_{T}^{W} >$ 75 GeV, nJ = 0",0]),
    ("SigXsecOverSMWHxGT75PTVx1J" , ["WH, $p_{T}^{W} >$ 75 GeV, nJ = 1",0]),
    ("SigXsecOverSMWHxGT75PTVxGE2J" , ["WH, $p_{T}^{W} >$ 75 GeV, nJ $\geq$ 2",0]),
    ("SigXsecOverSMWHx75x150PTV" , ["WH, 75 $< p_{T}^{W} <$ 150 GeV",0]),
    ("SigXsecOverSMWHx150x250PTV" , ["WH, 150 $< p_{T}^{W} <$ 250 GeV",0]),
    ("SigXsecOverSMWHx150x250PTVx0J" , ["WH, 150 $< p_{T}^{W} <$ 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMWHx150x250PTVxGE1J", ["WH, 150 $< p_{T}^{W} <$ 250 GeV, nJ $\geq$ 1",0]),
    ("SigXsecOverSMWHxGT250PTV", ["WH, $p_{T}^{W} >$ 250 GeV",0]),
    ("SigXsecOverSMWHxGT250PTVx0J", ["WH, $p_{T}^{W} >$ 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMWHxGT250PTVxGE1J", ["WH, $p_{T}^{W} >$ 250 GeV, nJ $\geq$ 1",0]),
    ("WHTot", ["WH",0]),
    ("SigXsecOverSMZHxGT75PTVx0J" , ["ZH, $p_{T}^{Z} >$ 75 GeV, nJ = 0",0]),
    ("SigXsecOverSMZHxGT75PTVx1J" , ["ZH, $p_{T}^{Z} >$ 75 GeV, nJ = 1",0]),
    ("SigXsecOverSMZHxGT75PTVxGE2J" , ["ZH, $p_{T}^{Z} >$ 75 GeV, nJ $\geq$ 2",0]),
    ("SigXsecOverSMZHx75x150PTV", ["ZH, 75 $< p_{T}^{Z} <$ 150 GeV",0]),
    ("SigXsecOverSMZHx150x250PTV" , ["ZH, 150 $< p_{T}^{Z} <$ 250 GeV",0]),
    ("SigXsecOverSMZHx150x250PTVx0J" , ["ZH, 150 $< p_{T}^{Z} <$ 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMZHx150x250PTVxGE1J", ["ZH, 150 $< p_{T}^{Z} <$ 250 GeV, nJ $\geq$ 1",0]),
    ("SigXsecOverSMZHxGT250PTV", ["ZH, $p_{T}^{Z} >$ 250 GeV",0]),
    ("SigXsecOverSMZHxGT250PTVx0J", ["ZH, $p_{T}^{Z} >$ 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMZHxGT250PTVxGE1J", ["ZH, $p_{T}^{Z} >$ 250 GeV, nJ $\geq$ 1",0]),
    ("ZHTot", ["ZH",0]),
    ("Total", ["VH",0]),
])

POIs_nom = OrderedDict([
    ("SigXsecOverSMWHxGT75PTVx0J" , ["WH, p_{T}^{W} > 75 GeV, nJ = 0",0]),
    ("SigXsecOverSMWHxGT75PTVx1J" , ["WH, p_{T}^{W} > 75 GeV, nJ = 1",0]),
    ("SigXsecOverSMWHxGT75PTVxGE2J" , ["WH, p_{T}^{W} > 75 GeV, nJ \geq 2",0]),
    ("SigXsecOverSMWHx75x150PTV" , ["WH, 75 < p_{T}^{W} < 150 GeV",0]),
    ("SigXsecOverSMWHx150x250PTV" , ["WH, 150 < p_{T}^{W} < 250 GeV",0]),
    ("SigXsecOverSMWHx150x250PTVx0J" , ["WH, 150 < p_{T}^{W} < 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMWHx150x250PTVxGE1J", ["WH, 150 < p_{T}^{W} < 250 GeV, nJ \geq 1",0]),
    ("SigXsecOverSMWHxGT250PTV", ["WH, p_{T}^{W} > 250 GeV, nJ \geq 0",0]),
    ("SigXsecOverSMWHxGT250PTVx0J", ["WH, p_{T}^{W} > 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMWHxGT250PTVxGE1J", ["WH, p_{T}^{W} > 250 GeV, nJ \geq 1",0]),
    ("WHTot", ["WH",0]),
    ("SigXsecOverSMZHxGT75PTVx0J" , ["ZH, p_{T}^{Z} > 75 GeV, nJ = 0",0]),
    ("SigXsecOverSMZHxGT75PTVx1J" , ["ZH, p_{T}^{Z} > 75 GeV, nJ = 1",0]),
    ("SigXsecOverSMZHxGT75PTVxGE2J" , ["ZH, p_{T}^{Z} > 75 GeV, nJ \geq 2",0]),
    ("SigXsecOverSMZHx75x150PTV", ["ZH, 75 < p_{T}^{Z} < 150 GeV, nJ \geq 0",0]),
    ("SigXsecOverSMZHx150x250PTV" , ["ZH, 150 < p_{T}^{Z} < 250 GeV",0]),
    ("SigXsecOverSMZHx150x250PTVx0J" , ["ZH, 150 < p_{T}^{Z} < 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMZHx150x250PTVxGE1J", ["ZH, 150 < p_{T}^{Z} < 250 GeV, nJ \geq 1",0]),
    ("SigXsecOverSMZHxGT250PTV", ["ZH, p_{T}^{Z} > 250 GeV, nJ \geq 0",0]),
    ("SigXsecOverSMZHxGT250PTVx0J", ["ZH, p_{T}^{Z} > 250 GeV, nJ = 0",0]),
    ("SigXsecOverSMZHxGT250PTVxGE1J", ["ZH, p_{T}^{Z} > 250 GeV, nJ \geq 1",0]),
    ("ZHTot", ["ZH",0]),
    ("Total", ["VH",0]),
])

if doReco:
    POIs = OrderedDict([
        ("SigXsecOverSM_J2_BMin75" , ["75 GeV $< p_{T}^{V} <$ 150 GeV, nJ = 2",0]),
        ("SigXsecOverSM_J2_BMin150" , ["150 GeV $< p_{T}^{V} <$ 250 GeV, nJ = 2",0]),
        ("SigXsecOverSM_J2_BMin250" , ["250 GeV $< p_{T}^{V} <$ 400 GeV, nJ = 2",0]),
        ("SigXsecOverSM_J2_BMin400" , ["$p_{T}^{V} >$ 400 GeV, nJ = 2",0]),
        ("2JTot", ["nJ = 2",0]),
        ("SigXsecOverSM_J3_BMin75" , ["75 GeV $< p_{T}^{V} <$ 150 GeV, nJ = 3",0]),
        ("SigXsecOverSM_J3_BMin150" , ["150 GeV $< p_{T}^{V} <$ 250 GeV, nJ = 3",0]),
        ("SigXsecOverSM_J3_BMin250" , ["250 GeV $< p_{T}^{V} <$ 400 GeV, nJ = 3",0]),
        ("SigXsecOverSM_J3_BMin400" , ["$p_{T}^{V} >$ 400 GeV, nJ = 3",0]),
        ("3JTot", ["nJ = 3",0]),
        ("SigXsecOverSM_J4_BMin75" , ["75 GeV $< p_{T}^{V} <$ 150 GeV, nJ = 4",0]),
        ("SigXsecOverSM_J4_BMin150" , ["150 GeV $< p_{T}^{V} <$ 250 GeV, nJ = 4",0]),
        ("SigXsecOverSM_J4_BMin250" , ["250 GeV $< p_{T}^{V} <$ 400 GeV, nJ = 4",0]),
        ("SigXsecOverSM_J4_BMin400" , ["$p_{T}^{V} >$ 400 GeV, nJ = 4",0]),
        ("4JTot", ["nJ = 4",0]),
        ("Total", ["VH",0]),
    ])
    POIs_nom = OrderedDict([
        ("SigXsecOverSM_J2_BMin75" , ["75 GeV < p_{T}^{V} < 150 GeV, nJ = 2",0]),
        ("SigXsecOverSM_J2_BMin150" , ["150 GeV < p_{T}^{V} < 250 GeV, nJ = 2",0]),
        ("SigXsecOverSM_J2_BMin250" , ["250 GeV < p_{T}^{V} < 400 GeV, nJ = 2",0]),
        ("SigXsecOverSM_J2_BMin400" , ["p_{T}^{V} > 400 GeV, nJ = 2",0]),
        ("2JTot", ["nJ = 2",0]),
        ("SigXsecOverSM_J3_BMin75" , ["75 GeV < p_{T}^{V} < 150 GeV, nJ = 3",0]),
        ("SigXsecOverSM_J3_BMin150" , ["150 GeV < p_{T}^{V} < 250 GeV, nJ = 3",0]),
        ("SigXsecOverSM_J3_BMin250" , ["250 GeV < p_{T}^{V} < 400 GeV, nJ = 3",0]),
        ("SigXsecOverSM_J3_BMin400" , ["p_{T}^{V} > 400 GeV, nJ = 3",0]),
        ("3JTot", ["nJ = 3",0]),
        ("SigXsecOverSM_J4_BMin75" , ["75 GeV < p_{T}^{V} < 150 GeV, nJ = 4",0]),
        ("SigXsecOverSM_J4_BMin150" , ["150 GeV < p_{T}^{V} < 250 GeV, nJ = 4",0]),
        ("SigXsecOverSM_J4_BMin250" , ["250 GeV < p_{T}^{V} < 400 GeV, nJ = 4",0]),
        ("SigXsecOverSM_J4_BMin400" , ["p_{T}^{V} > 400 GeV, nJ = 4",0]),
        ("4JTot", ["nJ = 4",0]),
        ("Total", ["VH",0]),
    ])



#for P in POIs:
#    print(P+" "+POIs[P][0])
#exit()

sig=0
POI=""
j = len(A)-1
#print(j)
while j > 0:
    j-=1
    
    #print("  "+str(j)+"  "+A[j])
    if "Median significance:" in A[j]:
        sig = float(A[j].split(" ")[-1])
    elif "Results for POI" in A[j]:
        POI = A[j].split(" ")[-1].strip("\n")
        POIs[POI][1]=sig
    elif "Printing results for all POI:" in A[j]:
        j=-1

sig=0
POI=""
j = len(B)-1
#print(j)
while j > 0:
    j-=1
    
    #print("  "+str(j)+"  "+B[j])
    if "Median significance:" in B[j]:
        sig = float(B[j].split(" ")[-1])
    elif "Results for POI" in B[j]:
        POI = B[j].split(" ")[-1].strip("\n")
        POIs_nom[POI][1]=sig
    elif "Printing results for all POI:" in B[j]:
        j=-1




for POI,vals in POIs.iteritems():
    if not doReco:
        if POI == "WHTot":
            if vals[1] > 0:
                print(vals[0]+" & {:.3f} & {:.3f} ({:.1f}\%) \\\\".format( POIs_nom[POI][1],vals[1],100*(vals[1]/POIs_nom[POI][1]-1) ))
                print("\\midrule")
            POIs["Total"][1] = np.sqrt(POIs["Total"][1]**2 + vals[1]**2)
            POIs_nom["Total"][1] = np.sqrt(POIs_nom["Total"][1]**2 + POIs_nom[POI][1]**2)
            continue
        elif POI == "ZHTot":
            if vals[1] > 0:
                print(vals[0]+" & {:.3f} & {:.3f} ({:.1f}\%) \\\\".format( POIs_nom[POI][1],vals[1],100*(vals[1]/POIs_nom[POI][1]-1) ))
                print("\\midrule")
            POIs["Total"][1] = np.sqrt(POIs["Total"][1]**2 + vals[1]**2)
            POIs_nom["Total"][1] = np.sqrt(POIs_nom["Total"][1]**2 + POIs_nom[POI][1]**2)
            continue
        elif "WH, " in vals[0]:
            if vals[1] > 0:
                POIs["WHTot"][1] = np.sqrt(POIs["WHTot"][1]**2 + vals[1]**2)
                POIs_nom["WHTot"][1] = np.sqrt(POIs_nom["WHTot"][1]**2 + POIs_nom[POI][1]**2)
        elif "ZH, " in vals[0]:
            if vals[1] > 0:
                POIs["ZHTot"][1] = np.sqrt(POIs["ZHTot"][1]**2 + vals[1]**2)
                POIs_nom["ZHTot"][1] = np.sqrt(POIs_nom["ZHTot"][1]**2 + POIs_nom[POI][1]**2)
    else:
        if POI == "2JTot":
            if vals[1] > 0:
                print(vals[0]+" & {:.3f} & {:.3f} ({:.1f}\%) \\\\".format( POIs_nom[POI][1],vals[1],100*(vals[1]/POIs_nom[POI][1]-1) ))
                print("\\midrule")
            POIs["Total"][1] = np.sqrt(POIs["Total"][1]**2 + vals[1]**2)
            POIs_nom["Total"][1] = np.sqrt(POIs_nom["Total"][1]**2 + POIs_nom[POI][1]**2)
            continue
        elif POI == "3JTot":
            if vals[1] > 0:
                print(vals[0]+" & {:.3f} & {:.3f} ({:.1f}\%) \\\\".format( POIs_nom[POI][1],vals[1],100*(vals[1]/POIs_nom[POI][1]-1) ))
                print("\\midrule")
            POIs["Total"][1] = np.sqrt(POIs["Total"][1]**2 + vals[1]**2)
            POIs_nom["Total"][1] = np.sqrt(POIs_nom["Total"][1]**2 + POIs_nom[POI][1]**2)
            continue
        elif POI == "4JTot":
            if vals[1] > 0:
                print(vals[0]+" & {:.3f} & {:.3f} ({:.1f}\%) \\\\".format( POIs_nom[POI][1],vals[1],100*(vals[1]/POIs_nom[POI][1]-1) ))
                print("\\midrule")
            POIs["Total"][1] = np.sqrt(POIs["Total"][1]**2 + vals[1]**2)
            POIs_nom["Total"][1] = np.sqrt(POIs_nom["Total"][1]**2 + POIs_nom[POI][1]**2)
            continue
        elif "J2" in POI:
            if vals[1] > 0:
                POIs["2JTot"][1] = np.sqrt(POIs["2JTot"][1]**2 + vals[1]**2)
                POIs_nom["2JTot"][1] = np.sqrt(POIs_nom["2JTot"][1]**2 + POIs_nom[POI][1]**2)
        elif "J3" in POI:
            if vals[1] > 0:
                POIs["3JTot"][1] = np.sqrt(POIs["3JTot"][1]**2 + vals[1]**2)
                POIs_nom["3JTot"][1] = np.sqrt(POIs_nom["3JTot"][1]**2 + POIs_nom[POI][1]**2)
        elif "J4" in POI:
            if vals[1] > 0:
                POIs["4JTot"][1] = np.sqrt(POIs["4JTot"][1]**2 + vals[1]**2)
                POIs_nom["4JTot"][1] = np.sqrt(POIs_nom["4JTot"][1]**2 + POIs_nom[POI][1]**2)

    if vals[1] > 0:
        print(vals[0]+" & {:.3f} & {:.3f} ({:.1f}\%) \\\\".format( POIs_nom[POI][1],vals[1],100*(vals[1]/POIs_nom[POI][1]-1) ))
        

exit()
