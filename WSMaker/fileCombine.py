import os
import sys
import ROOT
import collections
import math
import copy
import re
import ctypes
import numpy as np

sys.argv.append("-b")
print ("Opening files")

#workDir = "./inputs/SMVHVZ_2022_MVA_mc16ade_30J3_STXS"
#fileName = "quick.root"

#for workDir in ["./inputs/SMVHVZ_2022_MVA_mc16ade_25J3_STXS","./inputs/SMVHVZ_2022_MVA_mc16ade_Nom_STXS"]:
for workDir in ["./inputs/SMVHVZ_2021_MVA_mc16ade_2030_STXS"]:
    for file in os.listdir(workDir): #directory):
        if "TwoLepton_2tag3jet" in file:
            file2 = file.replace("2tag3jet","2tag4pjet")
            file3 = file.replace("2tag3jet","2tag3pjet")
            haddCom = "hadd "+workDir+"/"+file3+" "+workDir+"/"+file+" "+workDir+"/"+file2
            #print(haddCom)
            #haddCom = "hadd "+fileName+" "+workDir+"/"+file+" "+workDir+"/"+file2
            #print(haddCom)
            #exit()
            os.system(haddCom)
            #exit()
