"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
import copy
import re
import ctypes
import numpy as np
#import argparse

# python QuickCompare.py 13TeV_ZeroLepton_2tag2jet_150_250ptv_SR_mBB

sys.argv.append("-b")
print ("Opening files")

studyRegion = sys.argv[1]

samples_inFile = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
                  "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
                  "ttbar" : ["ttbar"],"data" : ["data"],
                  "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],
                  "signal" : ["qqZvvH125","ggZvvH125","qqZvvH125cc","ggZvvH125cc","qqZllH125","ggZllH125","qqZllH125cc","ggZllH125cc","qqWlvH125","qqWlvH125cc",]}

samples_plots = {"W+bb" :["Wbb"],"W+bc":["Wbc"],"W+bl":["Wbl"],"W+cc":["Wcc"],"W+cl":["Wcl"],"W+l":["Wl"],
                 "Z+bb" :["Zbb"],"Z+bc":["Zbc"],"Z+bl":["Zbl"],"Z+cc":["Zcc"],"Z+cl":["Zcl"],"Z+l":["Zl"],
                 "ttbar" : ["ttbar"], "data" : ["data"],
                 "Wt":["stopWtDS"],"s+t chan":["stopt","stops"],"ZZ":["ZZ","ggZZ",],"WZ":["WZ"],"WW":["WW","ggWW"],
                 "VH 125" : ["qqZvvH125","ggZvvH125","qqZvvH125cc","ggZvvH125cc","qqZllH125","ggZllH125","qqZllH125cc","ggZllH125cc","qqWlvH125","qqWlvH125cc",]}

samples_yields = {"W+bb" :["Wbb"],"W+bc":["Wbc"],"W+bl":["Wbl"],"W+cc":["Wcc"],"W+cl":["Wcl"],"W+l":["Wl"],
                 "Z+bb" :["Zbb"],"Z+bc":["Zbc"],"Z+bl":["Zbl"],"Z+cc":["Zcc"],"Z+cl":["Zcl"],"Z+l":["Zl"],
                 "ttbar" : ["ttbar"], "data" : ["data"],
                 "stop" : ["stopWtDS","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],
                 "signal" : ["qqZvvH125","ggZvvH125","qqZvvH125cc","ggZvvH125cc","qqZllH125","ggZllH125","qqZllH125cc","ggZllH125cc","qqWlvH125","qqWlvH125cc",]}

'''
samples = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
           "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
           "ttbar" : ["ttbar"],
           "stop" : ["stop"],"diboson" : ["diboson"],"data" : ["data"],
           "signal" : ["signal"]}
'''
colours = {"VH 125" : ROOT.kRed, 
           "signal" : ROOT.kRed,
           "WJets" : ROOT.kGreen+4,
           "W+bb" : ROOT.kGreen+4, 
           "W+bc" : ROOT.kGreen+3,
           "W+bl" : ROOT.kGreen+2,
           "W+cc" : ROOT.kGreen+1,
           "W+cl" : ROOT.kGreen-6,
           "W+l" : ROOT.kGreen-9,
           "ZJets" : ROOT.kAzure+2,
           "Z+bb" : ROOT.kAzure+2, 
           "Z+bc" : ROOT.kAzure+1, 
           "Z+bl" : ROOT.kAzure-2, 
           "Z+cc" : ROOT.kAzure-4, 
           "Z+cl" : ROOT.kAzure-8, 
           "Z+l" : ROOT.kAzure-9, 
           "ttbar" : ROOT.kOrange, 
           "stop" : ROOT.kOrange-1,
           "s+t chan" : ROOT.kOrange-1, 
           "Wt": ROOT.kYellow - 7,
           "diboson" : ROOT.kGray,
           "WW": ROOT.kGray + 3,
           "WZ" : ROOT.kGray, 
           "ZZ": ROOT.kGray + 1,
           "data" : ROOT.kBlack}


m_sigVals={}
m_sigValsTot={}
m_yieldVals={}
m_SBVals={}
m_sigValsMVA={}
m_yieldValsMVA={}
m_SBValsMVA={}

saveHistos = True
working_dir = "/afs/cern.ch/work/r/ratkin/WSMaker2/WSMaker_VHbb/quickCompare/"

if not os.path.isdir(working_dir):
    os.makedirs(working_dir)


FileName = "inputs/SMVHVZ_2021_MVA_mc16ade_Nom_STXS/"+studyRegion+".root"
FileName1 = "inputs/SMVHVZ_2021_MVA_mc16ade_2030_STXS/"+studyRegion+".root"
FileName2 = "inputs/SMVHVZ_2022_MVA_mc16ade_Nom_STXS/"+studyRegion+".root"
FileName3 = "inputs/SMVHVZ_2022_MVA_mc16ade_25J3_STXS/"+studyRegion+".root"
FileName4 = "inputs/SMVHVZ_2022_MVA_mc16ade_30J3_STXS/"+studyRegion+".root"

hist_file = ROOT.TFile(FileName, "READ")
hist_file1 = ROOT.TFile(FileName1, "READ")
hist_file2 = ROOT.TFile(FileName2, "READ")
hist_file3 = ROOT.TFile(FileName3, "READ")
hist_file4 = ROOT.TFile(FileName4, "READ")

assert hist_file.IsOpen()
assert hist_file1.IsOpen()
assert hist_file2.IsOpen()
assert hist_file3.IsOpen()
assert hist_file4.IsOpen()

def doBlinding(S1,B1,h1,var):
    thresh = 0.1
    if ("mBB" in var or "Mbb" in var) and "J" not in var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            i+=1
    elif "mva" in var:
        tot_sig = S1.Integral()
        if not tot_sig > 0: 
            return h1
        sum_sig=0
        i=h1.GetNbinsX()
        #while h1.GetNbinsX()-i<5:
        #    h1.SetBinContent(i, 0)
        #    h1.SetBinError(i, 0)
        #    i-=1
        while i>0:
            sum_sig+=S1.GetBinContent(i)
            if sum_sig/tot_sig < 0.7:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
                i-=1
            else:
                break
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1

    return h1


def doDataMC(varName,binning,canName,F,ptCut,applyScales):
    print("doDataMC")
    histograms={}
    # 2btag4pjet_150_250ptv_SR
    line=canName.split("_")
    F.cd()
    #print("Looping to Get histograms")
    for sampl in samples_plots:
        for samp in samples_plots[sampl]:
            histName1=samp+"_"+canName+"_"+varName+ptCut
            #print("Getting hist "+histName1)
            hist_tmp1 = F.Get(histName1)
            if sampl in histograms:
                try:
                    histograms[sampl].Add(hist_tmp1.Clone())
                except:
                    print(histName1+" doesn't exist")
            else:
                try:
                    histograms[sampl]=hist_tmp1.Clone()
                except:
                    print(histName1+" doesn't exist")

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    if applyScales:
        jetCat = "2jet"
        if "3jet" in canName.split("_")[0]:
            jetCat = "3jet"
        elif "4jet" in canName.split("_")[0]:
            jetCat = "4jet"
        for samp in ["ttbar"]:
            if samp in histograms:
                if ptCut == "20J3":
                    histograms[samp].Scale(scalefactors["ttbar"+jetCat])
                elif ptCut == "25J3":
                    histograms[samp].Scale(scalefactors25J3["ttbar"+jetCat])
                elif ptCut == "30J3":
                    histograms[samp].Scale(scalefactors30J3["ttbar"+jetCat])
        for samp in ["W+bb","W+bc","W+bl","W+cc"]:
            if samp in histograms:
                if ptCut == "20J3":
                    histograms[samp].Scale(scalefactors["WHF"+jetCat])
                elif ptCut == "25J3":
                    histograms[samp].Scale(scalefactors25J3["WHF"+jetCat])
                elif ptCut == "30J3":
                    histograms[samp].Scale(scalefactors30J3["WHF"+jetCat])
        for samp in ["Z+bb","Z+bc","Z+bl","Z+cc"]:
            if samp in histograms:
                if ptCut == "20J3":
                    histograms[samp].Scale(scalefactors["ZHF"+jetCat])
                elif ptCut == "25J3":
                    histograms[samp].Scale(scalefactors25J3["ZHF"+jetCat])
                elif ptCut == "30J3":
                    histograms[samp].Scale(scalefactors30J3["ZHF"+jetCat])

    newMVABins=[]
    if "mva" in varName:
        hb_temp = ROOT.TH1F()
        if channel == "2L":
            hb_temp = histograms["Z+bb"].Clone()
            for samp in ["s+t chan","Wt",
                         "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                         "ttbar","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                         "WW","WZ","ZZ",]:
                if samp in histograms:
                    hb_temp.Add(histograms[samp])
        else:
            hb_temp = histograms["ttbar"].Clone()
            for samp in ["s+t chan","Wt",
                         "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                         "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                         "WW","WZ","ZZ",]:
                if samp in histograms:
                    hb_temp.Add(histograms[samp])
        hsig_temp = histograms["VH 125"].Clone()
        newMVABins = applyTrafoDToHist(hsig_temp,hb_temp, canName)
    
    rebinning = int(binning.split(",")[0])
    xLow = float(binning.split(",")[1])   
    xHigh = float(binning.split(",")[2])   
    print(binning)
    print(xLow)
    print(xHigh)
    if varName == "pTV":
        if "75_150ptv" in canName:
            xLow = 60
            xHigh = 160
        elif "150_250ptv" in canName:
            xLow = 140
            xHigh = 260
        elif "250_400ptv" in canName:
            xLow = 240
            xHigh = 410
        elif "400ptv" in canName:
            xLow = 390

    for sampl in samples_plots:
        if sampl in histograms:
            if not (rebinning == 0 or varName == "mva"):
                histograms[sampl].Rebin(rebinning)
            elif "mva" in varName:
                rebinTrafoDHist(histograms[sampl], newMVABins)
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)
                #for i in range(histograms[sampl].GetNbinsX()):
                #    if histograms[sampl].GetBinContent(i+1)>0:
                #        histograms[sampl].SetBinError(i+1,math.sqrt(histograms[sampl].GetBinContent(i+1)))
            

    hs = ROOT.THStack("hs","")
    for samp in ["ttbar","s+t chan","Wt",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                 "WW","WZ","ZZ","VH 125",]:
        if samp in histograms:
            hs.Add(histograms[samp])

    if "VH 125" not in histograms:
        return
    hd = ROOT.TH1F()
    try:
        hd = histograms["data"].Clone()
    except:
        hd = histograms["VH 125"].Clone()
        hd.Reset("ICES")
    hb = ROOT.TH1F()
    try:
        hb = histograms["ttbar"].Clone()
    except:
        hb = histograms["VH 125"].Clone()
        hb.Reset("ICES")
    for samp in ["s+t chan","Wt",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                 "WW","WZ","ZZ",]:
        if samp in histograms:
            hb.Add(histograms[samp])
    hsig = histograms["VH 125"].Clone()



    if varName == "GSCptJ1":
        print("background 40-50: {} 50-60: {} total: {} percentage: {}".format(hb.GetBinContent(1),hb.GetBinContent(2),hb.Integral(),100*(hb.GetBinContent(1)+hb.GetBinContent(2))/hb.Integral()))
        print("signal 40-50: {} 50-60: {} total: {} percentage: {}".format(hsig.GetBinContent(1),hsig.GetBinContent(2),hsig.Integral(),100*(hsig.GetBinContent(1)+hsig.GetBinContent(2))/hsig.Integral()))

    #if ("mBB" in varName or "Mbb" in varName) and "60J1" in line[-4]:
    #    getSignificance(hsig,hb,canName+"_"+varName)
    #if ("mva" in varName) and "60J1" in line[-4]:
    #    getSignificanceMVA(hsig,hb,canName+"_"+varName)

    if ("mBB" in varName or "Mbb" in varName): 
        m_sigVals[canName]=getSignificance(hsig,hb,canName+"_"+varName)
        m_yieldVals[canName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBVals[canName]=hsig.Integral()/hb.Integral()
        else:
            m_SBVals[canName]=0

    if ("mva" in varName):
        m_sigValsMVA[canName]=getSignificanceMVA(hsig,hb,canName+"_"+varName)
        m_yieldValsMVA[canName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBValsMVA[canName]=hsig.Integral()/hb.Integral()
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),hsig.Integral()/hb.Integral()))
        else:
            m_SBValsMVA[canName]=0
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),0))

    
    doBlinding(hsig,hb,hd,varName)
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["VH 125"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")

    hd.GetXaxis().SetRangeUser(xLow,xHigh)
    hb.GetXaxis().SetRangeUser(xLow,xHigh)
    hsig.GetXaxis().SetRangeUser(xLow,xHigh)
    hs.GetXaxis().SetLimits(xLow,xHigh)
        

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName+"_"+ptCut+"  "+channel)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    if "data" in histograms:
        legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["VH 125","ZZ","WZ","WW",
                 "Z+l","Z+cl","Z+cc","Z+bl","Z+bc","Z+bb",
                 "W+l","W+cl","W+cc","W+bl","W+bc","W+bb",
                 "Wt","s+t chan","ttbar",]:
        if samp in histograms:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.2
    yHigh = 0.8
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("ep") 
    rat.GetXaxis().SetRangeUser(xLow,xHigh)

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    
    yHigh2 = rat1.GetMaximum()*1.1
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    #rat1.SetAxisRange(0, yHigh2, "Y")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinError(i+1)>0.075:
            rat1.SetBinContent(i+1,(rat1.GetBinContent(i)+rat1.GetBinContent(i+2))/2)
            rat1.SetBinError(i+1,(rat1.GetBinError(i)+rat1.GetBinError(i+2))/2)
            #print("{} {}".format(rat1.GetBinContent(i+1),rat1.GetBinError(i+1)))
    
    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("hist Y+")
    rat1.GetXaxis().SetRangeUser(xLow,xHigh)
    

    plotName = working_dir+"DataMC/DataMC_"+canName+"_"+varName+"_"+ptCut+"_"+channel
    if applyScales:
        plotName+="_Scaled"
    if saveHistos:
        canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def cutsComparisonAllSamples(canName,doSingle):
    for sample in samples_inFile:
        #if sample != "ZJets":
        #    continue
        if doSingle:
            cutsComparisonSingle(canName,sample)
        else:
            cutsComparison(canName,sample)

def cutsComparisonSingle(canName,sample):
    print("cutsComparison")
    # 13TeV_TwoLepton_2tag2jet_150_250ptv_SR_mBB
    line=canName.split("_")
    varName = line[-1]
    canName1 = canName.replace("13TeV",sample)

    h=ROOT.TH1F()
    i=0
    hist_file.cd()
    for samp in samples_inFile[sample]:
        histName1=samp
        #print("Getting hist "+histName1)
        hist_tmp1 = hist_file.Get(histName1)
        if i != 0:
            try:
                h.Add(hist_tmp1.Clone())
            except:
                print(histName1+" doesn't exist")
        else:
            try:
                h=hist_tmp1.Clone()
            except:
                print(histName1+" doesn't exist")
                i-=1
        i+=1
    h1=ROOT.TH1F()
    k=0
    hist_file1.cd()
    for samp in samples_inFile[sample]:
        histName3=samp
        #print("Getting hist "+histName1)
        hist_tmp3 = hist_file1.Get(histName3)
        if k != 0:
            try:
                h1.Add(hist_tmp3.Clone())
            except:
                print(histName3+" doesn't exist")
        else:
            try:
                h1=hist_tmp3.Clone()
            except:
                print(histName3+" doesn't exist")
                k-=1
        k+=1

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h.SetLineColor(ROOT.kBlue)
    h.SetLineWidth(2)
    h1.SetLineColor(ROOT.kRed)
    h1.SetLineWidth(2)

    rebinning = 10
    print(rebinning)
    if rebinning != 0:
        h.Rebin(rebinning)
        h1.Rebin(rebinning)
    '''
    xLow = float(binning.split(",")[1])   
    xHigh = float(binning.split(",")[2])   
    if varName == "pTV" or varName == "MET":
        if "75_150ptv" in canName:
            xLow = 60
            xHigh = 160
        elif "150_250ptv" in canName:
            xLow = 140
            xHigh = 260
        elif "250_400ptv" in canName:
            xLow = 240
            xHigh = 410
        elif "400ptv" in canName:
            xLow = 390
    h.GetXaxis().SetRangeUser(xLow,xHigh)
    h1.GetXaxis().SetRangeUser(xLow,xHigh)
    '''
    
    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    #upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.025, 0.995, 0.995)
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd() #.SetBottomMargin(0.2)
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h.GetMaximum(),h1.GetMaximum())*1.4 
    
    rat = h1.Clone()

    h.Draw("hist")
    h.SetTitle("")
    h.SetAxisRange(0.0, y_max, "Y")
    h.GetYaxis().SetMaxDigits(3)
    h.GetYaxis().SetTitle("Entries")
    h.GetYaxis().SetTitleSize(0.04)
    h.GetYaxis().SetTitleOffset(1.2)
    h.GetYaxis().SetLabelSize(0.05) #0.05
    h.SetYTitle("Entries")
    h.SetLabelOffset(5)

    h1.Draw("same hist")

    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    #legend1 = ROOT.TLegend(0.55, 0.75, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    #legend1.AddEntry(h, "20J2-20J3, {:.1f}, {:.1f}".format(h.GetEntries(),h.Integral()), "l")
    legend1.AddEntry(h, "Old, {:.1f}, {:.1f}".format(h.GetEntries(),h.Integral()), "l")
    legend1.AddEntry(h1, "New, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    #t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    #t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    #t.DrawLatex(0.15, 0.75, "#it{#bf{0L, == 3 Jets, 2 b-tag}}")
    #t.DrawLatex(0.15, 0.70, "#it{#bf{p_{T}^{V} > 150, SR}}")
    t.DrawLatex(0.15, 0.85, canName1)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)
    
    rat.Divide(h)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.25
    yHigh = 1.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetXaxis().SetTitle(varName)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("New/Old")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("p") 
    
    #h.GetXaxis().SetTitleSize(0.04)
    #h.GetXaxis().SetTitleOffset(1.04)
    #h.GetXaxis().SetLabelSize(0.04)
    #h.GetXaxis().SetTitle("p_{T}(J3) [GeV]")
    #h.SetAxisRange(0, 250, "X")
        
    canvas_hs.SaveAs(working_dir+canName1+".png")
    canvas_hs.Close()

def cutsComparison(canName,sample):
    print("cutsComparison")
    # 13TeV_TwoLepton_2tag2jet_150_250ptv_SR_mBB
    line=canName.split("_")
    varName = line[-1]
    canName1 = canName.replace("13TeV",sample)

    h=ROOT.TH1F()
    i=0
    hist_file.cd()
    for samp in samples_inFile[sample]:
        histName1=samp
        #print("Getting hist "+histName1)
        hist_tmp1 = hist_file.Get(histName1)
        if i != 0:
            try:
                h.Add(hist_tmp1.Clone())
            except:
                print(histName1+" doesn't exist")
        else:
            try:
                h=hist_tmp1.Clone()
            except:
                print(histName1+" doesn't exist")
                i-=1
        i+=1
    h1=ROOT.TH1F()
    i=0
    hist_file1.cd()
    for samp in samples_inFile[sample]:
        histName2=samp
        #print("Getting hist "+histName1)
        hist_tmp2 = hist_file1.Get(histName2)
        if i != 0:
            try:
                h1.Add(hist_tmp2.Clone())
            except:
                print(histName2+" doesn't exist")
        else:
            try:
                h1=hist_tmp2.Clone()
            except:
                print(histName2+" doesn't exist")
                i-=1
        i+=1

    h2=ROOT.TH1F()
    i=0
    hist_file2.cd()
    for samp in samples_inFile[sample]:
        histName3=samp
        #print("Getting hist "+histName1)
        hist_tmp3 = hist_file2.Get(histName3)
        if i != 0:
            try:
                h2.Add(hist_tmp3.Clone())
            except:
                print(histName3+" doesn't exist")
        else:
            try:
                h2=hist_tmp3.Clone()
            except:
                print(histName3+" doesn't exist")
                i-=1
        i+=1

    h3=ROOT.TH1F()
    i=0
    hist_file3.cd()
    for samp in samples_inFile[sample]:
        histName4=samp
        #print("Getting hist "+histName1)
        hist_tmp4 = hist_file3.Get(histName4)
        if i != 0:
            try:
                h3.Add(hist_tmp4.Clone())
            except:
                print(histName4+" doesn't exist")
        else:
            try:
                h3=hist_tmp4.Clone()
            except:
                print(histName4+" doesn't exist")
                i-=1
        i+=1

    h4=ROOT.TH1F()
    i=0
    hist_file4.cd()
    for samp in samples_inFile[sample]:
        histName5=samp
        #print("Getting hist "+histName1)
        hist_tmp5 = hist_file4.Get(histName5)
        if i != 0:
            try:
                h4.Add(hist_tmp5.Clone())
            except:
                print(histName5+" doesn't exist")
        else:
            try:
                h4=hist_tmp5.Clone()
            except:
                print(histName5+" doesn't exist")
                i-=1
        i+=1

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h.SetLineColor(ROOT.kCyan)
    h.SetLineWidth(2)
    h1.SetLineColor(ROOT.kMagenta)
    h1.SetLineWidth(2)
    h2.SetLineColor(ROOT.kBlue)
    h2.SetLineWidth(2)
    h3.SetLineColor(ROOT.kBlack)
    h3.SetLineWidth(2)
    h4.SetLineColor(ROOT.kRed)
    h4.SetLineWidth(2)

    hest = h.Clone()
    hest.Add(h1)
    hest.Scale(0.5)
    hest.SetLineColor(ROOT.kGray)
    hest.SetLineWidth(2)

    rebinning = 10
    if varName == "mva":
        rebinning = 25
    print(rebinning)
    if rebinning != 0:
        h.Rebin(rebinning)
        h1.Rebin(rebinning)
        h2.Rebin(rebinning)
        h3.Rebin(rebinning)
        h4.Rebin(rebinning)
        hest.Rebin(rebinning)

    
    xLow = 0
    xHigh = 300
    if varName == "pTV" or varName == "MET":
        if "75_150ptv" in canName:
            xLow = 60
            xHigh = 160
        elif "150_250ptv" in canName:
            xLow = 140
            xHigh = 260
        elif "250_400ptv" in canName:
            xLow = 240
            xHigh = 410
        elif "400ptv" in canName:
            xLow = 390
    elif varName == "mva":
        xLow = -1
        xHigh = 1
    h.GetXaxis().SetRangeUser(xLow,xHigh)
    h1.GetXaxis().SetRangeUser(xLow,xHigh)
    h2.GetXaxis().SetRangeUser(xLow,xHigh)
    h3.GetXaxis().SetRangeUser(xLow,xHigh)
    h4.GetXaxis().SetRangeUser(xLow,xHigh)
    hest.GetXaxis().SetRangeUser(xLow,xHigh)
    
    
    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    #upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.025, 0.995, 0.995)
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd() #.SetBottomMargin(0.2)
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h.GetMaximum(),h1.GetMaximum(),h2.GetMaximum(),h3.GetMaximum(),h4.GetMaximum(),hest.GetMaximum())*1.4 
    
    rat = h2.Clone()
    rat1 = h4.Clone()
    ratest = h3.Clone()

    h.Draw("hist")
    h.SetTitle("")
    h.SetAxisRange(0.0, y_max, "Y")
    h.GetYaxis().SetMaxDigits(3)
    h.GetYaxis().SetTitle("Entries")
    h.GetYaxis().SetTitleSize(0.04)
    h.GetYaxis().SetTitleOffset(1.2)
    h.GetYaxis().SetLabelSize(0.05) #0.05
    h.SetYTitle("Entries")
    h.SetLabelOffset(5)

    h1.Draw("same hist")
    h2.Draw("same hist")
    h3.Draw("same hist")
    h4.Draw("same hist")
    hest.Draw("same hist")

    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    #legend1 = ROOT.TLegend(0.55, 0.75, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    #legend1.AddEntry(h, "20J2-20J3, {:.1f}, {:.1f}".format(h.GetEntries(),h.Integral()), "l")
    legend1.AddEntry(h, "Old Nom, {:.1f}, {:.1f}".format(h.GetEntries(),h.Integral()), "l")
    legend1.AddEntry(hest, "Old 25J3 est., {:.1f}, {:.1f}".format(hest.GetEntries(),hest.Integral()), "l")
    legend1.AddEntry(h1, "Old 30J3, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
    legend1.AddEntry(h2, "New Nom, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
    legend1.AddEntry(h3, "New 25J3, {:.1f}, {:.1f}".format(h3.GetEntries(),h3.Integral()), "l")
    legend1.AddEntry(h4, "New 30J3, {:.1f}, {:.1f}".format(h4.GetEntries(),h4.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    #t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    #t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    #t.DrawLatex(0.15, 0.75, "#it{#bf{0L, == 3 Jets, 2 b-tag}}")
    #t.DrawLatex(0.15, 0.70, "#it{#bf{p_{T}^{V} > 150, SR}}")
    t.DrawLatex(0.15, 0.85, canName1)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)
    
    rat.Divide(h)
    rat1.Divide(h1)
    ratest.Divide(hest)

    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.25
    if varName == "mva":
        yLow = 0.4
        yHigh = 1.5
        
    rat.SetLineWidth(2)
    #rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetXaxis().SetTitle(varName)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("New/Old")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("p") 
    rat1.Draw("p same") 
    ratest.Draw("p same") 
    
    #h.GetXaxis().SetTitleSize(0.04)
    #h.GetXaxis().SetTitleOffset(1.04)
    #h.GetXaxis().SetLabelSize(0.04)
    #h.GetXaxis().SetTitle("p_{T}(J3) [GeV]")
    #h.SetAxisRange(0, 250, "X")
        
    canvas_hs.SaveAs(working_dir+canName1+"_FullNewOldComp.png")
    canvas_hs.Close()

#main

###############################################
###############################################
#  Ideas:
#  make yield tables of SR vs CR, including percentage in each category
###############################################
###############################################


print("Making plots")
sys.argv.append("-b")
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gROOT.SetBatch(True)


doSingleCompare = False
cutsComparisonAllSamples(studyRegion,doSingleCompare)



hist_file.Close()
hist_file1.Close()
hist_file2.Close()
hist_file3.Close()
hist_file4.Close()

print("")
print("")
print("")
print("All complete")
#exit()

