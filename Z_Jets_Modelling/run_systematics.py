"""
Zjets systematics calculations
Recipes specify:
jet multiplicity
ptv region
whether or not cut out signal window
extra options like topCR subtraction or signal MC subtraction
"""
import parse_args
import systematics
import sys
import os


def main(args):
    """
    Main function prepares arguments and calls do_systematics
    """
    if args.tree_type == "easy":
        two_lep_recipe = [
            ("two_jet_inclusive_blind",),  # don't forget the comma or its not a tuple!
            ("two_jet_inclusive_blind", "topdd"),
            ("two_plus_jet_inclusive_blind",),
            ("two_plus_jet_inclusive_blind", "topdd"),
            ("three_plus_jet_inclusive_blind",),
            ("three_plus_jet_inclusive_blind", "topdd"),
        ]
    elif args.tree_type == "mva":
        two_lep_recipe = [
            ("two_jet_inclusive_blind",),  # don't forget the comma or its not a tuple!
            ("two_plus_jet_inclusive_blind",),
            ("three_jet_inclusive_blind",),
            ("three_plus_jet_inclusive_blind",),
        ]

    zero_lep_recipe = [
        ("two_jet_inclusive_blind",),  # don't forget the comma or its not a tuple!
        ("two_jet_inclusive_blind", "doMGvSh"),
        ("two_plus_jet_inclusive_blind",),
        ("two_plus_jet_inclusive_blind", "doMGvSh"),
        ("three_jet_inclusive_blind",),
        ("three_jet_inclusive_blind", "doMGvSh"),
        ("three_plus_jet_inclusive_blind",),
        ("three_plus_jet_inclusive_blind", "doMGvSh"),
        ("two_plus_three_jet_inclusive_blind",),
        ("two_plus_three_jet_inclusive_blind", "doMGvSh"),
    ]

    if args.channel == "2lep":
        systematics.do_systematics(args, two_lep_recipe)
    elif args.channel == "0lep":
        systematics.do_systematics(args, zero_lep_recipe)
    else:
        print("unknown channel")

    print("\n\n\t Completed \n\n")


if __name__ == "__main__":
    args = parse_args.define_args()
    # Some checks to see that files exist before running.
    if not (args.channel == "0lep" or args.channel == "2lep"):
        print(
            "ERROR: channel was not defined correctly."
            "Please input 0lep or 2lep. \n Exiting now"
        )
        exit()
    if not os.path.isfile("samples/" + args.sample_list):
        print(
            "ERROR: The file you have included for the sample list, {},"
            "does not exist \n Exiting now".format("samples/" + args.sample_list)
        )
        exit()
    if not os.path.isfile("variables/" + args.variable_list):
        print(
            "ERROR: The file you have included for the variable list, {},"
            "does not exist \n Exiting now".format("variables/" + args.variable_list)
        )
        exit()

    main(args)
