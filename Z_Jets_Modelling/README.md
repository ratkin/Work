# vhbb-zjets-data-driven

Code to generate the Z + jets data driven systematics in the 0 and 2 lepton channel of the VHbb analysis.

```
usage: run_systematics.py [-h] [--channel CHANNEL] [--tree_type TREE_TYPE]
                          [--sample_list SAMPLE_LIST]
                          [--variable_list VARIABLE_LIST]
                          [--force_histograms FORCE_HISTOGRAMS]
                          sample_dirs
```

Required arguments:

| __Argument__  | __Description__                                                      |
| :---          | :---                                                                 |
| `sample_dirs` | comma separated list of paths to directories where tuples are stored |

Optional arguments:

| __Argument__         | __Default__         | __Description__                                       |
| :---                 | :---                | :---                                                  |
| `--channel`          | `"2lep"`            | Analysis channel, 2lep or 0lep supported              |
| `--tree_type`        | `"mva"`             | CxAODReader tuple type, mvatree or easytree supported |
| `--sample_list`      | `"default.txt"`     | Name of text file stored in the samples directory, text file should have the name of one sample and a description of that sample per line, space separated |
| `--variable_list`    | `"data-driven.txt"` | Name of text file stored in the variables directory, text file should list on variable per line with plotting options space separated, see default as an example |
| `--force_histograms` | `"False"`           | force the recreating of histograms even if they already exist |
