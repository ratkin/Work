"""
Helper functions to calculate systematics
"""
import os
import sys

from array import array
import plot_helpers as plot
import plotting as plt
import sample_handler
import checks
#import numpy as np

import cuts


def handle_df(bkg, df, v_name, options, mc_only):
    """
    writes histogram of variable from the data frame
    """
    sys.argv.append("-b")
    import ROOT

    n_bins = options[0]
    x_min = options[1]
    x_max = options[2]

    # print('making histogram with:\n'
    #       'n_bins: {}\n'.format(n_bins)
    #       + 'x min: {}\n'.format(x_min)
    #       + 'x max: {}'.format(x_max))

    # print('from dataframe column with:\n'
    #       'min: {}\n'.format(df.Min(v_name).GetValue())
    #       + 'max: {}\n'.format(df.Max(v_name).GetValue())
    #       )

    if bkg == "data" and mc_only:
        return

    print("Making {0} {1} histogram".format(bkg, v_name))
    h = ROOT.TH1D(bkg + "-" + v_name, bkg + "-" + v_name, n_bins, x_min, x_max)
    h_model = ROOT.RDF.TH1DModel(h)
    hist = df.Histo1D(h_model, v_name, "EventWeight")
    hist = hist.Clone()
    hist.Sumw2()

    #h1 = ROOT.TH1D(bkg + "-" + v_name + "-unweighted", bkg + "-" + v_name + "-unweighted", n_bins, x_min, x_max)
    #h_model1 = ROOT.RDF.TH1DModel(h1)
    #hist1 = df.Histo1D(h_model1, v_name)
    #hist1 = hist1.Clone()
    #hist1.Sumw2()
    
    hist.Write()
    #hist1.Write()

def make_histograms(name, dataframes, variables, args, mc_only=False):
    """
    Make histograms of the variables from the dataframes supplied.
    name - a string used to identify the histograms
    dataframes - RDataFrames containing the data
    variables - a dict with keys of variable names, and values which are tuples specifying
    histogram options
    """
    sys.argv.append("-b")
    import ROOT

    """
    Make histograms of the variables provided (with options) from the dataframes provided.
    """
    print("making histograms under {}".format(name))
    for v_name, options in variables.iteritems():
        if "unblind" in name and v_name == "mBB" and not mc_only:
            continue
        data_hist = None
        working_dir = "histograms/{0}/{1}/{2}/".format(
            args.channel, args.tree_type, name
        )
        if not os.path.isdir(working_dir):
            os.makedirs(working_dir)

        hist_file_name = working_dir + "{}.root".format(v_name)
        if os.path.isfile(hist_file_name) and not args.force_histograms:
            print("{} already exists, not creating.".format(hist_file_name))
            continue
        elif os.path.isfile(hist_file_name) and args.force_histograms:
            print("{} already exists, not creating.".format(hist_file_name))
            print("--force_histograms set to True, overwriting")
            hist_file = ROOT.TFile(hist_file_name, "RECREATE")
        else:
            hist_file = ROOT.TFile(hist_file_name, "CREATE")

        assert hist_file.IsOpen()

        hist_file.Write()
        # x = np.array([(1.0), (3.0, 4)], dtype=[('x', '<f8'), ('y', '<i8')])
        print(args.tree_type)
        [
            handle_df(bkg, df, v_name, options, mc_only)
            for bkg, df in dataframes.iteritems()
        ]
        hist_file.Write()
        hist_file.Close()


def sort_histograms(
    name,
    processes,
    variables,
    args,
    handle_signal=False,
    mc_only=False,
    skip_top=False,
    doMGvSh=False,
):
    """
    Sort histograms stored under the given name.
    Use list of processes (actually just processes) and variables to do the sorting.
    """
    sys.argv.append("-b")
    import ROOT

    filename_stem = "histograms/{0}/{1}/{2}/".format(args.channel, args.tree_type, name)
    sorted_hists_by_variable = {}
    vals_errs = {}
    for v_name, options in variables.iteritems():
        filename = filename_stem + v_name + ".root"
        tfile = ROOT.TFile(filename)
        if 'two' in name and (v_name=='pTJ3' or v_name=='mBBJ'):
            print("Skipping variable {} since in two jet region".format(v_name))
            continue
        bin_content_by_bkg = {}
        processed_histograms = {}
        first = True
        if "unblind" in name and v_name == "mBB" and not mc_only:
            continue
        for i, background in enumerate(processes):
            if "Hcc" in background or ("Hbb" in background and not handle_signal):
                continue
            elif skip_top and ("ttbar" in background or "stop" in background):
                continue
            # following conditions are 0 lepton specific
            if doMGvSh and not ("Zjets" in background or "ZjetsMG" in background):
                continue
            elif not doMGvSh and "ZjetsMG" in background:
                continue

            h_name = background + "-" + v_name
            h = tfile.Get(h_name)
            ROOT.gROOT.cd()
            this_hist = h.Clone()
            '''
            f_bin=this_hist.GetXaxis().FindBin(150.5)
            v_e=[[],[]]
            for i in range(10):
                j=i+f_bin
                v_e[0].append(this_hist.GetBinContent(j))
                v_e[1].append(this_hist.GetBinError(j))
            vals_errs[background]=v_e
            '''
            if background == "Zjets" or (background == "ZjetsMG" and doMGvSh):
                processed_histograms[background] = this_hist
            elif background == "data" and not mc_only:
                processed_histograms[background] = this_hist
            elif background == "data" and mc_only:
                continue
            else:
                if first:
                    first = False
                    processed_histograms["background"] = this_hist
                    # print('added {} to histogram of non zjets processes'.format(background))
                else:
                    processed_histograms["background"].Add(this_hist)
                    # print('added {} to histogram of non zjets processes'.format(background))
        tfile.Close()
        sorted_hists_by_variable[v_name] = processed_histograms
    '''
    bkg_vals=vals_errs["Wjets"]
    print("\nbackground: {}".format("Wjets"))
    print("Orig vals: {}".format( bkg_vals[0][:5]))
    print("           {}".format( bkg_vals[0][5:]))
    print("Orig errs: {}".format( bkg_vals[1][:5]))
    print("           {}".format( bkg_vals[1][5:]))
    for back,v_e in vals_errs.iteritems():
        print("\nbackground: {}".format(back))
        print("  vals: {}".format(v_e[0][:5]))
        print("        {}".format(v_e[0][5:]))
        print("  errs: {}".format(v_e[1][:5]))
        print("        {}".format(v_e[1][5:]))
        if not (back == "data" or back == "Zjets" or back == "Wjets"):
            for i in range(10):
                bkg_vals[0][i]+=v_e[0][i]
                bkg_vals[1][i]=ROOT.TMath.Sqrt(bkg_vals[1][i]**2 + v_e[1][i]**2)
            print("new value from bkg {} is {}".format(back,bkg_vals[0][0])) 
    print("")
    print("non_zjets total")
    print("  vals: {}".format(bkg_vals[0][:5]))
    print("        {}".format(bkg_vals[0][5:]))
    print("  errs: {}".format(bkg_vals[1][:5]))
    print("        {}\n".format(bkg_vals[1][5:]))
    subtracted=vals_errs["data"]
    fd_val=fd_err=0
    fz_val=fz_err=0
    for i in range(10):
        subtracted[0][i]=subtracted[0][i]-bkg_vals[0][i]
        subtracted[1][i]=ROOT.TMath.Sqrt(subtracted[1][i]**2+bkg_vals[1][i]**2)
    print("subtracted val: {}".format(subtracted[0][:5]))
    print("                {}".format(subtracted[0][5:]))
    print("subtracted err: {}".format(subtracted[1][:5]))
    print("                {}\n".format(subtracted[1][5:]))
    for i in range(10):
        fd_val+=subtracted[0][i]
        fd_err=ROOT.TMath.Sqrt(fd_err**2+subtracted[1][i]**2)
        fz_val+=vals_errs["Zjets"][0][i]
        fz_err=ROOT.TMath.Sqrt(fz_err**2+vals_errs["Zjets"][1][i]**2)
    print("final data val: {} final zjets val: {}".format(fd_val/10,fz_val/10))
    print("final data err: {} final zjets err: {}\n".format(fd_err/10,fz_err/10))
    
    print("\nfinal ratio val: {} err: {}\n".format(fd_val/fz_val,(fd_val/fz_val)*ROOT.TMath.Sqrt((fd_err/fd_val)**2+(fz_err/fz_val)**2)))
    '''
    return sorted_hists_by_variable


def subtract_background(
    sorted_hists_by_variable, top_dd=False, jet_multi=None, doMGvSh=False
):
    """
    Take sorted histograms subtract background from data, return zjets, data tuple.
    """
    sys.argv.append("-b")
    import ROOT

    data_mc_tuples_by_variable = {}
    for v_name, sorted_histograms in sorted_hists_by_variable.iteritems():
        if top_dd:
            assert jet_multi
            scale_factor = calculate_sf(jet_multi)
            topemuCR_file = ROOT.TFile(
                "histograms/2lep/easy/{}_jet_topemucr/{}.root".format(jet_multi, v_name)
            )
            ROOT.gROOT.cd()
            topemuCR_data = topemuCR_file.Get("data-{}".format(v_name))
            scale_factor = calculate_sf(jet_multi)

        ROOT.gROOT.cd()

        if doMGvSh:
            data_mc_tuples_by_variable[v_name] = (
                sorted_histograms["Zjets"].Clone(),
                sorted_histograms["ZjetsMG"].Clone(),
            )
            continue

        data_hist = sorted_histograms["data"]
        background_hist = sorted_histograms["background"]

        data_hist.Add(background_hist, -1)

        if top_dd:
            data_hist.Add(background_hist, -1 * scale_factor)

        background_subtracted_data = data_hist.Clone()

        zjets = sorted_histograms["Zjets"].Clone()
        data_mc = (zjets, background_subtracted_data)
        data_mc_tuples_by_variable[v_name] = data_mc

    return data_mc_tuples_by_variable


def calculate_sf(jet_multi):
    """
    Calculate scale-factor between topemuCR and SR.
    """
    sys.argv.append("-b")
    import ROOT

    topemuCR_file = ROOT.TFile(
        "histograms/2lep/easy/{}_jet_topemucr/pTV.root".format(jet_multi)
    )
    sr_file = ROOT.TFile(
        "histograms/2lep/easy/{}_jet_inclusive_blind/pTV.root".format(jet_multi)
    )

    topemuCR_ttbar = topemuCR_file.Get("ttbar-pTV")
    sr_ttbar = sr_file.Get("ttbar-pTV")

    topemuCR_stop = topemuCR_file.Get("stop-pTV")
    sr_stop = sr_file.Get("stop-pTV")

    topemuCR_ttbar.Add(topemuCR_stop, 1)
    sr_ttbar.Add(sr_stop, 1)

    bins = [75000, 120000, 150000, 200000, 250000, 300000, 1000000]
    # bins = [75000, 125000, 175000, 225000, 275000, 325000, 1000000]
    n_bins = len(bins) - 1
    bins = array("d", bins)

    topemuCR_ttbar = topemuCR_ttbar.Rebin(n_bins, "", bins)
    sr_ttbar = sr_ttbar.Rebin(n_bins, "", bins)

    canvas = ROOT.TCanvas("scale-factor", "scale-factor", 900, 900)
    # canvas.SetBottomMargin(0.15)
    upper, lower = plot.vertical_2_pad(0.01, 0.00001, 0.35)
    lower.cd().SetBottomMargin(0.25)

    upper.SetGridx()
    upper.SetGridy()

    lower.SetGridx()
    lower.SetGridy()

    upper.cd()
    legend = ROOT.TLegend(0.6, 0.65, 0.9, 0.8)
    ROOT.gStyle.SetOptStat(0)

    topemuCR_ttbar.SetLineColor(ROOT.kBlue)
    sr_ttbar.SetLineColor(ROOT.kRed)

    topemuCR_ttbar.Draw()
    sr_ttbar.Draw("same")

    scale_factor = sr_ttbar.Integral() / topemuCR_ttbar.Integral()

    lower.cd()

    ratio = sr_ttbar.Clone()
    ratio.Divide(topemuCR_ttbar)

    baseline = ROOT.TH1F("baseline", "baseline", 100, 0.0, 1000000.0)
    for i in range(baseline.GetNbinsX()):
        baseline.SetBinContent(i, 1)

    baseline.SetLineColor(ROOT.kBlue)
    baseline.Draw("l")
    baseline.SetAxisRange(0.8, 1.2, "Y")
    ratio.SetLineColor(ROOT.kRed)
    ratio.SetMarkerColor(ROOT.kRed)
    ratio.Draw("same pe")

    print("{}, {}".format(jet_multi, scale_factor))
    plot_dir = "plots/easy"
    if not os.path.isdir(plot_dir):
        os.makedirs(plot_dir)
    canvas.SaveAs("plots/easy/scale_factor_{}_jet.pdf".format(jet_multi))
    canvas.Close()
    topemuCR_file.Close()
    sr_file.Close()

    return scale_factor


def get_dataframes(args, samples):
    """
    Makes TChains of sample files for each process
    """
    sys.argv.append("-b")
    import ROOT

    RDF = ROOT.ROOT.RDataFrame

    chains_by_type = {_type: ROOT.TChain("Nominal") for _type in samples.keys()}

    for _type, samples in samples.iteritems():
        dirs = sample_handler.sample_dirs(args)
        for sample in samples:
            for d in dirs:
                # print('adding {0} to chain {1}'.format(d + sample, _type))
                chains_by_type[_type].Add(d + sample)

    dataframes_by_type = {
        _type: RDF(chain) for _type, chain in chains_by_type.iteritems()
    }

    return dataframes_by_type


def process_dataframes(args, dataframes_by_type_0):
    """
    process dataframes into regions (signal etc.)
    """

    dataframes_by_type = {
        bkg:get_pTBB(df) for bkg, df in dataframes_by_type_0.iteritems()
    }
    #dataframes_by_type['Wjets'].Snapshot('nom','hello.root')
    #dataframes_by_type = {
    #    bkg:get_METpT(df) for bkg, df in dataframes_by_type_1.iteritems()
    #}

    if args.channel == "2lep":

        signal_region = {
            bkg: cuts.signal_region(df, args)
            for bkg, df in dataframes_by_type.iteritems()
        }

        blinded = {
            _type: cuts.blind(df, args) for _type, df in signal_region.iteritems()
        }

        inside = {
            _type: cuts.inside_mbb_window(df, args)
            for _type, df in signal_region.iteritems()
        }

        dataframes = {
            "inclusive_unblind": signal_region,
            "inclusive_blind": blinded,
            "inclusive_unblind_inside": inside,
        }

        if args.tree_type == "easy":
            top_CR = {
                bkg: cuts.topemuCR(df) for bkg, df in dataframes_by_type.iteritems()
            }

            dataframes["topemucr"] = top_CR

    elif args.channel == "0lep":
        signal_region = {
            bkg: cuts.signal_region(df, args)
            for bkg, df in dataframes_by_type.iteritems()
        }

        blinded = {
            _type: cuts.blind(df, args) for _type, df in signal_region.iteritems()
        }

        inside = {
            _type: cuts.inside_mbb_window(df, args)
            for _type, df in signal_region.iteritems()
        }

        dataframes = {
            "inclusive_unblind": signal_region,
            "inclusive_blind": blinded,
            "inclusive_unblind_inside": inside,
        }

    else:
        print("undefined channel: {}".format(args.channel))
        exit(1)
    return dataframes


def make_all_n_jet_histograms(args, dataframes, n, variables, plus=False):
    """
    makes histograms for all variables and dataframes for a given jet multiplicity
    """
    n_string = ""
    if n == 2:
        n_string = "two"
    elif n == 3:
        n_string = "three"

    if plus:
        n_string += "_plus"

    if n == 2.5:
        n_string = "two_plus_three"

    for region, df in dataframes.iteritems():
        #print("Region: {}".format(region))
        n_jet_df = {
            bkg: cuts.get_n_jet(DF, n, args, plus=plus) for bkg, DF in df.iteritems()
        }
        make_histograms("{}_jet_".format(n_string) + region, n_jet_df, variables, args)

    return


def run_recipe(args, name, processes, variables, skip_top, top_dd, jet_multi, doMGvSh):
    """
    Run a single recipe.
    A recipe defines how to sort histograms, subtract backgrounds and do plots
    """
    sorted_histograms = sort_histograms(
        name, processes, variables, args, skip_top=skip_top, doMGvSh=doMGvSh
    )

    data_mc_tuples = subtract_background(
        sorted_histograms, top_dd=top_dd, jet_multi=jet_multi, doMGvSh=doMGvSh
    )

    if skip_top and top_dd:
        name += "_ttbardd"

    # At some point one plotting function should take all the options required to do all standard plots
    # Once this is the case we can remove the branch below
    fits_2lep={}
    if args.channel == "2lep":
        fits_2lep=plt.plot_histograms(
            name, data_mc_tuples, variables, args, top_dd=top_dd, do_fit=True
        )
    elif args.channel == "0lep":
        plt.plot_histos_0lep(name, data_mc_tuples, variables, args, doMGvSh)
    
    return fits_2lep

def run_recipes(args, recipes, processes, variables):
    """
    For some user supplied recipes unpack them into recipes that run_recipe understands
    and run them all.
    """
    fits_2lep={}
    for recipe in recipes:
        skip_top = False
        top_dd = False
        doMGvSh = False
        jet_multi = None
        print(recipe)
        name = recipe[0]
        print(name)
        if "two_plus" in name:
            jet_multi = "two_plus"
        elif "three_plus" in name:
            jet_multi = "three_plus"
        elif "two" in name:
            jet_multi = "two"
        elif "three" in name:
            jet_multi = "three"

        if len(recipe) > 1:
            if recipe[1] == "topdd":
                skip_top = True
                top_dd = True
            elif recipe[1] == "doMGvSh":
                doMGvSh = True

        fits_2lep.update(run_recipe(
            args, name, processes, variables, skip_top, top_dd, jet_multi, doMGvSh
        ))
    if args.channel=='2lep':
        print("The fit parameters from the 2 lepton plots\n")
        for name, pars in fits_2lep.iteritems():
            if len(pars)==4:
                print("{0},{1},{2},{3},{4}".format(name,pars[0],pars[1],pars[2],pars[3]))
            else:
                print(name)
        print("")


def run_checks(args, processes, variables):
    """
    Run all the checks for a given set of options
    """
    if args.channel == "2lep":
        checks.two_lep_checks(processes, variables, args)
    elif args.channel == "0lep":
        print("no checks for 0 lep yet")
    else:
        print("unknown channel")


def get_pTBB(df):
    return df.Define("pTBB","TLorentzVector L1, L2; L1.SetPtEtaPhiM(pTB1,etaB1,phiB1,mB1); L2.SetPtEtaPhiM(pTB2,etaB2,phiB2,mB2); return (L1+L2).Pt();")#.Define("METpT","double x, y; x=pTV*TMath::Cos(phiV)+MET*TMath::Sin(phiMET); y=pTV*TMath::Sin(phiV)+MET*TMath::Sin(phiMET); return TMath::Sqrt(pow(x,2)+pow(y,2));")
    #df.Define("mETpT","return MET;")
    #return df

def prepare_for_stack(name, args, processes, variables):
    sys.argv.append("-b")
    import ROOT

    filename_stem = "histograms/{0}/{1}/{2}/".format(args.channel, args.tree_type, name)
    data_mc_for_stack_by_var = {}
    for v_name, options in variables.iteritems():
        if "two" in name and "plus" not in name and (v_name=="pTJ3" or v_name=="mBBJ"):
            continue
        filename = filename_stem + v_name + ".root"
        tfile = ROOT.TFile(filename)
        mc_processes = {}
        data = None
        first = True
        for i, process in enumerate(processes):
            if "Hcc" in process or 'ZjetsMG' in process:
                continue
            h_name = process + "-" + v_name
            h = tfile.Get(h_name)
            ROOT.gROOT.cd()
            this_hist = h.Clone()
            if "data" in process:
                data = this_hist
            else:
                mc_processes[process] = this_hist
        tfile.Close()
        data_mc_for_stack_by_var[v_name] = (data, mc_processes)

    return data_mc_for_stack_by_var
