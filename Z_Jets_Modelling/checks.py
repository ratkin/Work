"""
Checks are for studying properties of the variables/phase space.
These checks don't affect the calculation of the nuisance parameter priors.
They inform decisions made by the group and provide material for the internal modeling note.
"""
import plot_helpers as plot
import sys_helpers as hlp
from array import array
import sys


def plot_in_vs_out(inside, outside, args, name="two_plus_jet_inclusive"):
    """
    Plots ptv spectrum of events inside and outside signal window
    Data and MC are plotted for both, and a double ratio is calculated and plotted
    """
    sys.argv.append("-b")
    import ROOT

    ROOT.gStyle.SetOptStat(0)
    # in_window_file = ROOT.TFile("histograms/two_plus_jet_inclusive_unblind_inside/pTV.root")
    # blind_file     = ROOT.TFile("histograms/two_plus_jet_inclusive_blind/pTV.root")

    data_in_window = inside["pTV"][0]  # in_window_file.Get('data-pTV')
    blind_data = outside["pTV"][0]  # blind_file.Get('data-pTV')

    zjets_in_window = inside["pTV"][1]  # in_window_file.Get('Zjets-pTV')
    blind_zjets = outside["pTV"][1]  # blind_file.Get('Zjets-pTV')

    # n_bins, bins = plot.get_variable_bins(blind_data, 1.5, min_bins=20)

    bins = [75, 100, 120, 135, 150, 200, 250, 300, 400, 600, 1000]
    if args.tree_type == "easy":
        bins = [edge * 1e3 for edge in bins]
    n_bins = len(bins) - 1
    bins = array("d", bins)

    canvas = ROOT.TCanvas("ptv", "ptv", 900, 900)

    upper = ROOT.TPad("upper", "upper", 0.05, 0.35, 0.995, 0.995)
    middle = ROOT.TPad("middle", "middle", 0.05, 0.23, 0.995, 0.40)
    lower = ROOT.TPad("lower", "lower", 0.05, 0.05, 0.995, 0.24)

    upper.Draw()
    middle.Draw()
    lower.Draw()

    lower.cd().SetBottomMargin(0.25)

    upper.cd()
    upper.SetGridx()
    upper.SetGridy()

    blind_data = plot.rebin(blind_data, n_bins, bins, variable=True)
    data_in_window = plot.rebin(data_in_window, n_bins, bins, variable=True)
    blind_zjets = plot.rebin(blind_zjets, n_bins, bins, variable=True)
    zjets_in_window = plot.rebin(zjets_in_window, n_bins, bins, variable=True)

    blind_data.SetMarkerColor(ROOT.kRed)
    blind_data.SetMarkerStyle(8)
    blind_data.SetLineColor(ROOT.kRed)
    blind_data.SetLineWidth(1)

    blind_zjets.SetMarkerColor(ROOT.kRed)
    blind_zjets.SetMarkerStyle(8)
    blind_zjets.SetLineColor(ROOT.kRed)
    blind_zjets.SetLineWidth(1)

    data_in_window.SetMarkerColor(ROOT.kBlue)
    data_in_window.SetMarkerStyle(8)
    data_in_window.GetXaxis().SetLabelOffset(5)

    zjets_in_window.SetMarkerColor(ROOT.kBlue)
    zjets_in_window.SetMarkerStyle(8)
    zjets_in_window.SetLineColor(ROOT.kBlue)
    zjets_in_window.SetLineWidth(1)
    zjets_in_window.GetXaxis().SetLabelOffset(5)

    if not blind_data.Integral() == 0:
        blind_data.Scale(1.0 / blind_data.Integral())
    else:
        print("WARNING could not scale, blind_data is empty")

    if not blind_zjets.Integral() == 0:
        blind_zjets.Scale(1.0 / blind_zjets.Integral())
    else:
        print("WARNING could not scale, blind_zjets is empty")

    if not zjets_in_window.Integral() == 0:
        zjets_in_window.Scale(1.0 / zjets_in_window.Integral())
    else:
        print("WARNING could not scale, zjets_in_window is empty")

    if not data_in_window.Integral() == 0:
        data_in_window.Scale(1.0 / data_in_window.Integral())
    else:
        print("WARNING could not scale, zjets_in_window is empty")

    legend = ROOT.TLegend(0.6, 0.65, 0.9, 0.85)
    legend.AddEntry(blind_data, "data outside mbb window", "pe")
    legend.AddEntry(blind_zjets, "zjets outside mbb window", "l")
    legend.AddEntry(data_in_window, "data inside mbb window", "pe")
    legend.AddEntry(zjets_in_window, "zjets inside mbb window", "l")

    data_in_window.SetTitle("")
    data_in_window.GetYaxis().SetTitle("a.u.")
    data_in_window.SetAxisRange(0.0, 1.0, "Y")
    data_in_window.Draw("pe")
    data_in_window.GetXaxis().SetLabelOffset(5)
    blind_data.Draw("same pe")

    zjets_in_window.Draw("same hist")
    blind_zjets.Draw("same hist")

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)

    info = plot.info_string(name)
    t.DrawLatex(0.13, 0.86, "#bf{#it{" + info + "}}")
    t.DrawLatex(0.13, 0.82, "ATLAS #it{#bf{Internal}}")

    legend.Draw()
    middle.cd()
    middle.SetGridx()
    middle.SetGridy()

    ratio_out = blind_data.Clone()
    ratio_out.Divide(blind_zjets)

    ratio_in = data_in_window.Clone()
    ratio_in.Divide(zjets_in_window)

    ratio_out.Draw()
    ratio_out.SetTitle("")
    ratio_out.GetXaxis().SetTitleSize(0.12)
    ratio_out.GetXaxis().SetTitleOffset(5)
    ratio_out.GetXaxis().SetLabelOffset(5)
    ratio_out.GetYaxis().SetTitle("data / MC")
    ratio_out.GetYaxis().SetTitleSize(0.15)
    ratio_out.GetYaxis().SetTitleOffset(0.22)
    ratio_out.SetAxisRange(0.85, 1.15, "Y")
    ratio_out.GetYaxis().SetLabelSize(0.09)
    ratio_out.Draw("same pe")
    ratio_in.Draw("same pe")

    lower.cd()
    lower.SetGridx()
    lower.SetGridy()

    double_ratio = ratio_in.Clone()
    d_baseline = double_ratio.Clone()
    d_baseline.SetLineColor(ROOT.kGreen + 2)
    d_baseline.SetLineWidth(2)
    for i in range(-1, d_baseline.GetNbinsX() + 2):
        d_baseline.SetBinContent(i, 1)
        d_baseline.SetBinError(i, 0)
    d_baseline.SetMarkerStyle(1)
    d_baseline.SetMarkerColor(ROOT.kGreen + 2)
    double_ratio.Divide(ratio_out)
    double_ratio.SetMarkerColor(ROOT.kBlack)
    double_ratio.SetMarkerStyle(1)
    double_ratio.SetLineColor(ROOT.kBlack)
    double_ratio.SetLineWidth(2)
    double_ratio.Draw()
    d_baseline.Draw("L same")
    double_ratio.Draw("L E2 same")
    double_ratio.SetFillStyle(3001)
    double_ratio.SetFillColor(ROOT.kBlack)
    double_ratio.SetLineStyle(1)
    double_ratio.SetAxisRange(0.85, 1.15, "Y")
    double_ratio.GetXaxis().SetTitle("#bf{pTV (GeV)}")
    double_ratio.GetXaxis().SetTitleSize(0.12)
    double_ratio.GetXaxis().SetTitleOffset(0.70)
    double_ratio.GetXaxis().SetLabelOffset(0.01)
    double_ratio.GetXaxis().SetLabelSize(0.09)
    double_ratio.GetYaxis().SetLabelSize(0.09)
    double_ratio.GetYaxis().SetTitle("double ratio")
    double_ratio.GetYaxis().SetTitleSize(0.15)
    double_ratio.GetYaxis().SetTitleOffset(0.22)
    canvas.Update()
    canvas.Draw()
    canvas.SaveAs(
        "plots/{}/data_vs_mc_in_vs_out_two_plus_jet_inclusive.pdf".format(
            args.tree_type
        )
    )

    middle.SetLogx()
    upper.SetLogx()
    lower.SetLogx()
    canvas.Update()
    canvas.Draw()
    canvas.SaveAs(
        "plots/{}/data_vs_mc_in_vs_out_two_plus_jet_inclusive_logx.pdf".format(
            args.tree_type
        )
    )
    return


def plot_blind_vs_unblind(variables, args):
    """
    Plot ptv spectrum of blinded events and unblinded events.
    To see if the ptv shape is effected by removing the signal window
    """
    sys.argv.append("-b")
    import ROOT

    unblind_file = ROOT.TFile(
        "histograms/{}/{}/two_plus_jet_inclusive_unblind/pTV.root".format(
            args.channel, args.tree_type
        )
    )
    blind_file = ROOT.TFile(
        "histograms/{}/{}/two_plus_jet_inclusive_blind/pTV.root".format(
            args.channel, args.tree_type
        )
    )

    zjets_unblind = unblind_file.Get("Zjets-pTV")
    blind_zjets = blind_file.Get("Zjets-pTV")
    options = variables["pTV"]
    bins = options[5]
    n_bins = len(bins) - 1
    bins = array("d", bins)
    canvas = ROOT.TCanvas("ptv", "ptv", 900, 900)

    upper, lower = plot.vertical_2_pad(0.005, 0.001, 0.25, grid=True)
    lower.cd().SetBottomMargin(0.25)
    upper.cd()

    blind_zjets = plot.rebin(blind_zjets, n_bins, bins, variable=True)
    zjets_unblind = plot.rebin(zjets_unblind, n_bins, bins, variable=True)

    blind_zjets.SetMarkerColor(ROOT.kRed)
    blind_zjets.SetMarkerStyle(8)
    blind_zjets.SetLineColor(ROOT.kRed)
    blind_zjets.SetLineWidth(1)

    zjets_unblind.SetMarkerColor(ROOT.kBlue)
    zjets_unblind.SetMarkerStyle(8)
    zjets_unblind.SetLineColor(ROOT.kBlue)
    zjets_unblind.SetLineWidth(1)
    zjets_unblind.GetXaxis().SetLabelOffset(5)

    if not blind_zjets.Integral() == 0:
        blind_zjets.Scale(zjets_unblind.Integral() / blind_zjets.Integral())
    else:
        print("WARNING could not scale, blind_zjets is empty")

    legend = ROOT.TLegend(0.6, 0.65, 0.9, 0.85)
    legend.AddEntry(blind_zjets, "zjets no mbb cut", "l")
    legend.AddEntry(zjets_unblind, "zjets with mbb cut", "l")

    zjets_unblind.SetTitle("")
    zjets_unblind.Draw()
    zjets_unblind.GetXaxis().SetLabelOffset(5)

    blind_zjets.Draw("same hist")

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)

    info = plot.info_string("two_plus_jets_inclusive")
    t.DrawLatex(0.13, 0.86, "#bf{#it{" + info + "}}")
    t.DrawLatex(0.13, 0.82, "ATLAS #it{#bf{Internal}}")

    legend.Draw()
    lower.cd()
    lower.SetGridx()
    lower.SetGridy()

    ratio_out = blind_zjets.Clone()
    ratio_out.Divide(zjets_unblind)

    ratio_out.Draw()
    ratio_out.SetTitle("")
    ratio_out.GetXaxis().SetTitle("#bf{pTV (Gev)}")
    ratio_out.GetXaxis().SetTitleSize(0.12)
    ratio_out.GetXaxis().SetTitleOffset(0.9)
    ratio_out.GetXaxis().SetLabelOffset(0.01)

    ratio_out.SetAxisRange(0.75, 1.25, "Y")
    ratio_out.GetXaxis().SetLabelSize(0.08)
    ratio_out.GetYaxis().SetLabelSize(0.08)
    d_baseline = ratio_out.Clone()
    d_baseline.SetLineColor(ROOT.kGreen + 2)
    d_baseline.SetLineWidth(2)
    for i in range(d_baseline.GetNbinsX() + 1):
        d_baseline.SetBinContent(i, 1)

    ratio_out.Draw("same")
    d_baseline.Draw("hist same")

    lower.SetLogx()
    upper.SetLogx()
    canvas.Update()
    canvas.Draw()
    canvas.SaveAs(
        "plots/{}/blind_vs_unblind_two_plus_jet_inclusive_logx.pdf".format(
            args.tree_type
        )
    )

    unblind_file.Close()
    blind_file.Close()

    return


def two_lep_checks(processes, variables, args):
    """
    Prepare the arguments for and run all the two lepton specific checks
    """
    two_plus_jet_inclusive_inside_sorted_nosig = hlp.sort_histograms(
        "two_plus_jet_inclusive_unblind_inside",
        processes,
        variables,
        args,
        handle_signal=True,
    )

    two_plus_jet_inclusive_inside_data_mc_tuples_nosig = hlp.subtract_background(
        two_plus_jet_inclusive_inside_sorted_nosig
    )

    two_plus_jet_inclusive_sorted_blind_nosig = hlp.sort_histograms(
        "two_plus_jet_inclusive_blind", processes, variables, args, handle_signal=True
    )

    two_plus_jet_inclusive_data_mc_tuples_blind_nosig = hlp.subtract_background(
        two_plus_jet_inclusive_sorted_blind_nosig
    )

    plot_in_vs_out(
        two_plus_jet_inclusive_inside_data_mc_tuples_nosig,
        two_plus_jet_inclusive_data_mc_tuples_blind_nosig,
        args,
    )

    plot_blind_vs_unblind(variables, args)
