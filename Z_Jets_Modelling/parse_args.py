import argparse


def define_args():
    """
    Defines arguments with argparse.
    Returns 'parser' namespace object.
    See README.md for more
    """
    parser = argparse.ArgumentParser(
        description="Makes data vs MC plots out of CxAODReader" "output"
    )
    parser.add_argument(
        "sample_dirs", type=str, help="paths to where samples are", default=""
    )
    parser.add_argument(
        "--channel", type=str, help="lepton channel we are studying", default="2lep"
    )
    parser.add_argument(
        "--tree_type",
        type=str,
        help="easy or mva, CxAODReader tree type",
        default="mva",
    )
    parser.add_argument(
        "--sample_list",
        type=str,
        default="default.txt",
        help="name of text file in samples/ that defines sample types",
    )
    parser.add_argument(
        "--variable_list",
        type=str,
        default="data-driven.txt",
        help="name of text file in variables/ that defines variables/options",
    )
    parser.add_argument("--force_histograms", type=bool, default=False)
    args = parser.parse_args()

    if args.channel == "0lep" and args.sample_list == "default.txt":
        args.sample_list = "default-0lep.txt"
    if args.channel=='0lep' and args.variable_list=='default.txt':
        args.variable_list='default-0lep.txt'

    # Ensure directories given end in a single forward slash
    args.sample_dirs = args.sample_dirs.rstrip("/") + "/"

    print("Running with options:")
    for arg in vars(args):
        print("{}: {}".format(arg, getattr(args, arg)))

    return args
