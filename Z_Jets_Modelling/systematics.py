"""
Two lepton specific code
"""
import sys_helpers as hlp
import plotting as plt
import cuts
import parse_args
import sample_handler
import variable_handler

import sys


def do_systematics(args, recipe):
    """
    The top level function for running z+jets systematics

    - Loads mva / easy trees into root dataframes and sorts by process
    - Makes histograms from the trees for a number of variables
    - Sorts histograms and does appropriate background subtractions
    - Makes plots of data vs MC
    - Fits ratios
    - Performs specific checks that came from discussions with VHbb group
    """
    sys.argv.append("-b")
    import ROOT  # import ROOT after args are parsed
    
    # Get the sample types and their corresponding samples
    samples = sample_handler.samples_by_type(args)

    RDF = ROOT.ROOT.RDataFrame

    # Make nominal TChains for each sample type
    chains_by_type = {_type: ROOT.TChain("Nominal") for _type in samples.keys()}

    #Adds each sample within a sample type together
    for _type, samples in samples.iteritems():
        dirs = sample_handler.sample_dirs(args)
        for sample in samples:
            for d in dirs:
                chains_by_type[_type].Add(d + sample)
    
    #Converts the TChains into RDataFrames
    dataframes_by_type = {
        _type: RDF(chain) for _type, chain in chains_by_type.iteritems()
    }

    
    dataframes = hlp.process_dataframes(args, dataframes_by_type)

    processes = dataframes["inclusive_unblind"].keys()
    # get the variables you want to run over and their plotting options
    variables = variable_handler.get_variables(args)
    
    hlp.make_all_n_jet_histograms(args, dataframes, 2, variables)
    hlp.make_all_n_jet_histograms(args, dataframes, 2, variables, plus=True)
    hlp.make_all_n_jet_histograms(args, dataframes, 3, variables)
    hlp.make_all_n_jet_histograms(args, dataframes, 3, variables, plus=True)
    if args.channel == '0lep':
        hlp.make_all_n_jet_histograms(args, dataframes, 2.5, variables)

    data_mc_for_stack_by_var = hlp.prepare_for_stack(
        "two_plus_jet_inclusive_blind", args, processes, variables
    )
    for v_name, data_mc in data_mc_for_stack_by_var.iteritems():
        plt.plot_stack(
            "two_plus_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args
        )
        plt.plot_stack(
            "two_plus_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args, normalise=False
        )
    data_mc_for_stack_by_var = hlp.prepare_for_stack(
        "three_jet_inclusive_blind", args, processes, variables
    )
    for v_name, data_mc in data_mc_for_stack_by_var.iteritems():
        plt.plot_stack(
            "three_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args
        )
        plt.plot_stack(
            "three_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args, normalise=False
        )
    data_mc_for_stack_by_var = hlp.prepare_for_stack(
        "two_jet_inclusive_blind", args, processes, variables
    )
    for v_name, data_mc in data_mc_for_stack_by_var.iteritems():
        plt.plot_stack(
            "two_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args
        )
        plt.plot_stack(
            "two_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args, normalise=False
        )

    if args.channel == '0lep':
        data_mc_for_stack_by_var = hlp.prepare_for_stack(
            "two_plus_three_jet_inclusive_blind", args, processes, variables
            )
        for v_name, data_mc in data_mc_for_stack_by_var.iteritems():
            plt.plot_stack(
                "two_plus_three_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args
                )
            plt.plot_stack(
                "two_plus_three_jet_inclusive_blind", data_mc[0], data_mc[1], v_name, args, normalise=False
                )

    hlp.run_recipes(args, recipe, processes, variables)

    #hlp.run_checks(args, processes, variables)
