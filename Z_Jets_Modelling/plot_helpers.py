import math
import sys
import ROOT
import sys
from array import array


def vertical_2_pad(margin, separation, up_start, grid=False):
    """
    Creates two ROOT.TPad's one above the other.
    TPad's are defined by two points in x, y that specify
    the bottom left and top right corners.
    """
    blx = 0 + margin
    low_bly = 0 + margin

    trx = 1 - margin
    up_try = 1 - margin

    up_bly = up_start + (separation / 2)
    low_try = up_start - (separation / 2)

    upper = ROOT.TPad("upper", "upper", blx, up_bly, trx, up_try)
    lower = ROOT.TPad("lower", "lower", blx, low_bly, trx, low_try)

    upper.Draw()
    lower.Draw()

    if grid:
        upper.SetGridx()
        upper.SetGridy()
        lower.SetGridx()
        lower.SetGridy()

    ROOT.gStyle.SetOptStat(0)

    return upper, lower


def scale_histograms(histograms, target_y):
    """
    Scales all histograms so that the maximum y_value displayed is target_y.
    """
    # Get maximum y values of all histograms
    maximum_values = tuple(h.GetMaximum() for h in histograms)
    # logging.debug(format_iter(maximum_values))

    # Get max of maximum y values
    y_max = max(maximum_values)
    # Get index of y_max
    max_idx = maximum_values.index(y_max)

    if y_max == 0:
        logging.warning("Could not scale histograms")
        return 1

    histograms[max_idx].Scale(target_y / y_max)
    h_max_area = histograms[max_idx].Integral()

    for i in range(len(histograms)):
        # if i == max_idx:
        #    continue
        this_area = histograms[i].Integral()
        if this_area == 0:
            # logging.warning('Could not scale histogram {}'.format(histograms[i].GetName()))
            continue
        histograms[i].Scale(h_max_area / this_area)

    maximum_values = tuple(h.GetMaximum() for h in histograms)
    areas = tuple(h.Integral() for h in histograms)
    # logging.debug(format_iter(maximum_values))
    # logging.debug(format_iter(areas))

    if max(maximum_values) != target_y:
        scale_histograms(histograms, target_y)

    return 0


def chi2(h1, h2, options):
    """
    returns, chi2, ndf, pvalue of h1 and h2
    """
    chi_2 = ROOT.Double(0)
    ndf = ctypes.c_int(0)
    igood = ctypes.c_int(0)

    p_value = h1.Chi2TestX(h2, chi_2, ndf, igood, options)

    return float(chi_2), ndf.value, p_value


def remove_duplicates(seq):
    """
    remove duplicates from sequence and return list
    """
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def get_variable_bins(
    histogram,
    percentile,
    min_bins=15,
    x_max=None,
    x_min=None,
    blind_max=None,
    blind_min=None,
):
    """
    Merges bins with less then percentile of events (weighted)
    Merges bins such that the sum in quadrature of the error of the old bins divided by the total content of those old bins is less than some threshold
    Supports blinding a single window between 
    """
    if not x_max:
        last_width = histogram.GetBinWidth(histogram.FindLastBinAbove(0))
        x_max = histogram.GetBinLowEdge(histogram.FindLastBinAbove(0)) + last_width

    if not x_min:
        x_min = histogram.GetBinLowEdge(histogram.FindFirstBinAbove(0))

    n_events = histogram.Integral()

    threshold = float(percentile) / 100.0
    new_bins = []

    running_total = 0.0
    running_error = []
    n_bins = histogram.GetNbinsX()
    blind_high = True
    blind_low = True
    in_window = False

    if blind_min and blind_low and blind_high and blind_max:
        print("blinding turned on")

    for i in range(histogram.GetNbinsX(), 0, -1):
        low_edge = histogram.GetBinLowEdge(i)
        width = histogram.GetBinWidth(i)
        high_edge = low_edge + width
        content = histogram.GetBinContent(i)
        error = histogram.GetBinError(i)

        if blind_min and blind_max:
            if high_edge <= blind_max and low_edge >= blind_min:
                in_window = True
            elif low_edge <= blind_min:
                in_window = False

        if blind_max and blind_high:
            if high_edge <= blind_max:
                new_bins.append(float(blind_max))
                blind_high = False
                running_total = 0.0
                running_error = []

                in_window = True
                continue

        if in_window:
            running_total = 0.0
            running_error = []
            continue

        if blind_min and blind_low:
            if low_edge <= blind_min:
                new_bins.append(float(blind_min))
                running_total = 0.0
                running_error = []

                blind_low = False
                continue

        if high_edge > x_max:
            continue
        elif high_edge == x_max:
            new_bins.append(high_edge)
            continue

        if low_edge <= x_min:
            new_bins.append(low_edge)
            break

        running_total += content
        running_error.append(error)
        summed_error = math.sqrt(sum(e * e for e in running_error))

        if running_total <= 0.0:
            relative_error = threshold + 1
        else:
            relative_error = summed_error / running_total

        if relative_error > threshold:
            continue
        else:
            new_bins.append(low_edge)
            running_total = 0.0
            running_error = []

    n_bins = len(new_bins) - 1
    if n_bins >= min_bins:
        new_bins.reverse()

        bins_vector = array("d", new_bins)

        return n_bins, bins_vector

    elif percentile >= 100:
        return -1, [-1]

    new_percentile = percentile * 1.1
    print(
        "binning with error < {0} error failed\n"
        "only {1} bins\n"
        "trying with error < {2}".format(percentile, n_bins, new_percentile)
    )
    percentile *= 1.1
    return get_variable_bins(
        histogram,
        percentile,
        min_bins=min_bins,
        blind_max=blind_max,
        blind_min=blind_min,
    )


def info_string(name):
    """
    Builds a string that contains information about a selection inferred from a name
    """
    info_string = "140 fb^{-1} Zjets CR "
    if "two" in name:
        info_string += "2"
    elif "three" in name:
        info_string += "3"
    if "plus" in name:
        info_string += "+"
    info_string += " jets "
    if "inclusive" in name:
        info_string += " pTV > 75 GeV "
    elif "low" in name:
        info_string += " 75 < pTV < 150 GeV "
    elif "med" in name:
        info_string += " 150 < pTV < 250 GeV "
    elif "high" in name:
        info_string += " pTV > 250 GeV "
    if not "unblind" in name:
        info_string += " 110 < mBB < 140 GeV events removed"
    if "inside" in name:
        info_string += " inside signal window"

    return info_string


def info_string_0lep(name):
    """
    Builds a string that contains information about a selection inferred from a name
    """
    info_string = "140 fb^{-1} Zjets CR "
    if "two" in name:
        info_string += "2"
    elif "three" in name:
        info_string += "3"
    if "plus" in name:
        info_string += "+"
    info_string += " jets "
    if "inclusive" in name:
        info_string += " MET > 150 GeV "
    elif "med" in name:
        info_string += " 150 < MET < 250 GeV "
    elif "high" in name:
        info_string += " MET > 250 GeV "
    if not "unblind" in name:
        info_string += " 110 < mBB < 140 GeV events removed"
    if "inside" in name:
        info_string += " inside signal window"

    return info_string


def get_colour(process):
    """
    Gets the ROOT colour to use for certain processes
    """
    if process == "stop":
        colour = ROOT.kOrange - 1

    if process == "ttbar":
        colour = ROOT.kOrange

    if process == "Zjets":
        colour = ROOT.kAzure - 1

    if process == "Wjets":
        colour = ROOT.kGreen + 3

    if process == "diboson":
        colour = ROOT.kGray + 1

    if process == "ZHbb":
        colour = ROOT.kRed

    if process == "WHbb":
        colour = ROOT.kRed - 2

    return colour


def set_all_colours(histogram, colour=None, process=None):
    """
    Sets the fill, line and marker colour of a histogram.
    If no colour provided then a process must be provided and a colour will be picked for you.
    """
    if not process and not colour:
        print("Setting colour failed, please choose a known process or colour.")
    elif process:
        histogram.SetFillColor(get_colour(process))
        histogram.SetLineColor(get_colour(process))
        histogram.SetMarkerColor(get_colour(process))
    elif colour:
        histogram.SetFillColor(colour)
        histogram.SetLineColor(colour)
        histogram.SetMarkerColor(colour)
    return


def rebin(histogram, n_bins=None, bins=None, x_range=False, variable=False):
    """
    Re-bins ROOT histograms nicely.
    """
    if x_range and not variable:
        current_bin_width = histogram.GetBinWidth(0)
        desired_bin_width = x_range / nBins

        bin_divisor = desired_bin_width / current_bin_width
        if not bin_divisor < 1:
            histogram = histogram.Rebin(int(bin_divisor))

    elif variable:
        if not n_bins and not bins:
            n_bins, bins = get_variable_bins(histogram)

        histogram = histogram.Rebin(n_bins, "rebinned " + histogram.GetName(), bins)

        for j in range(1, histogram.GetNbinsX() + 1):
            if not histogram.GetBinWidth(j) == 0.0:
                histogram.SetBinContent(
                    j, histogram.GetBinContent(j) / histogram.GetBinWidth(j)
                )
                histogram.SetBinError(
                    j, histogram.GetBinError(j) / histogram.GetBinWidth(j)
                )

    else:
        print("invalid options, histogram not rebinned")

    return histogram


def data_points_style(data, legend=None):
    """
    Applies the style we use to plot data points to a histogram of the data
    """
    # Markers
    data.SetMarkerColor(ROOT.kBlack)
    data.SetMarkerStyle(8)

    # Lines (error bars)
    data.SetLineColor(ROOT.kBlack)
    data.SetLineWidth(1)

    if legend:
        legend.AddEntry(data, "data", "pe")

def get_2lep_fits():
    """
    Get the parameters of the fits in the 2 lepton channel. order is [c,u(c),m,u(m)]
    """
    fits_by_region = {}
    with open("fits/" + "fits_drbb.txt", "r") as _file:
        for line in _file:
            if '#' in line:
                continue
            line_list = line.split(",")
            if len(line_list) == 5:
                fit_pars = [float(line_list[1]), float(line_list[2]),\
                            float(line_list[3]), float(line_list[4])]
            else:
                fit_pars = [1, 0, 0, 0]
            fits_by_region[line_list[0]] = fit_pars
    return fits_by_region

def get_ichep(v_name, args):
    """
    Get the ichep fit function, with units corrected for mva tree (GeV)
    or easy tree (MeV)
    """
    sys.argv.append("-b")
    import ROOT

    if "pTV" in v_name:
        if args.tree_type == "easy":
            ichep_syst = ROOT.TF1(
                "f_SysZPtV",
                "x>10000 ? [0]*log10(x/50000)-[1]+1 "
                ": [0]*log10(10000/500000)-[1] + 1",
                0,
                500000,
            )
            ichep_syst.SetParameter(0, 0.2)
            ichep_syst.SetParameter(1, 0.07)

            ichep_syst_sym = ROOT.TF1(
                "f_SysZPtV",
                "x>10000 ? -([0]*log10(x/50000)-[1])+1 "
                ": -([0]*log10(10000/50000)-[1])+1",
                0,
                500000,
            )
            ichep_syst_sym.SetParameter(0, 0.2)
            ichep_syst_sym.SetParameter(1, 0.07)
        elif args.tree_type == "mva":
            ichep_syst = ROOT.TF1(
                "f_SysZPtV",
                "x>10 ? [0]*log10(x/50)-[1]+1 " ": [0]*log10(10/500)-[1] + 1",
                0,
                500,
            )
            ichep_syst.SetParameter(0, 0.2)
            ichep_syst.SetParameter(1, 0.07)

            ichep_syst_sym = ROOT.TF1(
                "f_SysZPtV",
                "x>10 ? -([0]*log10(x/50)-[1])+1 " ": -([0]*log10(10/50)-[1])+1",
                0,
                500,
            )
            ichep_syst_sym.SetParameter(0, 0.2)
            ichep_syst_sym.SetParameter(1, 0.07)

    elif "mBB" in v_name:
        if args.tree_type == "easy":
            ichep_syst = ROOT.TF1(
                "f_SysZMbb",
                "x>[2] ? [0] * ([2]/1000. - [1])+1 : " "[0] * (x/1000. - [1])+1",
                0,
                400000,
            )
            ichep_syst.SetParameter(0, 0.0005)
            ichep_syst.SetParameter(1, 100.0)
            ichep_syst.SetParameter(2, 300000)

            ichep_syst_sym = ROOT.TF1(
                "f_SysZMbb",
                "x>[2] ? -([0] * ([2]/1000. - [1]))+1 " ": -([0] * (x/1000. - [1]))+1",
                0,
                400000,
            )
            ichep_syst_sym.SetParameter(0, 0.0005)
            ichep_syst_sym.SetParameter(1, 100.0)
            ichep_syst_sym.SetParameter(2, 300000)
        elif args.tree_type == "mva":
            ichep_syst = ROOT.TF1(
                "f_SysZMbb",
                "x>[2] ? [0] * ([2]/1. - [1])+1 : " "[0] * (x/1. - [1])+1",
                0,
                400,
            )
            ichep_syst.SetParameter(0, 0.0005)
            ichep_syst.SetParameter(1, 100.0)
            ichep_syst.SetParameter(2, 300)

            ichep_syst_sym = ROOT.TF1(
                "f_SysZMbb",
                "x>[2] ? -([0] * ([2]/1. - [1]))+1 " ": -([0] * (x/1. - [1]))+1",
                0,
                400,
            )
            ichep_syst_sym.SetParameter(0, 0.0005)
            ichep_syst_sym.SetParameter(1, 100.0)
            ichep_syst_sym.SetParameter(2, 300)
    else:
        ichep_syst = None
        ichep_syst_sym = None
    return ichep_syst, ichep_syst_sym


def get_fit_func(v_name, args):
    """
    Get the desired form of the fit function
    returns none for variables other than mbb and ptv
    handles units correctly for mva tree and easy tree
    """
    sys.argv.append("-b")
    import ROOT

    if "pTV" in v_name or "MET" in v_name or "METpT" in v_name:
        if args.tree_type == "easy":
            fit = ROOT.TF1(
                "error",
                "x>10000 ? [0]*log10(x/50000)-[1] : [0]*log10(10000/500000)-[1]",
                0,
                1000000,
            )
        elif args.tree_type == "mva":
            fit = ROOT.TF1(
                "error", "x>10 ? [0]*log10(x/50)-[1] : [0]*log10(10/500)-[1]", 0, 1000
            )

    elif "mBB" in v_name:
        if args.tree_type == "easy":
            fit = ROOT.TF1(
                "error", "x>300000 ? ([0]*300000) - [1] : ([0]*x) - [1]", 0, 1000000
            )
        elif args.tree_type == "mva":
            fit = ROOT.TF1("error", "x>300 ? ([0]*300) - [1] : ([0]*x) - [1]", 0, 1000)
    else:
        fit = ROOT.TF1("error", "([0]*x) + [1]", 0, 1000)
    return fit
