import sys
import os
import plot_helpers as plot
from array import array
from decimal import Decimal


def plot_stack(name, data, mc_processes, variable, args, normalise=True):
    """
    Plot a stack of mc_processes against data.
    mc_processes should be a dictionary.
    It should look like {"process" : ROOT.TH1}
    """
    sys.argv.append("-b")
    import ROOT
    
    blind_min = None
    blind_max = None
    if variable == "mBB":
        blind_min = 110 if args.tree_type == "mva" else 110000
        blind_max = 140 if args.tree_type == "mva" else 140000

    n_bins, bins = plot.get_variable_bins(
        data, 3, min_bins=20, x_max=500, blind_min=blind_min, blind_max=blind_max
    )
    if bins == [-1]:
        print("Binnning failed for {}_{}".format(name, variable))
        return
    stack = ROOT.THStack("monte-carlo", "monte-carlo")
    legend = ROOT.TLegend(0.6, 0.55, 0.8, 0.85)

    for process, histogram in mc_processes.iteritems():
        this_hist = histogram.Clone()
        this_hist = plot.rebin(this_hist, n_bins, bins, variable=True)
        plot.set_all_colours(this_hist, process=process)
        this_hist.SetLineWidth(0)
        legend.AddEntry(this_hist, process, "f")
        stack.Add(this_hist)

    sum_hist = stack.GetStack().Last()
    canvas = ROOT.TCanvas(name, name, 900, 900)

    upper, lower = plot.vertical_2_pad(0.005, 0.001, 0.25, grid=True)
    lower.cd().SetBottomMargin(0.25)
    upper.cd()

    data = plot.rebin(data, n_bins, bins, variable=True)

    plot.data_points_style(data, legend)
    y_max = max(sum_hist.GetMaximum(), data.GetMaximum())

    data.GetXaxis().SetLabelOffset(5)

    if not data.Integral() == 0:
        if normalise:
            data.Scale(sum_hist.Integral() / data.Integral())
    else:
        print("WARNING: could not scale, data is empty")

    data.SetTitle("")
    data.Draw("same")
    data.GetYaxis().SetRangeUser(0.0, y_max * 1.5)
    data.GetXaxis().SetLabelOffset(5)
    stack.Draw("hist same")
    data.Draw("same")

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)

    if args.channel == '0lep':
        info = plot.info_string_0lep(name)
    elif args.channel == '2lep':
        info = plot.info_string(name)
    t.DrawLatex(0.13, 0.86, "#bf{#it{" + info + "}}")
    t.DrawLatex(0.13, 0.82, "ATLAS #it{#bf{Internal}}")

    legend.Draw()
    lower.cd()

    ratio = data.Clone()
    ratio.Divide(sum_hist)

    ratio.Draw()
    ratio.SetTitle("")
    ratio.GetXaxis().SetTitle("#bf{" + "{}".format(variable) + "(Gev)}")
    ratio.GetXaxis().SetTitleSize(0.12)
    ratio.GetXaxis().SetTitleOffset(0.9)
    ratio.GetXaxis().SetLabelOffset(0.01)
    ratio.SetAxisRange(0.75, 1.25, "Y")
    ratio.GetXaxis().SetLabelSize(0.08)
    ratio.GetYaxis().SetLabelSize(0.08)
    baseline = ROOT.TH1F("baseline", "baseline", 100, 0.0, 1000.0)
    for i in range(1, baseline.GetNbinsX() + 1):
        baseline.SetBinContent(i, 1.0)
    baseline.SetLineStyle(1)
    baseline.SetLineWidth(1)
    baseline.SetLineColor(ROOT.kRed)
    baseline.SetMarkerSize(0)
    baseline.Draw("same l")

    fold=("stacks_" if normalise else "stacks_NotNormalised_")+name
    plot_dir = "plots/{0}/{1}/{2}".format(args.channel, args.tree_type, fold)
    if not os.path.isdir(plot_dir):
        os.makedirs(plot_dir)

    canvas.Update()
    canvas.Draw()
    canvas.SaveAs(
        "plots/{0}/{1}/{2}/{3}_full_stack_{4}.pdf".format(args.channel, args.tree_type, fold, name, variable)
    )

    if "pTV" in variable or variable=="MET":
        lower.SetLogx()
        upper.SetLogx()

        canvas.SaveAs(
            "plots/{0}/{1}/{2}/{3}_full_stack_{4}_logx.pdf".format(args.channel, args.tree_type, fold, name, variable)
        )

    # lower.SetLogx(0)
    # upper.SetLogx(0)

    # upper.cd()

    # data.SetAxisRange(75000, 150000, "X")
    # stack.GetXaxis().SetRangeUser(75000, 150000)
    # lower.cd()
    # ratio.SetAxisRange(75000, 150000, "X")
    # baseline.SetAxisRange(75000, 150000, "X")

    # canvas.SaveAs("plots/data_vs_MC_two_jet_inclusive_pTV_zoomed_low.pdf")

    # ptv_file.Close()

    return

 
def plot_histograms(
    name,
    data_mc_tuples_by_variable,
    variables,
    args,
    nosig=False,
    top_dd=False,
    do_fit=False,
    draw_ichep=True,
):
    """
    Plots histograms from data_mc_tuples
    Tuples are organised in a dictionary {'variable' : tuple}
    First element in tuple is MC
    Second element is data
    Legend assumes that montecarlo is z+jets

    Optional arguments:
    nosig: changes legend and filename to indicate signal MC has been subtracted from data
    top_dd: changes legend and filename to indicate top data driven subtraction has been performed
    do_fit: perform a fit to the ratio data/MC, fit functions only implemented for mbb and ptv
    draw_ichep: draws the ichep systematic in the ratio
    """
    sys.argv.append("-b")
    import ROOT
    fits_2lep={}
    varia = ['pTV','mBB','pTBB','dRBB','METpT']
    #for v_name, tup in data_mc_tuples_by_variable.iteritems():
    for v_name in varia:
        tup = data_mc_tuples_by_variable[v_name]
        if not (v_name=='pTV' or v_name=='mBB' or v_name=='pTBB' or v_name=='dRBB' or v_name=='METpT'):
            continue
        print("PLOTTING: {}, {}".format(name, v_name))
        options = variables[v_name]
        canvas = ROOT.TCanvas(v_name, v_name, 900, 900)
        upper, lower = plot.vertical_2_pad(0.01, 0.00001, 0.5, grid=True)
        lower.cd().SetBottomMargin(0.25)
        lower.SetTopMargin(0.01)
        upper.cd()
        upper.SetBottomMargin(0.01)

        legend = ROOT.TLegend(0.7, 0.55, 0.9, 0.8)
        ROOT.gStyle.SetOptStat(0)
        zjets_hist = tup[0]
        data_hist = tup[1]

        data_hist.Draw()
        zjets_hist.Draw("same")

        x_min = options[1]
        x_max = options[2]
        fit_min = options[3]
        fit_max = options[4]
        bins = array("d", options[5])
        n_bins = len(options[5]) - 1

        data_hist = plot.rebin(data_hist, n_bins, bins, variable=True)
        zjets_hist = plot.rebin(zjets_hist, n_bins, bins, variable=True)

        zjets_hist.Draw("hist")
        zjets_hist.SetTitle("")
        zjets_hist.SetFillStyle(1001)
        zjets_hist.SetFillColor(ROOT.kBlue + 2)
        zjets_hist.SetLabelOffset(5)

        data_hist.Draw("p same")
        if not zjets_hist.Integral() == 0:
            zjets_hist.Scale(data_hist.Integral() / zjets_hist.Integral())
        else:
            print("NAME: {} VARIABLE".format(name, v_name))
            print("WARNING could not scale, zjets_hist is empty")

        y_max = max(zjets_hist.GetMaximum(), data_hist.GetMaximum())
        zjets_hist.SetAxisRange(0.0, y_max * 1.5, "Y")
        data_hist.SetMarkerColor(ROOT.kOrange + 7)
        data_hist.SetMarkerStyle(8)
        data_hist.SetLineColor(ROOT.kOrange + 7)
        data_hist.SetLineWidth(1)

        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.03)
        t.SetTextAlign(4)

        info = plot.info_string(name)

        upper.cd()
        t.DrawLatex(0.13, 0.86, "#bf{#it{" + info + "}}")
        t.DrawLatex(0.13, 0.82, "ATLAS #it{#bf{Internal}}")

        legend.AddEntry(zjets_hist, "Z + jets", "f")

        if nosig:
            legend.AddEntry(data_hist, "data - (non zjets bkg + sig)", "pe")
        elif top_dd:
            legend.AddEntry(
                data_hist,
                "data - (topemuCR * SF + non (zjets, ttbar, stop) backgrounds)",
            )
        else:
            legend.AddEntry(data_hist, "data - non zjets bkg", "pe")
        

        lower.cd()

        ratio = data_hist.Clone()
        ratio.Divide(zjets_hist)
        ratio.SetTitle("")

        def closest_in_list(_list, val):
            return min(_list, key=lambda x: abs(x - val))

        fit = plot.get_fit_func(v_name, args)
        
        if fit and do_fit:
            ratio.Fit(fit, "QIMSN", "", fit_min, fit_max)
            if fit.GetNDF() != 0:
                fit_pars=[fit.GetParameter(0),fit.GetParError(0),
                          fit.GetParameter(1),fit.GetParError(1)]
                if "mBB" in v_name:
                    fit_func = "({0:.2E} +/- {1:.2E}) x - ({2:.2E} +/- {3:.2E})".format(
                        Decimal(str(fit.GetParameter(0))),
                        Decimal(str(fit.GetParError(0))),
                        Decimal(str(fit.GetParameter(1))),
                        Decimal(str(fit.GetParError(1))),
                    )
                elif "pTV" in v_name or "METpT" in v_name:
                    fit_func = "({0:.2E} +/- {1:.2E}) log10(x/50) - ({2:.2E} +/- {3:.2E})".format(
                        Decimal(str(fit.GetParameter(0))),
                        Decimal(str(fit.GetParError(0))),
                        Decimal(str(fit.GetParameter(1))),
                        Decimal(str(fit.GetParError(1))),
                    )
                elif "pTBB" in v_name or "dRBB" in v_name:
                    fit_func = "({0:.2E} +/- {1:.2E}) x - ({2:.2E} +/- {3:.2E})".format(
                        Decimal(str(fit.GetParameter(0))),
                        Decimal(str(fit.GetParError(0))),
                        Decimal(str(fit.GetParameter(1))),
                        Decimal(str(fit.GetParError(1))),
                    )
                upper.cd()
                t.DrawLatex(0.13, 0.74, "#bf{#it{" + fit_func + "}}")
                t.DrawLatex(
                    0.13,
                    0.78,
                    "#bf{#it{"
                    + "chisquare {0:.2f} / ndf {1} = {2:.2f}".format(
                        fit.GetChisquare(),
                        fit.GetNDF(),
                        fit.GetChisquare() / fit.GetNDF(),
                    )
                    + "}}",
                )
                lower.cd()
            else:
                print("can't plot chi2")
        ratio.Draw()
        if fit and do_fit:
            conf = ROOT.TH1F("conf int", "", n_bins, bins)
            ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(conf)
            my_red = ROOT.TColor().GetColorTransparent(ROOT.kOrange + 7, 0.3)
            conf.SetFillColor(my_red)
            conf.Draw("e3 same")

        canvas.Update()
        ichep_syst, ichep_syst_sym = plot.get_ichep(v_name, args)
        if draw_ichep and ichep_syst and ichep_syst_sym:
            ichep_syst.SetLineColor(ROOT.kGreen + 2)
            # ichep_syst.SetLineStyle(2)
            ichep_syst.SetLineWidth(2)
            ichep_syst.Draw("same l")
            ichep_syst_sym.SetLineColor(ROOT.kGreen + 2)
            ichep_syst_sym.SetLineStyle(2)
            ichep_syst_sym.SetLineWidth(2)
            ichep_syst_sym.Draw("same l")

        ratio.GetXaxis().SetTitle("#bf{" + v_name + " (Gev)}")
        ratio.GetXaxis().SetTitleSize(0.05)
        ratio.GetXaxis().SetTitleOffset(0.9)
        ratio.SetMarkerStyle(8)
        ratio.SetMarkerColor(ROOT.kBlack)
        ratio.SetLineColor(ROOT.kBlack)
        ratio.SetAxisRange(0.75, 1.25, "Y")
        ratio.GetXaxis().SetLabelSize(0.04)
        ratio.GetYaxis().SetLabelSize(0.04)
        ratio.GetYaxis().SetTitle("#bf{Data/MC}")
        ratio.GetYaxis().SetTitleSize(0.05)
        ratio.GetYaxis().SetTitleOffset(0.7)

        baseline = zjets_hist.Clone()
        #baseline.Divide(baseline)

        for j in range(baseline.GetNbinsX()):
            i=j+1
            if v_name=='pTV' and i==baseline.GetNbinsX():
                print("Number bins = {}".format(baseline.GetNbinsX()))
                print('{} {}'.format(baseline.GetBinContent(i),baseline.GetBinCenter(i)))
            if baseline.GetBinContent(i) != 0:
                baseline.SetBinError(i, baseline.GetBinError(i) / baseline.GetBinContent(i))
                baseline.SetBinContent(i, 1.0)


        baseline.Draw("same L E2")
        ratio.Draw("same")
        baseline.SetFillColor(ROOT.kAzure - 4)
        baseline.SetFillStyle(3344)
        baseline.SetLineStyle(1)
        baseline.SetLineColor(ROOT.kAzure - 4)
        baseline.SetLineWidth(3)
        baseline.SetMarkerStyle(1)
        baseline.SetMarkerSize(0)

        if do_fit and fit:
            fit.SetLineColor(ROOT.kOrange + 7)
            fit.SetFillColor(ROOT.kOrange + 7)
            fit.Draw("SAME l")

        if v_name=='METpT':
            print('LOOKING at metpt')
            for val in fits_2lep:
                print(val) 

        if v_name == 'METpT' and (name + "_pTV") in fits_2lep:
            print("DOING the METpT fits")
            fit_mp = plot.get_fit_func(v_name, args)
            fit_mp_sym = plot.get_fit_func(v_name, args)
            
            fit_pars_mp=fits_2lep[name + "_pTV"]
            print(fit_pars_mp)
            fit_mp.SetParameter(0, fit_pars_mp[0])
            fit_mp.SetParameter(1, fit_pars_mp[2])
            fit_mp_sym.SetParameter(0, -fit_pars_mp[0])
            fit_mp_sym.SetParameter(1, 2 - fit_pars_mp[2])
            
            fit_mp.SetLineColor(ROOT.kRed)
            fit_mp.SetLineWidth(2)
            fit_mp_sym.SetLineColor(ROOT.kRed)
            fit_mp_sym.SetLineStyle(2)
            fit_mp_sym.SetLineWidth(2)

            fit_mp.Draw("same l")
            fit_mp_sym.Draw("same l")

            legend.AddEntry(fit_mp,'pTV fit','l')

        legend.AddEntry(fit,'2lep fit','l')
        if ichep_syst:
            legend.AddEntry(ichep_syst,'ichep fit','l')
        legend.AddEntry(baseline,'Z+jets rel. err.','f')
        upper.cd()
        legend.Draw()

        canvas.Update()
        canvas.Draw()

        # finished with name, adding nosig if necessary
        if nosig:
            name = name + "_nosig"
        if 'mBB' in v_name or 'pTV' in v_name or 'pTBB' in v_name or 'dRBB' in v_name or 'METpT' in v_name:
            print("Saving {} fit".format(v_name))
            fits_2lep[name+'_'+v_name]=fit_pars
        plot_dir = "plots/{0}/{1}/{2}".format(args.channel,args.tree_type, name)
        if not os.path.isdir(plot_dir):
            os.makedirs(plot_dir)
        canvas.SaveAs("plots/{0}/{1}/{2}/{2}_{3}.pdf".format(args.channel,args.tree_type, name, v_name))

        if "pTV" in v_name:
            lower.SetLogx()
            upper.SetLogx()
            canvas.SaveAs("plots/{0}/{1}/{2}/{2}_pTV_logx.pdf".format(args.channel,args.tree_type, name))
    return fits_2lep

        # if "pTV" in v_name and "inclusive" in name:
        #     lower.SetLogx(0)
        #     upper.SetLogx(0)
        #     upper.cd()
        #     data_hist.SetAxisRange(75000, 150000, "X")
        #     zjets_hist.SetAxisRange(75000, 150000, "X")
        #     lower.cd()
        #     ratio.SetAxisRange(75000, 150000, "X")
        #     baseline.SetAxisRange(75000, 150000, "X")
        #     fit.GetXaxis().SetRangeUser(75000, 150000)
        #     canvas.SaveAs("plots/{0}/{0}_{1}_zoomed_low.pdf".format(name, v_name))

def plot_histos_0lep(name, data_mc_tuples_by_variable, variables, args, doMGvSh, nosig=False, do_fit=True):
    """
    Plots histograms from data_mc_tuples
    Tuples are organised in a dictionary {'variable' : tuple}
    First element in tuple is MC
    Second element is data
    Legend assumes that montecarlo is z+jets

    Optional arguments:
    nosig: changes legend and filename to indicate signal MC has been subtracted from data
    do_fit: perform a fit to the ratio data/MC, fit functions only implemented for mbb and ptv
    doMGvSh: Does a comparison between Z+jets for Sherpa_221 and MadGraph_Pythia 8
    """
    sys.argv.append("-b")
    import ROOT
    fits_2lep=plot.get_2lep_fits()
    for v_name, tup in data_mc_tuples_by_variable.iteritems():
        if not (v_name=='MET' or v_name=='mBB' or v_name=='pTBB' or v_name=='dRBB'):
            continue
        print("PLOTTING: {}, {}, doMGvSh={}".format(name, v_name, doMGvSh))
        options = variables[v_name]
        canvas = ROOT.TCanvas(v_name, v_name, 900, 900)
        upper, lower = plot.vertical_2_pad(0.01, 0.00001, 0.5, grid=True)
        lower.cd().SetBottomMargin(0.25)
        lower.SetTopMargin(0.01)
        upper.cd()
        upper.SetBottomMargin(0.01)
        legend = ROOT.TLegend(0.7, 0.55, 0.9, 0.8)
        ROOT.gStyle.SetOptStat(0)
        zjets_hist = tup[0]
        data_hist = tup[1]
        '''
        vals_errs={}
        f_bin=data_hist.GetXaxis().FindBin(150.5)
        v_e=[[],[]]
        for i in range(10):
            j=i+f_bin
            v_e[0].append(data_hist.GetBinContent(j))
            v_e[1].append(data_hist.GetBinError(j))
        vals_errs["data"]=v_e
        f_bin=zjets_hist.GetXaxis().FindBin(150.5)
        v_e=[[],[]]
        for i in range(10):
            j=i+f_bin
            v_e[0].append(zjets_hist.GetBinContent(j))
            v_e[1].append(zjets_hist.GetBinError(j))
        vals_errs["Zjets"]=v_e
        fd_val=fd_err=fz_val=fz_err=0
        for i in range(10):
            fd_val+=vals_errs["data"][0][i]
            fd_err=ROOT.TMath.Sqrt(fd_err**2+vals_errs["data"][1][i]**2)
            fz_val+=vals_errs["Zjets"][0][i]
            fz_err=ROOT.TMath.Sqrt(fz_err**2+vals_errs["Zjets"][1][i]**2)
        print("\nActual Data subtracted")
        print("  vals: {}".format(vals_errs["data"][0][:5]))
        print("        {}".format(vals_errs["data"][0][5:]))
        print("  errs: {}".format(vals_errs["data"][1][:5]))
        print("        {}".format(vals_errs["data"][1][5:]))
        print("\nActual Zjets")
        print("  vals: {}".format(vals_errs["Zjets"][0][:5]))
        print("        {}".format(vals_errs["Zjets"][0][5:]))
        print("  errs: {}".format(vals_errs["Zjets"][1][:5]))
        print("        {}".format(vals_errs["Zjets"][1][5:]))
        '''
        data_hist.Draw()
        zjets_hist.Draw("same")

        x_min = options[1]
        x_max = options[2]
        fit_min = options[3]
        fit_max = options[4]
        bins = array("d", options[5])
        n_bins = len(options[5]) - 1

        data_hist = plot.rebin(data_hist, n_bins, bins, variable=True)
        zjets_hist = plot.rebin(zjets_hist, n_bins, bins, variable=True)
        '''
        fd_val=data_hist.GetBinContent(data_hist.GetXaxis().FindBin(155))
        fd_err=data_hist.GetBinError(data_hist.GetXaxis().FindBin(155))
        fz_val=zjets_hist.GetBinContent(zjets_hist.GetXaxis().FindBin(155))
        fz_err=zjets_hist.GetBinError(zjets_hist.GetXaxis().FindBin(155))
        print("\nActual final data val: {} final zjets val: {}".format(fd_val,fz_val))
        print("Actual final data err: {} final zjets err: {}\n".format(fd_err,fz_err))
        '''

        zjets_hist.Draw("hist")
        zjets_hist.SetTitle("")
        zjets_hist.SetFillStyle(1001)
        zjets_hist.SetFillColor(ROOT.kBlue + 2)
        zjets_hist.SetLabelOffset(5)

        data_hist.Draw("p same")
        scale=0
        if not zjets_hist.Integral() == 0:
            scale=data_hist.Integral() / zjets_hist.Integral()
            zjets_hist.Scale(scale)
        else:
            print("NAME: {} VARIABLE".format(name, v_name))
            print("WARNING could not scale, zjets_hist is empty")
        '''
        print("\nfinal zjets after scaling val: {}  err: {}".format(fz_val*scale,fz_err*scale))
        fz_val=zjets_hist.GetBinContent(zjets_hist.GetXaxis().FindBin(155))
        fz_err=zjets_hist.GetBinError(zjets_hist.GetXaxis().FindBin(155))
        print("Actual final zjets after scaling val: {}  err: {}\n".format(fz_val,fz_err))
        '''
        y_max = max(zjets_hist.GetMaximum(), data_hist.GetMaximum())
        zjets_hist.SetAxisRange(0.0, y_max * 1.5, "Y")
        data_hist.SetMarkerColor(ROOT.kOrange + 7)
        data_hist.SetMarkerStyle(8)
        data_hist.SetLineColor(ROOT.kOrange + 7)
        data_hist.SetLineWidth(1)

        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.04)
        t.SetTextAlign(4)

        info = plot.info_string_0lep(name)

        upper.cd()
        t.DrawLatex(0.13, 0.86, "#bf{#it{" + info + "}}")
        t.DrawLatex(0.13, 0.80, "ATLAS #it{#bf{Internal}}")

        legend.AddEntry(zjets_hist, "Z + jets {}".format(("Sh221" if doMGvSh else "")), "f")
        
        if nosig:
            legend.AddEntry(data_hist, "data - (non zjets bkg + sig)", "pe")
        elif doMGvSh:
            legend.AddEntry(data_hist,"Z + jets MGPy8","pe")
        else:
            legend.AddEntry(data_hist, "data - non zjets bkg", "pe")
        
        lower.cd()

        ratio = data_hist.Clone()
        ratio.Divide(zjets_hist)
        ratio.SetTitle("")
        '''
        print("\nfinal ratio val: {} err: {}".format(fd_val/fz_val,(fd_val/fz_val)*ROOT.TMath.Sqrt((fd_err/fd_val)**2+(fz_err/fz_val)**2)))
        fr_val=ratio.GetBinContent(ratio.GetXaxis().FindBin(155))
        fr_err=ratio.GetBinError(ratio.GetXaxis().FindBin(155))
        print("Actual final ratio val: {} err: {}\n".format(fr_val,fr_err))
        '''

        def closest_in_list(_list, val):
            return min(_list, key=lambda x: abs(x - val))

        fit = plot.get_fit_func(v_name, args)
        fit_2lep = plot.get_fit_func(v_name, args)
        fit_2lep_sym = plot.get_fit_func(v_name, args)
        
        if fit and do_fit:
            ratio.Fit(fit, "QIMSN", "", fit_min, fit_max)
            if fit.GetNDF() != 0:
                if "mBB" in v_name:
                    fit_func = "({0:.2E} +/- {1:.2E}) x - ({2:.2E} +/- {3:.2E})".format(
                        Decimal(str(fit.GetParameter(0))),
                        Decimal(str(fit.GetParError(0))),
                        Decimal(str(fit.GetParameter(1))),
                        Decimal(str(fit.GetParError(1))),
                    )
                elif "MET" in v_name:
                    fit_func = "({0:.2E} +/- {1:.2E}) log10(x/500) - ({2:.2E} +/- {3:.2E})".format(
                        Decimal(str(fit.GetParameter(0))),
                        Decimal(str(fit.GetParError(0))),
                        Decimal(str(fit.GetParameter(1))),
                        Decimal(str(fit.GetParError(1))),
                    )
                elif "pTBB" in v_name:
                    fit_func = "({0:.2E} +/- {1:.2E}) x + ({2:.2E} +/- {3:.2E})".format(
                        Decimal(str(fit.GetParameter(0))),
                        Decimal(str(fit.GetParError(0))),
                        Decimal(str(fit.GetParameter(1))),
                        Decimal(str(fit.GetParError(1))),
                    )
                upper.cd()
                t.DrawLatex(0.13, 0.72, "#bf{#it{" + fit_func + "}}")
                t.DrawLatex(
                    0.13,
                    0.76,
                    "#bf{#it{"
                    + "chisquare {0:.2f} / ndf {1} = {2:.2f}".format(
                        fit.GetChisquare(),
                        fit.GetNDF(),
                        fit.GetChisquare() / fit.GetNDF(),
                    )
                    + "}}",
                )
                lower.cd()
            else:
                print("can't plot chi2")
        ratio.Draw()
        if fit and do_fit:
            conf = ROOT.TH1F("conf int", "", n_bins, bins)
            ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(conf)
            my_red = ROOT.TColor().GetColorTransparent(ROOT.kOrange + 7, 0.3)
            conf.SetFillColor(my_red)
            conf.Draw("e3 same")

        fit_name = name + "_" + ("pTV" if v_name == "MET" else v_name)  
        #print("Fit name ".format(fit_name))      
        fit_pars = []
        if fit_name in fits_2lep: 
            fit_pars=fits_2lep[fit_name]
        else:
            fit_pars=[1,0,0,0]
        print(fit_name, '' , fit_pars)
        fit_2lep.SetParameter(0, fit_pars[0])
        fit_2lep.SetParameter(1, fit_pars[2])
        fit_2lep_sym.SetParameter(0, -fit_pars[0]) 
        fit_2lep_sym.SetParameter(1, 2 - fit_pars[2]) 
        
        fit_2lep.SetLineColor(ROOT.kGreen + 2)
        fit_2lep.SetLineWidth(2)
        fit_2lep_sym.SetLineColor(ROOT.kGreen + 2)
        fit_2lep_sym.SetLineStyle(2)
        fit_2lep_sym.SetLineWidth(2)
        
        ratio.GetXaxis().SetTitle("#bf{" + v_name + " (Gev)}")
        ratio.GetXaxis().SetTitleSize(0.05)
        ratio.GetXaxis().SetTitleOffset(0.9)
        ratio.SetMarkerStyle(8)
        ratio.SetMarkerColor(ROOT.kBlack)
        ratio.SetLineColor(ROOT.kBlack)
        ratio.SetAxisRange(0.75, 1.25, "Y")
        ratio.GetXaxis().SetLabelSize(0.04)
        ratio.GetYaxis().SetLabelSize(0.04)
        ratio.GetYaxis().SetTitle(("#bf{MGPy8/Sh221}" if doMGvSh else "#bf{Data/MC}"))
        ratio.GetYaxis().SetTitleSize(0.05)
        ratio.GetYaxis().SetTitleOffset(0.7)

        baseline = zjets_hist.Clone()
        #print(baseline.GetBinError(2))
        #print(baseline.GetBinContent(2))
        #baseline.Divide(baseline)
        
        for i in range(baseline.GetNbinsX()):
            if baseline.GetBinContent(i) != 0:
                baseline.SetBinError(i, baseline.GetBinError(i) / baseline.GetBinContent(i))
                baseline.SetBinContent(i, 1.0)

        #print(baseline.GetBinError(2))
        #print(baseline.GetBinContent(2))

        baseline.Draw("same L E2")
        ratio.Draw("same")
        baseline.SetFillColor(ROOT.kAzure - 4)
        baseline.SetFillStyle(3344)
        baseline.SetLineStyle(1)
        baseline.SetLineColor(ROOT.kAzure - 4)
        baseline.SetLineWidth(3)
        baseline.SetMarkerStyle(1)
        baseline.SetMarkerSize(0)

        if do_fit and fit:
            fit.SetLineColor(ROOT.kOrange + 7)
            fit.SetFillColor(ROOT.kOrange + 7)
            fit.Draw("LSAME")
        fit_2lep.Draw("same l")
        fit_2lep_sym.Draw("same l")
    
        if v_name == 'MET' and (name + "_METpT") in fits_2lep:
            fit_mp = plot.get_fit_func(v_name, args)
            fit_mp_sym = plot.get_fit_func(v_name, args)
            
            fit_pars=fits_2lep[name + "_METpT"]

            fit_mp.SetParameter(0, fit_pars[0])
            fit_mp.SetParameter(1, fit_pars[2])
            fit_mp_sym.SetParameter(0, -fit_pars[0])
            fit_mp_sym.SetParameter(1, 2 - fit_pars[2])
            
            fit_mp.SetLineColor(ROOT.kRed)
            fit_mp.SetLineWidth(2)
            fit_mp_sym.SetLineColor(ROOT.kRed)
            fit_mp_sym.SetLineStyle(2)
            fit_mp_sym.SetLineWidth(2)

            fit_mp.Draw("same l")
            fit_mp_sym.Draw("same l")

            legend.AddEntry(fit_mp,'2lep METpT fit','l')
    

        legend.AddEntry(fit,'0lep fit','l')
        legend.AddEntry(fit_2lep,'2lep fit','l')
        legend.AddEntry(baseline,'Z+jets rel. err.','f')
        upper.cd()
        legend.Draw()
        
        canvas.Update()
        canvas.Draw()

        name1 = name
        # finished with name, adding nosig if necessary
        if nosig:
            name1 += "_nosig"
        if doMGvSh:
            name1 += "_MGvSh"
        plot_dir = "plots/{0}/{1}/{2}".format(args.channel,args.tree_type, name1)
        if not os.path.isdir(plot_dir):
            os.makedirs(plot_dir)
        canvas.SaveAs("plots/{0}/{1}/{2}/{2}_{3}.pdf".format(args.channel,args.tree_type, name1, v_name))

        if "MET" in v_name:
            lower.SetLogx()
            upper.SetLogx()
            canvas.SaveAs("plots/{0}/{1}/{2}/{2}_MET_logx.pdf".format(args.channel,args.tree_type, name1))
    
