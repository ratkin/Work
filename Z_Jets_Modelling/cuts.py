"""
Defines a number of cuts for use in RDF.Filter()
These cuts are defined in the internal notes for the vhbb analysis
"""


def topemuCR(df):
    """
    top e mu control region definition
    """
    desc = "topemucr"
    topemu = df.Filter('Description == "topemucr"')
    return topemu


def signal_region(df, args):
    """
    Signal region definition for easy/mva trees and 0/2 lepton
    """
    if args.tree_type == "easy":
        sr_desc = 'Description == "SR"'
        SR = df.Filter(sr_desc)
    elif args.tree_type == "mva":
        two_tag = "nTags  == 2"
        ptv = "pTV > 75"
        met = "MET > 150"
        twotag = df.Filter(two_tag)
        SR = twotag.Filter(ptv) if args.channel == "2lep" else twotag.Filter(met)
    return SR


def two_lep_cr(df):
    """
    ICHEP Z + jets control region definition
    apply signal region cuts also!!!
    """
    METsig_cut = "METSig < 3.5"
    CR = df.Filter(METsig_cut)
    return CR


def blind(df, args):
    """
    Removes events in signal window
    """
    mbb_cut = (
        "mBB < 110000 || mBB > 140000"
        if args.tree_type == "easy"
        else "mBB <110 || mBB > 140"
    )
    blind = df.Filter(mbb_cut)
    return blind


def inside_mbb_window(df, args):
    """
    Selects only events inside the signal window, use with caution
    """
    inverse_mbb_cut = (
        "mBB > 110000 && mBB < 140000"
        if args.tree_type == "easy"
        else "mBB >110 || mBB < 140"
    )
    inside = df.Filter(inverse_mbb_cut)
    return inside


def get_ptv_range(df, ptv_min, ptv_max=None):
    """
    Selects events in the desired ptv range
    """
    if ptv_max:
        ptv = "pTV > {0} && pTV < {1}".format(ptv_min, ptv_max)
    else:
        ptv = "pTV > {0}".format(ptv_min)

    ptv_region = df.Filter(ptv)

    return ptv_region


def get_n_jet(df, n, args, plus=False):
    """
    Gets events with n_jets = n.
    """
    if plus:
        n_jets = "nJ >= {}" if args.tree_type == "mva" else "nJets >= {}"
    else:
        n_jets = "nJ == {}" if args.tree_type == "mva" else "nJets == {}"

    n_jets = n_jets.format(int(n))
    if n == 2.5:
        n_jets = "nJ == 2 || nJ == 3" if args.tree_type == "mva" else "nJets == 2 || nJets == 3"

    n_jet = df.Filter(n_jets)

    return n_jet


def get_n_tag(df, t, args, plus=False):
    """
    Gets events with nTags = t.
    """
    if plus:
        tag = "nbJets >= {}" if args.tree_type == "easy" else "nTags >= {}"
    else:
        tag = "nbJets == {}" if args.tree_type == "easy" else "nTags == {}"

    tag = tag.format(int(t))
    t_tag = df.Filter(tag)

    return t_tag


def get_flavour(df, f, args):
    """
    Gets events with flavour label f
    """
    if args.tree_type == "mva":
        flav = "FlavourLabel == {}".format(float(f))
        flavour = df.Filter(flav)
    else:
        print("Flavour label not implemented for easy tree")
        exit(1)

    return flavour
