def _process_var_row(variable_row):
    """
    Processes a single row of a variables .txt file.
    """
    variable_row = variable_row.split()
    unpacked = [None] * 7
    for i, element in enumerate(variable_row):
        unpacked[i] = element
    return unpacked


def get_variables(args):
    """
    Gets all the variables and relevant plotting options from the variables file
    """
    variables = {}
    with open("variables/" + args.variable_list, "r") as _file:
        for variable_row in _file:
            if variable_row[0] == "#":
                continue  # Skip line starting with a #
            variable, nbins, x_min, x_max, fit_min, fit_max, bins = _process_var_row(
                variable_row
            )
            bins = [float(edge) for edge in bins.split(",")]

            # Convert to MeV for easy tree
            if args.tree_type == "easy":
                x_min = float(x_min) * 1e3
                x_max = float(x_max) * 1e3
                fit_min = float(fit_min) * 1e3
                fit_max = float(fit_max) * 1e3
                bins = [edge * 1e3 for edge in bins]

            variables[variable] = (
                int(nbins),
                float(x_min),
                float(x_max),
                float(fit_min),
                float(fit_max),
                bins,
            )

    return variables
