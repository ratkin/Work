# don't put blank lines in this file!!!
# Write units in GeV, easy tree tuples will be automatically converted to MeV
# Make sure you use --tree_type=easy with easy trees!
#
#var     nbins xmin xmax fit_min, fit_max, bins
#default 1000  0    1000 0        1000     0,50,110,140,200,250,300,400,600
pTV      925   75   1000 75       400      75,120,150,200,250,300,1000
mBB      600   0    600  0        400      0,50,110,140,200,250,300,400,600
