"""
Helper functions for processing the text file which lists samples and their processes
"""


def sample_dirs(args):
    """
    returns a list of the directories supplied by the user
    """
    dirs = args.sample_dirs.split(",")
    dirs = [d.rstrip("/") + "/" for d in dirs]

    return dirs


def samples_by_type(args):
    """
    Sorts samples into a dictionary by the processes description
    """
    samples_by_type = {}
    with open("samples/" + args.sample_list, "r") as _file:
        for name_type in _file:
            sample_name, sample_type = name_type.split()

            if sample_type in samples_by_type:
                samples_by_type[sample_type].append(sample_name)
            else:
                samples_by_type[sample_type] = [sample_name]
    return samples_by_type


def filter_by_sample(df, args):
    """
    Computes total event count and event count for each processes
    """
    samples_dict = samples_by_type(args)
    background_filters = {}
    for _type, samples in samples_dict.iteritems():
        # print(_type)
        this_filter = ""
        for sample in samples:
            this_filter += 'sample == "{}" || '.format(sample.replace("*.root", ""))
            print(sample)

        this_filter = this_filter[:-3]
        print(this_filter)
        background_filters[_type] = this_filter

    zjets_df = df.Filter(background_filters["Zjets"])
    n_zjets = zjets_df.Count().GetValue()
    print("Zjets entries {}".format(n_zjets))

    wjets_df = df.Filter(background_filters["Wjets"])
    n_wjets = wjets_df.Count().GetValue()
    print("Wjets entries {}".format(n_wjets))

    diboson_df = df.Filter(background_filters["diboson"])
    n_diboson = diboson_df.Count().GetValue()
    print("diboson entries {}".format(n_diboson))

    stop_df = df.Filter(background_filters["stop"])
    n_stop = stop_df.Count().GetValue()
    print("single top entries {}".format(n_stop))

    ttbar_df = df.Filter(background_filters["ttbar"])
    n_ttbar = ttbar_df.Count().GetValue()
    print("ttbar entries {}".format(n_ttbar))

    signal_df = df.Filter(background_filters["signal"])
    n_signal = signal_df.Count().GetValue()
    print("signal entries: {}".format(n_signal))

    hcc_df = df.Filter(background_filters["hcc"])
    n_hcc = hcc_df.Count().GetValue()
    print("hcc entries: {}".format(n_hcc))

    n_total = n_zjets + n_wjets + n_diboson + n_stop + n_ttbar + n_signal + n_hcc
    print("sum of background entries: {}".format(n_total))
