#! /bin/bash

# source code.sh 0lep y 

channel=${1}
log=$2
Lep2="/home/ratkin/workdir/Z_Jets_modelling/files/2lep_phiMET/"
Lep0="/home/ratkin/workdir/Z_Jets_modelling/files/0lep/"

command=""
if [[ ${channel} == "0lep" ]]; then
    command="python run_systematics.py "${Lep0}"0L_32-15_a_MVA_D/,"${Lep0}"0L_32-15_d_MVA_D/,"${Lep0}"0L_32-15_e_MVA_D/ --channel=0lep --variable_list=default-0lep.txt"
    if [[ ${log} == "y" ]]; then
	command="nohup "$command" >& running0lep.log &"
    fi
fi
if [[ ${channel} == "2lep" ]]; then
    command="python run_systematics.py "${Lep2}"2L_32-15_a_MVA_D/,"${Lep2}"2L_32-15_d_MVA_D/,"${Lep2}"2L_32-15_e_MVA_D/ --channel=2lep --variable_list=default.txt --tree_type=mva"
    if [[ ${log} == "y" ]]; then
	command="nohup "$command" >& running2lep.log &"
    fi
fi

echo $command
eval $command
