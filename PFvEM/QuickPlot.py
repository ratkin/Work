"""
Runs over histograms
"""
import os
import sys
import ROOT
import math
import copy
from array import array
from decimal import Decimal
#import argparse

# run as: a is the period (use all for combination), em18 is the jet type for 33-01 (leave blank if doing special tests)
# python Validation_AllSamples.py all >& ValidationSignificances.txt


sys.argv.append("-b")

/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3_STXS_AllChannels/JetPtOptimisation45J120J220J3_STXS_AllChannels/Reader_0L_33-05_a_Resolved_D_D/fet

File1 = "/afs/cern.ch/user/r/ratkin/CxAODMaker_24Nov_v32-15/run/Maker_410470_DoCut/hist-CxAOD.root"
File2 = "/afs/cern.ch/user/r/ratkin/CxAODMaker_24Nov_v32-15/run/Maker_410470_NoDoCut/hist-CxAOD.root"
File3 = "/afs/cern.ch/user/r/ratkin/CxAODMaker_24Nov_v33-05/run/Maker_410470_DoCut/hist-CxAOD.root"
File4 = "/afs/cern.ch/user/r/ratkin/CxAODMaker_24Nov_v33-05/run/Maker_410470_NoDoCut/hist-CxAOD.root"
File5 = "/afs/cern.ch/user/r/ratkin/CxAODMaker_24Nov_v33-05/run/Maker_410470_DoCut_em18/hist-CxAOD.root"

f1 = ROOT.TFile.Open(File1)
f2 = ROOT.TFile.Open(File2)
f3 = ROOT.TFile.Open(File3)
f4 = ROOT.TFile.Open(File4)
f5 = ROOT.TFile.Open(File5)

def plotter(histName):

    f1.cd()
    h1 = f1.Get(histName)
    f2.cd()
    h2 = f2.Get(histName)
    f3.cd()
    h3 = f3.Get(histName)
    f4.cd()
    h4 = f4.Get(histName)
    f5.cd()
    h5 = f5.Get(histName)
    hName=histName.split("/")[1]

    if "Preselection" in hName:
        print("32-15 doCut     : 5 {} 6 {} 7 {} 8 {} 9 {} 10 {}".format(h1.GetBinContent(5),h1.GetBinContent(6),h1.GetBinContent(7),h1.GetBinContent(8),h1.GetBinContent(9),h1.GetBinContent(10),))
        print("32-15 doNoCut   : 5 {} 6 {} 7 {} 8 {} 9 {} 10 {}".format(h2.GetBinContent(5),h2.GetBinContent(6),h2.GetBinContent(7),h2.GetBinContent(8),h2.GetBinContent(9),h2.GetBinContent(10),))
        print("33-05 doCut     : 5 {} 6 {} 7 {} 8 {} 9 {} 10 {}".format(h3.GetBinContent(5),h3.GetBinContent(6),h3.GetBinContent(7),h3.GetBinContent(8),h3.GetBinContent(9),h3.GetBinContent(10),))
        print("33-05 doNoCut   : 5 {} 6 {} 7 {} 8 {} 9 {} 10 {}".format(h4.GetBinContent(5),h4.GetBinContent(6),h4.GetBinContent(7),h4.GetBinContent(8),h4.GetBinContent(9),h4.GetBinContent(10),))
        print("33-05 doCut em18: 5 {} 6 {} 7 {} 8 {} 9 {} 10 {}".format(h5.GetBinContent(5),h5.GetBinContent(6),h5.GetBinContent(7),h5.GetBinContent(8),h5.GetBinContent(9),h5.GetBinContent(10),))

    if "electron" in hName:
        print("32-15 doCut     : 1 {} 2 {} 3 {} 4 {} 10 {}".format(h1.GetBinContent(1),h1.GetBinContent(2),h1.GetBinContent(3),h1.GetBinContent(4),h1.GetBinContent(10)))
        print("32-15 doNoCut   : 1 {} 2 {} 3 {} 4 {} 10 {}".format(h2.GetBinContent(1),h2.GetBinContent(2),h2.GetBinContent(3),h2.GetBinContent(4),h2.GetBinContent(10)))
        print("33-05 doCut     : 1 {} 2 {} 3 {} 4 {} 10 {}".format(h3.GetBinContent(1),h3.GetBinContent(2),h3.GetBinContent(3),h3.GetBinContent(4),h3.GetBinContent(10)))
        print("33-05 doNoCut   : 1 {} 2 {} 3 {} 4 {} 10 {}".format(h4.GetBinContent(1),h4.GetBinContent(2),h4.GetBinContent(3),h4.GetBinContent(4),h4.GetBinContent(10)))
        print("33-05 doCut em18: 1 {} 2 {} 3 {} 4 {} 10 {}".format(h5.GetBinContent(1),h5.GetBinContent(2),h5.GetBinContent(3),h5.GetBinContent(4),h5.GetBinContent(10)))
    

    
    canvas1 = ROOT.TCanvas(hName, hName, 900, 900)
    upper1 = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower1 = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    
    upper1.Draw()
    lower1.Draw()
    lower1.SetGridy()
    lower1.cd().SetBottomMargin(0.3)
    upper1.cd()
    ROOT.gStyle.SetOptStat(0)
    
    h1.Draw("hist")
    h1.SetTitle("")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    h1.SetLineColor(ROOT.kBlack)
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
    
    h2.Draw("same")
    h2.SetLineColor(ROOT.kBlue)
    h2.SetLineWidth(2)
    h3.Draw("same")
    h3.SetLineColor(ROOT.kRed)
    h3.SetLineWidth(2)
    h4.Draw("same")
    h4.SetLineColor(ROOT.kMagenta)
    h4.SetLineWidth(2)
    h5.Draw("same")
    h5.SetLineColor(ROOT.kCyan)
    h5.SetLineWidth(2)
    
    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    #legend.SetHeader("              MC,    NEntries,    SoW")
    legend.AddEntry(h1, "32-15 doCut",  "l")
    legend.AddEntry(h2, "32-15 doNoCut",  "l")
    legend.AddEntry(h3, "33-05 doCut",  "l")
    legend.AddEntry(h4, "33-05 doNoCut",  "l")
    legend.AddEntry(h5, "33-05 doCut em18",  "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()
    
    lower1.cd()
    rat2 = h2.Clone()
    rat2.Divide(h1)
    rat3 = h3.Clone()
    rat3.Divide(h1)
    rat4 = h4.Clone()
    rat4.Divide(h1)
    rat5 = h5.Clone()
    rat5.Divide(h1)
    
    rat2.SetTitle("")
    #rat2.GetXaxis().SetTitle(Var)
    rat2.GetXaxis().SetTitleSize(0.09)
    rat2.GetXaxis().SetTitleOffset(1.05)
    
    yLow = 0.7
    yHigh = 1.5
    rat2.SetLineWidth(2)
    rat2.SetLineColor(ROOT.kBlue)
    rat3.SetLineWidth(2)
    rat3.SetLineColor(ROOT.kRed)
    rat4.SetLineWidth(2)
    rat4.SetLineColor(ROOT.kMagenta)
    rat5.SetLineWidth(2)
    rat5.SetLineColor(ROOT.kCyan)
    
    rat2.SetAxisRange(yLow, yHigh, "Y")
    rat2.GetXaxis().SetLabelSize(0.09)
    rat2.GetYaxis().SetLabelSize(0.07)
    rat2.GetYaxis().SetTitleOffset(0.7)
    rat2.GetYaxis().SetTitleSize(0.06)
    rat2.GetYaxis().SetTitle("/32-15 doCut")
    rat2.GetYaxis().SetNdivisions(506)
    rat2.Draw("")
    rat3.Draw("same")
    rat4.Draw("same")
    rat5.Draw("same")
    
    canvas1.SaveAs("QuickPlot_{}.png".format(hName))
    
    
names = ["CutFlow/PreselectionCutFlow",
         "CutFlow_Nominal/muon_Nominal",
         "CutFlow_Nominal/tau_Nominal",
         "CutFlow_Nominal/electron_Nominal",
         "CutFlow_Nominal/jet_Nominal",
         "CutFlow_Nominal/fatJet_Nominal",
         "CutFlow_Nominal/vrtrackJet_Nominal",]

for name in names:
    plotter(name)
                   
