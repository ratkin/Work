import argparse


def get_args():
    parser = argparse.ArgumentParser(description="PFlow vs EMTopo")
    parser.add_argument("PF", type=str, help="choice of pflow or emtopo", default="")
    parser.add_argument("channel", type=str, help="channel number", default="")
    parser.add_argument("period", type=str, help="data taking period", default="")
    parser.add_argument("--s", type=str, help="qq or gg production", default="")
    args = parser.parse_args()

    return args
