"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
import copy
import ctypes
#import argparse



sys.argv.append("-b")
print ("Opening files")


samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"]}

#print(chains_by_type["ZJets"].GetEntries())
#samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
#           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
#           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
#           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
#           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}

samples = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
           "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
           "stop" : ["stop"],"diboson" : ["diboson"],"data" : ["data"],
           "signal" : ["signal"]}

colours = {"signal" : ROOT.kRed, "WJetsbb" : ROOT.kGreen+4, 
           "WJetsbc" : ROOT.kGreen+3,
           "WJetsbl" : ROOT.kGreen+2,
           "WJetscc" : ROOT.kGreen+1,
           "WJetscl" : ROOT.kGreen-6,
           "WJetsl" : ROOT.kGreen-9,
           "ZJetsbb" : ROOT.kAzure+2, 
           "ZJetsbc" : ROOT.kAzure+1, 
           "ZJetsbl" : ROOT.kAzure-2, 
           "ZJetscc" : ROOT.kAzure-4, 
           "ZJetscl" : ROOT.kAzure-8, 
           "ZJetsl" : ROOT.kAzure-9, 
           "ttbarbb" : ROOT.kOrange, 
           "ttbarbc" : ROOT.kOrange+1, 
           "ttbarbl" : ROOT.kOrange+2, 
           "ttbarcc" : ROOT.kOrange+3, 
           "ttbarcl" : ROOT.kOrange+4, 
           "ttbarl" : ROOT.kOrange+5, 
           "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

scalefactors = {"ttbar2jet" : 0.98, "ttbar3jet" : 0.93,"WHF2jet" : 1.06,"WHF3jet" : 1.15,"ZHF2jet" : 1.16,"ZHF3jet" : 1.09,}

m_sigVals={}
m_sigValsTot={}
m_yieldVals={}
m_SBVals={}
m_sigValsMVA={}
m_yieldValsMVA={}
m_SBValsMVA={}

saveHistos = True
working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/topValidation/METFilt/" #noMETFilt/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/Condor/"

#hist_fileTTd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/Dilep/Dilep/hadds/hist-ttbar.root", "READ")
#hist_fileTTnd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/noDilep/noDilep/hadds/hist-ttbar_non.root", "READ")
#hist_fileSd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/Dilep/Dilep/hadds/hist-stop.root", "READ")
#hist_fileSnd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/noDilep/noDilep/hadds/hist-stop_noDi.root", "READ")

# no MET filtered samples
#hist_fileTTd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/Dilep_noMETFilt/Dilep_noMETFilt/hadds/hist-ttbar.root", "READ")
#hist_fileTTnd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/noDilep_noMETFilt/noDilep_noMETFilt/hadds/hist-ttbar.root", "READ")
#hist_fileSd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/Dilep_noMETFilt/Dilep_noMETFilt/hadds/hist-stop.root", "READ")
#hist_fileSnd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/noDilep_noMETFilt/noDilep_noMETFilt/hadds/hist-stop.root", "READ")

# Met filtered samples
hist_fileTTd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/Dilep_METFilt/Dilep_METFilt/hadds/hist-ttbar.root", "READ")
hist_fileTTnd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/noDilep_METFilt/noDilep_METFilt/hadds/hist-ttbar.root", "READ")
hist_fileSd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/Dilep_METFilt/Dilep_METFilt/hadds/hist-stop.root", "READ")
hist_fileSnd = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/ttbar_dilep_studies/noDilep_METFilt/noDilep_METFilt/hadds/hist-stop.root", "READ")

assert hist_fileTTd.IsOpen()
assert hist_fileTTnd.IsOpen()
assert hist_fileSd.IsOpen()
assert hist_fileSnd.IsOpen()

'''

W=open("topsValid.tex",'w')
W.write(r"\documentclass[aspectratio=916]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"    \usetheme{default}}")
W.write("\n")
W.write(r"    \usepackage{graphicx}")
W.write("\n")
W.write(r"    \usepackage{booktabs}")
W.write("\n")
W.write(r"    \usepackage{caption}")
W.write("\n")
W.write(r"    \usepackage{subcaption}")
W.write("\n")
W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"    \begin{document}")
W.write("\n")        
W.write(r"\end{document} % move this to end of file")
W.write("\n")
W.close()
'''


def getSignificance(S,B,vtag):
    i = S.FindBin(30)
    binMax = S.FindBin(250)
    summ=0
    while i <= binMax:
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)

def getSignificanceMVA(S,B,vtag):
    i = 1
    summ=0
    while i <= S.GetNbinsX():
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)

def doBlinding(S1,B1,h1,var):
    thresh = 0.1
    if ("mBB" in var or "Mbb" in var) and "J" not in var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            i+=1
    elif "mva" in var:
        tot_sig = S1.Integral()
        if not tot_sig > 0: 
            return h1
        sum_sig=0
        i=h1.GetNbinsX()
        #while h1.GetNbinsX()-i<5:
        #    h1.SetBinContent(i, 0)
        #    h1.SetBinError(i, 0)
        #    i-=1
        while i>0:
            sum_sig+=S1.GetBinContent(i)
            if sum_sig/tot_sig < 0.7:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
                i-=1
            else:
                break
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1

    return h1

def MbbFitter(h422,h4225,h423,h432,h433):
    #h.SetLineColor(ROOT.kBlue)
    #h.SetFillColor(ROOT.kBlue)
    
    m = ROOT.RooRealVar("m","m",25,200)
    B_xp = ROOT.RooRealVar("B_xp","B_xp",121.5,100,130)
    B_sig = ROOT.RooRealVar("B_sig","B_sig",15,5,25)
    B_xi = ROOT.RooRealVar("B_xi","B_xi",0,-1,1)
    B_rho1 = ROOT.RooRealVar("B_rho1","B_rho1", 0,-1,1)
    B_rho2 = ROOT.RooRealVar("B_rho2","B_rho2", 0,-1,1)

    B_xp_1 = ROOT.RooRealVar("B_xp_1","B_xp_1",121.5,100,130)
    B_sig_1 = ROOT.RooRealVar("B_sig_1","B_sig_1",15,5,25)
    B_xi_1 = ROOT.RooRealVar("B_xi_1","B_xi_1",0,-1,1)
    B_rho1_1 = ROOT.RooRealVar("B_rho1_1","B_rho1_1", 0,-1,1)
    B_rho2_1 = ROOT.RooRealVar("B_rho2_1","B_rho2_1", 0,-1,1)

    B_xp_2 = ROOT.RooRealVar("B_xp_2","B_xp_2",121.5,100,130)
    B_sig_2 = ROOT.RooRealVar("B_sig_2","B_sig_2",15,5,25)
    B_xi_2 = ROOT.RooRealVar("B_xi_2","B_xi_2",0,-1,1)
    B_rho1_2 = ROOT.RooRealVar("B_rho1_2","B_rho1_2", 0,-1,1)
    B_rho2_2 = ROOT.RooRealVar("B_rho2_2","B_rho2_2", 0,-1,1)

    B_xp_3 = ROOT.RooRealVar("B_xp_3","B_xp_3",121.5,100,130)
    B_sig_3 = ROOT.RooRealVar("B_sig_3","B_sig_3",15,5,25)
    B_xi_3 = ROOT.RooRealVar("B_xi_3","B_xi_3",0,-1,1)
    B_rho1_3 = ROOT.RooRealVar("B_rho1_3","B_rho1_3", 0,-1,1)
    B_rho2_3 = ROOT.RooRealVar("B_rho2_3","B_rho2_3", 0,-1,1)

    B_xp_4 = ROOT.RooRealVar("B_xp_4","B_xp_4",121.5,100,130)
    B_sig_4 = ROOT.RooRealVar("B_sig_4","B_sig_4",15,5,25)
    B_xi_4 = ROOT.RooRealVar("B_xi_4","B_xi_4",0,-1,1)
    B_rho1_4 = ROOT.RooRealVar("B_rho1_4","B_rho1_4", 0,-1,1)
    B_rho2_4 = ROOT.RooRealVar("B_rho2_4","B_rho2_4", 0,-1,1)

    
    bukin = ROOT.RooBukinPdf("bukin","bukin",m,B_xp,B_sig,B_xi,B_rho1,B_rho2)
    bukin1 = ROOT.RooBukinPdf("bukin1","bukin1",m,B_xp_1,B_sig_1,B_xi_1,B_rho1_1,B_rho2_1)
    bukin2 = ROOT.RooBukinPdf("bukin2","bukin2",m,B_xp_2,B_sig_2,B_xi_2,B_rho1_2,B_rho2_2)
    bukin3 = ROOT.RooBukinPdf("bukin3","bukin3",m,B_xp_3,B_sig_3,B_xi_3,B_rho1_3,B_rho2_3)
    bukin4 = ROOT.RooBukinPdf("bukin4","bukin4",m,B_xp_4,B_sig_4,B_xi_4,B_rho1_4,B_rho2_4)

    #h.Rebin(5)
    #h1.Rebin(5)
    dH = ROOT.RooDataHist("dH","dH",ROOT.RooArgList(m),h422)
    dH1 = ROOT.RooDataHist("dH1","dH1",ROOT.RooArgList(m),h4225)
    dH2 = ROOT.RooDataHist("dH2","dH2",ROOT.RooArgList(m),h423)
    dH3 = ROOT.RooDataHist("dH3","dH3",ROOT.RooArgList(m),h432)
    dH4 = ROOT.RooDataHist("dH4","dH4",ROOT.RooArgList(m),h433)
    
    mframe = m.frame()
    
    fit = bukin.fitTo(dH,ROOT.RooFit.Save())
    fit1 = bukin1.fitTo(dH1,ROOT.RooFit.Save())
    fit2 = bukin2.fitTo(dH2,ROOT.RooFit.Save())
    fit3 = bukin3.fitTo(dH3,ROOT.RooFit.Save())
    fit4 = bukin4.fitTo(dH4,ROOT.RooFit.Save())
    #fit2.Print()
    
    #dH.plotOn(mframe,ROOT.RooFit.Name("Data"))
    #dH1.plotOn(mframe,ROOT.RooFit.Name("Data1"))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("Fit"),ROOT.RooFit.LineColor(2))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("Fit1"),ROOT.RooFit.LineColor(3))
    #mframe.Draw()
    #chi2 = mframe.chiSquare("Fit","Data",5)
    #chi21 = mframe.chiSquare("Fit1","Data1",5)
    #print("chi2: {} chi21: {}".format(chi2,chi21))

    HI = bukin.createHistogram('m', 175)
    HI1 = bukin1.createHistogram('m', 175)
    HI2 = bukin2.createHistogram('m', 175)
    HI3 = bukin3.createHistogram('m', 175)
    HI4 = bukin4.createHistogram('m', 175)
    #print(h.Integral("width"), h.GetBinContent(100))
    #print(HI.Integral("width"), HI.GetBinContent(100))
    #print(h.Integral("width")/HI.Integral("width"))
    #print(h.GetBinContent(h.GetXaxis().FindBin(100))/HI.GetBinContent(HI.GetXaxis().FindBin(100)))
    HI.Scale(h422.Integral("width")/HI.Integral("width"))
    HI1.Scale(h4225.Integral("width")/HI1.Integral("width"))
    HI2.Scale(h423.Integral("width")/HI2.Integral("width"))
    HI3.Scale(h432.Integral("width")/HI3.Integral("width"))
    HI4.Scale(h433.Integral("width")/HI4.Integral("width"))
    #HI.Scale(h.Integral()/HI.Integral())
    #HI1.Scale(h1.Integral()/HI1.Integral())
    #print(HI.Integral(), HI.GetBinContent(100))
    HI.SetFillStyle(0)
    HI.SetLineWidth(2)
    HI.SetLineStyle(2)
    HI1.SetFillStyle(0)
    HI1.SetLineWidth(2)
    HI1.SetLineStyle(2)
    HI2.SetFillStyle(0)
    HI2.SetLineWidth(2)
    HI2.SetLineStyle(2)
    HI3.SetFillStyle(0)
    HI3.SetLineWidth(2)
    HI3.SetLineStyle(2)
    HI4.SetFillStyle(0)
    HI4.SetLineWidth(2)
    HI4.SetLineStyle(2)

    mean = B_xp.getVal()
    sigma = B_sig.getVal()
    mean1 = B_xp_1.getVal()
    sigma1 = B_sig_1.getVal()
    mean2 = B_xp_2.getVal()
    sigma2 = B_sig_2.getVal()
    mean3 = B_xp_3.getVal()
    sigma3 = B_sig_3.getVal()
    mean4 = B_xp_4.getVal()
    sigma4 = B_sig_4.getVal()

    return HI,HI1,HI2,HI3,HI4,[mean,sigma,mean1,sigma1,mean2,sigma2,mean3,sigma3,mean4,sigma4]
    
    #dH.plotOn(mframe,ROOT.RooFit.DrawOption("B"),ROOT.RooFit.FillColorAlpha(0,0),ROOT.RooFit.LineColor(ROOT.kBlue), ROOT.RooFit.MarkerColor(ROOT.kBlue))
    #dH1.plotOn(mframe,ROOT.RooFit.LineColor(ROOT.kRed), ROOT.RooFit.MarkerColor(ROOT.kRed))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("bukin"),ROOT.RooFit.LineColor(ROOT.kBlue),ROOT.RooFit.LineStyle(1))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("bukin1"),ROOT.RooFit.LineColor(ROOT.kRed),ROOT.RooFit.LineStyle(1))
    
    '''
    leg1 = ROOT.TLegend(0.2,0.75,0.6,0.85)
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetTextSize(0.03)
    leg1.SetHeader("   Jet type  peak  width")
    leg1.AddEntry(mframe.findObject("bukin"),"EMTopo {:.2f} {:.2f}".format(mean, sigma), "L")
    leg1.AddEntry(mframe.findObject("bukin1"),"PFlow {:.2f} {:.2f}".format(mean1, sigma1), "L")
    
    can = ROOT.TCanvas("massframe","massframe",750,800)
    can.cd()
    can.SetTickx()
    can.SetTicky()
    h.Draw('hist')
    h1.Draw('hist same')
    HI.Draw('c hist same')
    HI1.Draw('c hist same')
    
    #bukin.Draw('same')
    #fit.Draw('same')
    
    xTitle = ""
    words = histName.split("_")
    for word in words:
        if "Mbb" in word:
            xTitle = "#bf{"+word+"} [GeV]"
    
    mframe.Draw()
    mframe.SetTitle("")
    mframe.GetXaxis().SetTitle(xTitle)
    mframe.GetYaxis().SetTitle("Events")
    mframe.SetMaximum(mframe.GetMaximum()*1.2)
    leg1.Draw("same")
    mframe.GetYaxis().SetTitle("Events")
    
    can.SaveAs(plot_dir+histName+".png")
    '''


def printYields(yields,canName):
    
    W=open("JetOptTables.tex",'a')
    canName=canName.replace("_","-")

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+canName+"}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.25\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J2\_20J3}} & \multicolumn{1}{c|}{\textbf{30J2\_20J3}} & \multicolumn{1}{c|}{\textbf{20J2\_25J3}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3}} & \textbf{30J2\_30J3}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in ["data","signal","diboson","stop","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl","ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
        nom = yields[samp]["20J2_20J3"]
        if nom ==0.0:
            nom=1
        W.write(r"    {} & {:.1f} & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$)\\ ".format(
            samp,nom,yields[samp]["30J2_20J3"],100*(yields[samp]["30J2_20J3"]/nom-1),
            yields[samp]["20J2_25J3"],100*(yields[samp]["20J2_25J3"]/nom-1),
            yields[samp]["20J2_30J3"],100*(yields[samp]["20J2_30J3"]/nom-1),
            yields[samp]["30J2_30J3"],100*(yields[samp]["30J2_30J3"]/nom-1)))
        
        W.write("\n")
        if samp == "stop" or samp == "ZJetsl" or samp == "WJetsl":
             W.write(r"    \midrule")
             W.write("\n")

    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()

def printSignificance(hName):
    
    W=open("JetOptTables.tex",'a')

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+hName+" Significances}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.30\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J2\_20J3}} & \multicolumn{1}{c|}{\textbf{20J2\_25J3}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3}} & \multicolumn{1}{c|}{\textbf{30J2\_20J3}} & \textbf{30J2\_30J3}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for JETS in ["2jet","3jet","4jet","2$+$3jet","2$+$3$+$4jet"]:
        for MET in ["150_250ptv","250ptv","250_400ptv","400ptv","150ptv"]:
            keyName=JETS+"_"+MET
            keyName1=keyName.replace("_","-")

            

            if JETS == "2$+$3$+$4jet":
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\ ".format(
                    keyName1,
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["4jet_"+MET]["20J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_25J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_25J3"]**2+m_sigValsTot["4jet_"+MET]["20J2_25J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["4jet_"+MET]["20J2_30J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_20J3"]**2+m_sigValsTot["4jet_"+MET]["30J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["4jet_"+MET]["30J2_30J3"]**2)))
                W.write("\n")
            elif JETS == "2$+$3jet":
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\ ".format(
                    keyName1,
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_25J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_25J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3"]**2)))
                W.write("\n")
            else:
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\ ".format(
                    keyName1,m_sigValsTot[keyName]["20J2_20J3"],m_sigValsTot[keyName]["20J2_25J3"],
                    m_sigValsTot[keyName]["20J2_30J3"],m_sigValsTot[keyName]["30J2_20J3"],m_sigValsTot[keyName]["30J2_30J3"]))
                W.write("\n")
        W.write(r"    \midrule")
        W.write("\n")
    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()


def compareDataMC(varName,canName,applyScales):
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    histo452020={}
    histo453020={}
    histo452025={}
    histo452030={}
    histo453030={}
    Yields={"signal" : {}, "WJetsbb" : {}, "WJetsbc" : {}, "WJetsbl" : {}, "WJetscc" : {}, "WJetscl" : {}, "WJetsl" : {}, "ZJetsbb" : {}, "ZJetsbc" : {}, "ZJetsbl" : {}, "ZJetscc" : {}, "ZJetscl" : {}, "ZJetsl" : {}, "ttbarbb" : {}, "ttbarbc" : {}, "ttbarbl" : {}, "ttbarcc" : {}, "ttbarcl" : {}, "ttbarl" : {}, "stop" : {}, "diboson" : {}, "data" : {}}
    line=canName.split("_")
    canName1=canName
    hist_file422.cd()
    #print("Looping to Get histograms")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp1 = hist_file422.Get(histName1)
            histo452020[samp]=hist_tmp1.Clone()
            Yields[samp]["20J2_20J3"]=histo452020[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))

    hist_file4225.cd()
    canName1=canName1.replace("20J3","25J3")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp15 = hist_file4225.Get(histName1)
            histo452025[samp]=hist_tmp15.Clone()
            Yields[samp]["20J2_25J3"]=histo452025[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
            
    hist_file423.cd()
    canName1=canName1.replace("25J3","30J3")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp2 = hist_file423.Get(histName1)
            histo452030[samp]=hist_tmp2.Clone()
            Yields[samp]["20J2_30J3"]=histo452030[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
            
    hist_file433.cd()
    canName1=canName1.replace("20J2","30J2")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp3 = hist_file433.Get(histName1)
            histo453030[samp]=hist_tmp3.Clone()
            Yields[samp]["30J2_30J3"]=histo453030[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))

    hist_file432.cd()
    canName1=canName1.replace("30J3","20J3")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp4 = hist_file432.Get(histName1)
            histo453020[samp]=hist_tmp4.Clone()
            Yields[samp]["30J2_20J3"]=histo453020[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
    
    printYields(Yields,canName)

    histograms={}
    for sampl in samples:
        for samp in samples[sampl]:
            histograms[samp] = ROOT.TH1F(samp+canName,samp+canName,5,0,5)
            histograms[samp].SetBinContent(1,histo452020[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(1, "45J1_20J2_20J3")
            
            histograms[samp].SetBinContent(2,histo453020[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(2, "45J1_30J2_20J3")
            
            histograms[samp].SetBinContent(3,histo452025[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(3, "45J1_20J2_25J3")

            histograms[samp].SetBinContent(4,histo452030[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(4, "45J1_20J2_30J3")
            
            histograms[samp].SetBinContent(5,histo453030[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(5, "45J1_30J2_30J3")
            if samp != "data":
                histograms[samp].SetBinError(1,histo452020[samp].GetBinError(1))
                histograms[samp].SetBinError(2,histo453020[samp].GetBinError(1))
                histograms[samp].SetBinError(3,histo452025[samp].GetBinError(1))
                histograms[samp].SetBinError(4,histo452030[samp].GetBinError(1))
                histograms[samp].SetBinError(5,histo453030[samp].GetBinError(1))
            else:
                histograms[samp].SetBinError(1,math.sqrt(histo452020[samp].GetBinContent(1)))
                histograms[samp].SetBinError(2,math.sqrt(histo453020[samp].GetBinContent(1)))
                histograms[samp].SetBinError(3,math.sqrt(histo452025[samp].GetBinContent(1)))
                histograms[samp].SetBinError(4,math.sqrt(histo452030[samp].GetBinContent(1)))
                histograms[samp].SetBinError(5,math.sqrt(histo453030[samp].GetBinContent(1)))
        

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    for samp in samples:
        for sampl in samples[samp]:
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)

    if applyScales:
        for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["ttbar2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ttbar3jet"])
        for samp in ["WJetsbb","WJetsbc","WJetsbl","WJetscc"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["WHF2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["WHF3jet"])
        for samp in ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["ZHF2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ZHF3jet"])

    hs = ROOT.THStack("hs","")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl","stop",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson","signal",]:
        hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson"]:
        hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    canName2=""
    canNameLine=canName.split("_")
    for canN in canNameLine:
        if not ("J1" in canN or "J2" in canN or "J3" in canN):
            canName2+=canN+"_"
    t.DrawLatex(0.15, 0.85, canName2)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "stop","ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
        if "ZJets" in samp or "WJets" in samp:
            sampleName=samp.replace("Jets","")
            legend1.AddEntry(histograms[samp], sampleName+", {:.1f}".format(histograms[samp].Integral()), "f")
        else:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("Cuts Scenario")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.5
    yHigh = 0.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetTitle("")
    yHigh = rat1.GetMaximum()*1.1
    rat1.SetAxisRange(0, yHigh, "Y")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    

    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("Y+")


    plotName = working_dir+"compareDataMC/compareDataMC_"+canName+"_"+varName
    if applyScales:
        plotName+="_Scaled"
    if saveHistos:
        canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def doDataMC(varName,binning,canName,F,applyScales):
    print("doDataMC")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    histograms={}
    line=canName.split("_")
    F.cd()
    #print("Looping to Get histograms")
    for sampl in samples:
        for samp in samples[sampl]:
            if "nSigJets-nMatchedTruthSigJets" == varName:
                if sampl == "data" or sampl == "diboson" or sampl == "stop" or sampl == "signal":
                    histName1=samp+"_"+canName+"_"+varName
                    #print("Getting hist "+histName1)
                    hist_tmp1 = F.Get(histName1)
                    histograms[samp]=hist_tmp1.Clone()
                elif "bb" not in samp:
                    histograms[samp]=ROOT.TH1F("empty"+samp,"empty"+samp,10,0,10)
                else:
                    histName1=sampl+"_"+canName+"_"+varName
                    #print("Getting hist "+histName1)
                    hist_tmp1 = F.Get(histName1)
                    histograms[samp]=hist_tmp1.Clone()
            else:
                histName1=samp+"_"+canName+"_"+varName
                #print("Getting hist "+histName1)
                hist_tmp1 = F.Get(histName1)
                if varName == "TTBarDecay" and "ttbar" not in samp:
                    histograms[samp]=ROOT.TH1F("empty"+samp,"empty"+samp,10,0,10)
                else:
                    histograms[samp]=hist_tmp1.Clone()
                #print(samp+": {}".format(histograms[samp].Integral()))

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)


    
    rebinning = int(binning.split(",")[0])
    for samp in samples:
        for sampl in samples[samp]:
            if rebinning != 0:
                histograms[sampl].Rebin(rebinning)
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)
                for i in range(histograms[sampl].GetNbinsX()):
                    if histograms[sampl].GetBinContent(i+1)>0:
                        histograms[sampl].SetBinError(i+1,math.sqrt(histograms[sampl].GetBinContent(i+1)))
    if applyScales:
        for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["ttbar2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ttbar3jet"])
        for samp in ["WJetsbb","WJetsbc","WJetsbl","WJetscc"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["WHF2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["WHF3jet"])
        for samp in ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["ZHF2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ZHF3jet"])


    hs = ROOT.THStack("hs","")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl","stop",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson","signal",]:
        hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson"]:
        hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()

    if varName == "GSCptJ1":
        print("background 40-50: {} 50-60: {} total: {} percentage: {}".format(hb.GetBinContent(1),hb.GetBinContent(2),hb.Integral(),100*(hb.GetBinContent(1)+hb.GetBinContent(2))/hb.Integral()))
        print("signal 40-50: {} 50-60: {} total: {} percentage: {}".format(hsig.GetBinContent(1),hsig.GetBinContent(2),hsig.Integral(),100*(hsig.GetBinContent(1)+hsig.GetBinContent(2))/hsig.Integral()))

    #if ("mBB" in varName or "Mbb" in varName) and "60J1" in line[-4]:
    #    getSignificance(hsig,hb,canName+"_"+varName)
    #if ("mva" in varName) and "60J1" in line[-4]:
    #    getSignificanceMVA(hsig,hb,canName+"_"+varName)

    if ("mBB" in varName or "Mbb" in varName): # and "45J1" in line[-4]:
        keyName = ""
        keyName = line[-3]+"_"+line[-2]
        #print("filling dicts "+keyName)
        m_sigVals[keyName]=getSignificance(hsig,hb,canName+"_"+varName)
        m_yieldVals[keyName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBVals[keyName]=hsig.Integral()/hb.Integral()
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),hsig.Integral()/hb.Integral()))
        else:
            m_SBVals[keyName]=0
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),0))

    if ("mva" in varName): # and "45J1" in line[-4]:
        keyName = ""
        keyName = line[-3]+"_"+line[-2]
        m_sigValsMVA[keyName]=getSignificanceMVA(hsig,hb,canName+"_"+varName)
        m_yieldValsMVA[keyName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBValsMVA[keyName]=hsig.Integral()/hb.Integral()
        else:
            m_SBValsMVA[keyName]=0

    
    doBlinding(hsig,hb,hd,varName)
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")
        

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "stop","ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
        if "ZJets" in samp or "WJets" in samp:
            sampleName=samp.replace("Jets","")
            legend1.AddEntry(histograms[samp], sampleName+", {:.1f}".format(histograms[samp].Integral()), "f")
        else:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    if varName == "TTBarDecay":
        labs=["j-j","j-e","j-mu","j-tau","e-e","mu-mu","tau-tau","e-mu","e-tau","mu-tau"]
        for i in range(len(labs)): 
            rat.GetXaxis().SetBinLabel(i+1, labs[i])
            rat1.GetXaxis().SetBinLabel(i+1, labs[i])
 

    yLow = -0.2
    yHigh = 0.8
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("ep") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    
    yHigh2 = rat1.GetMaximum()*1.1
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    #rat1.SetAxisRange(0, yHigh2, "Y")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinError(i+1)>0.075:
            rat1.SetBinContent(i+1,(rat1.GetBinContent(i)+rat1.GetBinContent(i+2))/2)
            rat1.SetBinError(i+1,(rat1.GetBinError(i)+rat1.GetBinError(i+2))/2)
            #print("{} {}".format(rat1.GetBinContent(i+1),rat1.GetBinError(i+1)))
    
    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("hist Y+")
    

    plotName = working_dir+"DataMC/DataMC_"+canName+"_"+varName
    if applyScales:
        plotName+="_Scaled"
    if saveHistos:
        canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def makeSignificancePlots(sigVals,yieldVals,SBVals,JetMET,varName):
    h_sig = ROOT.TH1F("sig","sig",5,0,5)
    h_yield = ROOT.TH1F("yield","yield",5,0,5)
    h_SB = ROOT.TH1F("SB","SB",5,0,5)
    #gRandom = ROOT.TRandom()

    i=0
    print("\n"+JetMET+"  Sig   Yield   S/B")
    sigValsTotTemp={}
    for keyName in ["20J2_20J3","30J2_20J3","20J2_25J3","20J2_30J3","30J2_30J3"]:
        i+=1
        #keyName = JetCut1+"_"+JetCut2
        keyName1 = "20J2_20J3"
        
        #if JetCut1=="45J1" and JetCut2=="20J2" and JetCut3=="20J3":
        h_sig.SetBinContent(i,sigVals[keyName])
        h_sig.GetXaxis().SetBinLabel(i, keyName)
        h_yield.SetBinContent(i,yieldVals[keyName])
        h_yield.GetXaxis().SetBinLabel(i, keyName)
        h_SB.SetBinContent(i,SBVals[keyName])
        h_SB.GetXaxis().SetBinLabel(i, keyName)
        sigValsNom=sigVals[keyName1]
        if sigValsNom==0:
            sigValsNom=1
        yieldValsNom=yieldVals[keyName1]
        if yieldValsNom==0:
            yieldValsNom=1
        SBValsNom=SBVals[keyName1]
        if SBValsNom==0:
            SBValsNom=1
        print(keyName+":  {:.3f}({:.2f})  {:.3f}({:.2f})  {:.3f}({:.2f})".format(
            sigVals[keyName],100*(sigVals[keyName]/sigValsNom-1),
            yieldVals[keyName],100*(yieldVals[keyName]/yieldValsNom-1),
            SBVals[keyName],100*(SBVals[keyName]/SBValsNom-1)))
        sigValsTotTemp[keyName]=sigVals[keyName]
        #else:
        #    h_sig.SetBinContent(i,sigVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
        #    h_yield.SetBinContent(i,yieldVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
        #    h_SB.SetBinContent(i,SBVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
    print("")
    m_sigValsTot[JetMET]=copy.deepcopy(sigValsTotTemp)
    
    canvasSigs = ROOT.TCanvas("SigYields", "SigYields", 900, 900)
    upperSigs = ROOT.TPad("upperSigs", "upperSigs", 0.025, 0.445, 0.995, 0.995)
    upperSigs.Draw()
    upperSigs.cd()
    #upperSigs.SetGridy()
    #upperSigs.SetBottomMargin(0.3)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gPad.SetTicky(0)
    
    h_sig.SetTitle("")
    h_sig.GetXaxis().SetTitle("")
    h_sig.GetXaxis().SetTitleSize(0.09)
    h_sig.GetXaxis().SetTitleOffset(1.05)

    h_sig.SetLineWidth(2)
    h_sig.SetLineColor(ROOT.kBlack)
    h_sig.SetAxisRange(0, 4.5, "Y")
    h_sig.GetXaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetTitleOffset(0.8)
    h_sig.GetYaxis().SetTitleSize(0.05)
    h_sig.GetYaxis().SetTitle("Sig and S/B")
    h_sig.GetYaxis().SetNdivisions(506)
    y_max = h_sig.GetMaximum()*1.4
    h_sig.SetAxisRange(0.0, y_max, "Y")    
    h_sig.Draw("")
    h_SB.SetLineWidth(2)
    h_SB.SetLineColor(ROOT.kBlue)
    h_SB.Scale(100)
    h_SB.Draw("same hist")

    h_yield.SetLineColor(ROOT.kRed)
    h_yield.SetLineWidth(2)
    h_yield.SetTitle("")
    h_yield.GetXaxis().SetTitle("")
    h_yield.GetXaxis().SetTitleSize(0.09)
    h_yield.GetXaxis().SetTitleOffset(1.05)
    h_yield.GetXaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetTitleOffset(0.8)
    h_yield.GetYaxis().SetTitleSize(0.05)
    h_yield.GetYaxis().SetTitle("Signal Yield")
    h_yield.GetYaxis().SetNdivisions(506)
    y_max = h_yield.GetMaximum()*1.4
    h_yield.SetAxisRange(0.0, y_max, "Y")    

    
    canvasSigs.cd()
    upperSigs2 = ROOT.TPad("upperSigs2", "upperSigs2", 0.025, 0.445, 0.995, 0.995)
    upperSigs2.SetFillStyle(4000)
    upperSigs2.SetFrameFillStyle(0)
    upperSigs2.Draw()
    upperSigs2.cd()
    ROOT.gPad.SetTicky(0)
    #upperSigs2.SetBottomMargin(0.3)
    h_yield.Draw("Y+")


    legend1 = ROOT.TLegend(0.705, 0.75, 0.93, 0.87)
    legend1.AddEntry(h_sig ,"Significance","l")
    legend1.AddEntry(h_SB ,"S/B x 100", "l")
    legend1.AddEntry(h_yield ,"Signal Yield","l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    canvasSigs.cd()
    lowerSigs = ROOT.TPad("lowerSigs", "lowerSigs", 0.025, 0.025, 0.995, 0.495)
    lowerSigs.Draw()
    lowerSigs.SetGridy()
    lowerSigs.cd().SetBottomMargin(0.3)

    rat1=h_sig.Clone()
    rat2=h_SB.Clone()
    rat3=h_yield.Clone()
    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinContent(1)>0:
            rat1.SetBinContent(i+1,h_sig.GetBinContent(i+1)/h_sig.GetBinContent(1))
            rat1.SetBinError(i+1,h_sig.GetBinError(i+1)/h_sig.GetBinContent(1))
        if rat2.GetBinContent(1)>0:
            rat2.SetBinContent(i+1,h_SB.GetBinContent(i+1)/h_SB.GetBinContent(1))
            rat2.SetBinError(i+1,h_SB.GetBinError(i+1)/h_SB.GetBinContent(1))
        if rat3.GetBinContent(1)>0:
            rat3.SetBinContent(i+1,h_yield.GetBinContent(i+1)/h_yield.GetBinContent(1))
            rat3.SetBinError(i+1,h_yield.GetBinError(i+1)/h_yield.GetBinContent(1))

    rat1.SetTitle("")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)

    #rat1.SetLineWidth(2)
    #rat1.SetLineColor(ROOT.kBlack)
    rat1.GetXaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetTitleOffset(0.8)
    rat1.GetYaxis().SetTitleSize(0.05)
    rat1.GetYaxis().SetTitle("Cuts/Nom")
    rat1.GetYaxis().SetNdivisions(506)
    rat1.SetAxisRange(0.5, 1.5, "Y")  

    rat1.Draw("hist")
    rat2.Draw("same hist")
    rat3.Draw("same hist")



    if saveHistos:
        canvasSigs.SaveAs(working_dir+"Significances/Significances_"+JetMET+"_"+varName+".png")
    canvasSigs.Close()
    
def cutsComparison(F1,F2,binning,canName):
    print("cutsComparison")
    #ttbar_2btag2jet_400ptv_CRLow_mBB
    line=canName.split("_")
    canName1 = canName

    if "stop" in line[0]:
        canName1 = canName.replace("stop","stopt")
    if "All" in canName:
        canName1 = canName.replace("All","SR")
    
    F1.cd()
    print("Getting "+canName1)
    hDilep = ROOT.TH1F()
    if not "stop" in line[0]:
        hist_tmp1 = F1.Get(canName1)
        hDilep = hist_tmp1.Clone()
        if "All" in canName:
            canName2 = canName.replace("All","CRLow")
            hist_tmp2 = F1.Get(canName2)
            hDilep.Add(hist_tmp2.Clone())
            canName2 = canName.replace("All","CRHigh")
            hist_tmp3 = F1.Get(canName2)
            hDilep.Add(hist_tmp3.Clone())
    else:
        i=0
        for samp in ["stopt","stops","stopWtDS"]:
            canName2=canName1.replace("stopt",samp)
            if i==0:
                try:
                    hDilep=F1.Get(canName2).Clone()
                except:
                    print(samp+" does not exist")
                    i=-1
                i+=1
            else:
                try:
                    hDilep.Add(F1.Get(canName2).Clone())
                except:
                    print(samp+" does not exist")



    F2.cd()
    hnoDilep = ROOT.TH1F()
    if not "stop" in line[0]:
        hist_tmp2 = F2.Get(canName1)
        hnoDilep = hist_tmp2.Clone()
        if "All" in canName:
            canName2 = canName.replace("All","CRLow")
            hist_tmp2a = F2.Get(canName2)
            hnoDilep.Add(hist_tmp2a.Clone())
            canName2 = canName.replace("All","CRHigh")
            hist_tmp3a = F2.Get(canName2)
            hnoDilep.Add(hist_tmp3a.Clone())
    else:
        i=0
        for samp in ["stopt","stops","stopWtDS"]:
            canName2=canName1.replace("stopt",samp)
            if i==0:
                try:
                    hnoDilep=F2.Get(canName2).Clone()
                except:
                    print(samp+" does not exist")
                    i=-1
                i+=1
            else:
                try:
                    hnoDilep.Add(F2.Get(canName2).Clone())
                except:
                    print(samp+" does not exist")
        

    errnoD = ctypes.c_double(0.)
    integralnoD = hnoDilep.IntegralAndError(1, hnoDilep.GetNbinsX(), errnoD)
    errD = ctypes.c_double(0.)
    integralD = hDilep.IntegralAndError(1, hDilep.GetNbinsX(), errD)
    if "mva" in line[-1]:
        print("noDilep Integral: {} error: {}".format(integralnoD,errnoD.value))
        print("Dilep Integral: {} error: {}".format(integralD,errD.value))

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    hnoDilep.SetLineColor(ROOT.kBlue)
    hnoDilep.SetLineWidth(2)
    hDilep.SetLineColor(ROOT.kRed)
    hDilep.SetLineWidth(2)

    rebinning = int(binning.split(",")[0])
    print(rebinning)
    if rebinning != 0:
        hnoDilep.Rebin(rebinning)
        hDilep.Rebin(rebinning)

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    #upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.025, 0.995, 0.995)
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.4, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.225, 0.995, 0.5)
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.995, 0.3)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs1.Draw()
    lower_hs1.cd().SetBottomMargin(0.3)
    lower_hs1.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    upper_hs.cd().SetBottomMargin(0.2)
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hnoDilep.GetMaximum(),hDilep.GetMaximum())*1.45 
    
    ratDilep = hDilep.Clone()
    errDilep = hDilep.Clone()
    errDilep.Reset("ICEMS")
    errnoDilep = hnoDilep.Clone()
    errnoDilep.Reset("ICEMS")
    for i in range(hDilep.GetNbinsX()):
        errDilep.SetBinContent(i+1,hDilep.GetBinError(i+1))
        errDilep.SetBinError(i+1,0.001)
        errnoDilep.SetBinContent(i+1,hnoDilep.GetBinError(i+1))
        errnoDilep.SetBinError(i+1,0.001)
    

    hnoDilep.Draw("")
    hnoDilep.SetTitle("")
    hnoDilep.SetAxisRange(0.0, y_max, "Y")
    hnoDilep.GetYaxis().SetMaxDigits(3)
    hnoDilep.GetYaxis().SetTitle("Entries")
    hnoDilep.GetYaxis().SetTitleSize(0.04)
    hnoDilep.GetYaxis().SetTitleOffset(1.2)
    hnoDilep.GetYaxis().SetLabelSize(0.05)
    hnoDilep.SetYTitle("Entries")
    hnoDilep.SetLabelOffset(5)
    hnoDilep.SetAxisRange(float(binning.split(",")[1]), float(binning.split(",")[2]), "X")
    
    hDilep.Draw("same")
    #legend1 = ROOT.TLegend(0.60, 0.65, 0.93, 0.87)
    legend1 = ROOT.TLegend(0.55, 0.75, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW,  error")
    #legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
    legend1.AddEntry(hnoDilep, "no Dilep, {:.0f}, {:.3f}, {:.3f}".format(hnoDilep.GetEntries(),integralnoD,errnoD.value), "l")
    legend1.AddEntry(hDilep, "Dilep, {:.0f}, {:.3f}, {:.3f}".format(hDilep.GetEntries(),integralD,errD.value), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    t.DrawLatex(0.15, 0.75, "#it{#bf{0L, "+line[1]+"}}")
    t.DrawLatex(0.15, 0.70, "#it{#bf{"+((line[2]+"_"+line[3]) if "ptv" in line[3] else line[2])+", "+line[-2]+"}}")
    #t.DrawLatex(0.15, 0.85, canName)
    #t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    lower_hs.cd()
    
    
    #ROOT.gPad.SetTicky(0)
    
    ratDilep.Divide(hnoDilep)
    ratDilep.SetTitle("")
    #ratDilep.GetXaxis().SetTitle("")
    #ratDilep.GetXaxis().SetTitleSize(0.09)
    #ratDilep.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.5
    #if "2jet" in canName:
    #    yHigh = 2
    ratDilep.SetLineWidth(2)
    #ratDilep.SetLineColor(ROOT.kBlue)
    ratDilep.SetAxisRange(yLow, yHigh, "Y")
    ratDilep.GetXaxis().SetLabelSize(0.0)
    #ratDilep.GetXaxis().SetTitle(line[-1])
    ratDilep.GetYaxis().SetLabelSize(0.07)
    ratDilep.GetYaxis().SetTitleOffset(0.7)
    ratDilep.GetYaxis().SetTitleSize(0.06)
    ratDilep.GetYaxis().SetTitle("Dilep / no Dilep")
    ratDilep.GetYaxis().SetNdivisions(506)

    ratDilep.Draw("p") 
    #ratDilep.GetXaxis().SetTitleSize(0.04)
    #ratDilep.GetXaxis().SetTitleOffset(1.04)
    #ratDilep.GetXaxis().SetLabelSize(0.04)
    #ratDilep.GetXaxis().SetTitle(line[-1])
    ratDilep.SetAxisRange(float(binning.split(",")[1]), float(binning.split(",")[2]), "X")


    lower_hs1.cd()
    errDilep.Divide(errnoDilep)
    errDilep.SetTitle("")
    errDilep.GetXaxis().SetTitle("")
    errDilep.GetXaxis().SetTitleSize(0.09)
    errDilep.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.2
    #if "2jet" in canName:
    #    yHigh = 2
    errDilep.SetLineWidth(2)
    errDilep.SetLineColor(ROOT.kRed)
    errDilep.SetMarkerStyle(20)
    errDilep.SetMarkerColor(ROOT.kBlack)
    #errDilep.SetLineColor(ROOT.kBlue)
    errDilep.SetAxisRange(yLow, yHigh, "Y")
    errDilep.GetXaxis().SetLabelSize(0.09)
    errDilep.GetXaxis().SetTitle(line[-1])
    errDilep.GetYaxis().SetLabelSize(0.07)
    errDilep.GetYaxis().SetTitleOffset(0.7)
    errDilep.GetYaxis().SetTitleSize(0.06)
    errDilep.GetYaxis().SetTitle("Errors (Dilep / no Dilep)")
    errDilep.GetYaxis().SetNdivisions(506)
    
    errDilep.Draw("E") 
    #errDilep.GetXaxis().SetTitleSize(0.04)
    #errDilep.GetXaxis().SetTitleOffset(1.04)
    #errDilep.GetXaxis().SetLabelSize(0.04)
    #errDilep.GetXaxis().SetTitle(line[-1])
    errDilep.SetAxisRange(float(binning.split(",")[1]), float(binning.split(",")[2]), "X")

    canvas_hs.SaveAs(working_dir+canName+".png")
    canvas_hs.Close()

def pileupEstimateAllSamples(varName,canName):
    for sample in samples:
        if sample != "signal":
            continue
        pileupEstimate(varName,canName,sample)

def pileupEstimate(varName,canName,sample):
    print("pileupEstimate")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    
    sample2=sample
    hist_file422.cd()
    histName1=sample2+"_"+canName+"_"+varName+"_2D"
    hist_tmp1 = hist_file422.Get(histName1)
    g422=hist_tmp1.Clone()
    h422 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)
    
    #hist_file4225.cd()
    #histName1=histName1.replace("20J3","25J3")
    #hist_tmp1a = hist_file4225.Get(histName1)
    #g4225=hist_tmp1a.Clone()
    #h4225 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    #hist_file423.cd()
    #histName1=histName1.replace("25J3","30J3")
    #hist_tmp1a = hist_file423.Get(histName1)
    #g423=hist_tmp1a.Clone()
    #h423 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    hist_file433.cd()
    #histName1=histName1.replace("20J2","30J2")
    histName1=histName1.replace("20J2_20J3","30J2_30J3")
    hist_tmp1c = hist_file433.Get(histName1)
    g433=hist_tmp1c.Clone()
    h433 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    #hist_file432.cd()
    #histName1=histName1.replace("30J3","20J3")
    #hist_tmp1b = hist_file432.Get(histName1)
    #g432=hist_tmp1b.Clone()
    #h432 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    
    totnum422=0
    totsum422=0
    totnum4225=0
    totsum4225=0
    totnum423=0
    totsum423=0
    totnum433=0
    totsum433=0
    totnum432=0
    totsum432=0
    for i in range(g422.GetNbinsX()):
        num422=0
        sum422=0
        num4225=0
        sum4225=0
        num423=0
        sum423=0
        num433=0
        sum433=0
        num432=0
        sum432=0
        for j in range(g422.GetNbinsY()):
            num422+=g422.GetBinContent(i+1,j+1)
            sum422+=g422.GetBinContent(i+1,j+1)*g422.GetYaxis().GetBinLowEdge(j+1)
            '''
            num4225+=g4225.GetBinContent(i+1,j+1)
            sum4225+=g4225.GetBinContent(i+1,j+1)*g4225.GetYaxis().GetBinLowEdge(j+1)
            num423+=g423.GetBinContent(i+1,j+1)
            sum423+=g423.GetBinContent(i+1,j+1)*g423.GetYaxis().GetBinLowEdge(j+1)
            num432+=g432.GetBinContent(i+1,j+1)
            sum432+=g432.GetBinContent(i+1,j+1)*g432.GetYaxis().GetBinLowEdge(j+1)
            '''
            num433+=g433.GetBinContent(i+1,j+1)
            sum433+=g433.GetBinContent(i+1,j+1)*g433.GetYaxis().GetBinLowEdge(j+1)
        if num422>0:
            h422.SetBinContent(i+1,sum422/num422)
            totnum422+=num422
            totsum422+=sum422
        '''
        if num4225>0:
            h4225.SetBinContent(i+1,sum4225/num4225)
            totnum4225+=num4225
            totsum4225+=sum4225
        if num423>0:
            h423.SetBinContent(i+1,sum423/num423)
            totnum423+=num423
            totsum423+=sum423
        if num432>0:
            h432.SetBinContent(i+1,sum432/num432)
            totnum432+=num432
            totsum432+=sum432
        '''
        if num433>0:
            h433.SetBinContent(i+1,sum433/num433)
            totnum433+=num433
            totsum433+=sum433
            
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlue) #ROOT.kBlack)
    h422.SetLineWidth(2)
    h422.SetMarkerColor(ROOT.kBlue) #ROOT.kBlack)
    h422.SetMarkerStyle(8)
    h422.SetMarkerSize(1)
    '''
    h4225.SetLineColor(ROOT.kOrange+1)
    h4225.SetLineWidth(2)
    h4225.SetMarkerColor(ROOT.kOrange+1)
    h4225.SetMarkerStyle(8)
    h4225.SetMarkerSize(1)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h423.SetMarkerColor(ROOT.kBlue)
    h423.SetMarkerStyle(8)
    h423.SetMarkerSize(1)
    h432.SetLineColor(ROOT.kRed)
    h432.SetLineWidth(2)
    h432.SetMarkerColor(ROOT.kRed)
    h432.SetMarkerStyle(8)
    h432.SetMarkerSize(1)
    '''
    h433.SetLineColor(ROOT.kRed) #ROOT.kGreen+3)
    h433.SetLineWidth(2)
    h433.SetMarkerColor(ROOT.kRed) #ROOT.kGreen+3)
    h433.SetMarkerStyle(8)
    h433.SetMarkerSize(1)

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h433.GetMaximum(), )*1.5 #h423.GetMaximum(), h432.GetMaximum(),  h4225.GetMaximum())*1.5
                    
    #rat423 = h423.Clone()
    #rat4225 = h4225.Clone()
    #rat432 = h432.Clone()
    rat433 = h433.Clone()
    h422.Draw("")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    YTitle = "avg. nSigJets"
    if varName == "nSigJets-nMatchedTruthSigJets":
        #YTitle = "avg. (nSigJets-nMatchedTruthSigJets)"
        YTitle = "avg. no. Pileup Jets"
    h422.GetYaxis().SetTitle(YTitle)
    h422.SetYTitle(YTitle)
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetLabelOffset(5)
    
    #h423.Draw("same hist")
    #h4225.Draw("same hist")
    #h432.Draw("same hist")
    h433.Draw("same")
    
    if totnum422==0:
        totnum422=1
    '''
    if totnum4225==0:
        totnum4225=1
    if totnum423==0:
        totnum423=1
    if totnum432==0:
        totnum432=1
    '''
    if totnum433==0:
        totnum433=1
    #legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1 = ROOT.TLegend(0.55, 0.7, 0.93, 0.87)
    legend1.SetHeader("              sample,    total avg.")
    legend1.AddEntry(h422, "Nominal, {:.3f}".format(totsum422/totnum422), "l")
    '''
    legend1.AddEntry(h4225, "20J2-25J3, {:.3f}".format(totsum4225/totnum4225), "l")
    legend1.AddEntry(h423, "20J2-30J3, {:.3f}".format(totsum423/totnum423), "l")
    legend1.AddEntry(h432, "30J2-20J3, {:.3f}".format(totsum432/totnum432), "l")
    '''
    legend1.AddEntry(h433, "30GeV pT cut, {:.3f}".format(totsum433/totnum433), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.04)
    t.SetTextAlign(4)
    #t.DrawLatex(0.15, 0.85, canName)
    #t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")
    t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    t.DrawLatex(0.15, 0.75, "#it{#bf{0L, 2+ Jets, 2 b-tag}}")
    t.DrawLatex(0.15, 0.70, "#it{#bf{p_{T}^{V} > 150, SR}}")
            
    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)
    '''
    rat423.Divide(h422)
    rat4225.Divide(h422)
    rat432.Divide(h422)
    '''
    rat433.Divide(h422)
    rat433.SetTitle("")
    rat433.GetXaxis().SetTitle("")
    rat433.GetXaxis().SetTitleSize(0.09)
    rat433.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.5
    if varName == "nSigJets-nMatchedTruthSigJets":
        yLow=0
        yHigh=1.1
    rat433.SetLineWidth(2)
    rat433.SetLineColor(ROOT.kRed)
    rat433.SetAxisRange(yLow, yHigh, "Y")
    rat433.GetXaxis().SetLabelSize(0.09)
    rat433.GetXaxis().SetTitle("#mu")
    rat433.GetYaxis().SetLabelSize(0.07)
    rat433.GetYaxis().SetTitleOffset(0.7)
    rat433.GetYaxis().SetTitleSize(0.06)
    rat433.GetYaxis().SetTitle("Cuts/Orig")
    rat433.GetYaxis().SetNdivisions(506)
    
    rat433.Draw("hist") 
    '''
    rat423.Draw("same hist") 
    rat4225.Draw("same hist") 
    rat432.Draw("same hist") 
    '''
    if saveHistos:
        #canvas_hs.SaveAs(working_dir+"cutsCompare/"+sample+"/CutsCompare_"+canName+"_"+varName+"_2D.png")
        canvas_hs.SaveAs(working_dir+"CutsCompare_"+canName+"_"+varName+"_2D.png")
    canvas_hs.Close()

def makeTTBarDecay(varName,binning,canName):
    print("TTBarDecay")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    
    h422=ROOT.TH1F("TTBarDecay422","TTBarDecay422",12,0,12)
    h4225=ROOT.TH1F("TTBarDecay4225","TTBarDecay4225",12,0,12)
    h423=ROOT.TH1F("TTBarDecay423","TTBarDecay423",12,0,12)
    h433=ROOT.TH1F("TTBarDecay433","TTBarDecay433",12,0,12)
    h432=ROOT.TH1F("TTBarDecay432","TTBarDecay432",12,0,12)

    hist_file422.cd()
    histName="_"+canName+"_"+varName
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1 = hist_file422.Get(histName1)
        h422.Add(hist_tmp1.Clone())

    hist_file4225.cd()
    histName=histName.replace("20J3","25J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1a = hist_file4225.Get(histName1)
        h4225.Add(hist_tmp1a.Clone())

    hist_file423.cd()
    histName=histName.replace("25J3","30J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1a = hist_file423.Get(histName1)
        h423.Add(hist_tmp1a.Clone())

    hist_file433.cd()
    histName=histName.replace("20J2","30J2")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1c = hist_file433.Get(histName1)
        h433.Add(hist_tmp1c.Clone())

    hist_file432.cd()
    histName=histName.replace("30J3","20J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1b = hist_file432.Get(histName1)
        h432.Add(hist_tmp1b.Clone())

    h422.SetBinContent(11,h422.GetBinContent(2)+h422.GetBinContent(3)+h422.GetBinContent(4))
    h422.SetBinError(11,math.sqrt(h422.GetBinError(2)**2+h422.GetBinError(3)**2+h422.GetBinError(4)**2))
    h422.SetBinContent(12,h422.GetBinContent(5)+h422.GetBinContent(6)+h422.GetBinContent(7)+h422.GetBinContent(8)+h422.GetBinContent(9)+h422.GetBinContent(10))
    h422.SetBinError(12,math.sqrt(h422.GetBinError(5)**2+h422.GetBinError(6)**2+h422.GetBinError(7)**2+h422.GetBinError(8)**2+h422.GetBinError(9)**2+h422.GetBinError(10)**2))

    h4225.SetBinContent(11,h4225.GetBinContent(2)+h4225.GetBinContent(3)+h4225.GetBinContent(4))
    h4225.SetBinError(11,math.sqrt(h4225.GetBinError(2)**2+h4225.GetBinError(3)**2+h4225.GetBinError(4)**2))
    h4225.SetBinContent(12,h4225.GetBinContent(5)+h4225.GetBinContent(6)+h4225.GetBinContent(7)+h4225.GetBinContent(8)+h4225.GetBinContent(9)+h4225.GetBinContent(10))
    h4225.SetBinError(12,math.sqrt(h4225.GetBinError(5)**2+h4225.GetBinError(6)**2+h4225.GetBinError(7)**2+h4225.GetBinError(8)**2+h4225.GetBinError(9)**2+h4225.GetBinError(10)**2))

    h423.SetBinContent(11,h423.GetBinContent(2)+h423.GetBinContent(3)+h423.GetBinContent(4))
    h423.SetBinError(11,math.sqrt(h423.GetBinError(2)**2+h423.GetBinError(3)**2+h423.GetBinError(4)**2))
    h423.SetBinContent(12,h423.GetBinContent(5)+h423.GetBinContent(6)+h423.GetBinContent(7)+h423.GetBinContent(8)+h423.GetBinContent(9)+h423.GetBinContent(10))
    h423.SetBinError(12,math.sqrt(h423.GetBinError(5)**2+h423.GetBinError(6)**2+h423.GetBinError(7)**2+h423.GetBinError(8)**2+h423.GetBinError(9)**2+h423.GetBinError(10)**2))

    h433.SetBinContent(11,h433.GetBinContent(2)+h433.GetBinContent(3)+h433.GetBinContent(4))
    h433.SetBinError(11,math.sqrt(h433.GetBinError(2)**2+h433.GetBinError(3)**2+h433.GetBinError(4)**2))
    h433.SetBinContent(12,h433.GetBinContent(5)+h433.GetBinContent(6)+h433.GetBinContent(7)+h433.GetBinContent(8)+h433.GetBinContent(9)+h433.GetBinContent(10))
    h433.SetBinError(12,math.sqrt(h433.GetBinError(5)**2+h433.GetBinError(6)**2+h433.GetBinError(7)**2+h433.GetBinError(8)**2+h433.GetBinError(9)**2+h433.GetBinError(10)**2))

    h432.SetBinContent(11,h432.GetBinContent(2)+h432.GetBinContent(3)+h432.GetBinContent(4))
    h432.SetBinError(11,math.sqrt(h432.GetBinError(2)**2+h432.GetBinError(3)**2+h432.GetBinError(4)**2))
    h432.SetBinContent(12,h432.GetBinContent(5)+h432.GetBinContent(6)+h432.GetBinContent(7)+h432.GetBinContent(8)+h432.GetBinContent(9)+h432.GetBinContent(10))
    h432.SetBinError(12,math.sqrt(h432.GetBinError(5)**2+h432.GetBinError(6)**2+h432.GetBinError(7)**2+h432.GetBinError(8)**2+h432.GetBinError(9)**2+h432.GetBinError(10)**2))
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlack)
    h422.SetLineWidth(2)
    h4225.SetLineColor(ROOT.kOrange+1)
    h4225.SetLineWidth(2)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h432.SetLineColor(ROOT.kRed)
    h432.SetLineWidth(2)
    h433.SetLineColor(ROOT.kGreen+3)
    h433.SetLineWidth(2)
            
    rebinning = int(binning.split(",")[0])
    print(rebinning)
    if rebinning != 0:
        h422.Rebin(rebinning)
        h4225.Rebin(rebinning)
        h423.Rebin(rebinning)
        h432.Rebin(rebinning)
        h433.Rebin(rebinning)


    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h4225.GetMaximum(), h423.GetMaximum(), h432.GetMaximum(), h433.GetMaximum())*1.4
                    
    rat423 = h423.Clone()
    rat4225 = h4225.Clone()
    rat432 = h432.Clone()
    rat433 = h433.Clone()
    h422.Draw("hist")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    h422.GetYaxis().SetTitle("Entries")
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetYTitle("Entries")
    h422.SetLabelOffset(5)
    
    h423.Draw("same hist")
    h4225.Draw("same hist")
    h432.Draw("same hist")
    h433.Draw("same hist")
    
    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
    legend1.AddEntry(h4225, "20J2-25J3, {:.1f}, {:.1f}".format(h4225.GetEntries(),h4225.Integral()), "l")
    legend1.AddEntry(h423, "20J2-30J3, {:.1f}, {:.1f}".format(h423.GetEntries(),h423.Integral()), "l")
    legend1.AddEntry(h432, "30J2-20J3, {:.1f}, {:.1f}".format(h432.GetEntries(),h432.Integral()), "l")
    legend1.AddEntry(h433, "30J2-30J3, {:.1f}, {:.1f}".format(h433.GetEntries(),h433.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)

    rat423.Divide(h422)
    rat4225.Divide(h422)
    rat432.Divide(h422)
    rat433.Divide(h422)
    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)
    ind=0
    for label in ["j-j","j-e","j-mu","j-tau","e-e","mu-mu","tau-tau","e-mu","e-tau","mu-tau","SemiLep","DiLep"]:
        rat423.GetXaxis().SetBinLabel(ind+1, label)
        ind+=1

    yLow = 0.5
    yHigh = 4
    if "4jet" in canName:
        yHigh=2.2
    elif "5pjet" in canName:
        yLow = 0
        yHigh = 1.2
    elif "4jet" in canName:
        yHigh=2.6
    elif "2pjet" in canName:
        yHigh=1.2
    
    rat423.SetLineWidth(2)
    rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle(varName)
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("p") 
    rat433.Draw("same p") 
    rat4225.Draw("same p") 
    rat432.Draw("same p") 
    
    if saveHistos:
        canvas_hs.SaveAs(working_dir+"cutsCompare/CutsCompare_"+canName+"_"+varName+".png")
    canvas_hs.Close()

def doNJetMigration(varName,canName,F):
    # signal_2pjet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    for sampl in samples:
        histograms={}
        histograms2={"2jet":0,"3jet":0,"4jet":0,"5pjet":0}
        F.cd()
        for samp in samples[sampl]:
            histName1=samp+"_"+canName+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp111 = F.Get(histName1)
            histograms[samp] = ROOT.TH1F(samp+"_"+canName,samp+"_"+canName,10,0,10)            
            histograms[samp].SetBinContent(1,hist_tmp111.GetBinContent(23))
            histograms[samp].SetBinContent(2,hist_tmp111.GetBinContent(33))
            histograms[samp].SetBinContent(3,hist_tmp111.GetBinContent(43))
            histograms[samp].SetBinContent(4,hist_tmp111.GetBinContent(53)+hist_tmp111.GetBinContent(63)+hist_tmp111.GetBinContent(73)+hist_tmp111.GetBinContent(83)+hist_tmp111.GetBinContent(93)+hist_tmp111.GetBinContent(103))
            histograms[samp].SetBinContent(5,hist_tmp111.GetBinContent(34))
            histograms[samp].SetBinContent(6,hist_tmp111.GetBinContent(44))
            histograms[samp].SetBinContent(7,hist_tmp111.GetBinContent(54)+hist_tmp111.GetBinContent(64)+hist_tmp111.GetBinContent(74)+hist_tmp111.GetBinContent(84)+hist_tmp111.GetBinContent(94)+hist_tmp111.GetBinContent(104))
            histograms[samp].SetBinContent(8,hist_tmp111.GetBinContent(45))
            histograms[samp].SetBinContent(9,hist_tmp111.GetBinContent(55)+hist_tmp111.GetBinContent(65)+hist_tmp111.GetBinContent(75)+hist_tmp111.GetBinContent(85)+hist_tmp111.GetBinContent(95)+hist_tmp111.GetBinContent(105))
        hist_file422.cd()
        histDiv=ROOT.TH1F(sampl+"_"+canName+"_nTrkJets_reduceDiv",sampl+"_"+canName+"_nTrkJets_reduceDiv",10,0,10)            
        for samp in samples[sampl]:
            histName1=samp+"_"+canName+"_nSigJets"
            histName2=histName1.replace("20J2_30J3","20J2_20J3")
            #histName2=histName1.replace("2pjet","2jet")
            print("histName2 "+histName2)
            hist_tmp1a = hist_file422.Get(histName2).Clone()
            histograms2["2jet"]+=hist_tmp1a.GetBinContent(3)

            #histName2=histName1.replace("2pjet","3jet")
            #print("histName2 "+histName2)
            #hist_tmp2 = hist_file422.Get(histName2).Clone()
            histograms2["3jet"]+=hist_tmp1a.GetBinContent(4)

            #histName2=histName1.replace("2pjet","4jet")
            #hist_tmp3 = hist_file422.Get(histName2).Clone()
            histograms2["4jet"]+=hist_tmp1a.GetBinContent(5)

            #histName2=histName1.replace("2pjet","5pjet")
            #hist_tmp4 = hist_file422.Get(histName2).Clone()
            histograms2["5pjet"]+=hist_tmp1a.GetBinContent(6)+hist_tmp1a.GetBinContent(7)+hist_tmp1a.GetBinContent(8)+hist_tmp1a.GetBinContent(9)+hist_tmp1a.GetBinContent(10)
        histDiv.SetBinContent(1,histograms2["2jet"])
        histDiv.SetBinContent(2,histograms2["3jet"])
        histDiv.SetBinContent(3,histograms2["4jet"])
        histDiv.SetBinContent(4,histograms2["5pjet"])
        histDiv.SetBinContent(5,histograms2["3jet"])
        histDiv.SetBinContent(6,histograms2["4jet"])
        histDiv.SetBinContent(7,histograms2["5pjet"])
        histDiv.SetBinContent(8,histograms2["4jet"])
        histDiv.SetBinContent(9,histograms2["5pjet"])

        if sampl == "signal":
            for i in range(9):
                print("actual: {} divider: {} ratio: {}".format(histograms["signal"].GetBinContent(i+1),histDiv.GetBinContent(i+1),histograms["signal"].GetBinContent(i+1)/histDiv.GetBinContent(i+1)))
        
        i=0
        ROOT.gStyle.SetPadTickX(1)
        ROOT.gStyle.SetPadTickY(1)

        hs = ROOT.THStack("hs",";;Entries")
        for samp in samples[sampl]:
            if sampl != "data":
                histograms[samp].SetLineColor(colours[samp])
                histograms[samp].SetFillColor(colours[samp])
            else:
                histograms[samp].SetLineColor(colours[samp])
                histograms[samp].SetMarkerColor(colours[samp])
                histograms[samp].SetLineWidth(2)
                histograms[samp].SetMarkerStyle(8)
                histograms[samp].SetMarkerSize(1)
                for i in range(histograms[samp].GetNbinsX()):
                    if histograms[samp].GetBinContent(i+1)>0:
                        histograms[samp].SetBinError(i+1,math.sqrt(histograms[samp].GetBinContent(i+1)))

            hs.Add(histograms[samp])
    
        histNum=hs.GetStack().Last().Clone()
        histDraw=hs.GetStack().First().Clone()
        y_max = histNum.GetMaximum()*1.4
        histNum.Divide(histDiv)
        
        canvas_hs = ROOT.TCanvas("DataMC_"+sampl, "DataMC_"+sampl, 900, 900)
        #(x1,y1,x2,y2) 
        upper_hs = ROOT.TPad("upper_hs_"+sampl, "upper_hs_"+sampl, 0.025, 0.345, 0.995, 0.995)
        lower_hs = ROOT.TPad("lower_hs_"+sampl, "lower_hs_"+sampl, 0.025, 0.025, 0.995, 0.4)
        upper_hs.Draw()
    
        lower_hs.Draw()
        lower_hs.cd().SetBottomMargin(0.3)
        lower_hs.SetGridy()
        lower_hs.SetTopMargin(0)
        upper_hs.cd()
        ROOT.gStyle.SetOptStat(0)
        
        
        histDraw.Draw()
        histDraw.SetFillColor(0)
        histDraw.SetLineColor(0)
        histDraw.SetTitle("")
        histDraw.SetAxisRange(0.0, y_max, "Y")
        histDraw.GetYaxis().SetRangeUser(0.0,y_max)
        histDraw.GetYaxis().SetMaxDigits(3)
        histDraw.GetYaxis().SetTitle("Entries")
        histDraw.GetYaxis().SetTitleSize(0.04)
        histDraw.GetYaxis().SetTitleOffset(1.2)
        histDraw.GetYaxis().SetLabelSize(0.05)
        histDraw.SetYTitle("Entries")
        #histDraw.GetYaxis().SetLabelOffset(5)
        hs.Draw("same")
                
        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.03)
        t.SetTextAlign(4)
        t.DrawLatex(0.15, 0.85, canName)
        t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

        legend1 = ROOT.TLegend(0.805, 0.65, 0.93, 0.87)
        if sampl == "data" or sampl == "stop" or sampl == "diboson" or sampl == "stop": 
            legend1 = ROOT.TLegend(0.805, 0.8, 0.93, 0.87)
        legend1.SetBorderSize(0)
        #legend1.SetTextSize(0.08)
        for samp in samples[sampl]:
            if "ZJets" in samp or "WJets" in samp:
                sampleName=samp.replace("Jets","")
                legend1.AddEntry(histograms[samp], sampleName, "f")
            else:
                legend1.AddEntry(histograms[samp], samp, "f")
        legend1.SetFillColor(0)
        legend1.SetFillStyle(0)
        legend1.Draw()

        lower_hs.cd()
        ROOT.gPad.SetTicky(0)

        histNum.SetTitle("")
        histNum.GetXaxis().SetTitle("")
        histNum.GetXaxis().SetTitleSize(0.09)
        histNum.GetXaxis().SetTitleOffset(1.05)
    
        labs=["2-> 2","3-> 2","4-> 2","5p-> 2",
              "3-> 3","4-> 3","5p-> 3","4-> 4","5p-> 4"]
        for i in range(len(labs)): 
            histNum.GetXaxis().SetBinLabel(i+1, labs[i])


        yLow = 0
        yHigh = 1.05
        histNum.SetLineWidth(2)
        histNum.SetLineColor(ROOT.kBlack)
        histNum.SetAxisRange(yLow, yHigh, "Y")
        histNum.GetXaxis().SetLabelSize(0.09)
        histNum.GetXaxis().SetTitle("nSigJet Migrations")
        histNum.GetXaxis().SetTitleSize(0.09)
        histNum.GetXaxis().SetTitleOffset(1.05)
        histNum.GetYaxis().SetRangeUser(yLow,yHigh)
        histNum.GetYaxis().SetLabelSize(0.07)
        histNum.GetYaxis().SetTitleOffset(0.7)
        histNum.GetYaxis().SetTitleSize(0.06)
        histNum.GetYaxis().SetTitle("new cat./old cat.")
        histNum.GetYaxis().SetNdivisions(506)
        
        histNum.Draw("hist") 
        
        plotName = working_dir+"JetMigrations/JetMigrations_"+canName+"_"+sampl
        if saveHistos:
            canvas_hs.SaveAs(plotName+".png")
        canvas_hs.Close()


NUMJETS={"nJets==2":"2jet","nJets==3":"3jet","nJets==4":"4jet"} #,"nJets>=5":"5pjet","nJets>=2":"2pjet"}
#NUMJETS={"nJets==3":"3jet"}
METCUT={"MET>150&&MET<250":"150_250ptv","MET>400":"400ptv","MET>250&&MET<400":"250_400ptv",} #"MET>150":"150ptv","MET>250":"250ptv"}
#METCUT={"MET>150&&MET<250":"150_250ptv"}
REGIONS = ["All"] #"SR","CRLow","CRHigh","All"]
# For flavour, 15=tau, 5=b, 4=c, <4=l
# rebion,start,end
histNames = {"mBB" : "5,0,400", 
             "pTB1" : "2,40,500", "pTB2" : "2,20,400", "pTJ3" : "2,20,400",
             "sumPtJets":"2,0,500", 
             "nJets" : "0,0,10", "nSigJets" : "0,0,10",
             "mva" : "25,-1,1",
             "dRBB" : "50,0,5", 
             "MET" : "45,150,600",}

# rebin,start,end
#histNames = {"nJets" : "0,0,10","GSCptJ1" : "2,40,500", "mBB" : "0,0,400"} #, "GSCMbb" : "40,0,400"}
#histNames = {"Njets_truth_pTjet30" : "0,0,10"}
histNames = {"mva" : "25,-1,1",} #"mBB" : "10,0,300",} #"mva" : "25,-1,1",} #
#histNames = {"pTB1" : "2,40,500", "nTrkJets" : "0,0,10"}
ROOT.gROOT.SetBatch(True)
# signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
#main
#sys.argv.append("-b")
histNamesOrder = histNames.keys()
histNamesOrder.sort()

#histNamesOrder=[]

print("Making plots")
for hName in histNamesOrder:
    m_sigValsTot={}
    for numJets in NUMJETS:
        if "J3" in hName:
            if numJets == "nJets==2":
                continue
        for METCut in METCUT:
            m_sigVals={}
            m_sigValsMVA={}
            m_yieldVals={}
            m_SBVals={}
            for Reg in REGIONS:
                #if not ("dRBB" in hName or "pTB1" in hName):
                #    continue
                sys.argv.append("-b")
                ROOT.gStyle.SetPadTickX(1)
                ROOT.gStyle.SetPadTickY(1)

                #ttbar_2btag2jet_400ptv_CRLow_mBB
                histEx="_2btag"+NUMJETS[numJets]+"_"+METCUT[METCut]+"_"+Reg+"_"+hName
                print("Looking at "+histEx)
                ROOT.gROOT.SetBatch(True)

                binning = histNames[hName]
                cutsComparison(hist_fileTTd,hist_fileTTnd,binning,"ttbar"+histEx)
                #cutsComparison(hist_fileSd,hist_fileSnd,binning,"stop"+histEx)
          
hist_fileTTd.Close()
hist_fileTTnd.Close()
hist_fileSd.Close()
hist_fileSnd.Close()

print("")
print("")
print("")
print("All complete")
#exit()
