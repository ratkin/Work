import os
import sys
import ROOT
import re
import copy

sys.argv.append("-b")

ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)

# python METtrig.py a

#per = "all"
per = sys.argv[1]
JType = "pf19"
E1Name = "Condor/test_outputs/em18_33-01_0L/em18_0L_33-01.root"
E2Name = "Condor/noTrigger_outputs/em18_33-01_0L/em18_0L_33-01.root"
P1Name = "Condor/test_outputs/{}_33-01_0L/{}_0L_33-01.root".format(JType,JType)
P2Name = "Condor/noTrigger_outputs/{}_33-01_0L/{}_0L_33-01.root".format(JType,JType)

met_weights = ["MET2j_w","MET3j_w","MET4j_w","MET2j","MET3j","MET4j"]

if per != "all":
    E1Name = "Condor/test_outputs/em18_33-01_0L/em18_0L_{}_33-01.root".format(per)
    E2Name = "Condor/noTrigger_outputs/em18_33-01_0L/em18_0L_{}_33-01.root".format(per)
    P1Name = "Condor/test_outputs/{}_33-01_0L/{}_0L_{}_33-01.root".format(JType,JType,per)
    P2Name = "Condor/noTrigger_outputs/{}_33-01_0L/{}_0L_{}_33-01.root".format(JType,JType,per)

work_dir = "histograms/weightsPerMET/"

E1 = ROOT.TFile.Open(E1Name)
E2 = ROOT.TFile.Open(E2Name)
P1 = ROOT.TFile.Open(P1Name)
P2 = ROOT.TFile.Open(P2Name)


for hName in met_weights:
    print("looking at {}".format(hName))
    E1.cd()
    HE1 = E1.Get(hName).Clone()
    E2.cd()
    HE2 = E2.Get(hName).Clone()
    P1.cd()
    HP1 = P1.Get(hName).Clone()
    P2.cd()
    HP2 = P2.Get(hName).Clone()

    HE1.Rebin(2)
    HE2.Rebin(2)
    HP1.Rebin(2)
    HP2.Rebin(2)

    canvas = ROOT.TCanvas(hName+" compare", hName+" compare", 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.65, 0.87, 0.87)
    ROOT.gStyle.SetOptStat(0)
    
    HE1.Divide(HE2)
    HP1.Divide(HP2)

    HE1.Draw("hist")
    HP1.Draw("hist same")
    
    R2 = HP1.Clone()
    
    y_max = max(HE1.GetMaximum(), HP1.GetMaximum())
    y_min = min(HE1.GetMinimum(), HP1.GetMinimum())
    HE1.SetAxisRange(y_min / 1.2, y_max * 1.2, "Y")    
    HE1.SetTitle("")
    HE1.GetYaxis().SetMaxDigits(3)
    HE1.GetYaxis().SetTitle("# pass / # pass except trigger")
    HE1.GetYaxis().SetTitleSize(0.04)
    HE1.GetYaxis().SetTitleOffset(1.2)
    HE1.SetLineColor(ROOT.kBlue)
    HE1.SetLineWidth(2)
    HE1.SetLabelOffset(5)
    
    HP1.SetLineColor(ROOT.kRed)
    HP1.SetLineWidth(2)
    
    R2.SetLineColor(ROOT.kBlack)
    R2.SetLineWidth(2)
    
    legend.SetBorderSize(0)
    legend.AddEntry(HE1, "em18 33-01", "l")
    legend.AddEntry(HP1, JType+" 33-01", "l")
    legend.Draw()
    
    lower.cd()
    R2.Divide(HE1)
    
    R2.SetTitle("")
    R2.GetXaxis().SetTitle("MET (GeV)")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    dy = 0.05
    R2.SetAxisRange(1-dy, 1+dy, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("pf/em")
    R2.GetYaxis().SetNdivisions(506)
    
    R2.Draw("hist")
    
    canvas.SaveAs(work_dir+"trigRatio_{}_{}_{}.png".format(hName,per,JType))

