"""
Runs over histograms
"""
import os
import sys
import ROOT
import math
import copy
from array import array
from decimal import Decimal
#import argparse

# run as: a is the period (use all for combination), em18 is the jet type for 33-01 (leave blank if doing special tests)
# python Validation_AllSamples.py all >& ValidationSignificances.txt   for when wanting the significances
# python Validation_AllSamples.py all >& Validation_AllSamples.log

per = sys.argv[1]
JType = "pf19"

sys.argv.append("-b")

print ("Opening files")
File1=""
File2=""

FileLoc1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-MPT/hadds/"
FileLoc2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-noNjetCut/hadds/"
'''
if per=="all":
    File1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-MPT/hadds/hist-Total.root"
    File2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-noNjetCut/hadds/hist-Total.root"
else:
    File1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-MPT/hadds/hist-Total-{}.root".format(per)
    File2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-noNjetCut/hadds/hist-Total-{}.root".format(per)
'''
if per=="all":
    File1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-METstudies/hadds/hist-VH.root"
    File2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-METstudies/hadds/hist-VH.root"
else:
    File1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-METstudies/hadds/hist-VH-{}.root".format(per)
    File2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-METstudies/hadds/hist-VH-{}.root".format(per)

#extra_arg = "_{}_{}_{}".format("old" if oldOrNew1=="" else oldOrNew1,"old" if oldOrNew2=="" else oldOrNew2,JType)
extra_arg = ""
VTag = "32-15"
VTag2 = "33-05"

#working_dir = "histograms/validation/{}/".format(JType)
#if JType == "":
# this needs the forward slash at the end
#working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/ValidationPlots/"
working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-METstudies/plots/"
print("Output directory: "+working_dir)
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)


dRBBRegions = ["SR","CRLow","CRHigh"]
JetRegions = ["2tag2jet","2tag3jet","2tag4jet","0ptag2jet","0ptag3jet","0ptag4jet"] #"2tag5pjet",]
ptvRegions = ["150_250ptv","250ptv"]
ranges150 = {"mBB":[0,300,10],"MET":[100,300,0],"dPhiVBB":[2,3.2,2],"dRBB":[0,4,2],"softMET":[0,100,0],
             "pTB1":[0,400,2],"HT":[200,1000,5],"pTB2":[0,200,0],"dPhiBB":[0,2.6,4],"dEtaBB":[0,3.6,4],
             "sumPtJets":[0,500,2],"pTBB":[0,400,2],"pTBBJ":[0,400,2],"pTJ3":[0,300,2],"mBBJ":[0,500,4],
             "nJ":[0,10,0],"METOverSqrtHT":[0,1000,10],"METSig":[0,30,10],
             "MPT":[0,300,0],"dPhiMETMPT":[0,2,4],"MindPhiMETJet":[0,3.2,5],
             "sumET":[100,1500,5],"softSumET":[0,400,0],"metSigPU":[0,30,2],"MPTsumET":[0,1000,5]
}
ranges250 = {"mBB":[0,400,10],"MET":[200,500,0],"dPhiVBB":[2,3.2,2],"dRBB":[0,4,2],"softMET":[0,100,0],
             "pTB1":[0,500,2],"HT":[300,1000,5],"pTB2":[0,300,0],"dPhiBB":[0,2.6,4],"dEtaBB":[0,3.6,4],
             "sumPtJets":[100,700,2],"pTBB":[0,500,2],"pTBBJ":[100,500,2],"pTJ3":[0,300,2],"mBBJ":[0,500,4],
             "nJ":[0,10,0],"METOverSqrtHT":[0,1000,10],"METSig":[0,30,10],
             "MPT":[0,300,0],"dPhiMETMPT":[0,2,4],"MindPhiMETJet":[0,3.2,5],
             "sumET":[100,1500,5],"softSumET":[0,400,0],"metSigPU":[0,30,2],"MPTsumET":[0,1000,5]
}

#samples = {"Wbb" : ["W+bb",ROOT.Green + 3],"Wbc" : ["W+bc",ROOT.Green + 4],"Wbl" : ["W+bl",ROOT.Green + 2],"Wcl" : ["W+cl",ROOT.Green - 6],"Wcc" : ["W+cc",ROOT.Green + 1],"Wl" : ["W+l",ROOT.Green - 9],
#           "Zbb" : ["Z+bb",ROOT.Azure + 2],"Zbc" : ["Z+bc",ROOT.Azure + 3],"Zcc" : ["Z+cc",ROOT.Azure - 4],"Zbl" : ["Z+bl",ROOT.Azure + 1],"Zcl" : ["Z+cl",ROOT.Azure - 8],"Zl" : ["Z+l",ROOT.Azure - 9],
#           "ttbar" : ["ttbar",ROOT.Orange],"stop" : ["stop",ROOT.Yellow - 7],"diboson" : ["Diboson",ROOT.Gray],"data" : ["Data",1],"signal" : ["VH 125", ROOT.Red]}

#samples = ["WJets","Wbb","Wheavy","Wlight","ZJets","Zbb","Zheavy","Zlight","ttbar","stop","diboson","data","signal"]
samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}
colours = {"signal" : ROOT.kRed, "WJets" : ROOT.kGreen+3, "ZJets" : ROOT.kAzure+1, "ttbar" : ROOT.kOrange, "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

#histNames=[]
cutFlowNames = ["CutsResolved","CutsResolved_NoWeight","PreselectionCutFlow"]
cutFlowSamples=[]
if per == "all":
    cutFlowSamples = ["data","stop","VH","ZJets","diboson","ttbar","WJets"] #,"OldSig"]
else:
    cutFlowSamples = ["data-{}".format(per),"stop-{}".format(per),"VH-{}".format(per),"ZJets-{}".format(per),
                      "diboson-{}".format(per),"ttbar-{}".format(per),"WJets-{}".format(per)] #,"OldSig-{}".format(per)] #,"ttbar_nonallhad-{}".format(per),"ttbar_nonallhad_METfilt-{}".format(per)]

cutFlowSamples=[]
#samples = {}
doTables=0

#f3 = ROOT.TFile.Open(File3)

def getHistos(sample,F,histo,vtag):
    F.cd()
    H = ROOT.TH1F()
    i=0
    for samp in samples[sample]:
        if i==0:
            try:
                H=F.Get(samp+"_"+histo).Clone()
            except:
                print(samp+"_"+histo+" does not exist for "+vtag)
                i=-1
            i+=1
        else:
            try:
                H.Add(F.Get(samp+"_"+histo).Clone())
            except:
                print(samp+"_"+histo+" does not exist for "+vtag)

    return H

def getSAndBHists(F,histo,vtag,rebins):
    F.cd()
    S = ROOT.TH1F()
    i=0
    for samp in samples["signal"]:
        if i==0:
            try:
                S=F.Get(samp+"_"+histo).Clone()
            except:
                print(samp+"_"+histo+" does not exist for "+vtag)
                i=-1
            i+=1
        else:
            try:
                S.Add(F.Get(samp+"_"+histo).Clone())
            except:
                print(samp+"_"+histo+" does not exist for "+vtag)
    B = ROOT.TH1F()
    i=0
    for sampl in ["ttbar","ZJets","WJets","diboson","stop"]:
        for samp in samples[sampl]:
            if i==0:
                try:
                    B=F.Get(samp+"_"+histo).Clone()
                except:
                    print(samp+"_"+histo+" does not exist for "+vtag)
                    i=-1
                i+=1
            else:
                try:
                    B.Add(F.Get(samp+"_"+histo).Clone())
                except:
                    print(samp+"_"+histo+" does not exist for "+vtag)
                    
    if rebins[2]!=0:
        S.Rebin(rebins[2])
        B.Rebin(rebins[2])

    return S,B
    
def getIntegralAndError(h):
    i=1
    integ=0
    err=0
    while i<=h.FindLastBinAbove(0):
        integ+=h.GetBinContent(i)
        err=math.sqrt(err**2+h.GetBinError(i)**2)
        i+=1
    return integ,err

def getSignificance(S,B,vtag):
    i = S.FindBin(50)
    binMax = S.FindBin(250)
    summ=0
    while i <= binMax:
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if b!=0:
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
                        
def do4pSig(F,histo,vtag,rebins):
    histo2 = histo.replace("5p","4")
    F.cd()
    S1 = ROOT.TH1F()
    i=0
    for samp in samples["signal"]:
        for his in [histo,histo2]:
            if i==0:
                try:
                    S1=F.Get(samp+"_"+his).Clone()
                except:
                    print(samp+"_"+his+" does not exist for "+vtag)
                    i=-1
                i+=1
            else:
                try:
                    S1.Add(F.Get(samp+"_"+his).Clone())
                except:
                    print(samp+"_"+his+" does not exist for "+vtag)
    B1 = ROOT.TH1F()
    i=0
    for sampl in ["ttbar","ZJets","WJets","diboson","stop"]:
        for samp in samples[sampl]:
            for his in [histo,histo2]:
                if i==0:
                    try:
                        B1=F.Get(samp+"_"+his).Clone()
                    except:
                        print(samp+"_"+his+" does not exist for "+vtag)
                        i=-1
                    i+=1
                else:
                    try:
                        B1.Add(F.Get(samp+"_"+his).Clone())
                    except:
                        print(samp+"_"+his+" does not exist for "+vtag)
                    
    if rebins[2]!=0:
        S1.Rebin(rebins[2])
        B1.Rebin(rebins[2])

    i = S1.FindBin(50)
    binMax = S1.FindBin(250)
    summ=0
    while i <= binMax:
        s=S1.GetBinContent(i)
        b=B1.GetBinContent(i)
        if b!=0:
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("\nSignificance for {}: {}\n".format(histo,math.sqrt(summ)))

def doNJetComp(F,histo,vtag):
    print("Doing NJetComp")
    histo2 = histo.replace("5p","4")
    histo3 = histo.replace("5p","3")
    histo4 = histo.replace("5p","2")
    histograms = {samp : ROOT.TH1F("h_"+samp,"h_"+samp,11,-0.5,10.5) for samp in samples}
    F.cd()
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    for sampl in histograms: #eg signal
        i=0
        for samp in samples[sampl]: # eg qqZvvH125
            for his in [histo,histo2,histo3,histo4]:
                if i==0:
                    try:
                        histograms[sampl]=F.Get(samp+"_"+his).Clone()
                    except:
                        print(samp+"_"+his+" does not exist for "+vtag)
                        i=-1
                    i+=1
                else:
                    try:
                        histograms[sampl].Add(F.Get(samp+"_"+his).Clone())
                    except:
                        print(samp+"_"+his+" does not exist for "+vtag)
        if sampl != "data":
            histograms[sampl].SetLineColor(colours[sampl])
            histograms[sampl].SetFillColor(colours[sampl])
        else:
            histograms[sampl].SetLineColor(colours[sampl])
            histograms[sampl].SetMarkerColor(colours[sampl])
            histograms[sampl].SetLineWidth(2)
            histograms[sampl].SetMarkerStyle(8)
            histograms[sampl].SetMarkerSize(1)

    hs = ROOT.THStack("hs","")
    hs.Add(histograms["ttbar"])
    hs.Add(histograms["stop"])
    hs.Add(histograms["WJets"])
    hs.Add(histograms["ZJets"])
    hs.Add(histograms["diboson"])
    hs.Add(histograms["signal"])
    
    hd = histograms["data"]
    hb = histograms["ttbar"].Clone()
    hb.Add(histograms["stop"])
    hb.Add(histograms["WJets"])
    hb.Add(histograms["ZJets"])
    hb.Add(histograms["diboson"])
    
    canvas_hs = ROOT.TCanvas("nJetComp", "nJetComp", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.3
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same")
    
    legend1 = ROOT.TLegend(0.605, 0.62, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}, {:.1f}".format(histograms["data"].GetEntries(),histograms["data"].Integral()), "pl")
    legend1.AddEntry(histograms["signal"], "signal, {:.1f}, {:.1f}".format(histograms["signal"].GetEntries(),histograms["signal"].Integral()), "f")
    legend1.AddEntry(histograms["diboson"], "diboson, {:.1f}, {:.1f}".format(histograms["diboson"].GetEntries(),histograms["diboson"].Integral()), "f")
    legend1.AddEntry(histograms["ZJets"], "ZJets, {:.1f}, {:.1f}".format(histograms["ZJets"].GetEntries(),histograms["ZJets"].Integral()), "f")
    legend1.AddEntry(histograms["WJets"], "WJets, {:.1f}, {:.1f}".format(histograms["WJets"].GetEntries(),histograms["WJets"].Integral()), "f")
    legend1.AddEntry(histograms["stop"], "stop, {:.1f}, {:.1f}".format(histograms["stop"].GetEntries(),histograms["stop"].Integral()), "f")
    legend1.AddEntry(histograms["ttbar"], "ttbar, {:.1f}, {:.1f}".format(histograms["ttbar"].GetEntries(),histograms["ttbar"].Integral()), "f")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    
    t.DrawLatex(0.15, 0.8, "ATLAS #it{#bf{Internal}}")
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("nJets")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.5
    yHigh = 0.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("") 

    print(rat.GetBinContent(3))
    print(rat.GetBinContent(4))
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetTitle("")
    rat1.GetXaxis().SetTitle("nJets")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.09)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)


    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.995, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    rat1.Draw("Y+")


    canvas_hs.SaveAs(working_dir+histo+"_"+vtag+"_NJetComp.png")


def doBlinding(h1,h2,histo,vals):
    thresh = 0.1
    S1,B1 = getSAndBHists(f1,histo,"32-15",vals)
    S2,B2 = getSAndBHists(f2,histo,"33-05",vals)
    if "mBB" in histo:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        getSignificance(S1,B1,"32-15")
        getSignificance(S2,B2,"33-05")
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            h2.SetBinContent(i, 0)
            h2.SetBinError(i, 0)
            i+=1
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1
        i=1
        while i<=h2.GetNbinsX():
            s=S2.GetBinContent(i)
            b=B2.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h2.SetBinContent(i, 0)
                h2.SetBinError(i, 0)
            i+=1

    return h1,h2

def doInclusiveBlinding(h1,h2,tag,dRBBReg,Var,vals,BTag):
    thresh = 0.1

    hists32S = []
    hists32B = []
    hists33S = []
    hists33B = []

    if BTag:
        hists32S = [ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F()]
        hists32B = [ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F()]
        hists33S = [ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F()]
        hists33B = [ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F(),ROOT.TH1F()]
        
        hists32S[0],hists32B[0] = getSAndBHists(f1,tag+"2jet_150_250ptv_"+dRBBReg+"_"+Var,"32-15",vals)
        hists32S[1],hists32B[1] = getSAndBHists(f1,tag+"2jet_250ptv_"+dRBBReg+"_"+Var,"32-15",vals)
        hists32S[2],hists32B[2] = getSAndBHists(f1,tag+"3jet_150_250ptv_"+dRBBReg+"_"+Var,"32-15",vals)
        hists32S[3],hists32B[3] = getSAndBHists(f1,tag+"3jet_250ptv_"+dRBBReg+"_"+Var,"32-15",vals)
        
        hists33S[0],hists33B[0] = getSAndBHists(f2,tag+"2jet_150_250ptv_"+dRBBReg+"_"+Var,"33-05",vals)
        hists33S[1],hists33B[1] = getSAndBHists(f2,tag+"2jet_250ptv_"+dRBBReg+"_"+Var,"33-05",vals)
        hists33S[2],hists33B[2] = getSAndBHists(f2,tag+"3jet_150_250ptv_"+dRBBReg+"_"+Var,"33-05",vals)
        hists33S[3],hists33B[3] = getSAndBHists(f2,tag+"3jet_250ptv_"+dRBBReg+"_"+Var,"33-05",vals)
    else:
        hists32S = [ROOT.TH1F(),ROOT.TH1F()]
        hists32B = [ROOT.TH1F(),ROOT.TH1F()]
        hists33S = [ROOT.TH1F(),ROOT.TH1F()]
        hists33B = [ROOT.TH1F(),ROOT.TH1F()]
        
        hists32S[0],hists32B[0] = getSAndBHists(f1,tag+"_150_250ptv_"+dRBBReg+"_"+Var,"32-15",vals)
        hists32S[1],hists32B[1] = getSAndBHists(f1,tag+"_250ptv_"+dRBBReg+"_"+Var,"32-15",vals)
        
        hists33S[0],hists33B[0] = getSAndBHists(f2,tag+"_150_250ptv_"+dRBBReg+"_"+Var,"33-05",vals)
        hists33S[1],hists33B[1] = getSAndBHists(f2,tag+"_250ptv_"+dRBBReg+"_"+Var,"33-05",vals)

    S1 = hists32S[0].Clone()
    B1 = hists32B[0].Clone()
    S2 = hists33S[0].Clone()
    B2 = hists33B[0].Clone()
    S1.Reset()
    B1.Reset()
    S2.Reset()
    B2.Reset()
    list32S = ROOT.TList()
    list32B = ROOT.TList()
    list33S = ROOT.TList()
    list33B = ROOT.TList()

    for hist in hists32S:
        try:
            list32S.Add(hist.Clone())
        except:
            print("skip")
    for hist in hists32B:
        try:
            list32B.Add(hist.Clone())
        except:
            print("skip")
    for hist in hists33S:
        try:
            list33S.Add(hist.Clone())
        except:
            print("skip")
    for hist in hists33B:
        try:
            list33B.Add(hist.Clone())
        except:
            print("skip")
    S1.Merge(list32S)
    B1.Merge(list32B)
    S2.Merge(list33S)
    B2.Merge(list33B)



    if "mBB" in Var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            h2.SetBinContent(i, 0)
            h2.SetBinError(i, 0)
            i+=1
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1
        i=1
        while i<=h2.GetNbinsX():
            s=S2.GetBinContent(i)
            b=B2.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h2.SetBinContent(i, 0)
                h2.SetBinError(i, 0)
            i+=1

    return h1,h2

def inclusiveBtag(sample,tag,dRBBReg,Var,f1,f2):
    hists32 = []
    hists33 = []
    hists32.append(getHistos(sample,f1,tag+"2jet_150_250ptv_"+dRBBReg+"_"+Var,"32-15"))
    hists32.append(getHistos(sample,f1,tag+"2jet_250ptv_"+dRBBReg+"_"+Var,"32-15"))
    hists32.append(getHistos(sample,f1,tag+"3jet_150_250ptv_"+dRBBReg+"_"+Var,"32-15"))
    hists32.append(getHistos(sample,f1,tag+"3jet_250ptv_"+dRBBReg+"_"+Var,"32-15"))
    hists33.append(getHistos(sample,f2,tag+"2jet_150_250ptv_"+dRBBReg+"_"+Var,"33-05"))
    hists33.append(getHistos(sample,f2,tag+"2jet_250ptv_"+dRBBReg+"_"+Var,"33-05"))
    hists33.append(getHistos(sample,f2,tag+"3jet_150_250ptv_"+dRBBReg+"_"+Var,"33-05"))
    hists33.append(getHistos(sample,f2,tag+"3jet_250ptv_"+dRBBReg+"_"+Var,"33-05"))

    canvas1 = ROOT.TCanvas(histName, histName, 900, 900)
    #(x1,y1,x2,y2) 
    upper1 = ROOT.TPad("upper1", "upper1", 0.025, 0.345, 0.995, 0.995)
    lower1 = ROOT.TPad("lower1", "lower1", 0.025, 0.025, 0.995, 0.4)
    upper1.Draw()
    lower1.Draw()
    lower1.SetGridy()
    lower1.cd().SetBottomMargin(0.3)
    upper1.cd()
    ROOT.gStyle.SetOptStat(0)

    h1 = hists32[0].Clone()
    h2 = hists33[0].Clone()
    h1.Reset()
    h2.Reset()
    list32 = ROOT.TList()
    list33 = ROOT.TList()

    for hist in hists32:
        try:
            list32.Add(hist.Clone())
        except:
            print("skip")
    for hist in hists33:
        try:
            list33.Add(hist.Clone())
        except:
            print("skip")
    h1.Merge(list32)
    h2.Merge(list33)

    xLow = ranges150[Var][0]
    xHigh = ranges250[Var][1]
    if ranges250[Var][2]!=0:
        h1.Rebin(ranges250[Var][2])
        h2.Rebin(ranges250[Var][2])
                            
    if sample == "data":
        h1, h2 = doInclusiveBlinding(h1,h2,tag,dRBBReg,Var,ranges250[Var],True)

    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.3
                    
    h1.Draw("hist")
    h1.SetTitle("")
    h1.SetAxisRange(0.0, y_max, "Y")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    h1.SetLineColor(ROOT.kBlue)    
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
    h1.GetXaxis().SetRangeUser(xLow,xHigh)
    
    h2.Draw("same")
    h2.SetMarkerColor(ROOT.kRed)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)
    h2.GetXaxis().SetRangeUser(xLow,xHigh)

    legend1 = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend1.SetHeader("              MC,    NEntries,    SoW")
    legend1.AddEntry(h1, "32-15 em18, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
    legend1.AddEntry(h2, "33-05 pf19, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    
    t.DrawLatex(0.15, 0.75, histName)
    t.DrawLatex(0.15, 0.8, "ATLAS #it{#bf{Internal}}")
    
    lower1.cd()
    rat = h2.Clone()
    rat.Divide(h1)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(Var)
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)
    
#    if Var == "mJIncl" or Var == "MindPhiMETJet" or Var == "dPhiMETMPT" or Var == "dPhiMETLeadFatJet":
#        Line = ROOT.TLine(w,0.0,xHigh,y_max/1.3)
#        Line.SetLineWidth(2)
#        Line.SetLineStyle(4)
#        Line.SetLineColor(ROOT.kBlack)
#        Line.Draw()
    
    yLow = 0.75
    yHigh = 1.25
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("33-05/{}".format(VTag))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    #Line.Draw("same")
    rat.GetXaxis().SetRangeUser(xLow,xHigh)
    
    
    canvas1.SaveAs(working_dir+"InclusiveBTag_{}_{}_{}_{}_{}{}.png".format(sample,tag,dRBBReg,Var,per,extra_arg))
    
def inclusiveJet(sample,JetReg,dRBBReg,Var,f1,f2):
    print("doing inclusive jet")
    hists32 = []
    hists33 = []
    hists32.append(getHistos(sample,f1,JetReg+"_150_250ptv_"+dRBBReg+"_"+Var,"32-15"))
    hists32.append(getHistos(sample,f1,JetReg+"_250ptv_"+dRBBReg+"_"+Var,"32-15"))
    hists33.append(getHistos(sample,f2,JetReg+"_150_250ptv_"+dRBBReg+"_"+Var,"33-05"))
    hists33.append(getHistos(sample,f2,JetReg+"_250ptv_"+dRBBReg+"_"+Var,"33-05"))    

    canvas1 = ROOT.TCanvas(histName, histName, 900, 900)
    #(x1,y1,x2,y2) 
    upper1 = ROOT.TPad("upper1", "upper1", 0.025, 0.345, 0.995, 0.995)
    lower1 = ROOT.TPad("lower1", "lower1", 0.025, 0.025, 0.995, 0.4)
    upper1.Draw()
    lower1.Draw()
    lower1.SetGridy()
    lower1.cd().SetBottomMargin(0.3)
    upper1.cd()
    ROOT.gStyle.SetOptStat(0)

    h1 = hists32[0].Clone()
    h2 = hists33[0].Clone()
    h1.Reset()
    h2.Reset()
    list32 = ROOT.TList()
    list33 = ROOT.TList()

    for hist in hists32:
        try:
            list32.Add(hist.Clone())
        except:
            print("skip")
    for hist in hists33:
        try:
            list33.Add(hist.Clone())
        except:
            print("skip")
    h1.Merge(list32)
    h2.Merge(list33)

    xLow = ranges150[Var][0]
    xHigh = ranges250[Var][1]
    if ranges250[Var][2]!=0:
        h1.Rebin(ranges250[Var][2])
        h2.Rebin(ranges250[Var][2])
                            
    if sample == "data":
        h1, h2 = doInclusiveBlinding(h1,h2,JetReg,dRBBReg,Var,ranges250[Var],False)

    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.3
                    
    h1.Draw("hist")
    h1.SetTitle("")
    h1.SetAxisRange(0.0, y_max, "Y")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    h1.SetLineColor(ROOT.kBlue)    
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
    h1.GetXaxis().SetRangeUser(xLow,xHigh)
    
    h2.Draw("same")
    h2.SetMarkerColor(ROOT.kRed)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)
    h2.GetXaxis().SetRangeUser(xLow,xHigh)

    legend1 = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend1.SetHeader("              MC,    NEntries,    SoW")
    legend1.AddEntry(h1, "32-15 em18, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
    legend1.AddEntry(h2, "33-05 pf19, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    
    t.DrawLatex(0.15, 0.75, histName)
    t.DrawLatex(0.15, 0.8, "ATLAS #it{#bf{Internal}}")
    
    lower1.cd()
    rat = h2.Clone()
    rat.Divide(h1)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(Var)
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)
    
#    if Var == "mJIncl" or Var == "MindPhiMETJet" or Var == "dPhiMETMPT" or Var == "dPhiMETLeadFatJet":
#        Line = ROOT.TLine(w,0.0,xHigh,y_max/1.3)
#        Line.SetLineWidth(2)
#        Line.SetLineStyle(4)
#        Line.SetLineColor(ROOT.kBlack)
#        Line.Draw()
    
    yLow = 0.5
    yHigh = 1.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("33-05/{}".format(VTag))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    #Line.Draw("same")
    rat.GetXaxis().SetRangeUser(xLow,xHigh)
    
    
    canvas1.SaveAs(working_dir+"InclusiveJets_{}_{}_{}_{}_{}{}.png".format(sample,JetReg,dRBBReg,Var,per,extra_arg))


print("File1: "+File1)
print("File2: "+File2)

f1 = ROOT.TFile.Open(File1)
f2 = ROOT.TFile.Open(File2)

#MAIN
# per variable plots
for sample in samples:
    if not (sample == "signal"):
        continue
    for JetReg in JetRegions: # 2 
        if "0ptag4jet" not in JetReg: #JetReg != "2tag2jet":
            continue
        for ptvReg in ptvRegions: # 2
            #if ptvReg != "150_250ptv":
            #    continue
            regHistName = sample+"_"+JetReg+"_"+ptvReg+"_dRBBRegions"
            regHist1 = ROOT.TH1F(regHistName+"1",regHistName+"1",3,0,3)
            regHist2 = ROOT.TH1F(regHistName+"2",regHistName+"2",3,0,3)
            for dRBBReg in dRBBRegions: # 3
                #if dRBBReg != "SR":
                #    continue
                for Var in ranges150:
                    #if not Var == "MET": #(Var == "softSumET" or Var == "sumET" or Var == "METSig" or Var == "metSigPU" or Var == "MPTsumET"):
                    #    continue
                        
                    ROOT.gROOT.SetBatch(True)
    
                    histo = JetReg+"_"+ptvReg+"_"+dRBBReg+"_"+Var
                    histName = sample+"_"+JetReg+"_"+ptvReg+"_"+dRBBReg+"_"+Var

                    if "2jet" in JetReg and ("pTBBJ" in Var or "pTJ3" in Var or "mBBJ" in Var):
                        continue
                    #elif "4jet" in JetReg:
                    #    if not (sample == "data" and Var == "mBB" and dRBBReg == "SR"):
                    #        continue
                    elif "5pjet" in JetReg:
                        if (sample == "data" and Var == "mBB" and dRBBReg == "SR"):
                            do4pSig(f2,histo,"33-05",ranges150["mBB"])
                        elif (sample == "data" and Var == "nJ"):
                            doNJetComp(f2,histo,"33-05")
                        continue
                        
                    ROOT.gStyle.SetPadTickX(1)
                    ROOT.gStyle.SetPadTickY(1)
                    print("looking at "+histName)

                    if dRBBReg == "SR":
                        inclusiveJet(sample,JetReg,dRBBReg,Var,f1,f2)
                        if JetReg == "2tag3jet":
                            inclusiveBtag(sample,"2tag",dRBBReg,Var,f1,f2)
                        elif JetReg == "0ptag3jet":
                            inclusiveBtag(sample,"0ptag",dRBBReg,Var,f1,f2)
                    if "0ptag" in JetReg:
                        continue
                    
                    hist1 = ROOT.TH1F()
                    hist2 = ROOT.TH1F()
                    if sample=="data":
                        hist1 = f1.Get(histName)
                        hist2 = f2.Get(histName)
                    else:
                        hist1 = getHistos(sample,f1,histo,"32-15")
                        hist2 = getHistos(sample,f2,histo,"33-05")
                        
                    
                    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
                    canvas = ROOT.TCanvas(histName, histName, 900, 900)
                                                         #(x1,y1,x2,y2) 
                    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
                    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)

                    #upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.75, 0.995)
                    #lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.75, 0.4)
                    #right = ROOT.TPad("right", "right", 0.75, 0.345, 0.995, 0.995)

                    upper.Draw()
                    lower.Draw()
                    lower.SetGridy()
                    lower.cd().SetBottomMargin(0.3)
                    upper.cd()
                    ROOT.gStyle.SetOptStat(0)
                    
                    h1 = ROOT.TH1F()
                    h2 = ROOT.TH1F()
                    try:
                        h1 = hist1.Clone()
                    except:
                        print("h1 doesn't exist, skipping this plot")
                        continue
                    try:
                        h2 = hist2.Clone()
                    except:
                        print("h2 doesn't exist, skipping this plot")
                        continue

                    #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
                    #h1.Add(hist1a.Clone())
                    #h2.Add(hist2a.Clone())
                    #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
                    vals=[]
                    if "150_250ptv" in ptvReg:
                        vals = ranges150[Var]
                    else:
                        vals = ranges250[Var]
                    
                    xLow = 0
                    xHigh = 500
                    
                    xLow = vals[0]
                    xHigh = vals[1]
                    if vals[2]!=0:
                        h1.Rebin(vals[2])
                        h2.Rebin(vals[2])

                    if sample == "data":
                        h1, h2 = doBlinding(h1,h2,histo,vals)
                            
                    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.3
                    
                    h1.Draw("hist")
                    h1.SetTitle("")
                    h1.SetAxisRange(0.0, y_max, "Y")
                    h1.GetYaxis().SetMaxDigits(3)
                    h1.GetYaxis().SetTitle("Entries")
                    h1.GetYaxis().SetTitleSize(0.04)
                    h1.GetYaxis().SetTitleOffset(1.2)
                    h1.GetYaxis().SetLabelSize(0.05)
                    h1.SetYTitle("Entries")
                    #h1.SetMarkerColor(ROOT.kBlue)
                    #h1.SetMarkerStyle(8)
                    #h1.SetMarkerSize(1.5)
                    h1.SetLineColor(ROOT.kBlue)    
                    h1.SetLineWidth(2)
                    h1.SetLabelOffset(5)
                    h1.GetXaxis().SetRangeUser(xLow,xHigh)
                    
                    h2.Draw("same")
                    h2.SetMarkerColor(ROOT.kRed)
                    #h2.SetMarkerStyle(8)
                    #h2.SetMarkerSize(1.5)
                    h2.SetLineColor(ROOT.kRed)
                    h2.SetLineWidth(2)
                    h2.GetXaxis().SetRangeUser(xLow,xHigh)

                    if sample == "data":
                        S1,B1 = getSAndBHists(f1,histo,"32-15",vals)
                        S2,B2 = getSAndBHists(f2,histo,"33-05",vals)
                        if Var == "mBB":
                            i = h1.FindBin(80)
                            binMax = h1.FindBin(139.9)
                            print("\n\nSIGNIFICANCES for {}".format(histo))
                            getSignificance(S1,B1,"32-15")
                            getSignificance(S2,B2,"33-05")
                            print("\n\n")
                            while i <= binMax:
                                h1.SetBinContent(i, 0)
                                h1.SetBinError(i, 0)
                                h2.SetBinContent(i, 0)
                                h2.SetBinError(i, 0)
                                i+=1
                        else:
                            i=1
                            thresh = 0.1
                            while i<=h1.GetNbinsX():
                                s=S1.GetBinContent(i)
                                b=B1.GetBinContent(i)
                                if s+b == 0:
                                    i+=1
                                    continue
                                elif s/(s+b) > thresh:
                                    h1.SetBinContent(i, 0)
                                    h1.SetBinError(i, 0)
                                i+=1
                            i=1
                            while i<=h2.GetNbinsX():
                                s=S2.GetBinContent(i)
                                b=B2.GetBinContent(i)
                                if s+b == 0:
                                    i+=1
                                    continue
                                elif s/(s+b) > thresh:
                                    h2.SetBinContent(i, 0)
                                    h2.SetBinError(i, 0)
                                i+=1

                    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
                    legend.SetHeader("              MC,    NEntries,    SoW")
                    legend.AddEntry(h1, "32-15 em18, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
                    legend.AddEntry(h2, "33-05 pf19, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
                    legend.SetBorderSize(0)
                    legend.SetFillColor(0)
                    legend.SetFillStyle(0)
                    legend.Draw()
                    
                    t = ROOT.TLatex()
                    t.SetNDC()
                    t.SetTextFont(72)
                    t.SetTextColor(1)
                    t.SetTextSize(0.03)
                    t.SetTextAlign(4)
                    
                    t.DrawLatex(0.15, 0.75, histName)
                    t.DrawLatex(0.15, 0.8, "ATLAS #it{#bf{Internal}}")
                    
                    lower.cd()
                    rat = h2.Clone()
                    rat.Divide(h1)
                    rat.SetTitle("")
                    rat.GetXaxis().SetTitle(Var)
                    rat.GetXaxis().SetTitleSize(0.09)
                    rat.GetXaxis().SetTitleOffset(1.05)
                    
                    #Line = ROOT.TLine(xLow,1.,xHigh,1.)
                    #Line.SetLineWidth(2)
                    #Line.SetLineColor(ROOT.kBlack)
                    yLow = 0.5
                    yHigh = 1.5
                    rat.SetLineWidth(2)
                    rat.SetLineColor(ROOT.kBlack)
                    rat.SetAxisRange(yLow, yHigh, "Y")
                    rat.GetXaxis().SetLabelSize(0.09)
                    rat.GetYaxis().SetLabelSize(0.07)
                    rat.GetYaxis().SetTitleOffset(0.7)
                    rat.GetYaxis().SetTitleSize(0.06)
                    rat.GetYaxis().SetTitle("33-05/{}".format(VTag))
                    rat.GetYaxis().SetNdivisions(506)
                    
                    rat.Draw("")
                    #Line.Draw("same")
                    rat.GetXaxis().SetRangeUser(xLow,xHigh)
                    
    
                    canvas.SaveAs(working_dir+histName+"_{}{}.png".format(per,extra_arg))
                    #end of Vars
                #end of dRBBReg
            #doDRBBRegions(regHistName,regHist1,regHist2,regHistVals1,regHistVals2)
            #end of ptv
        #end of jet
    # end of sample

            
# cutflows
for sample in cutFlowSamples:
    #if "ttbar" not in sample:
    #    continue
    f1 = ROOT.TFile.Open(FileLoc1+"/hist-"+sample+".root")
    f2 = ROOT.TFile.Open(FileLoc2+"/hist-"+sample+".root")
    for hName in cutFlowNames:
        ROOT.gStyle.SetPadTickX(1)
        ROOT.gStyle.SetPadTickY(1)
        ROOT.gROOT.SetBatch(True)

        hist1 = ROOT.TH1D()
        hist2 = ROOT.TH1D()
        if "Preselection" in hName:
            hist1 = f1.Get("CutFlow/"+hName)
            hist2 = f2.Get("CutFlow/"+hName)
        else:
            hist1 = f1.Get("CutFlow/Nominal/"+hName)
            hist2 = f2.Get("CutFlow/Nominal/"+hName)
        
        print("looking at "+hName)
        
        canvas = ROOT.TCanvas(hName, hName, 900, 900)
        upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
        lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
        upper.Draw()
        lower.Draw()
        lower.SetGridy()
        lower.cd().SetBottomMargin(0.3)
        upper.cd()
        ROOT.gStyle.SetOptStat(0)
        h1 = hist1.Clone()
        h2 = hist2.Clone()
        rat = hist2.Clone()
        rat.Divide(h1)
        
        y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.1
        
        h1.Draw("hist")
        h1.SetTitle("")
        h1.SetAxisRange(0.0, y_max, "Y")
        h1.GetYaxis().SetMaxDigits(3)
        h1.GetYaxis().SetTitle("Entries")
        h1.GetYaxis().SetTitleSize(0.04)
        h1.GetYaxis().SetTitleOffset(1.2)
        h1.GetYaxis().SetLabelSize(0.05)
        h1.SetYTitle("Entries")
        #h1.SetMarkerColor(ROOT.kBlue)
        #h1.SetMarkerStyle(8)
        #h1.SetMarkerSize(1.5)
        h1.SetLineColor(ROOT.kBlue)    
        h1.SetLineWidth(2)
        h1.SetLabelOffset(5)
        
        h2.Draw("same")
        h2.SetMarkerColor(ROOT.kRed)
        #h2.SetMarkerStyle(8)
        #h2.SetMarkerSize(1.5)
        h2.SetLineColor(ROOT.kRed)
        h2.SetLineWidth(2)

        h1.SetMinimum(0.00001)
        h2.SetMinimum(0.00001)
        
        legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
        legend.SetHeader("              MC,    NEntries,    SoW")
        legend.AddEntry(h1, "32-15 em18, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
        legend.AddEntry(h2, "33-05 pf19, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(0)
        legend.Draw()

        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.03)
        t.SetTextAlign(4)
        
        t.DrawLatex(0.575, 0.65, "ATLAS #it{#bf{Internal}}")
        t.DrawLatex(0.575, 0.6, sample)
        
        
        lower.cd()
        rat.SetTitle("")
        rat.GetXaxis().SetTitle(hName)
        rat.GetXaxis().SetTitleSize(0.09)
        rat.GetXaxis().SetTitleOffset(1.05)
        
        #Line = ROOT.TLine(xLow,1.,xHigh,1.)
        #Line.SetLineWidth(2)
        #Line.SetLineColor(ROOT.kBlack)
        yLow = 0.74
        yHigh = 1.26
        #if "NoWeight" in hName:
        #    yLow = 0.99
        #    yHigh = 1.01
        rat.SetLineWidth(2)
        rat.SetLineColor(ROOT.kBlack)
        rat.SetAxisRange(yLow, yHigh, "Y")
        rat.GetXaxis().SetLabelSize(0.09)
        rat.GetYaxis().SetLabelSize(0.07)
        rat.GetYaxis().SetTitleOffset(0.7)
        rat.GetYaxis().SetTitleSize(0.06)
        rat.GetYaxis().SetTitle("33-05/{}".format(VTag))
        rat.GetYaxis().SetNdivisions(506)
        
        rat.Draw("")
        
        
        canvas.SaveAs(working_dir+hName+"_"+sample+"_{}{}.png".format(per,extra_arg))
        if "Preselection" in hName:
            continue
        upper.cd()
        maxi = h1.GetBinContent(17) #1tag3jet
        h1.SetAxisRange(0.0, maxi*3, "Y")
        canvas.Update()
        canvas.SaveAs(working_dir+hName+"_"+sample+"_{}{}_Zoomed.png".format(per,extra_arg))
    f1.Close()
    f2.Close()



###############################################################################################################################
#######
#######
###############################################################################################################################

if doTables:
    print("doing tables")
else:
    print("skipping tables")
    exit()

tableFileName = ""
plotsFileName = ""
plots4JetFileName = ""
if per=="all":
    tableFileName = working_dir+"ValidationTables.tex"
    plotsFileName = working_dir+"ValidationPlots.tex"
    plots4JetFileName = working_dir+"Validation4JetsPlots.tex"
    File1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-MPT/hadds/hist-Total.root"
    File2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-noNjetCut/hadds/hist-Total.root"
else:
    tableFileName = working_dir+"ValidationTables-{}.tex".format(per)
    plotsFileName = working_dir+"ValidationPlots-{}.tex".format(per)
    plots4JetFileName = working_dir+"Validation4JetsPlots-{}.tex".format(per)
    File1 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-MPT/hadds/hist-Total-{}.root".format(per)
    File2 = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05-noNjetCut/hadds/hist-Total-{}.root".format(per)
f1 = ROOT.TFile.Open(File1)
f2 = ROOT.TFile.Open(File2)


samp_groups = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"],"ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"],
               #"ttbar" : ["ttbar"],
               "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],
               "stop" : ["stopWt","stopt","stops"],"diboson" : ["WW","ZZ","WZ","ggZZ","ggWW"],"data" : ["data"],
               "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}

samp_yields = {"Wbb" : [0,0],"Wbc" : [0,0],"Wbl" : [0,0],"Wcc" : [0,0],"Wcl" : [0,0],"Wl" : [0,0],"Zbb" : [0,0],"Zbc" : [0,0],"Zbl" : [0,0],"Zcc" : [0,0],"Zcl" : [0,0],"Zl" : [0,0],
               #"ttbar" : [0,0],
               "ttbarbb" : [0,0],"ttbarbc" : [0,0],"ttbarbl" : [0,0],"ttbarcc" : [0,0],"ttbarcl" : [0,0],"ttbarl" : [0,0],
               "stopWt" : [0,0],"stopt" : [0,0],"stops" : [0,0],"WW" : [0,0],"ZZ" : [0,0],"WZ" : [0,0],"ggZZ" : [0,0],"ggWW" : [0,0],"data" : [0,0],
               "qqZvvH125" : [0,0],"ggZllH125" : [0,0],"ggZllH125cc" : [0,0],"ggZvvH125" : [0,0],"qqWlvH125" : [0,0],"qqWlvH125cc" : [0,0],"qqZllH125" : [0,0],"qqZllH125cc" : [0,0],"qqZvvH125cc" : [0,0],}
samp_names = ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl","Zbb","Zbc","Zbl","Zcc","Zcl","Zl",
              #"ttbar",
              "ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",
              "stopWt","stopt","stops","WW","ZZ","WZ","ggZZ","ggWW","data",
              "qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc",]
yields33 = {"2tag2jet_150_250ptv_SR" : copy.deepcopy(samp_yields), "2tag3jet_150_250ptv_SR" : copy.deepcopy(samp_yields),
          "2tag2jet_150_250ptv_CRLow" : copy.deepcopy(samp_yields), "2tag3jet_150_250ptv_CRLow" : copy.deepcopy(samp_yields),
          "2tag2jet_150_250ptv_CRHigh" : copy.deepcopy(samp_yields), "2tag3jet_150_250ptv_CRHigh" : copy.deepcopy(samp_yields),
          "2tag2jet_250ptv_SR" : copy.deepcopy(samp_yields), "2tag3jet_250ptv_SR" : copy.deepcopy(samp_yields),
          "2tag2jet_250ptv_CRLow" : copy.deepcopy(samp_yields), "2tag3jet_250ptv_CRLow" : copy.deepcopy(samp_yields),
          "2tag2jet_250ptv_CRHigh" : copy.deepcopy(samp_yields), "2tag3jet_250ptv_CRHigh" : copy.deepcopy(samp_yields),
          "2tag4jet_150_250ptv_SR" : copy.deepcopy(samp_yields), "2tag4jet_250ptv_SR" : copy.deepcopy(samp_yields),
          "2tag4jet_150_250ptv_CRLow" : copy.deepcopy(samp_yields), "2tag4jet_250ptv_CRLow" : copy.deepcopy(samp_yields),
          "2tag4jet_150_250ptv_CRHigh" : copy.deepcopy(samp_yields), "2tag4jet_250ptv_CRHigh" : copy.deepcopy(samp_yields),
}
yields32 = copy.deepcopy(yields33)
samp_yields2 = {"WqqZll" : [0,0],"ZbbZll" : [0,0],"ZqqZll" : [0,0],"ggZvvHcc" : [0,0]}
samp_names2 = ["WqqZll","ZbbZll","ZqqZll","ggZvvHcc"]
yieldsLow = {"2tag2jet_150_250ptv_SR" : copy.deepcopy(samp_yields2), "2tag3jet_150_250ptv_SR" : copy.deepcopy(samp_yields2),
          "2tag2jet_150_250ptv_CRLow" : copy.deepcopy(samp_yields2), "2tag3jet_150_250ptv_CRLow" : copy.deepcopy(samp_yields2),
          "2tag2jet_150_250ptv_CRHigh" : copy.deepcopy(samp_yields2), "2tag3jet_150_250ptv_CRHigh" : copy.deepcopy(samp_yields2),
          "2tag2jet_250ptv_SR" : copy.deepcopy(samp_yields2), "2tag3jet_250ptv_SR" : copy.deepcopy(samp_yields2),
          "2tag2jet_250ptv_CRLow" : copy.deepcopy(samp_yields2), "2tag3jet_250ptv_CRLow" : copy.deepcopy(samp_yields2),
          "2tag2jet_250ptv_CRHigh" : copy.deepcopy(samp_yields2), "2tag3jet_250ptv_CRHigh" : copy.deepcopy(samp_yields2),
          "2tag4jet_150_250ptv_SR" : copy.deepcopy(samp_yields2), "2tag4jet_250ptv_SR" : copy.deepcopy(samp_yields2),
          "2tag4jet_150_250ptv_CRLow" : copy.deepcopy(samp_yields2), "2tag4jet_250ptv_CRLow" : copy.deepcopy(samp_yields2),
          "2tag4jet_150_250ptv_CRHigh" : copy.deepcopy(samp_yields2), "2tag4jet_250ptv_CRHigh" : copy.deepcopy(samp_yields2),}

#Get the yields of each sample
for reg in yields33:
    for samp in samp_yields:
        histo = samp+"_"+reg+"_dPhiBB"
        #print("Getting yields for "+histo)
        h1=ROOT.TH1F()
        h2=ROOT.TH1F()
        do1=1
        do2=1
        ent1=0
        integ1=0
        ent2=0
        integ2=0
        f1.cd()
        try:
            h1=f1.Get(histo).Clone()
        except:
            do1=0
            #print("32-15 doesn't exist")
        f2.cd()
        try:
            h2=f2.Get(histo).Clone()
        except:
            do2=0
            #print("33-05 doesn't exist")
            
        if do2:
            ent2=h2.GetEntries()
            integ2=h2.Integral()
        if do1:
            ent1=h1.GetEntries()
            integ1=h1.Integral()
        #print("ent1: {} ent2: {} integ1: {} integ2: {}".format(ent1,ent2,integ1,integ2))
        yields32[reg][samp][0]=ent1            
        yields32[reg][samp][1]=integ1            
        yields33[reg][samp][0]=ent2            
        yields33[reg][samp][1]=integ2            

for samp in samp_yields2:
    for reg in yieldsLow:
        sampName = ""
        sampFile = ""
        if samp=="WqqZll":
            sampName="WZ"
            sampFile="hist-WqqZll_Sh221.root"
        elif samp=="ZbbZll":
            sampName="ZZ"
            sampFile="hist-ZbbZll_Sh221.root"
        elif samp=="ZqqZll":
            sampName="ZZ"
            sampFile="hist-ZqqZll_Sh221.root"
        elif samp=="ggZvvHcc":
            sampName="ggZvvH125cc"
            sampFile="hist-ggZvvHcc_PwPy8.root"
        
        f1=ROOT.TFile.Open("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15-lowAcceptanceSamples/"+sampFile)
        histo = sampName+"_"+reg+"_dPhiBB"
        #print("Getting yields for "+histo)
        h1=ROOT.TH1F()
        do1=1
        ent1=0
        integ1=0
        f1.cd()
        try:
            h1=f1.Get(histo).Clone()
        except:
            do1=0
            #print("32-15 doesn't exist")
        if do1:
            ent1=h1.GetEntries()
            integ1=h1.Integral()
        #print("ent1: {} ent2: {} integ1: {} integ2: {}".format(ent1,ent2,integ1,integ2))
        yieldsLow[reg][samp][0]=ent1            
        yieldsLow[reg][samp][1]=integ1            

print("Writing out tables")
W=open(tableFileName,'w')     
print("Writing tables out to "+tableFileName)   
# yield tables

W.write(r"\documentclass[aspectratio=916]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"    \usetheme{default}}")
W.write("\n")
W.write(r"    \usepackage{graphicx}")
W.write("\n")
W.write(r"    \usepackage{booktabs}")
W.write("\n")
W.write(r"    \usepackage{caption}")
W.write("\n")
W.write(r"    \usepackage{subcaption}")
W.write("\n")
W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"    \begin{document}")
for reg in yields33:
    regs = reg.replace("_"," ")
    W.write(r"%%%%%%%%%%%%%%%%%%%%%%%%%% Printing out Yields in 0Lep  %%%%%%%%%%%%%%%%%%%%%%%%%%")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{Yields in 0Lep "+regs+"}")
    #W.write("\n")
    #W.write(r"\vspace{5mm}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.35\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|cccccc}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{3}{c|}{\textbf{Entries}} & \multicolumn{3}{c}{\textbf{Sum of Weights}} \\ ")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} ")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{ratio}} & \multicolumn{1}{c|}{\textbf{33-05}} ")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c}{\textbf{ratio}} \\ ")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in samp_names:
        entRat=0
        integRat=0
        if yields32[reg][samp][0] != 0:
            entRat = yields33[reg][samp][0]/yields32[reg][samp][0]
        if yields32[reg][samp][1] != 0.0:
            integRat = yields33[reg][samp][1]/yields32[reg][samp][1]
        W.write(r"    {:15} & {:.0f} & {:.0f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & {:.3f}\\ ".format(
            samp,yields33[reg][samp][0],yields32[reg][samp][0],entRat,yields33[reg][samp][1],yields32[reg][samp][1],integRat))
        W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in samp_names2:
        W.write(r"    {:15} & {:.0f} & {:.0f} & \multicolumn{{1}}{{c|}}{{{:.0f}}} & {:.0f}  & {:.3f} & {:.0f}\\ ".format(
            samp,0,yieldsLow[reg][samp][0],0,0,yieldsLow[reg][samp][1],0))
        W.write("\n")

    W.write(r"    ")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")

# total per sub sample
W.write(r"%%%%%%%%%%%%%%%%%%%%%%%%%% Printing out Yields in 0Lep  %%%%%%%%%%%%%%%%%%%%%%%%%%")
W.write("\n")
W.write(r"\begin{frame}")
W.write("\n")
W.write(r"\begin{table}")
W.write("\n")
W.write(r"\begin{center}")
W.write("\n")
W.write(r"\caption{Yields in 0Lep combined regions}")
#W.write("\n")
#W.write(r"\vspace{5mm}")
W.write("\n")
W.write(r"\resizebox{!}{0.35\linewidth}{")
W.write("\n")
W.write(r"    \begin{tabular}{c|cccccc}")
W.write("\n")
W.write(r"    \toprule")
W.write("\n")
W.write(r"    & \multicolumn{3}{c|}{\textbf{Entries}} & \multicolumn{3}{c}{\textbf{Sum of Weights}} \\ ")
W.write("\n")
W.write(r"    \midrule")
W.write("\n")
W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} ")
W.write("\n")
W.write(r"    & \multicolumn{1}{c|}{\textbf{ratio}} & \multicolumn{1}{c|}{\textbf{33-05}} ")
W.write("\n")
W.write(r"    & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c}{\textbf{ratio}} \\ ")
W.write("\n")
W.write(r"    \midrule")
W.write("\n")
for samp in samp_names:
    entRat=0
    integRat=0
    ent33=0
    ent32=0
    integ33=0
    integ32=0
    for reg in yields33:
        ent33+=yields33[reg][samp][0]
        ent32+=yields32[reg][samp][0]
        integ33+=yields33[reg][samp][1]
        integ32+=yields32[reg][samp][1]
    if ent32 != 0:
        entRat = ent33/ent32
    if integ32 != 0.0:
        integRat = integ33/integ32
    W.write(r"    {:15} & {:.0f} & {:.0f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & {:.3f}\\ ".format(
        samp,ent33,ent32,entRat,integ33,integ32,integRat))
    W.write("\n")
W.write(r"    \midrule")
W.write("\n")
for samp in samp_names2:
    ent32=0
    integ32=0
    for reg in yieldsLow:
        ent32+=yieldsLow[reg][samp][0]
        integ32+=yieldsLow[reg][samp][1]
    W.write(r"    {:15} & {:.0f} & {:.0f} & \multicolumn{{1}}{{c|}}{{{:.0f}}} & {:.0f}  & {:.3f} & {:.0f}\\ ".format(
        samp,0,ent32,0,0,integ32,0))
    W.write("\n")

W.write(r"    ")
W.write(r"    \bottomrule")
W.write("\n")
W.write(r"    \end{tabular}}")
W.write("\n")
W.write(r"\end{center}")
W.write("\n")
W.write(r"\end{table}")
W.write("\n")
W.write(r"\end{frame}")
W.write("\n")

# total per sample
W.write(r"%%%%%%%%%%%%%%%%%%%%%%%%%% Printing out Yields in 0Lep  %%%%%%%%%%%%%%%%%%%%%%%%%%")
W.write("\n")
W.write(r"\begin{frame}")
W.write("\n")
W.write(r"\begin{table}")
W.write("\n")
W.write(r"\begin{center}")
W.write("\n")
W.write(r"\caption{Yields in 0Lep combined regions}")
#W.write("\n")
#W.write(r"\vspace{5mm}")
W.write("\n")
W.write(r"\resizebox{!}{0.15\linewidth}{")
W.write("\n")
W.write(r"    \begin{tabular}{c|cccccc}")
W.write("\n")
W.write(r"    \toprule")
W.write("\n")
W.write(r"    & \multicolumn{3}{c|}{\textbf{Entries}} & \multicolumn{3}{c}{\textbf{Sum of Weights}} \\ ")
W.write("\n")
W.write(r"    \midrule")
W.write("\n")
W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} ")
W.write("\n")
W.write(r"    & \multicolumn{1}{c|}{\textbf{ratio}} & \multicolumn{1}{c|}{\textbf{33-05}} ")
W.write("\n")
W.write(r"    & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c}{\textbf{ratio}} \\ ")
W.write("\n")
W.write(r"    \midrule")
W.write("\n")
for sam in samp_groups:
    entRat=0
    integRat=0
    ent33=0
    ent32=0
    integ33=0
    integ32=0
    for samp in samp_groups[sam]:
        for reg in yields33:
            ent33+=yields33[reg][samp][0]
            ent32+=yields32[reg][samp][0]
            integ33+=yields33[reg][samp][1]
            integ32+=yields32[reg][samp][1]
    if ent32 != 0:
        entRat = ent33/ent32
    if integ32 != 0.0:
        integRat = integ33/integ32
    W.write(r"    {:15} & {:.0f} & {:.0f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & {:.3f}\\ ".format(
        sam,ent33,ent32,entRat,integ33,integ32,integRat))
    W.write("\n")

W.write(r"    ")
W.write(r"    \bottomrule")
W.write("\n")
W.write(r"    \end{tabular}}")
W.write("\n")
W.write(r"\end{center}")
W.write("\n")
W.write(r"\end{table}")
W.write("\n")
W.write(r"\end{frame}")
W.write("\n")


#dRBB region ratios
for reg in ["2tag2jet_150_250ptv","2tag2jet_250ptv","2tag3jet_150_250ptv","2tag3jet_250ptv","2tag4jet_150_250ptv","2tag4jet_250ptv"]:
    regs = reg.replace("_"," ")
    W.write(r"%%%%%%%%%%%%%%%%%%%%%%%%%% Printing out Yields in 0Lep  %%%%%%%%%%%%%%%%%%%%%%%%%%")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{Yields in 0Lep "+regs+"}")
    #W.write("\n")
    #W.write(r"\vspace{5mm}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.15\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|ccccccccc}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{3}{c|}{\textbf{SR}} & \multicolumn{3}{c|}{\textbf{CRLow}} & \multicolumn{3}{c}{\textbf{CRHigh}}\\ ")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c|}{\textbf{ratio}}")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c|}{\textbf{ratio}}")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c}{\textbf{ratio}} \\ ")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in samp_groups:
        SR33=0
        SR32=0
        SRRat=0
        CRLow33=0
        CRLow32=0
        CRLowRat=0
        CRHigh33=0
        CRHigh32=0
        CRHighRat=0
        for sampl in samp_groups[samp]:
            SR33+=yields33[reg+"_SR"][sampl][1]
            SR32+=yields32[reg+"_SR"][sampl][1]
            CRLow33+=yields33[reg+"_CRLow"][sampl][1]
            CRLow32+=yields32[reg+"_CRLow"][sampl][1]
            CRHigh33+=yields33[reg+"_CRHigh"][sampl][1]
            CRHigh32+=yields32[reg+"_CRHigh"][sampl][1]
        if SR32 != 0:
            SRRat = SR33/SR32
        if CRLow32 != 0:
            CRLowRat = CRLow33/CRLow32
        if CRHigh32 != 0:
            CRHighRat = CRHigh33/CRHigh32
        W.write(r"    {:15} & {:.3f} & {:.3f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & {:.3f}\\ ".format(
            samp,SR33,SR32,SRRat,CRLow33,CRLow32,CRLowRat,CRHigh33,CRHigh32,CRHighRat))
        W.write("\n")

    W.write(r"    ")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")

    W.write(r"%%%%%%%%%%%%%%%%%%%%%%%%%% Printing out Yields in 0Lep  %%%%%%%%%%%%%%%%%%%%%%%%%%")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{Percentages of each sample in each dRBB "+regs+"}")
    #W.write("\n")
    #W.write(r"\vspace{5mm}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.15\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|ccccccccc}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{3}{c|}{\textbf{SR}} & \multicolumn{3}{c|}{\textbf{CRLow}} & \multicolumn{3}{c}{\textbf{CRHigh}}\\ ")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c|}{\textbf{ratio}}")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c|}{\textbf{ratio}}")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{33-05}} & \multicolumn{1}{c|}{\textbf{32-15}} & \multicolumn{1}{c}{\textbf{ratio}} \\ ")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in samp_groups:
        SR33=0
        SR32=0
        SRRat=0
        CRLow33=0
        CRLow32=0
        CRLowRat=0
        CRHigh33=0
        CRHigh32=0
        CRHighRat=0
        Tot32=0
        Tot33=0
        for sampl in samp_groups[samp]:
            Tot32+=yields32[reg+"_SR"][sampl][1]+yields32[reg+"_CRHigh"][sampl][1]+yields32[reg+"_CRLow"][sampl][1]
            Tot33+=yields33[reg+"_SR"][sampl][1]+yields33[reg+"_CRHigh"][sampl][1]+yields33[reg+"_CRLow"][sampl][1]
            SR33+=yields33[reg+"_SR"][sampl][1]
            SR32+=yields32[reg+"_SR"][sampl][1]
            CRLow33+=yields33[reg+"_CRLow"][sampl][1]
            CRLow32+=yields32[reg+"_CRLow"][sampl][1]
            CRHigh33+=yields33[reg+"_CRHigh"][sampl][1]
            CRHigh32+=yields32[reg+"_CRHigh"][sampl][1]
        if Tot32 != 0:
            SR32/=Tot32
            CRLow32/=Tot32
            CRHigh32/=Tot32
        if Tot33 != 0:
            SR33/=Tot33
            CRLow33/=Tot33
            CRHigh33/=Tot33
        if SR32 != 0:
            SRRat=SR33/SR32
        if CRLow32 != 0:
            CRLowRat=CRLow33/CRLow32
        if CRHigh32 != 0:
            CRHighRat=CRHigh33/CRHigh32
        W.write(r"    {:15} & {:.3f} & {:.3f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & \multicolumn{{1}}{{c|}}{{{:.3f}}} & {:.3f}  & {:.3f} & {:.3f}\\ ".format(
            samp,SR33*100,SR32*100,SRRat,CRLow33*100,CRLow32*100,CRLowRat,CRHigh33*100,CRHigh32*100,CRHighRat))
        W.write("\n")

    W.write(r"    ")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")

W.write("\n")        
W.write("\end{document}")
W.close()

#DOING PLOTS

W=open(plotsFileName,'w')     
print("Writing plots out to "+plotsFileName)   

W.write(r"\documentclass[aspectratio=169]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"\usetheme{default}}")
W.write("\n")
W.write(r"\usepackage{graphicx}")
W.write("\n")
W.write(r"\usepackage{booktabs}")
W.write("\n")
W.write(r"\usepackage{caption}")
W.write("\n")
W.write(r"\usepackage{subcaption}")
W.write("\n")
W.write(r"\setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"\begin{document}")
W.write("\n")
W.write("\n")
W.write(r"\section{plots}")
W.write("\n")
W.write(r"\newcommand{\FigLocRes}{.}")
W.write("\n")
W.write(r"\newcommand{\MiniPageHeight}{0.42\textheight}")
W.write("\n")
W.write(r"\newcommand{\MiniPageWidth}{0.45\textheight}")
W.write("\n")
W.write(r"\newcommand{\FigSize}{1\linewidth}")
W.write("\n")
for Reg in ["SR","CRLow","CRHigh"]:
    for jetReg in ["2tag2jet_150_250ptv","2tag2jet_250ptv","2tag3jet_150_250ptv","2tag3jet_250ptv"]:
        Title = jetReg.replace("_","\_")
        for Var in ranges150:
            if "2jet" in jetReg:
                if Var == "pTJ3" or Var == "mBBJ" or Var == "pTBBJ":
                    continue
            W.write(r"\begin{frame}")
            W.write("\n")
            W.write(r"\frametitle{{ {} {} {} }}".format(Title,Reg,Var))
            W.write("\n")
            W.write(r"\begin{columns}")
            W.write("\n")
            W.write(r"\column{\MiniPageWidth}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{1\textwidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/signal_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/WJets_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\column{\MiniPageWidth}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/diboson_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/stop_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\column{\MiniPageWidth}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/ZJets_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/ttbar_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\column{\MiniPageWidth}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
            W.write("\n")
            W.write(r"  \centering")
            W.write("\n")
            W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/data_{}_{}_{}_all.png}}".format(jetReg,Reg,Var))
            W.write("\n")
            W.write(r"\end{minipage}")
            W.write("\n")
            W.write(r"\end{columns}")
            W.write("\n")
            W.write(r"\end{frame}")
            W.write("\n")
            W.write("\n")


W.write("\n")        
W.write("\end{document}")
W.close()
W=open(plots4JetFileName,'w')     
print("Writing plots out to "+plots4JetFileName)   

W.write(r"\documentclass[aspectratio=169]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"    \usetheme{default}}")
W.write("\n")
W.write(r"\usepackage{graphicx}")
W.write("\n")
W.write(r"\usepackage{booktabs}")
W.write("\n")
W.write(r"\usepackage{caption}")
W.write("\n")
W.write(r"\usepackage{subcaption}")
W.write("\n")
W.write(r"\setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"\begin{document}")
W.write("\n")
W.write(r"\newcommand{\FigLocRes}{.}")
W.write("\n")
W.write(r"\newcommand{\MiniPageHeight}{0.42\textheight}")
W.write("\n")
W.write(r"\newcommand{\MiniPageWidth}{0.45\textheight}")
W.write("\n")
W.write(r"\newcommand{\FigSize}{1\linewidth}")
W.write("\n")
W.write(r"\section{4jet plots}")
W.write("\n")

for jetReg in ["2tag4jet_150_250ptv","2tag4jet_250ptv"]:
    Title = jetReg.replace("_","\_")
    for Var in ranges150:
        W.write(r"\begin{frame}")
        W.write("\n")
        W.write(r"\frametitle{{ {} {} }}".format(Title,Var))
        W.write("\n")
        W.write(r"\begin{columns}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{1\textwidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/signal_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/WJets_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/diboson_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/stop_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/ZJets_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/ttbar_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/data_{}_SR_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\end{columns}")
        W.write("\n")
        W.write(r"\end{frame}")
        W.write("\n")
        W.write("\n")

W.write("\n")
W.write("\n")
W.write("\n")
W.write(r"\section{inclusiveBtag plots}")
W.write("\n")

for jetReg in ["0ptag_SR","2tag_SR"]:
    Title = jetReg.replace("_","\_")
    for Var in ranges150:
        W.write(r"\begin{frame}")
        W.write("\n")
        W.write(r"\frametitle{{InclusiveBtag {} {} }}".format(Title,Var))
        W.write("\n")
        W.write(r"\begin{columns}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{1\textwidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_signal_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_WJets_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_diboson_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_stop_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_ZJets_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_ttbar_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveBTag_data_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\end{columns}")
        W.write("\n")
        W.write(r"\end{frame}")
        W.write("\n")
        W.write("\n")

W.write("\n")
W.write("\n")
W.write("\n")
W.write(r"\section{inclusiveJets plots}")
W.write("\n")

for jetReg in ["2tag2jet_SR","2tag3jet_SR","2tag4jet_SR","0ptag2jet_SR","0ptag3jet_SR"]:
    Title = jetReg.replace("_","\_")
    for Var in ranges150:
        if "2jet" in jetReg:
            if Var == "pTJ3" or Var == "mBBJ" or Var == "pTBBJ":
                continue
        W.write(r"\begin{frame}")
        W.write("\n")
        W.write(r"\frametitle{{InclusiveJets {} {} }}".format(Title,Var))
        W.write("\n")
        W.write(r"\begin{columns}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{1\textwidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_signal_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_WJets_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_diboson_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_stop_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_ZJets_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_ttbar_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\column{\MiniPageWidth}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\begin{minipage}[c][\MiniPageHeight][c]{\linewidth}")
        W.write("\n")
        W.write(r"  \centering")
        W.write("\n")
        W.write(r"  \includegraphics[width=\FigSize]{{\FigLocRes/InclusiveJets_data_{}_{}_all.png}}".format(jetReg,Var))
        W.write("\n")
        W.write(r"\end{minipage}")
        W.write("\n")
        W.write(r"\end{columns}")
        W.write("\n")
        W.write(r"\end{frame}")
        W.write("\n")
        W.write("\n")

W.write("\n")        
W.write("\end{document}")
            
