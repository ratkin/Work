#! /bin/bash

export C=/afs/cern.ch/user/r/ratkin/CxAODMaker_24Nov_v33-05

setupATLAS
lsetup git python
#lsetup rucio
lsetup root
#lsetup panda
#voms-proxy-init --voms atlas
kinit ratkin@CERN.CH

echo ""
echo "%%%  Building Athena now  %%%"
echo ""

CurrentDir=${PWD}

cd ${C}/build
asetup AnalysisBase,21.2.145,here
cd ${CurrentDir}

