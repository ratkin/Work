"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
import copy
import ctypes
import numpy as np
#import argparse



sys.argv.append("-b")
print ("Opening files")


samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"]}

#print(chains_by_type["ZJets"].GetEntries())
#samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
#           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
#           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
#           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
#           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}

samples = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
           "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
           "stop" : ["stop"],"diboson" : ["diboson"],"data" : ["data"],
           "signal" : ["signal"]}

colours = {"signal" : ROOT.kRed, "WJetsbb" : ROOT.kGreen+4, 
           "WJetsbc" : ROOT.kGreen+3,
           "WJetsbl" : ROOT.kGreen+2,
           "WJetscc" : ROOT.kGreen+1,
           "WJetscl" : ROOT.kGreen-6,
           "WJetsl" : ROOT.kGreen-9,
           "ZJetsbb" : ROOT.kAzure+2, 
           "ZJetsbc" : ROOT.kAzure+1, 
           "ZJetsbl" : ROOT.kAzure-2, 
           "ZJetscc" : ROOT.kAzure-4, 
           "ZJetscl" : ROOT.kAzure-8, 
           "ZJetsl" : ROOT.kAzure-9, 
           "ttbarbb" : ROOT.kOrange, 
           "ttbarbc" : ROOT.kOrange+1, 
           "ttbarbl" : ROOT.kOrange+2, 
           "ttbarcc" : ROOT.kOrange+3, 
           "ttbarcl" : ROOT.kOrange+4, 
           "ttbarl" : ROOT.kOrange+5,
           "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

scalefactors = {"ttbar2jet" : 0.98, "ttbar3jet" : 0.93,"WHF2jet" : 1.06,"WHF3jet" : 1.15,"ZHF2jet" : 1.16,"ZHF3jet" : 1.09,}

m_sigVals={}
m_sigValsTot={}
m_yieldVals={}
m_SBVals={}
m_sigValsMVA={}
m_yieldValsMVA={}
m_SBValsMVA={}

working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/plots_FSR/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/Condor/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)
if not os.path.isdir(working_dir+"JetMigrations/"):
    os.makedirs(working_dir+"JetMigrations/")
if not os.path.isdir(working_dir+"Significances/"):
    os.makedirs(working_dir+"Significances/")
if not os.path.isdir(working_dir+"compareDataMC/"):
    os.makedirs(working_dir+"compareDataMC/")
if not os.path.isdir(working_dir+"DataMC/"):
    os.makedirs(working_dir+"DataMC/")
if not os.path.isdir(working_dir+"cutsCompare/data/"):
    os.makedirs(working_dir+"cutsCompare/data/")
if not os.path.isdir(working_dir+"cutsCompare/signal/"):
    os.makedirs(working_dir+"cutsCompare/signal/")
if not os.path.isdir(working_dir+"cutsCompare/diboson/"):
    os.makedirs(working_dir+"cutsCompare/diboson/")
if not os.path.isdir(working_dir+"cutsCompare/stop/"):
    os.makedirs(working_dir+"cutsCompare/stop/")
if not os.path.isdir(working_dir+"cutsCompare/ttbar/"):
    os.makedirs(working_dir+"cutsCompare/ttbar/")
if not os.path.isdir(working_dir+"cutsCompare/WJets/"):
    os.makedirs(working_dir+"cutsCompare/WJets/")
if not os.path.isdir(working_dir+"cutsCompare/ZJets/"):
    os.makedirs(working_dir+"cutsCompare/ZJets/")
#hist_file_name = working_dir+"JetPtOptimisation.root"
#hist_file_name = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/"+"JetOpt.root"

hist_file422 = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J220J3/JetOpt.root", "READ")
hist_file422F = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J220J3_FSR/JetOpt.root", "READ")
hist_file423 = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J230J3/JetOpt.root", "READ")
hist_file423F = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J230J3_FSR/JetOpt.root", "READ")
hist_file423FB = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J230J3_FSR_b4Cuts/JetOpt.root", "READ")
hist_file433 = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J130J230J3/JetOpt.root", "READ")
hist_file433F = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J130J230J3_FSR/JetOpt.root", "READ")
hist_file433FB = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J130J230J3_FSR_b4Cuts/JetOpt.root", "READ")

assert hist_file422.IsOpen()
assert hist_file422F.IsOpen()
assert hist_file423.IsOpen()
assert hist_file423F.IsOpen()
assert hist_file423FB.IsOpen()
assert hist_file433.IsOpen()
assert hist_file433F.IsOpen()
assert hist_file433FB.IsOpen()

W=open("JetOptTables_FSR.tex",'w')
W.write(r"\documentclass[aspectratio=916]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"    \usetheme{default}}")
W.write("\n")
W.write(r"    \usepackage{graphicx}")
W.write("\n")
W.write(r"    \usepackage{booktabs}")
W.write("\n")
W.write(r"    \usepackage{caption}")
W.write("\n")
W.write(r"    \usepackage{subcaption}")
W.write("\n")
W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"    \begin{document}")
W.write("\n")        
W.write(r"\end{document} % move this to end of file")
W.write("\n")
W.close()


def rebinTrafoDHist(histo, bins): 
  
    histoOld = ROOT.TH1F(histo) 
    histo.Reset()
    newNBins = len(bins)-1
    
    nBinEdges = len(bins) # add one more bin for plotting
    binEdges = [None]*nBinEdges
    for iBinEdge in range(0, nBinEdges) :
        iBin = bins[nBinEdges - iBinEdge - 1] # vector is reverse-ordered
        if (iBin == 0): 
            iBin = 1
        if (iBin == histoOld.GetNbinsX() + 2): 
            iBin = histoOld.GetNbinsX() + 1
    
        binEdges[iBinEdge] = histoOld.GetBinLowEdge(iBin)
    
    #    binEdges[nBinEdges - 1] = 1.8; # additional bin for plotting
    binEdges = np.array(binEdges , dtype="d") 
    histo.SetBins(newNBins, binEdges)
  
    for iBinNew in range(1, newNBins+1):
        # vector is reverse-ordered
        iBinLow  = bins[newNBins - iBinNew + 1] 
        iBinHigh = bins[newNBins - iBinNew] - 1 
        
        err = ctypes.c_double(0.)
        integral = histoOld.IntegralAndError(iBinLow, iBinHigh, err)
        histo.SetBinContent(iBinNew, integral)
        histo.SetBinError(iBinNew, err)
  
    oldNbins = histoOld.GetNbinsX()
    diff = 1. - float(histo.Integral(0, newNBins + 1)) / histoOld.Integral(0, oldNbins + 1)
    # double diff = 1 - histo -> GetSumOfWeights() / histoOld -> GetSumOfWeights();
    if (abs(diff) > 1.e-7) :
        print("WARNING: sizeable difference in transformation of {} found. Integrals: (old-new)/old = {}".format(
            histo.GetName()   , diff              ))
        

def applyTrafoDToHist(hist_sig, hist_bkg, vtag):
    Zs=0
    Zb=0
    if "150_250" in vtag:
        Zs=10
        Zb=5
    else:
        Zs=5
        Zb=3
    # rebin histogram based on trafoD binning 
    newBins = getTrafoD(hist_sig, hist_bkg, Zs, Zb)
    
    rebinTrafoDHist(hist_sig, newBins)
    rebinTrafoDHist(hist_bkg, newBins)
    
    return newBins

def getTrafoD(hist_sig, hist_bkg, Zsig, Zbkg, MCstatUpBound = 0.2) : 
    histoSig = ROOT.TH1F(hist_sig) 
    histoBkg = ROOT.TH1F(hist_bkg) 
    
    bins = [] 
    nBins = histoBkg.GetNbinsX()
    iBin = nBins + 1; # start with overflow bin
    bins.append(nBins+2)
    
    nBkgerr = ctypes.c_double(0.)
    
    nBkg = histoBkg.IntegralAndError(0, nBins+1, nBkgerr)
    nSig = histoSig.Integral(0, nBins+1)
    #print("iBin {} nBkg {} nSig {}".format(iBin,nBkg,nSig))
    while (iBin > 0) :
        #print(iBin)
        sumBkg    = 0.
        sumSig    = 0.
        err2Bkg   = 0.
        bool_pass = False
        
        dist = 1e10
        distPrev = 1e10

        while (not bool_pass and iBin >= 0) :
            nBkgBin  = histoBkg.GetBinContent(iBin)
            nSigBin  = histoSig.GetBinContent(iBin)
            sumBkg  += nBkgBin
            sumSig  += nSigBin
            err2Bkg += pow(histoBkg.GetBinError(iBin),2)
            
            #print("  iBin {} sunBkg {} sumSig {} err2Bkg {}".format(iBin,sumBkg,sumSig,err2Bkg))
            err2RelBkg = 1.
            if (sumBkg != 0): 
                err2RelBkg = err2Bkg / pow(sumBkg, 2)
            
            err2Rel = 1.
            # "trafo D"
            if (sumBkg != 0 and sumSig != 0) :
                err2Rel = 1. / (sumBkg / (nBkg / Zbkg) + sumSig / (nSig / Zsig))
            elif (sumBkg != 0) : 
                err2Rel = (nBkg / Zbkg) / sumBkg
            elif (sumSig != 0) : 
                err2Rel = (nSig / Zsig) / sumSig

            bool_pass = (np.sqrt(err2Rel) < 1 and np.sqrt(err2RelBkg) < MCstatUpBound)
            # distance
            dist = np.abs(err2Rel - 1)
            #print("  err2RelBkg {} err2Rel {} bool_pass {} dist {} distPrev {}".format(err2RelBkg,err2Rel,bool_pass,dist,distPrev))
            if (not (bool_pass and dist > distPrev)): 
                iBin-=1
            #else : 
                # use previous bin
            distPrev = dist
        # remove last bin
        if (iBin+1 == 0 and len(bins) > 1 and len(bins) > Zsig + Zbkg + 0.01): 
            # remove last bin if Nbin > Zsig + Zbkg
            # (1% threshold to capture rounding issues)
            bins.pop()
        #print("appending "+str(iBin+1))
        bins.append(iBin+1)
    return bins

def getSignificance(S,B,vtag):
    i = S.FindBin(30)
    binMax = S.FindBin(250)
    summ=0
    while i <= binMax:
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)

def getSignificanceMVA(S,B,vtag,doTrafoD):
    summ=0
    S1=S.Clone()
    B1=B.Clone()
    #S1 = ROOT.TH1F("S1","S1",200,-1,1)
    #B1 = ROOT.TH1F("B1","B1",200,-1,1)
    #for i in range(S.GetNbinsX()):
    #    for j in range(10):
    #        S1.SetBinContent(i*10+j+1,S.GetBinContent(i+1)/10)
    #        S1.SetBinError(i*10+j+1,S.GetBinError(i+1)/10)
     #       B1.SetBinContent(i*10+j+1,B.GetBinContent(i+1)/10)
      #      B1.SetBinError(i*10+j+1,B.GetBinError(i+1)/10)

    if doTrafoD:
        applyTrafoDToHist(S1, B1, vtag)
    i = 1
    while i <= S1.GetNbinsX():
        s=S1.GetBinContent(i)
        b=B1.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    if doTrafoD:
        print("Significance with TrafoD for {}: {}".format(vtag,math.sqrt(summ)))
    else:
        print("Significance no TrafoD for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)

def doBlinding(S1,B1,h1,var):
    thresh = 0.1
    if ("mBB" in var or "Mbb" in var) and "J" not in var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            i+=1
    elif "mva" in var:
        tot_sig = S1.Integral()
        if not tot_sig > 0: 
            return h1
        sum_sig=0
        i=h1.GetNbinsX()
        #while h1.GetNbinsX()-i<5:
        #    h1.SetBinContent(i, 0)
        #    h1.SetBinError(i, 0)
        #    i-=1
        while i>0:
            sum_sig+=S1.GetBinContent(i)
            if sum_sig/tot_sig < 0.7:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
                i-=1
            else:
                break
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1

    return h1

def MbbFitter(h422,h422F,h423,h423F,h423FB,h433,h433F,h433FB):
    #h.SetLineColor(ROOT.kBlue)
    #h.SetFillColor(ROOT.kBlue)
    

    m = ROOT.RooRealVar("m","m",25,200)
    B_xp = ROOT.RooRealVar("B_xp","B_xp",121.5,100,140)
    B_sig = ROOT.RooRealVar("B_sig","B_sig",15,5,25)
    B_xi = ROOT.RooRealVar("B_xi","B_xi",0,-1,1)
    B_rho1 = ROOT.RooRealVar("B_rho1","B_rho1", 0,-1,1)
    B_rho2 = ROOT.RooRealVar("B_rho2","B_rho2", 0,-1,1)

    B_xp_1 = ROOT.RooRealVar("B_xp_1","B_xp_1",121.5,100,140)
    B_sig_1 = ROOT.RooRealVar("B_sig_1","B_sig_1",15,5,25)
    B_xi_1 = ROOT.RooRealVar("B_xi_1","B_xi_1",0,-1,1)
    B_rho1_1 = ROOT.RooRealVar("B_rho1_1","B_rho1_1", 0,-1,1)
    B_rho2_1 = ROOT.RooRealVar("B_rho2_1","B_rho2_1", 0,-1,1)

    B_xp_2 = ROOT.RooRealVar("B_xp_2","B_xp_2",121.5,100,140)
    B_sig_2 = ROOT.RooRealVar("B_sig_2","B_sig_2",15,5,25)
    B_xi_2 = ROOT.RooRealVar("B_xi_2","B_xi_2",0,-1,1)
    B_rho1_2 = ROOT.RooRealVar("B_rho1_2","B_rho1_2", 0,-1,1)
    B_rho2_2 = ROOT.RooRealVar("B_rho2_2","B_rho2_2", 0,-1,1)

    B_xp_3 = ROOT.RooRealVar("B_xp_3","B_xp_3",121.5,100,140)
    B_sig_3 = ROOT.RooRealVar("B_sig_3","B_sig_3",15,5,25)
    B_xi_3 = ROOT.RooRealVar("B_xi_3","B_xi_3",0,-1,1)
    B_rho1_3 = ROOT.RooRealVar("B_rho1_3","B_rho1_3", 0,-1,1)
    B_rho2_3 = ROOT.RooRealVar("B_rho2_3","B_rho2_3", 0,-1,1)

    B_xp_4 = ROOT.RooRealVar("B_xp_4","B_xp_4",121.5,100,140)
    B_sig_4 = ROOT.RooRealVar("B_sig_4","B_sig_4",15,5,25)
    B_xi_4 = ROOT.RooRealVar("B_xi_4","B_xi_4",0,-1,1)
    B_rho1_4 = ROOT.RooRealVar("B_rho1_4","B_rho1_4", 0,-1,1)
    B_rho2_4 = ROOT.RooRealVar("B_rho2_4","B_rho2_4", 0,-1,1)

    B_xp_5 = ROOT.RooRealVar("B_xp_5","B_xp_5",121.5,100,140)
    B_sig_5 = ROOT.RooRealVar("B_sig_5","B_sig_5",15,5,25)
    B_xi_5 = ROOT.RooRealVar("B_xi_5","B_xi_5",0,-1,1)
    B_rho1_5 = ROOT.RooRealVar("B_rho1_5","B_rho1_5", 0,-1,1)
    B_rho2_5 = ROOT.RooRealVar("B_rho2_5","B_rho2_5", 0,-1,1)

    B_xp_6 = ROOT.RooRealVar("B_xp_6","B_xp_6",121.5,100,140)
    B_sig_6 = ROOT.RooRealVar("B_sig_6","B_sig_6",15,5,25)
    B_xi_6 = ROOT.RooRealVar("B_xi_6","B_xi_6",0,-1,1)
    B_rho1_6 = ROOT.RooRealVar("B_rho1_6","B_rho1_6", 0,-1,1)
    B_rho2_6 = ROOT.RooRealVar("B_rho2_6","B_rho2_6", 0,-1,1)

    B_xp_7 = ROOT.RooRealVar("B_xp_7","B_xp_7",121.5,100,140)
    B_sig_7 = ROOT.RooRealVar("B_sig_7","B_sig_7",15,5,25)
    B_xi_7 = ROOT.RooRealVar("B_xi_7","B_xi_7",0,-1,1)
    B_rho1_7 = ROOT.RooRealVar("B_rho1_7","B_rho1_7", 0,-1,1)
    B_rho2_7 = ROOT.RooRealVar("B_rho2_7","B_rho2_7", 0,-1,1)

    
    bukin = ROOT.RooBukinPdf("bukin","bukin",m,B_xp,B_sig,B_xi,B_rho1,B_rho2)
    bukin1 = ROOT.RooBukinPdf("bukin1","bukin1",m,B_xp_1,B_sig_1,B_xi_1,B_rho1_1,B_rho2_1)
    bukin2 = ROOT.RooBukinPdf("bukin2","bukin2",m,B_xp_2,B_sig_2,B_xi_2,B_rho1_2,B_rho2_2)
    bukin3 = ROOT.RooBukinPdf("bukin3","bukin3",m,B_xp_3,B_sig_3,B_xi_3,B_rho1_3,B_rho2_3)
    bukin4 = ROOT.RooBukinPdf("bukin4","bukin4",m,B_xp_4,B_sig_4,B_xi_4,B_rho1_4,B_rho2_4)
    bukin5 = ROOT.RooBukinPdf("bukin5","bukin5",m,B_xp_5,B_sig_5,B_xi_5,B_rho1_5,B_rho2_5)
    bukin6 = ROOT.RooBukinPdf("bukin6","bukin6",m,B_xp_6,B_sig_6,B_xi_6,B_rho1_6,B_rho2_6)
    bukin7 = ROOT.RooBukinPdf("bukin7","bukin7",m,B_xp_7,B_sig_7,B_xi_7,B_rho1_7,B_rho2_7)

    #h.Rebin(5)
    #h1.Rebin(5)
    dH = ROOT.RooDataHist("dH","dH",ROOT.RooArgList(m),h422)
    dH1 = ROOT.RooDataHist("dH1","dH1",ROOT.RooArgList(m),h422F)
    dH2 = ROOT.RooDataHist("dH2","dH2",ROOT.RooArgList(m),h423)
    dH3 = ROOT.RooDataHist("dH3","dH3",ROOT.RooArgList(m),h423F)
    dH4 = ROOT.RooDataHist("dH4","dH4",ROOT.RooArgList(m),h423FB)
    dH5 = ROOT.RooDataHist("dH5","dH5",ROOT.RooArgList(m),h433)
    dH6 = ROOT.RooDataHist("dH6","dH6",ROOT.RooArgList(m),h433F)
    dH7 = ROOT.RooDataHist("dH7","dH7",ROOT.RooArgList(m),h433FB)
    
    mframe = m.frame()
    
    fit = bukin.fitTo(dH,ROOT.RooFit.Save())
    fit1 = bukin1.fitTo(dH1,ROOT.RooFit.Save())
    fit2 = bukin2.fitTo(dH2,ROOT.RooFit.Save())
    fit3 = bukin3.fitTo(dH3,ROOT.RooFit.Save())
    fit4 = bukin4.fitTo(dH4,ROOT.RooFit.Save())
    fit5 = bukin5.fitTo(dH5,ROOT.RooFit.Save())
    fit6 = bukin6.fitTo(dH6,ROOT.RooFit.Save())
    fit7 = bukin7.fitTo(dH7,ROOT.RooFit.Save())
    #fit2.Print()
    
    #dH.plotOn(mframe,ROOT.RooFit.Name("Data"))
    #dH1.plotOn(mframe,ROOT.RooFit.Name("Data1"))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("Fit"),ROOT.RooFit.LineColor(2))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("Fit1"),ROOT.RooFit.LineColor(3))
    #mframe.Draw()
    #chi2 = mframe.chiSquare("Fit","Data",5)
    #chi21 = mframe.chiSquare("Fit1","Data1",5)
    #print("chi2: {} chi21: {}".format(chi2,chi21))

    HI = bukin.createHistogram('m', 175)
    HI1 = bukin1.createHistogram('m', 175)
    HI2 = bukin2.createHistogram('m', 175)
    HI3 = bukin3.createHistogram('m', 175)
    HI4 = bukin4.createHistogram('m', 175)
    HI5 = bukin5.createHistogram('m', 175)
    HI6 = bukin6.createHistogram('m', 175)
    HI7 = bukin7.createHistogram('m', 175)
    #print(h.Integral("width"), h.GetBinContent(100))
    #print(HI.Integral("width"), HI.GetBinContent(100))
    #print(h.Integral("width")/HI.Integral("width"))
    #print(h.GetBinContent(h.GetXaxis().FindBin(100))/HI.GetBinContent(HI.GetXaxis().FindBin(100)))
    HI.Scale(h422.Integral("width")/HI.Integral("width"))
    HI1.Scale(h422F.Integral("width")/HI1.Integral("width"))
    HI2.Scale(h423.Integral("width")/HI2.Integral("width"))
    HI3.Scale(h423F.Integral("width")/HI3.Integral("width"))
    HI4.Scale(h423FB.Integral("width")/HI4.Integral("width"))
    HI5.Scale(h433.Integral("width")/HI5.Integral("width"))
    HI6.Scale(h433F.Integral("width")/HI6.Integral("width"))
    HI7.Scale(h433FB.Integral("width")/HI7.Integral("width"))
    #HI.Scale(h.Integral()/HI.Integral())
    #HI1.Scale(h1.Integral()/HI1.Integral())
    #print(HI.Integral(), HI.GetBinContent(100))
    HI.SetFillStyle(0)
    HI.SetLineWidth(2)
    HI.SetLineStyle(2)
    HI1.SetFillStyle(0)
    HI1.SetLineWidth(2)
    HI1.SetLineStyle(2)
    HI2.SetFillStyle(0)
    HI2.SetLineWidth(2)
    HI2.SetLineStyle(2)
    HI3.SetFillStyle(0)
    HI3.SetLineWidth(2)
    HI3.SetLineStyle(2)
    HI4.SetFillStyle(0)
    HI4.SetLineWidth(2)
    HI4.SetLineStyle(2)
    HI5.SetFillStyle(0)
    HI5.SetLineWidth(2)
    HI5.SetLineStyle(2)
    HI6.SetFillStyle(0)
    HI6.SetLineWidth(2)
    HI6.SetLineStyle(2)
    HI7.SetFillStyle(0)
    HI7.SetLineWidth(2)
    HI7.SetLineStyle(2)

    mean = B_xp.getVal()
    sigma = B_sig.getVal()
    mean1 = B_xp_1.getVal()
    sigma1 = B_sig_1.getVal()
    mean2 = B_xp_2.getVal()
    sigma2 = B_sig_2.getVal()
    mean3 = B_xp_3.getVal()
    sigma3 = B_sig_3.getVal()
    mean4 = B_xp_4.getVal()
    sigma4 = B_sig_4.getVal()
    mean5 = B_xp_5.getVal()
    sigma5 = B_sig_5.getVal()
    mean6 = B_xp_6.getVal()
    sigma6 = B_sig_6.getVal()
    mean7 = B_xp_7.getVal()
    sigma7 = B_sig_7.getVal()

    return HI,HI1,HI2,HI3,HI4,HI5,HI6,HI7,[mean,sigma,mean1,sigma1,mean2,sigma2,mean3,sigma3,mean4,sigma4,mean5,sigma5,mean6,sigma6,mean7,sigma7,]
    
    #dH.plotOn(mframe,ROOT.RooFit.DrawOption("B"),ROOT.RooFit.FillColorAlpha(0,0),ROOT.RooFit.LineColor(ROOT.kBlue), ROOT.RooFit.MarkerColor(ROOT.kBlue))
    #dH1.plotOn(mframe,ROOT.RooFit.LineColor(ROOT.kRed), ROOT.RooFit.MarkerColor(ROOT.kRed))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("bukin"),ROOT.RooFit.LineColor(ROOT.kBlue),ROOT.RooFit.LineStyle(1))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("bukin1"),ROOT.RooFit.LineColor(ROOT.kRed),ROOT.RooFit.LineStyle(1))
    
    '''
    leg1 = ROOT.TLegend(0.2,0.75,0.6,0.85)
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetTextSize(0.03)
    leg1.SetHeader("   Jet type  peak  width")
    leg1.AddEntry(mframe.findObject("bukin"),"EMTopo {:.2f} {:.2f}".format(mean, sigma), "L")
    leg1.AddEntry(mframe.findObject("bukin1"),"PFlow {:.2f} {:.2f}".format(mean1, sigma1), "L")
    
    can = ROOT.TCanvas("massframe","massframe",750,800)
    can.cd()
    can.SetTickx()
    can.SetTicky()
    h.Draw('hist')
    h1.Draw('hist same')
    HI.Draw('c hist same')
    HI1.Draw('c hist same')
    
    #bukin.Draw('same')
    #fit.Draw('same')
    
    xTitle = ""
    words = histName.split("_")
    for word in words:
        if "Mbb" in word:
            xTitle = "#bf{"+word+"} [GeV]"
    
    mframe.Draw()
    mframe.SetTitle("")
    mframe.GetXaxis().SetTitle(xTitle)
    mframe.GetYaxis().SetTitle("Events")
    mframe.SetMaximum(mframe.GetMaximum()*1.2)
    leg1.Draw("same")
    mframe.GetYaxis().SetTitle("Events")
    
    can.SaveAs(plot_dir+histName+".png")
    '''


def printYields(yields,canName):
    
    W=open("JetOptTables_FSR.tex",'a')
    canName=canName.replace("_","-")

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+canName+"}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.25\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c|c|c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J2\_20J3}} & \multicolumn{1}{c|}{\textbf{20J2\_20J3_FSR}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3_FSR}} & \textbf{20J2\_30J3_FSR_b4Cuts} & \multicolumn{1}{c|}{\textbf{30J2\_30J3}} & \multicolumn{1}{c|}{\textbf{30J2\_30J3_FSR}} & \textbf{30J2\_30J3_FSR_b4Cuts}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in ["data","signal","diboson","stop","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl","ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
        nom = yields[samp]["20J2_20J3"]
        if nom ==0.0:
            nom=1
        W.write(r"    {} & {:.1f} & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$)\\ ".format(
            samp,nom,
            yields[samp]["20J2_20J3_FSR"],100*(yields[samp]["20J2_20J3_FSR"]/nom-1),
            yields[samp]["20J2_30J3"],100*(yields[samp]["20J2_30J3"]/nom-1),
            yields[samp]["20J2_30J3_FSR"],100*(yields[samp]["20J2_30J3_FSR"]/nom-1),
            yields[samp]["20J2_30J3_FSR_b4Cuts"],100*(yields[samp]["20J2_30J3_FSR_b4Cuts"]/nom-1),
            yields[samp]["30J2_30J3"],100*(yields[samp]["30J2_30J3"]/nom-1),
            yields[samp]["30J2_30J3_FSR"],100*(yields[samp]["30J2_30J3_FSR"]/nom-1),
            yields[samp]["30J2_30J3_FSR_b4Cuts"],100*(yields[samp]["30J2_30J3_FSR_b4Cuts"]/nom-1),
        ))
        
        W.write("\n")
        if samp == "stop" or samp == "ZJetsl" or samp == "WJetsl":
             W.write(r"    \midrule")
             W.write("\n")
    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()

def printSignificance(hName):
    
    W=open("JetOptTables_FSR.tex",'a')

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+hName+" Significances}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.30\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c|c|c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J2\_20J3}} & \multicolumn{1}{c|}{\textbf{20J2\_20J3_FSR}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3_FSR}} & \textbf{20J2\_30J3_FSR_b4Cuts} & \multicolumn{1}{c|}{\textbf{30J2\_30J3}} & \multicolumn{1}{c|}{\textbf{30J2\_30J3_FSR}} & \textbf{30J2\_30J3_FSR_b4Cuts}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for JETS in ["2jet","3jet","2$+$3jet"]:
        for MET in ["150_250ptv","250ptv","250_400ptv","400ptv","150ptv"]:
            keyName=JETS+"_"+MET
            keyName1=keyName
            keyName1.replace("_","\_")
            
            if JETS == "2$+$3jet":
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f}\\ ".format(
                    keyName1,
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_20J3_FSR"]**2+m_sigValsTot["3jet_"+MET]["20J2_20J3_FSR"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3_FSR"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3_FSR"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3_FSR_b4Cuts"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3_FSR_b4Cuts"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3_FSR"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3_FSR"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3_FSR_b4Cuts"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3_FSR_b4Cuts"]**2)
                ))
                W.write("\n")
            else:
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f}\\ ".format(
                    keyName1,m_sigValsTot[keyName]["20J2_20J3"],m_sigValsTot[keyName]["20J2_20J3_FSR"],
                    m_sigValsTot[keyName]["20J2_30J3"],m_sigValsTot[keyName]["20J2_30J3_FSR"],m_sigValsTot[keyName]["20J2_30J3_FSR_b4Cuts"],
                    m_sigValsTot[keyName]["30J2_30J3"],m_sigValsTot[keyName]["30J2_30J3_FSR"],m_sigValsTot[keyName]["30J2_30J3_FSR_b4Cuts"]))
                W.write("\n")
        W.write(r"    \midrule")
        W.write("\n")
    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()


def compareDataMC(varName,canName,applyScales):
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    histo452020={}
    histo452020F={}
    histo452030={}
    histo452030F={}
    histo452030FB={}
    histo453030={}
    histo453030F={}
    histo453030FB={}
    Yields={"signal" : {}, "WJetsbb" : {}, "WJetsbc" : {}, "WJetsbl" : {}, "WJetscc" : {}, "WJetscl" : {}, "WJetsl" : {}, "ZJetsbb" : {}, "ZJetsbc" : {}, "ZJetsbl" : {}, "ZJetscc" : {}, "ZJetscl" : {}, "ZJetsl" : {}, "ttbarbb" : {}, "ttbarbc" : {}, "ttbarbl" : {}, "ttbarcc" : {}, "ttbarcl" : {}, "ttbarl" : {}, "stop" : {}, "diboson" : {}, "data" : {}}
    line=canName.split("_")
    canName1=canName
    hist_file422.cd()
    #print("Looping to Get histograms")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp1 = hist_file422.Get(histName1)
            histo452020[samp]=hist_tmp1.Clone()
            Yields[samp]["20J2_20J3"]=histo452020[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))

    hist_file422F.cd()
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp15 = hist_file422F.Get(histName1)
            histo452020F[samp]=hist_tmp15.Clone()
            Yields[samp]["20J2_20J3_FSR"]=histo452020F[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
            
    hist_file423.cd()
    canName1=canName1.replace("20J3","30J3")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp2 = hist_file423.Get(histName1)
            histo452030[samp]=hist_tmp2.Clone()
            Yields[samp]["20J2_30J3"]=histo452030[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
            
    hist_file423F.cd()
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp3 = hist_file423F.Get(histName1)
            histo452030F[samp]=hist_tmp3.Clone()
            Yields[samp]["20J2_30J3_FSR"]=histo452030F[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))

    hist_file423FB.cd()
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp4 = hist_file423FB.Get(histName1)
            histo452030FB[samp]=hist_tmp4.Clone()
            Yields[samp]["20J2_30J3_FSR_b4Cuts"]=histo452030FB[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))

    hist_file433.cd()
    canName1=canName1.replace("20J2","30J2")
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp25 = hist_file433.Get(histName1)
            histo453030[samp]=hist_tmp25.Clone()
            Yields[samp]["30J2_30J3"]=histo453030[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
            
    hist_file433F.cd()
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp35 = hist_file433F.Get(histName1)
            histo453030F[samp]=hist_tmp35.Clone()
            Yields[samp]["30J2_30J3_FSR"]=histo453030F[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))

    hist_file433FB.cd()
    for sampl in samples:
        for samp in samples[sampl]:
            histName1=samp+"_"+canName1+"_"+varName
            #print("Getting hist "+histName1)
            hist_tmp45 = hist_file433FB.Get(histName1)
            histo453030FB[samp]=hist_tmp45.Clone()
            Yields[samp]["30J2_30J3_FSR_b4Cuts"]=histo453030FB[samp].Integral()
            #print(samp+": {}".format(histograms[samp].Integral()))
    
    printYields(Yields,canName)

    histograms={}
    for sampl in samples:
        for samp in samples[sampl]:
            histograms[samp] = ROOT.TH1F(samp+canName,samp+canName,8,0,8)
            histograms[samp].SetBinContent(1,histo452020[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(1, "45J1_20J2_20J3")
            
            histograms[samp].SetBinContent(2,histo452020F[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(2, "45J1_20J2_20J3_FSR")

            histograms[samp].SetBinContent(3,histo452030[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(3, "45J1_20J2_30J3")

            histograms[samp].SetBinContent(4,histo452030F[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(4, "45J1_20J2_30J3_FSR")

            histograms[samp].SetBinContent(5,histo452030FB[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(5, "45J1_20J2_30J3_FSR_b4Cuts")            

            histograms[samp].SetBinContent(6,histo453030[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(6, "45J1_30J2_30J3")

            histograms[samp].SetBinContent(7,histo453030F[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(7, "45J1_30J2_30J3_FSR")

            histograms[samp].SetBinContent(8,histo453030FB[samp].GetBinContent(1))
            histograms[samp].GetXaxis().SetBinLabel(8, "45J1_30J2_30J3_FSR_b4Cuts")            

            
            if samp != "data":
                histograms[samp].SetBinError(1,histo452020[samp].GetBinError(1))
                histograms[samp].SetBinError(2,histo452020F[samp].GetBinError(1))
                histograms[samp].SetBinError(3,histo452030[samp].GetBinError(1))
                histograms[samp].SetBinError(4,histo452030F[samp].GetBinError(1))
                histograms[samp].SetBinError(5,histo452030FB[samp].GetBinError(1))
                histograms[samp].SetBinError(6,histo453030[samp].GetBinError(1))
                histograms[samp].SetBinError(7,histo453030F[samp].GetBinError(1))
                histograms[samp].SetBinError(8,histo453030FB[samp].GetBinError(1))
            else:
                histograms[samp].SetBinError(1,math.sqrt(histo452020[samp].GetBinContent(1)))
                histograms[samp].SetBinError(2,math.sqrt(histo452020F[samp].GetBinContent(1)))
                histograms[samp].SetBinError(3,math.sqrt(histo452030[samp].GetBinContent(1)))
                histograms[samp].SetBinError(4,math.sqrt(histo452030F[samp].GetBinContent(1)))
                histograms[samp].SetBinError(5,math.sqrt(histo452030FB[samp].GetBinContent(1)))
                histograms[samp].SetBinError(6,math.sqrt(histo453030[samp].GetBinContent(1)))
                histograms[samp].SetBinError(7,math.sqrt(histo453030F[samp].GetBinContent(1)))
                histograms[samp].SetBinError(8,math.sqrt(histo453030FB[samp].GetBinContent(1)))
        

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    for samp in samples:
        for sampl in samples[samp]:
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)

    if applyScales:
        for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["ttbar2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ttbar3jet"])
        for samp in ["WJetsbb","WJetsbc","WJetsbl","WJetscc"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["WHF2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["WHF3jet"])
        for samp in ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc"]:
            if canName.split("_")[0] == "2jet":
                histograms[samp].Scale(scalefactors["ZHF2jet"])
            elif canName.split("_")[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ZHF3jet"])

    hs = ROOT.THStack("hs","")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl","stop",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson","signal",]:
        hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson"]:
        hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    canName2=""
    canNameLine=canName.split("_")
    for canN in canNameLine:
        if not ("J1" in canN or "J2" in canN or "J3" in canN):
            canName2+=canN+"_"
    t.DrawLatex(0.15, 0.85, canName2)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "stop","ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
        if "ZJets" in samp or "WJets" in samp:
            sampleName=samp.replace("Jets","")
            legend1.AddEntry(histograms[samp], sampleName+", {:.1f}".format(histograms[samp].Integral()), "f")
        else:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("Cuts Scenario")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.5
    yHigh = 0.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetTitle("")
    yHigh = rat1.GetMaximum()*1.1
    rat1.SetAxisRange(0, yHigh, "Y")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    

    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("Y+")


    plotName = working_dir+"compareDataMC/compareDataMC_"+canName+"_"+varName
    if applyScales:
        plotName+="_Scaled"
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def doDataMC(varName,binning,canName,F,applyScales,FSRType):
    print("doDataMC")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    histograms={}
    line=canName.split("_")
    canName1=canName
    if FSRType != "":
        if "20J2_20J3" in canName1:
            canName1=canName1.replace("20J2_20J3","20J2_20J3_"+FSRType)
        elif "20J2_30J3" in canName1:
            canName1=canName1.replace("20J2_30J3","20J2_30J3_"+FSRType)
        elif "30J2_30J3" in canName1:
            canName1=canName1.replace("30J2_30J3","30J2_30J3_"+FSRType)
    F.cd()
    #print("Looping to Get histograms")
    for sampl in samples:
        for samp in samples[sampl]:
            if "nSigJets-nMatchedTruthSigJets" == varName:
                if sampl == "data" or sampl == "diboson" or sampl == "stop" or sampl == "signal":
                    histName1=samp+"_"+canName+"_"+varName
                    #print("Getting hist "+histName1)
                    hist_tmp1 = F.Get(histName1)
                    histograms[samp]=hist_tmp1.Clone()
                    #elif "bb" not in samp:
                    #    histograms[samp]=ROOT.TH1F("empty"+samp,"empty"+samp,10,0,10)
                else:
                    histName1=samp+"_"+canName+"_"+varName+"_2D-nMatchedTruthSigJets"
                    print("Getting hist "+histName1)
                    hist_tmp1 = F.Get(histName1)
                    histograms[samp]=hist_tmp1.Clone()
            else:
                histName1=samp+"_"+canName+"_"+varName
                #print("Getting hist "+histName1)
                hist_tmp1 = F.Get(histName1)
                if varName == "TTBarDecay" and "ttbar" not in samp:
                    histograms[samp]=ROOT.TH1F("empty"+samp,"empty"+samp,10,0,10)
                else:
                    histograms[samp]=hist_tmp1.Clone()
                #print(samp+": {}".format(histograms[samp].Integral()))

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)


    
    rebinning = int(binning.split(",")[0])
    xLow = float(binning.split(",")[1])
    xHigh = float(binning.split(",")[2])
    if varName == "MET":
        if "150_250ptv" in canName:
            xLow = 100
            xHigh = 300
        elif "250_400ptv" in canName:
            xLow = 200
            xHigh = 450
        elif "400ptv" in canName:
            xLow = 350
            xHigh = 700
    for samp in samples:
        for sampl in samples[samp]:
            if rebinning != 0:
                histograms[sampl].Rebin(rebinning)
            histograms[sampl].GetXaxis().SetRangeUser(xLow,xHigh)
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)
                for i in range(histograms[sampl].GetNbinsX()):
                    if histograms[sampl].GetBinContent(i+1)>0:
                        histograms[sampl].SetBinError(i+1,math.sqrt(histograms[sampl].GetBinContent(i+1)))
    if applyScales:
        for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
            if line[0] == "2jet":
                histograms[samp].Scale(scalefactors["ttbar2jet"])
            elif line[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ttbar3jet"])
        for samp in ["WJetsbb","WJetsbc","WJetsbl","WJetscc"]:
            if line[0] == "2jet":
                histograms[samp].Scale(scalefactors["WHF2jet"])
            elif line[0] != "2pjet":
                histograms[samp].Scale(scalefactors["WHF3jet"])
        for samp in ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc"]:
            if line[0] == "2jet":
                histograms[samp].Scale(scalefactors["ZHF2jet"])
            elif line[0] != "2pjet":
                histograms[samp].Scale(scalefactors["ZHF3jet"])


    hs = ROOT.THStack("hs","")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl","stop",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson","signal",]:
        hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson"]:
        hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()

    if varName == "GSCptJ1":
        print("background 40-50: {} 50-60: {} total: {} percentage: {}".format(hb.GetBinContent(1),hb.GetBinContent(2),hb.Integral(),100*(hb.GetBinContent(1)+hb.GetBinContent(2))/hb.Integral()))
        print("signal 40-50: {} 50-60: {} total: {} percentage: {}".format(hsig.GetBinContent(1),hsig.GetBinContent(2),hsig.Integral(),100*(hsig.GetBinContent(1)+hsig.GetBinContent(2))/hsig.Integral()))

    if ("mBB" in varName or "Mbb" in varName) and "60J1" in line[-4]:
        getSignificance(hsig,hb,canName1+"_"+varName)
    if ("mva" in varName) and "60J1" in line[-4]:
        getSignificanceMVA(hsig,hb,canName1+"_"+varName)

    if ("mBB" in varName or "Mbb" in varName) and "mBBJ" not in varName and "45J1" in line[-4] and "SR" in line[-1]: 
        keyName = line[-3]+"_"+line[-2]
        if FSRType != "":
            keyName+="_"+FSRType
        #print("filling dicts "+keyName)
        m_sigVals[keyName]=getSignificance(hsig,hb,canName1+"_"+varName)
        m_yieldVals[keyName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBVals[keyName]=hsig.Integral()/hb.Integral()
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),hsig.Integral()/hb.Integral()))
        else:
            m_SBVals[keyName]=0
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),0))

    if ("mva" in varName) and "45J1" in line[-4] and "SR" in line[-1]:
        keyName = line[-3]+"_"+line[-2]
        if FSRType != "":
            keyName+="_"+FSRType
        getSignificanceMVA(hsig,hb,canName1+"_"+varName, True)
        m_sigValsMVA[keyName]=getSignificanceMVA(hsig,hb,canName1+"_"+varName, False)
        m_yieldValsMVA[keyName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBValsMVA[keyName]=hsig.Integral()/hb.Integral()
        else:
            m_SBValsMVA[keyName]=0

    
    doBlinding(hsig,hb,hd,varName)
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")
        

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName1)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "stop","ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"]:
        if "ZJets" in samp or "WJets" in samp:
            sampleName=samp.replace("Jets","")
            legend1.AddEntry(histograms[samp], sampleName+", {:.1f}".format(histograms[samp].Integral()), "f")
        else:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    if varName == "TTBarDecay":
        labs=["j-j","j-e","j-mu","j-tau","e-e","mu-mu","tau-tau","e-mu","e-tau","mu-tau"]
        for i in range(len(labs)): 
            rat.GetXaxis().SetBinLabel(i+1, labs[i])
            rat1.GetXaxis().SetBinLabel(i+1, labs[i])
 

    yLow = -0.2
    yHigh = 0.8
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("ep") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    
    yHigh2 = rat1.GetMaximum()*1.1
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    #rat1.SetAxisRange(0, yHigh2, "Y")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinError(i+1)>0.075:
            rat1.SetBinContent(i+1,(rat1.GetBinContent(i)+rat1.GetBinContent(i+2))/2)
            rat1.SetBinError(i+1,(rat1.GetBinError(i)+rat1.GetBinError(i+2))/2)
            #print("{} {}".format(rat1.GetBinContent(i+1),rat1.GetBinError(i+1)))
    
    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("hist Y+")
    

    plotName = working_dir+"DataMC/DataMC_"+canName1+"_"+varName
    if applyScales:
        plotName+="_Scaled"
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def makeSignificancePlots(sigVals,yieldVals,SBVals,JetMET,varName):
    h_sig = ROOT.TH1F("sig","sig",8,0,8)
    h_yield = ROOT.TH1F("yield","yield",8,0,8)
    h_SB = ROOT.TH1F("SB","SB",8,0,8)
    #gRandom = ROOT.TRandom()

    i=0
    print("\n"+JetMET+"  Sig   Yield   S/B")
    sigValsTotTemp={}
    for keyName in ["20J2_20J3","20J2_20J3_FSR","20J2_30J3","20J2_30J3_FSR","20J2_30J3_FSR_b4Cuts","30J2_30J3","30J2_30J3_FSR","30J2_30J3_FSR_b4Cuts"]:
        i+=1
        #keyName = JetCut1+"_"+JetCut2
        keyName1 = "20J2_20J3"
        
        #if JetCut1=="45J1" and JetCut2=="20J2" and JetCut3=="20J3":
        h_sig.SetBinContent(i,sigVals[keyName])
        h_sig.GetXaxis().SetBinLabel(i, keyName)
        h_yield.SetBinContent(i,yieldVals[keyName])
        h_yield.GetXaxis().SetBinLabel(i, keyName)
        h_SB.SetBinContent(i,SBVals[keyName])
        h_SB.GetXaxis().SetBinLabel(i, keyName)
        sigValsNom=sigVals[keyName1]
        if sigValsNom==0:
            sigValsNom=1
        yieldValsNom=yieldVals[keyName1]
        if yieldValsNom==0:
            yieldValsNom=1
        SBValsNom=SBVals[keyName1]
        if SBValsNom==0:
            SBValsNom=1
        print(keyName+":  {:.3f}({:.2f})  {:.3f}({:.2f})  {:.3f}({:.2f})".format(
            sigVals[keyName],100*(sigVals[keyName]/sigValsNom-1),
            yieldVals[keyName],100*(yieldVals[keyName]/yieldValsNom-1),
            SBVals[keyName],100*(SBVals[keyName]/SBValsNom-1)))
        sigValsTotTemp[keyName]=sigVals[keyName]
        #else:
        #    h_sig.SetBinContent(i,sigVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
        #    h_yield.SetBinContent(i,yieldVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
        #    h_SB.SetBinContent(i,SBVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
    print("")
    m_sigValsTot[JetMET]=copy.deepcopy(sigValsTotTemp)
    
    canvasSigs = ROOT.TCanvas("SigYields", "SigYields", 900, 900)
    upperSigs = ROOT.TPad("upperSigs", "upperSigs", 0.025, 0.445, 0.995, 0.995)
    upperSigs.Draw()
    upperSigs.cd()
    #upperSigs.SetGridy()
    #upperSigs.SetBottomMargin(0.3)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gPad.SetTicky(0)
    
    h_sig.SetTitle("")
    h_sig.GetXaxis().SetTitle("")
    h_sig.GetXaxis().SetTitleSize(0.09)
    h_sig.GetXaxis().SetTitleOffset(1.05)

    h_sig.SetLineWidth(2)
    h_sig.SetLineColor(ROOT.kBlack)
    h_sig.SetAxisRange(0, 4.5, "Y")
    h_sig.GetXaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetTitleOffset(0.8)
    h_sig.GetYaxis().SetTitleSize(0.05)
    h_sig.GetYaxis().SetTitle("Sig and S/B")
    h_sig.GetYaxis().SetNdivisions(506)
    y_max = h_sig.GetMaximum()*1.4
    h_sig.SetAxisRange(0.0, y_max, "Y")    
    h_sig.Draw("")
    h_SB.SetLineWidth(2)
    h_SB.SetLineColor(ROOT.kBlue)
    h_SB.Scale(100)
    h_SB.Draw("same hist")

    h_yield.SetLineColor(ROOT.kRed)
    h_yield.SetLineWidth(2)
    h_yield.SetTitle("")
    h_yield.GetXaxis().SetTitle("")
    h_yield.GetXaxis().SetTitleSize(0.09)
    h_yield.GetXaxis().SetTitleOffset(1.05)
    h_yield.GetXaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetTitleOffset(0.8)
    h_yield.GetYaxis().SetTitleSize(0.05)
    h_yield.GetYaxis().SetTitle("Signal Yield")
    h_yield.GetYaxis().SetNdivisions(506)
    y_max = h_yield.GetMaximum()*1.4
    h_yield.SetAxisRange(0.0, y_max, "Y")    

    
    canvasSigs.cd()
    upperSigs2 = ROOT.TPad("upperSigs2", "upperSigs2", 0.025, 0.445, 0.995, 0.995)
    upperSigs2.SetFillStyle(4000)
    upperSigs2.SetFrameFillStyle(0)
    upperSigs2.Draw()
    upperSigs2.cd()
    ROOT.gPad.SetTicky(0)
    #upperSigs2.SetBottomMargin(0.3)
    h_yield.Draw("Y+")


    legend1 = ROOT.TLegend(0.705, 0.75, 0.93, 0.87)
    legend1.AddEntry(h_sig ,"Significance","l")
    legend1.AddEntry(h_SB ,"S/B x 100", "l")
    legend1.AddEntry(h_yield ,"Signal Yield","l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    canvasSigs.cd()
    lowerSigs = ROOT.TPad("lowerSigs", "lowerSigs", 0.025, 0.025, 0.995, 0.495)
    lowerSigs.Draw()
    lowerSigs.SetGridy()
    lowerSigs.cd().SetBottomMargin(0.3)

    rat1=h_sig.Clone()
    rat2=h_SB.Clone()
    rat3=h_yield.Clone()
    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinContent(1)>0:
            rat1.SetBinContent(i+1,h_sig.GetBinContent(i+1)/h_sig.GetBinContent(1))
            rat1.SetBinError(i+1,h_sig.GetBinError(i+1)/h_sig.GetBinContent(1))
        if rat2.GetBinContent(1)>0:
            rat2.SetBinContent(i+1,h_SB.GetBinContent(i+1)/h_SB.GetBinContent(1))
            rat2.SetBinError(i+1,h_SB.GetBinError(i+1)/h_SB.GetBinContent(1))
        if rat3.GetBinContent(1)>0:
            rat3.SetBinContent(i+1,h_yield.GetBinContent(i+1)/h_yield.GetBinContent(1))
            rat3.SetBinError(i+1,h_yield.GetBinError(i+1)/h_yield.GetBinContent(1))

    rat1.SetTitle("")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)

    #rat1.SetLineWidth(2)
    #rat1.SetLineColor(ROOT.kBlack)
    rat1.GetXaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetTitleOffset(0.8)
    rat1.GetYaxis().SetTitleSize(0.05)
    rat1.GetYaxis().SetTitle("Cuts/Nom")
    rat1.GetYaxis().SetNdivisions(506)
    rat1.SetAxisRange(0.5, 1.5, "Y")  

    rat1.Draw("hist")
    rat2.Draw("same hist")
    rat3.Draw("same hist")



    canvasSigs.SaveAs(working_dir+"Significances/Significances_"+JetMET+"_"+varName+".png")
    canvasSigs.Close()
    
def cutsComparisonAllSamples(varName,binning,canName,doNjet):
    for sample in samples:
        cutsComparison(varName,binning,canName,sample,doNjet)

def cutsComparison(varName,binning,canName,sample,doNjet):
    print("cutsComparison")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    sample2=sample
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): # and varName != "nSigJets-nMatchedTruthSigJets":
        sample2+="bb"
        
    hist_file422.cd()
    histName1=sample2+"_"+canName+"_"+varName
    if varName == "nSigJets-nMatchedTruthSigJets" and (sample == "ttbar" or sample == "ZJets" or sample == "WJets"):
        histName1+="_2D-nMatchedTruthSigJets"
    #histName2=histName1.replace("2jet","3jet")
    #histName3=histName1.replace("2jet","4jet")
    #histName4=histName1.replace("2jet","5pjet")
    if doNjet:
        histName1 = histName1.replace("2jet","2pjet")
    print(histName1)
    hist_tmp1 = hist_file422.Get(histName1)
    h422=hist_tmp1.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp50 = hist_file422.Get(histName1a)
            h422.Add(hist_tmp50.Clone())

    hist_file422F.cd()
    hist_tmp1aa = hist_file422F.Get(histName1)
    h422F=hist_tmp1aa.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp51 = hist_file422F.Get(histName1a)
            h422F.Add(hist_tmp51.Clone())

    hist_file423.cd()
    histName1=histName1.replace("20J3","30J3")
    #histName2=histName2.replace("20J3","30J3")
    #histName3=histName3.replace("20J3","30J3")
    #histName4=histName4.replace("20J3","30J3")
    hist_tmp1a = hist_file423.Get(histName1)
    h423=hist_tmp1a.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp52 = hist_file423.Get(histName1a)
            h423.Add(hist_tmp52.Clone())

    hist_file423F.cd()
    hist_tmp1c = hist_file423F.Get(histName1)
    h423F=hist_tmp1c.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp53 = hist_file423F.Get(histName1a)
            h423F.Add(hist_tmp53.Clone())

    hist_file423FB.cd()
    hist_tmp1b = hist_file423FB.Get(histName1)
    h423FB=hist_tmp1b.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp54 = hist_file423FB.Get(histName1a)
            h423FB.Add(hist_tmp54.Clone())

    hist_file433.cd()
    histName1=histName1.replace("20J2","30J2")
    #histName2=histName2.replace("20J2","30J2")
    #histName3=histName3.replace("20J2","30J2")
    #histName4=histName4.replace("20J2","30J2")
    hist_tmp1a = hist_file433.Get(histName1)
    h433=hist_tmp1a.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp452 = hist_file433.Get(histName1a)
            h433.Add(hist_tmp452.Clone())

    hist_file433F.cd()
    hist_tmp1c = hist_file433F.Get(histName1)
    h433F=hist_tmp1c.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp453 = hist_file433F.Get(histName1a)
            h433F.Add(hist_tmp453.Clone())

    hist_file433FB.cd()
    hist_tmp1b = hist_file433FB.Get(histName1)
    h433FB=hist_tmp1b.Clone()
    if (sample == "ttbar" or sample == "ZJets" or sample == "WJets"): #  and varName != "nSigJets-nMatchedTruthSigJets":
        for samp in samples[sample]:
            if "bb" in samp:
                continue
            histName1a=histName1.replace(sample2,samp)
            hist_tmp454 = hist_file433FB.Get(histName1a)
            h433FB.Add(hist_tmp454.Clone())

    '''
    if doNjet:
        hist_tmp2b = hist_file433FB.Get(histName2)
        hist_tmp3b = hist_file433FB.Get(histName3)
        hist_tmp4b = hist_file433FB.Get(histName4)
        h433FB.Add(hist_tmp2b.Clone())
        h433FB.Add(hist_tmp3b.Clone())
        h433FB.Add(hist_tmp4b.Clone())
        if (sample == "ttbar" or sample == "ZJets" or sample == "WJets") and varName != "nSigJets-nMatchedTruthSigJets":
            for samp in samples[sample]:
                if "bb" in samp:
                    continue
                histName2a=histName2.replace(sample2,samp)
                histName3a=histName3.replace(sample2,samp)
                histName4a=histName4.replace(sample2,samp)
                hist_tmp454b = hist_file433FB.Get(histName2a)
                hist_tmp454c = hist_file433FB.Get(histName3a)
                hist_tmp454d = hist_file433FB.Get(histName4a)
                h433FB.Add(hist_tmp454b.Clone())
                h433FB.Add(hist_tmp454c.Clone())
                h433FB.Add(hist_tmp454d.Clone())
    '''
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlack)
    h422.SetLineWidth(2)
    h422F.SetLineColor(ROOT.kGray+2)
    h422F.SetLineWidth(2)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h423FB.SetLineColor(ROOT.kRed)
    h423FB.SetLineWidth(2)
    h423F.SetLineColor(ROOT.kGreen+3)
    h423F.SetLineWidth(2)
    h433.SetLineColor(ROOT.kViolet-1)
    h433.SetLineWidth(2)
    h433FB.SetLineColor(ROOT.kOrange+1)
    h433FB.SetLineWidth(2)
    h433F.SetLineColor(ROOT.kYellow-3)
    h433F.SetLineWidth(2)
            
    rebinning = int(binning.split(",")[0])
    xLow = float(binning.split(",")[1])
    xHigh = float(binning.split(",")[2])
    if varName == "MET":
        if "150_250ptv" in canName:
            xLow = 100
            xHigh = 300
        elif "250_400ptv" in canName:
            xLow = 200
            xHigh = 450
        elif "400ptv" in canName:
            xLow = 350
            xHigh = 700
    h422.GetXaxis().SetRangeUser(xLow,xHigh)
    h422F.GetXaxis().SetRangeUser(xLow,xHigh)
    h423.GetXaxis().SetRangeUser(xLow,xHigh)
    h423F.GetXaxis().SetRangeUser(xLow,xHigh)
    h423FB.GetXaxis().SetRangeUser(xLow,xHigh)
    h433.GetXaxis().SetRangeUser(xLow,xHigh)
    h433F.GetXaxis().SetRangeUser(xLow,xHigh)
    h433FB.GetXaxis().SetRangeUser(xLow,xHigh)

    print(rebinning)
    if rebinning != 0:
        h422.Rebin(rebinning)
        h422F.Rebin(rebinning)
        h423.Rebin(rebinning)
        h423F.Rebin(rebinning)
        h423FB.Rebin(rebinning)
        h433.Rebin(rebinning)
        h433F.Rebin(rebinning)
        h433FB.Rebin(rebinning)


    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h422F.GetMaximum(), h423.GetMaximum(), h423FB.GetMaximum(), h423F.GetMaximum(),h433.GetMaximum(), h433FB.GetMaximum(), h433F.GetMaximum())*1.4
                    
    rat423 = h423.Clone()
    rat422F = h422F.Clone()
    rat423FB = h423FB.Clone()
    rat423F = h423F.Clone()
    rat433FB = h433FB.Clone()
    rat433F = h433F.Clone()
    rat433 = h433.Clone()
    h422.Draw("hist")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    h422.GetYaxis().SetTitle("Entries")
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetYTitle("Entries")
    h422.SetLabelOffset(5)
    
    h422F.Draw("same hist")
    h423.Draw("same hist")
    h423F.Draw("same hist")
    h423FB.Draw("same hist")
    h433.Draw("same hist")
    h433F.Draw("same hist")
    h433FB.Draw("same hist")
    
    if ("mBB" in varName or "Mbb" in varName) and sample == "signal":
        fit_pars = []
        H = ROOT.TH1F()
        H1 = ROOT.TH1F()
        H2 = ROOT.TH1F()
        H3 = ROOT.TH1F()
        H4 = ROOT.TH1F()
        H5 = ROOT.TH1F()
        H6 = ROOT.TH1F()
        H7 = ROOT.TH1F()
        H,H1,H2,H3,H4,H5,H6,H7,fit_pars=MbbFitter(h422.Clone(),h422F.Clone(),h423.Clone(),h423F.Clone(),h423FB.Clone(),h433.Clone(),h433F.Clone(),h433FB.Clone())

        H.SetLineColor(ROOT.kBlack)
        H1.SetLineColor(ROOT.kGray+2)
        H2.SetLineColor(ROOT.kBlue)
        H3.SetLineColor(ROOT.kRed)
        H4.SetLineColor(ROOT.kGreen+3)
        H5.SetLineColor(ROOT.kViolet-1)
        H6.SetLineColor(ROOT.kOrange+1)
        H7.SetLineColor(ROOT.kYellow-3)
        

        H.Draw('c hist same')
        H1.Draw('c hist same')
        H2.Draw('c hist same')
        H3.Draw('c hist same')
        H4.Draw('c hist same')
        H5.Draw('c hist same')
        H6.Draw('c hist same')
        H7.Draw('c hist same')

        #    legend.SetHeader("    Jet type,  peak,  width,  integral")
        #    legend.AddEntry(h, "EMTopo, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[0],fit_pars[1],h.Integral()), "l")
        #    legend.AddEntry(h1, "PFlow, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[2],fit_pars[3],h1.Integral()), "l")
        legend1 = ROOT.TLegend(0.605, 0.6, 0.93, 0.87)
        legend1.SetHeader("     sample,  peak,  width,   SoW")
        legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[0],fit_pars[1],h422.Integral()), "l")
        legend1.AddEntry(h422F, "20J2-20J3-FSR, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[2],fit_pars[3],h422F.Integral()), "l")
        legend1.AddEntry(h423, "20J2-30J3, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[4],fit_pars[5],h423.Integral()), "l")
        legend1.AddEntry(h423F, "20J2-30J3-FSR, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[6],fit_pars[7],h423F.Integral()), "l")
        legend1.AddEntry(h423FB, "20J2-30J3-FSR-b4Cuts, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[8],fit_pars[9],h423FB.Integral()), "l")
        legend1.AddEntry(h433, "30J2-30J3, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[10],fit_pars[11],h433.Integral()), "l")
        legend1.AddEntry(h433F, "30J2-30J3-FSR, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[12],fit_pars[13],h433F.Integral()), "l")
        legend1.AddEntry(h433FB, "30J2-30J3-FSR-b4Cuts, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[14],fit_pars[15],h433FB.Integral()), "l")
        legend1.SetBorderSize(0)
        legend1.SetFillColor(0)
        legend1.SetFillStyle(0)
        legend1.Draw()
    else:
        legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
        legend1.SetHeader("              sample,    NEntries,    SoW")
        legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
        legend1.AddEntry(h422F, "20J2-20J3-FSR, {:.1f}, {:.1f}".format(h422F.GetEntries(),h422F.Integral()), "l")
        legend1.AddEntry(h423, "20J2-30J3, {:.1f}, {:.1f}".format(h423.GetEntries(),h423.Integral()), "l")
        legend1.AddEntry(h423F, "20J2-30J3-FSR, {:.1f}, {:.1f}".format(h423F.GetEntries(),h423F.Integral()), "l")
        legend1.AddEntry(h423FB, "20J2-30J3-FSR-b4Cuts, {:.1f}, {:.1f}".format(h423FB.GetEntries(),h423FB.Integral()), "l")
        legend1.AddEntry(h433, "30J2-30J3, {:.1f}, {:.1f}".format(h433.GetEntries(),h433.Integral()), "l")
        legend1.AddEntry(h433F, "30J2-30J3-FSR, {:.1f}, {:.1f}".format(h433F.GetEntries(),h433F.Integral()), "l")
        legend1.AddEntry(h433FB, "30J2-30J3-FSR-b4Cuts, {:.1f}, {:.1f}".format(h433FB.GetEntries(),h433FB.Integral()), "l")
        legend1.SetBorderSize(0)
        legend1.SetFillColor(0)
        legend1.SetFillStyle(0)
        legend1.Draw()
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)

    rat423.Divide(h422)
    rat422F.Divide(h422)
    rat423FB.Divide(h422)
    rat423F.Divide(h422)
    rat433FB.Divide(h422)
    rat433F.Divide(h422)
    rat433.Divide(h422)
    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.5
    if "2jet" in canName:
        yHigh = 2
    rat423.SetLineWidth(2)
    rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle(varName)
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("p") 
    rat422F.Draw("same p") 
    rat423F.Draw("same p") 
    rat423FB.Draw("same p") 
    rat433.Draw("same p") 
    rat433F.Draw("same p") 
    rat433FB.Draw("same p") 
    
    if doNjet:
        varName+="_AllJets"
        
    canvas_hs.SaveAs(working_dir+"cutsCompare/"+sample+"/CutsCompare_"+canName+"_"+varName+".png")
    canvas_hs.Close()

def pileupEstimateAllSamples(varName,canName):
    for sample in samples:
        if not "signal" in sample:
            continue
        if "WJets" in sample or "ZJets" in sample:
            pileupEstimate(varName,canName,sample+"bb")
        elif "ttbar" in sample:
            if "150" in canName:
                pileupEstimate(varName,canName,sample+"bb")
            else:
                pileupEstimate(varName,canName,sample+"bc")
        else:
            pileupEstimate(varName,canName,sample)
def pileupEstimate(varName,canName,sample):
    print("pileupEstimate: {} {} {}".format(varName,canName,sample))
    
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    
    sample2=sample
    hist_file422.cd()
    histName1=sample2+"_"+canName+"_"+varName+"_2D"
    
    hist_tmp1 = hist_file422.Get(histName1)
    g422=hist_tmp1.Clone()
    h422 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    hist_file422F.cd()
    hist_tmp1d = hist_file422F.Get(histName1)
    g422F=hist_tmp1d.Clone()
    h422F = ROOT.TH1F(histName1+"_FSR_avg",histName1+"_FSR_avg",20,0,100)

    hist_file423.cd()
    histName1=histName1.replace("20J3","30J3")
    hist_tmp1a = hist_file423.Get(histName1)
    g423=hist_tmp1a.Clone()
    h423 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    hist_file423F.cd()
    hist_tmp1c = hist_file423F.Get(histName1)
    g423F=hist_tmp1c.Clone()
    h423F = ROOT.TH1F(histName1+"_FSR_avg",histName1+"_FSR_avg",20,0,100)

    hist_file423FB.cd()
    hist_tmp1b = hist_file423FB.Get(histName1)
    g423FB=hist_tmp1b.Clone()
    h423FB = ROOT.TH1F(histName1+"_FSRb4Cuts_avg",histName1+"_FSRb4Cuts_avg",20,0,100)

    hist_file433.cd()
    histName1=histName1.replace("20J2","30J2")
    hist_tmp1a = hist_file433.Get(histName1)
    g433=hist_tmp1a.Clone()
    h433 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    hist_file433F.cd()
    hist_tmp1c = hist_file433F.Get(histName1)
    g433F=hist_tmp1c.Clone()
    h433F = ROOT.TH1F(histName1+"_FSR_avg",histName1+"_FSR_avg",20,0,100)

    hist_file433FB.cd()
    hist_tmp1b = hist_file433FB.Get(histName1)
    g433FB=hist_tmp1b.Clone()
    h433FB = ROOT.TH1F(histName1+"_FSRb4Cuts_avg",histName1+"_FSRb4Cuts_avg",20,0,100)

    
    totnum422=0
    totsum422=0
    totnum422F=0
    totsum422F=0
    totnum423=0
    totsum423=0
    totnum423F=0
    totsum423F=0
    totnum423FB=0
    totsum423FB=0
    totnum433=0
    totsum433=0
    totnum433F=0
    totsum433F=0
    totnum433FB=0
    totsum433FB=0
    for i in range(g422.GetNbinsX()):
        num422=0
        sum422=0
        num422F=0
        sum422F=0
        num423=0
        sum423=0
        num423F=0
        sum423F=0
        num423FB=0
        sum423FB=0
        num433=0
        sum433=0
        num433F=0
        sum433F=0
        num433FB=0
        sum433FB=0
        for j in range(g422.GetNbinsY()):
            num422+=g422.GetBinContent(i+1,j+1)
            sum422+=g422.GetBinContent(i+1,j+1)*g422.GetYaxis().GetBinLowEdge(j+1)
            num422F+=g422F.GetBinContent(i+1,j+1)
            sum422F+=g422F.GetBinContent(i+1,j+1)*g422F.GetYaxis().GetBinLowEdge(j+1)
            num423+=g423.GetBinContent(i+1,j+1)
            sum423+=g423.GetBinContent(i+1,j+1)*g423.GetYaxis().GetBinLowEdge(j+1)
            num423F+=g423F.GetBinContent(i+1,j+1)
            sum423F+=g423F.GetBinContent(i+1,j+1)*g423F.GetYaxis().GetBinLowEdge(j+1)
            num423FB+=g423FB.GetBinContent(i+1,j+1)
            sum423FB+=g423FB.GetBinContent(i+1,j+1)*g423FB.GetYaxis().GetBinLowEdge(j+1)
            num433+=g433.GetBinContent(i+1,j+1)
            sum433+=g433.GetBinContent(i+1,j+1)*g433.GetYaxis().GetBinLowEdge(j+1)
            num433F+=g433F.GetBinContent(i+1,j+1)
            sum433F+=g433F.GetBinContent(i+1,j+1)*g433F.GetYaxis().GetBinLowEdge(j+1)
            num433FB+=g433FB.GetBinContent(i+1,j+1)
            sum433FB+=g433FB.GetBinContent(i+1,j+1)*g433FB.GetYaxis().GetBinLowEdge(j+1)
        if num422>0:
            h422.SetBinContent(i+1,sum422/num422)
            totnum422+=num422
            totsum422+=sum422
        if num422F>0:
            h422F.SetBinContent(i+1,sum422F/num422F)
            totnum422F+=num422F
            totsum422F+=sum422F
        if num423>0:
            h423.SetBinContent(i+1,sum423/num423)
            totnum423+=num423
            totsum423+=sum423
        if num423F>0:
            h423F.SetBinContent(i+1,sum423F/num423F)
            totnum423F+=num423F
            totsum423F+=sum423F
        if num423FB>0:
            h423FB.SetBinContent(i+1,sum423FB/num423FB)
            totnum423FB+=num423FB
            totsum423FB+=sum423FB
        if num433>0:
            h433.SetBinContent(i+1,sum433/num433)
            totnum433+=num433
            totsum433+=sum433
        if num433F>0:
            h433F.SetBinContent(i+1,sum433F/num433F)
            totnum433F+=num433F
            totsum433F+=sum433F
        if num433FB>0:
            h433FB.SetBinContent(i+1,sum433FB/num433FB)
            totnum433FB+=num433FB
            totsum433FB+=sum433FB
            
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     

    h422.SetLineColor(ROOT.kBlack)
    h422.SetLineWidth(2)
    h422.SetMarkerColor(ROOT.kBlack)
    h422.SetMarkerStyle(8)
    h422.SetMarkerSize(1)
    h422F.SetLineColor(ROOT.kGray+2)
    h422F.SetLineWidth(2)
    h422F.SetMarkerColor(ROOT.kGray+2)
    h422F.SetMarkerStyle(8)
    h422F.SetMarkerSize(1)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h423.SetMarkerColor(ROOT.kBlue)
    h423.SetMarkerStyle(8)
    h423.SetMarkerSize(1)
    h423FB.SetLineColor(ROOT.kRed)
    h423FB.SetLineWidth(2)
    h423FB.SetMarkerColor(ROOT.kRed)
    h423FB.SetMarkerStyle(8)
    h423FB.SetMarkerSize(1)
    h423F.SetLineColor(ROOT.kGreen+3)
    h423F.SetLineWidth(2)
    h423F.SetMarkerColor(ROOT.kGreen+3)
    h423F.SetMarkerStyle(8)
    h423F.SetMarkerSize(1)
    h433.SetLineColor(ROOT.kViolet-1)
    h433.SetLineWidth(2)
    h433.SetMarkerColor(ROOT.kViolet-1)
    h433.SetMarkerStyle(8)
    h433.SetMarkerSize(1)
    h433F.SetLineColor(ROOT.kYellow-3)
    h433F.SetLineWidth(2)
    h433F.SetMarkerColor(ROOT.kYellow-3)
    h433F.SetMarkerStyle(8)
    h433F.SetMarkerSize(1)
    h433FB.SetLineColor(ROOT.kOrange+1)
    h433FB.SetLineWidth(2)
    h433FB.SetMarkerColor(ROOT.kOrange+1)
    h433FB.SetMarkerStyle(8)
    h433FB.SetMarkerSize(1)

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h423.GetMaximum(), h423FB.GetMaximum(), h423F.GetMaximum(), h422F.GetMaximum(), h433.GetMaximum(), h433FB.GetMaximum(), h433F.GetMaximum())*1.5
                    
    rat423 = h423.Clone()
    rat422F = h422F.Clone()
    rat423FB = h423FB.Clone()
    rat423F = h423F.Clone()
    rat433 = h433.Clone()
    rat433F = h433F.Clone()
    rat433FB = h433FB.Clone()

    h422.Draw("hist")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    YTitle = "avg. nSigJets"
    if varName == "nSigJets-nMatchedTruthSigJets":
        YTitle = "avg. (nSigJets-nMatchedTruthSigJets)"
    h422.GetYaxis().SetTitle(YTitle)
    h422.SetYTitle(YTitle)
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetLabelOffset(5)
    
    h423.Draw("same hist")
    h422F.Draw("same hist")
    h423FB.Draw("same hist")
    h423F.Draw("same hist")
    h433.Draw("same hist")
    h433F.Draw("same hist")
    h433FB.Draw("same hist")
    
    if totnum422==0:
        totnum422=1
    if totnum422F==0:
        totnum422F=1
    if totnum423==0:
        totnum423=1
    if totnum423F==0:
        totnum423F=1
    if totnum423FB==0:
        totnum423FB=1
    if totnum433==0:
        totnum433=1
    if totnum433F==0:
        totnum433F=1
    if totnum433FB==0:
        totnum433FB=1

    legend1 = ROOT.TLegend(0.605, 0.6, 0.93, 0.87)
    if "nMatchedTruthSigJets" in varName:
        legend1.SetHeader("              sample,    total avg. x 100")
        legend1.AddEntry(h422, "20J2-20J3, {:.3f}".format(100*totsum422/totnum422), "l")
        legend1.AddEntry(h422F, "20J2-20J3 FSR, {:.3f}".format(100*totsum422F/totnum422F), "l")
        legend1.AddEntry(h423, "20J2-30J3, {:.3f}".format(100*totsum423/totnum423), "l")
        legend1.AddEntry(h423F, "20J2-30J3 FSR, {:.3f}".format(100*totsum423F/totnum423F), "l")
        legend1.AddEntry(h423FB, "20J2-30J3 FSR b4Cuts, {:.3f}".format(100*totsum423FB/totnum423FB), "l")
        legend1.AddEntry(h433, "30J2-30J3, {:.3f}".format(100*totsum433/totnum433), "l")
        legend1.AddEntry(h433F, "30J2-30J3 FSR, {:.3f}".format(100*totsum433F/totnum433F), "l")
        legend1.AddEntry(h433FB, "30J2-30J3 FSR b4Cuts, {:.3f}".format(100*totsum433FB/totnum433FB), "l")
    else:
        legend1.SetHeader("              sample,    total avg.")
        legend1.AddEntry(h422, "20J2-20J3, {:.3f}".format(totsum422/totnum422), "l")
        legend1.AddEntry(h422F, "20J2-20J3 FSR, {:.3f}".format(totsum422F/totnum422F), "l")
        legend1.AddEntry(h423, "20J2-30J3, {:.3f}".format(totsum423/totnum423), "l")
        legend1.AddEntry(h423F, "20J2-30J3 FSR, {:.3f}".format(totsum423F/totnum423F), "l")
        legend1.AddEntry(h423FB, "20J2-30J3 FSR b4Cuts, {:.3f}".format(totsum423FB/totnum423FB), "l")
        legend1.AddEntry(h433, "30J2-30J3, {:.3f}".format(totsum433/totnum433), "l")
        legend1.AddEntry(h433F, "30J2-30J3 FSR, {:.3f}".format(totsum433F/totnum433F), "l")
        legend1.AddEntry(h433FB, "30J2-30J3 FSR b4Cuts, {:.3f}".format(totsum433FB/totnum433FB), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")
    t.DrawLatex(0.15, 0.85, "#it{#bf{"+canName+"}}")

    lower_hs.cd()

    #ROOT.gPad.SetTicky(0)

    rat423.Divide(h422)
    rat422F.Divide(h422)
    rat423FB.Divide(h422)
    rat423F.Divide(h422)
    rat433.Divide(h422)
    rat433F.Divide(h422)
    rat433FB.Divide(h422)

    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.9
    yHigh = 1.05
    if varName == "nSigJets-nMatchedTruthSigJets":
        yLow=0
        yHigh=1.2
    rat423.SetLineWidth(2)
    rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle("ActualMu")
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("hist") 
    rat423F.Draw("same hist") 
    rat422F.Draw("same hist") 
    rat423FB.Draw("same hist") 
    rat433.Draw("same hist") 
    rat433F.Draw("same hist") 
    rat433FB.Draw("same hist") 
    
    if "bb" in sample:
        sample=sample.replace("bb","")
    elif "bc" in sample:
        sample=sample.replace("bc","")
    canvas_hs.SaveAs(working_dir+"cutsCompare/"+sample+"/CutsCompare_"+canName+"_"+varName+"_2D.png")
    canvas_hs.Close()

def makeTTBarDecay(varName,binning,canName):
    print("TTBarDecay")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    
    h422=ROOT.TH1F("TTBarDecay422","TTBarDecay422",12,0,12)
    h4225=ROOT.TH1F("TTBarDecay4225","TTBarDecay4225",12,0,12)
    h423=ROOT.TH1F("TTBarDecay423","TTBarDecay423",12,0,12)
    h433=ROOT.TH1F("TTBarDecay433","TTBarDecay433",12,0,12)
    h432=ROOT.TH1F("TTBarDecay432","TTBarDecay432",12,0,12)

    hist_file422.cd()
    histName="_"+canName+"_"+varName
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1 = hist_file422.Get(histName1)
        h422.Add(hist_tmp1.Clone())

    hist_file4225.cd()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1d = hist_file4225.Get(histName1)
        h4225.Add(hist_tmp1d.Clone())

    hist_file423.cd()
    histName=histName.replace("20J3","30J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1a = hist_file423.Get(histName1)
        h423.Add(hist_tmp1a.Clone())

    hist_file433.cd()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1c = hist_file433.Get(histName1)
        h433.Add(hist_tmp1c.Clone())

    hist_file432.cd()
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1b = hist_file432.Get(histName1)
        h432.Add(hist_tmp1b.Clone())

    h422.SetBinContent(11,h422.GetBinContent(2)+h422.GetBinContent(3)+h422.GetBinContent(4))
    h422.SetBinError(11,math.sqrt(h422.GetBinError(2)**2+h422.GetBinError(3)**2+h422.GetBinError(4)**2))
    h422.SetBinContent(12,h422.GetBinContent(5)+h422.GetBinContent(6)+h422.GetBinContent(7)+h422.GetBinContent(8)+h422.GetBinContent(9)+h422.GetBinContent(10))
    h422.SetBinError(12,math.sqrt(h422.GetBinError(5)**2+h422.GetBinError(6)**2+h422.GetBinError(7)**2+h422.GetBinError(8)**2+h422.GetBinError(9)**2+h422.GetBinError(10)**2))

    h4225.SetBinContent(11,h4225.GetBinContent(2)+h4225.GetBinContent(3)+h4225.GetBinContent(4))
    h4225.SetBinError(11,math.sqrt(h4225.GetBinError(2)**2+h4225.GetBinError(3)**2+h4225.GetBinError(4)**2))
    h4225.SetBinContent(12,h4225.GetBinContent(5)+h4225.GetBinContent(6)+h4225.GetBinContent(7)+h4225.GetBinContent(8)+h4225.GetBinContent(9)+h4225.GetBinContent(10))
    h4225.SetBinError(12,math.sqrt(h4225.GetBinError(5)**2+h4225.GetBinError(6)**2+h4225.GetBinError(7)**2+h4225.GetBinError(8)**2+h4225.GetBinError(9)**2+h4225.GetBinError(10)**2))

    h423.SetBinContent(11,h423.GetBinContent(2)+h423.GetBinContent(3)+h423.GetBinContent(4))
    h423.SetBinError(11,math.sqrt(h423.GetBinError(2)**2+h423.GetBinError(3)**2+h423.GetBinError(4)**2))
    h423.SetBinContent(12,h423.GetBinContent(5)+h423.GetBinContent(6)+h423.GetBinContent(7)+h423.GetBinContent(8)+h423.GetBinContent(9)+h423.GetBinContent(10))
    h423.SetBinError(12,math.sqrt(h423.GetBinError(5)**2+h423.GetBinError(6)**2+h423.GetBinError(7)**2+h423.GetBinError(8)**2+h423.GetBinError(9)**2+h423.GetBinError(10)**2))

    h433.SetBinContent(11,h433.GetBinContent(2)+h433.GetBinContent(3)+h433.GetBinContent(4))
    h433.SetBinError(11,math.sqrt(h433.GetBinError(2)**2+h433.GetBinError(3)**2+h433.GetBinError(4)**2))
    h433.SetBinContent(12,h433.GetBinContent(5)+h433.GetBinContent(6)+h433.GetBinContent(7)+h433.GetBinContent(8)+h433.GetBinContent(9)+h433.GetBinContent(10))
    h433.SetBinError(12,math.sqrt(h433.GetBinError(5)**2+h433.GetBinError(6)**2+h433.GetBinError(7)**2+h433.GetBinError(8)**2+h433.GetBinError(9)**2+h433.GetBinError(10)**2))

    h432.SetBinContent(11,h432.GetBinContent(2)+h432.GetBinContent(3)+h432.GetBinContent(4))
    h432.SetBinError(11,math.sqrt(h432.GetBinError(2)**2+h432.GetBinError(3)**2+h432.GetBinError(4)**2))
    h432.SetBinContent(12,h432.GetBinContent(5)+h432.GetBinContent(6)+h432.GetBinContent(7)+h432.GetBinContent(8)+h432.GetBinContent(9)+h432.GetBinContent(10))
    h432.SetBinError(12,math.sqrt(h432.GetBinError(5)**2+h432.GetBinError(6)**2+h432.GetBinError(7)**2+h432.GetBinError(8)**2+h432.GetBinError(9)**2+h432.GetBinError(10)**2))
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlack)
    h422.SetLineWidth(2)
    h4225.SetLineColor(ROOT.kOrange+1)
    h4225.SetLineWidth(2)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h432.SetLineColor(ROOT.kRed)
    h432.SetLineWidth(2)
    h433.SetLineColor(ROOT.kGreen+3)
    h433.SetLineWidth(2)
            
    rebinning = int(binning.split(",")[0])
    print(rebinning)
    if rebinning != 0:
        h422.Rebin(rebinning)
        h4225.Rebin(rebinning)
        h423.Rebin(rebinning)
        h432.Rebin(rebinning)
        h433.Rebin(rebinning)


    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h4225.GetMaximum(), h423.GetMaximum(), h432.GetMaximum(), h433.GetMaximum())*1.4
                    
    rat423 = h423.Clone()
    rat4225 = h4225.Clone()
    rat432 = h432.Clone()
    rat433 = h433.Clone()
    h422.Draw("hist")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    h422.GetYaxis().SetTitle("Entries")
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetYTitle("Entries")
    h422.SetLabelOffset(5)
    
    h423.Draw("same hist")
    h4225.Draw("same hist")
    h432.Draw("same hist")
    h433.Draw("same hist")
    
    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
    legend1.AddEntry(h4225, "20J2-20J3 FSR, {:.1f}, {:.1f}".format(h4225.GetEntries(),h4225.Integral()), "l")
    legend1.AddEntry(h423, "20J2-30J3, {:.1f}, {:.1f}".format(h423.GetEntries(),h423.Integral()), "l")
    legend1.AddEntry(h433, "20J2-30J3 FSR, {:.1f}, {:.1f}".format(h433.GetEntries(),h433.Integral()), "l")
    legend1.AddEntry(h432, "20J2-30J3 FSR b4Cuts, {:.1f}, {:.1f}".format(h432.GetEntries(),h432.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)

    rat423.Divide(h422)
    rat4225.Divide(h422)
    rat432.Divide(h422)
    rat433.Divide(h422)
    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)
    ind=0
    for label in ["j-j","j-e","j-mu","j-tau","e-e","mu-mu","tau-tau","e-mu","e-tau","mu-tau","SemiLep","DiLep"]:
        rat423.GetXaxis().SetBinLabel(ind+1, label)
        ind+=1

    yLow = 0.5
    yHigh = 4
    if "4jet" in canName:
        yHigh=2.2
    elif "5pjet" in canName:
        yLow = 0
        yHigh = 1.2
    elif "4jet" in canName:
        yHigh=2.6
    elif "2pjet" in canName:
        yHigh=1.2
    
    rat423.SetLineWidth(2)
    rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle(varName)
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("p") 
    rat433.Draw("same p") 
    rat4225.Draw("same p") 
    rat432.Draw("same p") 
    
    canvas_hs.SaveAs(working_dir+"cutsCompare/CutsCompare_"+canName+"_"+varName+".png")
    canvas_hs.Close()



doNotCutCompare = False
NUMJETS={"nJets==2":"2jet","nJets==3":"3jet"} #,"nJets==4":"4jet","nJets>=5":"5pjet","nJets>=2":"2pjet"}
#NUMJETS={"nJets==2":"2jet"}
METCUT={"MET>150&&MET<250":"150_250ptv","MET>150":"150ptv","MET>250":"250ptv","MET>400":"400ptv","MET>250&&MET<400":"250_400ptv"}
#METCUT={"MET>150&&MET<250":"150_250ptv"}
#JETCUT1={"GSCptJ1>45":"45J1"} #,"GSCptJ1>60":"60J1"} #,"GSCptJ1>50":"50J1"} #,"GSCptJ1>70":"70J1"}
#JETCUT2={"GSCptJ2>20":"20J2","GSCptJ2>30":"30J2"} #,"GSCptJ2>25":"25J2"} #,"GSCptJ2>35":"35J2"}
#JETCUT3={"GSCptJ3>20":"20J3","GSCptJ3>30":"30J3"} #,"GSCptJ3>25":"25J3"} #,"GSCptJ3>35":"35J3"}

# For flavour, 15=tau, 5=b, 4=c, <4=l
# rebion,start,end
histNames = {"mBB" : "0,0,400", "GSCMbb" : "0,0,400", "GSCptJ1" : "2,40,500", "GSCptJ2" : "2,20,400", 
             "TruthWZptJ1" : "2,40,500", "TruthWZptJ2" : "2,20,400", "TruthWZptJ3" : "2,20,400", "TruthWZMbb" : "0,0,400", 
             "pTB1" : "2,40,500", "pTB2" : "2,20,400", "pTJ3" : "2,20,400","pTJ4" : "2,20,400", "nMatchedTruthSigJets" : "0,0,10",
             "sumPtJets":"2,0,500", "MindPhiMETJet":"0,0,3.2", "MindPhiMETJet4J":"0,0,3.2", "HT" : "4,250,1000","HT4J" : "4,250,1000",
             "nJets" : "0,0,10", "nSigJets" : "0,0,10","nFwdJets" : "0,0,10","nbJets" : "0,0,10","nTrkJets" : "0,0,10", 
             "Njets_truth_pTjet30" : "0,0,10","mva" : "0,-1,1", "ActualMu" : "2,0,100", "TTBarDecay" : "0,0,10",
             "nSigJets-nMatchedTruthSigJets" : "0,-5,10"}

# rebin,start,end
histNames = {"mBB" : "0,0,300", "mva" : "0,-1,1","sumPtJets":"2,0,500", "dRBB" : "0,0.4,2.5", "MET" : "0,150,600",
             "pTB1" : "2,40,500", "pTB2" : "2,20,400", "pTJ3" : "2,20,400","pTJ4" : "2,20,400", 
             "nMatchedTruthSigJets" : "0,0,10","nJets" : "0,0,10", "nSigJets" : "0,0,10","nFwdJets" : "0,0,10",
             "nbJets" : "0,0,10","nTrkJets" : "0,0,10","nSigJets-nMatchedTruthSigJets" : "0,-5,10",
             "mBBJ" : "2,0,600"}
histNames = {"dRBB" : "0,0.4,2.5", "MET" : "0,150,600"} #,"nSigJets" : "0,0,10","nSigJets-nMatchedTruthSigJets" : "0,-5,10"}
#histNames = {"nSigJets" : "0,0,10","nSigJets-nMatchedTruthSigJets" : "0,-5,10"}
#histNames = {"mBB" : "0,0,400", "nSigJets" : "0,0,10", "TTBarDecay" : "0,0,10", "nTrkJets" : "0,0,10"} #"nSigJets" : "0,0,10","nSigJets-nMatchedTruthSigJets" : "0,-5,10", "TTBarDecay" : "0,0,10"}
#histNames = {"mva" : "0,-1,1"} #, "nTrkJets" : "0,0,10"}
ROOT.gROOT.SetBatch(True)

# signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
#main
#sys.argv.append("-b")# ['ActualMu', 'GSCMbb', 'GSCptJ1', 'GSCptJ2', 'HT', 'HT4J', 'MindPhiMETJet', 'MindPhiMETJet4J', 'Njets_truth_pTjet30', 'TTBarDecay', 'TruthWZMbb', 'TruthWZptJ1', 'TruthWZptJ2', 'TruthWZptJ3', 'mBB', 'mva', 'nFwdJets', 'nJets', 'nMatchedTruthSigJets', 'nSigJets', 'nSigJets-nMatchedTruthSigJets', 'nTrkJets', 'nbJets', 'pTB1', 'pTB2', 'pTJ3', 'pTJ4', 'sumPtJets']
histNamesOrder = histNames.keys()
histNamesOrder.sort()
print("Making plots")
for hName in histNamesOrder:
    print("VARIABLE "+hName)
    m_sigValsTot={}
    for numJets in NUMJETS:
        if "J3" in hName or "BBJ" in hName:
            if numJets == "nJets==2":
                continue
        if "J4" in hName:
            if numJets != "nJets==4":
                continue
        for METCut in METCUT:
            m_sigVals={}
            m_sigValsMVA={}
            m_yieldVals={}
            m_SBVals={}
            for JetCut1 in ["45J1_20J2_20J3","45J1_20J2_30J3","45J1_30J2_30J3"]: #,"60J1_20J2_20J3","60J1_20J2_30J3"]: #,"60J1_30J2_30J3","60J1_30J2_20J3",]:
                #if not ("dRBB" in hName or "pTB1" in hName):
                #    continue
                sys.argv.append("-b")
                ROOT.gStyle.SetPadTickX(1)
                ROOT.gStyle.SetPadTickY(1)
                
                histEx=NUMJETS[numJets]+"_"+METCUT[METCut]+"_"+JetCut1
                CUTS=numJets+"_"+METCut+"-&&PassNonJetCountCuts==1-&&"+JetCut1
                #if numJets == "nJets==2":
                #    if ("30" in JetCut1.split("_")[-1]):
                #        continue
                    
                histEx=histEx+"_SR"
                CUTS+="_SR"
                
                print("Looking at "+hName+" with cuts "+histEx)
                ROOT.gROOT.SetBatch(True)

                binning = histNames[hName]
                if ("20J2_20J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file422,False,"")
                    doDataMC(hName,binning,histEx,hist_file422,True,"")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file422,False,"")
                        doDataMC(hName,binning,histExTemp,hist_file422,True,"")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file422,False,"")
                        doDataMC(hName,binning,histExTemp,hist_file422,True,"")                    
                    if ("Jets" in hName or "jets" in hName) and ("sumPtJets" != hName):
                        if "pTjet30" in hName:
                            cutsComparison(hName,binning,histEx,"signal",False)
                        if numJets == "nJets==2":
                            cutsComparisonAllSamples(hName,binning,histEx,True)
                    #elif hName == "TTBarDecay":
                    #    makeTTBarDecay(hName,binning,histEx)
                    elif "45J1" in JetCut1:
                        cutsComparisonAllSamples(hName,binning,histEx,False)
                    if hName == "nTrkJets":
                        compareDataMC(hName,histEx,False)
                        compareDataMC(hName,histEx,True)
                        histExTemp=histEx.replace("SR","CRLow")
                        compareDataMC(hName,histExTemp,False)
                        compareDataMC(hName,histExTemp,True)
                        histExTemp=histEx.replace("SR","CRHigh")
                        compareDataMC(hName,histExTemp,False)
                        compareDataMC(hName,histExTemp,True)
                    elif "nSigJets" in hName:
                        pileupEstimateAllSamples(hName,histEx)
                        
                    # for the FSR component
                    doDataMC(hName,binning,histEx,hist_file422F,False,"FSR")
                    doDataMC(hName,binning,histEx,hist_file422F,True,"FSR")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file422F,False,"FSR"),
                        doDataMC(hName,binning,histExTemp,hist_file422F,True,"FSR")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file422F,False,"FSR")
                        doDataMC(hName,binning,histExTemp,hist_file422F,True,"FSR")                    
                elif ("20J2_30J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file423,False,"")
                    doDataMC(hName,binning,histEx,hist_file423,True,"")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file423,False,"")
                        doDataMC(hName,binning,histExTemp,hist_file423,True,"")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file423,False,"")
                        doDataMC(hName,binning,histExTemp,hist_file423,True,"")
                    
                    # FSR component
                    doDataMC(hName,binning,histEx,hist_file423F,False,"FSR")
                    doDataMC(hName,binning,histEx,hist_file423F,True,"FSR")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file423F,False,"FSR")
                        doDataMC(hName,binning,histExTemp,hist_file423F,True,"FSR")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file423F,False,"FSR")
                        doDataMC(hName,binning,histExTemp,hist_file423F,True,"FSR")

                    # FSR before component
                    doDataMC(hName,binning,histEx,hist_file423FB,False,"FSR_b4Cuts")
                    doDataMC(hName,binning,histEx,hist_file423FB,True,"FSR_b4Cuts")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file423FB,False,"FSR_b4Cuts")
                        doDataMC(hName,binning,histExTemp,hist_file423FB,True,"FSR_b4Cuts")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file423FB,False,"FSR_b4Cuts")
                        doDataMC(hName,binning,histExTemp,hist_file423FB,True,"FSR_b4Cuts")
                elif ("30J2_30J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file433,False,"")
                    doDataMC(hName,binning,histEx,hist_file433,True,"")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file433,False,"")
                        doDataMC(hName,binning,histExTemp,hist_file433,True,"")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file433,False,"")
                        doDataMC(hName,binning,histExTemp,hist_file433,True,"")
                    
                    # FSR component
                    doDataMC(hName,binning,histEx,hist_file433F,False,"FSR")
                    doDataMC(hName,binning,histEx,hist_file433F,True,"FSR")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file433F,False,"FSR")
                        doDataMC(hName,binning,histExTemp,hist_file433F,True,"FSR")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file433F,False,"FSR")
                        doDataMC(hName,binning,histExTemp,hist_file433F,True,"FSR")

                    # FSR before component
                    doDataMC(hName,binning,histEx,hist_file433FB,False,"FSR_b4Cuts")
                    doDataMC(hName,binning,histEx,hist_file433FB,True,"FSR_b4Cuts")
                    if hName == "mBB":
                        histExTemp=histEx.replace("SR","CRLow")
                        doDataMC(hName,binning,histExTemp,hist_file433FB,False,"FSR_b4Cuts")
                        doDataMC(hName,binning,histExTemp,hist_file433FB,True,"FSR_b4Cuts")
                        histExTemp=histEx.replace("SR","CRHigh")
                        doDataMC(hName,binning,histExTemp,hist_file433FB,False,"FSR_b4Cuts")
                        doDataMC(hName,binning,histExTemp,hist_file433FB,True,"FSR_b4Cuts")
            
            # end of JetCut1
            if ("mBB" in hName or "Mbb" in hName) and "mBBJ" not in hName:
                makeSignificancePlots(m_sigVals,m_yieldVals,m_SBVals,
                                      NUMJETS[numJets]+"_"+METCUT[METCut],hName)
            if "mva" in hName:
                makeSignificancePlots(m_sigValsMVA,m_yieldValsMVA,m_SBValsMVA,
                                      NUMJETS[numJets]+"_"+METCUT[METCut],hName)
    if ("mBB" in hName or "Mbb" in hName or "mva" in hName) and "mBBJ" not in hName:
        printSignificance(hName)

hist_file422.Close()
hist_file422F.Close()
hist_file423.Close()
hist_file423F.Close()
hist_file423FB.Close()
hist_file433.Close()
hist_file433F.Close()
hist_file433FB.Close()

print("")
print("")
print("")
print("All complete")
#exit()

if doNotCutCompare:
    print("Running the corrections comparisons")
else:
    exit()
#hist_file = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J220J3/JetOpt.root","READ")
histNames = {"pTB1" : "46,40,500","pTB2" : "56,20,300","pTJ3" : "56,20,300", "mBB" : "40,0,400"}
#histNames={}
#TruthWZptJ1
#GSCptJ1
#OneMuptJ1

for hName in histNames:
    for numJets in ["nJets==2","nJets==3","nJets==4","nJets>=4"]:
        if hName == "pTJ3":
            if numJets == "nJets==2":
                continue
        for METCut in ["MET>150","MET>250","MET>400","MET>150&&MET<250","MET>250&&MET<400"]:
            #if not ("dRBB" in hName or "pTB1" in hName):
            #    continue
            sys.argv.append("-b")
            ROOT.gStyle.SetPadTickX(1)
            ROOT.gStyle.SetPadTickY(1)
            
            histEx="2tag"
            if numJets=="nJets==2":
                histEx=histEx+"2jet"
            elif numJets=="nJets==3":
                histEx=histEx+"3jet"
            elif numJets=="nJets==4":
                histEx=histEx+"4jet"
            elif numJets=="nJets>=4":
                histEx=histEx+"4pjet"
            if METCut=="MET>150":
                histEx=histEx+"_150ptv"
            elif METCut=="MET>250":
                histEx=histEx+"_250ptv"
            elif METCut=="MET>400":
                histEx=histEx+"_400ptv"
            elif METCut=="MET>150&&MET<250":
                histEx=histEx+"_150_250ptv"
            elif METCut=="MET>250&&MET<400":
                histEx=histEx+"_250_400ptv"

            print("Looking at MET: "+METCut+" var: "+hName+" numJets: "+numJets)
            cutString1 = "EventWeight*(nbJets==2&&nSigJets>=2&&"+numJets+"&&PassNonJetCountCuts==1&&"+METCut+")"
            CUTS="nbJets==2&&nSigJets>=2&&"+numJets+"-&&PassNonJetCountCuts==1-&&"+METCut
            ROOT.gROOT.SetBatch(True)
    
            binning = histNames[hName]
            varName = hName
            histName1 = "h_"+hName
            histName2 = "h_"+hName
            histName3 = "h_"+hName
            drawString1 = varName+">>"+histName1+"("+binning+")"
            drawString2 = varName+">>"+histName2+"("+binning+")"
            drawString3 = varName+">>"+histName3+"("+binning+")"
            if hName == "pTB1":
                histName2="h_TruthWZptJ1"
                drawString2 = "TruthWZptJ1>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ1"
                drawString3 = "GSCptJ1>>"+histName3+"("+binning+")"
            elif hName == "pTB2":
                histName2="h_TruthWZptJ2"
                drawString2 = "TruthWZptJ2>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ2"
                drawString3 = "GSCptJ2>>"+histName3+"("+binning+")"
            elif hName == "pTJ3":
                histName2="h_TruthWZptJ3"
                drawString2 = "TruthWZptJ3>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ3"
                drawString3 = "GSCptJ3>>"+histName3+"("+binning+")"
            elif hName == "mBB":
                histName2="h_OneMuMbb"
                drawString2 = "OneMuMbb>>"+histName2+"("+binning+")"
                histName3="h_GSCMbb"
                drawString3 = "GSCMbb>>"+histName3+"("+binning+")"
            print("Draw("+drawString1+","+cutString1+")")
            print("Draw("+drawString2+","+cutString1+")")
            print("Draw("+drawString3+","+cutString1+")")
    
            VH_chain.Draw(drawString1,ROOT.TCut(cutString1))
            hist_tmp1 = ROOT.gDirectory.Get(histName1)
            hist_tmp1.SetDirectory(0)
            h1 = hist_tmp1.Clone()

            VH_chain.Draw(drawString2,ROOT.TCut(cutString1))
            hist_tmp2 = ROOT.gDirectory.Get(histName2)
            hist_tmp2.SetDirectory(0)
            h2 = hist_tmp2.Clone()

            VH_chain.Draw(drawString3,ROOT.TCut(cutString1))
            hist_tmp3 = ROOT.gDirectory.Get(histName3)
            hist_tmp3.SetDirectory(0)
            h3 = hist_tmp3.Clone()
    
            print("looking at "+hName)
            
            canvas = ROOT.TCanvas(hName, hName, 900, 900)
            upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
            lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
            upper.Draw()
            lower.Draw()
            lower.SetGridy()
            lower.cd().SetBottomMargin(0.3)
            upper.cd()
            ROOT.gStyle.SetOptStat(0)
            
            legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
            legend.SetHeader("              MC,    NEntries,    SoW")
            legend.AddEntry(h1, "PtReco, {:.0f}, {:.2f}".format(h1.GetEntries(),h1.Integral()), "l")
            if "mBB" in varName:
                legend.AddEntry(h2, "OneMu,  {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            else:
                legend.AddEntry(h2, "TruthWZ,  {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            legend.AddEntry(h3, "GSC,    {:.0f}, {:.2f}".format(h3.GetEntries(),h3.Integral()), "l")
            legend.SetBorderSize(0)
            legend.SetFillColor(0)
            legend.SetFillStyle(0)
            
            #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
            #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
            rat1 = h1.Clone()
            rat2 = h2.Clone()
            rat1.Divide(h3) 
            rat2.Divide(h3)
            
            y_max = max(h1.GetMaximum(), h2.GetMaximum(), h3.GetMaximum())*1.4
            
            h1.Draw("hist")
            h1.SetTitle("")
            h1.SetAxisRange(0, y_max, "Y")
            h1.GetYaxis().SetMaxDigits(3)
            h1.GetYaxis().SetTitle("Entries")
            h1.GetYaxis().SetTitleSize(0.04)
            h1.GetYaxis().SetTitleOffset(1.2)
            h1.GetYaxis().SetLabelSize(0.05)
            h1.SetYTitle("Entries")
            h1.SetMarkerColor(ROOT.kBlue)
            #h1.SetMarkerStyle(8)
            #h1.SetMarkerSize(1.5)
            h1.SetLineColor(ROOT.kBlue)    
            h1.SetLineWidth(2)
            h1.SetLabelOffset(5)
            #h1.GetXaxis().SetRangeUser(xLow,xHigh)
            
            h2.Draw("same hist")
            h2.SetMarkerColor(ROOT.kRed)
            h2.SetLineColor(ROOT.kRed)
            h2.SetLineWidth(2)

            h3.Draw("same hist")
            h3.SetMarkerColor(ROOT.kBlack)
            h3.SetLineColor(ROOT.kBlack)
            h3.SetLineWidth(2)
            
            t = ROOT.TLatex()
            t.SetNDC()
            t.SetTextFont(72)
            t.SetTextColor(1)
            t.SetTextSize(0.03)
            t.SetTextAlign(4)
            t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
            t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
            t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
            
            legend.Draw()
            
            lower.cd()
            rat1.SetTitle("")
            rat1.GetXaxis().SetTitle(hName)
            rat1.GetXaxis().SetTitleSize(0.09)
            rat1.GetXaxis().SetTitleOffset(1.05)
            
            #Line = ROOT.TLine(xLow,1.,xHigh,1.)
            #Line.SetLineWidth(2)
            #Line.SetLineColor(ROOT.kBlack)
            yLow = 0
            yHigh = 2
            rat1.SetLineWidth(2)
            rat1.SetLineColor(ROOT.kBlue)
            rat1.SetAxisRange(yLow, yHigh, "Y")
            rat1.GetXaxis().SetLabelSize(0.09)
            rat1.GetYaxis().SetLabelSize(0.07)
            rat1.GetYaxis().SetTitleOffset(0.7)
            rat1.GetYaxis().SetTitleSize(0.06)
            rat1.GetYaxis().SetTitle("bc/bb")
            rat1.GetYaxis().SetNdivisions(506)
            rat2.SetLineColor(ROOT.kRed)
            
            rat1.Draw("")
            rat2.Draw("same")

            #Line.Draw("same")
            #rat.GetXaxis().SetRangeUser(xLow,xHigh)
            
            
            canvas.SaveAs(working_dir+"VH_"+histEx+"_"+hName+".png")
            

        
