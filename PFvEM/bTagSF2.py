import os
import sys
import ROOT
import re
import copy

sys.argv.append("-b")

ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)

# python bTagSF2.py a

#per = "all"
per = sys.argv[1]
JType = "pf19"
f1Name = "Condor/outputs2/em18_33-01_0L/em18_0L_33-01.root"
f2Name = "Condor/outputs2/{}_33-01_0L/{}_0L_33-01.root".format(JType,JType)
#f1Name = "Condor/test_outputs/em18_33-01_0L/em18_0L_33-01_qq.root"
#f2Name = "Condor/test_outputs/{}_33-01_0L/{}_0L_33-01_qq.root".format(JType,JType)


weights = ["WeightSF2j_MET","WeightSF3j_MET","WeightSF4j_MET",
           "LumiMCPUSF2j_MET","LumiMCPUSF3j_MET","LumiMCPUSF4j_MET",
           "JVTbTagSF2j_MET","JVTbTagSF3j_MET","JVTbTagSF4j_MET",
           "bTagSF2j_MET","bTagSF3j_MET","bTagSF4j_MET",
           "TriggerSF2j_MET","TriggerSF3j_MET","TriggerSF4j_MET",
           "JVTSF2j_MET","JVTSF3j_MET","JVTSF4j_MET",
           "PUSF2j_MET","PUSF3j_MET","PUSF4j_MET",
           "LumiSF2j_MET","LumiSF3j_MET","LumiSF4j_MET",
           "MCSF2j_MET","MCSF3j_MET","MCSF4j_MET",
           "MET2j_w","MET3j_w","MET4j_w",
           "ST2j_MET","ST3j_MET","ST4j_MET",
           "PTB12j_MET","PTB13j_MET","PTB14j_MET",
           "PTB22j_MET","PTB23j_MET","PTB24j_MET",
           "PTBB2j_MET","PTBB3j_MET","PTBB4j_MET",
           "PTJ32j_MET","PTJ33j_MET","PTJ34j_MET",
           "PTBBJ2j_MET","PTBBJ3j_MET","PTBBJ4j_MET",
           "mBB2j_MET","mBB3j_MET","mBB4j_MET",
           "dPhiBB2j_MET","dPhiBB3j_MET","dPhiBB4j_MET",
           "dRBB2j_MET","dRBB3j_MET","dRBB4j_MET",
           "dPhiMETdijet2j_MET","dPhiMETdijet3j_MET","dPhiMETdijet4j_MET",
           "dPhiMETMPT2j_MET","dPhiMETMPT3j_MET","dPhiMETMPT4j_MET",
           "MindPhiMETJet2j_MET","MindPhiMETJet3j_MET","MindPhiMETJet4j_MET",]



met_weights = ["MET2j_w","MET3j_w","MET4j_w",
               "WeightSF2j_MET","WeightSF3j_MET","WeightSF4j_MET",
               "MET2j","MET3j","MET4j",
               "ST2j","ST3j","ST4j",
               "PTB12j","PTB13j","PTB14j",
               "PTB22j","PTB23j","PTB24j",
               "PTBB2j","PTBB3j","PTBB4j",
               "PTJ32j","PTJ33j","PTJ34j",
               "PTBBJ2j","PTBBJ3j","PTBBJ4j",
               "mBB2j","mBB3j","mBB4j",
               "dPhiBB2j","dPhiBB3j","dPhiBB4j",
               "dRBB2j","dRBB3j","dRBB4j",
               "dRBBReg2j","dRBBReg3j","dRBBReg4j",
               "MET_2j_cuts","MET_3j_cuts","MET_4j_cuts",
               "nJ_2j_cuts","nJ_3j_cuts","nJ_4j_cuts",
               "nSigJ_2j_cuts","nSigJ_3j_cuts","nSigJ_4j_cuts",
               "nB_2j_cuts","nB_3j_cuts","nB_4j_cuts",
               "pTB1_2j_cuts","pTB1_3j_cuts","pTB1_4j_cuts",
               "dPhiMETBB_2j_cuts","dPhiMETBB_3j_cuts","dPhiMETBB_4j_cuts",
               "dPhiBB_2j_cuts","dPhiBB_3j_cuts","dPhiBB_4j_cuts",
               "dPhiMETMPT_2j_cuts","dPhiMETMPT_3j_cuts","dPhiMETMPT_4j_cuts",
               "MindPhiMETJet_2j_cuts","MindPhiMETJet_3j_cuts","MindPhiMETJet_4j_cuts",
               "ST_2j_cuts","ST_3j_cuts","ST_4j_cuts",
               "met_nonMain_2j","met_nonMain_3j","met_nonMain_4j",
               "met_Trigger_2j","met_Trigger_3j","met_Trigger_4j",
               "met_PU_2j","met_PU_3j","met_PU_4j",
               "met_MC_2j","met_MC_3j","met_MC_4j",
               "met_Lumi_2j","met_Lumi_3j","met_Lumi_4j",
               "met_JVT_2j","met_JVT_3j","met_JVT_4j",
               "met_bTag_2j","met_bTag_3j","met_bTag_4j",
               "dPhiMETdijet2j","dPhiMETdijet3j","dPhiMETdijet4j",
               "dPhiMETMPT2j","dPhiMETMPT3j","dPhiMETMPT4j",
               "MindPhiMETJet2j","MindPhiMETJet3j","MindPhiMETJet4j",]

OtherHists = ["pTJ1_GSC","pTJ2_GSC","pTJ3_GSC",
              "pTJ1_OneMu","pTJ2_OneMu","pTJ3_OneMu",
              "pTBB_GSC","pTBB_OneMu","pTBBJ_GSC","pTBBJ_OneMu"]


weights = []
met_weights = []

if per != "all":
    f1Name = "Condor/test_outputs/em18_33-01_0L/em18_0L_{}_33-01.root".format(per)
    f2Name = "Condor/test_outputs/{}_33-01_0L/{}_0L_{}_33-01.root".format(JType,JType,per)

print("Reading EM from file "+f1Name)
print("Reading PF from file "+f2Name)

work_dir = "histograms/weightsPerMET_SR/"
histNameExtra = ""

print("Outputting to directory "+work_dir+" with extra name "+histNameExtra)

f1 = ROOT.TFile.Open(f1Name)
f2 = ROOT.TFile.Open(f2Name)


for hName in weights:
    print("looking at "+hName)
    H1aName = ""
    H2aName = ""
    if "2j" in hName:
        H1aName = "MET2j"
        H2aName = "MET2j"
    elif "3j" in hName:
        H1aName = "MET3j"
        H2aName = "MET3j"
    elif "4j" in hName:
        H1aName = "MET4j"
        H2aName = "MET4j"

    f1.cd()
    H1 = f1.Get(hName).Clone()
    H1a = f1.Get(H1aName).Clone()
    f2.cd()
    H2 = f2.Get(hName).Clone()
    H2a = f2.Get(H2aName).Clone()
    #print("h1 "+H1.GetName()+" h1a "+H1a.GetName()+" H2 "+H2.GetName()+" H2a "+H2a.GetName())

    H1.Rebin(2)
    H1a.Rebin(2)
    H2.Rebin(2)
    H2a.Rebin(2)

    canvas = ROOT.TCanvas(hName+" compare", hName+" compare", 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.65, 0.87, 0.87)
    ROOT.gStyle.SetOptStat(0)

    #print("bin 1 of h1 {} h1a {} h2 {} h2a {}".format(H1.GetBinContent(1),H1a.GetBinContent(1),H2.GetBinContent(1),H2a.GetBinContent(1)))
    H1.Divide(H1a)
    H2.Divide(H2a)
    #print("bin 1 of h1 {} h2 {} ".format(H1.GetBinContent(1),H2.GetBinContent(1)))
    
    H1.Draw("hist")
    H2.Draw("hist same")
    
    R2 = H2.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum())
    y_min = min(H1.GetMinimum(), H2.GetMinimum())
    fact = 1.01
    if "Lumi" in hName or "MC" in hName:
        fact=1.1
    H1.SetAxisRange(y_min/fact, y_max * fact, "Y")    
    var = hName.split("SF")[0]
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("Avg. "+var)
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetLineColor(ROOT.kBlue)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)
    
    H2.SetLineColor(ROOT.kRed)
    H2.SetLineWidth(2)
    
    R2.SetLineColor(ROOT.kBlack)
    R2.SetMarkerColor(ROOT.kBlack)
    R2.SetMarkerSize(2)
    R2.SetLineWidth(2)
    
    legend.SetBorderSize(0)
    legend.AddEntry(H1, "em18 33-01", "l")
    legend.AddEntry(H2, JType+" 33-01", "l")
    legend.Draw()
    
    lower.cd()
    R2.Divide(H1)
    #print("bin 1 of r1 {}".format(R2.GetBinContent(1)))
    
    R2.SetTitle("")
    R2.GetXaxis().SetTitle("MET (GeV)")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    dy = 1
    if "bTag" in hName:
        dy=0.02
    elif "PU" in hName:
        dy=0.02
    elif "Lumi" in hName:
        dy=0.02
    elif "JVT" in hName:
        dy=0.02
    elif "MC" in hName:
        dy=0.02
    elif "Trigger" in hName:
        dy=0.01
    elif "Weight" in hName:
        dy=0.03
    elif "MET" in hName:
        dy=0.03
    #if "4j" in hName:
    #    dy*=5
    R2.SetAxisRange(1-dy, 1+dy, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("pf/em")
    R2.GetYaxis().SetNdivisions(506)
    
    R2.Draw()
    
    canvas.SaveAs(work_dir+"{}_{}_{}_avg{}.png".format(hName,per,JType,histNameExtra))

for hName in met_weights:
    print("looking at {}".format(hName))
    f1.cd()
    H1 = f1.Get(hName).Clone()
    f2.cd()
    H2 = f2.Get(hName).Clone()

    if H1.GetNbinsX()%2 == 0:
        H1.Rebin(2)
        H2.Rebin(2)

    canvas = ROOT.TCanvas(hName+" compare", hName+" compare", 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.65, 0.87, 0.87)
    ROOT.gStyle.SetOptStat(0)
    
    H1.Draw("hist")
    H2.Draw("hist same")
    
    R2 = H2.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum())
    H1.SetAxisRange(0.0, y_max * 1.2, "Y")    
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("Entries")
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetLineColor(ROOT.kBlue)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)
    
    H2.SetLineColor(ROOT.kRed)
    H2.SetLineWidth(2)

    #print("EM {} PF {} PF/EM {}".format(H1.Integral(),H2.Integral(),H2.Integral()/H1.Integral()))
    #print("EM {} PF {} PF/EM {}".format(H1.Integral(4,40),H2.Integral(4,40),H2.Integral(4,40)/H1.Integral(4,40)))
    #print("{}".format(H1.Integral()))
    #print("bin 3 EM {} PF {} PF/EM {}".format(H1.GetBinContent(3),H2.GetBinContent(3),H2.GetBinContent(3)/H1.GetBinContent(3)))
    R2.SetLineColor(ROOT.kBlack)
    R2.SetMarkerColor(ROOT.kBlack)
    R2.SetMarkerSize(2)
    R2.SetLineWidth(2)
    
    legend.SetBorderSize(0)
    legend.AddEntry(H1, "em18 33-01", "l")
    legend.AddEntry(H2, JType+" 33-01", "l")
    legend.Draw()
    
    lower.cd()
    R2.Divide(H1)
    dy = 0.1
    centre = 1
    if "_cuts" in hName and not ("dPhiBB" in hName or "dPhiMETMPT" in hName or "MindPhiMETJet" in hName or "ST" in hName):
        dy=0.1
        centre = 0.9 

    for i in range(20):
        cont = R2.GetBinContent(i+1)
        if (cont > centre+dy or cont < centre-dy) and (cont>0):
            cent = R2.GetBinCenter(i+1)
            print("in bin {}, content is {}".format(cent,cont))
    

    R2.SetTitle("")
    R2.GetXaxis().SetTitle(hName)
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    #if "4j" in hName:
    #    dy*=2
    R2.SetAxisRange(centre-dy, centre+dy, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("pf/em")
    R2.GetYaxis().SetNdivisions(506)
    
    R2.Draw()
    
    canvas.SaveAs(work_dir+"{}_{}_{}{}.png".format(hName,per,JType,histNameExtra))


for ind in [2,3,4]:
    for Name in OtherHists:
        hName = "{}jet_inc_{}".format(ind,Name)
        print("looking at {}".format(hName))
        f1.cd()
        H1 = f1.Get(hName+"_cuts_w").Clone()
        f2.cd()
        H2 = f2.Get(hName+"_cuts_w").Clone()

        H1.Rebin(10)
        H2.Rebin(10)

        canvas = ROOT.TCanvas(hName+" compare", hName+" compare", 900, 900)
        upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
        lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
        upper.Draw()
        lower.Draw()
        lower.SetGridy()
        lower.cd().SetBottomMargin(0.3)
        upper.cd()
        legend = ROOT.TLegend(0.65, 0.65, 0.87, 0.87)
        ROOT.gStyle.SetOptStat(0)

        H1.Draw("hist")
        H2.Draw("hist same")

        R2 = H2.Clone()

        y_max = max(H1.GetMaximum(), H2.GetMaximum())
        H1.SetAxisRange(0.0, y_max * 1.2, "Y")    
        H1.SetTitle("")
        H1.GetYaxis().SetMaxDigits(3)
        H1.GetYaxis().SetTitle("Entries")
        H1.GetYaxis().SetTitleSize(0.04)
        H1.GetYaxis().SetTitleOffset(1.2)
        H1.SetLineColor(ROOT.kBlue)
        H1.SetLineWidth(2)
        H1.SetLabelOffset(5)

        H2.SetLineColor(ROOT.kRed)
        H2.SetLineWidth(2)

        #print("EM {} PF {} PF/EM {}".format(H1.Integral(),H2.Integral(),H2.Integral()/H1.Integral()))
        #print("EM {} PF {} PF/EM {}".format(H1.Integral(4,40),H2.Integral(4,40),H2.Integral(4,40)/H1.Integral(4,40)))
        #print("{}".format(H1.Integral()))
        #print("bin 3 EM {} PF {} PF/EM {}".format(H1.GetBinContent(3),H2.GetBinContent(3),H2.GetBinContent(3)/H1.GetBinContent(3)))
        R2.SetLineColor(ROOT.kBlack)
        R2.SetMarkerColor(ROOT.kBlack)
        R2.SetMarkerSize(2)
        R2.SetLineWidth(2)

        legend.SetBorderSize(0)
        legend.AddEntry(H1, "em18 33-01", "l")
        legend.AddEntry(H2, JType+" 33-01", "l")
        legend.Draw()

        lower.cd()
        R2.Divide(H1)
        dy = 0.1
        centre = 1

        for i in range(20):
            cont = R2.GetBinContent(i+1)
            if (cont > centre+dy or cont < centre-dy) and (cont>0):
                cent = R2.GetBinCenter(i+1)
                print("in bin {}, content is {}".format(cent,cont))


        R2.SetTitle("")
        R2.GetXaxis().SetTitle(hName)
        R2.GetXaxis().SetTitleSize(0.09)
        R2.GetXaxis().SetTitleOffset(1.05)
        #if "4j" in hName:
        #    dy*=2
        R2.SetAxisRange(centre-dy, centre+dy, "Y")
        R2.GetXaxis().SetLabelSize(0.09)
        R2.GetYaxis().SetLabelSize(0.07)
        R2.GetYaxis().SetTitleOffset(0.7)
        R2.GetYaxis().SetTitleSize(0.06)
        R2.GetYaxis().SetTitle("pf/em")
        R2.GetYaxis().SetNdivisions(506)

        R2.Draw()

        canvas.SaveAs(work_dir+"{}_{}_{}{}.png".format(hName,per,JType,histNameExtra))
