"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
#import argparse



sys.argv.append("-b")
print ("Opening files")
#File1="/eos/user/r/ratkin/Reader_output/tag_33-04/32-15/ZvvHbb.root"
#File2="/eos/user/r/ratkin/Reader_output/tag_33-04/pf19/ZvvHbb.root"
#File1="/eos/user/r/ratkin/Reader_output/tag_33-04/pf19/0L_33-01_e_Resolved_D/ggZvvHbb_PwPy8.root"
#File2="/eos/user/r/ratkin/Reader_output/tag_33-04/pf19/0L_33-04_e_Resolved_D/ggZvvHbb_PwPy8.root"
#nom="pf19"
#oldOrNew1 = sys.argv[3] if sys.argv[3]=="new" else ""
#oldOrNew2 = sys.argv[4] if sys.argv[4]=="new" else ""
#oldOrNew1 = "new"
#oldOrNew2 = "new"
RDF = ROOT.ROOT.RDataFrame
def get_FSRCut(df):
    return df.Define("FSRCut","TLorentzVector L1, L2, L3; L1.SetPtEtaPhiM(pTB1,etaB1,phiB1,mB1); L2.SetPtEtaPhiM(pTB2,etaB2,phiB2,mB2); L1.SetPtEtaPhiM(pTJ3,etaJ3,phiJ3,mJ3); return L1.DeltaR(L3)+L2.DeltaR(L3);")
def get_FSRCutGSC(df):
    return df.Define("FSRCutGSC","TLorentzVector L1, L2, L3; L1.SetPtEtaPhiM(GSCptJ1,GSCetaJ1,GSCphiJ1,GSCmJ1); L2.SetPtEtaPhiM(GSCptJ2,GSCetaJ2,GSCphiJ2,GSCmJ2); L1.SetPtEtaPhiM(pTJ3,etaJ3,phiJ3,mJ3); return L1.DeltaR(L3)+L2.DeltaR(L3);")

# 10% 2jet, CRLow =
cut_CRLow_2j = "dRBB < (3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 95% 2jet, CRHigh =
cut_CRHigh_2j = "dRBB > (8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))"
#SR 2jet
cut_SR_2j = "dRBB<=(8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))&&dRBB>=(3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 10% 3jet, CRLow =
cut_CRLow_3j = "dRBB < (4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))"
# 85% 3jet, CRHigh =
cut_CRHigh_3j = "dRBB > (7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"
# SR
cut_SR_3j = "dRBB>=(4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))&&dRBB<=(7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"

samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"]}
chains_by_type  = {_type:ROOT.TChain("Nominal") for _type in samples_by_type.keys()}
MC_chain = ROOT.TChain("Nominal")

dirs = ["/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_a_Resolved_D_D/fetch/data-MVATree/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_d_Resolved_D_D/fetch/data-MVATree/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_e_Resolved_D_D/fetch/data-MVATree/"]

# for quick running
#dirs = ["/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation/JetPtOptimisation/forQuickRun/"]
#samples_by_type = {"data" : ["data*"], "ZJets" : ["Znunu*"], "WJets" : ["Wmunu*"],
#                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Zqq*"],
#                   "signal" : ["qq*"]}

for _type, samples in samples_by_type.iteritems():
    if _type != "signal":
        continue
    for sample in samples:
        for d in dirs:
            #print('adding {0} to chain {1}'.format(d + sample, _type))
            chains_by_type[_type].Add(d + sample)
            if _type != "data":
                MC_chain.Add(d + sample)

dataframes_by_type_1 = {_type:RDF(chain) for _type, chain in chains_by_type.iteritems()}
#print(dataframes_by_type_1["signal"].Count().GetValue())
dataframes_by_type_0 = {"signal":get_FSRCut(dataframes_by_type_1["signal"])}
dataframes_by_type = {"signal":get_FSRCutGSC(dataframes_by_type_0["signal"])}
sigDF_150_250ptv = dataframes_by_type["signal"].Filter("nbJets==2").Filter("nSigJets>=2").Filter("PassNonJetCountCuts==1").Filter("nJets==3").Filter("MET>150").Filter("MET<250").Filter("Description==\"SR\"")
sigDF_250_400ptv = dataframes_by_type["signal"].Filter("nbJets==2").Filter("nSigJets>=2").Filter("PassNonJetCountCuts==1").Filter("nJets==3").Filter("MET>250").Filter("MET<400").Filter("Description==\"SR\"")
sigDF_400ptv = dataframes_by_type["signal"].Filter("nbJets==2").Filter("nSigJets>=2").Filter("PassNonJetCountCuts==1").Filter("nJets==3").Filter("MET>400").Filter("Description==\"SR\"")
sigDF = dataframes_by_type["signal"].Filter("nbJets==2").Filter("nSigJets>=2").Filter("PassNonJetCountCuts==1").Filter("nJets==3").Filter("MET>150").Filter("MET<400").Filter("Description==\"SR\"")
#print(sigDF.Count().GetValue())
sigDF_fsr = sigDF.Filter("FSRCut < 1.95 * exp(-0.0013 * MET)")
#print(sigDF_fsr.Count().GetValue())
sigDF_fsrGSC = sigDF.Filter("FSRCutGSC < 1.95 * exp(-0.0013 * MET)")
print(sigDF_fsrGSC.Count().GetValue())
sigDF_fsrBoth = sigDF_fsr.Filter("FSRCutGSC < 1.95 * exp(-0.0013 * MET)")
print(sigDF_fsrBoth.Count().GetValue())

sigDF_150_250ptv_fsr = sigDF_150_250ptv.Filter("FSRCut < 1.95 * exp(-0.0013 * MET)")
print("150 250 {} {} {}".format(sigDF_150_250ptv_fsr.Count().GetValue(),sigDF_150_250ptv.Count().GetValue(),float(sigDF_150_250ptv_fsr.Count().GetValue())/float(sigDF_150_250ptv.Count().GetValue())))
sigDF_250_400ptv_fsr = sigDF_250_400ptv.Filter("FSRCut < 1.95 * exp(-0.0013 * MET)")
print("250 400 {} {} {}".format(sigDF_250_400ptv_fsr.Count().GetValue(),sigDF_250_400ptv.Count().GetValue(),float(sigDF_250_400ptv_fsr.Count().GetValue())/float(sigDF_250_400ptv.Count().GetValue())))
sigDF_400ptv_fsr = sigDF_400ptv.Filter("FSRCut < 1.95 * exp(-0.0013 * MET)")
print("400 {} {} {}".format(sigDF_400ptv_fsr.Count().GetValue(),sigDF_400ptv.Count().GetValue(),float(sigDF_400ptv_fsr.Count().GetValue())/float(sigDF_400ptv.Count().GetValue())))


ROOT.gROOT.SetBatch(True)

#hFSRCut = ROOT.TH2F("hFSRCut","hFSRCut", 25,150,400,20,0,2)
print("making model")
h_model = ROOT.RDF.TH2DModel("hFSRCut","hFSRCut", 25,150,400,13,0.4,1.7)
hist1 = sigDF.Histo2D(h_model, "MET", "FSRCut", "EventWeight")
print("made model")
exit()
#hist2 = ROOT.gDirectory.Get("hFSRCut")
#hist2.SetDirectory(0)
hist = hist1.Clone()
print(hist.Integral())

canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
#(x1,y1,x2,y2) 
#upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.025, 0.995, 0.995)
#upper_hs.Draw()
#upper_hs.cd().SetBottomMargin(0.3)
ROOT.gStyle.SetOptStat(0)

hist.Draw("colz")
hist.SetTitle("")
hist.GetYaxis().SetMaxDigits(3)
hist.GetYaxis().SetTitle("dR(B1,J3)+dR(B2,J3)")
hist.GetYaxis().SetTitleSize(0.04)
hist.GetYaxis().SetTitleOffset(1.2)
hist.GetYaxis().SetLabelSize(0.05)
hist.SetYTitle("dR(B1,J3)+dR(B2,J3)")
#hist.SetLabelOffset(5)
hist.GetXaxis().SetTitle("MET (GeV)")
hist.GetXaxis().SetTitleSize(0.04)
hist.GetXaxis().SetTitleOffset(1.05)
hist.GetXaxis().SetLabelSize(0.05)

t = ROOT.TLatex()
t.SetNDC()
t.SetTextFont(72)
t.SetTextColor(1)
t.SetTextSize(0.03)
t.SetTextAlign(4)
t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Internal}}")
t.DrawLatex(0.15, 0.80, "#it{#bf{Signal 3jet SR}}")

m_fit_func_FSRCut = ROOT.TF1("fit_func_FSRCut", "[0]+[1]*exp(-[2]*x)", 150, 500)
m_fit_func_FSRCut.SetParameters(0, 1.95, 0.0013)
m_fit_func_FSRCut.Draw("same")

canvas_hs.SaveAs("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/FSRCut_SR_zoomed.png")
exit()



#print(chains_by_type["ZJets"].GetEntries())
samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}
colours = {"signal" : ROOT.kRed, "WJets" : ROOT.kGreen+3, "ZJets" : ROOT.kAzure+1, "ttbar" : ROOT.kOrange, "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

m_sigVals={}
m_yieldVals={}
m_SBVals={}

working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)

#histNames = {"dEtaBB" : "70,0,7","dPhiBB" : "30,0,3", "dRBB" : "40,0,4", "dPhiMETdijetResolved" : "16,2.4,3.2", "pTBB" : "45,50,500",
#             "pTB1" : "46,40,500","pTB2" : "28,20,300","MindPhiMETJet" : "28,0.4,3.2", "nSigJets" : "10,0,10","nFwdJets" : "10,0,10",
#             "dPhiMETMPT" : "20,0,1", "flavB1" : "19,-2,17","flavB2" : "19,-2,17", "metSig" : "30,0,30", "MET" : "35,150,500",
#             "etaB1" : "100,-5,5", "etaB2" : "100,-5,5", "flavJ3" : "19,-2,17", "pTJ3" : "28,20,300", "MV2c10B1" : "20,-1,1", "MV2c10B2" : "20,-1,1",
#             "mBB" : "40,0,400", "mBBJ" : "40,0,400", "pTBBJ" : "45,50,500", "etaJ3" : "100,-5,5", "nbJets" : "5,0,5",
#             "bin_MV2c10B1" : "7,0,7", "bin_MV2c10B2" : "7,0,7", "bin_MV2c10J3" : "7,0,7" }

histNames = {"dRBB" : "40,0,4","pTBB" : "45,50,500","pTB1" : "46,40,500","pTB2" : "28,20,300",
             "MET" : "35,150,500", "pTJ3" : "28,20,300", 
             "mBB" : "40,0,400", "mBBJ" : "40,0,400", "pTBBJ" : "45,50,500",}
# For flavour, 15=tau, 5=b, 4=c, <4=l


def getSignificance(S,B,vtag):
    i = S.FindBin(50)
    binMax = S.FindBin(250)
    summ=0
    while i <= binMax:
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)

def doBlinding(S1,B1,h1,var):
    thresh = 0.1
    if "mBB" in var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            i+=1
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1

    return h1

def doDataMC(varName,binning,cutString1,canName):
    print("Doing DataMC")
    histograms={}
    
    print("Looping to create histograms")
    for samp,chain in chains_by_type.iteritems():
        histName1=samp+"_"+varName
        drawString1 = varName+">>"+histName1+"("+binning+")"
        
        chain.Draw(drawString1,ROOT.TCut(cutString1))
        hist_tmp1 = ROOT.gDirectory.Get(histName1)
        hist_tmp1.SetDirectory(0)
        histograms[samp]=hist_tmp1.Clone()

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    for sampl in histograms:
        if sampl != "data":
            histograms[sampl].SetLineColor(colours[sampl])
            histograms[sampl].SetFillColor(colours[sampl])
        else:
            histograms[sampl].SetLineColor(colours[sampl])
            histograms[sampl].SetMarkerColor(colours[sampl])
            histograms[sampl].SetLineWidth(2)
            histograms[sampl].SetMarkerStyle(8)
            histograms[sampl].SetMarkerSize(1)

    hs = ROOT.THStack("hs","")
    hs.Add(histograms["ttbar"])
    hs.Add(histograms["stop"])
    hs.Add(histograms["WJets"])
    hs.Add(histograms["ZJets"])
    hs.Add(histograms["diboson"])
    hs.Add(histograms["signal"])
    
    prevYield=0
    hd = histograms["data"].Clone()
    print("data: {}".format(hd.Integral()))
    hb = histograms["ttbar"].Clone()
    print("ttbar: {}".format(hb.Integral()-prevYield))
    prevYield=hb.Integral()
    hb.Add(histograms["stop"].Clone())
    print("stop: {}".format(hb.Integral()-prevYield))
    prevYield=hb.Integral()
    hb.Add(histograms["WJets"].Clone())
    print("WJets: {}".format(hb.Integral()-prevYield))
    prevYield=hb.Integral()
    hb.Add(histograms["ZJets"].Clone())
    print("ZJets: {}".format(hb.Integral()-prevYield))
    prevYield=hb.Integral()
    hb.Add(histograms["diboson"].Clone())
    print("diboson: {}".format(hb.Integral()-prevYield))
    prevYield=hb.Integral()
    hsig = histograms["signal"].Clone()

    line=canName.split("_")
    
    if "mBB" in varName or "Mbb" in varName:
        keyName = ""
        if "2jet" not in line[0]:
            keyName = line[-4]+"_"+line[-3]+"_"+line[-2]
        else:
            keyName = line[-3]+"_"+line[-2]
        print("filling dicts "+keyName)
        m_sigVals[keyName]=getSignificance(hsig,hb,canName)
        m_yieldVals[keyName]=hsig.Integral()
        m_SBVals[keyName]=hsig.Integral()/hb.Integral()
        print("Signal yield: {} background yield: {}".format(hsig.Integral(),hb.Integral()))
        print("S/B: {}".format(hsig.Integral()/hb.Integral()))
    
    
    return
    doBlinding(hsig,hb,hd,varName)
    print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.3
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same")
    
    legend1 = ROOT.TLegend(0.605, 0.62, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}, {:.1f}".format(histograms["data"].GetEntries(),histograms["data"].Integral()), "pl")
    legend1.AddEntry(histograms["signal"], "signal, {:.1f}, {:.1f}".format(histograms["signal"].GetEntries(),histograms["signal"].Integral()), "f")
    legend1.AddEntry(histograms["diboson"], "diboson, {:.1f}, {:.1f}".format(histograms["diboson"].GetEntries(),histograms["diboson"].Integral()), "f")
    legend1.AddEntry(histograms["ZJets"], "ZJets, {:.1f}, {:.1f}".format(histograms["ZJets"].GetEntries(),histograms["ZJets"].Integral()), "f")
    legend1.AddEntry(histograms["WJets"], "WJets, {:.1f}, {:.1f}".format(histograms["WJets"].GetEntries(),histograms["WJets"].Integral()), "f")
    legend1.AddEntry(histograms["stop"], "stop, {:.1f}, {:.1f}".format(histograms["stop"].GetEntries(),histograms["stop"].Integral()), "f")
    legend1.AddEntry(histograms["ttbar"], "ttbar, {:.1f}, {:.1f}".format(histograms["ttbar"].GetEntries(),histograms["ttbar"].Integral()), "f")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.5
    yHigh = 0.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("") 

    print(rat.GetBinContent(3))
    print(rat.GetBinContent(4))
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetTitle("")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.09)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)


    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.995, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    rat1.Draw("Y+")


    canvas_hs.SaveAs(working_dir+varName+"_"+canName+"_DataMC.png")

def makeSignificancePlots(sigVals,yieldVals,SBVals,JetMET,varName):
    h_sig = ROOT.TH1F("sig","sig",70,0,70)
    h_yield = ROOT.TH1F("yield","yield",70,0,70)
    h_SB = ROOT.TH1F("SB","SB",70,0,70)
    gRandom = ROOT.TRandom()

    i=0
    print("\n"+JetMET+"  Sig   Yield   S/B")
    for JetCut1 in ["45J1","50J1","60J1","70J1"]:
        for JetCut2 in ["20J2","25J2","30J2","35J2"]:
            for JetCut3 in ["20J3","25J3","30J3","35J3"]:
                i+=1
                keyName = JetCut1+"_"+JetCut2
                keyName1=""
                if "2jet" not in JetMET:
                    keyName += "_"+JetCut3
                    keyName1 = "45J1_20J2_20J3"
                else:
                    keyName1 = "45J1_20J2"
                #if JetCut1=="45J1" and JetCut2=="20J2" and JetCut3=="20J3":
                h_sig.SetBinContent(i,sigVals[keyName])
                h_yield.SetBinContent(i,yieldVals[keyName])
                h_SB.SetBinContent(i,SBVals[keyName])
                print(keyName+":  {:.3f}({:.2f})  {:.3f}({:.2f})  {:.3f}({:.2f})".format(
                    sigVals[keyName],100*(sigVals[keyName]/sigVals[keyName1]-1),
                    yieldVals[keyName],100*(yieldVals[keyName]/yieldVals[keyName1]-1),
                    SBVals[keyName],100*(SBVals[keyName]/SBVals[keyName1]-1)))
                #else:
                #    h_sig.SetBinContent(i,sigVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
                #    h_yield.SetBinContent(i,yieldVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
                #    h_SB.SetBinContent(i,SBVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
    print("")

    canvasSigs = ROOT.TCanvas("SigYields", "SigYields", 900, 900)
    upperSigs = ROOT.TPad("upperSigs", "upperSigs", 0.025, 0.445, 0.995, 0.995)
    upperSigs.Draw()
    upperSigs.cd()
    #upperSigs.SetGridy()
    #upperSigs.SetBottomMargin(0.3)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gPad.SetTicky(0)
    
    h_sig.SetTitle("")
    h_sig.GetXaxis().SetTitle("")
    h_sig.GetXaxis().SetTitleSize(0.09)
    h_sig.GetXaxis().SetTitleOffset(1.05)

    h_sig.SetLineWidth(2)
    h_sig.SetLineColor(ROOT.kBlack)
    h_sig.SetAxisRange(0, 4.5, "Y")
    h_sig.GetXaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetTitleOffset(0.8)
    h_sig.GetYaxis().SetTitleSize(0.05)
    h_sig.GetYaxis().SetTitle("Sig and S/B")
    h_sig.GetYaxis().SetNdivisions(506)
    y_max = h_sig.GetMaximum()*1.3
    h_sig.SetAxisRange(0.0, y_max, "Y")    
    h_sig.Draw("")
    h_SB.SetLineWidth(2)
    h_SB.SetLineColor(ROOT.kBlue)
    h_SB.Draw("same")

    h_yield.SetLineColor(ROOT.kRed)
    h_yield.SetLineWidth(2)
    h_yield.SetTitle("")
    h_yield.GetXaxis().SetTitle("")
    h_yield.GetXaxis().SetTitleSize(0.09)
    h_yield.GetXaxis().SetTitleOffset(1.05)
    h_yield.GetXaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetTitleOffset(0.8)
    h_yield.GetYaxis().SetTitleSize(0.05)
    h_yield.GetYaxis().SetTitle("Signal Yield")
    h_yield.GetYaxis().SetNdivisions(506)
    y_max = h_yield.GetMaximum()*1.3
    h_yield.SetAxisRange(0.0, y_max, "Y")    

    
    canvasSigs.cd()
    upperSigs2 = ROOT.TPad("upperSigs2", "upperSigs2", 0.025, 0.445, 0.995, 0.995)
    upperSigs2.SetFillStyle(4000)
    upperSigs2.SetFrameFillStyle(0)
    upperSigs2.Draw()
    upperSigs2.cd()
    ROOT.gPad.SetTicky(0)
    #upperSigs2.SetBottomMargin(0.3)
    h_yield.Draw("Y+")


    legend1 = ROOT.TLegend(0.705, 0.75, 0.93, 0.87)
    legend1.AddEntry(h_sig ,"Significance","l")
    legend1.AddEntry(h_SB ,"S/B", "l")
    legend1.AddEntry(h_yield ,"Signal Yield","l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    canvasSigs.cd()
    lowerSigs = ROOT.TPad("lowerSigs", "lowerSigs", 0.025, 0.025, 0.995, 0.495)
    lowerSigs.Draw()
    lowerSigs.SetGridy()
    lowerSigs.cd().SetBottomMargin(0.3)

    rat1=h_sig.Clone()
    rat2=h_SB.Clone()
    rat3=h_yield.Clone()
    for i in range(rat1.GetNbinsX()+1):
        if rat1.GetBinContent(1)>0:
            rat1.SetBinContent(i+1,h_sig.GetBinContent(i+1)/h_sig.GetBinContent(1))
            rat1.SetBinError(i+1,h_sig.GetBinError(i+1)/h_sig.GetBinContent(1))
        if rat2.GetBinContent(1)>0:
            rat2.SetBinContent(i+1,h_SB.GetBinContent(i+1)/h_SB.GetBinContent(1))
            rat2.SetBinError(i+1,h_SB.GetBinError(i+1)/h_SB.GetBinContent(1))
        if rat3.GetBinContent(1)>0:
            rat3.SetBinContent(i+1,h_yield.GetBinContent(i+1)/h_yield.GetBinContent(1))
            rat3.SetBinError(i+1,h_yield.GetBinError(i+1)/h_yield.GetBinContent(1))

    rat1.SetTitle("")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)

    #rat1.SetLineWidth(2)
    #rat1.SetLineColor(ROOT.kBlack)
    rat1.GetXaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetTitleOffset(0.8)
    rat1.GetYaxis().SetTitleSize(0.05)
    rat1.GetYaxis().SetTitle("Cuts/Nom")
    rat1.GetYaxis().SetNdivisions(506)
    rat1.SetAxisRange(0.85, 1.25, "Y")  

    rat1.Draw("hist")
    rat2.Draw("same hist")
    rat3.Draw("same hist")



    canvasSigs.SaveAs(working_dir+"Significances_"+JetMET+"_"+varName+".png")

    

    


NUMJETS={"nJets==3":"3jet"} #,"nJets==4":"4jet","nJets>=4":"4pjet"}
METCUT={"MET>150&&MET<400":"150_400ptv"} #,"MET>250":"250ptv","MET>400":"400ptv","MET>250&&MET<400":"250_400ptv"}
#JETCUT1={"GSCptJ1>45":"45J1","GSCptJ1>50":"50J1","GSCptJ1>60":"60J1","GSCptJ1>70":"70J1"}
#JETCUT2={"GSCptJ2>20":"20J2","GSCptJ2>25":"25J2","GSCptJ2>30":"30J2","GSCptJ2>35":"35J2"}
#JETCUT3={"GSCptJ3>20":"20J3","GSCptJ3>25":"25J3","GSCptJ3>30":"30J3","GSCptJ3>35":"35J3"}

histNames = {"FSRCut" : "25,150,400,20,0,2"} #, "GSCMbb" : "40,0,400"}

histNames={}
#main
#sys.argv.append("-b")
print("Doing bc and bb")
for hName in histNames:
    for numJets in NUMJETS:
        if hName == "pTJ3":
            if numJets == "nJets==2":
                continue
        for METCut in METCUT:
            m_sigVals={}
            m_yieldVals={}
            m_SBVals={}
                            
            #if not ("dRBB" in hName or "pTB1" in hName):
            #    continue
            sys.argv.append("-b")
            ROOT.gStyle.SetPadTickX(1)
            ROOT.gStyle.SetPadTickY(1)
            
            histEx="2btag"+NUMJETS[numJets]+"_"+METCUT[METCut]+"45J120J220J3"
            #extraCuts="&&"+numJets+"&&"+METCut+"&&"+JetCut1+"&&"+JetCut2+"&&"+JetCut3+"&&"+dRBBReg
            extraCuts="&&"+numJets+"&&"+METCut
            CUTS=numJets+"_"+METCut+"-&&PassNonJetCountCuts==1-&&"+JetCut1+"&&"+JetCut2
            if numJets == "nJets==2":
                if JetCut3 != "GSCptJ3>20":
                    continue
            #else:
            #histEx+="_"+JETCUT3[JetCut3]
            #extraCuts+="&&"+JetCut3
            #CUTS+="&&"+JetCut3
                            
            #histEx=histEx+"_SR"
            #extraCuts=extraCuts+"&&Description==\"SR\""
            
            #if NUMJETS[numJets]=="2jet":
            #    histEx=histEx+"_SR"
            #    extraCuts=extraCuts+"&&"+cut_SR_2j
            #else:
            #    histEx=histEx+"_SR"
            #    extraCuts=extraCuts+"&&"+cut_SR_3j
            
            print("Looking at "+hName+" with cuts "+extraCuts)
            cutString1 = "EventWeight*(nbJets==2&&nSigJets>=2&&PassNonJetCountCuts==1"+extraCuts+")"
            ROOT.gROOT.SetBatch(True)
            
            binning = histNames[hName]
            varName = hName
            if "FSRCut" in hName:
                varName = "():MET"
            histName1 = "h_"+hName
            drawString1 = varName+">>"+histName1+"("+binning+")"
            print("Draw("+drawString1+","+cutString1+")")
            
            doDataMC(varName,binning,cutString1,histEx)
            continue
            exit()
            
            chains_by_type[sample].Draw(drawString1,ROOT.TCut(cutString1))
            hist_tmp1 = ROOT.gDirectory.Get(histName1)
            hist_tmp1.SetDirectory(0)
            h1 = hist_tmp1.Clone()
            #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
            
            print("looking at "+hName)
            
            canvas = ROOT.TCanvas(hName, hName, 900, 900)
            upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
            lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
            upper.Draw()
            lower.Draw()
            lower.SetGridy()
            lower.cd().SetBottomMargin(0.3)
            upper.cd()
            ROOT.gStyle.SetOptStat(0)
            
            legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
            legend.SetHeader("              MC,    NEntries,    SoW")
            legend.AddEntry(h1, "bb, {:.0f}, {:.2f}".format(h1.GetEntries(),h1.Integral()), "l")
            legend.AddEntry(h2, "bc, {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            legend.SetBorderSize(0)
            legend.SetFillColor(0)
            legend.SetFillStyle(0)
            
            
            if h1.Integral() != 0:
                h1.Scale(1/h1.Integral())
            if h2.Integral() != 0:
                h2.Scale(1/h2.Integral())
                
            #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
            #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
            rat = h2.Clone()
            rat.Divide(h1)        
            
            y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.3
            
            h1.Draw("hist")
            h1.SetTitle("")
            h1.SetAxisRange(0, y_max, "Y")
            h1.GetYaxis().SetMaxDigits(3)
            h1.GetYaxis().SetTitle("Entries")
            h1.GetYaxis().SetTitleSize(0.04)
            h1.GetYaxis().SetTitleOffset(1.2)
            h1.GetYaxis().SetLabelSize(0.05)
            h1.SetYTitle("Entries")
            #h1.SetMarkerColor(ROOT.kBlue)
            #h1.SetMarkerStyle(8)
            #h1.SetMarkerSize(1.5)
            h1.SetLineColor(ROOT.kBlue)    
            h1.SetLineWidth(2)
            h1.SetLabelOffset(5)
            #h1.GetXaxis().SetRangeUser(xLow,xHigh)
            
            h2.Draw("same hist")
            h2.SetMarkerColor(ROOT.kRed)
            #h2.SetMarkerStyle(8)
            #h2.SetMarkerSize(1.5)
            h2.SetLineColor(ROOT.kRed)
            h2.SetLineWidth(2)
            
            t = ROOT.TLatex()
            t.SetNDC()
            t.SetTextFont(72)
            t.SetTextColor(1)
            t.SetTextSize(0.03)
            t.SetTextAlign(4)
            t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
            t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
            t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
            
            legend.Draw()
            
            lower.cd()
            rat.SetTitle("")
            rat.GetXaxis().SetTitle(hName)
            rat.GetXaxis().SetTitleSize(0.09)
            rat.GetXaxis().SetTitleOffset(1.05)
            
            #Line = ROOT.TLine(xLow,1.,xHigh,1.)
            #Line.SetLineWidth(2)
            #Line.SetLineColor(ROOT.kBlack)
            yLow = 0
            yHigh = 2
            rat.SetLineWidth(2)
            rat.SetLineColor(ROOT.kBlack)
            rat.SetAxisRange(yLow, yHigh, "Y")
            rat.GetXaxis().SetLabelSize(0.09)
            rat.GetYaxis().SetLabelSize(0.07)
            rat.GetYaxis().SetTitleOffset(0.7)
            rat.GetYaxis().SetTitleSize(0.06)
            rat.GetYaxis().SetTitle("bc/bb")
            rat.GetYaxis().SetNdivisions(506)
            
            rat.Draw("")
            #Line.Draw("same")
            #rat.GetXaxis().SetRangeUser(xLow,xHigh)
            
            
            canvas.SaveAs(working_dir+sample+"_"+hName+"_"+histEx+".png")
            
print("")
print("")
print("")
print("All complete")


VH_chain = chains_by_type["signal"]
histNames = {"pTB1" : "46,40,500","pTB2" : "56,20,300","pTJ3" : "56,20,300", "mBB" : "40,0,400"}
#histNames={}
#TruthWZptJ1
#GSCptJ1
#OneMuptJ1

for hName in histNames:
    for numJets in ["nJets==2","nJets==3","nJets==4","nJets>=4"]:
        if hName == "pTJ3":
            if numJets == "nJets==2":
                continue
        for METCut in ["MET>150","MET>250","MET>400","MET>150&&MET<250","MET>250&&MET<400"]:
            #if not ("dRBB" in hName or "pTB1" in hName):
            #    continue
            sys.argv.append("-b")
            ROOT.gStyle.SetPadTickX(1)
            ROOT.gStyle.SetPadTickY(1)
            
            histEx="2tag"
            if numJets=="nJets==2":
                histEx=histEx+"2jet"
            elif numJets=="nJets==3":
                histEx=histEx+"3jet"
            elif numJets=="nJets==4":
                histEx=histEx+"4jet"
            elif numJets=="nJets>=4":
                histEx=histEx+"4pjet"
            if METCut=="MET>150":
                histEx=histEx+"_150ptv"
            elif METCut=="MET>250":
                histEx=histEx+"_250ptv"
            elif METCut=="MET>400":
                histEx=histEx+"_400ptv"
            elif METCut=="MET>150&&MET<250":
                histEx=histEx+"_150_250ptv"
            elif METCut=="MET>250&&MET<400":
                histEx=histEx+"_250_400ptv"

            print("Looking at MET: "+METCut+" var: "+hName+" numJets: "+numJets)
            cutString1 = "EventWeight*(nbJets==2&&nSigJets>=2&&"+numJets+"&&PassNonJetCountCuts==1&&"+METCut+"&&Description==\"SR\")"
            CUTS="nbJets==2&&nSigJets>=2&&"+numJets+"-&&PassNonJetCountCuts==1-&&"+METCut+"&&SR"
            ROOT.gROOT.SetBatch(True)
    
            binning = histNames[hName]
            varName = hName
            histName1 = "h_"+hName
            histName2 = "h_"+hName
            histName3 = "h_"+hName
            drawString1 = varName+">>"+histName1+"("+binning+")"
            drawString2 = varName+">>"+histName2+"("+binning+")"
            drawString3 = varName+">>"+histName3+"("+binning+")"
            if hName == "pTB1":
                histName2="h_TruthWZptJ1"
                drawString2 = "TruthWZptJ1>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ1"
                drawString3 = "GSCptJ1>>"+histName3+"("+binning+")"
            elif hName == "pTB2":
                histName2="h_TruthWZptJ2"
                drawString2 = "TruthWZptJ2>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ2"
                drawString3 = "GSCptJ2>>"+histName3+"("+binning+")"
            elif hName == "pTJ3":
                histName2="h_TruthWZptJ3"
                drawString2 = "TruthWZptJ3>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ3"
                drawString3 = "GSCptJ3>>"+histName3+"("+binning+")"
            elif hName == "mBB":
                histName2="h_OneMuMbb"
                drawString2 = "OneMuMbb>>"+histName2+"("+binning+")"
                histName3="h_GSCMbb"
                drawString3 = "GSCMbb>>"+histName3+"("+binning+")"
            print("Draw("+drawString1+","+cutString1+")")
            print("Draw("+drawString2+","+cutString1+")")
            print("Draw("+drawString3+","+cutString1+")")
    
            VH_chain.Draw(drawString1,ROOT.TCut(cutString1))
            hist_tmp1 = ROOT.gDirectory.Get(histName1)
            hist_tmp1.SetDirectory(0)
            h1 = hist_tmp1.Clone()

            VH_chain.Draw(drawString2,ROOT.TCut(cutString1))
            hist_tmp2 = ROOT.gDirectory.Get(histName2)
            hist_tmp2.SetDirectory(0)
            h2 = hist_tmp2.Clone()

            VH_chain.Draw(drawString3,ROOT.TCut(cutString1))
            hist_tmp3 = ROOT.gDirectory.Get(histName3)
            hist_tmp3.SetDirectory(0)
            h3 = hist_tmp3.Clone()
    
            print("looking at "+hName)
            
            canvas = ROOT.TCanvas(hName, hName, 900, 900)
            upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
            lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
            upper.Draw()
            lower.Draw()
            lower.SetGridy()
            lower.cd().SetBottomMargin(0.3)
            upper.cd()
            ROOT.gStyle.SetOptStat(0)
            
            legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
            legend.SetHeader("              MC,    NEntries,    SoW")
            legend.AddEntry(h1, "PtReco, {:.0f}, {:.2f}".format(h1.GetEntries(),h1.Integral()), "l")
            if "mBB" in varName:
                legend.AddEntry(h2, "OneMu,  {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            else:
                legend.AddEntry(h2, "TruthWZ,  {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            legend.AddEntry(h3, "GSC,    {:.0f}, {:.2f}".format(h3.GetEntries(),h3.Integral()), "l")
            legend.SetBorderSize(0)
            legend.SetFillColor(0)
            legend.SetFillStyle(0)
            
            #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
            #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
            rat1 = h1.Clone()
            rat2 = h2.Clone()
            rat1.Divide(h3) 
            rat2.Divide(h3)
            
            y_max = max(h1.GetMaximum(), h2.GetMaximum(), h3.GetMaximum())*1.3
            
            h1.Draw("hist")
            h1.SetTitle("")
            h1.SetAxisRange(0, y_max, "Y")
            h1.GetYaxis().SetMaxDigits(3)
            h1.GetYaxis().SetTitle("Entries")
            h1.GetYaxis().SetTitleSize(0.04)
            h1.GetYaxis().SetTitleOffset(1.2)
            h1.GetYaxis().SetLabelSize(0.05)
            h1.SetYTitle("Entries")
            h1.SetMarkerColor(ROOT.kBlue)
            #h1.SetMarkerStyle(8)
            #h1.SetMarkerSize(1.5)
            h1.SetLineColor(ROOT.kBlue)    
            h1.SetLineWidth(2)
            h1.SetLabelOffset(5)
            #h1.GetXaxis().SetRangeUser(xLow,xHigh)
            
            h2.Draw("same hist")
            h2.SetMarkerColor(ROOT.kRed)
            h2.SetLineColor(ROOT.kRed)
            h2.SetLineWidth(2)

            h3.Draw("same hist")
            h3.SetMarkerColor(ROOT.kBlack)
            h3.SetLineColor(ROOT.kBlack)
            h3.SetLineWidth(2)
            
            t = ROOT.TLatex()
            t.SetNDC()
            t.SetTextFont(72)
            t.SetTextColor(1)
            t.SetTextSize(0.03)
            t.SetTextAlign(4)
            t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
            t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
            t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
            
            legend.Draw()
            
            lower.cd()
            rat1.SetTitle("")
            rat1.GetXaxis().SetTitle(hName)
            rat1.GetXaxis().SetTitleSize(0.09)
            rat1.GetXaxis().SetTitleOffset(1.05)
            
            #Line = ROOT.TLine(xLow,1.,xHigh,1.)
            #Line.SetLineWidth(2)
            #Line.SetLineColor(ROOT.kBlack)
            yLow = 0
            yHigh = 2
            rat1.SetLineWidth(2)
            rat1.SetLineColor(ROOT.kBlue)
            rat1.SetAxisRange(yLow, yHigh, "Y")
            rat1.GetXaxis().SetLabelSize(0.09)
            rat1.GetYaxis().SetLabelSize(0.07)
            rat1.GetYaxis().SetTitleOffset(0.7)
            rat1.GetYaxis().SetTitleSize(0.06)
            rat1.GetYaxis().SetTitle("Correction/GSC")
            rat1.GetYaxis().SetNdivisions(506)
            rat2.SetLineColor(ROOT.kRed)
            
            rat1.Draw("")
            rat2.Draw("same")

            #Line.Draw("same")
            #rat.GetXaxis().SetRangeUser(xLow,xHigh)
            
            
            canvas.SaveAs(working_dir+"VH423_"+histEx+"_"+hName+".png")
            

        
