import os
import sys
import ROOT
import argparse
import re
import copy

# python plotting.py 0Lep full qq_gg
parser = argparse.ArgumentParser(description="PFlow vs EMTopo")
parser.add_argument("channel", type=str, help="channel number", default="")
parser.add_argument("period", type=str, help="data taking period", default="")
parser.add_argument("samples", type=str, help="qq, gg or qq_gg", default="qq_gg")
args = parser.parse_args()

sys.argv.append("-b")

channel = args.channel
period = args.period
samples = args.samples

#filenamePF0 = "histograms/{}/{}/PF0_{}.root".format(channel,period,samples)
#filenamePF1 = "histograms/{}/{}/PF1_{}.root".format(channel,period,samples)
filenamePF0 = "Condor/outputs_33-04/em18/em18_0L_32-15.root"
filenamePF1 = "Condor/outputs_33-04/pf19/pf19_0L_33-04.root"

#plot_dir = "plots/{}/{}/{}/".format(channel,period,samples)
plot_dir = "histograms/TrackTagStudies/"
if not os.path.isdir(plot_dir):
    os.makedirs(plot_dir)

print("For EM opening "+filenamePF0)
print("For PF opening "+filenamePF1)

f0 = ROOT.TFile.Open(filenamePF0)
f1 = ROOT.TFile.Open(filenamePF1)
f0.cd()

###########################################################################

def MbbMaker(f,histName,typ,t,regionName):
    H1name = histName.replace("OneMu", "")
    H2name = histName
    H3name = histName.replace("OneMu", "PtReco")
    H4name = histName.replace("OneMu", "TruthWZ")
    H5name = histName.replace("OneMu", "GSC")
    H1 = f.Get(H1name).Clone()
    H2 = f.Get(H2name).Clone()
    H3 = f.Get(H3name).Clone()
    H4 = f.Get(H4name).Clone()
    H5 = f.Get(H5name).Clone()

    canvas = ROOT.TCanvas("Mbb compare {}".format(typ), "Mbb compare {}".format(typ), 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.85)
    ROOT.gStyle.SetOptStat(0)

    H1.Draw("hist")
    H2.Draw("hist same")
    H3.Draw("hist same")
    H4.Draw("hist same")
    H5.Draw("hist same")
    
    H1.Rebin(10)
    H2.Rebin(10)
    H3.Rebin(10)
    H4.Rebin(10)
    H5.Rebin(10)

    R2 = H2.Clone()
    R3 = H3.Clone()
    R1 = H1.Clone()
    R5 = H5.Clone()

    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H4.GetMaximum(), H5.GetMaximum())
    H1.SetAxisRange(0.0, y_max * 1.2, "Y")    
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("#bf{Entries}")
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetYTitle("#bf{Entries}")
    H1.SetLineColor(ROOT.kBlack)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)

    H2.SetLineColor(ROOT.kBlue)
    H2.SetLineWidth(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H4.SetLineColor(ROOT.kMagenta)
    H4.SetLineWidth(2)
    H4.SetLineStyle(9)
    H5.SetLineColor(8)
    H5.SetLineWidth(2)

    R2.SetLineColor(ROOT.kBlue)
    R2.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R1.SetLineColor(ROOT.kBlack)
    R1.SetLineWidth(2)
    R5.SetLineColor(8)
    R5.SetLineWidth(2)

    legend.SetBorderSize(0)
    legend.AddEntry(H4, "MbbTruth", "l")
    legend.AddEntry(H5, "MbbGSC", "l")
    legend.AddEntry(H2, "MbbOneMu", "l")
    legend.AddEntry(H3, "MbbPtReco", "l")
    legend.AddEntry(H1, "Mbb", "l")
    legend.Draw()

    t.DrawLatex(0.15, 0.8, regionName )
    
    lower.cd()
    R2.Divide(H4)
    R3.Divide(H4)
    R1.Divide(H4)
    R5.Divide(H4)
    
    R2.SetTitle("")
    R2.GetXaxis().SetTitle("#bf{Mbb} [GeV]")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    R2.SetAxisRange(0., 2, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("reco/Truth")
    R2.GetYaxis().SetNdivisions(506)

    R2.Draw("hist")
    R3.Draw("same hist")
    R1.Draw("same hist")
    R5.Draw("same hist")

    canvas.SaveAs(plot_dir+H2name+"_Comp{}.png".format(typ))

def MbbTrueVReco(f,f1,histName,t,regionName):
    H0name = histName.replace("TruthWZ", "")
    H0tname = histName
    H1name = histName.replace("TruthWZ", "")
    H1tname = histName
    
    H1 = f.Get(H0name).Clone()
    H2 = f.Get(H0tname).Clone()
    H3 = f1.Get(H1name).Clone()
    H4 = f1.Get(H1tname).Clone()

    canvas = ROOT.TCanvas("Mbb true vs reco", "Mbb true vs reco", 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.85)
    ROOT.gStyle.SetOptStat(0)

    H2.Draw("hist")
    H1.Draw("hist same")
    H3.Draw("hist same")
    H4.Draw("hist same")
    
    H1.Rebin(10)
    H2.Rebin(10)
    H3.Rebin(10)
    H4.Rebin(10)

    R4 = H4.Clone()
    R3 = H3.Clone()
    R1 = H1.Clone()

    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H4.GetMaximum())
    H2.SetAxisRange(0.0, y_max * 1.2, "Y")    
    H2.SetTitle("")
    H2.GetYaxis().SetMaxDigits(3)
    H2.GetYaxis().SetTitle("#bf{Entries}")
    H2.GetYaxis().SetTitleSize(0.04)
    H2.GetYaxis().SetTitleOffset(1.2)
    H2.SetYTitle("#bf{Entries}")
    H2.SetLineColor(ROOT.kBlack)
    H2.SetLineWidth(2)
    H2.SetLabelOffset(5)

    H1.SetLineColor(ROOT.kBlue)
    H1.SetLineWidth(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H4.SetLineColor(ROOT.kMagenta)
    H4.SetLineWidth(2)
    
    R4.SetLineColor(ROOT.kMagenta)
    R4.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R1.SetLineColor(ROOT.kBlue)
    R1.SetLineWidth(2)
    
    legend.SetBorderSize(0)
    legend.AddEntry(H4, "PFlow Truth", "l")
    legend.AddEntry(H2, "EMTopo Truth", "l")
    legend.AddEntry(H3, "PFlow Reco", "l")
    legend.AddEntry(H1, "EMTopo Reco", "l")
    legend.Draw()

    t.DrawLatex(0.15, 0.8, regionName )
    
    lower.cd()
    R4.Divide(H2)
    R3.Divide(H2)
    R1.Divide(H2)
    
    R1.SetTitle("")
    R1.GetXaxis().SetTitle("#bf{Mbb} [GeV]")
    R1.GetXaxis().SetTitleSize(0.09)
    R1.GetXaxis().SetTitleOffset(1.05)
    R1.SetAxisRange(0., 2, "Y")
    R1.GetXaxis().SetLabelSize(0.09)
    R1.GetYaxis().SetLabelSize(0.07)
    R1.GetYaxis().SetTitleOffset(0.7)
    R1.GetYaxis().SetTitleSize(0.06)
    R1.GetYaxis().SetTitle("others/EMTruth")
    R1.GetYaxis().SetNdivisions(506)

    R1.Draw("hist")
    R3.Draw("same hist")
    R4.Draw("same hist")

    canvas.SaveAs(plot_dir+H0tname+"_TrueVReco.png")

def MbbFitter(histName, h, h1):
    h.SetLineColor(ROOT.kBlue)
    #h.SetFillColor(ROOT.kBlue)
    

    m = ROOT.RooRealVar("m","m",25,200)
    B_xp = ROOT.RooRealVar("B_xp","B_xp",121.5,100,130)
    B_sig = ROOT.RooRealVar("B_sig","B_sig",15,5,25)
    B_xi = ROOT.RooRealVar("B_xi","B_xi",0,-1,1)
    B_rho1 = ROOT.RooRealVar("B_rho1","B_rho1", 0,-1,1)
    B_rho2 = ROOT.RooRealVar("B_rho2","B_rho2", 0,-1,1)

    B_xp_1 = ROOT.RooRealVar("B_xp_1","B_xp_1",121.5,100,130)
    B_sig_1 = ROOT.RooRealVar("B_sig_1","B_sig_1",15,5,25)
    B_xi_1 = ROOT.RooRealVar("B_xi_1","B_xi_1",0,-1,1)
    B_rho1_1 = ROOT.RooRealVar("B_rho1_1","B_rho1_1", 0,-1,1)
    B_rho2_1 = ROOT.RooRealVar("B_rho2_1","B_rho2_1", 0,-1,1)

    
    bukin = ROOT.RooBukinPdf("bukin","bukin",m,B_xp,B_sig,B_xi,B_rho1,B_rho2)
    bukin1 = ROOT.RooBukinPdf("bukin1","bukin1",m,B_xp_1,B_sig_1,B_xi_1,B_rho1_1,B_rho2_1)

    h.Rebin(5)
    h1.Rebin(5)
    dH = ROOT.RooDataHist("dH","dH",ROOT.RooArgList(m),h)
    dH1 = ROOT.RooDataHist("dH1","dH1",ROOT.RooArgList(m),h1)
    
    mframe = m.frame()
    
    fit = bukin.fitTo(dH,ROOT.RooFit.Save())
    fit1 = bukin1.fitTo(dH1,ROOT.RooFit.Save())
    #fit2.Print()
    
    #dH.plotOn(mframe,ROOT.RooFit.Name("Data"))
    #dH1.plotOn(mframe,ROOT.RooFit.Name("Data1"))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("Fit"),ROOT.RooFit.LineColor(2))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("Fit1"),ROOT.RooFit.LineColor(3))
    #mframe.Draw()
    #chi2 = mframe.chiSquare("Fit","Data",5)
    #chi21 = mframe.chiSquare("Fit1","Data1",5)
    #print("chi2: {} chi21: {}".format(chi2,chi21))

    HI = bukin.createHistogram('m', 175)
    HI1 = bukin1.createHistogram('m', 175)
    #print(h.Integral("width"), h.GetBinContent(100))
    #print(HI.Integral("width"), HI.GetBinContent(100))
    #print(h.Integral("width")/HI.Integral("width"))
    #print(h.GetBinContent(h.GetXaxis().FindBin(100))/HI.GetBinContent(HI.GetXaxis().FindBin(100)))
    HI.Scale(h.Integral("width")/HI.Integral("width"))
    HI1.Scale(h1.Integral("width")/HI1.Integral("width"))
    #HI.Scale(h.Integral()/HI.Integral())
    #HI1.Scale(h1.Integral()/HI1.Integral())
    #print(HI.Integral(), HI.GetBinContent(100))
    HI.SetLineColor(ROOT.kBlue)
    HI.SetFillStyle(0)
    HI.SetLineWidth(2)
    HI.SetLineStyle(2)
    HI1.SetLineColor(ROOT.kRed)
    HI1.SetFillStyle(0)
    HI1.SetLineWidth(2)
    HI1.SetLineStyle(2)

    mean = B_xp.getVal()
    sigma = B_sig.getVal()
    mean1 = B_xp_1.getVal()
    sigma1 = B_sig_1.getVal()

    return HI,HI1,[mean,sigma,mean1,sigma1]
    
    #dH.plotOn(mframe,ROOT.RooFit.DrawOption("B"),ROOT.RooFit.FillColorAlpha(0,0),ROOT.RooFit.LineColor(ROOT.kBlue), ROOT.RooFit.MarkerColor(ROOT.kBlue))
    #dH1.plotOn(mframe,ROOT.RooFit.LineColor(ROOT.kRed), ROOT.RooFit.MarkerColor(ROOT.kRed))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("bukin"),ROOT.RooFit.LineColor(ROOT.kBlue),ROOT.RooFit.LineStyle(1))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("bukin1"),ROOT.RooFit.LineColor(ROOT.kRed),ROOT.RooFit.LineStyle(1))
    
    '''
    leg1 = ROOT.TLegend(0.2,0.75,0.6,0.85)
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetTextSize(0.03)
    leg1.SetHeader("   Jet type  peak  width")
    leg1.AddEntry(mframe.findObject("bukin"),"EMTopo {:.2f} {:.2f}".format(mean, sigma), "L")
    leg1.AddEntry(mframe.findObject("bukin1"),"PFlow {:.2f} {:.2f}".format(mean1, sigma1), "L")
    
    can = ROOT.TCanvas("massframe","massframe",750,800)
    can.cd()
    can.SetTickx()
    can.SetTicky()
    h.Draw('hist')
    h1.Draw('hist same')
    HI.Draw('c hist same')
    HI1.Draw('c hist same')
    
    #bukin.Draw('same')
    #fit.Draw('same')
    
    xTitle = ""
    words = histName.split("_")
    for word in words:
        if "Mbb" in word:
            xTitle = "#bf{"+word+"} [GeV]"
    
    mframe.Draw()
    mframe.SetTitle("")
    mframe.GetXaxis().SetTitle(xTitle)
    mframe.GetYaxis().SetTitle("Events")
    mframe.SetMaximum(mframe.GetMaximum()*1.2)
    leg1.Draw("same")
    mframe.GetYaxis().SetTitle("Events")
    
    can.SaveAs(plot_dir+histName+".png")
    '''

def TaggerMaker(f,histName,typ,t,regionName):

    H1name = histName
    H2name = histName.replace("nBTags", "nBTrackJets")
    H3name = histName.replace("nBTags", "nNewBTags")
    H4name = histName.replace("nBTags", "nTrueBTags")
    H1 = f.Get(H1name).Clone()
    H2 = f.Get(H2name).Clone()
    H3 = f.Get(H3name).Clone()
    H4 = f.Get(H4name).Clone()

    canvas = ROOT.TCanvas("nTag compare {}".format(typ), "nTag compare {}".format(typ), 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.85)
    ROOT.gStyle.SetOptStat(0)

    H1.Draw("hist")
    H2.Draw("hist same")
    H3.Draw("hist same")
    H4.Draw("hist same")
    
    R2 = H2.Clone()
    R3 = H3.Clone()
    R4 = H4.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H4.GetMaximum())
    H1.SetAxisRange(0.0, y_max * 1.2, "Y")    
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("#bf{Entries}")
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetYTitle("#bf{Entries}")
    H1.SetLineColor(ROOT.kBlack)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)

    H2.SetLineColor(ROOT.kBlue)
    H2.SetLineWidth(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H4.SetLineColor(ROOT.kMagenta)
    H4.SetLineWidth(2)

    R2.SetLineColor(ROOT.kBlue)
    R2.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R4.SetLineColor(ROOT.kMagenta)
    R4.SetLineWidth(2)

    legend.SetBorderSize(0)
    legend.AddEntry(H1, "nBTags", "l")
    legend.AddEntry(H2, "nBTrackJets", "l")
    legend.AddEntry(H3, "nNewBTags", "l")
    legend.AddEntry(H4, "nTrueBTags", "l")
    legend.Draw()

    t.DrawLatex(0.15, 0.8, regionName )
    
    lower.cd()
    R2.Divide(H1)
    R3.Divide(H1)
    R4.Divide(H1)
    
    R2.SetTitle("")
    R2.GetXaxis().SetTitle("#bf{nTags}")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    R2.SetAxisRange(0., 2, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("others/nBTags")
    R2.GetYaxis().SetNdivisions(506)

    R2.Draw("hist")
    R3.Draw("same hist")
    R4.Draw("same hist")

    canvas.SaveAs(plot_dir+H1name+"_Comp{}.png".format(typ))

def cutFlowMaker(f,f1,histName):
    Htname = histName
    Hname = histName.replace("Tracks","") 
    H1 = f.Get(Hname).Clone()
    H3 = f1.Get(Hname).Clone()
    H2 = f.Get(Htname).Clone()
    H4 = f1.Get(Htname).Clone()
    
    canvas = ROOT.TCanvas("cutFlow compare", "cutFlow compare", 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.85)
    ROOT.gStyle.SetOptStat(0)

    H1.Draw("hist")
    H2.Draw("hist same")
    H3.Draw("hist same")
    H4.Draw("hist same")
    
    R2 = H2.Clone()
    R3 = H3.Clone()
    R4 = H4.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H4.GetMaximum())
    H1.SetAxisRange(0.0, y_max * 1.2, "Y")    
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("#bf{Entries}")
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetYTitle("#bf{Entries}")
    H1.SetLineColor(ROOT.kBlack)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)

    H2.SetLineColor(ROOT.kBlue)
    H2.SetLineWidth(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H4.SetLineColor(ROOT.kMagenta)
    H4.SetLineWidth(2)

    R2.SetLineColor(ROOT.kBlue)
    R2.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R4.SetLineColor(ROOT.kMagenta)
    R4.SetLineWidth(2)

    legend.SetBorderSize(0)
    legend.AddEntry(H1, "32-15 cutFlow", "l")
    legend.AddEntry(H2, "32-15 trackCutFlow", "l")
    legend.AddEntry(H3, "33-04 cutFlow", "l")
    legend.AddEntry(H4, "33-04 trackCutFlow", "l")
    legend.Draw()
    
    lower.cd()
    R2.Divide(H1)
    R3.Divide(H1)
    R4.Divide(H1)
    
    ymin=0.8
    ymax=1.2
    if "Anti" in Hname:
        ymin=0.5
        ymax=1.5        
    R2.SetTitle("")
    R2.GetXaxis().SetTitle("#bf{cutFlow}")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    R2.SetAxisRange(ymin,ymax, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("others/32-15 cutFlow")
    R2.GetYaxis().SetNdivisions(506)

    R2.Draw("")
    R3.Draw("same")
    R4.Draw("same")

    canvas.SaveAs(plot_dir+Hname+"_Comp.png")

def TrackCompare(f,f1,histName,t,regionName):
    H1name = histName
    H2name = histName
    if "cuts_w_T" in histName:
        #print("Before change "+H1name)
        H1name = H1name.replace("cuts_w_T", "cuts_w")
        #print("After change "+H1name)
    elif not ("_w" in histName and "cuts" not in histName):
        return
    if "Track" in H1name:
        #print("Before change "+H1name)
        H1name = H1name.replace("Track", "")
        #print("After change "+H1name)
    else:
        H1name = H1name.replace("Jt", "J")
    H1 = f.Get(H1name).Clone()
    H2 = f.Get(H2name).Clone()
    H3 = f1.Get(H1name).Clone()
    H4 = f1.Get(H2name).Clone()
    
    VarL2 = H2name.split("_")
    VarL = H1name.split("_")
    Var=""
    for i in range(len(VarL2)):
        if "Track" in VarL2[i]:
            Var = VarL[i]
        elif "Jt" in VarL2[i]:
            Var = VarL[i]

    print("Doing Track Compare, Var {}".format(Var))
    print("H1Name {} H2name {}".format(H1name, H2name))


    canvas = ROOT.TCanvas("{} compare".format(Var), "{} compare".format(Var), 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.85)
    ROOT.gStyle.SetOptStat(0)

    if "dR" in histName:
        Zoomed(H2name, H1.Clone(), H2.Clone(), H3.Clone(), H4.Clone())
        xLow = 0
        xHigh = 4
        H1.SetAxisRange(xLow, xHigh, "X")
        H2.SetAxisRange(xLow, xHigh, "X")
        H3.SetAxisRange(xLow, xHigh, "X")
        H4.SetAxisRange(xLow, xHigh, "X")
        H1.GetXaxis().SetRangeUser(xLow, xHigh)
        H2.GetXaxis().SetRangeUser(xLow, xHigh)
        H3.GetXaxis().SetRangeUser(xLow, xHigh)
        H4.GetXaxis().SetRangeUser(xLow, xHigh)
        H1.SetMinimum(0.01)
        H2.SetMinimum(0.01)
        H3.SetMinimum(0.01)
        H4.SetMinimum(0.01)
        upper.SetLogy()
    

    H1.Draw("hist")
    H2.Draw("hist same")
    H3.Draw("hist same")
    H4.Draw("hist same")

    if "Mbb" in histName or "pT" in histName or "dR" in histName or "Pt" in histName:
        H1.Rebin(10)
        H2.Rebin(10)
        H3.Rebin(10)
        H4.Rebin(10)
    elif "dPhi" in histName or "Eta" in histName:
        H1.Rebin(2)
        H2.Rebin(2)
        H3.Rebin(2)
        H4.Rebin(2)

    R2 = H2.Clone()
    R3 = H3.Clone()
    R4 = H4.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H4.GetMaximum())
    
    H1.SetAxisRange(0.01, y_max * 1.2, "Y")    
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("#bf{Entries}")
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetYTitle("#bf{Entries}")
    H1.SetLineColor(ROOT.kBlack)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)

    H2.SetLineColor(ROOT.kBlue)
    H2.SetLineWidth(2)
    #H2.SetLineStyle(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H4.SetLineColor(ROOT.kMagenta)
    H4.SetLineWidth(2)
    #H4.SetLineStyle(2)

    R2.SetLineColor(ROOT.kBlue)
    R2.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R4.SetLineColor(ROOT.kMagenta)
    R4.SetLineWidth(2)

    legend.SetBorderSize(0)
    legend.AddEntry(H1, "EM {}".format(Var), "l")
    legend.AddEntry(H2, "EM track{}".format(Var), "l")
    legend.AddEntry(H3, "PF {}".format(Var), "l")
    legend.AddEntry(H4, "PF track{}".format(Var), "l")
    legend.Draw()

    t.DrawLatex(0.15, 0.8, regionName )
    '''
    if "dPhiMETdijet" in Var:
        Line = ROOT.TLine(120*ROOT.TMath.Pi()/180,0,120*ROOT.TMath.Pi()/180,y_max)
        Line.Draw("same")
        Line.SetLineWidth(2)
    elif "dPhiBB" in Var:
        Line = ROOT.TLine(140*ROOT.TMath.Pi()/180,0,140*ROOT.TMath.Pi()/180,y_max)
        Line.Draw("same")
        Line.SetLineWidth(2)
    '''
    lower.cd()
    R2.Divide(H1)
    R3.Divide(H1)
    R4.Divide(H1)
    
    #if "dR" in histName:
    #    xLow = 0
    #    xHigh = 4
    #    R2.GetXaxis().SetRangeUser(xLow, xHigh)
    #    R3.GetXaxis().SetRangeUser(xLow, xHigh)
    #    R4.GetXaxis().SetRangeUser(xLow, xHigh)


    R2.SetTitle("")
    if "dPtJt1" in histName:
        R2.GetXaxis().SetTitle("#bf{TrueJ1Pt - RecoJ1Pt} [GeV]")
    elif "dPtJt2" in histName:
        R2.GetXaxis().SetTitle("#bf{TrueJ2Pt - RecoJ2Pt} [GeV]")
    elif "dPhi" in histName or "Eta" in histName or "dR" in histName:
        R2.GetXaxis().SetTitle("#bf{"+Var+"}")
    else:
        R2.GetXaxis().SetTitle("#bf{"+Var+"} [GeV]")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    R2.SetAxisRange(0., 2, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("others/EM {}".format(Var))
    R2.GetYaxis().SetNdivisions(506)

    R2.Draw("hist")
    R3.Draw("same hist")
    R4.Draw("same hist")

    canvas.SaveAs(plot_dir+H2name+"_TComp.png")
    
    #print("Waiting for input")
    #input()

def Zoomed(histName,H1,H2,H3,H4):    
    Var="histName"

    #print("Doing Track Compare, Var {}".format(Var))
    #print("H1Name {} H2name {}".format(H1name, H2name))

    canvas = ROOT.TCanvas("{} compare zoomed".format(Var), "{} compare zoomed".format(Var), 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.65, 0.55, 0.85, 0.85)
    ROOT.gStyle.SetOptStat(0)

    if "dR" in histName:
        xLow = 0
        xHigh = 0.4
        H1.SetAxisRange(xLow, xHigh, "X")
        H2.SetAxisRange(xLow, xHigh, "X")
        H3.SetAxisRange(xLow, xHigh, "X")
        H4.SetAxisRange(xLow, xHigh, "X")
        H1.GetXaxis().SetRangeUser(xLow, xHigh)
        H2.GetXaxis().SetRangeUser(xLow, xHigh)
        H3.GetXaxis().SetRangeUser(xLow, xHigh)
        H4.GetXaxis().SetRangeUser(xLow, xHigh)
        H1.SetMinimum(0.01)
        H2.SetMinimum(0.01)
        H3.SetMinimum(0.01)
        H4.SetMinimum(0.01)
        upper.SetLogy()
    

    H1.Draw("hist")
    H2.Draw("hist same")
    H3.Draw("hist same")
    H4.Draw("hist same")

    R2 = H2.Clone()
    R3 = H3.Clone()
    R4 = H4.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H4.GetMaximum())
    
    H1.SetAxisRange(0.01, y_max * 1.2, "Y")    
    H1.SetTitle("")
    H1.GetYaxis().SetMaxDigits(3)
    H1.GetYaxis().SetTitle("#bf{Entries}")
    H1.GetYaxis().SetTitleSize(0.04)
    H1.GetYaxis().SetTitleOffset(1.2)
    H1.SetYTitle("#bf{Entries}")
    H1.SetLineColor(ROOT.kBlack)
    H1.SetLineWidth(2)
    H1.SetLabelOffset(5)

    H2.SetLineColor(ROOT.kBlue)
    H2.SetLineWidth(2)
    #H2.SetLineStyle(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H4.SetLineColor(ROOT.kMagenta)
    H4.SetLineWidth(2)
    #H4.SetLineStyle(2)

    R2.SetLineColor(ROOT.kBlue)
    R2.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R4.SetLineColor(ROOT.kMagenta)
    R4.SetLineWidth(2)

    legend.SetBorderSize(0)
    legend.AddEntry(H1, "EM", "l")
    legend.AddEntry(H2, "EM track", "l")
    legend.AddEntry(H3, "PF","l")
    legend.AddEntry(H4, "PF track", "l")
    legend.Draw()

    t.DrawLatex(0.15, 0.8, regionName )
    '''
    if "dPhiMETdijet" in Var:
        Line = ROOT.TLine(120*ROOT.TMath.Pi()/180,0,120*ROOT.TMath.Pi()/180,y_max)
        Line.Draw("same")
        Line.SetLineWidth(2)
    elif "dPhiBB" in Var:
        Line = ROOT.TLine(140*ROOT.TMath.Pi()/180,0,140*ROOT.TMath.Pi()/180,y_max)
        Line.Draw("same")
        Line.SetLineWidth(2)
    '''
    lower.cd()
    R2.Divide(H1)
    R3.Divide(H1)
    R4.Divide(H1)
    
    #if "dR" in histName:
    #    xLow = 0
    #    xHigh = 4
    #    R2.GetXaxis().SetRangeUser(xLow, xHigh)
    #    R3.GetXaxis().SetRangeUser(xLow, xHigh)
    #    R4.GetXaxis().SetRangeUser(xLow, xHigh)


    R2.SetTitle("")
#    if "dPtJt1" in histName:
#        R2.GetXaxis().SetTitle("#bf{TrueJ1Pt - RecoJ1Pt} [GeV]")
#    elif "dPtJt2" in histName:
#        R2.GetXaxis().SetTitle("#bf{TrueJ2Pt - RecoJ2Pt} [GeV]")
    if "dPhi" in histName or "Eta" in histName or "dR" in histName:
        R2.GetXaxis().SetTitle("#bf{"+Var+"}")
#    else:
#        R2.GetXaxis().SetTitle("#bf{"+Var+"} [GeV]")
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    R2.SetAxisRange(0., 2, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("others/EM")
    R2.GetYaxis().SetNdivisions(506)

    R2.Draw("hist")
    R3.Draw("same hist")
    R4.Draw("same hist")

    canvas.SaveAs(plot_dir+histName+"_TCompZoomed.png")
    
    #print("Waiting for input")
    #input()

def NumEventsPassed(h0, histName, PF):
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPaintTextFormat("4.1f")
    '''
    passes = {-2 : "passed track cuts", -1 : "passed none", 0 : "all", 1 : "passed normal cuts", 2 : "passed both cuts"}
    regionPass = {"inc" : 0, "inc_2jet" : 0, "1jet_75ptv" : 0, "1jet_150ptv" : 0, "1jet_250ptv" : 0, "1jet_400ptv" : 0,
                  "2jet_75ptv" : 0, "2jet_150ptv" : 0, "2jet_250ptv" : 0, "2jet_400ptv" : 0,
                  "3jet_75ptv" : 0, "3jet_150ptv" : 0, "3jet_250ptv" : 0, "3jet_400ptv" : 0,
                  "4jet_75ptv" : 0, "4jet_150ptv" : 0, "4jet_250ptv" : 0, "4jet_400ptv" : 0}
    # -1 = passed none, 1 = passed only normal cuts, 0 = all, -2 = passed only track cuts, 2 = passed both cuts
    acceptedEvents = {"passed track cuts" : copy.deepcopy(regionPass), "passed none" : copy.deepcopy(regionPass), 
                      "all" : copy.deepcopy(regionPass), "passed normal cuts" : copy.deepcopy(regionPass), 
                      "passed both cuts" : copy.deepcopy(regionPass)}
    regionPassNames = ["inc", "inc_2jet", "1jet_75ptv", "1jet_150ptv", "1jet_250ptv", "1jet_400ptv",
                       "2jet_75ptv", "2jet_150ptv", "2jet_250ptv", "2jet_400ptv",
                       "3jet_75ptv", "3jet_150ptv", "3jet_250ptv", "3jet_400ptv",
                       "4jet_75ptv", "4jet_150ptv", "4jet_250ptv", "4jet_400ptv"]
    passes2 = ["all", "passed both cuts", "passed normal cuts", "passed track cuts", "passed none"]
    for i in range(len(passes)):
        ybin = i-2
        #print("ybin {} passes {}".format(ybin, passes[ybin]))
        for j in range(len(regionPassNames)):
            #print("xbin {}".format(h0.GetXaxis().GetBinLabel(j+1)))
            acceptedEvents[passes[ybin]][h0.GetXaxis().GetBinLabel(j+1)]=h0.GetBinContent(j+1,i+1)
    
    print "                    ",
    for r in regionPassNames:
        print "{:13s}".format(r),
    print()
    #for p in passes2:
    #    for r in regionPassNames:
    '''
    
    canvas = ROOT.TCanvas("C", "C", 900, 900)
    pad = ROOT.TPad("pad", "pad", 0.025, 0.025, 0.995, 0.995)
    pad.Draw()
    #pad.cd().SetBottomMargin(0.3)
    pad.cd().SetLeftMargin(0.12)
    pad.cd().SetRightMargin(0.12)
    pad.cd()
    h0.Draw("text colz")
    #h0.SetTitle("{}".format())
    totEV = h0.GetBinContent(1,1)
    #h0.Scale(100.0/totEV)
    #h0.SetBinContent(1,1,totEV)
    
    canvas.SaveAs(plot_dir+histName+PF+".png")



# make the plots
# main
for h in f0.GetListOfKeys():
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    hist = h.ReadObj()
    histName = h.GetName()
    hist1 = f1.Get(histName)
    
    print "\nLooking at {}".format(histName)
    
    if "cutFlowTracks" in histName:
        cutFlowMaker(f0,f1,histName)
    elif "regionPass" in histName:
        NumEventsPassed(hist.Clone(), histName, "PF0")
        NumEventsPassed(hist1.Clone(), histName, "PF1")

D0 = f0.Get("cut-2j-3j")
D1 = f1.Get("cut-2j-3j")
print("Starting loop")
for h in D0.GetListOfKeys():
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)


    hist = h.ReadObj()
    histName = h.GetName()


    #if "regionPass" in histName or "Track" in histName or "Jt" in histName:
      #  print("skip")
    #    continue
    if not ("150ptv" in histName or "250ptv" in histName):
       # print("skip")
        continue
    if "HS" in histName or "PU" in histName: 
        if not ("_mu" in histName or "sumnPtTrue" in histName):
            continue
        
    #if "cuts_w_T" in histName or "cuts" not in histName:
    #    continue
    #if not "dPtJt1" in histName:
    #    continue
    #if not ("cutFlow" in histName): # or "inc_" in histName): #"inc_" in histName
    #    continue
    #if "_2jet_" in histName or "had" in histName or "semi" in histName or "HS" in histName or "PU" in histName:
    #    continue
    #if not "_Mbb_" in histName:
    #    continue
    
    hist1 = D1.Get(histName)
    print "\nLooking at {}".format(histName)
    
        #exit()
    #continue
    
    regionName = ""
    if "inc" in histName and "_inc" not in histName:
        regionName = "Inclusive, >=1 SigJet"
        if "2SigJet" in histName:
            regionName = "Inclusive, >=2 SigJets"
        elif "2s&3j" in histName:
            regionName = "Inclusive, >=2 SigJets, <=3 Jets"
            
    elif "jet" in histName:
        jetName = histName.split("_")[0]
        if "4jet" in jetName:
            #jetName = "\geq4jet"
            jetName = "4jet"
        if "150ptv" in histName:
            regionName = jetName+", 150 < p_{T} < 250"
        elif "250ptv" in histName:
            regionName = jetName+", 250 < p_{T} < 400"
        elif "75ptv" in histName:
            regionName = jetName+", 75 < p_{T} < 150"
        elif "400ptv" in histName:
            regionName = jetName+", 400 < p_{T}"
        elif "inc" in histName:
            regionName = jetName+", inclusive"
        

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.04)
    t.SetTextAlign(4)

    
    if "OneMuMbb" in histName:
        MbbMaker(D0,histName,"EM",t,regionName)
        MbbMaker(D1,histName,"PF",t,regionName)
    if "TruthWZMbb" in histName:
        MbbTrueVReco(D0,D1,histName,t,regionName)
    if "nBTags" in histName:
        TaggerMaker(D0,histName,"EM",t,regionName)
        TaggerMaker(D1,histName,"PF",t,regionName)
    if "_Track" in histName: #"TrackMbb" in histName or "TrackpTBB" in histName or "TrackdPhiBB" in histName or "TrackdPhiMETdijet" in histName or "TrackGSCMbb" in histName:
        print "\nDoing Track compare of {}".format(histName)
        TrackCompare(D0,D1,histName,t,regionName)
    elif "dRJt1TB1" in histName or "dRJt2TB2" in histName or "dPtJt1TB1" in histName or "dPtJt2TB2" in histName:
        #print "\nLooking at {}".format(histName)
        TrackCompare(D0,D1,histName,t,regionName)        
    elif "pTJt1" in histName or "EtaJt1" in histName or "pTJt2" in histName or "EtaJt2" in histName:
        #print "\nLooking at {}".format(histName)
        TrackCompare(D0,D1,histName,t,regionName)        
    #continue

    #fit_pars = []
    #H = ROOT.TH1F()
    #H1 = ROOT.TH1F()
    #if "Mbb" in histName and not ("_mu" in histName or "sum" in histName):
    #    H,H1,fit_pars=MbbFitter(histName, hist.Clone(), hist1.Clone())
        

    canvas = ROOT.TCanvas(histName, histName, 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    
    
    upper.Draw()
    lower.Draw()
    lower.SetGridy()

    

    lower.cd().SetBottomMargin(0.3)

    upper.cd()
    ROOT.gStyle.SetOptStat(0)

    h = hist.Clone()
    h1 = hist1.Clone()

    
    
    if "_mu" in histName or "sumnPtTrue" in histName:
        if "sum" not in histName:
            continue
        else:
            if "True" in histName:
                histName2 = histName.replace("sum","")
            else:
                if ("_semi_" in histName):
                    histName2 = re.sub('semi.*mu','nMu',histName, flags=re.DOTALL)
                elif "_HS_" in histName:
                    histName2 = re.sub('HS.*mu','nMu',histName, flags=re.DOTALL)
                elif "_PU_" in histName: 
                    histName2 = re.sub('PU.*mu','nMu',histName, flags=re.DOTALL)
                elif "_had_" in histName:
                    histName2 = re.sub('had.*mu','nMu',histName, flags=re.DOTALL)
                else:
                    histName2 = re.sub('sum.*mu','nMu',histName, flags=re.DOTALL)
            #print(histName+"  "+histName2)
            hista = D0.Get(histName2).Clone()
            hist1a = D1.Get(histName2).Clone()
            
            reb = 0
            if "True" in histName:
                reb = 5
            elif "_mu" in histName:
                reb = 5
            #print("h {} h1 {} hista {} hist1a {}".format(h.GetName(), h1.GetName(), hista.GetName(), hist1a.GetName()))
            #print("Bins h {} h1 {} hista {} hist1a {}".format(h.GetNbinsX(), h1.GetNbinsX(), hista.GetNbinsX(), hist1a.GetNbinsX()))
            h.Rebin(reb)
            h1.Rebin(reb)
            hista.Rebin(reb)
            hist1a.Rebin(reb)
             
            #print("sum PF0 {} {} {} {}".format(h.GetBinContent(0), h.GetBinContent(1), h.GetBinContent(2), h.GetBinContent(3)))
            #print("n PF0 {} {} {} {}".format(hista.GetBinContent(0), hista.GetBinContent(1), hista.GetBinContent(2), hista.GetBinContent(3)))
            #print("sum PF1 {} {} {} {}".format(h1.GetBinContent(0), h1.GetBinContent(1), h1.GetBinContent(2), h1.GetBinContent(3)))
            #print("n PF1 {} {} {} {}".format(hist1a.GetBinContent(0), hist1a.GetBinContent(1), hist1a.GetBinContent(2), hist1a.GetBinContent(3)))
           
            #if "nSig" in histName:
            #    for i in range(h.GetNbinsX()):
            #        if hista.GetBinContent(i)!=0:
            #            h.SetBinContent(i, h.GetBinContent(i)/hista.GetBinContent(i))
            #            h1.SetBinContent(i, h1.GetBinContent(i)/hist1a.GetBinContent(i))
            h.Divide(hista.Clone())
            h1.Divide(hist1a.Clone())
                        
    h.Draw()
    h1.Draw("same")
    xLow = 0
    xHigh = 1000
    '''
    if "cutFlow" in histName and "_w" in histName:
        print("Signal amounts for {}".format(histName))
        if "Tracks" not in histName:
            # 1: before any cuts, 5:b4 bjets, 6:after bjets, 11:after all cuts
            print("EMTOPO: no cuts, full cuts")
            print("{}, {}, {}, {}".format(h.GetBinContent(1), h.GetBinContent(5), h.GetBinContent(6), h.GetBinContent(11)))
            print("PFLOW: no cuts, full cuts")
            print("{}, {}, {}, {}".format(h1.GetBinContent(1), h1.GetBinContent(5), h1.GetBinContent(6), h1.GetBinContent(11)))
            if "Anti" in histName:
                print("EMTOPO: passing track but failing norm at nBJets")
                print("{}, {}".format(h.GetBinContent(6), h.GetBinContent(6)/h.Integral()))
                print("PFLOW: passing track but failing norm at nBJets")
                print("{}, {}".format(h1.GetBinContent(6), h1.GetBinContent(6)/h1.Integral()))                
        else:
            # 1: before any cuts, 5:b4 bjets, 6:after bjets, 11:b4 additional track cuts, 13: after all cuts
            print("EMTOPO: no cuts, norm cuts, full cuts")
            print("{}, {}, {}, {}, {}".format(h.GetBinContent(1), h.GetBinContent(5), h.GetBinContent(6), h.GetBinContent(11), h.GetBinContent(13)))
            print("PFLOW: no cuts, norm cuts, full cuts")
            print("{}, {}, {}, {}, {}".format(h1.GetBinContent(1), h1.GetBinContent(5), h1.GetBinContent(6), h1.GetBinContent(11), h1.GetBinContent(13)))
            if "Anti" in histName:
                print("EMTOPO: passing norm but failing tracks at nBJets")
                print("{}, {}".format(h.GetBinContent(6), h.GetBinContent(6)/h.Integral()))
                print("{}, {}".format(h.GetBinContent(12) + h.GetBinContent(13), (h.GetBinContent(12) + h.GetBinContent(13))/h.Integral() ))
                print("PFLOW: passing norm but failing tracks at nBJets")
                print("{}, {}".format(h1.GetBinContent(6), h1.GetBinContent(6)/h1.Integral()))
                print("{}, {}".format(h1.GetBinContent(12) + h1.GetBinContent(13), (h1.GetBinContent(12) + h1.GetBinContent(13))/h1.Integral() ))    
    '''
    #summ=0
    #summ2=0
    #for i in range(h.GetNbinsX()):
    #    if h.GetBinCenter(i) > 50:
    #        if h.GetBinCenter(i) > 200:
    #            break
    #        summ += (h.GetBinContent(i)-h1.GetBinContent(i))
    #        summ2 += (h.GetBinContent(i)-h1.GetBinContent(i))**2
    #print("SUMM: {} SUMM2: {}".format(summ, summ2))
    #print("em max {} pf max {}".format(h.GetBinCenter(h.GetMaximumBin()),h1.GetBinCenter(h1.GetMaximumBin())))

    #print(h.Integral())

    if "dRTruth" in histName:
        #h.Rebin(5)
        #h1.Rebin(5)
        xLow = 0
        xHigh = 0.2
        h.GetXaxis().SetRangeUser(xLow, xHigh)
        h1.GetXaxis().SetRangeUser(xLow, xHigh)
    
    #print(h.Integral())
    #print(h.Integral(0,20))
    #print(h.Integral(20,500))
    if (("pTV" in histName) or ("Mbb" in histName) or ("pTBB" in histName) or ("dPtTruth" in histName)) and ("_mu" not in histName): 
        h.Rebin(5)
        h1.Rebin(5)
    elif "Eta" in histName or "dPhi" in histName:
        h.Rebin(5)
        h1.Rebin(5)
    elif "sum" in histName:
        # do nothing
        print()
    elif "Pt" in histName or "pT" in histName or "dR" in histName:
        h.Rebin(10)
        h1.Rebin(10)
    

    #summ=0
    #summ2=0
    #for i in range(h.GetNbinsX()):
    #    if h.GetBinCenter(i) > 50:
    #        if h.GetBinCenter(i) > 200:
    #            break
    #        summ += (h.GetBinContent(i)-h1.GetBinContent(i))
    #        summ2 += (h.GetBinContent(i)-h1.GetBinContent(i))**2
    #print("After rebin")
    #print("SUMM: {} SUMM2: {}".format(summ, summ2))
    #print("em max {} pf max {}".format(h.GetBinCenter(h.GetMaximumBin()),h1.GetBinCenter(h1.GetMaximumBin())))

    drawType = "p" if ("_mu" in histName) else "hist"

    yTitle = "#bf{Entries #}"
    if "_mu" in histName:
        if "Mbb" in histName:
            yTitle = "#bf{GSCMbb} [GeV]"
        elif "nSig" in histName:
            yTitle = "#bf{No. Signal Jets}"
    elif "PtTrue_Final" in histName:
        yTitle = "#bf{pT(TruthWZJ1)/pT(B1)}"
        h.GetXaxis().SetRangeUser(40,200)
    elif "PtTrue_PtReco" in histName:
        yTitle = "#bf{pT(TruthWZJ1)/pT(PtRecoJ1)}"
        h.GetXaxis().SetRangeUser(40,200)
    elif "PtTrue_GSC" in histName:
        yTitle = "#bf{pT(TruthWZJ1)/pT(GSCJ1)}"
        h.GetXaxis().SetRangeUser(40,200)
    elif "PtTrue_OneMu" in histName:
        yTitle = "#bf{pT(TruthWZJ1)/pT(OneMuJ1)}"
        h.GetXaxis().SetRangeUser(40,200)
    

    #print(yTitle)
    ratio = h1.Clone()
    h.Draw(drawType)
    h.SetTitle("")
    h.GetYaxis().SetMaxDigits(3)
    h.GetYaxis().SetTitle(yTitle)
    h.GetYaxis().SetTitleSize(0.04)
    h.GetYaxis().SetTitleOffset(1.2)
    h.GetYaxis().SetLabelSize(0.05)
    h.SetYTitle(yTitle)
    h.SetMarkerColor(ROOT.kBlue)
    h.SetMarkerStyle(8)
    h.SetMarkerSize(1.5)
    h.SetLineColor(ROOT.kBlue)    
    h.SetLineWidth(2)
    h.SetLabelOffset(5)
        
    h1.Draw("{} same".format(drawType))
    h1.SetMarkerColor(ROOT.kRed)
    h1.SetMarkerStyle(8)
    h1.SetMarkerSize(1.5)
    h1.SetLineColor(ROOT.kRed)
    h1.SetLineWidth(2)


    #if ("Mbb" in histName) and not ("_mu" in histName or "sum" in histName):
    
    #    H.Draw('c hist same')
    #    H1.Draw('c hist same')
    
    y_max = max(h.GetMaximum(), h1.GetMaximum())
    fac = 1.2
    if "_mu" in histName:
        fac=1.5
    h.SetAxisRange(0.0009, y_max * fac, "Y")
    
    #if "dRTruth" in histName:
        #h.SetMinimum(0.1)
        #h1.SetMinimum(0.1)
        #upper.SetLogy()


    #upper.cd()
    legend = ROOT.TLegend(0.7, 0.75, 0.9, 0.85)
    
    #if "Mbb" in histName and not ("_mu" in histName or "sum" in histName):
    #    legend.SetX1(0.55)
    #    legend.SetY1(0.7)
    #    legend.SetHeader("    Jet type,  peak,  width,  integral")
    #    legend.AddEntry(h, "EMTopo, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[0],fit_pars[1],h.Integral()), "l")
    #    legend.AddEntry(h1, "PFlow, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[2],fit_pars[3],h1.Integral()), "l")
    if "dRTruth" in histName:
        legend.SetX1(0.65)
        legend.SetY1(0.7)
        legend.SetHeader("    Jet type,  integral")
        legend.AddEntry(h, "EMTopo, {:.1f}".format(h.Integral()), "l")
        legend.AddEntry(h1, "PFlow, {:.1f}".format(h1.Integral()), "l")
    else:
        legend.AddEntry(h, "EMTopo", drawType if drawType == "p" else "l")
        legend.AddEntry(h1, "PFlow", drawType if drawType == "p" else "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()

    if not "dRTruth" in histName:
        t.DrawLatex(0.15, 0.8, regionName )
    else:
        t.DrawLatex(0.3, 0.8, regionName )
    
    
    
    lower.cd()
    
    #print("{} {} {}".format(ratio.GetBinCenter(4),ratio.GetBinContent(4), h1.GetBinContent(4)))
    
    ratio.Divide(h)
    ratio.SetTitle("")

    xTitle = histName
    words = histName.split("_")
    yLow = 0.8
    yHigh = 1.2
    if "_mu" in histName:
        xTitle = "#bf{actual #mu}"
    elif "nSig" in histName:
        xTitle = "#bf{No. Signal Jets}"
    elif "pTV" in histName:
        xTitle = "#bf{pTV} [GeV]"
    elif "pTBB" in histName:
        xTitle = "#bf{pTBB} [GeV]"
    elif "dPtTruth" in histName:
        xTitle = "#bf{TruthPt-RecoPt} [GeV]"
    elif "PtTrue_Final" in histName:
        ratio.GetXaxis().SetRangeUser(40,200)
        xTitle = "#bf{pT(B1)} [GeV]"
    elif "PtTrue_PtReco" in histName:
        ratio.GetXaxis().SetRangeUser(40,200)
        xTitle = "#bf{pT(PtRecoJ1)} [GeV]"
    elif "PtTrue_OneMu" in histName:
        ratio.GetXaxis().SetRangeUser(40,200)
        xTitle = "#bf{pT(OneMuJ1)} [GeV]"
    elif "PtTrue_GSC" in histName:
        ratio.GetXaxis().SetRangeUser(40,200)
        xTitle = "#bf{pT(GSCJ1)} [GeV]"
    elif "cutFlow" in histName:
        yLow = 0.85
        yHigh = 1.15
        xTitle = "#bf{Cuts}"
    else:    
        for i in range(len(words)):
            if "Mbb" in words[i]:
                xTitle = "#bf{"+words[i]+"} [GeV]"
                break
            elif "dRTruth" in words[i]:
                xTitle = "#bf{dR(TruthJ1,"+words[i+1]+"J1)} [GeV]"
                ratio.GetXaxis().SetRangeUser(xLow, xHigh)
                break
        

    
    ratio.GetXaxis().SetTitle(xTitle)
    ratio.GetXaxis().SetTitleSize(0.09)
    ratio.GetXaxis().SetTitleOffset(1.05)

    ratio.SetLineWidth(2)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetAxisRange(yLow, yHigh, "Y")
    ratio.GetXaxis().SetLabelSize(0.09)
    ratio.GetYaxis().SetLabelSize(0.07)
    ratio.GetYaxis().SetTitleOffset(0.7)
    ratio.GetYaxis().SetTitleSize(0.06)
    ratio.GetYaxis().SetTitle("PFlow/EMTopo")
    ratio.GetYaxis().SetNdivisions(506)
    
    ratio.Draw("hist")
    
    canvas.SaveAs(plot_dir+histName+".png")

#    print("Waiting for input")
#    input()
    

