"""
Helper functions to calculate systematics
"""
import os
import sys
import ROOT
#import argparse
import parse_args
from PFvEMTopo_Helper import *
import logging
logging.basicConfig(level=logging.INFO)

#ROOT.EnableImplicitMT()
# python PFvEMTopo_TreeReader.py PF0 0Lep a >& test.log
# python PFvEMTopo_TreeReader.py PF0 0Lep a --s test >& test.log
# nohup python PFvEMTopo_TreeReader.py PF0 2Lep a --s qq >& Log_0_2_a_qq.log &
# python PFvEMTopo_TreeReader.py PF0 0Lep full >& PF0_0L.log
# python PFvEMTopo_TreeReader.py PF1 0Lep full >& PF1_0L.log
args = parse_args.get_args()
sys.argv.append("-b")


PF = args.PF
channel = args.channel
period = args.period
# "gg" for gg, "qq" for qq, "" for both. test for using test output
samples = args.s
tag = ""
if PF == "PF0":
    tag="32-15"
else:
    tag="32-16"

#df = ROOT.ROOT.RDataFrame("Nominal",fileName)
#f = ROOT.TFile.Open(fileName, "read")
#t = f.Get("Nominal")

logging.info("Opening files")
t = ROOT.TChain("Nominal")
if samples != "test":
    if period == "full":
        logging.info("Opening a,d,e files /eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_a_MVA_D/{}*".format(PF,channel[0],tag,samples))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_a_MVA_D/{}*".format(PF,channel[0],tag,samples))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_d_MVA_D/{}*".format(PF,channel[0],tag,samples))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_e_MVA_D/{}*".format(PF,channel[0],tag,samples))
    else:
        logging.info("Opening files /eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/{}*".format(PF,channel[0],tag,period,samples))
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/{}*".format(PF,channel[0],tag,period,samples))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/ggZllHbb_PwPy8-0.root".format(PF,channel[0],tag,period))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/ggZllHbb_PwPy8-1.root".format(PF,channel[0],tag,period))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/ggZllHbb_PwPy8-2.root".format(PF,channel[0],tag,period))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-0.root".format(PF,channel[0],tag,period))
        t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-1.root".format(PF,channel[0],tag,period))
        
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-2.root".format(PF,channel[0],tag,period))
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-3.root".format(PF,channel[0],tag,period))
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-4.root".format(PF,channel[0],tag,period))
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-5.root".format(PF,channel[0],tag,period))
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-6.root".format(PF,channel[0],tag,period))
        #t.Add("/eos/user/r/ratkin/Reader_output/EasyTrees_Direct_{}/{}L_{}_{}_MVA_D/qqZllHbbJ_PwPy8MINLO-7.root".format(PF,channel[0],tag,period))
else:
    logging.info("Opening files /eos/user/r/ratkin/Reader_output/Test/{}L_{}_{}_MVA_H/*".format(channel[0],tag,period))
    t.Add("/eos/user/r/ratkin/Reader_output/Test/{}L_{}_{}_MVA_H/*".format(channel[0],tag,period))
do_writeTree = 1
# Make the histogram file
working_dir = "histograms/{}/{}/".format(channel,period)
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)
hist_file_name = working_dir + "{}_{}-0.root".format(PF,"qq_gg" if samples=="" else samples)

if os.path.isfile(hist_file_name):
    #inp=raw_input("File {} already exists, do you want to overwrite? y or n: ".format(hist_file_name))
    inp = "n"
    if inp == "y":
        logging.info("Overwriting file")
        hist_file = ROOT.TFile(hist_file_name, "RECREATE")
    elif inp == "n":
        logging.info("File won't be overwritten")
        do_writeTree = 0
else:
    hist_file = ROOT.TFile(hist_file_name, "CREATE")


cutFlow = {}
cutFlowTracks = {}
cutFlow_w = {}
cutFlowTracks_w = {}
cutFlowAnti_w = {} # counts at what cut the event fails the normal cuts if it passed the track cuts
cutFlowTracksAnti_w = {} # counts at what cut the event fails the track cuts if it passed the normal cuts
cutFlowAnti = {} # counts at what cut the event fails the normal cuts if it passed the track cuts
cutFlowTracksAnti = {} # counts at what cut the event fails the track cuts if it passed the normal cuts
regionPass = {}
regionPassTracks = {}
regionPassNoCuts = {}
cutFlowNames = []
cutFlowTracksNames = []
regionPassNames = []
if channel == "2Lep":
    cutFlow = {"nJ, nSJ >=1" : 0, "pTV>75" : 0, "nSigJets>=2" : 0, "numJets>=2" : 0, "J1pt>45" : 0, "2BJets" : 0, "mLL" : 0}
    cutFlowTracks = {"nJ, nSJ >=1" : 0, "pTV>75" : 0, "nSigJets>=2" : 0, "numJets>=2" : 0, "J1pt>45" : 0, "2BJets" : 0, "mLL" : 0,
                     "passVROR" : 0, "BTracksNotMatched==0" : 0}
    cutFlow_w = copy.deepcopy(cutFlow)
    cutFlowTracks_w = copy.deepcopy(cutFlowTracks)
    cutFlowAnti_w = copy.deepcopy(cutFlow)
    cutFlowTracksAnti_w = copy.deepcopy(cutFlowTracks)
    cutFlowAnti = copy.deepcopy(cutFlow)
    cutFlowTracksAnti = copy.deepcopy(cutFlowTracks)
    cutFlowNames = ["nJ, nSJ >=1", "pTV>75", "numJets>=2", "nSigJets>=2", "J1pt>45", "2BJets", "mLL"]
    cutFlowTracksNames = ["nJ, nSJ >=1", "pTV>75", "numJets>=2", "nSigJets>=2", "J1pt>45", "2BJets", "mLL", "passVROR", "BTracksNotMatched==0"]
elif channel == "0Lep":
    cutFlow = {"nJ, nSJ >=1" : 0, "MET>150" : 0, "nSigJets>=2" : 0, "numJets>=2" : 0, "J1pt>45" : 0, "2BJets" : 0, 
               "dPhiMETBB" : 0, "dPhiBB" : 0, "dPhiMETMPT" : 0, "MinDPhiMETJet" : 0, "HT" : 0}
    cutFlowTracks = {"nJ, nSJ >=1" : 0, "MET>150" : 0, "nSigJets>=2" : 0, "numJets>=2" : 0, "J1pt>45" : 0, "2BJets" : 0, 
               "dPhiMETBB" : 0, "dPhiBB" : 0, "dPhiMETMPT" : 0, "MinDPhiMETJet" : 0, "HT" : 0, "passVROR" : 0, "BTracksNotMatched==0" : 0}
    cutFlow_w = copy.deepcopy(cutFlow)
    cutFlowTracks_w = copy.deepcopy(cutFlowTracks)
    cutFlowAnti_w = copy.deepcopy(cutFlow)
    cutFlowTracksAnti_w = copy.deepcopy(cutFlowTracks)
    cutFlowAnti = copy.deepcopy(cutFlow)
    cutFlowTracksAnti = copy.deepcopy(cutFlowTracks)
    cutFlowNames = ["nJ, nSJ >=1", "MET>150", "numJets>=2", "nSigJets>=2", "J1pt>45", "2BJets", "dPhiMETBB", "dPhiBB", 
                    "dPhiMETMPT", "MinDPhiMETJet", "HT"]
    cutFlowTracksNames = ["nJ, nSJ >=1", "MET>150", "numJets>=2", "nSigJets>=2", "J1pt>45", "2BJets", "dPhiMETBB", "dPhiBB", 
                    "dPhiMETMPT", "MinDPhiMETJet", "HT", "passVROR", "BTracksNotMatched==0"]

regionPass = {"inc" : 0, "inc_2SigJet" : 0, "inc_2s&3j" : 0,
              "1jet_inc" : 0, "1jet_75ptv" : 0, "1jet_150ptv" : 0, "1jet_250ptv" : 0, "1jet_400ptv" : 0, 
              "2jet_inc" : 0, "2jet_75ptv" : 0, "2jet_150ptv" : 0, "2jet_250ptv" : 0, "2jet_400ptv" : 0, 
              "3jet_inc" : 0, "3jet_75ptv" : 0, "3jet_150ptv" : 0, "3jet_250ptv" : 0, "3jet_400ptv" : 0, 
              "4jet_inc" : 0, "4jet_75ptv" : 0, "4jet_150ptv" : 0, "4jet_250ptv" : 0, "4jet_400ptv" : 0}
# -1 = passed none, 1 = passed only normal cuts, -2 = passed only track cuts, 2 = passed both cuts
acceptedEvents_w = {-2 : copy.deepcopy(regionPass), -1 : copy.deepcopy(regionPass), 0 : copy.deepcopy(regionPass),1 : copy.deepcopy(regionPass), 2 : copy.deepcopy(regionPass)}
acceptedEvents = {-2 : copy.deepcopy(regionPass), -1 : copy.deepcopy(regionPass), 0 : copy.deepcopy(regionPass),1 : copy.deepcopy(regionPass), 2 : copy.deepcopy(regionPass)}
regionPassNames = ["inc", "inc_2SigJet", "inc_2s&3j",
                   "1jet_inc", "1jet_75ptv", "1jet_150ptv", "1jet_250ptv", "1jet_400ptv",
                   "2jet_inc", "2jet_75ptv", "2jet_150ptv", "2jet_250ptv", "2jet_400ptv",
                   "3jet_inc", "3jet_75ptv", "3jet_150ptv", "3jet_250ptv", "3jet_400ptv",
                   "4jet_inc", "4jet_75ptv", "4jet_150ptv", "4jet_250ptv", "4jet_400ptv"]
#noWeightsHistos = ["nSigJets", "sumnSigJets_mu", "nMu", "nTrackJets", "nBTrackJets", "nBTags", "nNewBTags", "nTrueBTags", "nMatchedTruthJets", "nRecoMatchToATrack",
#                   "nTrackMatchToAReco", "nTracksNotMatched", "nSigNotMatched", "nBTracksNotMatched", "nBSigNotMatched",
#                   "nBJetsNotMatchedBTracks", "nBTracksNotMatchedBJets", "nBSigMatchedBTrack", "nSigMatchedBTracks"]

N = t.GetEntries()
#N=100000
#N=2000
logging.info("Making histos")
logging.info("Number entries {}, but running over {}".format(t.GetEntries(),N))
#exit()
it = -1
mod = int(N*0.005) if (int(N*0.005)>0) else 1
#main
for event in t:
    it+=1
    calls = 0
    if it%mod == 0:
        logging.info("{}%    {}".format(int(100*it/N), it))
    if it == N:
        logging.info("Reached max N. Breaking loop")
        break
    #    break
    #print(t.nSigJets)
    #print("## {}".format(it))
    #print("GSC   {} ptReco {}".format(t.GSCMbb,t.PtRecoMbb))
    #print("Onemu {} mBB    {}".format(t.OneMuMbb, t.mBB))
    '''
    print("Run {}, Event {}".format(t.RunNumber, t.EventNumber))
    print("Actual event weight {}".format(t.EventWeight))
    EWest = t.MCEventWeight
    print("MC {} ratio {}".format(EWest, t.EventWeight/EWest))
    EWest = t.MCEventWeight*t.LumiWeight
    print("MC*Lumi {} ratio {}".format(EWest, t.EventWeight/EWest))
    EWest = t.PUWeight*t.LumiWeight*t.MCEventWeight
    print("lumi*mc*PU {} ratio {}".format(EWest, t.EventWeight/EWest))
    EWest = t.PUWeight*t.TriggerSF*t.LumiWeight*t.MCEventWeight
    print("lumi*mc*trig*PU {} ratio {}".format(EWest, t.EventWeight/EWest))
    EWest = t.PUWeight*t.JVTWeight*t.TriggerSF*t.LumiWeight*t.MCEventWeight
    print("lumi*mc*trig*jvt*PU {} ratio {}".format(EWest, t.EventWeight/EWest))
    if channel == "2Lep":
        EWest = t.LeptonSF*t.PUWeight*t.JVTWeight*t.TriggerSF*t.LumiWeight*t.MCEventWeight
        print("lumi*mc*trig*jvt*PU*lep {} ratio {}".format(EWest, t.EventWeight/EWest))
        EWest = t.BTagSF*t.LeptonSF*t.PUWeight*t.JVTWeight*t.TriggerSF*t.LumiWeight*t.MCEventWeight
        print("lumi*mc*trig*jvt*PU*lep*btag {} ratio {}".format(EWest, t.EventWeight/EWest))
        print("l1Pt {}, l2Pt {}".format(t.ptL1,t.ptL2))
        print("l1Eta {}, l2Eta {}".format(t.etaL1,t.etaL2))
    else:
        EWest = t.BTagSF*t.PUWeight*t.JVTWeight*t.TriggerSF*t.LumiWeight*t.MCEventWeight
        print("lumi*mc*trig*jvt*PU*btag {} ratio {}".format(EWest, t.EventWeight/EWest))
        print("MET {}, dphiMETMPT {}, TruthWZptJ1 {}, TruthWZptJ2 {}".format(t.MET,t.dPhiMETMPT,t.TruthWZptJ1,t.TruthWZptJ2))
    continue
    '''
    #nBJets = t.nbJets if channel == "2Lep" else t.nTags
    #if nBJets != 2:
    #    continue
    NumJets = t.nJets if channel == "2Lep" else t.nJ
    pTV = t.pTV if channel == "2Lep" else t.MET
    mu = t.ActualMu
    numSigJets = t.nSigJets
    Reg = ""
    HSorPU = ""
    notHSorPU = ""
    HadOrSemi = "had" if (t.GSCMbb == t.OneMuMbb) else "semi"

    if (t.TruthWZptJ1 > 10):
        HSorPU = "HS"
        notHSorPU = "PU"
    else:
        HSorPU = "PU"
        notHSorPU = "HS"
        
    Reg = "4jet" if (NumJets > 3) else "{}jet".format(int(NumJets))
    RegJ = Reg+"_inc"
    if (pTV >= 75 and pTV < 150): Reg += "_75ptv"
    elif (pTV >= 150 and pTV < 250): Reg += "_150ptv"
    elif (pTV >= 250 and pTV < 400): Reg += "_250ptv"
    elif (pTV >= 400): Reg += "_400ptv"

    EWeight = t.MCEventWeight*t.LumiWeight

    cutFlow["nJ, nSJ >=1"]+=1
    cutFlowTracks["nJ, nSJ >=1"]+=1
    cutFlow_w["nJ, nSJ >=1"]+=EWeight
    cutFlowTracks_w["nJ, nSJ >=1"]+=EWeight
    eventPassed = -1

    trackVars = getTrackVars(t)
    pass_cuts = cuts2Lep(t, cutFlow, cutFlow_w, False, []) if channel == "2Lep" else cuts0Lep(t, cutFlow, cutFlow_w, False, [])
    pass_cutsTracks = cuts2Lep(t, cutFlowTracks, cutFlowTracks_w, True, trackVars) if channel == "2Lep" else cuts0Lep(t, cutFlowTracks, cutFlowTracks_w, True, trackVars)

    ########################
    # NORMAL VARIABLE DEFINITIONS
    ########################
    
    logging.debug("Region {} nSigJets {}".format(Reg, numSigJets))#,HadOrSemi,HSorPU))
    fill_histos(hist_regions["inc"], hist_regions_w["inc"], t, calls)
    calls=1
    #fill_histos(hist_regions["inc_{}".format(HadOrSemi)], hist_regions_w["inc_{}".format(HadOrSemi)], t)
    #fill_histos(hist_regions["inc_{}".format(HSorPU)], hist_regions_w["inc_{}".format(HSorPU)], t)
    #hist_regions["inc_{}".format(notHSorPU)]["nSigJets"].Fill(0)
    #hist_regions_w["inc_{}".format(notHSorPU)]["nSigJets"].Fill(0)
    if numSigJets >= 2: # and NumJets <= 3:
        fill_histos(hist_regions["inc_2SigJet"], hist_regions_w["inc_2SigJet"], t, calls)
        if NumJets <= 3:
            fill_histos(hist_regions["inc_2s&3j"], hist_regions_w["inc_2s&3j"], t, calls)
        #fill_histos(hist_regions["inc_2jet_{}".format(HadOrSemi)], hist_regions_w["inc_2jet_{}".format(HadOrSemi)], t, calls)
        #fill_histos(hist_regions["inc_2jet_{}".format(HSorPU)], hist_regions_w["inc_2jet_{}".format(HSorPU)], t, calls)
        #hist_regions["inc_2jet_{}".format(notHSorPU)]["nSigJets"].Fill(0)
        #hist_regions_w["inc_2jet_{}".format(notHSorPU)]["nSigJets"].Fill(0)
        
    fill_histos(hist_regions[Reg], hist_regions_w[Reg], t, calls)
    fill_histos(hist_regions[RegJ], hist_regions_w[RegJ], t, calls)
    #fill_histos(hist_regions["{}_{}".format(Reg,HadOrSemi)], hist_regions_w["{}_{}".format(Reg,HadOrSemi)], t, calls)
    #fill_histos(hist_regions["{}_{}".format(Reg,HSorPU)], hist_regions_w["{}_{}".format(Reg,HSorPU)], t, calls)
    #hist_regions["{}_{}".format(Reg,notHSorPU)]["nSigJets"].Fill(0)
    #hist_regions_w["{}_{}".format(Reg,notHSorPU)]["nSigJets"].Fill(0)

    if pass_cuts > 0:
        logging.debug("Passed normal cuts")
        eventPassed*=-1
        fill_histos(hist_regions_cuts["inc"], hist_regions_cuts_w["inc"], t, calls)
        #fill_histos(hist_regions_cuts["inc_{}".format(HadOrSemi)], hist_regions_cuts_w["inc_{}".format(HadOrSemi)], t, calls)
        #fill_histos(hist_regions_cuts["inc_{}".format(HSorPU)], hist_regions_cuts_w["inc_{}".format(HSorPU)], t, calls)
        #hist_regions_cuts["inc_{}".format(notHSorPU)]["nSigJets"].Fill(0)
        #hist_regions_cuts_w["inc_{}".format(notHSorPU)]["nSigJets"].Fill(0)
        if numSigJets >= 2: # and NumJets <= 3:
            fill_histos(hist_regions_cuts["inc_2SigJet"], hist_regions_cuts_w["inc_2SigJet"], t, calls)
            if NumJets <= 3:
                fill_histos(hist_regions_cuts["inc_2s&3j"], hist_regions_cuts_w["inc_2s&3j"], t, calls)
            #fill_histos(hist_regions_cuts["inc_2jet_{}".format(HadOrSemi)], hist_regions_cuts_w["inc_2jet_{}".format(HadOrSemi)], t, calls)
            #fill_histos(hist_regions_cuts["inc_2jet_{}".format(HSorPU)], hist_regions_cuts_w["inc_2jet_{}".format(HSorPU)], t, calls)
            #hist_regions_cuts["inc_2jet_{}".format(notHSorPU)]["nSigJets"].Fill(0)
            #hist_regions_cuts_w["inc_2jet_{}".format(notHSorPU)]["nSigJets"].Fill(0)
            
        fill_histos(hist_regions_cuts[Reg], hist_regions_cuts_w[Reg], t, calls)
        fill_histos(hist_regions_cuts[RegJ], hist_regions_cuts_w[RegJ], t, calls)
        #fill_histos(hist_regions_cuts["{}_{}".format(Reg,HadOrSemi)], hist_regions_cuts_w["{}_{}".format(Reg,HadOrSemi)], t, calls)
        #fill_histos(hist_regions_cuts["{}_{}".format(Reg,HSorPU)], hist_regions_cuts_w["{}_{}".format(Reg,HSorPU)], t, calls)
        #hist_regions_cuts["{}_{}".format(Reg,notHSorPU)]["nSigJets"].Fill(0)
        #hist_regions_cuts_w["{}_{}".format(Reg,notHSorPU)]["nSigJets"].Fill(0)
        
    #continue

    ########################
    # TRACK TAGGED VARIABLE DEFINITIONS
    ########################

    #print("{} {} {}".format(Reg,HadOrSemi,HSorPU))
    #fill_histos(hist_regions_T["inc"], hist_regions_w_T["inc"], t)
        
    if pass_cutsTracks > 0:
        logging.debug("Passed track cuts")
        eventPassed*=2
        fill_histos(hist_regions_cuts_T["inc"], hist_regions_cuts_w_T["inc"], t, calls)
        #fill_histos(hist_regions_cutsT["inc_{}".format(HadOrSemi)], hist_regions_cutsT_w["inc_{}".format(HadOrSemi)], t, calls)
        #fill_histos(hist_regions_cutsT["inc_{}".format(HSorPU)], hist_regions_cutsT_w["inc_{}".format(HSorPU)], t, calls)
        #hist_regions_cutsT["inc_{}".format(notHSorPU)]["nSigJets"].Fill(0)
        #hist_regions_cutsT_w["inc_{}".format(notHSorPU)]["nSigJets"].Fill(0)
        if numSigJets >= 2: # and NumJets <= 3:
            fill_histos(hist_regions_cuts_T["inc_2SigJet"], hist_regions_cuts_w_T["inc_2SigJet"], t, calls)
            if NumJets <= 3:
                fill_histos(hist_regions_cuts_T["inc_2s&3j"], hist_regions_cuts_w_T["inc_2s&3j"], t, calls)
            #fill_histos(hist_regions_cutsT["inc_2jet_{}".format(HadOrSemi)], hist_regions_cutsT_w["inc_2jet_{}".format(HadOrSemi)], t, calls)
            #fill_histos(hist_regions_cutsT["inc_2jet_{}".format(HSorPU)], hist_regions_cutsT_w["inc_2jet_{}".format(HSorPU)], t, calls)
            #hist_regions_cutsT["inc_2jet_{}".format(notHSorPU)]["nSigJets"].Fill(0)
            #hist_regions_cutsT_w["inc_2jet_{}".format(notHSorPU)]["nSigJets"].Fill(0)
            
        fill_histos(hist_regions_cuts_T[Reg], hist_regions_cuts_w_T[Reg], t, calls)
        fill_histos(hist_regions_cuts_T[RegJ], hist_regions_cuts_w_T[RegJ], t, calls)
        #fill_histos(hist_regions_cutsT["{}_{}".format(Reg,HadOrSemi)], hist_regions_cutsT_w["{}_{}".format(Reg,HadOrSemi)], t, calls)
        #fill_histos(hist_regions_cutsT["{}_{}".format(Reg,HSorPU)], hist_regions_cutsT_w["{}_{}".format(Reg,HSorPU)], t, calls)
        #hist_regions_cutsT["{}_{}".format(Reg,notHSorPU)]["nSigJets"].Fill(0)
        #hist_regions_cutsT_w["{}_{}".format(Reg,notHSorPU)]["nSigJets"].Fill(0)
        
    logging.debug("pass_cuts {} pass_cutsTracks {}".format(pass_cuts, pass_cutsTracks))
    if (eventPassed == -2): # passed tracks but not norm
        logging.debug("Failed normal cuts at {}".format(cutFlowNames[1-1*pass_cuts]))
        cutFlowAnti_w[cutFlowNames[1-1*pass_cuts]]+=EWeight
        cutFlowAnti[cutFlowNames[1-1*pass_cuts]]+=1
    elif (eventPassed == 1): # passed norm but not tracks
        logging.debug("Failed track cuts at {}".format(cutFlowTracksNames[1-1*pass_cutsTracks]))
        cutFlowTracksAnti_w[cutFlowTracksNames[1-1*pass_cutsTracks]]+=EWeight
        cutFlowTracksAnti[cutFlowTracksNames[1-1*pass_cutsTracks]]+=1

    acceptedEvents_w[eventPassed][Reg]+=EWeight
    acceptedEvents_w[eventPassed]["inc"]+=EWeight
    acceptedEvents_w[0][Reg]+=EWeight
    acceptedEvents_w[0]["inc"]+=EWeight
    acceptedEvents_w[eventPassed][RegJ]+=EWeight
    acceptedEvents_w[0][RegJ]+=EWeight
    acceptedEvents[eventPassed][Reg]+=1
    acceptedEvents[eventPassed]["inc"]+=1
    acceptedEvents[0][Reg]+=1
    acceptedEvents[0]["inc"]+=1
    acceptedEvents[eventPassed][RegJ]+=1
    acceptedEvents[0][RegJ]+=1
    if numSigJets >= 2: # and NumJets <= 3:
        acceptedEvents_w[eventPassed]["inc_2SigJet"]+=EWeight
        acceptedEvents_w[0]["inc_2SigJet"]+=EWeight
        acceptedEvents[eventPassed]["inc_2SigJet"]+=1
        acceptedEvents[0]["inc_2SigJet"]+=1
        if NumJets <= 3:
            acceptedEvents_w[eventPassed]["inc_2s&3j"]+=EWeight
            acceptedEvents_w[0]["inc_2s&3j"]+=EWeight
            acceptedEvents[eventPassed]["inc_2s&3j"]+=1
            acceptedEvents[0]["inc_2s&3j"]+=1
            
logging.info("Filling histograms complete")
if not (it == N or it == N-1):
    logging.error("ERROR: Loop exited ({}) before max N ({}) was reached".format(it, N))



if do_writeTree:
    h_cutFlow = ROOT.TH1D("cutFlow","cutFlow",len(cutFlow),-0.5,len(cutFlow)-0.5)
    h_cutFlowTracks = ROOT.TH1D("cutFlowTracks","cutFlowTracks",len(cutFlowTracks),-0.5,len(cutFlowTracks)-0.5)
    h_cutFlow_w = ROOT.TH1D("cutFlow_w","cutFlow_w",len(cutFlow_w),-0.5,len(cutFlow_w)-0.5)
    h_cutFlowTracks_w = ROOT.TH1D("cutFlowTracks_w","cutFlowTracks_w",len(cutFlowTracks_w),-0.5,len(cutFlowTracks_w)-0.5)
    h_cutFlowAnti_w = ROOT.TH1D("cutFlowAnti_w","cutFlowAnti_w",len(cutFlowAnti_w),-0.5,len(cutFlowAnti_w)-0.5)
    h_cutFlowTracksAnti_w = ROOT.TH1D("cutFlowTracksAnti_w","cutFlowTracksAnti_w",len(cutFlowTracksAnti_w),-0.5,len(cutFlowTracksAnti_w)-0.5)
    h_cutFlowAnti = ROOT.TH1D("cutFlowAnti","cutFlowAnti",len(cutFlowAnti),-0.5,len(cutFlowAnti)-0.5)
    h_cutFlowTracksAnti = ROOT.TH1D("cutFlowTracksAnti","cutFlowTracksAnti",len(cutFlowTracksAnti),-0.5,len(cutFlowTracksAnti)-0.5)
    h_regionPass_w = ROOT.TH2D("regionPass_w","regionPass_w",5,-2,3,len(regionPass),0,len(regionPass))
    h_regionPass = ROOT.TH2D("regionPass","regionPass",5,-2,3,len(regionPass),0,len(regionPass))
    counter = 0
    for cut in cutFlowNames:
        h_cutFlow.SetBinContent(counter+1, cutFlow[cut])
        h_cutFlow.GetXaxis().SetBinLabel(counter+1, cut)
        h_cutFlow_w.SetBinContent(counter+1, cutFlow_w[cut])
        h_cutFlow_w.GetXaxis().SetBinLabel(counter+1, cut)
        h_cutFlowAnti_w.SetBinContent(counter+1, cutFlowAnti_w[cut])
        h_cutFlowAnti_w.GetXaxis().SetBinLabel(counter+1, cut)
        h_cutFlowAnti.SetBinContent(counter+1, cutFlowAnti[cut])
        h_cutFlowAnti.GetXaxis().SetBinLabel(counter+1, cut)
        counter +=1
    counter = 0
    for cut in cutFlowTracksNames:
        h_cutFlowTracks.SetBinContent(counter+1, cutFlowTracks[cut])
        h_cutFlowTracks.GetXaxis().SetBinLabel(counter+1, cut)
        h_cutFlowTracks_w.SetBinContent(counter+1, cutFlowTracks_w[cut])
        h_cutFlowTracks_w.GetXaxis().SetBinLabel(counter+1, cut)
        h_cutFlowTracksAnti_w.SetBinContent(counter+1, cutFlowTracksAnti_w[cut])
        h_cutFlowTracksAnti_w.GetXaxis().SetBinLabel(counter+1, cut)
        h_cutFlowTracksAnti.SetBinContent(counter+1, cutFlowTracksAnti[cut])
        h_cutFlowTracksAnti.GetXaxis().SetBinLabel(counter+1, cut)
        counter +=1
    #print(acceptedEvents_w[0])
    for pas in acceptedEvents_w:
        xlabel = ""
        if pas == 0:
            xbin = 1
            xlabel = "Before_Cuts"
        elif pas == 1:
            xbin = 3
            xlabel = "Pass_Norm_Cuts"
        elif pas == 2:
            xbin = 2
            xlabel = "Pass_Both_Cuts"
        elif pas == -1:
            xbin = 5
            xlabel = "Pass_No_Cuts"
        elif pas == -2:
            xbin = 4
            xlabel = "Pass_Track_Cuts"
        counter=0
        for reg in regionPassNames:
            h_regionPass_w.SetBinContent(xbin, counter+1, acceptedEvents_w[pas][reg])
            h_regionPass_w.GetYaxis().SetBinLabel(counter+1, reg)
            h_regionPass_w.GetXaxis().SetBinLabel(xbin, xlabel)
            h_regionPass.SetBinContent(xbin, counter+1, acceptedEvents[pas][reg])
            h_regionPass.GetYaxis().SetBinLabel(counter+1, reg)
            h_regionPass.GetXaxis().SetBinLabel(xbin, xlabel)
            counter +=1

    logging.info("Writing histograms")
    assert hist_file.IsOpen()
    hist_file.Write()
    
    h_cutFlow.Write()
    h_cutFlowTracks.Write()
    h_cutFlow_w.Write()
    h_cutFlowTracks_w.Write()
    h_cutFlowAnti_w.Write()
    h_cutFlowTracksAnti_w.Write()
    h_cutFlowAnti.Write()
    h_cutFlowTracksAnti.Write()
    h_regionPass_w.Write()
    h_regionPass.Write()
    for reg in hist_regions_w:
        #print("  "+reg)
        for name in hist_regions_w[reg]:
            #print("    "+name)
            hist_regions_w[reg][name].Write()
            hist_regions_cuts_w[reg][name].Write()
            hist_regions_cuts_w_T[reg][name].Write()
            #hist_regions_w_T[reg][name].Write()
            #if name in noWeightsHistos:
            #    hist_regions_cuts[reg][name].Write()
            #    hist_regions_cuts_T[reg][name].Write()
            #    hist_regions[reg][name].Write()
            #    #hist_regions_T[reg][name].Write()
            
    logging.info("Closing hist file")
    hist_file.Write()
    hist_file.Close()
