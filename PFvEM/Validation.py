"""
Runs over histograms
"""
import os
import sys
import ROOT
#import argparse

# run as: a is the period (use all for combination), em18 is the jet type for 33-01 (leave blank if doing special tests)
# python Validation.py a em18 old old

per = sys.argv[1]
JType = ""
if not len(sys.argv)==2:
    JType = sys.argv[2]
sys.argv.append("-b")

print ("Opening files")
File1=""
File2=""
nom="em"
#oldOrNew1 = sys.argv[3] if sys.argv[3]=="new" else ""
#oldOrNew2 = sys.argv[4] if sys.argv[4]=="new" else ""
#oldOrNew1 = "new"
#oldOrNew2 = "new"

if per=="all":
    #File1 = "/afs/cern.ch/user/r/ratkin/CxAODReader_07Jul/run/histTest/hist-ZvvHbb.root" #.format(oldOrNew1 if oldOrNew1=="" else "-new") 
    #if nom=="em" else "/afs/cern.ch/user/r/ratkin/CxAODReader_07Jul/run/Test/hist-ZvvHbb-pf.root"
    File2 = "/afs/cern.ch/user/r/ratkin/CxAODReader_11Nov_33-04/run/Test/{}/ZvvHbb.root".format(JType) #,oldOrNew2 if oldOrNew2=="" else "-new")
    File1 = "/afs/cern.ch/user/r/ratkin/CxAODReader_11Nov_33-04/run/Test/32-15/ZvvHbb.root"
else:
    #File1 = "/afs/cern.ch/user/r/ratkin/CxAODReader_01Oct_32-15/run/Test/em/Reader_0L_32-15_{}_MVA_D/hist-qqZvvHbbJ_PwPy8MINLO.root".format(per)
    #File1 = "/afs/cern.ch/user/r/ratkin/CxAODReader_07Jul/run/histTest/Reader_0L_32-15_{}_MVA_D/hist-ZvvHbb.root".format(per) #,oldOrNew1 if oldOrNew1=="" else "_new")
    #File2 = "/afs/cern.ch/user/r/ratkin/CxAODReader_28Sep/run/hist_outputs/{}/hist-ZvvHbb_{}.root".format(JType,per)
    #File1 = "/afs/cern.ch/work/c/ckato/public/forCF/CF-r32-24-Reader-Resolved-05-CSFix/run06_zeroLep_115k/Reader_0L_32-15_{}_MVA_D/hist-qqZvvHbbJ_PwPy8MINLO.root".format(per)
    #File1 = "/afs/cern.ch/user/r/ratkin/CxAODReader_28Sep/run/cutflow/pf18/hists/Reader_0L_33-01_a_MVA_D/hist-qqZvvHbbJ_PwPy8MINLO.root"
    #File2 = "/afs/cern.ch/user/r/ratkin/CxAODReader_28Sep/run/Test/{}/Reader_0L_33-01_{}_MVA_D/hist-ZvvHbb.root".format(JType,per) #,oldOrNew2 if oldOrNew2=="" else "_new")
    File2 = "/afs/cern.ch/user/r/ratkin/CxAODReader_11Nov_33-04/run/Test/{}/ZvvHbb_{}.root".format(JType,per) #,oldOrNew2 if oldOrNew2=="" else "-new")
    File1 = "/afs/cern.ch/user/r/ratkin/CxAODReader_11Nov_33-04/run/Test/32-15/ZvvHbb_{}.root".format(per)

#extra_arg = "_{}_{}_{}".format("old" if oldOrNew1=="" else oldOrNew1,"old" if oldOrNew2=="" else oldOrNew2,JType)
extra_arg = "_"+JType
VTag = "32-15"

#working_dir = "histograms/validation/{}/".format(JType)
#if JType == "":
working_dir = "histograms/validation/33-04/{}/".format(JType)
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)

dRBBRegions = ["SR"]
JetRegions = ["2tag2jet","2tag3jet"]
ptvRegions = ["150_250ptv","250ptv"]
Vars = ["mBB","MET","dPhiVBB","dRBB","softMET","pTB1","HT","pTB2","dPhiBB","dEtaBB","SumPtJets"]
ranges150 = {"mBB":[0,250,0],"MET":[100,300,0],"dPhiVBB":[2,3.2,0],"dRBB":[0,3,0],"softMET":[0,100,0],
             "pTB1":[0,400,0],"HT":[200,1000,5],"pTB2":[0,200,0],"dPhiBB":[0,2.5,2],"dEtaBB":[0,3,2],"SumPtJets":[0,500,5]}
ranges250 = {"mBB":[0,250,0],"MET":[200,450,0],"dPhiVBB":[2,3.2,0],"dRBB":[0,3,0],"softMET":[0,100,0],
             "pTB1":[0,500,0],"HT":[300,1000,5],"pTB2":[0,300,0],"dPhiBB":[0,2.5,2],"dEtaBB":[0,3,2],"SumPtJets":[100,700,5]}
histNames=[]
for dRBBReg in dRBBRegions:
    for JetReg in JetRegions:
        for ptvReg in ptvRegions:
            for Var in Vars:
                histNames.append("qqZvvH125_"+JetReg+"_"+ptvReg+"_"+dRBBReg+"_"+Var)

#histNames = ["qqZvvH125_2tag2jet_150_250ptv_"+dRBBRegion+"_mBB","qqZvvH125_2tag2jet_150_250ptv_"+dRBBRegion+"_MET",
#             "qqZvvH125_2tag2jet_250ptv_"+dRBBRegion+"_MET","qqZvvH125_2tag2jet_250ptv_"+dRBBRegion+"_mBB",
#             "qqZvvH125_2tag3jet_150_250ptv_"+dRBBRegion+"_mBB","qqZvvH125_2tag3jet_150_250ptv_"+dRBBRegion+"_MET",
#             "qqZvvH125_2tag3jet_250ptv_"+dRBBRegion+"_MET","qqZvvH125_2tag3jet_250ptv_"+dRBBRegion+"_mBB"]

#histNames=[]
histNames2 = ["CutsResolved","CutsResolved_NoWeight"]
histNames=[]

print("File1: "+File1)
print("File2: "+File2)

f1 = ROOT.TFile.Open(File1)
f2 = ROOT.TFile.Open(File2)
#f3 = ROOT.TFile.Open(File3)

for hName in histNames:
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    
    hist1 = f1.Get(hName)
    hist2 = f2.Get(hName)
    hist1a = f1.Get(hName.replace("qqZ","ggZ"))
    hist2a = f2.Get(hName.replace("qqZ","ggZ"))

    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
  
    print("looking at "+hName)

    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    ROOT.gStyle.SetOptStat(0)
    h1 = hist1.Clone()
    h2 = hist2.Clone()
    #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
    h1.Add(hist1a.Clone())
    h2.Add(hist2a.Clone())
    #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))

    xLow = 0
    xHigh = 500

    if "150_250ptv" in hName:
        vals = ranges150[hName.split("_")[-1]]
        xLow = vals[0]
        xHigh = vals[1]
        if vals[2]!=0:
            h1.Rebin(vals[2])
            h2.Rebin(vals[2])
    else:
        vals = ranges250[hName.split("_")[-1]]
        xLow = vals[0]
        xHigh = vals[1]
        if vals[2]!=0:
            h1.Rebin(vals[2])
            h2.Rebin(vals[2])
        
    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.1

    h1.Draw("hist")
    h1.SetTitle("")
    h1.SetAxisRange(0.0, y_max, "Y")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    #h1.SetMarkerColor(ROOT.kBlue)
    #h1.SetMarkerStyle(8)
    #h1.SetMarkerSize(1.5)
    h1.SetLineColor(ROOT.kBlue)    
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
    h1.GetXaxis().SetRangeUser(xLow,xHigh)
        
    h2.Draw("same")
    h2.SetMarkerColor(ROOT.kRed)
    #h2.SetMarkerStyle(8)
    #h2.SetMarkerSize(1.5)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)
    

    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend.SetHeader("              MC,    NEntries,    SoW")
    legend.AddEntry(h1, "{} {}, {:.1f}, {:.1f}".format(VTag,nom,h1.GetEntries(),h1.Integral()), "l")
    legend.AddEntry(h2, "33-04 "+JType+", {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()


    lower.cd()
    rat = h2.Clone()
    rat.Divide(h1)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(hName.replace("qqZ","qq+ggZ"))
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    #Line = ROOT.TLine(xLow,1.,xHigh,1.)
    #Line.SetLineWidth(2)
    #Line.SetLineColor(ROOT.kBlack)
    yLow = 0.89
    yHigh = 1.11
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("33-04/{}".format(VTag))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    #Line.Draw("same")
    rat.GetXaxis().SetRangeUser(xLow,xHigh)

    
    canvas.SaveAs(working_dir+hName+"_{}_{}{}.png".format(nom,per,extra_arg))

for hName in histNames2:
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    
    hist1 = f1.Get("CutFlow/Nominal/"+hName)
    hist2 = f2.Get("CutFlow/Nominal/"+hName)

    print("looking at "+hName)

    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    ROOT.gStyle.SetOptStat(0)
    h1 = hist1.Clone()
    h2 = hist2.Clone()
    rat = hist2.Clone()
    rat.Divide(h1)

    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.1

    h1.Draw("hist")
    h1.SetTitle("")
    h1.SetAxisRange(0.0, y_max, "Y")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    #h1.SetMarkerColor(ROOT.kBlue)
    #h1.SetMarkerStyle(8)
    #h1.SetMarkerSize(1.5)
    h1.SetLineColor(ROOT.kBlue)    
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
        
    h2.Draw("same")
    h2.SetMarkerColor(ROOT.kRed)
    #h2.SetMarkerStyle(8)
    #h2.SetMarkerSize(1.5)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)

    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend.SetHeader("              MC,    NEntries,    SoW")
    legend.AddEntry(h1, "{} {}, {:.1f}, {:.1f}".format(VTag,nom,h1.GetEntries(),h1.Integral()), "l")
    legend.AddEntry(h2, "33-04 "+JType+", {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()


    lower.cd()
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(hName)
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    #Line = ROOT.TLine(xLow,1.,xHigh,1.)
    #Line.SetLineWidth(2)
    #Line.SetLineColor(ROOT.kBlack)
    yLow = 0.89
    yHigh = 1.11
    #if "NoWeight" in hName:
    #    yLow = 0.99
    #    yHigh = 1.01
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("33-04/{}".format(VTag))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    
    
    canvas.SaveAs(working_dir+hName+"_{}_{}{}.png".format(nom,per,extra_arg))


exit
for hName in histNames: #NOT DONE CORRECTLY
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    
    if "qqZvvH125_2tag3jet_150_250ptv_SR" not in hName:
        continue
    
    hist1 = f1.Get(hName)
    hist2 = f2.Get(hName)
    hist1a = f1.Get(hName.replace("qqZ","ggZ"))
    hist2a = f2.Get(hName.replace("qqZ","ggZ"))
    hName2 = hName.replace("2tag3jet","2tag2jet")
    print(hName)
    print(hName2)
    hist3 = f1.Get(hName2)
    hist4 = f2.Get(hName2)
    hist3a = f1.Get(hName2.replace("qqZ","ggZ"))
    hist4a = f2.Get(hName2.replace("qqZ","ggZ"))

    h1 = hist1.Clone()
    h2 = hist2.Clone()
    h1.Add(hist1a.Clone())
    h2.Add(hist2a.Clone())
    h3 = hist3.Clone()
    h4 = hist4.Clone()
    h3.Add(hist3a.Clone())
    h4.Add(hist4a.Clone())
    

    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
  
    print("looking at "+hName)

    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    ROOT.gStyle.SetOptStat(0)
    #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))

    xLow = 0
    xHigh = 500

    if "150_250ptv" in hName:
        vals = ranges150[hName.split("_")[-1]]
        xLow = vals[0]
        xHigh = vals[1]
        if vals[2]!=0:
            h1.Rebin(vals[2])
            h2.Rebin(vals[2])
            h3.Rebin(vals[2])
            h4.Rebin(vals[2])
            
    y_max = 1.5*1.1

    h1.Scale(1./h1.Integral())
    h2.Scale(1./h2.Integral())
    h3.Scale(1./h3.Integral())
    h4.Scale(1./h4.Integral())

    rat3j = h2.Clone()
    rat2j = h4.Clone()
    rat3j.Divide(h1)
    rat2j.Divide(h3)

    rat2j.Draw("hist")
    rat2j.SetTitle("")
    rat2j.SetAxisRange(0.0, y_max, "Y")
    rat2j.GetYaxis().SetMaxDigits(3)
    rat2j.GetYaxis().SetTitle("Entries")
    rat2j.GetYaxis().SetTitleSize(0.04)
    rat2j.GetYaxis().SetTitleOffset(1.2)
    rat2j.GetYaxis().SetLabelSize(0.05)
    rat2j.SetYTitle("Entries")
    #rat2j.SetMarkerColor(ROOT.kBlue)
    #rat2j.SetMarkerStyle(8)
    #rat2j.SetMarkerSize(1.5)
    rat2j.SetLineColor(ROOT.kBlue)    
    rat2j.SetLineWidth(2)
    rat2j.SetLabelOffset(5)
    rat2j.GetXaxis().SetRangeUser(xLow,xHigh)
        
    rat3j.Draw("same")
    rat3j.SetMarkerColor(ROOT.kRed)
    #rat3j.SetMarkerStyle(8)
    #rat3j.SetMarkerSize(1.5)
    rat3j.SetLineColor(ROOT.kRed)
    rat3j.SetLineWidth(2)
    

    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend.SetHeader("              MC,    NEntries,    SoW")
    legend.AddEntry(rat2j, "33-04/32-15 2j {}, {:.1f}, {:.1f}".format(nom,rat2j.GetEntries(),rat2j.Integral()), "l")
    legend.AddEntry(rat3j, "33-04/32-15 3j "+JType+", {:.1f}, {:.1f}".format(rat3j.GetEntries(),rat3j.Integral()), "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()


    lower.cd()
    rat = rat3j.Clone()
    rat.Divide(rat2j)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(hName.replace("qqZ","qq+ggZ"))
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    #Line = ROOT.TLine(xLow,1.,xHigh,1.)
    #Line.SetLineWidth(2)
    #Line.SetLineColor(ROOT.kBlack)
    yLow = 0.89
    yHigh = 1.11
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("3j/2j".format(VTag))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    #Line.Draw("same")
    rat.GetXaxis().SetRangeUser(xLow,xHigh)

    
    canvas.SaveAs(working_dir+hName+"_{}_{}{}_Ratios.png".format(nom,per,extra_arg))




