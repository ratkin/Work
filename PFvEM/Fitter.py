import ROOT
#include "TRandom3.h"
#include "TH3.h"
#include "TF3.h"
#include "TMath.h"
   
def myfunc3(x, par):
   xx = (x[0] - par[1])/par[2]
   yy = (x[1] - par[3])/par[4]
   zz = (x[2] - par[5])/par[6]
   rx = ROOT.TMath.Exp(-xx*xx)
   ry = ROOT.TMath.Exp(-yy*yy)
   rz = ROOT.TMath.Exp(-zz*zz)
   result = par[0]*rx*ry*rz
   return result

def myfunc3a(x,par):
   xx = (x[0]**2)*par[0] + par[1]*x[0]
   yy = (x[1]**2)*par[2] + par[3]*x[1]
   zz = (x[2]**2)*par[4] + par[5]*x[2]
   return xx+yy+zz

h3 = ROOT.TH3F("h3","test",40,-3,3,40,-3,3,40,-4,4)
r = ROOT.TRandom3(50)
for i in range(200):
    x = r.Gaus(-0.2,0.6)
    y = r.Gaus(0.3,0.5)
    z = r.Gaus(0,1)
    h3.Fill(x,y,z)
    
func3 = ROOT.TF3("func3",myfunc3,-3,3,-3,3,-4,4,7)
func3.SetParameters(h3.GetMaximum(),0,0.5,0,0.5,0,0.5)
h3.Fit(func3)
