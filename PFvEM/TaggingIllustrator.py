"""
Quick read and compare of 2 easytrees
"""
import os
import sys
import ROOT
import collections
import time
import ctypes
import math
#import argparse

sys.argv.append("-b")
print ("Opening files")
RDF = ROOT.ROOT.RDataFrame
# 10% 2jet, CRLow =

samples_by_type = {"ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"],"signalHcc" : ["*Hcc*"], "data" : ["data*"],
                   "background" : ["Zmumu*","Zee*","Znunu*","Ztautau*", "Wenu*","Wmunu*","Wtaunu*", "ttbar*", "stop*", "Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"]}

samples_by_type = {"ttbar" : ["ttbar*"]}



colours = {"c" : ROOT.kRed, 
           "b" : ROOT.kBlack,
           "l" : ROOT.kBlue}


chains_by_type  = {_type:ROOT.TChain("Nominal") for _type in samples_by_type.keys()}

dirs = ["/eos/user/r/ratkin/Reader_output/tag_33-05/CRSR_studies/VHcc/VHcc/Reader_0L_33-05_a_Resolved_D_D/fetch/data-MVATree/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/CRSR_studies/VHcc/VHcc/Reader_0L_33-05_d_Resolved_D_D/fetch/data-MVATree/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/CRSR_studies/VHcc/VHcc/Reader_0L_33-05_e_Resolved_D_D/fetch/data-MVATree/"]
        
for _type, samples in samples_by_type.iteritems():
    for sample in samples:
        for d in dirs:
            #print('adding {0} to chain {1}'.format(d + sample, _type))
            chains_by_type[_type].Add(d + sample)

#print(chains_by_type["ZJets"].GetEntries())
dataframes_by_type = {_type: RDF(chain) for _type, chain in chains_by_type.iteritems()}


working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/CRSR_studies/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)


def doDataMC(varName,binning,canName,FileName):
    print("doDataMC for "+canName)
    ROOT.gROOT.SetBatch(True)
    # 
    F = ROOT.TFile(FileName,"READ")
    histName = canName

    histograms={}
    # varName+"_"+nJet+"_"+pTV+"_"+tage+"_SR_"+DR
    canName1=canName
    F.cd()
    #print("Looping to Get histograms")

    #if "150ptv" in canName:
    #    histName=histName.replace("150ptv","150_250ptv")
    for sampl in samples_list:
        for samp in samples_list[sampl]:
            samp1 = samp
            if "bb" in samp or "cc" in samp:
                samp1 = samp[:5]+"bb_cc"
            elif "bc" in samp or "cl" in samp or "bl" in samp:
                samp1 = samp[:5]+"bc_bl_cl"
            histName1=samp+"_"+histName
            hist_tmp1 = F.Get(histName1)
            if samp1 in histograms:
                histograms[samp1].Add(hist_tmp1.Clone())
            else:
                histograms[samp1]=hist_tmp1.Clone()
    #if "150ptv" in canName1:
    #    histName=histName.replace("150_250ptv","250ptv")
    #    for sampl in samples_list:
    #        for samp in samples_list[sampl]:
    #            histName1=samp+"_"+histName
    #            hist_tmp1 = F.Get(histName1)
    #            histograms[samp].Add(hist_tmp1.Clone())
    #if "150ptv" in canName:
    #    histName=histName.replace("250ptv","150ptv")
        

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    xLow = float(binning.split(",")[1])
    xHigh = float(binning.split(",")[2])
    if varName == "MET":
        if "150_250ptv" in canName:
            xLow = 100
            xHigh = 300
        elif "250_400ptv" in canName:
            xLow = 200
            xHigh = 450
        elif "400ptv" in canName:
            xLow = 350
            xHigh = 700


    for sampl in ["ZJets","ZJetsbb_cc","ZJetsbc_bl_cl","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJets","WJetsbb_cc","WJetsbc_bl_cl","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ttbar","stop","diboson","signal","data"]:
        if sampl in histograms:
            histograms[sampl].GetXaxis().SetRangeUser(xLow,xHigh)            
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)
                for i in range(histograms[sampl].GetNbinsX()):
                    if histograms[sampl].GetBinContent(i+1)>0:
                        histograms[sampl].SetBinError(i+1,math.sqrt(histograms[sampl].GetBinContent(i+1)))

    hs = ROOT.THStack("hs","")
    for samp in ["ttbar","stop",
                 "WJets","WJetsbb_cc","WJetsbc_bl_cl","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJets","ZJetsbb_cc","ZJetsbc_bl_cl","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "diboson","signal",]:
        if samp in histograms:
            hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ZJets","ZJetsbb_cc","ZJetsbc_bl_cl","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJets","WJetsbb_cc","WJetsbc_bl_cl","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ttbar","diboson",]:
        if samp in histograms:
            hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()

    if ("mBB" in varName or "Mbb" in varName):
        getSignificance(hsig,hb)
    #if ("mva" in varName):
    #    getSignificanceMVA(hsig,hb,canName1+"_"+varName)

    
    doBlinding(hsig,hb,hd,varName)
    sigScale = 300
    if ("mBB" in varName or "Mbb" in varName):
        if "_TT_" in canName or "_TL_" in canName:
            sigScale = 200
    hsig.Scale(sigScale)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hsig.Draw("same hist")
    hd.Draw("same p")
    ROOT.gPad.RedrawAxis()

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, histName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","stop","ttbar",
                 "WJets","WJetsbb_cc","WJetsbc_bl_cl","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJets","ZJetsbb_cc","ZJetsbc_bl_cl","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",]:
        if samp in histograms:
            if "ZJets" in samp or "WJets" in samp:
                sampleName=samp.replace("Jets","")
                legend1.AddEntry(histograms[samp], sampleName+", {:.1f}".format(histograms[samp].Integral()), "f")
            else:
                legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x {}".format(sigScale),"l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)


    yLow = -0.2
    yHigh = 0.8
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("ep") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    
    yHigh2 = rat1.GetMaximum()*1.1
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    #rat1.SetAxisRange(0, yHigh2, "Y")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinError(i+1)>0.075:
            rat1.SetBinContent(i+1,(rat1.GetBinContent(i)+rat1.GetBinContent(i+2))/2)
            rat1.SetBinError(i+1,(rat1.GetBinError(i)+rat1.GetBinError(i+2))/2)
            #print("{} {}".format(rat1.GetBinContent(i+1),rat1.GetBinError(i+1)))
    
    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("hist Y+")
    

    plotName = working_dir+"DataMC/"+histName
    canvas_hs.SaveAs(plotName+"_MV2c10.png")
    canvas_hs.Close()

def compareCuts(tag,fb,bVeto):
    print("Making {} @ {}".format(tag,fb))

    Db = "TMath::Log(DL1r_pc_B1/({}*DL1r_pb_B1+(1-{})*DL1r_pu_B1))".format(fb,fb)
    if tag == "B":
        Db = "TMath::Log(DL1r_pb_B1/({}*DL1r_pc_B1+(1-{})*DL1r_pu_B1))".format(fb,fb)
    flavs={"b":"5","c":"4","l":"0"}
    histosB = {}

    ROOT.gROOT.SetBatch(True)
    for flav in flavs:
        hNameB = "Db_{}".format(flav)
        eventFilter = "(FlavB1=={})*EventWeight/bTagWeight".format(flavs[flav])
        if bVeto:
            eventFilter = "(FlavB1=={}&&nBTags==0)*EventWeight/bTagWeight".format(flavs[flav])
        chains_by_type["ttbar"].Draw(Db+">>"+hNameB+"(80,-8,8)",eventFilter)
        h = ROOT.gDirectory.Get(hNameB)
        histosB[flav] = h.Clone()



    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    xLow = -6
    xHigh = 6

    for flav in histosB:
        histosB[flav].GetXaxis().SetRangeUser(xLow,xHigh)            
        histosB[flav].SetLineColor(colours[flav])
        histosB[flav].SetLineWidth(2)
        #histosB[flav].SetFillColor(colours[flav])
        histosB[flav].Scale(1/histosB[flav].Integral())
    

    canvas = ROOT.TCanvas("TagIllus_{}_{}.png".format(tag,int(fb*100)), "TagIllus_{}_{}.png".format(tag,int(fb*100)), 900, 900)
    ROOT.gStyle.SetOptStat(0)
    histosB["b"].Draw("hist")
    histosB["b"].SetTitle("")
    histosB["b"].GetYaxis().SetMaxDigits(3)
    histosB["b"].GetYaxis().SetTitle("a.u.")
    histosB["b"].GetYaxis().SetTitleSize(0.04)
    histosB["b"].GetYaxis().SetTitleOffset(1.2)
    histosB["b"].GetYaxis().SetLabelSize(0.05)
    histosB["b"].SetYTitle("a.u.")
    y_max = max(histosB["b"].GetMaximum(), histosB["c"].GetMaximum(), histosB["l"].GetMaximum())*1.4
    histosB["b"].SetAxisRange(0.0, y_max, "Y")

    histosB["c"].Draw("same hist")
    histosB["l"].Draw("same hist")

    legend1 = ROOT.TLegend(0.705, 0.75, 0.93, 0.87)
    legend1.AddEntry(histosB["b"] ,"true b-jets","l")
    legend1.AddEntry(histosB["c"] ,"true c-jets", "l")
    legend1.AddEntry(histosB["l"] ,"true l-jets","l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()


    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    
    t.DrawLatex(0.15, 0.85, tag+"-Tagging with f={}".format(fb))
    
    
    histosB["b"].GetXaxis().SetTitle("Discriminant")
    histosB["b"].GetXaxis().SetTitleSize(0.04)
    histosB["b"].GetXaxis().SetTitleOffset(1.05)
    histosB["b"].GetXaxis().SetLabelSize(0.05)
    
    plotName = "TagIllus_{}_{}.png".format(tag,int(fb*100))
    if bVeto:
        plotName = "TagIllus_{}_{}_bVeto.png".format(tag,int(fb*100))
    canvas.SaveAs(working_dir+plotName)



#main
sys.argv.append("-b")

ROOT.gROOT.SetBatch(True)


compareCuts("B",1,False)
compareCuts("B",0,False)
compareCuts("B",0.5,False)
compareCuts("B",0.018,False)
compareCuts("C",1,False)
compareCuts("C",0,False)
compareCuts("C",0.5,False)
compareCuts("C",0.3,False)
compareCuts("C",1,True)
compareCuts("C",0,True)
compareCuts("C",0.5,True)
compareCuts("C",0.3,True)
exit()
