"""
Quick read and compare of 2 easytrees
"""
import os
import sys
import ROOT
import collections
#import argparse

sys.argv.append("-b")
print ("Opening files")
#File1="/eos/user/r/ratkin/Reader_output/tag_33-04/32-15/ZvvHbb.root"
#File2="/eos/user/r/ratkin/Reader_output/tag_33-04/pf19/ZvvHbb.root"
#File1="/eos/user/r/ratkin/Reader_output/tag_33-04/pf19/0L_33-01_e_Resolved_D/ggZvvHbb_PwPy8.root"
#File2="/eos/user/r/ratkin/Reader_output/tag_33-04/pf19/0L_33-04_e_Resolved_D/ggZvvHbb_PwPy8.root"
#nom="pf19"
#oldOrNew1 = sys.argv[3] if sys.argv[3]=="new" else ""
#oldOrNew2 = sys.argv[4] if sys.argv[4]=="new" else ""
#oldOrNew1 = "new"
#oldOrNew2 = "new"

# 10% 2jet, CRLow =
cut_CRLow_2j = "dRBB < (3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 95% 2jet, CRHigh =
cut_CRHigh_2j = "dRBB > (8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))"
#SR 2jet
cut_SR_2j = "dRBB<=(8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))&&dRBB>=(3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 10% 3jet, CRLow =
cut_CRLow_3j = "dRBB < (4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))"
# 85% 3jet, CRHigh =
cut_CRHigh_3j = "dRBB > (7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"
# SR
cut_SR_3j = "dRBB>=(4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))&&dRBB<=(7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"

samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"], 
                   "background" : ["data*", "Zmumu*","Zee*","Znunu*","Ztautau*", "Wenu*","Wmunu*","Wtaunu*", "ttbar*", "stop*", "Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"]}
chains_by_type  = {_type:ROOT.TChain("Nominal") for _type in samples_by_type.keys()}

dirs = ["/eos/user/r/ratkin/Reader_output/tag_33-05/0L_33-05_a_Resolved_D/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/0L_33-05_d_Resolved_D/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/0L_33-05_e_Resolved_D/"]
        
for _type, samples in samples_by_type.iteritems():
    for sample in samples:
        for d in dirs:
            #print('adding {0} to chain {1}'.format(d + sample, _type))
            chains_by_type[_type].Add(d + sample)

#print(chains_by_type["ZJets"].GetEntries())
dataFrames_by_type_0 = {_type: ROOT.ROOT.RDataFrame(chain) for _type, chain in chains_by_type.iteritems()}

dataframes_by_type = {bkg:get_newVar(df) for bkg, df in dataframes_by_type_0.iteritems()}


def get_newVar(df):
    return df.Define("dRHJ3","TLorentzVector L1, L2, L3; L1.SetPtEtaPhiM(pTB1,etaB1,phiB1,mB1); L2.SetPtEtaPhiM(pTB2,etaB2,phiB2,mB2); L3.SetPtEtaPhiM(pTJ3,etaJ3,phiJ3,mJ3); return abs(L3.DeltaR((L1+L2)));")

working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/ttbar_bc_studies/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)

hNames = {"mBB" : "50,0,250", "MCEventWeight" : "50,-0.05,0.05", "EventWeight" : "50,0,0.05", "LumiWeight" : "50,0,0.2", 
          "PUWeight" : "50,0,2", "JVTWeight" : "50,0.6,1.8", "TriggerSF" : "50,0.8,1.2", "BTagSF" : "50,0.5,1.5",
          "dEtaBB" : "30,0,3","dPhiBB" : "30,0,3", "dRBB" : "30,0,3", "dPhiMETdijet" : "24,2,3.2", "pTBB" : "45,50,500",
          "pTBBJ" : "45,50,500","pTB1" : "36,40,400","pTB2" : "38,20,400","pTJ3" : "28,20,300", "MindPhiMETJet" : "32,0,3.2",
          "dPhiMETMPT" : "32,0,3.2", "mBBJ" : "40,0,400", "sumPtJets" : "40,0,400", "HT" : "130,200,1500", "flavB1" : "19,-2,17",
          "flavB2" : "19,-2,17", "flavJ3" : "19,-2,17", "isSLEvent" : "2,0,2", "metSig" : "30,0,30", "nFwdJets" : "3,0,3"}

histNames = {"dEtaBB" : "30,0,3","dPhiBB" : "30,0,3", "dRBB" : "40,0,4", "dPhiMETdijetResolved" : "16,2.4,3.2", "pTBB" : "45,50,500",
             "pTB1" : "46,40,500","pTB2" : "28,20,300","MindPhiMETJet" : "28,0.4,3.2", "nSigJets" : "10,0,10","nFwdJets" : "10,0,10",
             "dPhiMETMPT" : "10,0,1", "flavB1" : "19,-2,17","flavB2" : "19,-2,17", "metSig" : "30,0,30", "MET" : "35,150,500",
             "etaB1" : "50,0,5", "etaB2" : "50,0,5", "flavJ3" : "19,-2,17", "pTJ3" : "28,20,300", "MV2c10B1" : "20,-1,1", "MV2c10B2" : "20,-1,1",
             "mBB" : "40,0,400", "mBBJ" : "40,0,400", "pTBBJ" : "45,50,500", "etaJ3" : "50,0,5",
             "bin_MV2c10B1" : "10,0,10", "bin_MV2c10B2" : "10,0,10", "bin_MV2c10J3" : "10,0,10" }
# For flavour, 15=tau, 5=b, 4=c, <4=l


hNames={}
Typ = "qq"
vTag1 = "32-15"
vTag2 = "33-05"

#f1 = ROOT.TFile.Open(File1)
#T1 = f1.Get("Nominal")
#f2 = ROOT.TFile.Open(File2)
#T2 = f2.Get("Nominal")

#2D plots
def do2DPlots(hName,hName2,cutString,sample,flav,CUTS,extras):
    
    #sys.argv.append("-b")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    
    print("Doing 2D for "+hName+":"+hName2)
    
    m_fit_func_2j10 = ROOT.TF1("fit_func_2j10", "[0]+exp([1]+[2]*x)", 150, 500)
    m_fit_func_3j10 = ROOT.TF1("fit_func_3j10", "[0]+exp([1]+[2]*x)", 150, 500)
    m_fit_func_2j95 = ROOT.TF1("fit_func_2j95", "[0]+exp([1]+[2]*x)", 150, 500)
    m_fit_func_3j85 = ROOT.TF1("fit_func_3j85", "[0]+exp([1]+[2]*x)", 150, 500)
    
    # 10% 2jet
    m_fit_func_2j10.SetParameters(3.99879e-01, 7.88279e-01, -1.02303e-02)
    # 95% 2jet
    m_fit_func_2j95.SetParameters(8.70286e-01, 1.37867e+00, -7.95322e-03)
    # 10% 3jet
    m_fit_func_3j10.SetParameters(4.20840e-01, 2.67742e-01, -8.08892e-03)
    # 85% 3jet
    m_fit_func_3j85.SetParameters(7.63283e-01, 1.33179e+00, -7.30396e-03)
    
    
    binning = histNames[hName]
    binning2 = histNames[hName2]
    histName = "h_"+hName
    drawString = hName+":"+hName2+">>"+histName+"("+binning2+","+binning+")"
    print("drawString: "+drawString)
    
    chains_by_type[sample].Draw(drawString,ROOT.TCut(cutString))
    hist_tmp1 = ROOT.gDirectory.Get(histName)
    hist_tmp1.SetDirectory(0)
    h1 = hist_tmp1.Clone()
    
    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
    
    
    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    #upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    #lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    #upper.Draw()
    #lower.Draw()
    #lower.SetGridy()
    #lower.cd().SetBottomMargin(0.3)
    #upper.cd()
    ROOT.gStyle.SetOptStat(0)
    
    #h1.Scale(1/h1.Integral())
    
    h1.Draw("colz")
    h1.SetTitle("")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle(hName)
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle(hName)
    #h1.SetMarkerColor(ROOT.kBlue)
    #h1.SetMarkerStyle(8)
    #h1.SetMarkerSize(1.5)
    #h1.SetLineColor(ROOT.kBlue)    
    #h1.SetLineWidth(2)
    #h1.SetLabelOffset(5)
    #h1.GetXaxis().SetRangeUser(xLow,xHigh)

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    
    t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
    t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
    t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
    
    
    h1.GetXaxis().SetTitle(hName2)
    h1.GetXaxis().SetTitleSize(0.04)
    h1.GetXaxis().SetTitleOffset(1.05)
    h1.GetXaxis().SetLabelSize(0.05)
    if hName == "dRBB":
        if "nJets==2" in CUTS:
            m_fit_func_2j10.Draw("same")
            m_fit_func_2j95.Draw("same")
        elif "nJets==3" in CUTS:
            m_fit_func_3j10.Draw("same")
            m_fit_func_3j85.Draw("same")
    #Line.Draw("same")
    #rat.GetXaxis().SetRangeUser(xLow,xHigh)
    
    
    canvas.SaveAs(working_dir+sample+"_"+flav+"2D_"+hName+"_"+hName2+"_"+extras+".png")
    

for hName in hNames:
    if hName != "dRBB":
        continue
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    print("Looking at "+hName)
    #cutString = "EventWeight*(nJets>1.5&&nJets<2.5&&nbJets>1.5&&nbJets<2.5&&pTV>150&&Description==\"SR\")"
    cutString = "EventWeight*(nbJets==2)"
    if not "mBB" in hName:
        cutString = "(nbJets==2)"
    cutString = "EventWeight*(nSigJets>=2&&pTB1>45&&dPhiMETdijet>2.094395&&dPhiBB<2.443461&&dPhiMETMPT<1.570796&&MindPhiMETJet>0.523599&&sumPtJets>150&&nbJets==2&&nJets==2&&MET>150&&MET<250&&Description==\"SR\")"
    cutString = "EventWeight*(nSigJets>=2&&pTB1>45&&dPhiMETdijet>2.094395&&dPhiBB<2.443461&&dPhiBB>1&&dEtaBB>1&&dPhiMETMPT<1.570796&&MindPhiMETJet>0.523599&&sumPtJets>150&&nbJets==2&&nJets==2&&MET>150&&MET<250&&Description==\"SR\")"
    binning = hNames[hName]
    varName = hName
    histName = "h01_"+hName
    histName2 = "h04_"+hName
    drawString = varName+">>"+histName+"("+binning+")"
    drawString2 = varName+">>"+histName2+"("+binning+")"
    # mBB>>h01_mBB(50,0,250)
    print("drawString: "+drawString)

    T1.Draw(drawString,ROOT.TCut(cutString))
    hist_tmp1 = ROOT.gDirectory.Get(histName)
    hist_tmp1.SetDirectory(0)
    hist1 = hist_tmp1.Clone()
    T2.Draw(drawString2,ROOT.TCut(cutString))
    hist_tmp2 = ROOT.gDirectory.Get(histName2)
    hist_tmp2.SetDirectory(0)
    hist2 = hist_tmp2.Clone()
    
    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
  
    print("looking at "+hName)

    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    ROOT.gStyle.SetOptStat(0)
    h1 = hist1.Clone()
    h2 = hist2.Clone()
    #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
    #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
    rat = h2.Clone()
    rat.Divide(h1)

        
    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.1

    h1.Draw("hist")
    h1.SetTitle("")
    h1.SetAxisRange(0.0, y_max, "Y")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    #h1.SetMarkerColor(ROOT.kBlue)
    #h1.SetMarkerStyle(8)
    #h1.SetMarkerSize(1.5)
    h1.SetLineColor(ROOT.kBlue)    
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
        
    h2.Draw("same")
    h2.SetMarkerColor(ROOT.kRed)
    #h2.SetMarkerStyle(8)
    #h2.SetMarkerSize(1.5)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)
    

    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend.SetHeader("              MC,    NEntries,    SoW")
    legend.AddEntry(h1, "{} pf19, {:.1f}, {:.1f}".format(vTag1,h1.GetEntries(),h1.Integral()), "l")
    legend.AddEntry(h2, "{} pf19, {:.1f}, {:.1f}".format(vTag2,h2.GetEntries(),h2.Integral()), "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()


    lower.cd()
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(hName)
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    #Line = ROOT.TLine(xLow,1.,xHigh,1.)
    #Line.SetLineWidth(2)
    #Line.SetLineColor(ROOT.kBlack)
    yLow = 0.79
    yHigh = 1.21
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("{}/{}".format(vTag2,vTag1))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    #Line.Draw("same")

    
    canvas.SaveAs(working_dir+hName+"_Draw3.png")


#main
sys.argv.append("-b")
print("Doing bc and bb")


numJets = "3"
cutString1 = "EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\"&&MET>250&&flavJ3==5)"
cutString2 = "EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\"&&MET>250&&flavJ3==5)"
CUTS="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb&&MET>250&&flavJ3==5"
histEx = "250ptvAll3Reg_flavJ3is5_"+numJets
dataframes_by_type_cutbb = {bkg:df.Filter(cutString1) for bkg, df in dataframes_by_type.iteritems()}
dataframes_by_type_cutbc = {bkg:df.Filter(cutString2) for bkg, df in dataframes_by_type.iteritems()}

for sample in dataframes_by_type_cutbb:
    if sample != "ttbar":
            continue
    for hName in histNames:
        if not ("bin" in hName):
            continue
        
        sys.argv.append("-b")
        ROOT.gStyle.SetPadTickX(1)
        ROOT.gStyle.SetPadTickY(1)

        print("Looking at "+hName)
        ROOT.gROOT.SetBatch(True)
        
        h = ROOT.TH1D(bkg + "-" + v_name, bkg + "-" + v_name, n_bins, x_min, x_max)
        h_model = ROOT.RDF.TH1DModel(h)
        hist = df.Histo1D(h_model, v_name, "EventWeight")
        hist = hist.Clone()
        hist.Sumw2()


        if hName == "dRBB":
            CUTS1="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb"
            CUTS2="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bc"
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\")",sample,"bb",CUTS1,"150_"+numJets)
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\")",sample,"bc",CUTS2,"150_"+numJets)
            CUTS1="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb&&MET>250"
            CUTS2="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bc&&MET>250"
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\"&&MET>250)",sample,"bb",CUTS1,"250_"+numJets)
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\"&&MET>250)",sample,"bc",CUTS2,"250_"+numJets)
        if hName == "pTB1":
            CUTS1="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb&&MET>250&&SR"
            CUTS2="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bc&&MET>250&&SR"
            do2DPlots(hName,"pTB2","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\"&&MET>250&&"+cut_SR_3j+")",sample,"bb",CUTS1,"250SR_"+numJets)
            do2DPlots(hName,"pTB2","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\"&&MET>250&&"+cut_SR_3j+")",sample,"bc",CUTS2,"250SR_"+numJets)

        binning = histNames[hName]
        varName = hName
        histName1 = "bb_"+hName
        histName2 = "bc_"+hName
        drawString1 = varName+">>"+histName1+"("+binning+")"
        drawString2 = varName+">>"+histName2+"("+binning+")"
        print("drawString: "+drawString1)
        
        chains_by_type[sample].Draw(drawString1,ROOT.TCut(cutString1))
        hist_tmp1 = ROOT.gDirectory.Get(histName1)
        hist_tmp1.SetDirectory(0)
        h1 = hist_tmp1.Clone()
        
        chains_by_type[sample].Draw(drawString2,ROOT.TCut(cutString2))
        hist_tmp2 = ROOT.gDirectory.Get(histName2)
        hist_tmp2.SetDirectory(0)
        h2 = hist_tmp2.Clone()
        
        #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
        
        print("looking at "+hName)
        
        canvas = ROOT.TCanvas(hName, hName, 900, 900)
        upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
        lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
        upper.Draw()
        lower.Draw()
        lower.SetGridy()
        lower.cd().SetBottomMargin(0.3)
        upper.cd()
        ROOT.gStyle.SetOptStat(0)

        if h1.Integral() != 0:
            h1.Scale(1/h1.Integral())
        if h2.Integral() != 0:
            h2.Scale(1/h2.Integral())
        
        #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
        #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
        rat = h2.Clone()
        rat.Divide(h1)        

        y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.3
        
        h1.Draw("hist")
        h1.SetTitle("")
        h1.SetAxisRange(0, y_max, "Y")
        h1.GetYaxis().SetMaxDigits(3)
        h1.GetYaxis().SetTitle("Entries")
        h1.GetYaxis().SetTitleSize(0.04)
        h1.GetYaxis().SetTitleOffset(1.2)
        h1.GetYaxis().SetLabelSize(0.05)
        h1.SetYTitle("Entries")
        #h1.SetMarkerColor(ROOT.kBlue)
        #h1.SetMarkerStyle(8)
        #h1.SetMarkerSize(1.5)
        h1.SetLineColor(ROOT.kBlue)    
        h1.SetLineWidth(2)
        h1.SetLabelOffset(5)
        #h1.GetXaxis().SetRangeUser(xLow,xHigh)
        
        h2.Draw("same hist")
        h2.SetMarkerColor(ROOT.kRed)
        #h2.SetMarkerStyle(8)
        #h2.SetMarkerSize(1.5)
        h2.SetLineColor(ROOT.kRed)
        h2.SetLineWidth(2)

        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.03)
        t.SetTextAlign(4)
        t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
        t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
        t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
        
        
        legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
        legend.SetHeader("              MC,    NEntries,    SoW")
        legend.AddEntry(h1, "bb, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
        legend.AddEntry(h2, "bc, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(0)
        legend.Draw()
        
        
        lower.cd()
        rat.SetTitle("")
        rat.GetXaxis().SetTitle(hName)
        rat.GetXaxis().SetTitleSize(0.09)
        rat.GetXaxis().SetTitleOffset(1.05)
        
        #Line = ROOT.TLine(xLow,1.,xHigh,1.)
        #Line.SetLineWidth(2)
        #Line.SetLineColor(ROOT.kBlack)
        yLow = 0
        yHigh = 2
        rat.SetLineWidth(2)
        rat.SetLineColor(ROOT.kBlack)
        rat.SetAxisRange(yLow, yHigh, "Y")
        rat.GetXaxis().SetLabelSize(0.09)
        rat.GetYaxis().SetLabelSize(0.07)
        rat.GetYaxis().SetTitleOffset(0.7)
        rat.GetYaxis().SetTitleSize(0.06)
        rat.GetYaxis().SetTitle("bc/bb")
        rat.GetYaxis().SetNdivisions(506)
        
        rat.Draw("")
        #Line.Draw("same")
        #rat.GetXaxis().SetRangeUser(xLow,xHigh)
        
        
        canvas.SaveAs(working_dir+sample+"_"+hName+"_"+histEx+".png")

        
'''

    0 :nJets     : nJets/I                                                *
    1 :nTaus     : nTaus/I                                                *
    2 :METPhi    : METPhi/I                                               *
    3 :flavB1    : flavB1/I                                               *
    4 :flavB2    : flavB2/I                                               *
    5 :flavJ3    : flavJ3/I                                               *
    6 :nbJets    : nbJets/I                                               *
    8 :nFwdJets  : nFwdJets/I                                             *
    9 :nSigJets  : nSigJets/I                                             *
   10 :nTrkJets  : nTrkJets/I                                             *
   13 :nBTrkJets : nBTrkJets/I                                            *
   14 :TTBarDecay : TTBarDecay/I                                          *
   17 :Njets_truth_pTjet30 : Njets_truth_pTjet30/I                        *
   18 :PassNonJetCountCuts : PassNonJetCountCuts/I                        *
   20 :HT        : HT/F                                                   *
   21 :MET       : MET/F                                                  *
   22 :MPT       : MPT/F                                                  *
   23 :mB1       : mB1/F                                                  *
   24 :mB2       : mB2/F                                                  *
   25 :mBB       : mBB/F                                                  *
   26 :mJ3       : mJ3/F                                                  *
   27 :pTV       : pTV/F                                                  *
   28 :dRBB      : dRBB/F                                                 *
   29 :mBBJ      : mBBJ/F                                                 *
   30 :pTB1      : pTB1/F                                                 *
   31 :pTB2      : pTB2/F                                                 *
   32 :pTBB      : pTBB/F                                                 *
   33 :pTJ3      : pTJ3/F                                                 *
   34 :etaB1     : etaB1/F                                                *
   35 :etaB2     : etaB2/F                                                *
   36 :etaBB     : etaBB/F                                                *
   37 :etaJ3     : etaJ3/F                                                *
   38 :pTBBJ     : pTBBJ/F                                                *
   39 :phiB1     : phiB1/F                                                *
   40 :phiB2     : phiB2/F                                                *
   41 :phiBB     : phiBB/F                                                *
   42 :phiJ3     : phiJ3/F                                                *
   43 :BTagSF    : BTagSF/F                                               *
   44 :GSCMbb    : GSCMbb/F                                               *
   45 :dEtaBB    : dEtaBB/F                                               *
   46 :dPhiBB    : dPhiBB/F                                               *
   47 :metSig    : metSig/F                                               *
   48 :softMET   : softMET/F                                              *
   49 :ActualMu  : ActualMu/F                                             *
   50 :MV2c10B1  : MV2c10B1/F                                             *
   51 :MV2c10B2  : MV2c10B2/F                                             *
   52 :OneMuMbb  : OneMuMbb/F                                             *
   53 :PUWeight  : PUWeight/F                                             *
   54 :mH_truth  : mH_truth/F                                             *
   55 :mV_truth  : mV_truth/F                                             *
   56 :AverageMu : AverageMu/F                                            *
   57 :JVTWeight : JVTWeight/F                                            *
   58 :PtRecoMbb : PtRecoMbb/F                                            *
   59 :TriggerSF : TriggerSF/F                                            *
   60 :pTH_truth : pTH_truth/F                                            *
   61 :pTV_truth : pTV_truth/F                                            *
   62 :sumPtJets : sumPtJets/F                                            *
   63 :LumiWeight : LumiWeight/F                                          *
   64 :TruthWZMbb : TruthWZMbb/F                                          *
   65 :dPhiMETMPT : dPhiMETMPT/F                                          *
   66 :etaH_truth : etaH_truth/F                                          *
   67 :etaV_truth : etaV_truth/F                                          *
   68 :phiH_truth : phiH_truth/F                                          *
   69 :phiV_truth : phiV_truth/F                                          *
   70 :EventWeight : EventWeight/F                                        *
   71 :bin_MV2c10B1 : bin_MV2c10B1/F                                      *
   72 :bin_MV2c10B2 : bin_MV2c10B2/F                                      *
   73 :bin_MV2c10J3 : bin_MV2c10J3/F                                      *
   74 :MCEventWeight : MCEventWeight/F                                    *
   75 :MindPhiMETJet : MindPhiMETJet/F                                    *
   76 :ActualMuScaled : ActualMuScaled/F                                  *
   77 :AverageMuScaled : AverageMuScaled/F                                *
   78 :dPhiMETdijetResolved : dPhiMETdijetResolved/F                      *
   79 :mva       : mva/D                                                  *
   80 :mvadiboson : mvadiboson/D                                          *
   81 :sample    : string                                                 *
   82 :Description : string                                               *
   83 :EventFlavor : string                                               *

'''

