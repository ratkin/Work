"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
import copy
import numpy as np
#import argparse

# python JetPtOptimisation_Updated_MM.py 0L 5POI doHist=1

sys.argv.append("-b")
print ("Opening files")
channel = sys.argv[1]
POIsetup = sys.argv[2]
histOpt = sys.argv[3]

samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"]}

#print(chains_by_type["ZJets"].GetEntries())
#samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
#           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
#           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
#           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
#           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}

samples = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
           "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
           "stop" : ["stop"],"diboson" : ["diboson"],"data" : ["data"],
           "signal" : ["signal"]}

colours = {"signal" : ROOT.kRed, "WJetsbb" : ROOT.kGreen+4, 
           "WJetsbc" : ROOT.kGreen+3,
           "WJetsbl" : ROOT.kGreen+2,
           "WJetscc" : ROOT.kGreen+1,
           "WJetscl" : ROOT.kGreen-6,
           "WJetsl" : ROOT.kGreen-9,
           "ZJetsbb" : ROOT.kAzure+2, 
           "ZJetsbc" : ROOT.kAzure+1, 
           "ZJetsbl" : ROOT.kAzure-2, 
           "ZJetscc" : ROOT.kAzure-4, 
           "ZJetscl" : ROOT.kAzure-8, 
           "ZJetsl" : ROOT.kAzure-9, 
           "ttbarbb" : ROOT.kOrange, 
           "ttbarbc" : ROOT.kOrange+1, 
           "ttbarbl" : ROOT.kOrange+2, 
           "ttbarcc" : ROOT.kOrange+3, 
           "ttbarcl" : ROOT.kOrange+4, 
           "ttbarl" : ROOT.kOrange+5, 
           "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

scalefactors = {"ttbar2jet" : 0.98, "ttbar3jet" : 0.93,"WHF2jet" : 1.06,"WHF3jet" : 1.15,"ZHF2jet" : 1.16,"ZHF3jet" : 1.09,}

trueBins=["FWDH",
          "PTVx0x75xGE2J","PTVx75x150xGE2J","PTVx150x250xGE2J","PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J",
          "PTVx0x75x1J","PTVx75x150x1J","PTVx150x250x1J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J",
          "PTVx0x75x0J","PTVx75x150x0J","PTVx150x250x0J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"]

trueBinsZH5POI = {
    "PTVx0x75":["PTVx0x75xGE2J","PTVx0x75x1J","PTVx0x75x0J"],
    "PTVx75x150":["PTVx75x150xGE2J","PTVx75x150x1J","PTVx75x150x0J"],
    "PTVx150x250":["PTVx150x250xGE2J","PTVx150x250x1J","PTVx150x250x0J"],
    "PTVxGT250":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"]}
trueBinsWH5POI = {
    "PTVx0x150":["PTVx0x75xGE2J","PTVx0x75x1J","PTVx0x75x0J","PTVx75x150xGE2J","PTVx75x150x1J","PTVx75x150x0J"],
    "PTVx150x250":["PTVx150x250xGE2J","PTVx150x250x1J","PTVx150x250x0J"],
    "PTVxGT250":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"]}

trueBinsZH6POI = {
    "PTVx0x75":["PTVx0x75xGE2J","PTVx0x75x1J","PTVx0x75x0J"],
    "PTVxGT75x0J":["PTVx75x150x0J","PTVx150x250x0J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"],
    "PTVxGT75x1J":["PTVx75x150x1J","PTVx150x250x1J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J"],
    "PTVxGT75xGE2J":["PTVx75x150xGE2J","PTVx150x250xGE2J","PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J"],}
trueBinsWH6POI = {
    "PTVx0x75":["PTVx0x75xGE2J","PTVx0x75x1J","PTVx0x75x0J"],
    "PTVxGT75x0J":["PTVx75x150x0J","PTVx150x250x0J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"],
    "PTVxGT75x1J":["PTVx75x150x1J","PTVx150x250x1J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J"],
    "PTVxGT75xGE2J":["PTVx75x150xGE2J","PTVx150x250xGE2J","PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J"],}


trueBinsZH10POI = {
    "PTVx0x75":["PTVx0x75x0J","PTVx0x75xGE2J","PTVx0x75x1J"],
    "PTVx75x150":["PTVx75x150x0J","PTVx75x150xGE2J","PTVx75x150x1J"],
    "PTVx150x250x0J":["PTVx150x250x0J"],
    "PTVx150x250xGE1J":["PTVx150x250xGE2J","PTVx150x250x1J"],
    "PTVxGT250x0J":["PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"],
    "PTVxGT250xGE1J":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J"]}
trueBinsWH10POI = {
    "PTVx0x75":["PTVx0x75x0J","PTVx75x150x0J","PTVx0x75xGE2J","PTVx0x75x1J"],
    "PTVx75x150":["PTVx75x150x0J","PTVx75x150xGE2J","PTVx75x150x1J"],
    "PTVx150x250x0J":["PTVx150x250x0J"],
    "PTVx150x250xGE1J":["PTVx150x250xGE2J","PTVx150x250x1J"],
    "PTVxGT250x0J":["PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"],
    "PTVxGT250xGE1J":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J"]}



recoBins=["2btag2jet_150_250ptv","2btag2jet_250_400ptv","2btag2jet_400ptv",
            "2btag3jet_150_250ptv","2btag3jet_250_400ptv","2btag3jet_400ptv",
            "2btag4jet_150_250ptv","2btag4jet_250_400ptv","2btag4jet_400ptv",]
recoJETs = ["2btag2jet","2btag3jet","2btag4jet"]
recoPTVs = ["150_250ptv","250_400ptv","400ptv"]

if channel == "1L":
    recoPTVs = ["75_150ptv","150_250ptv","250_400ptv","400ptv"]
    recoBins=["2btag2jet_75_150ptv","2btag2jet_150_250ptv","2btag2jet_250_400ptv","2btag2jet_400ptv",
              "2btag3jet_75_150ptv","2btag3jet_150_250ptv","2btag3jet_250_400ptv","2btag3jet_400ptv",
              "2btag4jet_75_150ptv","2btag4jet_150_250ptv","2btag4jet_250_400ptv","2btag4jet_400ptv",]
elif channel == "2L":
    recoPTVs = ["75_150ptv","150_250ptv","250_400ptv","400ptv"]
    recoJETs = ["2btag2jet","2btag3jet","2btag4pjet"]
    recoBins=["2btag2jet_75_150ptv","2btag2jet_150_250ptv","2btag2jet_250_400ptv","2btag2jet_400ptv",
              "2btag3jet_75_150ptv","2btag3jet_150_250ptv","2btag3jet_250_400ptv","2btag3jet_400ptv",
              "2btag4pjet_75_150ptv","2btag4pjet_150_250ptv","2btag4pjet_250_400ptv","2btag4pjet_400ptv",]

miniYields={}
for TB in trueBins:
    for RB in recoBins:
        miniYields[TB+"_"+RB]=0


do5POI = False
do6POI = False
do10POI = False
doHists = 0
if POIsetup == "5POI": do5POI = True
elif POIsetup == "6POI": do6POI = True
elif POIsetup == "10POI": do10POI = True
if histOpt == "doHist=1": doHists = 1
miniYieldsZH={}
miniYieldsWH={}
trueLenPOI=0
if do5POI:
    trueLenPOI = len(trueBinsZH5POI) + len(trueBinsWH5POI)
    for TB in trueBinsZH5POI:
        for RB in recoBins:
            miniYieldsZH[TB+"_"+RB]=0
    for TB in trueBinsWH5POI:
        for RB in recoBins:
            miniYieldsWH[TB+"_"+RB]=0
elif do6POI:
    trueLenPOI = len(trueBinsZH6POI) + len(trueBinsWH6POI)
    for TB in trueBinsZH6POI:
        for RB in recoBins:
            miniYieldsZH[TB+"_"+RB]=0
    for TB in trueBinsWH6POI:
        for RB in recoBins:
            miniYieldsWH[TB+"_"+RB]=0
elif do10POI:
    trueLenPOI = len(trueBinsZH10POI) + len(trueBinsWH10POI)
    for TB in trueBinsZH10POI:
        for RB in recoBins:
            miniYieldsZH[TB+"_"+RB]=0
    for TB in trueBinsWH10POI:
        for RB in recoBins:
            miniYieldsWH[TB+"_"+RB]=0
        

Yields = {"qqZ422" : copy.deepcopy(miniYields), "ggZ422" : copy.deepcopy(miniYields), "qqW422" : copy.deepcopy(miniYields), 
          "qqZ423" : copy.deepcopy(miniYields), "ggZ423" : copy.deepcopy(miniYields), "qqW423" : copy.deepcopy(miniYields), 
          "qqZ425" : copy.deepcopy(miniYields), "ggZ425" : copy.deepcopy(miniYields), "qqW425" : copy.deepcopy(miniYields), 
          "All422" : copy.deepcopy(miniYields), "All423" : copy.deepcopy(miniYields), "All425" : copy.deepcopy(miniYields),}

YieldsPOI = {"Z422" : copy.deepcopy(miniYieldsZH), "W422" : copy.deepcopy(miniYieldsWH), 
          "Z423" : copy.deepcopy(miniYieldsZH), "W423" : copy.deepcopy(miniYieldsWH), 
          "Z425" : copy.deepcopy(miniYieldsZH), "W425" : copy.deepcopy(miniYieldsWH), }


recoLen = len(recoBins)
trueLen = len(trueBins)
Histos2D = {"qqZ422" : ROOT.TH2F("qqZ422","qqZ422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "ggZ422" : ROOT.TH2F("ggZ422","ggZ422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqW422" : ROOT.TH2F("qqW422","qqW422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqZ423" : ROOT.TH2F("qqZ423","qqZ423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "ggZ423" : ROOT.TH2F("ggZ423","ggZ423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqW423" : ROOT.TH2F("qqW423","qqW423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqZ425" : ROOT.TH2F("qqZ425","qqZ425",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "ggZ425" : ROOT.TH2F("ggZ425","ggZ425",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqW425" : ROOT.TH2F("qqW425","qqW425",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "All422" : ROOT.TH2F("All422","All422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "All423" : ROOT.TH2F("All423","All423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "All425" : ROOT.TH2F("All425","All425",trueLen-3,0,trueLen-3,recoLen,0,recoLen),}

Histos2DPOI = {"Z422" : ROOT.TH2F("Z422","Z422",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "W422" : ROOT.TH2F("W422","W422",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "Z423" : ROOT.TH2F("Z423","Z423",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "W423" : ROOT.TH2F("W423","W423",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "Z425" : ROOT.TH2F("Z425","Z425",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "W425" : ROOT.TH2F("W425","W425",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), }

working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation_Updated/STXS/"

#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/Condor/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)
#hist_file_name = working_dir+"JetPtOptimisation.root"
#hist_file_name = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/"+"JetOpt.root"

inputFileName = "/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation_Updated/JetPtOptimisation_Updated/VH_0Lep.root"
if channel == "1L":
    inputFileName = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VHLegacy/statArea/inputs_33-05/OneLep/LimitHistograms.VH.lvbb.13TeV.mc16ade.UCL.v01.root"
elif channel == "2L":
    inputFileName = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VHLegacy/statArea/inputs_33-05/TwoLep/LimitHistograms.VH.llbb.13TeV.mc16ade.UCT.v01.root"

print("Running channel "+channel+" with input file "+inputFileName)
print("Performing: 5POI {} 6POI {} 10POI {} Hists {}".format(1 if do5POI else 0,1 if do6POI else 0,1 if do10POI else 0,1 if doHists else 0,))
hist_file = ROOT.TFile(inputFileName, "READ")


assert hist_file.IsOpen()
#assert hist_file.IsOpen()
#assert hist_file.IsOpen()


doNotCutCompare = False

def get_key(V,element):
    key = ""
    #print(V+" "+element)
    if do5POI:
        if V=="Z":
            for k,elements in trueBinsZH5POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
        elif V=="W":
            for k,elements in trueBinsWH5POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
    if do6POI:
        if V=="Z":
            for k,elements in trueBinsZH6POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
        elif V=="W":
            for k,elements in trueBinsWH6POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
    elif do10POI:
        if V=="Z":
            for k,elements in trueBinsZH10POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
        elif V=="W":
            for k,elements in trueBinsWH10POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
    return key

def printYields(yields):
    print("Printing yields")
    #recoJETs = ["2btag2jet","2btag3jet","2btag4jet"]
    #recoPTVs = ["150_250ptv","250_400ptv","400ptv"]
    fileName = "JetOptTables_Updated_5POI_"+channel+".tex"
    Caption = "5POI"
    ZHPOI = ["PTVx0x75","PTVx75x150","PTVx150x250","PTVxGT250"]
    WHPOI = ["PTVx0x150","PTVx150x250","PTVxGT250"]

    if do6POI:
        fileName = "JetOptTables_Updated_6POI_"+channel+".tex"
        Caption = "6POI"
        ZHPOI = ["PTVx0x75","PTVxGT75x0J","PTVxGT75x1J","PTVxGT75xGE2J"]
        WHPOI = ["PTVx0x75","PTVxGT75x0J","PTVxGT75x1J","PTVxGT75xGE2J"]
    elif do10POI:
        fileName = "JetOptTables_Updated_10POI_"+channel+".tex"
        Caption = "10POI"
        ZHPOI = ["PTVx0x75","PTVx75x150","PTVx150x250x0J","PTVx150x250xGE1J","PTVxGT250x0J","PTVxGT250xGE1J"]
        WHPOI = ["PTVx0x75","PTVx75x150","PTVx150x250x0J","PTVx150x250xGE1J","PTVxGT250x0J","PTVxGT250xGE1J"]

    W=open(working_dir+fileName,'w')
    W.write(r"\documentclass[aspectratio=916]{beamer}")
    W.write("\n")
    W.write(r"\mode<presentation> {")
    W.write("\n")
    W.write(r"    \usetheme{default}}")
    W.write("\n")
    W.write(r"    \usepackage{graphicx}")
    W.write("\n")
    W.write(r"    \usepackage{booktabs}")
    W.write("\n")
    W.write(r"    \usepackage{caption}")
    W.write("\n")
    W.write(r"    \usepackage{subcaption}")
    W.write("\n")
    W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
    W.write("\n")
    W.write(r"    \begin{document}")
    W.write("\n")        
    W.write(r"\end{document} % move this to end of file")
    W.write("\n")

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{prefit "+Caption+"}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.25\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{\_20J3}} & \multicolumn{1}{c|}{\textbf{\_25J3}} & \multicolumn{1}{c}{\textbf{\_30J3}} \\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in ZHPOI:
        nom=0
        val25=0
        val30=0
        for recoBin in recoBins:
            nom += yields["Z422"][samp+"_"+recoBin]
            val25 += yields["Z425"][samp+"_"+recoBin]
            val30 += yields["Z423"][samp+"_"+recoBin]
                
        if nom ==0.0:
            nom=1
        W.write(r"    {} & {:.1f} & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) \\ ".format(
            "ZHx"+samp,nom,
            val25,100*(val25/nom-1),
            val30,100*(val30/nom-1)))
        
        W.write("\n")
    for samp in WHPOI:
        nom=0
        val25=0
        val30=0
        for recoBin in recoBins:
            nom += yields["W422"][samp+"_"+recoBin]
            val25 += yields["W425"][samp+"_"+recoBin]
            val30 += yields["W423"][samp+"_"+recoBin]
                
        if nom ==0.0:
            nom=1
        W.write(r"    {} & {:.1f} & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) \\ ".format(
            "WHx"+samp,nom,
            val25,100*(val25/nom-1),
            val30,100*(val30/nom-1)))
        
        W.write("\n")

    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()

def makeHistos(HISTS,reg):
    print("makeHistos "+reg)
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    #line=canName.split("_")
        
    reg1=reg.replace("20J3","25J3")
    reg2=reg.replace("20J3","30J3")
    
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    HISTS[reg].SetLineColor(ROOT.kBlack)
    HISTS[reg].SetLineWidth(2)
    HISTS[reg1].SetLineColor(ROOT.kRed)
    HISTS[reg1].SetLineWidth(2)
    HISTS[reg2].SetLineColor(ROOT.kBlue)
    HISTS[reg2].SetLineWidth(2)
    
    if "mBB" in reg:
        HISTS[reg].Rebin(10)
        HISTS[reg].GetXaxis().SetRangeUser(0,300)
        HISTS[reg1].Rebin(10)
        HISTS[reg1].GetXaxis().SetRangeUser(0,300)
        HISTS[reg2].Rebin(10)
        HISTS[reg2].GetXaxis().SetRangeUser(0,300)
    elif "mva" in reg:
        HISTS[reg].Rebin(20)
        HISTS[reg1].Rebin(20)
        HISTS[reg2].Rebin(20)
    elif "pTV" in reg:
        xLow = 140
        xHigh = 500
        if "75x150" in reg:
            xHigh = 200
        elif "150x250" in reg:
            xHigh = 360

        HISTS[reg].Rebin(2)
        HISTS[reg].GetXaxis().SetRangeUser(xLow,xHigh)
        HISTS[reg1].Rebin(2)
        HISTS[reg1].GetXaxis().SetRangeUser(xLow,xHigh)
        HISTS[reg2].Rebin(2)
        HISTS[reg2].GetXaxis().SetRangeUser(xLow,xHigh)

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    #upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.025, 0.995, 0.995)
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(HISTS[reg].GetMaximum(),HISTS[reg1].GetMaximum(),HISTS[reg2].GetMaximum())*1.4
    
    rat423 = HISTS[reg2].Clone()
    rat4225 = HISTS[reg1].Clone()

    HISTS[reg].Draw("hist")
    HISTS[reg].SetTitle("")
    HISTS[reg].SetAxisRange(0.0, y_max, "Y")
    HISTS[reg].GetYaxis().SetMaxDigits(3)
    HISTS[reg].GetYaxis().SetTitle("Entries")
    HISTS[reg].GetYaxis().SetTitleSize(0.04)
    HISTS[reg].GetYaxis().SetTitleOffset(1.2)
    HISTS[reg].GetYaxis().SetLabelSize(0.05)
    HISTS[reg].SetYTitle("Entries")
    HISTS[reg].SetLabelOffset(5)
    HISTS[reg1].Draw("same hist")
    HISTS[reg2].Draw("same hist")

    #legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1 = ROOT.TLegend(0.55, 0.75, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(HISTS[reg], "20J3, {:.1f}, {:.1f}".format(HISTS[reg].GetEntries(),HISTS[reg].Integral()), "l")
    legend1.AddEntry(HISTS[reg1], "25J3, {:.1f}, {:.1f}".format(HISTS[reg1].GetEntries(),HISTS[reg1].Integral()), "l")
    legend1.AddEntry(HISTS[reg2], "30J3, {:.1f}, {:.1f}".format(HISTS[reg2].GetEntries(),HISTS[reg2].Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    #t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    #t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    #t.DrawLatex(0.15, 0.75, "#it{#bf{0L, == 3 Jets, 2 b-tag}}")
    #t.DrawLatex(0.15, 0.70, "#it{#bf{p_{T}^{V} > 150, SR}}")
    regName = reg.replace("_20J3","")
    t.DrawLatex(0.15, 0.85, regName+"  "+channel)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)
    
    rat423.Divide(HISTS[reg])
    rat4225.Divide(HISTS[reg])
    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.5
    #if "2jet" in canName:
    #    yHigh = 2
    rat423.SetLineWidth(2)
    #rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle(regName.split("_")[-1])
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("p") 
    rat4225.Draw("same p") 

    #HISTS[reg].GetXaxis().SetTitleSize(0.04)
    #HISTS[reg].GetXaxis().SetTitleOffset(1.04)
    #HISTS[reg].GetXaxis().SetLabelSize(0.04)
    #HISTS[reg].GetXaxis().SetTitle("p_{T}(J3) [GeV]")
    #HISTS[reg].SetAxisRange(0, 250, "X")
    #if doNjet:
    #    varName+="_AllJets"
        
    #if saveHistos:
        #canvas_hs.SaveAs(working_dir+"cutsCompare/"+sample+"/CutsCompare_"+canName+"_"+varName+".png")
    canvas_hs.SaveAs(working_dir+"CutsCompare_"+regName+"_"+POIsetup+"_"+channel+".png")
    canvas_hs.Close()

def STXS2D(histo,name):
    print("STXS2D")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat("4.2f")

    #for j in range(histo.GetNbinsY()):
    #    sumx=0
    #    for i in range(histo.GetNbinsX()):
    #        sumx+=histo.GetBinContent(i+1,j+1)
    #    histo.SetBinContent(13,j+1,sumx)
    #        #histo.GetXaxis().SetBinLabel(i+1, "x")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 600)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.005, 0.005, 0.995, 0.995)
    upper_hs.Draw()
    upper_hs.cd().SetBottomMargin(0.1)
    upper_hs.SetLeftMargin(0.15)
    upper_hs.SetTopMargin(0.01)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPalette()

    histo.Draw("colz TEXT")
    histo.SetTitle("")
    #histo.GetYaxis().SetMaxDigits(3)
    histo.GetYaxis().SetTitle("Reco Regions")
    histo.GetYaxis().SetTitleSize(0.04)
    histo.GetYaxis().SetTitleOffset(1.2)
    histo.GetYaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetTitle("STXS Regions")
    histo.SetYTitle("")
    #histo.SetLabelOffset(5)
    
    plotName = working_dir+"STXS2D_"+name+"_"+channel
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.SaveAs(plotName+".pdf")
    canvas_hs.Close()

def STXS2D_Rel(histo2,histo,name,sumx,sumy,name2):
    print("STXS2D_Rel")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat("4.2f")

    # amount of reco per truth
    histoRat=histo.Clone()
    WHLen=0
    
    nomSum=[0]*histo2.GetNbinsY()
    for i in range(histo2.GetNbinsX()):
        for j in range(histo2.GetNbinsY()):
            nomSum[j]+=histo2.GetBinContent(i+1,j+1)

    if "POI" in name:
        if "5POI" in name:
            WHLen=len(trueBinsWH5POI)
        elif "10POI" in name:
            WHLen=len(trueBinsWH10POI)
        
        nomSum=[[0]*histo2.GetNbinsY(),[0]*histo2.GetNbinsY()]
        for i in range(histo2.GetNbinsX()):
            for j in range(histo2.GetNbinsY()):
                if i<WHLen:
                    nomSum[0][j]+=histo2.GetBinContent(i+1,j+1)
                else:
                    nomSum[1][j]+=histo2.GetBinContent(i+1,j+1)

    #print(sumx)
    #print(nomSum)
    for i in range(histo.GetNbinsX()):
        for j in range(histo.GetNbinsY()):
            if "POI" in name:
                histVal=0
                nomVal=0
                if i<WHLen:
                    if sumx[0][j]>0:
                        histVal = histo.GetBinContent(i+1,j+1)/sumx[0][j]
                    if nomSum[0][j]>0:
                        nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[0][j]
                else:
                    if sumx[1][j]>0:
                        histVal = histo.GetBinContent(i+1,j+1)/sumx[1][j]
                    if nomSum[1][j]>0:
                        nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[1][j]

                histo.SetBinContent(i+1,j+1,100*histVal)
                if nomVal > 0:
                    histoRat.SetBinContent(i+1,j+1,100*(histVal-nomVal)/nomVal)
                else:
                    histoRat.SetBinContent(i+1,j+1,0)
            elif sumx[j]>0:
                histVal = histo.GetBinContent(i+1,j+1)/sumx[j]
                nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[j]
                histo.SetBinContent(i+1,j+1,100*histVal)
                if nomVal > 0:
                    histoRat.SetBinContent(i+1,j+1,100*(histVal-nomVal)/nomVal)
                else:
                    histoRat.SetBinContent(i+1,j+1,0)
    # Amount of truth per reco
    '''
    histoRat=histo.Clone()
    nomSum=[0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(histo2.GetNbinsX()):
        for j in range(histo2.GetNbinsY()):
            nomSum[i]+=histo2.GetBinContent(i+1,j+1)

    print(sumy)
    print(nomSum)
    for i in range(histo.GetNbinsX()):
        for j in range(histo.GetNbinsY()):
            if sumy[i]>0:
                histVal = histo.GetBinContent(i+1,j+1)/sumy[i]
                nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[i]
                histo.SetBinContent(i+1,j+1,100*histVal)
                if nomVal > 0:
                    histoRat.SetBinContent(i+1,j+1,100*(histVal-nomVal)/nomVal)
                else:
                    histoRat.SetBinContent(i+1,j+1,0)
    '''

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 600)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.005, 0.005, 0.995, 0.995)
    upper_hs.Draw()
    upper_hs.cd().SetBottomMargin(0.1)
    upper_hs.SetLeftMargin(0.15)
    upper_hs.SetTopMargin(0.01)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPalette()

    histo.Draw("colz TEXT")
    histo.SetTitle("")
    #histo.GetYaxis().SetMaxDigits(3)
    histo.GetYaxis().SetTitle("Reco Regions")
    histo.GetYaxis().SetTitleSize(0.04)
    histo.GetYaxis().SetTitleOffset(1.2)
    histo.GetYaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetTitle("STXS Regions")
    histo.SetYTitle("")
    #histo.SetLabelOffset(5)
    
    plotName = working_dir+"STXS2D_Rel_"+name+"_"+channel
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.SaveAs(plotName+".pdf")
    canvas_hs.Close()



    canvas_hs2 = ROOT.TCanvas("DataMC2", "DataMC2", 900, 600)
    #(x1,y1,x2,y2) 
    upper_hs2 = ROOT.TPad("upper_hs2", "upper_hs2", 0.005, 0.005, 0.995, 0.995)
    upper_hs2.Draw()
    upper_hs2.cd().SetBottomMargin(0.1)
    upper_hs2.SetLeftMargin(0.15)
    upper_hs2.SetTopMargin(0.01)
    ROOT.gStyle.SetOptStat(0)
    #vec3=np.array([51,54,57,60,62,64,66,68,70,72,0,87,89,90,91,92,93,94,95,96,97,100,205], dtype=np.int32)
    vec3=np.array([51,57,62,66,70,0,87,89,90,91,92,93,94,95,96,97,100,205], dtype=np.int32)
    ROOT.gStyle.SetPalette(18,vec3)
    #vec4=np.array([-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0,0.01,10,20,30,40,50,60,70,80,90,100,1000,1500], dtype=np.float64)
    vec4=np.array([-100,-80,-60,-40,-20,0,0.01,20,40,60,80,100,120,140,160,180,200,1000,1500], dtype=np.float64)
    histoRat.SetContour(19,vec4)
        
    

    histoRat.Draw("colz TEXT")
    histoRat.SetTitle("")
    #histoRat.GetYaxis().SetMaxDigits(3)
    histoRat.GetYaxis().SetTitle("")
    histoRat.GetYaxis().SetTitleSize(0.04)
    histoRat.GetYaxis().SetTitleOffset(1.2)
    histoRat.GetYaxis().SetLabelSize(0.03)
    histoRat.GetXaxis().SetLabelSize(0.03)
    histoRat.GetXaxis().SetTitle("")
    histoRat.SetYTitle("")
    #histoRat.SetLabelOffset(5)
    
    plotName2 = working_dir+"STXS2D_Rat_"+name+"-"+name2+"_"+channel
    canvas_hs2.SaveAs(plotName2+".png")
    canvas_hs2.SaveAs(plotName2+".pdf")
    canvas_hs2.Close()
    

# rebin,start,end
#histNames = {"nJets" : "0,0,10","GSCptJ1" : "2,40,500", "mBB" : "0,0,400"} #, "GSCMbb" : "40,0,400"}
#histNames = {"Njets_truth_pTjet30" : "0,0,10"}
#histNames = {"nSigJets" : "0,0,10"}
#histNames = {"pTB1" : "2,40,500", "nTrkJets" : "0,0,10"}
ROOT.gROOT.SetBatch(True)

# signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
#main
#sys.argv.append("-b")
samps = ["QQ2HNUNU","GG2HNUNU","QQ2HLNU","QQ2HLL","GG2HLL"]
truePTVs = ["FWDH","PTVx0x75","PTVx75x150","PTVx150x250","PTVx250x400","PTVx400x600","PTVxGT600"]
trueJETs = ["0J","1J","GE2J"]


HISTOGRAMS = {}
print("Getting Yields")
hist_file.cd()
print("\n20J3 \n")
summer=0
for samp in samps:
    for truePTV in truePTVs:
        for trueJET in trueJETs:
            for recoJET in recoJETs:
                for recoPTV in recoPTVs:
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    yieldElement = truePTV+"x"+trueJET
                    if truePTV == "FWDH":
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                        yieldElement = truePTV
                    ZKey = get_key("Z",yieldElement) # gets the POI name corresponding to yieldElement
                    WKey = get_key("W",yieldElement)

                    histName = samp+"x"+yieldName+"_SR_mva20J3"
                    hist = hist_file.Get(histName)
                    integ=0
                    try:
                        integ=hist.Clone().Integral()
                    except:
                        #print(histName+" does not exist")
                        why=0
                        #continue
                    if samp == "QQ2HLNU":
                        Yields["qqW422"][yieldName] = integ
                    elif "QQ" in samp:
                        Yields["qqZ422"][yieldName] = integ
                    elif "GG" in samp:
                        Yields["ggZ422"][yieldName] = integ
                    Yields["All422"][yieldName] += integ
                    if ZKey != "" and "HLNU" not in samp:
                        YieldsPOI["Z422"][ZKey+"_"+recoJET+"_"+recoPTV] += integ
                    if WKey != "" and "HLNU" in samp:
                        YieldsPOI["W422"][WKey+"_"+recoJET+"_"+recoPTV] += integ
                        
                    if doHists:
                        for hName in ["mBB","mva","pTV"]:
                            #if hName == "mBB":
                            #    print(samp+"x"+yieldName+"_SR_"+hName+"20J3")
                            hist1 = hist_file.Get(samp+"x"+yieldName+"_SR_"+hName+"20J3")
                            hist2a = ROOT.TH1F()
                            try:
                                hist2a = hist1.Clone()
                            except:
                                #print(histName+" does not exist")
                                continue
                                
                            #if samp+"x"+yieldName+"_SR_"+hName+"20J3" == "QQ2HLLxPTVx150x250x1J_2btag2jet_150_250ptv_SR_mva20J3":
                            #    print("RYAN single hist {}".format(hist2a.Integral()))
                            #if hName == "mBB":
                            #    print("ZKey: "+ZKey+" WKey: "+WKey)
                            if ZKey != "" and "HLNU" not in samp:
                                hKey = "ZHx"+ZKey+"_"+hName+"_20J3"
                                if hKey in HISTOGRAMS:
                                    #if hName == "mBB": print("Adding to "+"ZHx"+ZKey+"_"+hName+"_20J3")
                                    HISTOGRAMS[hKey].Add(hist2a.Clone())
                                else:
                                    #if hName == "mBB": print("Creating to "+"ZHx"+ZKey+"_"+hName+"_20J3")
                                    HISTOGRAMS[hKey]=hist2a.Clone()
                                #if hName == "mva" and ZKey == "PTVx150x250xGE1J" and recoPTV == "150_250ptv":
                                #    print(samp+"x"+yieldName+"_SR_"+hName+"20J3 {}".format(hist2a.Integral()))
                                #    summer+=hist2a.Integral()
                                #    print("   {}".format(HISTOGRAMS[hKey].Integral()))
                            if WKey != "" and "HLNU" in samp:
                                hKey = "WHx"+WKey+"_"+hName+"_20J3"
                                if hKey in HISTOGRAMS:
                                    #if hName == "mBB": print("Adding to "+"WHx"+WKey+"_"+hName+"_20J3")
                                    HISTOGRAMS[hKey].Add(hist2a.Clone())
                                else:
                                    #if hName == "mBB": print("Creating to "+"WHx"+WKey+"_"+hName+"_20J3")
                                    HISTOGRAMS[hKey]=hist2a.Clone()
                            
#print(summer)
#print("RYAN {}".format(HISTOGRAMS["ZHxPTVx150x250xGE1J_mva_20J3"].Integral() ))                            

hist_file.cd()
print("\n30J3\n")
summer = 0
for samp in samps:
    for truePTV in truePTVs:
        for trueJET in trueJETs:
            for recoJET in recoJETs:
                for recoPTV in recoPTVs:
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    yieldElement = truePTV+"x"+trueJET
                    if truePTV == "FWDH":
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                        yieldElement = truePTV
                    histName = samp+"x"+yieldName+"_SR_mva30J3"
                    hist = hist_file.Get(histName)
                    integ=0
                    try:
                        integ=hist.Clone().Integral()
                    except:
                        #print(histName+" does not exist")
                        why=0
                        #continue
                    if samp == "QQ2HLNU":
                        Yields["qqW423"][yieldName] = integ
                    elif "QQ" in samp:
                        Yields["qqZ423"][yieldName] = integ
                    elif "GG" in samp:
                        Yields["ggZ423"][yieldName] = integ
                    Yields["All423"][yieldName] += integ
                    ZKey = get_key("Z",yieldElement)
                    WKey = get_key("W",yieldElement)
                    if ZKey != "" and "HLNU" not in samp:
                        YieldsPOI["Z423"][ZKey+"_"+recoJET+"_"+recoPTV] += integ
                    if WKey != "" and "HLNU" in samp:
                        YieldsPOI["W423"][WKey+"_"+recoJET+"_"+recoPTV] += integ

                    if doHists:
                        for hName in ["mBB","mva","pTV"]:
                            hist1 = hist_file.Get(samp+"x"+yieldName+"_SR_"+hName+"30J3")
                            hist2b = ROOT.TH1F()
                            try:
                                hist2b = hist1.Clone()
                            except:
                                #print(histName+" does not exist")
                                continue
                            
                            #if samp+"x"+yieldName+"_SR_"+hName+"30J3" == "QQ2HLLxPTVx150x250x1J_2btag2jet_150_250ptv_SR_mva30J3":
                            #    print("RYAN single hist {}".format(hist2b.Integral()))
                            if ZKey != "" and "HLNU" not in samp:
                                hKey = "ZHx"+ZKey+"_"+hName+"_30J3"
                                if hKey in HISTOGRAMS:
                                    HISTOGRAMS[hKey].Add(hist2b.Clone())
                                else:
                                    HISTOGRAMS[hKey]=hist2b.Clone()
                                #if hName == "mva" and ZKey == "PTVx150x250xGE1J" and recoPTV == "150_250ptv":
                                #    print(samp+"x"+yieldName+"_SR_"+hName+"30J3 {}".format(hist2b.Integral()))
                                #    summer+=hist2b.Integral()
                                #    print("   {}".format(HISTOGRAMS[hKey].Integral()))
                            if WKey != "" and "HLNU" in samp:
                                hKey = "WHx"+WKey+"_"+hName+"_30J3"
                                if hKey in HISTOGRAMS:
                                    HISTOGRAMS[hKey].Add(hist2b.Clone())
                                else:
                                    HISTOGRAMS[hKey]=hist2b.Clone()
#print(summer)
#print("RYAN {}".format(HISTOGRAMS["ZHxPTVx150x250xGE1J_mva_30J3"].Integral() ))

hist_file.cd()
print("\n25J3\n")
summer = 0
for samp in samps:
    for truePTV in truePTVs:
        for trueJET in trueJETs:
            for recoJET in recoJETs:
                for recoPTV in recoPTVs:
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    yieldElement = truePTV+"x"+trueJET
                    if truePTV == "FWDH":
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                        yieldElement = truePTV
                    histName = samp+"x"+yieldName+"_SR_mva25J3"
                    hist = hist_file.Get(histName)
                    integ=0
                    try:
                        integ=hist.Clone().Integral()
                    except:
                        #print(histName+" does not exist")
                        why=0
                        #continue
                    if samp == "QQ2HLNU":
                        Yields["qqW425"][yieldName] = integ
                    elif "QQ" in samp:
                        Yields["qqZ425"][yieldName] = integ
                    elif "GG" in samp:
                        Yields["ggZ425"][yieldName] = integ
                    Yields["All425"][yieldName] += integ
                    ZKey = get_key("Z",yieldElement)
                    WKey = get_key("W",yieldElement)
                    if ZKey != "" and "HLNU" not in samp:
                        YieldsPOI["Z425"][ZKey+"_"+recoJET+"_"+recoPTV] += integ
                    if WKey != "" and "HLNU" in samp:
                        YieldsPOI["W425"][WKey+"_"+recoJET+"_"+recoPTV] += integ

                    if doHists:
                        for hName in ["mBB","mva","pTV"]:
                            hist1 = hist_file.Get(samp+"x"+yieldName+"_SR_"+hName+"25J3")
                            hist2c = ROOT.TH1F()
                            try:
                                hist2c = hist1.Clone()
                            except:
                                #print(histName+" does not exist")
                                continue
                            
                            #if samp+"x"+yieldName+"_SR_"+hName+"25J3" == "QQ2HLLxPTVx150x250x1J_2btag2jet_150_250ptv_SR_mva25J3":
                            #    print("RYAN single hist {}".format(hist2c.Integral()))
                            if ZKey != "" and "HLNU" not in samp:
                                hKey = "ZHx"+ZKey+"_"+hName+"_25J3"
                                if hKey in HISTOGRAMS:
                                    HISTOGRAMS[hKey].Add(hist2c.Clone())
                                else:
                                    HISTOGRAMS[hKey]=hist2c.Clone()
                                #if hName == "mva" and ZKey == "PTVx150x250xGE1J" and recoPTV == "150_250ptv":
                                #    print(samp+"x"+yieldName+"_SR_"+hName+"25J3 {}".format(hist2c.Integral()))
                                #    summer+=hist2c.Integral()
                                #    print("   {}".format(HISTOGRAMS[hKey].Integral()))
                            if WKey != "" and "HLNU" in samp:
                                hKey = "WHx"+WKey+"_"+hName+"_25J3"
                                if hKey in HISTOGRAMS:
                                    HISTOGRAMS[hKey].Add(hist2c.Clone())
                                else:
                                    HISTOGRAMS[hKey]=hist2c.Clone()

#print(summer)
#print("RYAN {}".format(HISTOGRAMS["ZHxPTVx150x250xGE1J_mva_20J3"].Integral() ))
#print("RYAN {}".format(HISTOGRAMS["ZHxPTVx150x250xGE1J_mva_25J3"].Integral() ))
#print("RYAN {}".format(HISTOGRAMS["ZHxPTVx150x250xGE1J_mva_30J3"].Integral() ))
#exit()


printYields(YieldsPOI)

if doHists:
    for reg in HISTOGRAMS:
        if "20J3" not in reg:
            continue
        makeHistos(HISTOGRAMS,reg)

print("Histograms complete")
exit()
#main
histoNames = ["qqZ422","ggZ422","qqW422", 
              "qqZ423","ggZ423","qqW423", 
              "qqZ425","ggZ425","qqW425", 
              "All422","All423","All425",]

histoNames = ["Z422","W422", 
              "Z423","W423", 
              "Z425","W425", 
              "All422","All423","All425",]

truePTVLabels = {"FWDH":"FWDH","PTVx0x75":"0<pTV<75","PTVx75x150":"75<pTV<150",
                 "PTVx150x250":"150<pTV<250","PTVx250x400":"250<pTV<400",
                 "PTVx400x600":"400<pTV<600","PTVxGT600":"pTV>600","PTVxGT250":"pTV>250"}
trueJETLabels = {"0J":"2jet","1J":"3jets","GE2J":"4+jets","GE1J":"3+jets"}
recoPTVLabels = {"75_150ptv":"75<pTV<150","150_250ptv":"150<pTV<250","250_400ptv":"250<pTV<400","400ptv":"pTV>400"}
recoJETLabels = {"2btag2jet":"2jets","2btag3jet":"3jets","2btag4jet":"4jets","2btag4pjet":"4+jets"}

# for all truth bins
for typ in histoNames:
    #if "All" not in typ:
    #    continue
    print("\n Looking at {}\n".format(typ))
    i=0
    sumx=[0]*recoLen # sum per reco
    sumy=[0]*trueLen # sum per truth
    #print(len(sumx))
    #print(len(sumy))
    typ3 = typ
    if "Z" in typ:
        typ3 = typ.replace("Z","qqZ")
    elif "W" in typ:
        typ3 = typ.replace("W","qqW")

    typ1 = ""
    typ2 = ""
    if "Z" in typ:
        typ1 = typ.replace("Z","qqZ")
        typ2 = typ.replace("Z","ggZ")
    elif "W" in typ:
        typ1 = typ.replace("W","qqW")
    elif "All" in typ:
        typ1 = typ
    
    for truePTV in truePTVs:
        breaker=0
        if truePTV=="PTVxGT600":
            continue
        for trueJET in trueJETs:
            i+=1
            j=0
            trueLabel = trueJETLabels[trueJET]+" "+truePTVLabels[truePTV]
            if truePTV=="PTVx400x600":
                trueLabel = trueJETLabels[trueJET]+" pTV>400"
            if breaker == 1:
                i-=1
                continue
            if "FWDH" in truePTV:
                trueLabel = truePTVLabels[truePTV]
                breaker = 1
            Histos2D[typ3].GetXaxis().SetBinLabel(i, trueLabel) #truePTV+"x"+trueJET)
            #Histos2D[typ3].GetXaxis().SetBinLabel(i, "x")
            for recoPTV in recoPTVs:
                for recoJET in recoJETs:
                    j+=1
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    if truePTV=="PTVx400x600":
                        yieldName2 = "PTVxGT600x"+trueJET+"_"+recoJET+"_"+recoPTV
                        
                    if "FWDH" in truePTV:
                         yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                    #print("  {},{} {}".format(i,j,yieldName))
                    Yield = Yields[typ1][yieldName]
                    if truePTV=="PTVx400x600":
                        Yield += Yields[typ1][yieldName2]
                        if "Z" in typ:
                            Yield += Yields[typ2][yieldName2]
                    if "Z" in typ:
                        Yield += Yields[typ2][yieldName]
                    
                    Histos2D[typ3].SetBinContent(i,j,Yield)
                    sumy[i-1]+=Yield
                    sumx[j-1]+=Yield
                    if i==1:
                        Histos2D[typ3].GetYaxis().SetBinLabel(j, recoJETLabels[recoJET]+" "+recoPTVLabels[recoPTV]) #recoJET+"_"+recoPTV)

            #Histos2D[typ3].SetBinContent(i,10,sumy)
            
    STXS2D(Histos2D[typ3].Clone(),typ)
    if "qqZ" in typ3:
        typ1="qqZ422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="qqZ422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="qqZ423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
                
    elif "ggZ" in typ3:
        typ1="ggZ422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="ggZ422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="ggZ423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
    elif "qqW" in typ3:
        typ1="qqW422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="qqW422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="qqW423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
    elif "All" in typ3:
        typ1="All422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="All422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="All423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="All423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        elif "425_FSR" in typ:
            typ1="All422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="All425"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="All425_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)

if (not do5POI) and (not do10POI) and (not do6POI):
    print("Not doing any POI plots")
    exit()

# for POI bins
for typ in histoNames:
    if "All" in typ or "W" in typ:
        continue
    print("\n Looking at {}\n".format(typ))
    sumxWH=[0]*recoLen # sum per reco
    sumxZH=[0]*recoLen # sum per reco
    sumy=[0]*trueLenPOI # sum per truth
    #print(len(sumx))
    #print(len(sumy))
    typ3 = typ.replace("Z","W")
    
    trueLabels = []
    plotName = typ.replace("Z","")

    if do5POI:
        plotName+="_5POI"
        #if "Z" in typ:
        trueLabels = ["WHxPTVx0x150","WHxPTVx150x250","WHxPTVxGT250",
                      "ZHxPTVx0x75","ZHxPTVx75x150","ZHxPTVx150x250","ZHxPTVxGT250"]
        #elif "W" in typ:
        #    trueLabels = list(trueBinsWH5POI.keys())
    elif do6POI:
        plotName+="_6POI"
        #if "Z" in typ:
        trueLabels = ["WHxPTVxGT75x0J","WHxPTVxGT75x1J","WHxPTVxGT75xGE2J",
                      "ZHxPTVxGT75x0J","ZHxPTVxGT75x1J","ZHxPTVxGT75xGE2J",]
    elif do10POI:
        plotName+="_10POI"
        #if "Z" in typ:
        trueLabels = ["WHxPTVx0x75","WHxPTVx75x150",
                      "WHxPTVx150x250x0J","WHxPTVx150x250xGE1J",
                      "WHxPTVxGT250x0J","WHxPTVxGT250xGE1J",
                      "ZHxPTVx0x75","ZHxPTVx75x150",
                      "ZHxPTVx150x250x0J","ZHxPTVx150x250xGE1J",
                      "ZHxPTVxGT250x0J","ZHxPTVxGT250xGE1J"]
        #elif "W" in typ:
        #    trueLabels = list(trueBinsWH10POI.keys())

    i=0
    for trueLabel in trueLabels:
        i+=1
        j=0
        Histos2DPOI[typ].GetXaxis().SetBinLabel(i, trueLabel) #truePTV+"x"+trueJET)
        #Histos2D[typ].GetXaxis().SetBinLabel(i, "x")
        #print(" {} {}".format(i,trueLabel))
        for recoPTV in recoPTVs:
            for recoJET in recoJETs:
                j+=1
                yieldName = trueLabel+"_"+recoJET+"_"+recoPTV
                Yield = 0
                if "WH" in trueLabel:
                    yieldName = yieldName.replace("WHx","")
                    Yield = YieldsPOI[typ3][yieldName]
                    sumxWH[j-1]+=Yield
                elif "ZH" in trueLabel:
                    yieldName = yieldName.replace("ZHx","")
                    Yield = YieldsPOI[typ][yieldName]
                    sumxZH[j-1]+=Yield
                #print("  {},{} {}".format(i,j,yieldName))
                #print(" {} {}".format(yieldName,Yield))
                #print(sumxZH)
                Histos2DPOI[typ].SetBinContent(i,j,Yield)
                sumy[i-1]+=Yield
                if i==1:
                    Histos2DPOI[typ].GetYaxis().SetBinLabel(j, recoJETLabels[recoJET]+" "+recoPTVLabels[recoPTV]) #recoJET+"_"+recoPTV)

            #Histos2D[typ].SetBinContent(i,10,sumy)
    sumx = []
    sumx.append(sumxWH)
    sumx.append(sumxZH)
    #print(sumx)
    STXS2D(Histos2DPOI[typ].Clone(),plotName)
    if "Z" in typ:
        typ1="Z422"
        plotName2=typ1.replace("Z","")
        STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),plotName,sumx,sumy,plotName2)
        if "423_FSR" in typ:
            typ1="qqZ422_FSR"
            STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),typ,sumx,sumy,typ1)
            typ1="qqZ423"
            STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),typ,sumx,sumy,typ1)
                
                    
print("\n\n\n\nALL COMPLETE")
