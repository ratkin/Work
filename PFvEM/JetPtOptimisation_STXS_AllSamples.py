"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
import copy
import numpy as np
#import argparse

# python JetPtOptimisation_STXS_AllSamples.py 0L

sys.argv.append("-b")
print ("Opening files")
channel = sys.argv[1]

samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"]}

#print(chains_by_type["ZJets"].GetEntries())
#samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
#           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
#           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
#           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
#           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}

samples = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
           "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
           "stop" : ["stop"],"diboson" : ["diboson"],"data" : ["data"],
           "signal" : ["signal"]}

colours = {"signal" : ROOT.kRed, "WJetsbb" : ROOT.kGreen+4, 
           "WJetsbc" : ROOT.kGreen+3,
           "WJetsbl" : ROOT.kGreen+2,
           "WJetscc" : ROOT.kGreen+1,
           "WJetscl" : ROOT.kGreen-6,
           "WJetsl" : ROOT.kGreen-9,
           "ZJetsbb" : ROOT.kAzure+2, 
           "ZJetsbc" : ROOT.kAzure+1, 
           "ZJetsbl" : ROOT.kAzure-2, 
           "ZJetscc" : ROOT.kAzure-4, 
           "ZJetscl" : ROOT.kAzure-8, 
           "ZJetsl" : ROOT.kAzure-9, 
           "ttbarbb" : ROOT.kOrange, 
           "ttbarbc" : ROOT.kOrange+1, 
           "ttbarbl" : ROOT.kOrange+2, 
           "ttbarcc" : ROOT.kOrange+3, 
           "ttbarcl" : ROOT.kOrange+4, 
           "ttbarl" : ROOT.kOrange+5, 
           "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

scalefactors = {"ttbar2jet" : 0.98, "ttbar3jet" : 0.93,"WHF2jet" : 1.06,"WHF3jet" : 1.15,"ZHF2jet" : 1.16,"ZHF3jet" : 1.09,}

trueBins=["FWDH",
          "PTVx0x75xGE2J","PTVx75x150xGE2J","PTVx150x250xGE2J","PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J",
          "PTVx0x75x1J","PTVx75x150x1J","PTVx150x250x1J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J",
          "PTVx0x75x0J","PTVx75x150x0J","PTVx150x250x0J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"]

trueBinsZH5POI = {
    "PTVx0x75":["PTVx0x75xGE2J","PTVx0x75x1J","PTVx0x75x0J"],
    "PTVx75x150":["PTVx75x150xGE2J","PTVx75x150x1J","PTVx75x150x0J"],
    "PTVx150x250":["PTVx150x250xGE2J","PTVx150x250x1J","PTVx150x250x0J"],
    "PTVxGT250":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"]}
trueBinsWH5POI = {
    "PTVx0x150":["PTVx0x75xGE2J","PTVx0x75x1J","PTVx0x75x0J","PTVx75x150xGE2J","PTVx75x150x1J","PTVx75x150x0J"],
    "PTVx150x250":["PTVx150x250xGE2J","PTVx150x250x1J","PTVx150x250x0J"],
    "PTVxGT250":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J","PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"]}

trueBinsZH10POI = {
    "PTVx0x75":["PTVx0x75x0J","PTVx0x75xGE2J","PTVx0x75x1J"],
    "PTVx75x150":["PTVx75x150x0J","PTVx75x150xGE2J","PTVx75x150x1J"],
    "PTVx150x250x0J":["PTVx150x250x0J"],
    "PTVx150x250xGE1J":["PTVx150x250xGE2J","PTVx150x250x1J"],
    "PTVxGT250x0J":["PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"],
    "PTVxGT250xGE1J":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J"]}
trueBinsWH10POI = {
    "PTVx0x75":["PTVx0x75x0J","PTVx75x150x0J","PTVx0x75xGE2J","PTVx0x75x1J"],
    "PTVx75x150":["PTVx75x150x0J","PTVx75x150xGE2J","PTVx75x150x1J"],
    "PTVx150x250x0J":["PTVx150x250x0J"],
    "PTVx150x250xGE1J":["PTVx150x250xGE2J","PTVx150x250x1J"],
    "PTVxGT250x0J":["PTVx250x400x0J","PTVx400x600x0J","PTVxGT600x0J"],
    "PTVxGT250xGE1J":["PTVx250x400xGE2J","PTVx400x600xGE2J","PTVxGT600xGE2J","PTVx250x400x1J","PTVx400x600x1J","PTVxGT600x1J"]}



recoBins=["2btag2jet_150_250ptv","2btag2jet_250_400ptv","2btag2jet_400ptv",
            "2btag3jet_150_250ptv","2btag3jet_250_400ptv","2btag3jet_400ptv",
            "2btag4jet_150_250ptv","2btag4jet_250_400ptv","2btag4jet_400ptv",]

if channel == "1L":
    recoBins=["2btag2jet_75_150ptv","2btag2jet_150_250ptv","2btag2jet_250_400ptv","2btag2jet_400ptv",
              "2btag3jet_75_150ptv","2btag3jet_150_250ptv","2btag3jet_250_400ptv","2btag3jet_400ptv",
              "2btag4jet_75_150ptv","2btag4jet_150_250ptv","2btag4jet_250_400ptv","2btag4jet_400ptv",]
elif channel == "2L":
    recoBins=["2btag2jet_75_150ptv","2btag2jet_150_250ptv","2btag2jet_250_400ptv","2btag2jet_400ptv",
              "2btag3jet_75_150ptv","2btag3jet_150_250ptv","2btag3jet_250_400ptv","2btag3jet_400ptv",
              "2btag4pjet_75_150ptv","2btag4pjet_150_250ptv","2btag4pjet_250_400ptv","2btag4pjet_400ptv",]

miniYields={}
for TB in trueBins:
    for RB in recoBins:
        miniYields[TB+"_"+RB]=0

do5POI = False
do10POI = True
miniYieldsZH={}
miniYieldsWH={}
trueLenPOI=0
if do5POI:
    trueLenPOI = len(trueBinsZH5POI) + len(trueBinsWH5POI)
    for TB in trueBinsZH5POI:
        for RB in recoBins:
            miniYieldsZH[TB+"_"+RB]=0
    for TB in trueBinsWH5POI:
        for RB in recoBins:
            miniYieldsWH[TB+"_"+RB]=0
elif do10POI:
    trueLenPOI = len(trueBinsZH10POI) + len(trueBinsWH10POI)
    for TB in trueBinsZH10POI:
        for RB in recoBins:
            miniYieldsZH[TB+"_"+RB]=0
    for TB in trueBinsWH10POI:
        for RB in recoBins:
            miniYieldsWH[TB+"_"+RB]=0
        

Yields = {"qqZ422" : copy.deepcopy(miniYields), "ggZ422" : copy.deepcopy(miniYields), "qqW422" : copy.deepcopy(miniYields), 
          "qqZ423" : copy.deepcopy(miniYields), "ggZ423" : copy.deepcopy(miniYields), "qqW423" : copy.deepcopy(miniYields), 
          "qqZ433" : copy.deepcopy(miniYields), "ggZ433" : copy.deepcopy(miniYields), "qqW433" : copy.deepcopy(miniYields), 
          "All422" : copy.deepcopy(miniYields), "All423" : copy.deepcopy(miniYields), "All433" : copy.deepcopy(miniYields),}

YieldsPOI = {"Z422" : copy.deepcopy(miniYieldsZH), "W422" : copy.deepcopy(miniYieldsWH), 
          "Z423" : copy.deepcopy(miniYieldsZH), "W423" : copy.deepcopy(miniYieldsWH), 
          "Z433" : copy.deepcopy(miniYieldsZH), "W433" : copy.deepcopy(miniYieldsWH), }


recoLen = len(recoBins)
trueLen = len(trueBins)
Histos2D = {"qqZ422" : ROOT.TH2F("qqZ422","qqZ422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "ggZ422" : ROOT.TH2F("ggZ422","ggZ422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqW422" : ROOT.TH2F("qqW422","qqW422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqZ423" : ROOT.TH2F("qqZ423","qqZ423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "ggZ423" : ROOT.TH2F("ggZ423","ggZ423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqW423" : ROOT.TH2F("qqW423","qqW423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqZ433" : ROOT.TH2F("qqZ433","qqZ433",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "ggZ433" : ROOT.TH2F("ggZ433","ggZ433",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "qqW433" : ROOT.TH2F("qqW433","qqW433",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "All422" : ROOT.TH2F("All422","All422",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "All423" : ROOT.TH2F("All423","All423",trueLen-3,0,trueLen-3,recoLen,0,recoLen), 
            "All433" : ROOT.TH2F("All433","All433",trueLen-3,0,trueLen-3,recoLen,0,recoLen),}

Histos2DPOI = {"Z422" : ROOT.TH2F("Z422","Z422",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "W422" : ROOT.TH2F("W422","W422",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "Z423" : ROOT.TH2F("Z423","Z423",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "W423" : ROOT.TH2F("W423","W423",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "Z433" : ROOT.TH2F("Z433","Z433",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), 
               "W433" : ROOT.TH2F("W433","W433",trueLenPOI,0,trueLenPOI,recoLen,0,recoLen), }

working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/plots/STXS/AllSamples/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/Condor/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)
#hist_file_name = working_dir+"JetPtOptimisation.root"
#hist_file_name = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/"+"JetOpt.root"

hist_file422 = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3_STXS_AllChannels/JetPtOptimisation45J120J220J3_STXS_AllChannels/hadds/"+channel+"/VH.root", "READ")
hist_file423 = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J230J3_STXS_AllChannels/JetPtOptimisation45J120J230J3_STXS_AllChannels/hadds/"+channel+"/VH.root", "READ")
hist_file433 = ROOT.TFile("/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J130J230J3_STXS_AllChannels/JetPtOptimisation45J130J230J3_STXS_AllChannels/hadds/"+channel+"/VH.root", "READ")


assert hist_file422.IsOpen()
assert hist_file423.IsOpen()
assert hist_file433.IsOpen()


doNotCutCompare = False

def get_key(V,element):
    key = ""
    #print(V+" "+element)
    if do5POI:
        if V=="Z":
            for k,elements in trueBinsZH5POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
        elif V=="W":
            for k,elements in trueBinsWH5POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
    elif do10POI:
        if V=="Z":
            for k,elements in trueBinsZH10POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
        elif V=="W":
            for k,elements in trueBinsWH10POI.iteritems():
                for e in elements:
                    if e==element:
                        key = k
                        break
    return key

def STXS2D(histo,name):
    print("STXS2D")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat("4.2f")

    #for j in range(histo.GetNbinsY()):
    #    sumx=0
    #    for i in range(histo.GetNbinsX()):
    #        sumx+=histo.GetBinContent(i+1,j+1)
    #    histo.SetBinContent(13,j+1,sumx)
    #        #histo.GetXaxis().SetBinLabel(i+1, "x")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 600)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.005, 0.005, 0.995, 0.995)
    upper_hs.Draw()
    upper_hs.cd().SetBottomMargin(0.1)
    upper_hs.SetLeftMargin(0.15)
    upper_hs.SetTopMargin(0.01)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPalette()

    histo.Draw("colz TEXT")
    histo.SetTitle("")
    #histo.GetYaxis().SetMaxDigits(3)
    histo.GetYaxis().SetTitle("")
    histo.GetYaxis().SetTitleSize(0.04)
    histo.GetYaxis().SetTitleOffset(1.2)
    histo.GetYaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetTitle("")
    histo.SetYTitle("")
    #histo.SetLabelOffset(5)
    
    plotName = working_dir+"STXS2D_"+name+"_"+channel
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.SaveAs(plotName+".pdf")
    canvas_hs.Close()

def STXS2D_Rel(histo2,histo,name,sumx,sumy,name2):
    print("STXS2D_Rel")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat("4.2f")

    # amount of reco per truth
    histoRat=histo.Clone()
    WHLen=0
    
    nomSum=[0]*histo2.GetNbinsY()
    for i in range(histo2.GetNbinsX()):
        for j in range(histo2.GetNbinsY()):
            nomSum[j]+=histo2.GetBinContent(i+1,j+1)

    if "POI" in name:
        if "5POI" in name:
            WHLen=len(trueBinsWH5POI)
        elif "10POI" in name:
            WHLen=len(trueBinsWH10POI)
        
        nomSum=[[0]*histo2.GetNbinsY(),[0]*histo2.GetNbinsY()]
        for i in range(histo2.GetNbinsX()):
            for j in range(histo2.GetNbinsY()):
                if i<WHLen:
                    nomSum[0][j]+=histo2.GetBinContent(i+1,j+1)
                else:
                    nomSum[1][j]+=histo2.GetBinContent(i+1,j+1)

    #print(sumx)
    #print(nomSum)
    for i in range(histo.GetNbinsX()):
        for j in range(histo.GetNbinsY()):
            if "POI" in name:
                histVal=0
                nomVal=0
                if i<WHLen:
                    if sumx[0][j]>0:
                        histVal = histo.GetBinContent(i+1,j+1)/sumx[0][j]
                    if nomSum[0][j]>0:
                        nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[0][j]
                else:
                    if sumx[1][j]>0:
                        histVal = histo.GetBinContent(i+1,j+1)/sumx[1][j]
                    if nomSum[1][j]>0:
                        nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[1][j]

                histo.SetBinContent(i+1,j+1,100*histVal)
                if nomVal > 0:
                    histoRat.SetBinContent(i+1,j+1,100*(histVal-nomVal)/nomVal)
                else:
                    histoRat.SetBinContent(i+1,j+1,0)
            elif sumx[j]>0:
                histVal = histo.GetBinContent(i+1,j+1)/sumx[j]
                nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[j]
                histo.SetBinContent(i+1,j+1,100*histVal)
                if nomVal > 0:
                    histoRat.SetBinContent(i+1,j+1,100*(histVal-nomVal)/nomVal)
                else:
                    histoRat.SetBinContent(i+1,j+1,0)
    # Amount of truth per reco
    '''
    histoRat=histo.Clone()
    nomSum=[0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(histo2.GetNbinsX()):
        for j in range(histo2.GetNbinsY()):
            nomSum[i]+=histo2.GetBinContent(i+1,j+1)

    print(sumy)
    print(nomSum)
    for i in range(histo.GetNbinsX()):
        for j in range(histo.GetNbinsY()):
            if sumy[i]>0:
                histVal = histo.GetBinContent(i+1,j+1)/sumy[i]
                nomVal = histo2.GetBinContent(i+1,j+1)/nomSum[i]
                histo.SetBinContent(i+1,j+1,100*histVal)
                if nomVal > 0:
                    histoRat.SetBinContent(i+1,j+1,100*(histVal-nomVal)/nomVal)
                else:
                    histoRat.SetBinContent(i+1,j+1,0)
    '''

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 600)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.005, 0.005, 0.995, 0.995)
    upper_hs.Draw()
    upper_hs.cd().SetBottomMargin(0.1)
    upper_hs.SetLeftMargin(0.15)
    upper_hs.SetTopMargin(0.01)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPalette()

    histo.Draw("colz TEXT")
    histo.SetTitle("")
    #histo.GetYaxis().SetMaxDigits(3)
    histo.GetYaxis().SetTitle("")
    histo.GetYaxis().SetTitleSize(0.04)
    histo.GetYaxis().SetTitleOffset(1.2)
    histo.GetYaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetTitle("")
    histo.SetYTitle("")
    #histo.SetLabelOffset(5)
    
    plotName = working_dir+"STXS2D_Rel_"+name+"_"+channel
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.SaveAs(plotName+".pdf")
    canvas_hs.Close()



    canvas_hs2 = ROOT.TCanvas("DataMC2", "DataMC2", 900, 600)
    #(x1,y1,x2,y2) 
    upper_hs2 = ROOT.TPad("upper_hs2", "upper_hs2", 0.005, 0.005, 0.995, 0.995)
    upper_hs2.Draw()
    upper_hs2.cd().SetBottomMargin(0.1)
    upper_hs2.SetLeftMargin(0.15)
    upper_hs2.SetTopMargin(0.01)
    ROOT.gStyle.SetOptStat(0)
    #vec3=np.array([51,54,57,60,62,64,66,68,70,72,0,87,89,90,91,92,93,94,95,96,97,100,205], dtype=np.int32)
    vec3=np.array([51,57,62,66,70,0,87,89,90,91,92,93,94,95,96,97,100,205], dtype=np.int32)
    ROOT.gStyle.SetPalette(18,vec3)
    #vec4=np.array([-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0,0.01,10,20,30,40,50,60,70,80,90,100,1000,1500], dtype=np.float64)
    vec4=np.array([-100,-80,-60,-40,-20,0,0.01,20,40,60,80,100,120,140,160,180,200,1000,1500], dtype=np.float64)
    histoRat.SetContour(19,vec4)
        
    

    histoRat.Draw("colz TEXT")
    histoRat.SetTitle("")
    #histoRat.GetYaxis().SetMaxDigits(3)
    histoRat.GetYaxis().SetTitle("")
    histoRat.GetYaxis().SetTitleSize(0.04)
    histoRat.GetYaxis().SetTitleOffset(1.2)
    histoRat.GetYaxis().SetLabelSize(0.03)
    histoRat.GetXaxis().SetLabelSize(0.03)
    histoRat.GetXaxis().SetTitle("")
    histoRat.SetYTitle("")
    #histoRat.SetLabelOffset(5)
    
    plotName2 = working_dir+"STXS2D_Rat_"+name+"-"+name2+"_"+channel
    canvas_hs2.SaveAs(plotName2+".png")
    canvas_hs2.SaveAs(plotName2+".pdf")
    canvas_hs2.Close()
    

# rebin,start,end
#histNames = {"nJets" : "0,0,10","GSCptJ1" : "2,40,500", "mBB" : "0,0,400"} #, "GSCMbb" : "40,0,400"}
#histNames = {"Njets_truth_pTjet30" : "0,0,10"}
#histNames = {"nSigJets" : "0,0,10"}
#histNames = {"pTB1" : "2,40,500", "nTrkJets" : "0,0,10"}
ROOT.gROOT.SetBatch(True)

# signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
#main
#sys.argv.append("-b")
samps = ["QQ2HNUNU","GG2HNUNU","QQ2HLNU","QQ2HLL","GG2HLL"]
truePTVs = ["FWDH","PTVx0x75","PTVx75x150","PTVx150x250","PTVx250x400","PTVx400x600","PTVxGT600"]
trueJETs = ["0J","1J","GE2J"]
recoJETs = ["2btag2jet","2btag3jet","2btag4jet"]
recoPTVs = ["150_250ptv","250_400ptv","400ptv"]

if channel == "1L":
    recoPTVs = ["75_150ptv","150_250ptv","250_400ptv","400ptv"]
elif channel == "2L":
    recoPTVs = ["75_150ptv","150_250ptv","250_400ptv","400ptv"]
    recoJETs = ["2btag2jet","2btag3jet","2btag4pjet"]


print("Getting Yields")
hist_file422.cd()
print("\n Looking at hist_file422 \n")
for samp in samps:
    for truePTV in truePTVs:
        for trueJET in trueJETs:
            for recoJET in recoJETs:
                for recoPTV in recoPTVs:
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    yieldElement = truePTV+"x"+trueJET
                    if truePTV == "FWDH":
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                        yieldElement = truePTV
                    histName = samp+"x"+yieldName+"_SR_dPhiBB"
                    hist = hist_file422.Get(histName)
                    integ=0
                    try:
                        integ=hist.Clone().Integral()
                    except:
                        #print(histName+" does not exist")
                        continue
                    if samp == "QQ2HLNU":
                        Yields["qqW422"][yieldName] = integ
                    elif "QQ" in samp:
                        Yields["qqZ422"][yieldName] = integ
                    elif "GG" in samp:
                        Yields["ggZ422"][yieldName] = integ
                    Yields["All422"][yieldName] += integ
                    ZKey = get_key("Z",yieldElement)
                    WKey = get_key("W",yieldElement)
                    if ZKey != "" and "HLNU" not in samp:
                        YieldsPOI["Z422"][ZKey+"_"+recoJET+"_"+recoPTV] += integ
                    if WKey != "" and "HLNU" in samp:
                        YieldsPOI["W422"][WKey+"_"+recoJET+"_"+recoPTV] += integ

hist_file423.cd()
print("\n Looking at hist_file423 \n")
for samp in samps:
    for truePTV in truePTVs:
        for trueJET in trueJETs:
            for recoJET in recoJETs:
                for recoPTV in recoPTVs:
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    yieldElement = truePTV+"x"+trueJET
                    if truePTV == "FWDH":
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                        yieldElement = truePTV
                    histName = samp+"x"+yieldName+"_SR_dPhiBB"
                    hist = hist_file423.Get(histName)
                    integ=0
                    try:
                        integ=hist.Clone().Integral()
                    except:
                        #print(histName+" does not exist")
                        continue
                    if samp == "QQ2HLNU":
                        Yields["qqW423"][yieldName] = integ
                    elif "QQ" in samp:
                        Yields["qqZ423"][yieldName] = integ
                    elif "GG" in samp:
                        Yields["ggZ423"][yieldName] = integ
                    Yields["All423"][yieldName] += integ
                    ZKey = get_key("Z",yieldElement)
                    WKey = get_key("W",yieldElement)
                    if ZKey != "" and "HLNU" not in samp:
                        YieldsPOI["Z423"][ZKey+"_"+recoJET+"_"+recoPTV] += integ
                    if WKey != "" and "HLNU" in samp:
                        YieldsPOI["W423"][WKey+"_"+recoJET+"_"+recoPTV] += integ

hist_file433.cd()
print("\n Looking at hist_file433 \n")
for samp in samps:
    for truePTV in truePTVs:
        for trueJET in trueJETs:
            for recoJET in recoJETs:
                for recoPTV in recoPTVs:
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    yieldElement = truePTV+"x"+trueJET
                    if truePTV == "FWDH":
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                        yieldElement = truePTV
                    histName = samp+"x"+yieldName+"_SR_dPhiBB"
                    hist = hist_file433.Get(histName)
                    integ=0
                    try:
                        integ=hist.Clone().Integral()
                    except:
                        #print(histName+" does not exist")
                        continue
                    if samp == "QQ2HLNU":
                        Yields["qqW433"][yieldName] = integ
                    elif "QQ" in samp:
                        Yields["qqZ433"][yieldName] = integ
                    elif "GG" in samp:
                        Yields["ggZ433"][yieldName] = integ
                    Yields["All433"][yieldName] += integ
                    ZKey = get_key("Z",yieldElement)
                    WKey = get_key("W",yieldElement)
                    if ZKey != "" and "HLNU" not in samp:
                        YieldsPOI["Z433"][ZKey+"_"+recoJET+"_"+recoPTV] += integ
                    if WKey != "" and "HLNU" in samp:
                        YieldsPOI["W433"][WKey+"_"+recoJET+"_"+recoPTV] += integ



#main
histoNames = ["qqZ422","ggZ422","qqW422", 
              "qqZ423","ggZ423","qqW423", 
              "qqZ433","ggZ433","qqW433", 
              "All422","All423","All433",]

histoNames = ["Z422","W422", 
              "Z423","W423", 
              "Z433","W433", 
              "All422","All423","All433",]

truePTVLabels = {"FWDH":"FWDH","PTVx0x75":"0<pTV<75","PTVx75x150":"75<pTV<150",
                 "PTVx150x250":"150<pTV<250","PTVx250x400":"250<pTV<400",
                 "PTVx400x600":"400<pTV<600","PTVxGT600":"pTV>600","PTVxGT250":"pTV>250"}
trueJETLabels = {"0J":"2jet","1J":"3jets","GE2J":"4+jets","GE1J":"3+jets"}
recoPTVLabels = {"75_150ptv":"75<pTV<150","150_250ptv":"150<pTV<250","250_400ptv":"250<pTV<400","400ptv":"pTV>400"}
recoJETLabels = {"2btag2jet":"2jets","2btag3jet":"3jets","2btag4jet":"4jets","2btag4pjet":"4+jets"}

# for all truth bins
for typ in histoNames:
    #if "All" not in typ:
    #    continue
    print("\n Looking at {}\n".format(typ))
    i=0
    sumx=[0]*recoLen # sum per reco
    sumy=[0]*trueLen # sum per truth
    #print(len(sumx))
    #print(len(sumy))
    typ3 = typ
    if "Z" in typ:
        typ3 = typ.replace("Z","qqZ")
    elif "W" in typ:
        typ3 = typ.replace("W","qqW")

    typ1 = ""
    typ2 = ""
    if "Z" in typ:
        typ1 = typ.replace("Z","qqZ")
        typ2 = typ.replace("Z","ggZ")
    elif "W" in typ:
        typ1 = typ.replace("W","qqW")
    elif "All" in typ:
        typ1 = typ
    
    for truePTV in truePTVs:
        breaker=0
        if truePTV=="PTVxGT600":
            continue
        for trueJET in trueJETs:
            i+=1
            j=0
            trueLabel = trueJETLabels[trueJET]+" "+truePTVLabels[truePTV]
            if truePTV=="PTVx400x600":
                trueLabel = trueJETLabels[trueJET]+" pTV>400"
            if breaker == 1:
                i-=1
                continue
            if "FWDH" in truePTV:
                trueLabel = truePTVLabels[truePTV]
                breaker = 1
            Histos2D[typ3].GetXaxis().SetBinLabel(i, trueLabel) #truePTV+"x"+trueJET)
            #Histos2D[typ3].GetXaxis().SetBinLabel(i, "x")
            for recoPTV in recoPTVs:
                for recoJET in recoJETs:
                    j+=1
                    yieldName = truePTV+"x"+trueJET+"_"+recoJET+"_"+recoPTV
                    if truePTV=="PTVx400x600":
                        yieldName2 = "PTVxGT600x"+trueJET+"_"+recoJET+"_"+recoPTV
                        
                    if "FWDH" in truePTV:
                        yieldName = truePTV+"_"+recoJET+"_"+recoPTV
                    #print("  {},{} {}".format(i,j,yieldName))
                    Yield = Yields[typ1][yieldName]
                    if truePTV=="PTVx400x600":
                        Yield += Yields[typ1][yieldName2]
                        if "Z" in typ:
                            Yield += Yields[typ2][yieldName2]
                    if "Z" in typ:
                        Yield += Yields[typ2][yieldName]
                    
                    Histos2D[typ3].SetBinContent(i,j,Yield)
                    sumy[i-1]+=Yield
                    sumx[j-1]+=Yield
                    if i==1:
                        Histos2D[typ3].GetYaxis().SetBinLabel(j, recoJETLabels[recoJET]+" "+recoPTVLabels[recoPTV]) #recoJET+"_"+recoPTV)

            #Histos2D[typ3].SetBinContent(i,10,sumy)
            
    STXS2D(Histos2D[typ3].Clone(),typ)
    if "qqZ" in typ3:
        typ1="qqZ422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="qqZ422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="qqZ423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
                
    elif "ggZ" in typ3:
        typ1="ggZ422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="ggZ422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="ggZ423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
    elif "qqW" in typ3:
        typ1="qqW422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="qqW422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="qqW423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
    elif "All" in typ3:
        typ1="All422"
        STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        if "423_FSR" in typ:
            typ1="All422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="All423"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="All423_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
        elif "433_FSR" in typ:
            typ1="All422_FSR"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            typ1="All433"
            STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="All433_FSR"
                STXS2D_Rel(Histos2D[typ1].Clone(),Histos2D[typ3].Clone(),typ,sumx,sumy,typ1)

if (not do5POI) and (not do10POI):
    print("Not doing any POI plots")
    exit()

# for POI bins
for typ in histoNames:
    if "All" in typ or "W" in typ:
        continue
    print("\n Looking at {}\n".format(typ))
    sumxWH=[0]*recoLen # sum per reco
    sumxZH=[0]*recoLen # sum per reco
    sumy=[0]*trueLenPOI # sum per truth
    #print(len(sumx))
    #print(len(sumy))
    typ3 = typ.replace("Z","W")
    
    trueLabels = []
    plotName = typ.replace("Z","")

    if do5POI:
        plotName+="_5POI"
        #if "Z" in typ:
        trueLabels = ["WHxPTVx0x150","WHxPTVx150x250","WHxPTVxGT250",
                      "ZHxPTVx0x75","ZHxPTVx75x150","ZHxPTVx150x250","ZHxPTVxGT250"]
        #elif "W" in typ:
        #    trueLabels = list(trueBinsWH5POI.keys())
    elif do10POI:
        plotName+="_10POI"
        #if "Z" in typ:
        trueLabels = ["WHxPTVx0x75","WHxPTVx75x150",
                      "WHxPTVx150x250x0J","WHxPTVx150x250xGE1J",
                      "WHxPTVxGT250x0J","WHxPTVxGT250xGE1J",
                      "ZHxPTVx0x75","ZHxPTVx75x150",
                      "ZHxPTVx150x250x0J","ZHxPTVx150x250xGE1J",
                      "ZHxPTVxGT250x0J","ZHxPTVxGT250xGE1J"]
        #elif "W" in typ:
        #    trueLabels = list(trueBinsWH10POI.keys())

    i=0
    for trueLabel in trueLabels:
        i+=1
        j=0
        Histos2DPOI[typ].GetXaxis().SetBinLabel(i, trueLabel) #truePTV+"x"+trueJET)
        #Histos2D[typ].GetXaxis().SetBinLabel(i, "x")
        #print(" {} {}".format(i,trueLabel))
        for recoPTV in recoPTVs:
            for recoJET in recoJETs:
                j+=1
                yieldName = trueLabel+"_"+recoJET+"_"+recoPTV
                Yield = 0
                if "WH" in trueLabel:
                    yieldName = yieldName.replace("WHx","")
                    Yield = YieldsPOI[typ3][yieldName]
                    sumxWH[j-1]+=Yield
                elif "ZH" in trueLabel:
                    yieldName = yieldName.replace("ZHx","")
                    Yield = YieldsPOI[typ][yieldName]
                    sumxZH[j-1]+=Yield
                #print("  {},{} {}".format(i,j,yieldName))
                #print(" {} {}".format(yieldName,Yield))
                #print(sumxZH)
                Histos2DPOI[typ].SetBinContent(i,j,Yield)
                sumy[i-1]+=Yield
                if i==1:
                    Histos2DPOI[typ].GetYaxis().SetBinLabel(j, recoJETLabels[recoJET]+" "+recoPTVLabels[recoPTV]) #recoJET+"_"+recoPTV)

            #Histos2D[typ].SetBinContent(i,10,sumy)
    sumx = []
    sumx.append(sumxWH)
    sumx.append(sumxZH)
    #print(sumx)
    STXS2D(Histos2DPOI[typ].Clone(),plotName)
    if "Z" in typ:
        typ1="Z422"
        plotName2=typ1.replace("Z","")
        STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),plotName,sumx,sumy,plotName2)
        if "423_FSR" in typ:
            typ1="qqZ422_FSR"
            STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),typ,sumx,sumy,typ1)
            typ1="qqZ423"
            STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),typ,sumx,sumy,typ1)
            if "b4Cuts" in typ:
                typ1="qqZ423_FSR"
                STXS2D_Rel(Histos2DPOI[typ1].Clone(),Histos2DPOI[typ].Clone(),typ,sumx,sumy,typ1)
                
                    

exit()


NUMJETS={"nJets==2":"2jet","nJets==3":"3jet","nJets==4":"4jet","nJets>=4":"4pjet"} #,"nJets>=2":"2pjet"}
#NUMJETS={"nJets==2":"2jet"}
METCUT={"MET>150&&MET<250":"150_250ptv","MET>150":"150ptv","MET>250":"250ptv","MET>400":"400ptv","MET>250&&MET<400":"250_400ptv"}
#METCUT={"MET>150&&MET<250":"150_250ptv","MET>250":"250ptv"}
#JETCUT1={"GSCptJ1>45":"45J1"} #,"GSCptJ1>60":"60J1"} #,"GSCptJ1>50":"50J1"} #,"GSCptJ1>70":"70J1"}
#JETCUT2={"GSCptJ2>20":"20J2","GSCptJ2>30":"30J2"} #,"GSCptJ2>25":"25J2"} #,"GSCptJ2>35":"35J2"}
#JETCUT3={"GSCptJ3>20":"20J3","GSCptJ3>30":"30J3"} #,"GSCptJ3>25":"25J3"} #,"GSCptJ3>35":"35J3"}

# For flavour, 15=tau, 5=b, 4=c, <4=l
# rebion,start,end
histNames = {"mBB" : "0,0,400", "GSCMbb" : "0,0,400", "GSCptJ1" : "2,40,500", "GSCptJ2" : "2,20,400", "GSCptJ3" : "2,20,400",
             "TruthWZptJ1" : "2,40,500", "TruthWZptJ2" : "2,20,400", "TruthWZptJ3" : "2,20,400", #"TruthWZMbb" : "40,0,400", 
             "pTB1" : "2,40,500", "pTB2" : "2,20,400", "pTJ3" : "2,20,400",
             "sumPtJets":"2,0,500", "MindPhiMETJet":"0,0,3.2",
             "nJets" : "0,0,10", "nSigJets" : "0,0,10","nFwdJets" : "0,0,10","nbJets" : "0,0,10","nTrkJets" : "0,0,10",
             "Njets_truth_pTjet30" : "0,0,10","mva" : "0,-1,1", "ActualMu" : "2,0,100", "TTBarDecay" : "0,0,10"}



for hName in histNames:
    for numJets in NUMJETS:
        if "J3" in hName:
            if numJets == "nJets==2":
                continue
        for METCut in METCUT:
            m_sigVals={}
            m_sigValsMVA={}
            m_yieldVals={}
            m_SBVals={}
            for JetCut1 in ["45J1_20J2_20J3","45J1_20J2_30J3","45J1_30J2_30J3","45J1_30J2_20J3",]: #"60J1_20J2_20J3","60J1_20J2_30J3","60J1_30J2_30J3","60J1_30J2_20J3",]:
                #if not ("dRBB" in hName or "pTB1" in hName):
                #    continue
                sys.argv.append("-b")
                ROOT.gStyle.SetPadTickX(1)
                ROOT.gStyle.SetPadTickY(1)
                
                histEx=NUMJETS[numJets]+"_"+METCUT[METCut]+"_"+JetCut1
                CUTS=numJets+"_"+METCut+"-&&PassNonJetCountCuts==1-&&"+JetCut1
                #if numJets == "nJets==2":
                #    if ("30" in JetCut1.split("_")[-1]):
                #        continue
                    
                histEx=histEx+"_SR"
                CUTS+="_SR"
                
                print("Looking at "+hName+" with cuts "+histEx)
                ROOT.gROOT.SetBatch(True)

                binning = histNames[hName]
                if ("20J2_20J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file422,False)
                    doDataMC(hName,binning,histEx,hist_file422,True)
                    if ("Jets" in hName or "jets" in hName) and ("sumPtJets" != hName):
                        if "pTjet30" in hName:
                            cutsComparison(hName,binning,histEx,False)
                        if numJets == "nJets==2":
                            cutsComparison(hName,binning,histEx,True)
                    elif hName == "TTBarDecay":
                        makeTTBarDecay(hName,binning,histEx)
                    else:
                        cutsComparison(hName,binning,histEx,False)
                    if hName == "nTrkJets":
                        compareDataMC(hName,histEx,False)
                        compareDataMC(hName,histEx,True)
                    elif hName == "nSigJets":
                        pileupEstimate(hName,histEx)
                elif ("20J2_30J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file423,False)
                    doDataMC(hName,binning,histEx,hist_file423,True)
                elif ("30J2_30J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file433,False)
                    doDataMC(hName,binning,histEx,hist_file433,True)
                elif ("30J2_20J3" in JetCut1):
                    doDataMC(hName,binning,histEx,hist_file42525,False)
                    doDataMC(hName,binning,histEx,hist_file42525,True)
            
            # end of JetCut1
            if "mBB" in hName or "Mbb" in hName:
                makeSignificancePlots(m_sigVals,m_yieldVals,m_SBVals,
                                      NUMJETS[numJets]+"_"+METCUT[METCut],hName)
            if "mva" in hName:
                makeSignificancePlots(m_sigValsMVA,m_yieldValsMVA,m_SBValsMVA,
                                      NUMJETS[numJets]+"_"+METCUT[METCut],hName)

hist_file422.Close()
hist_file423.Close()
hist_file42525.Close()
hist_file433.Close()

print("")
print("")
print("")
print("All complete")
#exit()

if doNotCutCompare:
    print("Running the corrections comparisons")
else:
    exit()
#hist_file = ROOT.TFile("/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/Cuts_45J120J220J3/JetOpt.root","READ")
histNames = {"pTB1" : "46,40,500","pTB2" : "56,20,300","pTJ3" : "56,20,300", "mBB" : "40,0,400"}
#histNames={}
#TruthWZptJ1
#GSCptJ1
#OneMuptJ1

for hName in histNames:
    for numJets in ["nJets==2","nJets==3","nJets==4","nJets>=4"]:
        if hName == "pTJ3":
            if numJets == "nJets==2":
                continue
        for METCut in ["MET>150","MET>250","MET>400","MET>150&&MET<250","MET>250&&MET<400"]:
            #if not ("dRBB" in hName or "pTB1" in hName):
            #    continue
            sys.argv.append("-b")
            ROOT.gStyle.SetPadTickX(1)
            ROOT.gStyle.SetPadTickY(1)
            
            histEx="2tag"
            if numJets=="nJets==2":
                histEx=histEx+"2jet"
            elif numJets=="nJets==3":
                histEx=histEx+"3jet"
            elif numJets=="nJets==4":
                histEx=histEx+"4jet"
            elif numJets=="nJets>=4":
                histEx=histEx+"4pjet"
            if METCut=="MET>150":
                histEx=histEx+"_150ptv"
            elif METCut=="MET>250":
                histEx=histEx+"_250ptv"
            elif METCut=="MET>400":
                histEx=histEx+"_400ptv"
            elif METCut=="MET>150&&MET<250":
                histEx=histEx+"_150_250ptv"
            elif METCut=="MET>250&&MET<400":
                histEx=histEx+"_250_400ptv"

            print("Looking at MET: "+METCut+" var: "+hName+" numJets: "+numJets)
            cutString1 = "EventWeight*(nbJets==2&&nSigJets>=2&&"+numJets+"&&PassNonJetCountCuts==1&&"+METCut+")"
            CUTS="nbJets==2&&nSigJets>=2&&"+numJets+"-&&PassNonJetCountCuts==1-&&"+METCut
            ROOT.gROOT.SetBatch(True)
    
            binning = histNames[hName]
            varName = hName
            histName1 = "h_"+hName
            histName2 = "h_"+hName
            histName3 = "h_"+hName
            drawString1 = varName+">>"+histName1+"("+binning+")"
            drawString2 = varName+">>"+histName2+"("+binning+")"
            drawString3 = varName+">>"+histName3+"("+binning+")"
            if hName == "pTB1":
                histName2="h_TruthWZptJ1"
                drawString2 = "TruthWZptJ1>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ1"
                drawString3 = "GSCptJ1>>"+histName3+"("+binning+")"
            elif hName == "pTB2":
                histName2="h_TruthWZptJ2"
                drawString2 = "TruthWZptJ2>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ2"
                drawString3 = "GSCptJ2>>"+histName3+"("+binning+")"
            elif hName == "pTJ3":
                histName2="h_TruthWZptJ3"
                drawString2 = "TruthWZptJ3>>"+histName2+"("+binning+")"
                histName3="h_GSCptJ3"
                drawString3 = "GSCptJ3>>"+histName3+"("+binning+")"
            elif hName == "mBB":
                histName2="h_OneMuMbb"
                drawString2 = "OneMuMbb>>"+histName2+"("+binning+")"
                histName3="h_GSCMbb"
                drawString3 = "GSCMbb>>"+histName3+"("+binning+")"
            print("Draw("+drawString1+","+cutString1+")")
            print("Draw("+drawString2+","+cutString1+")")
            print("Draw("+drawString3+","+cutString1+")")
    
            VH_chain.Draw(drawString1,ROOT.TCut(cutString1))
            hist_tmp1 = ROOT.gDirectory.Get(histName1)
            hist_tmp1.SetDirectory(0)
            h1 = hist_tmp1.Clone()

            VH_chain.Draw(drawString2,ROOT.TCut(cutString1))
            hist_tmp2 = ROOT.gDirectory.Get(histName2)
            hist_tmp2.SetDirectory(0)
            h2 = hist_tmp2.Clone()

            VH_chain.Draw(drawString3,ROOT.TCut(cutString1))
            hist_tmp3 = ROOT.gDirectory.Get(histName3)
            hist_tmp3.SetDirectory(0)
            h3 = hist_tmp3.Clone()
    
            print("looking at "+hName)
            
            canvas = ROOT.TCanvas(hName, hName, 900, 900)
            upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
            lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
            upper.Draw()
            lower.Draw()
            lower.SetGridy()
            lower.cd().SetBottomMargin(0.3)
            upper.cd()
            ROOT.gStyle.SetOptStat(0)
            
            legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
            legend.SetHeader("              MC,    NEntries,    SoW")
            legend.AddEntry(h1, "PtReco, {:.0f}, {:.2f}".format(h1.GetEntries(),h1.Integral()), "l")
            if "mBB" in varName:
                legend.AddEntry(h2, "OneMu,  {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            else:
                legend.AddEntry(h2, "TruthWZ,  {:.0f}, {:.2f}".format(h2.GetEntries(),h2.Integral()), "l")
            legend.AddEntry(h3, "GSC,    {:.0f}, {:.2f}".format(h3.GetEntries(),h3.Integral()), "l")
            legend.SetBorderSize(0)
            legend.SetFillColor(0)
            legend.SetFillStyle(0)
            
            #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
            #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
            rat1 = h1.Clone()
            rat2 = h2.Clone()
            rat1.Divide(h3) 
            rat2.Divide(h3)
            
            y_max = max(h1.GetMaximum(), h2.GetMaximum(), h3.GetMaximum())*1.4
            
            h1.Draw("hist")
            h1.SetTitle("")
            h1.SetAxisRange(0, y_max, "Y")
            h1.GetYaxis().SetMaxDigits(3)
            h1.GetYaxis().SetTitle("Entries")
            h1.GetYaxis().SetTitleSize(0.04)
            h1.GetYaxis().SetTitleOffset(1.2)
            h1.GetYaxis().SetLabelSize(0.05)
            h1.SetYTitle("Entries")
            h1.SetMarkerColor(ROOT.kBlue)
            #h1.SetMarkerStyle(8)
            #h1.SetMarkerSize(1.5)
            h1.SetLineColor(ROOT.kBlue)    
            h1.SetLineWidth(2)
            h1.SetLabelOffset(5)
            #h1.GetXaxis().SetRangeUser(xLow,xHigh)
            
            h2.Draw("same hist")
            h2.SetMarkerColor(ROOT.kRed)
            h2.SetLineColor(ROOT.kRed)
            h2.SetLineWidth(2)

            h3.Draw("same hist")
            h3.SetMarkerColor(ROOT.kBlack)
            h3.SetLineColor(ROOT.kBlack)
            h3.SetLineWidth(2)
            
            t = ROOT.TLatex()
            t.SetNDC()
            t.SetTextFont(72)
            t.SetTextColor(1)
            t.SetTextSize(0.03)
            t.SetTextAlign(4)
            t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
            t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
            t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
            
            legend.Draw()
            
            lower.cd()
            rat1.SetTitle("")
            rat1.GetXaxis().SetTitle(hName)
            rat1.GetXaxis().SetTitleSize(0.09)
            rat1.GetXaxis().SetTitleOffset(1.05)
            
            #Line = ROOT.TLine(xLow,1.,xHigh,1.)
            #Line.SetLineWidth(2)
            #Line.SetLineColor(ROOT.kBlack)
            yLow = 0
            yHigh = 2
            rat1.SetLineWidth(2)
            rat1.SetLineColor(ROOT.kBlue)
            rat1.SetAxisRange(yLow, yHigh, "Y")
            rat1.GetXaxis().SetLabelSize(0.09)
            rat1.GetYaxis().SetLabelSize(0.07)
            rat1.GetYaxis().SetTitleOffset(0.7)
            rat1.GetYaxis().SetTitleSize(0.06)
            rat1.GetYaxis().SetTitle("bc/bb")
            rat1.GetYaxis().SetNdivisions(506)
            rat2.SetLineColor(ROOT.kRed)
            
            rat1.Draw("")
            rat2.Draw("same")

            #Line.Draw("same")
            #rat.GetXaxis().SetRangeUser(xLow,xHigh)
            
            
            canvas.SaveAs(working_dir+"VH_"+histEx+"_"+hName+".png")
            

        
