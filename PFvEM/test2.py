import os
import sys
import ROOT
import argparse
import re


###########################################################################
m = ROOT.RooRealVar("m","m",25,200)
B_xp = ROOT.RooRealVar("B_xp","B_xp",121.5,100,130)
B_sig = ROOT.RooRealVar("B_sig","B_sig",15,10,25)
B_xi = ROOT.RooRealVar("B_xi","B_xi",0,-1,1)
B_rho1 = ROOT.RooRealVar("B_rho1","B_rho1", 0,-1,1)
B_rho2 = ROOT.RooRealVar("B_rho2","B_rho2", 0,-1,1)


bukin = ROOT.RooBukinPdf("bukin","bukin",m,B_xp,B_sig,B_xi,B_rho1,B_rho2)

dF = ROOT.TFile("histograms/0Lep/full/PF0_qq_gg.root")
dmass = dF.Get("3jet_150ptv_Mbb_w").Clone()
dmass.Rebin(5)
dH = ROOT.RooDataHist("dH","dH",ROOT.RooArgList(m),dmass)
mframe = m.frame()

fit2 = bukin.fitTo(dH,ROOT.RooFit.Save())
fit2.Print()

#CB = ROOT.RooArgSet(bukin)

dH.plotOn(mframe)
bukin.plotOn(mframe,ROOT.RooFit.Name("bukin"),ROOT.RooFit.LineColor(2),ROOT.RooFit.LineStyle(1))

mean = B_xp.getVal()
sigma = B_sig.getVal()
leg1 = ROOT.TLegend(0.2,0.75,0.6,0.85)
leg1.SetBorderSize(0)
leg1.SetFillColor(0)
leg1.SetTextSize(0.03)
leg1.AddEntry(mframe.findObject("bukin"),"Bukin {:.2f} {:.2f}".format(mean, sigma), "L")
leg1.AddEntry(mframe.findObject("bukin"),"Bukin {:.2f} {:.2f}".format(mean, sigma), "L")

can = ROOT.TCanvas("massframe","massframe",750,800)
can.cd()
can.SetTickx()
can.SetTicky()
        
mframe.Draw()
mframe.SetTitle("")
mframe.GetXaxis().SetTitle("#pi^{+}#pi^{-} Invariant mass [GeV]")
mframe.GetYaxis().SetTitle("Events")
mframe.SetMaximum(mframe.GetMaximum()*1.2)
leg1.Draw("same")
mframe.GetYaxis().SetTitle("Events")
can.SaveAs("test2.png")

print("Waiting for input")
input()
    

