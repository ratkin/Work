import os
import sys
import ROOT
import re
import copy

sys.argv.append("-b")

ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)

# python bTagSF.py a

#per = "all"
per = sys.argv[1]

do_scale = 0
f0Name = "Condor/outputs/em18_32-15_0L/em18_0L_32-15.root"
f1Name = "Condor/outputs/em18_33-01_0L/em18_0L_33-01.root"
f2Name = "Condor/outputs/pf18_33-01_0L/pf18_0L_33-01.root"
f3Name = "Condor/test_outputs/pf19_33-01_0L/pf19_0L_33-01.root"

weights = ["bTagSF","bTagSF2j","bTagSF3j",]
           #"JVTSF","JVTSF2j","JVTSF3j",
           #"MCSF","MCSF2j","MCSF3j"]

if per != "all":
    f0Name = "Condor/outputs/em18_32-15_0L/em18_0L_{}_32-15.root".format(per)
    f1Name = "Condor/outputs/em18_33-01_0L/em18_0L_{}_33-01.root".format(per)
    f2Name = "Condor/outputs/pf18_33-01_0L/pf18_0L_{}_33-01.root".format(per)
    f3Name = "Condor/test_outputs/pf19_33-01_0L/pf19_0L_{}_33-01.root".format(per)
    #weights = ["bTagSF","bTagSF2j","bTagSF3j"]

work_dir = "histograms/weights_bTagFix/"

f0 = ROOT.TFile.Open(f0Name)
f1 = ROOT.TFile.Open(f1Name)
f2 = ROOT.TFile.Open(f2Name)
f3 = ROOT.TFile.Open(f3Name)


xLows={"bTagSF":0.7,"bTagSF2j":0.7,"bTagSF3j":0.7,
      "JVTSF":0.9,"JVTSF2j":0.9,"JVTSF3j":0.9,
       "MCSF":-0.2,"MCSF2j":-0.2,"MCSF3j":-0.2}
xHighs={"bTagSF":1.6,"bTagSF2j":1.6,"bTagSF3j":1.6,
       "JVTSF":1.6,"JVTSF2j":1.6,"JVTSF3j":1.6,
        "MCSF":0.3,"MCSF2j":0.3,"MCSF3j":0.3}

for hName in weights:
    print("looking at "+hName)
    f0.cd()
    H0 = f0.Get(hName).Clone()
    f1.cd()
    H1 = f1.Get(hName).Clone()
    f2.cd()
    H2 = f2.Get(hName).Clone()
    f3.cd()
    H3 = f3.Get(hName).Clone()

    canvas = ROOT.TCanvas(hName+" compare", hName+" compare", 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    legend = ROOT.TLegend(0.5, 0.55, 0.87, 0.87)
    ROOT.gStyle.SetOptStat(0)
    
    H0.Draw("hist")
    H1.Draw("hist same")
    H2.Draw("hist same")
    H3.Draw("hist same")
    
    if do_scale:
        H0.Scale(1/H0.Integral())
        H1.Scale(1/H1.Integral())
        H2.Scale(1/H2.Integral())
        H3.Scale(1/H3.Integral())
    
    H0.Rebin(2)
    H1.Rebin(2)
    H2.Rebin(2)
    H3.Rebin(2)
    
    xLow = xLows[hName]
    xHigh = xHighs[hName]
    H1.SetAxisRange(xLow, xHigh, "X")
    H2.SetAxisRange(xLow, xHigh, "X")
    H3.SetAxisRange(xLow, xHigh, "X")
    H0.SetAxisRange(xLow, xHigh, "X")
    H1.GetXaxis().SetRangeUser(xLow, xHigh)
    H2.GetXaxis().SetRangeUser(xLow, xHigh)
    H3.GetXaxis().SetRangeUser(xLow, xHigh)
    H0.GetXaxis().SetRangeUser(xLow, xHigh)
    
    
    R2 = H2.Clone()
    R3 = H3.Clone()
    R1 = H1.Clone()
    
    y_max = max(H1.GetMaximum(), H2.GetMaximum(), H3.GetMaximum(), H0.GetMaximum())
    H0.SetAxisRange(0.0, y_max * 1.2, "Y")    
    H0.SetTitle("")
    H0.GetYaxis().SetMaxDigits(3)
    H0.GetYaxis().SetTitle("Arb. units")
    H0.GetYaxis().SetTitleSize(0.04)
    H0.GetYaxis().SetTitleOffset(1.2)
    H0.SetYTitle("Arb. units")
    H0.SetLineColor(ROOT.kBlack)
    H0.SetLineWidth(2)
    H0.SetLabelOffset(5)
    
    H2.SetLineColor(ROOT.kBlue)
    H2.SetLineWidth(2)
    H3.SetLineColor(ROOT.kRed)
    H3.SetLineWidth(2)
    H1.SetLineColor(ROOT.kMagenta)
    H1.SetLineWidth(2)
    
    R2.SetLineColor(ROOT.kBlue)
    R2.SetLineWidth(2)
    R3.SetLineColor(ROOT.kRed)
    R3.SetLineWidth(2)
    R1.SetLineColor(ROOT.kMagenta)
    R1.SetLineWidth(2)
    
    legend.SetBorderSize(0)
    legend.SetHeader("       jets   vtag   mean   std dev")
    legend.AddEntry(H0, "em18 32-15, {:.2f}, {:.2f}".format(H0.GetMean(1),H0.GetStdDev(1)), "l")
    legend.AddEntry(H1, "em18 33-01, {:.2f}, {:.2f}".format(H1.GetMean(1),H1.GetStdDev(1)), "l")
    legend.AddEntry(H2, "pf18 33-01, {:.2f}, {:.2f}".format(H2.GetMean(1),H2.GetStdDev(1)), "l")
    legend.AddEntry(H3, "pf19 33-01, {:.2f}, {:.2f}".format(H3.GetMean(1),H3.GetStdDev(1)), "l")
    legend.Draw()
    
    lower.cd()
    R2.Divide(H0)
    R3.Divide(H0)
    R1.Divide(H0)
    
    R2.SetTitle("")
    R2.GetXaxis().SetTitle(hName)
    R2.GetXaxis().SetTitleSize(0.09)
    R2.GetXaxis().SetTitleOffset(1.05)
    R2.SetAxisRange(0., 2, "Y")
    R2.GetXaxis().SetLabelSize(0.09)
    R2.GetYaxis().SetLabelSize(0.07)
    R2.GetYaxis().SetTitleOffset(0.7)
    R2.GetYaxis().SetTitleSize(0.06)
    R2.GetYaxis().SetTitle("33-01/32-15")
    R2.GetYaxis().SetNdivisions(506)
    
    R2.Draw("hist")
    R3.Draw("same hist")
    R1.Draw("same hist")
    
    canvas.SaveAs(work_dir+"{}_Compare_{}_scale{}.png".format(hName,per,do_scale))

