"""
Quick read and compare of 2 easytrees
"""
import os
import sys
import ROOT
import collections
import time
import ctypes
import math
#import argparse

sys.argv.append("-b")
print ("Opening files")
RDF = ROOT.ROOT.RDataFrame
# 10% 2jet, CRLow =
cut_CRLow_2j = "dRBB < (3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 95% 2jet, CRHigh =
cut_CRHigh_2j = "dRBB > (8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))"
#SR 2jet
cut_SR_2j = "dRBB<=(8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))&&dRBB>=(3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 10% 3jet, CRLow =
cut_CRLow_3j = "dRBB < (4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))"
# 85% 3jet, CRHigh =
cut_CRHigh_3j = "dRBB > (7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"
# SR
cut_SR_3j = "dRBB>=(4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))&&dRBB<=(7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"



samples_by_type = {
    "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
    "signal" : ["*Hbb*"] #only look at Hbb samples for VHbb dRBB cuts
                   #"signal" : ["ggZll*","ggZvv*","qq*"], 
                   }

samples_by_type = {"ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"],"signalHbb" : ["*Hbb*"], "data" : ["data*"],
                   "background" : ["Zmumu*","Zee*","Znunu*","Ztautau*", "Wenu*","Wmunu*","Wtaunu*", "ttbar*", "stop*", "Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"]}


colours = {"signal" : ROOT.kRed, 
           "WJets" : ROOT.kGreen+4,
           "WJetsbb" : ROOT.kGreen+4, 
           "WJetsbc" : ROOT.kGreen+3,
           "WJetsbl" : ROOT.kGreen+2,
           "WJetscc" : ROOT.kGreen+1,
           "WJetscl" : ROOT.kGreen-6,
           "WJetsl" : ROOT.kGreen-9,
           "ZJets" : ROOT.kAzure+2, 
           "ZJetsbb" : ROOT.kAzure+2, 
           "ZJetsbc" : ROOT.kAzure+1, 
           "ZJetsbl" : ROOT.kAzure-2, 
           "ZJetscc" : ROOT.kAzure-4, 
           "ZJetscl" : ROOT.kAzure-8, 
           "ZJetsl" : ROOT.kAzure-9, 
           "ttbar" : ROOT.kOrange,
           "ttbarbb" : ROOT.kOrange, 
           "ttbarbc" : ROOT.kOrange+1, 
           "ttbarbl" : ROOT.kOrange+2, 
           "ttbarcc" : ROOT.kOrange+3, 
           "ttbarcl" : ROOT.kOrange+4, 
           "ttbarl" : ROOT.kOrange+5, 
           "stop" : ROOT.kOrange-1, "diboson" : ROOT.kGray, "data" : ROOT.kBlack}

samples_list = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
                "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
                "ttbar" : ["ttbar"],  "stop" : ["stop"],"diboson" : ["diboson"],
                "data" : ["data"],"signal" : ["signal"]}
samples_list = {"WJets" : ["WJets"], "ZJets" : ["ZJets"], 
                "ttbar" : ["ttbar"],  "stop" : ["stop"],"diboson" : ["diboson"],
                "data" : ["data"],"signal" : ["signal"]}


chains_by_type  = {_type:ROOT.TChain("Nominal") for _type in samples_by_type.keys()}

dirs = ["/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_a_Resolved_D_D/fetch/data-MVATree/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_d_Resolved_D_D/fetch/data-MVATree/",
        "/eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_e_Resolved_D_D/fetch/data-MVATree/"]
        
for _type, samples in samples_by_type.iteritems():
    for sample in samples:
        for d in dirs:
            #print('adding {0} to chain {1}'.format(d + sample, _type))
            chains_by_type[_type].Add(d + sample)

#print(chains_by_type["ZJets"].GetEntries())
dataframes_by_type = {_type: RDF(chain) for _type, chain in chains_by_type.iteritems()}

#dataframes_by_type = {bkg:get_newVar(df) for bkg, df in dataframes_by_type_0.iteritems()}


#Try doing a scan across the 3 parameters in the equation,in the different ptv regions separately but sharing the parameter value


#def get_newVar(df):
#    return df.Define("dRHJ3","TLorentzVector L1, L2, L3; L1.SetPtEtaPhiM(pTB1,etaB1,phiB1,mB1); L2.SetPtEtaPhiM(pTB2,etaB2,phiB2,mB2); L3.SetPtEtaPhiM(pTJ3,etaJ3,phiJ3,mJ3); return abs(L3.DeltaR((L1+L2)));")

working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/CRSR_studies/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)


#hNames = {"mBB" : "50,0,250", "MCEventWeight" : "50,-0.05,0.05", "EventWeight" : "50,0,0.05", "LumiWeight" : "50,0,0.2", 
#          "PUWeight" : "50,0,2", "JVTWeight" : "50,0.6,1.8", "TriggerSF" : "50,0.8,1.2", "BTagSF" : "50,0.5,1.5",
#          "dEtaBB" : "30,0,3","dPhiBB" : "30,0,3", "dRBB" : "30,0,3", "dPhiMETdijet" : "24,2,3.2", "pTBB" : "45,50,500",
#          "pTBBJ" : "45,50,500","pTB1" : "36,40,400","pTB2" : "38,20,400","pTJ3" : "28,20,300", "MindPhiMETJet" : "32,0,3.2",
#          "dPhiMETMPT" : "32,0,3.2", "mBBJ" : "40,0,400", "sumPtJets" : "40,0,400", "HT" : "130,200,1500", "flavB1" : "19,-2,17",
#          "flavB2" : "19,-2,17", "flavJ3" : "19,-2,17", "isSLEvent" : "2,0,2", "metSig" : "30,0,30", "nFwdJets" : "3,0,3"}

# For flavour, 15=tau, 5=b, 4=c, <4=l

#f1 = ROOT.TFile.Open(File1)
#T1 = f1.Get("Nominal")
#f2 = ROOT.TFile.Open(File2)
#T2 = f2.Get("Nominal")

def doDataMC(varName,binning,canName,FileName):
    print("doDataMC")
    ROOT.gROOT.SetBatch(True)
    # 
    F = ROOT.TFile(FileName,"READ")
    histName = varName
    if "nJets==2" in canName:
        histName=histName+"_2jet"
    elif "nJets==3" in canName:
        histName=histName+"_3jet"
    else:
        histName=histName+"_4jet"
    if "SR" in canName:
        histName=histName+"_SR"
    if "MindPhiMETJet4J" in canName:
        histName=histName+"_MindPhi4J"
    elif "MindPhiMETJet" not in canName:
        histName=histName+"_noMindPhi"


    histograms={}
    line=canName.split("&&")
    canName1=canName
    F.cd()
    #print("Looping to Get histograms")
    for sampl in samples_list:
        for samp in samples_list[sampl]:
            histName1=samp+"_"+histName
            hist_tmp1 = F.Get(histName1)
            histograms[samp]=hist_tmp1.Clone()

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)


    for samp in samples_list:
        for sampl in samples_list[samp]:
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)
                for i in range(histograms[sampl].GetNbinsX()):
                    if histograms[sampl].GetBinContent(i+1)>0:
                        histograms[sampl].SetBinError(i+1,math.sqrt(histograms[sampl].GetBinContent(i+1)))

    hs = ROOT.THStack("hs","")
    for samp in ["ttbar","stop",
                 "WJets","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJets","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson","signal",]:
        if samp in histograms:
            hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ttbar","WJets","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "ZJets","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl","diboson"]:
        if samp in histograms:
            hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()

    if ("mBB" in varName or "Mbb" in varName):
        getSignificance(hsig,hb)
    elif varName == "MindPhiMETJet":
        for samp in histograms:
            print("  {}: {}".format(samp,histograms[samp].Integral()))
    #if ("mva" in varName):
    #    getSignificanceMVA(hsig,hb,canName1+"_"+varName)

    
    doBlinding(hsig,hb,hd,varName)
    sigScale = 20
    if ("mBB" in varName or "Mbb" in varName) and "2jet" in histName:
        sigScale = 10
    hsig.Scale(sigScale)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")
        

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName1)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","ZJets","ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl",
                 "WJets","WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl",
                 "stop","ttbar"]:
        if samp in histograms:
            if "ZJets" in samp or "WJets" in samp:
                sampleName=samp.replace("Jets","")
                legend1.AddEntry(histograms[samp], sampleName+", {:.1f}".format(histograms[samp].Integral()), "f")
            else:
                legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x {}".format(sigScale),"l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)


    yLow = -0.2
    yHigh = 0.8
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("ep") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    
    yHigh2 = rat1.GetMaximum()*1.1
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    #rat1.SetAxisRange(0, yHigh2, "Y")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinError(i+1)>0.075:
            rat1.SetBinContent(i+1,(rat1.GetBinContent(i)+rat1.GetBinContent(i+2))/2)
            rat1.SetBinError(i+1,(rat1.GetBinError(i)+rat1.GetBinError(i+2))/2)
            #print("{} {}".format(rat1.GetBinContent(i+1),rat1.GetBinError(i+1)))
    
    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("hist Y+")
    

    plotName = working_dir+"analysisCuts/"+histName
    canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def doAnalysisCuts(dataframes_by_type,mBBCut,makeHistos):

    rads = ROOT.TMath.DegToRad()
    Cuts2jet = "nSigJets>=2&&nJets==2&&nbJets==2&&pTB1>45&&MindPhiMETJet>20*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>120{}".format(rads,rads,rads,rads,mBBCut)
    Cuts2jetSR = "nSigJets>=2&&nJets==2&&nbJets==2&&pTB1>45&&MindPhiMETJet>20*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>120&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts3jet = "nSigJets>=2&&nJets==3&&nbJets==2&&pTB1>45&&MindPhiMETJet>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150{}".format(rads,rads,rads,rads,mBBCut)
    Cuts3jetSR = "nSigJets>=2&&nJets==3&&nbJets==2&&pTB1>45&&MindPhiMETJet>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jet = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jetSR = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jet_noMindPhi = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150{}".format(rads,rads,rads,mBBCut)
    Cuts4jetSR_noMindPhi = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150&&Description==\"SR\"{}".format(rads,rads,rads,mBBCut)
    Cuts4jet_MindPhi4J = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jetSR_MindPhi4J = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jet_MindPhi4J25 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>25*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jetSR_MindPhi4J25 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>25*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>150&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jet_ST180 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>180{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jetSR_ST180 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>180&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jet_MindPhi4J_ST180 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>180{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jetSR_MindPhi4J_ST180 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>30*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>180&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jet_MindPhi4J25_ST180 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>25*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>180{}".format(rads,rads,rads,rads,mBBCut)
    Cuts4jetSR_MindPhi4J25_ST180 = "nSigJets>=2&&nJets==4&&nbJets==2&&pTB1>45&&MindPhiMETJet4J>25*{}&&dPhiMETdijet>120*{}&&dPhiBB<140*{}&&dPhiMETMPT<90*{}&&sumPtJets>180&&Description==\"SR\"{}".format(rads,rads,rads,rads,mBBCut)

    CutList = [Cuts4jet,Cuts4jetSR,Cuts4jet_noMindPhi,Cuts4jetSR_noMindPhi,Cuts4jet_MindPhi4J,Cuts4jetSR_MindPhi4J] #Cuts2jet,Cuts2jetSR,Cuts3jet,Cuts3jetSR,
    CutList = [Cuts4jet_MindPhi4J25,Cuts4jetSR_MindPhi4J25,Cuts4jet_ST180,Cuts4jetSR_ST180,Cuts4jet_MindPhi4J25_ST180,Cuts4jetSR_MindPhi4J25_ST180,Cuts4jet_MindPhi4J_ST180,Cuts4jetSR_MindPhi4J_ST180]
    hNames = {"mBB":"30,0,300","MindPhiMETJet4J":"32,0,3.2"} #"sumPtJets":"40,100,500","MindPhiMETJet":"32,0,3.2"} #,
    
    FileName = "CRSR.root"
    if makeHistos:
        print("making histos")
        ROOT.gROOT.SetBatch(True)
        F = ROOT.TFile(FileName,"RECREATE")
        
        """
        DF2jet = {bkg:df.Filter(Cuts2jet) for bkg, df in dataframes_by_type.iteritems()}
        DF2jetSR = {bkg:df.Filter(Cuts2jetSR) for bkg, df in dataframes_by_type.iteritems()}
        DF3jet = {bkg:df.Filter(Cuts3jet) for bkg, df in dataframes_by_type.iteritems()}
        DF3jetSR = {bkg:df.Filter(Cuts3jetSR) for bkg, df in dataframes_by_type.iteritems()}
        DF4jet = {bkg:df.Filter(Cuts4jet) for bkg, df in dataframes_by_type.iteritems()}
        DF4jetSR = {bkg:df.Filter(Cuts4jetSR) for bkg, df in dataframes_by_type.iteritems()}
        DF4jet_noMindPhi = {bkg:df.Filter(Cuts4jet_noMindPhi) for bkg, df in dataframes_by_type.iteritems()}
        DF4jetSR_noMindPhi = {bkg:df.Filter(Cuts4jetSR_noMindPhi) for bkg, df in dataframes_by_type.iteritems()}
        DF4jet_MindPhi4J = {bkg:df.Filter(Cuts4jet_MindPhi4J) for bkg, df in dataframes_by_type.iteritems()}
        DF4jetSR_MindPhi4J = {bkg:df.Filter(Cuts4jetSR_MindPhi4J) for bkg, df in dataframes_by_type.iteritems()}
        """
        
        
        for hName,cuts in hNames.iteritems():
            for sampl in samples_list:
                for samp in samples_list[sampl]:
                    print("looking at "+hName+" "+samp)
                    for cut in CutList:
                        print("   "+cut)
                        histName = hName
                        if "nJets==2" in cut:
                            histName=histName+"_2jet"
                        elif "nJets==3" in cut:
                            histName=histName+"_3jet"
                        else:
                            histName=histName+"_4jet"
                        if "SR" in cut:
                            histName=histName+"_SR"
                        if "MindPhiMETJet4J>25" in cut:
                            histName=histName+"_MindPhi4J25"
                        elif "MindPhiMETJet4J" in cut:
                            histName=histName+"_MindPhi4J"
                        elif "MindPhiMETJet" not in cut:
                            histName=histName+"_noMindPhi"
                        if "sumPtJets>180" in cut:
                            histName=histName+"_ST180"
                            
                        histName2 = samp+"_"+histName
                        #bins = cuts.split(",")
                        #h_model = ROOT.RDF.TH1DModel(histName2,histName2,int(bins[0]),float(bins[1]),float(bins[2]))
                        #h = dataframes_by_type_cutA[samp].Filter(METcutString).Histo1D(h_model,"dRBB","EventWeight")
                        
                        histCreation = hName+">>"+histName2+"("+hNames[hName]+")"
                        eventSelector = "EventWeight*("+cut
                        if "Jets" in samp:
                            if "b" in samp or "c" in samp or "l" in samp:
                                eventSelector += "&&EventFlavor==\""+samp[5:]+"\""
                        eventSelector += ")"
                        #print(eventSelector)
                        #continue
                        chains_by_type[sampl].Draw(hName+">>"+histName2+"("+cuts+")",eventSelector)
                        h = ROOT.gDirectory.Get(histName2)
                        F.cd()
                        #h.Sumw2()
                        h.Write()
        F.Close()                
    else:
        print("Making DataMC plots")
        for hName,cuts in hNames.iteritems():
            for cut in CutList:
                doDataMC(hName,cuts,cut,FileName)


#2D plots
def do2DPlots(hName,hName2,cutString,sample,flav,CUTS,extras):
    
    #sys.argv.append("-b")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    
    print("Doing 2D for "+hName+":"+hName2)
    
    m_fit_func_2j10 = ROOT.TF1("fit_func_2j10", "[0]+exp([1]+[2]*x)", 150, 500)
    m_fit_func_3j10 = ROOT.TF1("fit_func_3j10", "[0]+exp([1]+[2]*x)", 150, 500)
    m_fit_func_2j95 = ROOT.TF1("fit_func_2j95", "[0]+exp([1]+[2]*x)", 150, 500)
    m_fit_func_3j85 = ROOT.TF1("fit_func_3j85", "[0]+exp([1]+[2]*x)", 150, 500)
    
    # 10% 2jet
    m_fit_func_2j10.SetParameters(3.99879e-01, 7.88279e-01, -1.02303e-02)
    # 95% 2jet
    m_fit_func_2j95.SetParameters(8.70286e-01, 1.37867e+00, -7.95322e-03)
    # 10% 3jet
    m_fit_func_3j10.SetParameters(4.20840e-01, 2.67742e-01, -8.08892e-03)
    # 85% 3jet
    m_fit_func_3j85.SetParameters(7.63283e-01, 1.33179e+00, -7.30396e-03)
    
    
    binning = histNames[hName]
    binning2 = histNames[hName2]
    histName = "h_"+hName
    drawString = hName+":"+hName2+">>"+histName+"("+binning2+","+binning+")"
    print("drawString: "+drawString)
    
    chains_by_type[sample].Draw(drawString,ROOT.TCut(cutString))
    hist_tmp1 = ROOT.gDirectory.Get(histName)
    hist_tmp1.SetDirectory(0)
    h1 = hist_tmp1.Clone()
    
    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
    
    
    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    #upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    #lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    #upper.Draw()
    #lower.Draw()
    #lower.SetGridy()
    #lower.cd().SetBottomMargin(0.3)
    #upper.cd()
    ROOT.gStyle.SetOptStat(0)
    
    #h1.Scale(1/h1.Integral())
    
    h1.Draw("colz")
    h1.SetTitle("")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle(hName)
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle(hName)
    #h1.SetMarkerColor(ROOT.kBlue)
    #h1.SetMarkerStyle(8)
    #h1.SetMarkerSize(1.5)
    #h1.SetLineColor(ROOT.kBlue)    
    #h1.SetLineWidth(2)
    #h1.SetLabelOffset(5)
    #h1.GetXaxis().SetRangeUser(xLow,xHigh)

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    
    t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
    t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
    t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
    
    
    h1.GetXaxis().SetTitle(hName2)
    h1.GetXaxis().SetTitleSize(0.04)
    h1.GetXaxis().SetTitleOffset(1.05)
    h1.GetXaxis().SetLabelSize(0.05)
    if hName == "dRBB":
        if "nJets==2" in CUTS:
            m_fit_func_2j10.Draw("same")
            m_fit_func_2j95.Draw("same")
        elif "nJets==3" in CUTS:
            m_fit_func_3j10.Draw("same")
            m_fit_func_3j85.Draw("same")
    #Line.Draw("same")
    #rat.GetXaxis().SetRangeUser(xLow,xHigh)
    
    
    canvas.SaveAs(working_dir+sample+"_"+flav+"2D_"+hName+"_"+hName2+"_"+extras+".png")
    
'''
for hName in hNames:
    if hName != "dRBB":
        continue
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    print("Looking at "+hName)
    #cutString = "EventWeight*(nJets>1.5&&nJets<2.5&&nbJets>1.5&&nbJets<2.5&&pTV>150&&Description==\"SR\")"
    cutString = "EventWeight*(nbJets==2)"
    if not "mBB" in hName:
        cutString = "(nbJets==2)"
    cutString = "EventWeight*(nSigJets>=2&&pTB1>45&&dPhiMETdijet>2.094395&&dPhiBB<2.443461&&dPhiMETMPT<1.570796&&MindPhiMETJet>0.523599&&sumPtJets>150&&nbJets==2&&nJets==2&&MET>150&&MET<250&&Description==\"SR\")"
    cutString = "EventWeight*(nSigJets>=2&&pTB1>45&&dPhiMETdijet>2.094395&&dPhiBB<2.443461&&dPhiBB>1&&dEtaBB>1&&dPhiMETMPT<1.570796&&MindPhiMETJet>0.523599&&sumPtJets>150&&nbJets==2&&nJets==2&&MET>150&&MET<250&&Description==\"SR\")"
    binning = hNames[hName]
    varName = hName
    histName = "h01_"+hName
    histName2 = "h04_"+hName
    drawString = varName+">>"+histName+"("+binning+")"
    drawString2 = varName+">>"+histName2+"("+binning+")"
    # mBB>>h01_mBB(50,0,250)
    print("drawString: "+drawString)

    T1.Draw(drawString,ROOT.TCut(cutString))
    hist_tmp1 = ROOT.gDirectory.Get(histName)
    hist_tmp1.SetDirectory(0)
    hist1 = hist_tmp1.Clone()
    T2.Draw(drawString2,ROOT.TCut(cutString))
    hist_tmp2 = ROOT.gDirectory.Get(histName2)
    hist_tmp2.SetDirectory(0)
    hist2 = hist_tmp2.Clone()
    
    #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
  
    print("looking at "+hName)

    canvas = ROOT.TCanvas(hName, hName, 900, 900)
    upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
    lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
    upper.Draw()
    lower.Draw()
    lower.SetGridy()
    lower.cd().SetBottomMargin(0.3)
    upper.cd()
    ROOT.gStyle.SetOptStat(0)
    h1 = hist1.Clone()
    h2 = hist2.Clone()
    #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
    #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
    rat = h2.Clone()
    rat.Divide(h1)

        
    y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.1

    h1.Draw("hist")
    h1.SetTitle("")
    h1.SetAxisRange(0.0, y_max, "Y")
    h1.GetYaxis().SetMaxDigits(3)
    h1.GetYaxis().SetTitle("Entries")
    h1.GetYaxis().SetTitleSize(0.04)
    h1.GetYaxis().SetTitleOffset(1.2)
    h1.GetYaxis().SetLabelSize(0.05)
    h1.SetYTitle("Entries")
    #h1.SetMarkerColor(ROOT.kBlue)
    #h1.SetMarkerStyle(8)
    #h1.SetMarkerSize(1.5)
    h1.SetLineColor(ROOT.kBlue)    
    h1.SetLineWidth(2)
    h1.SetLabelOffset(5)
        
    h2.Draw("same")
    h2.SetMarkerColor(ROOT.kRed)
    #h2.SetMarkerStyle(8)
    #h2.SetMarkerSize(1.5)
    h2.SetLineColor(ROOT.kRed)
    h2.SetLineWidth(2)
    

    legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
    legend.SetHeader("              MC,    NEntries,    SoW")
    legend.AddEntry(h1, "{} pf19, {:.1f}, {:.1f}".format(vTag1,h1.GetEntries(),h1.Integral()), "l")
    legend.AddEntry(h2, "{} pf19, {:.1f}, {:.1f}".format(vTag2,h2.GetEntries(),h2.Integral()), "l")
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.Draw()


    lower.cd()
    rat.SetTitle("")
    rat.GetXaxis().SetTitle(hName)
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    #Line = ROOT.TLine(xLow,1.,xHigh,1.)
    #Line.SetLineWidth(2)
    #Line.SetLineColor(ROOT.kBlack)
    yLow = 0.79
    yHigh = 1.21
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.09)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("{}/{}".format(vTag2,vTag1))
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("")
    #Line.Draw("same")

    
    canvas.SaveAs(working_dir+hName+"_Draw3.png")
'''


def printYields(yields,canName,mBBCut,yieldFile):
    
    W=open(working_dir+yieldFile,'a')
    canName=canName.replace("_","-")
    if mBBCut != "":
        canName+="-mBBCut"

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+canName+"}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.09\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{CRLow}} & \multicolumn{1}{c|}{\textbf{SR}} & \multicolumn{1}{c|}{\textbf{CRHigh}} & \multicolumn{1}{c|}{\textbf{new CRLow}} & \multicolumn{1}{c|}{\textbf{new SR}} & \multicolumn{1}{c}{\textbf{new CRHigh}}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in ["signal","diboson","stop","ttbar","ZJets","WJets"]:
        tot = yields[samp]["tot"]
        tot1 = tot
        if tot1 ==0.0:
            tot1=1
        W.write(r"    {} & {:.1f} ({:.2f}$\%$) & {:.1f} ({:.2f}$\%$) & {:.1f} ({:.2f}$\%$) & {:.1f} ({:.2f}$\%$) & {:.1f} ({:.2f}$\%$) & {:.1f} ({:.2f}$\%$)\\ ".format(
            samp,
            yields[samp]["CRL"],100*(yields[samp]["CRL"]/tot1),
            yields[samp]["SR"],100*(yields[samp]["SR"]/tot1),
            yields[samp]["CRH"],100*(yields[samp]["CRH"]/tot1),
            yields[samp]["CRLn"],100*(yields[samp]["CRLn"]/tot1),
            yields[samp]["SRn"],100*(yields[samp]["SRn"]/tot1),
            yields[samp]["CRHn"],100*(yields[samp]["CRHn"]/tot1),
        ))
        
        W.write("\n")
    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()

def doBlinding(S1,B1,h1,var):
    thresh = 0.1
    if ("mBB" in var or "Mbb" in var) and "J" not in var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            i+=1
    elif "mva" in var:
        tot_sig = S1.Integral()
        if not tot_sig > 0: 
            return h1
        sum_sig=0
        i=h1.GetNbinsX()
        #while h1.GetNbinsX()-i<5:
        #    h1.SetBinContent(i, 0)
        #    h1.SetBinError(i, 0)
        #    i-=1
        while i>0:
            sum_sig+=S1.GetBinContent(i)
            if sum_sig/tot_sig < 0.7:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
                i-=1
            else:
                break
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1

    return h1

def getSignificance(S,B):
    i = S.FindBin(30)
    binMax = S.FindBin(250)
    summ=0
    while i <= binMax:
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print(math.sqrt(summ))
    return math.sqrt(summ)

def calcSigs(H,L,Hn,Ln,df150,df250,df400,nJ,mBBCut):
    print("Calculating signifincances")

    SRdef = "dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(H[0],H[1],H[2],L[0],L[1],L[2])
    SRdefn = "dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(Hn[0],Hn[1],Hn[2],Ln[0],Ln[1],Ln[2])

    h1_model = ROOT.RDF.TH1DModel("hist","hist",40,0,400,)
    h_sig_a = df150["signal"].Filter(SRdef).Histo1D(h1_model,"mBB","EventWeight")
    h_bkg_a = df150["background"].Filter(SRdef).Histo1D(h1_model,"mBB","EventWeight")
    h_sig = h_sig_a.Clone()
    h_bkg = h_bkg_a.Clone()
    h_sign_a = df150["signal"].Filter(SRdefn).Histo1D(h1_model,"mBB","EventWeight")
    h_bkgn_a = df150["background"].Filter(SRdefn).Histo1D(h1_model,"mBB","EventWeight")
    h_sign = h_sign_a.Clone()
    h_bkgn = h_bkgn_a.Clone()
    
    print("Significance for orig "+nJ+"jet, 150_250ptv, "+mBBCut)
    sig = getSignificance(h_sig,h_bkg)
    print("Significance for new "+nJ+"jet, 150_250ptv, "+mBBCut)
    sig = getSignificance(h_sign,h_bkgn)

    h_sig_a = df250["signal"].Filter(SRdef).Histo1D(h1_model,"mBB","EventWeight")
    h_bkg_a = df250["background"].Filter(SRdef).Histo1D(h1_model,"mBB","EventWeight")
    h_sig = h_sig_a.Clone()
    h_bkg = h_bkg_a.Clone()
    h_sign_a = df250["signal"].Filter(SRdefn).Histo1D(h1_model,"mBB","EventWeight")
    h_bkgn_a = df250["background"].Filter(SRdefn).Histo1D(h1_model,"mBB","EventWeight")
    h_sign = h_sign_a.Clone()
    h_bkgn = h_bkgn_a.Clone()
    
    print("Significance for orig "+nJ+"jet, 250_400ptv, "+mBBCut)
    sig = getSignificance(h_sig,h_bkg)
    print("Significance for new "+nJ+"jet, 250_400ptv, "+mBBCut)
    sig = getSignificance(h_sign,h_bkgn)

    h_sig_a = df400["signal"].Filter(SRdef).Histo1D(h1_model,"mBB","EventWeight")
    h_bkg_a = df400["background"].Filter(SRdef).Histo1D(h1_model,"mBB","EventWeight")
    h_sig = h_sig_a.Clone()
    h_bkg = h_bkg_a.Clone()
    h_sign_a = df400["signal"].Filter(SRdefn).Histo1D(h1_model,"mBB","EventWeight")
    h_bkgn_a = df400["background"].Filter(SRdefn).Histo1D(h1_model,"mBB","EventWeight")
    h_sign = h_sign_a.Clone()
    h_bkgn = h_bkgn_a.Clone()
    
    print("Significance for orig "+nJ+"jet, 400ptv, "+mBBCut)
    sig = getSignificance(h_sig,h_bkg)
    print("Significance for new "+nJ+"jet, 400ptv, "+mBBCut)
    sig = getSignificance(h_sign,h_bkgn)

def compareCuts(dfA,df150,df250,df400,nJ,mBBCut):
    print("Getting yields for "+nJ+"jets")
    H = [0,0,0]
    L = [0,0,0]
    if nJ == "2":
        H = [0.870286,1.37867,0.00795322]
        L = [0.399879,0.788279,0.0102303]
    else:
        H = [0.763283,1.33179,0.00730396]
        L = [0.42084,0.267742,0.00808892]

    Hn = [0,0,0]
    Ln = [0,0,0]
    if mBBCut == "":
        if nJ == "2":
            Hn = [0.678379, 1.18108, 0.00568135]
            Ln = [0.402995, 0.699758, 0.00980900]
        elif nJ == "3":
            Hn = [0.592662, 1.06871, 0.00512076]
            Ln = [0.442423, 0.685141, 0.0106205]
        elif nJ == "4":
            Hn = [0.559864, 0.886349, 0.00343594]
            Ln = [0.452607, 0.0703649, 0.00853378]
    else:
        if nJ == "2":
            Hn = [0.677828, 1.18123, 0.00567942]
            Ln = [0.404530, 0.809654, 0.0102129]
        elif nJ == "3":
            Hn = [0.585644, 1.06923, 0.00507580]
            Ln = [0.443068, 0.844347, 0.0110951]
        elif nJ == "4":
            #Hn = [0.554328, 0.893958, 0.00342458]
            Ln = [0.460234, 0.516161, 0.0101334]
            Hn = [0.451226, 0.902484, 0.00352704]  #80%
            
    calcSigs(H,L,Hn,Ln,df150,df250,df400,nJ,mBBCut)
    #exit()

    yieldFile = "CRSRstudies_Yields_"+nJ+"jet.tex"
    if mBBCut == "":
        yieldFile = "CRSRstudies_Yields_"+nJ+"jet_mBBCut.tex"
    W=open(working_dir+yieldFile,'w')
    W.write(r"\documentclass[aspectratio=916]{beamer}")
    W.write("\n")
    W.write(r"\mode<presentation> {")
    W.write("\n")
    W.write(r"    \usetheme{default}}")
    W.write("\n")
    W.write(r"    \usepackage{graphicx}")
    W.write("\n")
    W.write(r"    \usepackage{booktabs}")
    W.write("\n")
    W.write(r"    \usepackage{caption}")
    W.write("\n")
    W.write(r"    \usepackage{subcaption}")
    W.write("\n")
    W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
    W.write("\n")
    W.write(r"    \begin{document}")
    W.write("\n")
    W.write(r"\end{document} % move this to end of file")
    W.write("\n")
    W.close()

    
    yields = {"signal":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "diboson":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "stop":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ttbar":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ZJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "WJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,}}

    print("Doing inclusive")
    for samp in ["signal","diboson","stop","ttbar","ZJets","WJets"]:
        print("  "+samp)
        yields[samp]["tot"] = dfA[samp].Sum("EventWeight").GetValue()
        yields[samp]["CRL"] = dfA[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRH"] = dfA[samp].Filter("dRBB > ({} + exp({} - {}*MET))".format(H[0],H[1],H[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRLn"] = dfA[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRHn"] = dfA[samp].Filter("dRBB > ({} + exp({} - {}*MET))".format(Hn[0],Hn[1],Hn[2])).Sum("EventWeight").GetValue()
        yields[samp]["SR"] = yields[samp]["tot"]-yields[samp]["CRH"]-yields[samp]["CRL"]
        yields[samp]["SRn"] = yields[samp]["tot"]-yields[samp]["CRHn"]-yields[samp]["CRLn"]
        print("  tot: {} CRL: {} SR: {} CRH: {}".format(yields[samp]["tot"],yields[samp]["CRL"],yields[samp]["SR"],yields[samp]["CRH"]))
    printYields(yields,"Inclusive_"+nJ+"jets",mBBCut,yieldFile)

    yields = {"signal":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "diboson":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "stop":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ttbar":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ZJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "WJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,}}
        
    print("Doing 150")
    for samp in ["signal","diboson","stop","ttbar","ZJets","WJets"]:
        print("  "+samp)
        yields[samp]["tot"] = df150[samp].Sum("EventWeight").GetValue()
        yields[samp]["CRL"] = df150[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["SR"] = df150[samp].Filter("dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(H[0],H[1],H[2],L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRLn"] = df150[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["SRn"] = df150[samp].Filter("dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(Hn[0],Hn[1],Hn[2],Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRH"] = yields[samp]["tot"]-yields[samp]["SR"]-yields[samp]["CRL"]
        yields[samp]["CRHn"] = yields[samp]["tot"]-yields[samp]["SRn"]-yields[samp]["CRLn"]
    printYields(yields,"150-250_"+nJ+"jets",mBBCut,yieldFile)

    yields = {"signal":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "diboson":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "stop":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ttbar":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ZJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "WJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,}}
        
    print("Doing 250")
    for samp in ["signal","diboson","stop","ttbar","ZJets","WJets"]:
        print("  "+samp)
        yields[samp]["tot"] = df250[samp].Sum("EventWeight").GetValue()
        yields[samp]["CRL"] = df250[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["SR"] = df250[samp].Filter("dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(H[0],H[1],H[2],L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRLn"] = df250[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["SRn"] = df250[samp].Filter("dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(Hn[0],Hn[1],Hn[2],Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRH"] = yields[samp]["tot"]-yields[samp]["SR"]-yields[samp]["CRL"]
        yields[samp]["CRHn"] = yields[samp]["tot"]-yields[samp]["SRn"]-yields[samp]["CRLn"]
    printYields(yields,"250-400_"+nJ+"jets",mBBCut,yieldFile)

    yields = {"signal":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "diboson":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "stop":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ttbar":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "ZJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,},
              "WJets":{"tot":0,"CRL":0,"SR":0,"CRH":0,"CRLn":0,"SRn":0,"CRHn":0,}}
        
    print("Doing 400")
    for samp in ["signal","diboson","stop","ttbar","ZJets","WJets"]:
        print("  "+samp)
        yields[samp]["tot"] = df400[samp].Sum("EventWeight").GetValue()
        yields[samp]["CRL"] = df400[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["SR"] = df400[samp].Filter("dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(H[0],H[1],H[2],L[0],L[1],L[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRLn"] = df400[samp].Filter("dRBB < ({} + exp({} - {}*MET))".format(Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["SRn"] = df400[samp].Filter("dRBB < ({} + exp({} - {}*MET))&&dRBB > ({} + exp({} - {}*MET))".format(Hn[0],Hn[1],Hn[2],Ln[0],Ln[1],Ln[2])).Sum("EventWeight").GetValue()
        yields[samp]["CRH"] = yields[samp]["tot"]-yields[samp]["SR"]-yields[samp]["CRL"]
        yields[samp]["CRHn"] = yields[samp]["tot"]-yields[samp]["SRn"]-yields[samp]["CRLn"]
    printYields(yields,"400_"+nJ+"jets",mBBCut,yieldFile)
    
    

    #dataframes_by_type_cut400 = {bkg:df.Filter(cutString3) for bkg, df in dataframes_by_type.iteritems()}


def myfunc3(x,par):
   xx = par[0]*(x[0]-par[1])**2
   yy = par[2]*(x[1]-par[3])**2
   zz = par[4]*(x[2]-par[5])**2
   return xx+yy+zz

def myfunc1(x,par):
   xx = par[0]+ROOT.TMath.Exp(par[1]-par[2]*x[0])
   return xx

def getPercentBin(h,perc):
    tot=h.Integral()
    nBins=h.GetNbinsX()
    #i=nBins
    i=1
    dRBB=-1
    diff=100
    newdRBB = 0
    while (i<nBins):
        #print(i)
        newTot=h.Integral(1,i)
        #print("{} {}".format(newTot,newTot/tot))
        newDiff = perc-float(newTot/tot)
        newdRBB = h.GetBinCenter(i)
        #print("{} {}".format(diff,newDiff))
        if newDiff > 0:
            diff = newDiff
            dRBB = newdRBB
        else: # newDiff > diff:
            print("tot: {} diff: {} bin: {} dRBB: {}".format(tot,diff,i,dRBB))
            print("tot: {} diff: {} bin: {} dRBB: {}".format(tot,newDiff,i+1,newdRBB))
            return newdRBB
        i+=1

#main
sys.argv.append("-b")

# 10% 2jet, CRLow =
cut_CRLow_2j = "dRBB < (3.99879e-01 + exp(7.88279e-01 + -1.02303e-02*MET))"
# 95% 2jet, CRHigh =
cut_CRHigh_2j = "dRBB > (8.70286e-01 + exp(1.37867e+00 + -7.95322e-03*MET))"
# 10% 3jet, CRLow =
cut_CRLow_3j = "dRBB < (4.20840e-01 + exp(2.67742e-01 + -8.08892e-03*MET))"
# 85% 3jet, CRHigh =
cut_CRHigh_3j = "dRBB > (7.63283e-01 + exp(1.33179e+00 + -7.30396e-03*MET))"


numJets = "4"
optimiseCuts = False
mBBCut = "" # either "&&GSCMbb>50" or ""
analysisCuts = True
makeHistos = True
if analysisCuts:
    print("Doing analysis Cuts")
    doAnalysisCuts(dataframes_by_type,mBBCut,makeHistos)
    exit()



#mBBCut = ""
cutStringA = "(nSigJets>=2&&nJets=="+numJets+"&&nbJets==2&&PassNonJetCountCuts==1&&MET>150"+mBBCut+")"
cutString1 = "(nSigJets>=2&&nJets=="+numJets+"&&nbJets==2&&PassNonJetCountCuts==1&&MET>150&&MET<250"+mBBCut+")"
cutString2 = "(nSigJets>=2&&nJets=="+numJets+"&&nbJets==2&&PassNonJetCountCuts==1&&MET>250&&MET<400"+mBBCut+")"
cutString3 = "(nSigJets>=2&&nJets=="+numJets+"&&nbJets==2&&PassNonJetCountCuts==1&&MET>400"+mBBCut+")"
dataframes_by_type_cutA = {bkg:df.Filter(cutStringA) for bkg, df in dataframes_by_type.iteritems()}
dataframes_by_type_cut150 = {bkg:df.Filter(cutString1) for bkg, df in dataframes_by_type.iteritems()}
dataframes_by_type_cut250 = {bkg:df.Filter(cutString2) for bkg, df in dataframes_by_type.iteritems()}
dataframes_by_type_cut400 = {bkg:df.Filter(cutString3) for bkg, df in dataframes_by_type.iteritems()}
ROOT.gROOT.SetBatch(True)


if not optimiseCuts:
    print("Comparing CRSR cuts, not optimising")
    compareCuts(dataframes_by_type_cutA,dataframes_by_type_cut150,dataframes_by_type_cut250,dataframes_by_type_cut400,numJets,mBBCut)
    exit()

ROOT.gROOT.SetBatch(True)

CRHperc = 0.95
CRLperc = 0.1
if numJets == "3":
    CRHperc = 0.85
elif numJets == "4":
    CRHperc = 0.80

binWidth = 5
XL = 150
XH = 500
nBins=int((XH-XL)/binWidth)
#hFit = ROOT.TH1F("hFit","hFit",35,150,500)
hFitH = ROOT.TH1F("hFitH","hFitH",nBins,XL,XH)
hFitL = ROOT.TH1F("hFitL","hFitL",nBins,XL,XH)
#h_model = ROOT.RDF.TH2DModel("hFSRCut","hFSRCut", 25,150,400,13,0.4,1.7)
#hist1 = dataframes_by_type_cut150["signalHbb"].Histo2D(h_model, "MET", "dRBB", "EventWeight")

h2_model = ROOT.RDF.TH2DModel("hist","hist",70,150,500,140,0,3.5)
h_2D_a = dataframes_by_type_cutA["signalHbb"].Histo2D(h2_model,"MET","dRBB","EventWeight")
h_2D = h_2D_a.Clone()

'''
hFitH.SetBinContent(1,2)
hFitH.SetBinContent(5,1.5)
hFitH.SetBinContent(10,1)
hFitH.SetBinContent(15,0.5)
'''
for i in range(nBins):
    lowMET = 150 + binWidth*i
    highMET = 150 + binWidth*(1+i)
    print("looking at {} <ptv< {}".format(lowMET,highMET))
    METcutString = "MET>{}&&MET<{}".format(lowMET,highMET)
    #hist = ROOT.TH1F("hist","hist",350,0,3.5)
    h_model = ROOT.RDF.TH1DModel("hist","hist",350,0,3.5)
    h = dataframes_by_type_cutA["signalHbb"].Filter(METcutString).Histo1D(h_model,"dRBB","EventWeight")
    h_model1 = ROOT.RDF.TH1DModel("hist1","hist1",350,0,3.5)
    h1 = dataframes_by_type_cutA["diboson"].Filter(METcutString).Histo1D(h_model1,"dRBB","EventWeight")
    dRBBVal = getPercentBin(h.Clone(),CRHperc)
    dRBBVal1 = getPercentBin(h1.Clone(),CRLperc)

    hFitH.SetBinContent(i+1,dRBBVal)
    hFitL.SetBinContent(i+1,dRBBVal1)
    err = ctypes.c_double(0.)
    integral = h.IntegralAndError(1, h.GetNbinsX(), err)
    err1 = ctypes.c_double(0.)
    integral1 = h1.IntegralAndError(1, h1.GetNbinsX(), err1)
    #print("{} {} {}".format(h.Integral(),h.GetSumOfWeights(),integral))
    #print("{} {}".format(dRBBVal/ROOT.TMath.Sqrt(h.GetSumOfWeights()),err))
    #hFitH.SetBinError(i+1,err.value)
    #hFitL.SetBinError(i+1,err1.value)
    hFitH.SetBinError(i+1,0.01)
    hFitL.SetBinError(i+1,0.01)
    
func1 = ROOT.TF1("func1",myfunc1, 150,500,3)
func1.SetParameters(0.8,1.3,0.007)
hFitH.Fit(func1)
FUNC1 = hFitH.GetFunction("func1")
func2 = ROOT.TF1("func2",myfunc1, 150,500,3)
func2.SetParameters(0.4,0.8,0.01)
hFitL.Fit(func2)
FUNC2 = hFitL.GetFunction("func2")
#print("{} {} {}".format(FUNC1.GetParameter(0),FUNC1.GetParameter(1),FUNC1.GetParameter(2)))

print("finished fit")
#time.sleep(300)

#print("Actual a,b,c: {} {} {}".format(a,b,c))

a=0
b=0
c=0

a = FUNC1.GetParameter(0)
b = FUNC1.GetParameter(1)
c = FUNC1.GetParameter(2)
#a=0.8; b=1.3; c=0.007

print("CRHigh from fit: {} {} {}".format(a,b,c))
numA = dataframes_by_type_cutA["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
NUMA = dataframes_by_type_cutA["signalHbb"].Sum("EventWeight").GetValue()
num1 = dataframes_by_type_cut150["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
NUM1 = dataframes_by_type_cut150["signalHbb"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
NUM2 = dataframes_by_type_cut250["signalHbb"].Sum("EventWeight").GetValue()
num3 = dataframes_by_type_cut400["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
NUM3 = dataframes_by_type_cut400["signalHbb"].Sum("EventWeight").GetValue()
print("Inclusive: {} {} {} {}".format(NUMA,numA,100*(numA/NUMA),100*abs(CRHperc-(numA/NUMA))))
print("150: {} {} {} {}   250: {} {} {} {}   400: {} {} {} {}".format(NUM1,num1,100*(num1/NUM1),100*abs(CRHperc-(num1/NUM1)), NUM2,num2,100*(num2/NUM2),100*abs(CRHperc-(num2/NUM2)), NUM3,num3,100*(num3/NUM3),100*abs(CRHperc-(num3/NUM3))))

a1 = FUNC2.GetParameter(0)
b1 = FUNC2.GetParameter(1)
c1 = FUNC2.GetParameter(2)

print("Signal below CRLow from fit: {} {} {}".format(a1,b1,c1))
numA = dataframes_by_type_cutA["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUMA = dataframes_by_type_cutA["signalHbb"].Sum("EventWeight").GetValue()
num1 = dataframes_by_type_cut150["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUM1 = dataframes_by_type_cut150["signalHbb"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUM2 = dataframes_by_type_cut250["signalHbb"].Sum("EventWeight").GetValue()
num3 = dataframes_by_type_cut400["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUM3 = dataframes_by_type_cut400["signalHbb"].Sum("EventWeight").GetValue()
print("Inclusive: {} {} {}".format(NUMA,numA,100*(numA/NUMA)))
print("150: {} {} {}   250: {} {} {}   400: {} {} {}".format(NUM1,num1,100*(num1/NUM1), NUM2,num2,100*(num2/NUM2), NUM3,num3,100*(num3/NUM3)))

print("CRLow from fit: {} {} {}".format(a1,b1,c1))
numA = dataframes_by_type_cutA["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUMA = dataframes_by_type_cutA["diboson"].Sum("EventWeight").GetValue()
num1 = dataframes_by_type_cut150["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUM1 = dataframes_by_type_cut150["diboson"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUM2 = dataframes_by_type_cut250["diboson"].Sum("EventWeight").GetValue()
num3 = dataframes_by_type_cut400["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a1,b1,c1)).Sum("EventWeight").GetValue()
NUM3 = dataframes_by_type_cut400["diboson"].Sum("EventWeight").GetValue()
print("Inclusive: {} {} {} {}".format(NUMA,numA,100*(numA/NUMA),100*abs(CRLperc-(numA/NUMA))))
print("150: {} {} {} {}   250: {} {} {} {}   400: {} {} {} {}".format(NUM1,num1,100*(num1/NUM1),100*abs(CRLperc-(num1/NUM1)), NUM2,num2,100*(num2/NUM2),100*abs(CRLperc-(num2/NUM2)), NUM3,num3,100*(num3/NUM3),100*abs(CRLperc-(num3/NUM3))))


print("Making plot")
ah=bh=ch=al=bl=cl=0
if numJets == "2":
    ah = 0.870286
    bh = 1.37867
    ch = 0.00795322
    al = 0.399879
    bl = 0.788279
    cl = 0.0102303
else:
    ah = 0.763283
    bh = 1.33179
    ch = 0.00730396
    al = 0.42084
    bl = 0.267742
    cl = 0.00808892

print("")
print("Original CRHigh: {} {} {}".format(ah,bh,ch))
numA = dataframes_by_type_cutA["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(ah,bh,ch)).Sum("EventWeight").GetValue()
NUMA = dataframes_by_type_cutA["signalHbb"].Sum("EventWeight").GetValue()
num1 = dataframes_by_type_cut150["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(ah,bh,ch)).Sum("EventWeight").GetValue()
NUM1 = dataframes_by_type_cut150["signalHbb"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(ah,bh,ch)).Sum("EventWeight").GetValue()
NUM2 = dataframes_by_type_cut250["signalHbb"].Sum("EventWeight").GetValue()
num3 = dataframes_by_type_cut400["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(ah,bh,ch)).Sum("EventWeight").GetValue()
NUM3 = dataframes_by_type_cut400["signalHbb"].Sum("EventWeight").GetValue()
print("Inclusive: {} {} {} {}".format(NUMA,numA,100*(numA/NUMA),100*abs(CRHperc-(numA/NUMA))))
print("150: {} {} {} {}   250: {} {} {} {}   400: {} {} {} {}".format(NUM1,num1,100*(num1/NUM1),100*abs(CRHperc-(num1/NUM1)), NUM2,num2,100*(num2/NUM2),100*abs(CRHperc-(num2/NUM2)), NUM3,num3,100*(num3/NUM3),100*abs(CRHperc-(num3/NUM3))))

print("Original signal below CRLow: {} {} {}".format(al,bl,cl))
numA = dataframes_by_type_cutA["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUMA = dataframes_by_type_cutA["signalHbb"].Sum("EventWeight").GetValue()
num1 = dataframes_by_type_cut150["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUM1 = dataframes_by_type_cut150["signalHbb"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUM2 = dataframes_by_type_cut250["signalHbb"].Sum("EventWeight").GetValue()
num3 = dataframes_by_type_cut400["signalHbb"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUM3 = dataframes_by_type_cut400["signalHbb"].Sum("EventWeight").GetValue()
print("Inclusive: {} {} {}".format(NUMA,numA,100*(numA/NUMA)))
print("150: {} {} {}   250: {} {} {}   400: {} {} {}".format(NUM1,num1,100*(num1/NUM1), NUM2,num2,100*(num2/NUM2), NUM3,num3,100*(num3/NUM3)))

print("Original CRLow: {} {} {}".format(al,bl,cl))
numA = dataframes_by_type_cutA["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUMA = dataframes_by_type_cutA["diboson"].Sum("EventWeight").GetValue()
num1 = dataframes_by_type_cut150["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUM1 = dataframes_by_type_cut150["diboson"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUM2 = dataframes_by_type_cut250["diboson"].Sum("EventWeight").GetValue()
num3 = dataframes_by_type_cut400["diboson"].Filter("dRBB < ({} + exp({} - {}*MET))".format(al,bl,cl)).Sum("EventWeight").GetValue()
NUM3 = dataframes_by_type_cut400["diboson"].Sum("EventWeight").GetValue()
print("Inclusive: {} {} {} {}".format(NUMA,numA,100*(numA/NUMA),100*abs(CRLperc-(numA/NUMA))))
print("150: {} {} {} {}   250: {} {} {} {}   400: {} {} {} {}".format(NUM1,num1,100*(num1/NUM1),100*abs(CRLperc-(num1/NUM1)), NUM2,num2,100*(num2/NUM2),100*abs(CRLperc-(num2/NUM2)), NUM3,num3,100*(num3/NUM3),100*abs(CRLperc-(num3/NUM3))))


CRHigh = ROOT.TF1("CRHigh",myfunc1, 150,500,3)
CRHigh.SetParameters(ah,bh,ch)
CRLow = ROOT.TF1("CRLow",myfunc1, 150,500,3)
CRLow.SetParameters(al,bl,cl)

canvas = ROOT.TCanvas("c","c", 900, 900)
ROOT.gStyle.SetOptStat(0)

h_2D.Draw("colz")
h_2D.SetTitle("")
h_2D.GetYaxis().SetMaxDigits(3)
h_2D.GetYaxis().SetTitle("dRBB")
h_2D.GetYaxis().SetTitleSize(0.04)
h_2D.GetYaxis().SetTitleOffset(1.2)
h_2D.GetYaxis().SetLabelSize(0.05)
h_2D.SetYTitle("dRBB")

legend = ROOT.TLegend(0.65, 0.7, 0.9, 0.85)
#legend.SetHeader("              MC,    NEntries,    SoW")
legend.AddEntry(FUNC1, "New fit", "l")
legend.AddEntry(CRHigh, "Original fit", "l")
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.Draw()

t = ROOT.TLatex()
t.SetNDC()
t.SetTextFont(72)
t.SetTextColor(1)
t.SetTextSize(0.03)
t.SetTextAlign(4)

t.DrawLatex(0.15, 0.85, "0 lepton, signal, "+numJets+" jets")
t.DrawLatex(0.15, 0.80, "CRLow: {} CRHigh: {}".format(CRLperc,CRHperc))
if mBBCut != "":
    t.DrawLatex(0.15, 0.75, "GSCMbb > 50 GeV")


h_2D.GetXaxis().SetTitle("pTV (GeV)")
h_2D.GetXaxis().SetTitleSize(0.04)
h_2D.GetXaxis().SetTitleOffset(1.05)
h_2D.GetXaxis().SetLabelSize(0.05)
    
hFitH.SetLineColor(ROOT.kBlack)
hFitH.SetMarkerColor(ROOT.kBlack)
hFitH.SetMarkerStyle(20)
hFitH.SetLineWidth(2)

hFitL.SetLineColor(ROOT.kBlack)
hFitL.SetMarkerColor(ROOT.kBlack)
hFitL.SetMarkerStyle(20)
hFitL.SetLineWidth(2)

hFitH.Draw("epsames")
hFitL.Draw("epsames")
CRHigh.Draw("same")
CRHigh.SetLineColor(ROOT.kGreen)
CRLow.Draw("same")
CRLow.SetLineColor(ROOT.kGreen)
print("saving canvas")
plotName = "Test_CRSR_Fit_"+numJets+"jets.png"
if mBBCut != "":
    plotName = "Test_CRSR_Fit_"+numJets+"jets_mBBCut.png"
canvas.SaveAs(working_dir+plotName)




"""
numJets = "2"
cutString1 = "(nSigJets>=2&&nJets=="+numJets+"&&nbJets==2&&PassNonJetCountCuts==1&&MET>150&&MET<250)"#*EventWeight"
cutString2 = "(nSigJets>=2&&nJets=="+numJets+"&&nbJets==2&&PassNonJetCountCuts==1&&MET>250)"#*EventWeight"
#CUTS="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb&&MET>250&&flavJ3==5"
#histEx = "250ptvAll3Reg_flavJ3is5_"+numJets
dataframes_by_type_cut150 = {bkg:df.Filter(cutString1) for bkg, df in dataframes_by_type.iteritems()}
dataframes_by_type_cut250 = {bkg:df.Filter(cutString2) for bkg, df in dataframes_by_type.iteritems()}


NUM1 = dataframes_by_type_cut150["signal"].Sum("EventWeight").GetValue()
a=0.870286
b=1.37867
c=0.00795322
num1 = dataframes_by_type_cut150["signal"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
print("{} {} {}".format(NUM1,num1,100*(num1/NUM1)))
NUM2 = dataframes_by_type_cut250["signal"].Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["signal"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
print("{} {} {}".format(NUM2,num2,100*(num2/NUM2)))

#NUM1=148
#NUM2=41

#0.9 1.4 0.008
alpha = [0.775,0.8,0.825,0.85,0.875,0.9,0.925,0.95,0.975]
beta = [1.275,1.3,1.325,1.35,1.375,1.4,1.425,1.45,1.475]
gamma = [0.007,0.00725,0.0075,0.00775,0.008,0.00825,0.0085,0.00875,0.009]
#alpha = [0.75,0.95]
#beta = [1.25,1.45]
#gamma = [0.0065,0.0085]
imin=-1
jmin=-1
kmin=-1
min1=100
min2=100
h3 = ROOT.TH3F("h3","test",9,0.7625,0.9875,9,1.2625,1.4875,9,0.006875,0.009125)
for i in range(len(alpha)):
    for j in range(len(beta)):
        for k in range(len(gamma)):
            num1 = dataframes_by_type_cut150["signal"].Filter("dRBB < ({} + exp({} - {}*MET))".format(alpha[i],beta[j],gamma[k])).Sum("EventWeight").GetValue()
            num2 = dataframes_by_type_cut250["signal"].Filter("dRBB < ({} + exp({} - {}*MET))".format(alpha[i],beta[j],gamma[k])).Sum("EventWeight").GetValue()
            print("{} {} {}".format(alpha[i],beta[j],gamma[k]))
            print("{} {} {} {}   {} {} {} {}".format(NUM1,num1,100*(num1/NUM1),abs(95-100*(num1/NUM1)),NUM2,num2,100*(num2/NUM2),abs(95-100*(num2/NUM2))))
            val = abs(95-100*(num1/NUM1))**2 + abs(95-100*(num2/NUM2))**2
            if val < min1:
                min1=val
                imin=i
                jmin=j
                kmin=k
            h3.SetBinContent(h3.GetXaxis().FindBin(alpha[i]),h3.GetYaxis().FindBin(beta[j]),h3.GetZaxis().FindBin(gamma[k]),val)
print("Minimum at {} {} {}".format(alpha[imin],beta[jmin],gamma[kmin]))

func3 = ROOT.TF3("func3",myfunc3, 0.7625,0.9875,1.2625,1.4875,0.006875,0.009125, 6)
#func3 = ROOT.TF3("func3",myfunc3, 0.5,1.0,1.0,1.6,0.005,0.010,6)
func3.SetParameters(100,a,100,b,100,c)
h3.Fit(func3)

f=open("CRSR.log",'r')
A=f.readlines()
f.close()
last2=len(A)
i=1

print("Actual a,b,c: {} {} {}".format(a,b,c))

while i<10:
    line=A[last2-i].split(' ')
    if "p5" in line[5]:
        c=float(line[16])
        print("c: {}".format(c))
    elif "p3" in line[5]:
        b=float(line[16])
        print("b: {}".format(b))
    elif "p1" in line[5]:
        a=float(line[16])
        print("a: {}".format(a))
   # print(line)
    i+=1
num1 = dataframes_by_type_cut150["signal"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
num2 = dataframes_by_type_cut250["signal"].Filter("dRBB < ({} + exp({} - {}*MET))".format(a,b,c)).Sum("EventWeight").GetValue()
print("{} {} {} {}   {} {} {} {}".format(NUM1,num1,100*(num1/NUM1),abs(95-100*(num1/NUM1)),NUM2,num2,100*(num2/NUM2),abs(95-100*(num2/NUM2))))
"""

exit()
for sample in dataframes_by_type_cutbb:
    if sample != "ttbar":
            continue
    for hName in histNames:
        if not ("bin" in hName):
            continue
        
        sys.argv.append("-b")
        ROOT.gStyle.SetPadTickX(1)
        ROOT.gStyle.SetPadTickY(1)

        print("Looking at "+hName)
        ROOT.gROOT.SetBatch(True)
        
        h = ROOT.TH1D(bkg + "-" + v_name, bkg + "-" + v_name, n_bins, x_min, x_max)
        h_model = ROOT.RDF.TH1DModel(h)
        hist = df.Histo1D(h_model, v_name, "EventWeight")
        hist = hist.Clone()
        hist.Sumw2()


        if hName == "dRBB":
            CUTS1="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb"
            CUTS2="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bc"
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\")",sample,"bb",CUTS1,"150_"+numJets)
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\")",sample,"bc",CUTS2,"150_"+numJets)
            CUTS1="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb&&MET>250"
            CUTS2="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bc&&MET>250"
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\"&&MET>250)",sample,"bb",CUTS1,"250_"+numJets)
            do2DPlots(hName,"MET","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\"&&MET>250)",sample,"bc",CUTS2,"250_"+numJets)
        if hName == "pTB1":
            CUTS1="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bb&&MET>250&&SR"
            CUTS2="nSigJets>=2&&nJets=="+numJets+"&&-PassNonJetCountCuts==1&&-EventFlavor==bc&&MET>250&&SR"
            do2DPlots(hName,"pTB2","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bb\"&&MET>250&&"+cut_SR_3j+")",sample,"bb",CUTS1,"250SR_"+numJets)
            do2DPlots(hName,"pTB2","EventWeight*(nSigJets>=2&&nJets=="+numJets+"&&PassNonJetCountCuts==1&&EventFlavor==\"bc\"&&MET>250&&"+cut_SR_3j+")",sample,"bc",CUTS2,"250SR_"+numJets)

        binning = histNames[hName]
        varName = hName
        histName1 = "bb_"+hName
        histName2 = "bc_"+hName
        drawString1 = varName+">>"+histName1+"("+binning+")"
        drawString2 = varName+">>"+histName2+"("+binning+")"
        print("drawString: "+drawString1)
        
        chains_by_type[sample].Draw(drawString1,ROOT.TCut(cutString1))
        hist_tmp1 = ROOT.gDirectory.Get(histName1)
        hist_tmp1.SetDirectory(0)
        h1 = hist_tmp1.Clone()
        
        chains_by_type[sample].Draw(drawString2,ROOT.TCut(cutString2))
        hist_tmp2 = ROOT.gDirectory.Get(histName2)
        hist_tmp2.SetDirectory(0)
        h2 = hist_tmp2.Clone()
        
        #print("hist1 "+hist1.GetName()+" hist2 "+hist2.GetName()+" hist1a "+hist1a.GetName()+" hist2a "+hist2a.GetName())
        
        print("looking at "+hName)
        
        canvas = ROOT.TCanvas(hName, hName, 900, 900)
        upper = ROOT.TPad("upper", "upper", 0.025, 0.345, 0.995, 0.995)
        lower = ROOT.TPad("lower", "lower", 0.025, 0.025, 0.995, 0.4)
        upper.Draw()
        lower.Draw()
        lower.SetGridy()
        lower.cd().SetBottomMargin(0.3)
        upper.cd()
        ROOT.gStyle.SetOptStat(0)

        if h1.Integral() != 0:
            h1.Scale(1/h1.Integral())
        if h2.Integral() != 0:
            h2.Scale(1/h2.Integral())
        
        #print("hist1 {} hist2 {} hist1a {} hist2a {}".format(hist1.Integral(),hist2.Integral(),hist1a.Integral(),hist2a.Integral()))
        #print("h1 {} h2 {}".format(h1.Integral(),h2.Integral()))
        rat = h2.Clone()
        rat.Divide(h1)        

        y_max = max(h1.GetMaximum(), h2.GetMaximum())*1.3
        
        h1.Draw("hist")
        h1.SetTitle("")
        h1.SetAxisRange(0, y_max, "Y")
        h1.GetYaxis().SetMaxDigits(3)
        h1.GetYaxis().SetTitle("Entries")
        h1.GetYaxis().SetTitleSize(0.04)
        h1.GetYaxis().SetTitleOffset(1.2)
        h1.GetYaxis().SetLabelSize(0.05)
        h1.SetYTitle("Entries")
        #h1.SetMarkerColor(ROOT.kBlue)
        #h1.SetMarkerStyle(8)
        #h1.SetMarkerSize(1.5)
        h1.SetLineColor(ROOT.kBlue)    
        h1.SetLineWidth(2)
        h1.SetLabelOffset(5)
        #h1.GetXaxis().SetRangeUser(xLow,xHigh)
        
        h2.Draw("same hist")
        h2.SetMarkerColor(ROOT.kRed)
        #h2.SetMarkerStyle(8)
        #h2.SetMarkerSize(1.5)
        h2.SetLineColor(ROOT.kRed)
        h2.SetLineWidth(2)

        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.03)
        t.SetTextAlign(4)
        t.DrawLatex(0.15, 0.85, CUTS.split("-")[0])
        t.DrawLatex(0.15, 0.80, CUTS.split("-")[1])
        t.DrawLatex(0.15, 0.75, CUTS.split("-")[2])
        
        
        legend = ROOT.TLegend(0.575, 0.7, 0.9, 0.85)
        legend.SetHeader("              MC,    NEntries,    SoW")
        legend.AddEntry(h1, "bb, {:.1f}, {:.1f}".format(h1.GetEntries(),h1.Integral()), "l")
        legend.AddEntry(h2, "bc, {:.1f}, {:.1f}".format(h2.GetEntries(),h2.Integral()), "l")
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(0)
        legend.Draw()
        
        
        lower.cd()
        rat.SetTitle("")
        rat.GetXaxis().SetTitle(hName)
        rat.GetXaxis().SetTitleSize(0.09)
        rat.GetXaxis().SetTitleOffset(1.05)
        
        #Line = ROOT.TLine(xLow,1.,xHigh,1.)
        #Line.SetLineWidth(2)
        #Line.SetLineColor(ROOT.kBlack)
        yLow = 0
        yHigh = 2
        rat.SetLineWidth(2)
        rat.SetLineColor(ROOT.kBlack)
        rat.SetAxisRange(yLow, yHigh, "Y")
        rat.GetXaxis().SetLabelSize(0.09)
        rat.GetYaxis().SetLabelSize(0.07)
        rat.GetYaxis().SetTitleOffset(0.7)
        rat.GetYaxis().SetTitleSize(0.06)
        rat.GetYaxis().SetTitle("bc/bb")
        rat.GetYaxis().SetNdivisions(506)
        
        rat.Draw("")
        #Line.Draw("same")
        #rat.GetXaxis().SetRangeUser(xLow,xHigh)
        
        
        canvas.SaveAs(working_dir+sample+"_"+hName+"_"+histEx+".png")

        
'''

    0 :nJets     : nJets/I                                                *
    1 :nTaus     : nTaus/I                                                *
    2 :METPhi    : METPhi/I                                               *
    3 :flavB1    : flavB1/I                                               *
    4 :flavB2    : flavB2/I                                               *
    5 :flavJ3    : flavJ3/I                                               *
    6 :nbJets    : nbJets/I                                               *
    8 :nFwdJets  : nFwdJets/I                                             *
    9 :nSigJets  : nSigJets/I                                             *
   10 :nTrkJets  : nTrkJets/I                                             *
   13 :nBTrkJets : nBTrkJets/I                                            *
   14 :TTBarDecay : TTBarDecay/I                                          *
   17 :Njets_truth_pTjet30 : Njets_truth_pTjet30/I                        *
   18 :PassNonJetCountCuts : PassNonJetCountCuts/I                        *
   20 :HT        : HT/F                                                   *
   21 :MET       : MET/F                                                  *
   22 :MPT       : MPT/F                                                  *
   23 :mB1       : mB1/F                                                  *
   24 :mB2       : mB2/F                                                  *
   25 :mBB       : mBB/F                                                  *
   26 :mJ3       : mJ3/F                                                  *
   27 :pTV       : pTV/F                                                  *
   28 :dRBB      : dRBB/F                                                 *
   29 :mBBJ      : mBBJ/F                                                 *
   30 :pTB1      : pTB1/F                                                 *
   31 :pTB2      : pTB2/F                                                 *
   32 :pTBB      : pTBB/F                                                 *
   33 :pTJ3      : pTJ3/F                                                 *
   34 :etaB1     : etaB1/F                                                *
   35 :etaB2     : etaB2/F                                                *
   36 :etaBB     : etaBB/F                                                *
   37 :etaJ3     : etaJ3/F                                                *
   38 :pTBBJ     : pTBBJ/F                                                *
   39 :phiB1     : phiB1/F                                                *
   40 :phiB2     : phiB2/F                                                *
   41 :phiBB     : phiBB/F                                                *
   42 :phiJ3     : phiJ3/F                                                *
   43 :BTagSF    : BTagSF/F                                               *
   44 :GSCMbb    : GSCMbb/F                                               *
   45 :dEtaBB    : dEtaBB/F                                               *
   46 :dPhiBB    : dPhiBB/F                                               *
   47 :metSig    : metSig/F                                               *
   48 :softMET   : softMET/F                                              *
   49 :ActualMu  : ActualMu/F                                             *
   50 :MV2c10B1  : MV2c10B1/F                                             *
   51 :MV2c10B2  : MV2c10B2/F                                             *
   52 :OneMuMbb  : OneMuMbb/F                                             *
   53 :PUWeight  : PUWeight/F                                             *
   54 :mH_truth  : mH_truth/F                                             *
   55 :mV_truth  : mV_truth/F                                             *
   56 :AverageMu : AverageMu/F                                            *
   57 :JVTWeight : JVTWeight/F                                            *
   58 :PtRecoMbb : PtRecoMbb/F                                            *
   59 :TriggerSF : TriggerSF/F                                            *
   60 :pTH_truth : pTH_truth/F                                            *
   61 :pTV_truth : pTV_truth/F                                            *
   62 :sumPtJets : sumPtJets/F                                            *
   63 :LumiWeight : LumiWeight/F                                          *
   64 :TruthWZMbb : TruthWZMbb/F                                          *
   65 :dPhiMETMPT : dPhiMETMPT/F                                          *
   66 :etaH_truth : etaH_truth/F                                          *
   67 :etaV_truth : etaV_truth/F                                          *
   68 :phiH_truth : phiH_truth/F                                          *
   69 :phiV_truth : phiV_truth/F                                          *
   70 :EventWeight : EventWeight/F                                        *
   71 :bin_MV2c10B1 : bin_MV2c10B1/F                                      *
   72 :bin_MV2c10B2 : bin_MV2c10B2/F                                      *
   73 :bin_MV2c10J3 : bin_MV2c10J3/F                                      *
   74 :MCEventWeight : MCEventWeight/F                                    *
   75 :MindPhiMETJet : MindPhiMETJet/F                                    *
   76 :ActualMuScaled : ActualMuScaled/F                                  *
   77 :AverageMuScaled : AverageMuScaled/F                                *
   78 :dPhiMETdijetResolved : dPhiMETdijetResolved/F                      *
   79 :mva       : mva/D                                                  *
   80 :mvadiboson : mvadiboson/D                                          *
   81 :sample    : string                                                 *
   82 :Description : string                                               *
   83 :EventFlavor : string                                               *

'''

