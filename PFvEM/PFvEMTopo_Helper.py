"""
Contains definitions and functions
"""
import ROOT
import copy
import parse_args
#import argparse
import logging

args = parse_args.get_args()

'''
m_fit_func_2j10 = ROOT.TF1("fit_func_2j10", "[0]+exp([1]+[2]*x)", 75, 500)
m_fit_func_3j10 = ROOT.TF1("fit_func_3j10", "[0]+exp([1]+[2]*x)", 75, 500)
m_fit_func_2j95 = ROOT.TF1("fit_func_2j95", "[0]+exp([1]+[2]*x)", 75, 500)
m_fit_func_3j85 = ROOT.TF1("fit_func_3j85", "[0]+exp([1]+[2]*x)", 75, 500)

# 10% 2jet
m_fit_func_2j10.SetParameters(3.99879e-01, 7.88279e-01, -1.02303e-02)
# 95% 2jet
m_fit_func_2j95.SetParameters(8.70286e-01, 1.37867e+00, -7.95322e-03)
# 10% 3jet
m_fit_func_3j10.SetParameters(4.20840e-01, 2.67742e-01, -8.08892e-03)
# 85% 3jet
m_fit_func_3j85.SetParameters(7.63283e-01, 1.33179e+00, -7.30396e-03)

def getCR(pTV, dR):
    dRBB_min = m_fit_func_2j10.Eval(pTV)
    dRBB_max = m_fit_func_2j95.Eval(pTV)
    CR = ""
    if (dR > dRBB_max):
        CR = "CRHigh"
    elif (dR < dRBB_min):
        CR = "CRLow"
    else:
        CR = "SR"

    return CR
'''

Signals = []
Tracks = []
Truths = []
TrMatchJet = []
JetWithNoTrack = []
JetMatchTr = []
TrackWithNoJet = []
nTrackJets = 0
nBTrackJets = 0
nTracksNotMatched = 0
nSigNotMatched = 0 # num signal jets not matched to track jets
nSigMatchedBTracks = 0 # Num signal jets matched to atleast one tagged tracks
nBTracksNotMatched = 0 # num tagged track jets not matched to reco jet
nBSigNotMatched = 0 # num tagged signal jets not matched to track jets
nBSigMatchedBTrack = 0
nBJetsNotMatchedBTracks = 0 # num tagged reco jets not matched to a tagged track jet
nBTracksNotMatchedBJets = 0 # num tagged track jets not matched to a tagged reco jet
nBJetsAreTrueTags = 0
nNewBJetsAreTrueTags = 0
newBTags = []
oldBTags = []
truthBTags = []
SignalTagged = [] #boolean vector
TrackTagged = [] #boolean vector. when applying track jet selections, no btagged track jet must be left unmatched to a reco jet
TruthTagged = [] #boolean vector. when applying track jet selections, no btagged track jet must be left unmatched to a reco jet
passVROR = 0 # number of jets that passed. if 0, veto event. only valid for track jet selections
nMatchedTruthJets = 0
HCand = ROOT.TLorentzVector()
HCandGSC = ROOT.TLorentzVector()

def doTrackJetCalc(t):
    
    nSigJets = t.nSigJets #t.nJets if args.channel == "2Lep" else t.nJ
    pTV = t.pTV if args.channel == "2Lep" else t.MET
    NumBTags = t.nbJets if args.channel == "2Lep" else t.nTags
    mu = t.ActualMu
    pTB1 = t.pTB1
    OneMuJ1 = t.OneMuptJ1
    GSCJ1 = t.GSCptJ1
    PtRecoJ1 = t.PtRecoptJ1
    TruthJ1 = t.TruthWZptJ1
    GSC = t.GSCMbb
    OneMu = t.OneMuMbb
    PtReco = t.PtRecoMbb
    Truth = t.TruthWZMbb
    mBB = t.mBB
    #EW = t.EventWeight
    EW = t.MCEventWeight*t.LumiWeight

    SignalJetsPt = t.SignalJetsPt
    SignalJetsEta = t.SignalJetsEta
    SignalJetsPhi = t.SignalJetsPhi
    SignalJetsM = t.SignalJetsM
    TrackJetsPt = t.TrackJetsPt
    TrackJetsEta = t.TrackJetsEta
    TrackJetsPhi = t.TrackJetsPhi
    TrackJetsM = t.TrackJetsM
    TruthJetsPt = t.TruthJetsPt
    TruthJetsEta = t.TruthJetsEta
    TruthJetsPhi = t.TruthJetsPhi
    TruthJetsM = t.TruthJetsM

    SignalTagged = t.SignalTagged #boolean vector
    TrackTagged = t.TrackTagged #boolean vector. when applying track jet selections, no btagged track jet must be left unmatched to a reco jet
    TruthTagged = t.TruthTagged #boolean vector. when applying track jet selections, no btagged track jet must be left unmatched to a reco jet
    passVROR = t.passVROR # number of jets that passed. if 0, veto event. only valid for track jet selections
    nMatchedTruthJets = t.nMatchedTruthJets
    
    global Signals; Signals = []
    global Tracks; Tracks = []
    global Truths; Truths = []
    logging.debug("Num jets {} and tags {}".format(nSigJets,NumBTags))
    logging.debug("Signal Jets (pt,eta,phi,m)")
    for i in range(len(SignalJetsPt)):
        LSig=ROOT.TLorentzVector()
        LSig.SetPtEtaPhiM(SignalJetsPt[i]/1000, SignalJetsEta[i], SignalJetsPhi[i], SignalJetsM[i]/1000)
        logging.debug("  {}: ({}, {}, {}, {})".format(i,LSig.Pt(),LSig.Eta(),LSig.Phi(),LSig.M()))
        Signals.append(LSig)
    logging.debug("Jets tagged ")
    logging.debug(SignalTagged)
    logging.debug("Track Jets (pt,eta,phi,m)")
    for i in range(len(TrackJetsPt)):
        LTra=ROOT.TLorentzVector()
        LTra.SetPtEtaPhiM(TrackJetsPt[i], TrackJetsEta[i], TrackJetsPhi[i], TrackJetsM[i])
        logging.debug("  {}: ({}, {}, {}, {})".format(i,LTra.Pt(),LTra.Eta(),LTra.Phi(),LTra.M()))
        Tracks.append(LTra)
    logging.debug("Tracks tagged ") 
    logging.debug(TrackTagged)
    logging.debug("Truth Jets (pt,eta,phi,m)")
    for i in range(len(TruthJetsPt)):
        LTru=ROOT.TLorentzVector()
        LTru.SetPtEtaPhiM(TruthJetsPt[i]/1000, TruthJetsEta[i], TruthJetsPhi[i], TruthJetsM[i]/1000)
        logging.debug("  {}: ({}, {}, {}, {})".format(i,LTru.Pt(),LTru.Eta(),LTru.Phi(),LTru.M()))
        Truths.append(LTru)
    logging.debug("Truths tagged ") 
    logging.debug(TruthTagged)
    
    # get track jets matching to reco jets
    global TrMatchJet,JetWithNoTrack
    TrMatchJet,JetWithNoTrack = trackJetMatching(Signals, Tracks, 0.4)
    # get reco jets matching to track jets
    global JetMatchTr,TrackWithNoJet
    JetMatchTr,TrackWithNoJet = trackJetMatching(Tracks, Signals, 0.4)
    
    
    global nTrackJets; nTrackJets=len(TrackJetsPt)
    global nBTrackJets; nBTrackJets=0
    global nTracksNotMatched; nTracksNotMatched=0
    global nSigNotMatched; nSigNotMatched=0  # num signal jets not matched to track jets
    global nSigMatchedBTracks; nSigMatchedBTracks=0  # Num signal jets matched to atleast one tagged tracks
    global nBTracksNotMatched; nBTracksNotMatched=0  # num tagged track jets not matched to reco jet
    global nBSigNotMatched; nBSigNotMatched=0  # num tagged signal jets not matched to track jets
    global nBSigMatchedBTrack; nBSigMatchedBTrack=0 
    global nBJetsNotMatchedBTracks; nBJetsNotMatchedBTracks=0  # num tagged reco jets not matched to a tagged track jet
    global nBTracksNotMatchedBJets; nBTracksNotMatchedBJets=0  # num tagged track jets not matched to a tagged reco jet
    global newBTags; newBTags=[]
    global oldBTags; oldBTags=[]
    global truthBTags; truthBTags=[]
    global nBJetsAreTrueTags
    global nNewBJetsAreTrueTags
    # loop through track jets
    for ind in range(nTrackJets):
        if len(JetMatchTr[ind])==0:
            nTracksNotMatched+=1
        if TrackTagged[ind]:
            nBTrackJets+=1
            # if atleast one sig jet matched to track jet
            if len(JetMatchTr[ind])>0:
                # new btag is the sig jet closest to the b track
                if JetMatchTr[ind][0] not in newBTags:
                    newBTags.append(JetMatchTr[ind][0])
            # if track jet has no reco jet match
            if ind in TrackWithNoJet:
                nBTracksNotMatched+=1
                nBTracksNotMatchedBJets+=1
            else:
                taggedJet=0
                #get jets matched to this track jet
                for ID in JetMatchTr[ind]:
                    if SignalTagged[ID]:
                        taggedJet+=1
                if taggedJet==0:
                    nBTracksNotMatchedBJets+=1
    # sort tagged jets in terms of leading pt. Sig jet indices are already based on leading pt order
    if len(newBTags)>1:
        #print(newBTags)
        newBTags = sorted(newBTags)
    # loop through sig jets
    for ind in range(len(TrMatchJet)):
        if len(TrMatchJet[ind])==0:
            nSigNotMatched+=1
        for ID in TrMatchJet[ind]:
            if TrackTagged[ID]:
                nSigMatchedBTracks+=1
                break
        if TruthTagged[ind]:
            truthBTags.append(ind)
        if SignalTagged[ind]:
            oldBTags.append(ind)
            if ind in JetWithNoTrack:
                nBJetsNotMatchedBTracks+=1
            else:
                taggedJet=0
                for ID in TrMatchJet[ind]:
                    if TrackTagged[ID]:
                        taggedJet+=1
                if taggedJet==0:
                    nBJetsNotMatchedBTracks+=1
                else:
                    nBSigMatchedBTrack+=1
    nBJetsAreTrueTags = 0
    nNewBJetsAreTrueTags = 0
    for T in truthBTags:
        if T in oldBTags:
            nBJetsAreTrueTags+=1
        if T in newBTags:
            nNewBJetsAreTrueTags+=1

    logging.debug("Tracks matched to jets")
    logging.debug(TrMatchJet)
    logging.debug("Jets with no tracks")
    logging.debug(JetWithNoTrack)
    logging.debug("Jets matched to tracks")
    logging.debug(JetMatchTr)
    logging.debug("Tracks with no jets")
    logging.debug(TrackWithNoJet)
    logging.debug("Num track jets {}".format(nTrackJets))
    logging.debug("Num tagged track jets {}".format(nBTrackJets))
    logging.debug("Num tagged tracks with no match to reco jets {}".format(nBTracksNotMatched))
    logging.debug("Num new tag jets {}".format(nSigMatchedBTracks))
    logging.debug("Old b tagged jets {}".format(oldBTags))
    logging.debug("New b tagged jets {}".format(newBTags))
    logging.debug("True b tagged jets {}".format(truthBTags))
    logging.debug("nBJetsAreTrueTags {} diff with truth {}".format(nBJetsAreTrueTags, len(truthBTags) - nBJetsAreTrueTags ))
    logging.debug("nNewBJetsAreTrueTags {} diff with truth {}".format(nNewBJetsAreTrueTags, len(truthBTags) - nNewBJetsAreTrueTags))
    logging.debug("nBTracksNotMatchedBJets {}".format(nBTracksNotMatchedBJets))
    logging.debug("nBJetsNotMatchedBTracks {}".format(nBJetsNotMatchedBTracks))
    logging.debug("nSigNotMatched {}".format(nSigNotMatched))
    logging.debug("nBSigMatchedBTrack {}".format(nBSigMatchedBTrack))
    

pi=3.14159265358979323846
# Functions
def cuts2Lep(t, cutFlow, cutFlow_w, trackSelection, trackVars):
    p = 0
    #EW = t.EventWeight
    EW = t.MCEventWeight*t.LumiWeight
    numBJets = t.nbJets if not trackSelection else trackVars[0]
    pTB1 = t.pTB1 if not trackSelection else trackVars[1]
    if t.pTV > 75:
        cutFlow["pTV>75"]+=1
        cutFlow_w["pTV>75"]+=EW
        p=-1
        if t.nJets >=2:
            cutFlow["numJets>=2"]+=1
            cutFlow_w["numJets>=2"]+=EW
            p=-2
            if t.nSigJets>=2:
                cutFlow["nSigJets>=2"]+=1
                cutFlow_w["nSigJets>=2"]+=EW
                p=-3
                if pTB1 > 45:
                    cutFlow["J1pt>45"]+=1
                    cutFlow_w["J1pt>45"]+=EW
                    p=-4
                    if numBJets==2:
                        cutFlow["2BJets"]+=1
                        cutFlow_w["2BJets"]+=EW
                        p=-5
                        if t.mLL > 81 and t.mLL < 101:
                            cutFlow["mLL"]+=1
                            cutFlow_w["mLL"]+=EW
                            p=-6
                            if trackSelection:
                                if t.passVROR != 0:
                                    cutFlow["passVROR"]+=1
                                    cutFlow_w["passVROR"]+=EW
                                    p=-7
                                    if nBTracksNotMatched ==0:
                                        cutFlow["BTracksNotMatched==0"]+=1
                                        cutFlow_w["BTracksNotMatched==0"]+=EW
                                        p=1
                                        
                            else:
                                p=1
    return p


def cuts0Lep(t, cutFlow, cutFlow_w, trackSelection, trackVars):
    #if trackSelection: print("Trackvars nBJets {} pTB1 {} dPhiBB {} dPhiMETBB {}".format(trackVars[0],trackVars[1],trackVars[2],trackVars[3]))
    p = 0
    #EW = t.EventWeight
    EW = t.MCEventWeight*t.LumiWeight
    numBJets = t.nTags if not trackSelection else trackVars[0]
    #print("In cuts tracks {} numBJets {}".format(trackSelection,numBJets))
    pTB1 = t.pTB1 if not trackSelection else trackVars[1]
    dPhiBB = t.dPhiBB if not trackSelection else trackVars[2]
    dPhiMETBB = t.dPhiMETdijet if not trackSelection else trackVars[3]    
    if t.MET>=150:
        cutFlow["MET>150"]+=1
        cutFlow_w["MET>150"]+=EW
        p=-1
        if t.nJ >=2:
            cutFlow["numJets>=2"]+=1
            cutFlow_w["numJets>=2"]+=EW
            p=-2
            if t.nSigJets>=2:
                cutFlow["nSigJets>=2"]+=1
                cutFlow_w["nSigJets>=2"]+=EW
                p=-3
                if pTB1 > 45:
                    cutFlow["J1pt>45"]+=1
                    cutFlow_w["J1pt>45"]+=EW
                    p=-4
                    if numBJets==2:
                        cutFlow["2BJets"]+=1
                        cutFlow_w["2BJets"]+=EW
                        p=-5
                        if abs(dPhiMETBB)>120*pi/180:
                            cutFlow["dPhiMETBB"]+=1
                            cutFlow_w["dPhiMETBB"]+=EW
                            p=-6
                            if abs(dPhiBB)<140*pi/180:
                                cutFlow["dPhiBB"]+=1
                                cutFlow_w["dPhiBB"]+=EW
                                p=-7
                                if abs(t.dPhiMETMPT)<90*pi/180:
                                    cutFlow["dPhiMETMPT"]+=1
                                    cutFlow_w["dPhiMETMPT"]+=EW
                                    p=-8
                                    if t.nJ==2:
                                        if abs(t.MindPhiMETJet)>20*pi/180:
                                            cutFlow["MinDPhiMETJet"]+=1
                                            cutFlow_w["MinDPhiMETJet"]+=EW
                                            p=-9
                                            if t.sumPtJets>120:
                                                cutFlow["HT"]+=1
                                                cutFlow_w["HT"]+=EW
                                                p=-10
                                                if trackSelection:
                                                    if t.passVROR != 0:
                                                        cutFlow["passVROR"]+=1
                                                        cutFlow_w["passVROR"]+=EW
                                                        p=-11
                                                        if nBTracksNotMatched ==0:
                                                            cutFlow["BTracksNotMatched==0"]+=1
                                                            cutFlow_w["BTracksNotMatched==0"]+=EW
                                                            p=1
                                                        
                                                else:
                                                    p=1
                                    elif t.nJ>=3:
                                        if abs(t.MindPhiMETJet)>30*pi/180:
                                            cutFlow["MinDPhiMETJet"]+=1
                                            cutFlow_w["MinDPhiMETJet"]+=EW
                                            p=-9
                                            if t.sumPtJets>150:
                                                cutFlow["HT"]+=1
                                                cutFlow_w["HT"]+=EW
                                                p=-10
                                                if trackSelection:
                                                    if t.passVROR != 0:
                                                        cutFlow["passVROR"]+=1
                                                        cutFlow_w["passVROR"]+=EW
                                                        p=-11
                                                        if nBTracksNotMatched ==0:
                                                            cutFlow["BTracksNotMatched==0"]+=1
                                                            cutFlow_w["BTracksNotMatched==0"]+=EW
                                                            p=1
                                                else:
                                                    p=1
    #print(p)
    return p

def getTrackVars(t):#Signals, newTags, METphi):
    doTrackJetCalc(t)
    #print("AFTER doTrackJetCalc")
    #print(newBTags)
    numBJets = len(newBTags)
    pTB1 = 0
    dPhiBB = 0
    dPhiMETBB = 0
    J1 = ROOT.TLorentzVector()
    J2 = ROOT.TLorentzVector()
    J1GSC = ROOT.TLorentzVector()
    J2GSC = ROOT.TLorentzVector()
    Higgs = ROOT.TLorentzVector()
    global HCand
    global HCandGSC
    if len(Signals)==1:
        J1.SetPtEtaPhiM(t.TrackTaggedptJ1, t.TrackTaggedetaJ1, t.TrackTaggedphiJ1, t.TrackTaggedmJ1)
        J1GSC=Signals[0]
        pTB1 = J1.Pt()
        if args.channel=="2Lep":
            return [numBJets, pTB1]
        dPhiBB = t.dPhitBB
        dPhiMETBB = t.dPhiMETtdijet
        HCand=J1
        HCandGSC=J1GSC
        return [numBJets, pTB1, dPhiBB, dPhiMETBB]
    elif len(Signals)>=2:
        J1.SetPtEtaPhiM(t.TrackTaggedptJ1, t.TrackTaggedetaJ1, t.TrackTaggedphiJ1, t.TrackTaggedmJ1)
        J2.SetPtEtaPhiM(t.TrackTaggedptJ2, t.TrackTaggedetaJ2, t.TrackTaggedphiJ2, t.TrackTaggedmJ2)
      
        # Follows the AllSignalJets ordering of the jets. H candidate is always the 2 leading jets
        if numBJets==0:
            J1GSC=Signals[0]
            J2GSC=Signals[1]
        elif numBJets==1:
            J1GSC=Signals[0]
            J2GSC=Signals[1] if newBTags[0]==0 else Signals[newBTags[0]]
        elif numBJets>=2:
            J1GSC=Signals[newBTags[0]]
            J2GSC=Signals[newBTags[1]]
    
    HCand=J1+J2
    HCandGSC=J1GSC+J2GSC
    
    #print(HCand.Pt())
    
    pTB1 = J1.Pt()
    if args.channel=="2Lep":
          return [numBJets, pTB1]
    dPhiBB = t.dPhitBB
    dPhiMETBB = t.dPhiMETtdijet

    return [numBJets, pTB1, dPhiBB, dPhiMETBB]

def dRMatching(L1, L2, R):
    dR = L1.DeltaR(L2)
    if dR<R:
        return True
    return False

def getTrueHCand(TruthTagged):
    J1 = ROOT.TLorentzVector()
    J2 = ROOT.TLorentzVector()
    trpt = [J.Pt() for J in Truths]
    Truths2 = [J for _,J in sorted(zip(trpt,Truths), reverse=True)]
    TruthTagged2 = [J for _,J in sorted(zip(trpt,TruthTagged), reverse=True)]
    LeadB = -1
    subLeadB = -1
    for i in range(len(TruthTagged)):
        if TruthTagged[i]==1:
            if LeadB==-1:
                LeadB=i
            else:
                subLeadB=i
                break
    if len(Signals)==1:
        J1=Truths2[0]
        #print("true pTBB {} mBB {} PhiBB {} dPhiBB {}".format(J1.Pt()/1000,J1.M()/1000, J1.Phi(), J1.Phi()))
    elif len(truthBTags)==0:
        J1=Truths2[0]
        J2=Truths2[1]
    elif len(truthBTags)==1:
        J1=Truths2[0]
        J2=Truths2[1] if TruthTagged2[0]==1 else Truths[LeadB]
    elif len(truthBTags)>=2:
        J1=Truths[LeadB]
        J2=Truths[subLeadB]
    return J1,J2
    #print("true pTBB {} mBB {} PhiBB {} dPhiBB {}".format((J1+J2).Pt()/1000,(J1+J2).M()/1000, (J1+J2).Phi(), abs(J1.DeltaPhi(J2))))

    
        

# gets jets matched to other type of jets based on dR
# orders the matches in order of smallest dR
def trackJetMatching(jets, tracks, R):
    matches = []
    unmatched = []
    for jet in jets:
        tr = []
        dRl = []
        for j in range(len(tracks)):
            dR = jet.DeltaR(tracks[j])
            #print("j {} dr {}".format(j, dR))
            if dR < R:
                dRl.append(dR)
                tr.append(j)
        # Put in ascending order of dR
        if len(tr)>1:
            tr = [x for _,x in sorted(zip(dRl,tr))]
        matches.append(tr)
        #print(matches)
        #print("")
    for i in range(len(matches)):
        if len(matches[i])==0:
            unmatched.append(i)
    return matches,unmatched

#def getNewVariables(t):
    

# Histograms
histos = { "nSigJets" : ROOT.TH1D("nSigJets","nSigJets",10,0,10),
           "nTrackJets" : ROOT.TH1D("nTrackJets","nTrackJets",10,0,10),
           "nBTrackJets" : ROOT.TH1D("nBTrackJets","nBTrackJets",10,0,10),
           "nBTags" : ROOT.TH1D("nBTags","nBTags",10,0,10),
           "nNewBTags" : ROOT.TH1D("nNewBTags","nNewBTags",10,0,10),
           "nBTrackJetsDIFFnewBTags" : ROOT.TH1D("nBTrackJetsDIFFnewBTags","nBTrackJetsDIFFnewBTags",10,-5,5),
           "nTrueBTags" : ROOT.TH1D("nTrueBTags","nTrueBTags",10,0,10),
           "nBSigMatchedBTrack" : ROOT.TH1D("nBSigMatchedBTrack","nBSigMatchedBTrack",10,0,10),
           "nSigMatchedBTracks" : ROOT.TH1D("nSigMatchedBTracks","nSigMatchedBTracks",10,0,10),
           "nTracksNotMatched" : ROOT.TH1D("nTracksNotMatched","nTracksNotMatched",10,0,10),
           "nSigNotMatched" : ROOT.TH1D("nSigNotMatched","nSigNotMatched",10,0,10),
           "nBTracksNotMatched" : ROOT.TH1D("nBTracksNotMatched","nBTracksNotMatched",10,0,10),
           "nBSigNotMatched" : ROOT.TH1D("nBSigNotMatched","nBSigNotMatched",10,0,10),
           "nBJetsNotMatchedBTracks" : ROOT.TH1D("nBJetsNotMatchedBTracks","nBJetsNotMatchedBTracks",10,0,10),
           "nBTracksNotMatchedBJets" : ROOT.TH1D("nBTracksNotMatchedBJets","nBTracksNotMatchedBJets",10,0,10),
           "nMatchedTruthJets" : ROOT.TH1D("nMatchedTruthJets","nMatchedTruthJets",10,0,10),
           "nRecoMatchToATrack" : ROOT.TH1D("nRecoMatchToATrack","nRecoMatchToATrack",10,0,10),
           "nTrackMatchToAReco" : ROOT.TH1D("nTrackMatchToAReco","nTrackMatchToAReco",10,0,10),
           "nBTrackMatchToAReco" : ROOT.TH1D("nBTrackMatchToAReco","nBTrackMatchToAReco",10,0,10),
           "nTrueTagsNotTagged" : ROOT.TH1D("nTrueTagsNotTagged","nTrueTagsNotTagged",5,0,5),
           "nTrueTagsNotNewTagged" : ROOT.TH1D("nTrueTagsNotNewTagged","nTrueTagsNotNewTagged",5,0,5),
           "dRMatchedSigTracks" : ROOT.TH1D("dRMatchedSigTracks","dRMatchedSigTracks",50,0,0.5),
           "dRMatchedSigBTracks" : ROOT.TH1D("dRMatchedSigBTracks","dRMatchedSigBTracks",50,0,0.5),
           "GSCMbb" : ROOT.TH1D("GSCMbb","GSCMbb",250,0,250), 
           "OneMuMbb" : ROOT.TH1D("OneMuMbb","OneMuMbb",250,0,250),
           "PtRecoMbb" : ROOT.TH1D("PtRecoMbb","PtRecoMbb",250,0,250),
           "TruthWZMbb" : ROOT.TH1D("TruthWZMbb","TruthWZMbb",250,0,250),
           "TrueBMbb" : ROOT.TH1D("TrueBMbb","TrueBMbb",250,0,250),
           "Mbb" : ROOT.TH1D("Mbb","Mbb",250,0,250),
           "pTBB" : ROOT.TH1D("pTBB","pTBB",400,0,400),
           "dPhiBB" : ROOT.TH1D("dPhiBB","dPhiBB",100,0,3.2),
           "dRBB" : ROOT.TH1D("dRBB","dRBB",500,0,5),
           "dEtaBB" : ROOT.TH1D("dEtaBB","dEtaBB",100,0,5),
           "TrackMbb" : ROOT.TH1D("TrackMbb","TrackMbb",250,0,250),
           "TrackGSCMbb" : ROOT.TH1D("TrackGSCMbb","TrackGSCMbb",250,0,250),
           "TrackpTBB" : ROOT.TH1D("TrackpTBB","TrackpTBB",400,0,400),
           "TrackdPhiBB" : ROOT.TH1D("TrackdPhiBB","TrackdPhiBB",100,0,3.2),
           #"TrackdRBB" : ROOT.TH1D("TrackdRBB","TrackdRBB",500,0,5),
           #"TrackdEtaBB" : ROOT.TH1D("TrackdEtaBB","TrackdEtaBB",100,0,5),
           "dPhiMETdijet" : ROOT.TH1D("dPhiMETdijet","dPhiMETdijet",100,0,3.2),
           "TrackdPhiMETdijet" : ROOT.TH1D("TrackdPhiMETdijet","TrackdPhiMETdijet",100,0,3.2),
           "pTV" : ROOT.TH1D("pTV","pTV",400,0,400),
           "pTJ1" : ROOT.TH1D("pTJ1","pTJ1",400,0,400),
           "pTJ2" : ROOT.TH1D("pTJ2","pTJ2",400,0,400),
           "pTJt1" : ROOT.TH1D("pTJt1","pTJt1",400,0,400),
           "pTJt2" : ROOT.TH1D("pTJt2","pTJt2",400,0,400),
           "EtaJ1" : ROOT.TH1D("EtaJ1","EtaJ1",50,-2.5,2.5),
           "EtaJ2" : ROOT.TH1D("EtaJ2","EtaJ2",50,-2.5,2.5),
           "EtaJt1" : ROOT.TH1D("EtaJt1","EtaJt1",50,-2.5,2.5),
           "EtaJt2" : ROOT.TH1D("EtaJt2","EtaJt2",50,-2.5,2.5),           
           "pTTrackJ2" : ROOT.TH1D("pTTrackJ2","pTTrackJ2",400,0,400),
           "sumnSigJets_mu" : ROOT.TH1D("sumnSigJets_mu","sumnSigJets_mu",100,0,100),
           "nMu" : ROOT.TH1D("nMu","nMu",100,0,100),
           "sumGSCMbb_mu" : ROOT.TH1D("sumGSCMbb_mu","sumGSCMbb_mu",100,0,100),
           "nPtTrue_Final" : ROOT.TH1D("nPtTrue_Final","nPtTrue_Final",400,0,400),
           "nPtTrue_PtReco" : ROOT.TH1D("nPtTrue_PtReco","nPtTrue_PtReco",400,0,400),
           "nPtTrue_GSC" : ROOT.TH1D("nPtTrue_GSC","nPtTrue_GSC",400,0,400),
           "nPtTrue_OneMu" : ROOT.TH1D("nPtTrue_OneMu","nPtTrue_OneMu",400,0,400),
           "sumnPtTrue_Final" : ROOT.TH1D("sumnPtTrue_Final","sumnPtTrue_Final",400,0,400),
           "sumnPtTrue_PtReco" : ROOT.TH1D("sumnPtTrue_PtReco","sumnPtTrue_PtReco",400,0,400),
           "sumnPtTrue_OneMu" : ROOT.TH1D("sumnPtTrue_OneMu","sumnPtTrue_OneMu",400,0,400),
           "sumnPtTrue_GSC" : ROOT.TH1D("sumnPtTrue_GSC","sumnPtTrue_GSC",400,0,400),
           "dRTruth_GSC" : ROOT.TH1D("dRTruth_GSC","dRTruth_GSC",500,0,5),
           "dRTruth_OneMu" : ROOT.TH1D("dRTruth_OneMu","dRTruth_OneMu",500,0,5),
           "dRTruth_PtReco" : ROOT.TH1D("dRTruth_PtReco","dRTruth_PtReco",500,0,5),
           "dRTruth_Final" : ROOT.TH1D("dRTruth_Final","dRTruth_Final",500,0,5),
           "dRJ1TB1" : ROOT.TH1D("dRJ1TB1","dRJ1TB1",500,0,5),  # DR between the two tags and the truth b jets
           "dRJ2TB2" : ROOT.TH1D("dRJ2TB2","dRJ2TB2",500,0,5),
           "dRJt1TB1" : ROOT.TH1D("dRJt1TB1","dRJt1TB1",500,0,5),
           "dRJt2TB2" : ROOT.TH1D("dRJt2TB2","dRJt2TB2",500,0,5),
           "dPtTruth_GSC" : ROOT.TH1D("dPtTruth_GSC","dPtTruth_GSC",200,-100,100),
           "dPtTruth_Final" : ROOT.TH1D("dPtTruth_Final","dPtTruth_Final",200,-100,100),
           "dPtJ1TB1" : ROOT.TH1D("dPtJ1TB1","dPtJ1TB1",200,-100,100),
           "dPtJ2TB2" : ROOT.TH1D("dPtJ2TB2","dPtJ2TB2",200,-100,100),
           "dPtJt1TB1" : ROOT.TH1D("dPtJt1TB1","dPtJt1TB1",200,-100,100),
           "dPtJt2TB2" : ROOT.TH1D("dPtJt2TB2","dPtJt2TB2",200,-100,100),
           #"dRTruth_zoomed_Final" : ROOT.TH1D("dRTruth_Final","dRTruth_Final",500,0,0.1)
           

}


hist_regions = {}
if args.channel == "2Lep":
    hist_regions = {"inc" : {}, #"inc_semi" : {}, "inc_had" : {}, "inc_HS" : {}, "inc_PU" : {},
                    "inc_2SigJet" : {}, #"inc_2jet_semi" : {}, "inc_2jet_had" : {}, "inc_2jet_HS" : {}, "inc_2jet_PU" : {},
                    "inc_2s&3j" : {},
                    
                    "1jet_inc" : {},
                    "1jet_75ptv" : {}, #"1jet_75ptv_semi" : {}, "1jet_75ptv_had" : {}, "1jet_75ptv_HS" : {}, "1jet_75ptv_PU" : {}, 
                    "1jet_150ptv" : {},# "1jet_150ptv_semi" : {}, "1jet_150ptv_had" : {}, "1jet_150ptv_HS" : {}, "1jet_150ptv_PU" : {}, 
                    "1jet_250ptv" : {}, #"1jet_250ptv_semi" : {}, "1jet_250ptv_had" : {}, "1jet_250ptv_HS" : {}, "1jet_250ptv_PU" : {}, 
                    "1jet_400ptv" : {}, #"1jet_400ptv_semi" : {}, "1jet_400ptv_had" : {}, "1jet_400ptv_HS" : {}, "1jet_400ptv_PU" : {}, 

                    "2jet_inc" : {},
                    "2jet_75ptv" : {}, #"2jet_75ptv_semi" : {}, "2jet_75ptv_had" : {}, "2jet_75ptv_HS" : {}, "2jet_75ptv_PU" : {}, 
                    "2jet_150ptv" : {}, #"2jet_150ptv_semi" : {}, "2jet_150ptv_had" : {}, "2jet_150ptv_HS" : {}, "2jet_150ptv_PU" : {}, 
                    "2jet_250ptv" : {}, #"2jet_250ptv_semi" : {}, "2jet_250ptv_had" : {}, "2jet_250ptv_HS" : {}, "2jet_250ptv_PU" : {}, 
                    "2jet_400ptv" : {}, #"2jet_400ptv_semi" : {}, "2jet_400ptv_had" : {}, "2jet_400ptv_HS" : {}, "2jet_400ptv_PU" : {}, 
                    
                    "3jet_inc" : {},
                    "3jet_75ptv" : {}, #"3jet_75ptv_semi" : {}, "3jet_75ptv_had" : {}, "3jet_75ptv_HS" : {}, "3jet_75ptv_PU" : {}, 
                    "3jet_150ptv" : {}, #"3jet_150ptv_semi" : {}, "3jet_150ptv_had" : {}, "3jet_150ptv_HS" : {}, "3jet_150ptv_PU" : {}, 
                    "3jet_250ptv" : {}, #"3jet_250ptv_semi" : {}, "3jet_250ptv_had" : {}, "3jet_250ptv_HS" : {}, "3jet_250ptv_PU" : {}, 
                    "3jet_400ptv" : {}, #"3jet_400ptv_semi" : {}, "3jet_400ptv_had" : {}, "3jet_400ptv_HS" : {}, "3jet_400ptv_PU" : {}, 
                    
                    "4jet_inc" : {},
                    "4jet_75ptv" : {}, #"4jet_75ptv_semi" : {}, "4jet_75ptv_had" : {}, "4jet_75ptv_HS" : {}, "4jet_75ptv_PU" : {}, 
                    "4jet_150ptv" : {}, #"4jet_150ptv_semi" : {}, "4jet_150ptv_had" : {}, "4jet_150ptv_HS" : {}, "4jet_150ptv_PU" : {}, 
                    "4jet_250ptv" : {}, #"4jet_250ptv_semi" : {}, "4jet_250ptv_had" : {}, "4jet_250ptv_HS" : {}, "4jet_250ptv_PU" : {}, 
                    "4jet_400ptv" : {}, #"4jet_400ptv_semi" : {}, "4jet_400ptv_had" : {}, "4jet_400ptv_HS" : {}, "4jet_400ptv_PU" : {}, 
                    
                    
    }
    
elif args.channel == "0Lep":
    hist_regions = {"inc" : {}, #"inc_semi" : {}, "inc_had" : {}, "inc_HS" : {}, "inc_PU" : {},
                    "inc_2SigJet" : {}, #"inc_2jet_semi" : {}, "inc_2jet_had" : {}, "inc_2jet_HS" : {}, "inc_2jet_PU" : {},
                    "inc_2s&3j" : {},

                    "1jet_inc" : {},
                    "1jet_150ptv" : {}, #"1jet_150ptv_semi" : {}, "1jet_150ptv_had" : {}, "1jet_150ptv_HS" : {}, "1jet_150ptv_PU" : {}, 
                    "1jet_250ptv" : {}, #"1jet_250ptv_semi" : {}, "1jet_250ptv_had" : {}, "1jet_250ptv_HS" : {}, "1jet_250ptv_PU" : {}, 
                    "1jet_400ptv" : {}, #"1jet_400ptv_semi" : {}, "1jet_400ptv_had" : {}, "1jet_400ptv_HS" : {}, "1jet_400ptv_PU" : {}, 

                    "2jet_inc" : {},
                    "2jet_150ptv" : {}, #"2jet_150ptv_semi" : {}, "2jet_150ptv_had" : {}, "2jet_150ptv_HS" : {}, "2jet_150ptv_PU" : {}, 
                    "2jet_250ptv" : {}, #"2jet_250ptv_semi" : {}, "2jet_250ptv_had" : {}, "2jet_250ptv_HS" : {}, "2jet_250ptv_PU" : {}, 
                    "2jet_400ptv" : {}, #"2jet_400ptv_semi" : {}, "2jet_400ptv_had" : {}, "2jet_400ptv_HS" : {}, "2jet_400ptv_PU" : {}, 
                    
                    "3jet_inc" : {},
                    "3jet_150ptv" : {}, #"3jet_150ptv_semi" : {}, "3jet_150ptv_had" : {}, "3jet_150ptv_HS" : {}, "3jet_150ptv_PU" : {}, 
                    "3jet_250ptv" : {}, #"3jet_250ptv_semi" : {}, "3jet_250ptv_had" : {}, "3jet_250ptv_HS" : {}, "3jet_250ptv_PU" : {}, 
                    "3jet_400ptv" : {}, #"3jet_400ptv_semi" : {}, "3jet_400ptv_had" : {}, "3jet_400ptv_HS" : {}, "3jet_400ptv_PU" : {}, 
                    
                    "4jet_inc" : {},
                    "4jet_150ptv" : {},
                    "4jet_250ptv" : {},
                    "4jet_400ptv" : {},
                    
    }
                    
    
hist_regions_w = copy.deepcopy(hist_regions)
hist_regions_cuts_w = copy.deepcopy(hist_regions)
hist_regions_cuts = copy.deepcopy(hist_regions)
hist_regions_cuts_w_T = copy.deepcopy(hist_regions)
hist_regions_cuts_T = copy.deepcopy(hist_regions)
noWeightsHistos = ["nSigJets", "sumnSigJets_mu", "nMu", "nTrackJets", "nBTrackJets", "nBTags", "nNewBTags", "nTrueBTags", "nMatchedTruthJets", "nRecoMatchToATrack", 
                   "nTrackMatchToAReco", "nBTrackMatchToAReco", "nTracksNotMatched", "nSigNotMatched", "nBTracksNotMatched", "nBSigNotMatched", 
                   "nBJetsNotMatchedBTracks", "nBTracksNotMatchedBJets", "nBSigMatchedBTrack", "nSigMatchedBTracks", "dRMatchedSigBTracks", "dRMatchedSigTracks"]

for reg in hist_regions:
    for name, hist in histos.items():
        hist_regions_w[reg][name] = hist.Clone(reg+"_"+name+"_w") 
        hist_regions_cuts_w[reg][name] = hist.Clone(reg+"_"+name+"_cuts_w") 
        hist_regions_cuts_w_T[reg][name] = hist.Clone(reg+"_"+name+"_cuts_w_T") 
        if name in noWeightsHistos:
            hist_regions[reg][name] = hist.Clone(reg+"_"+name) 
            hist_regions_cuts[reg][name] = hist.Clone(reg+"_"+name+"_cuts")
            hist_regions_cuts_T[reg][name] = hist.Clone(reg+"_"+name+"_cuts_T")

#Following according to selected jets        
# The 2 btags
LB1=ROOT.TLorentzVector()
LB2=ROOT.TLorentzVector()
# 2 new tags
LBt1=ROOT.TLorentzVector()
LBt2=ROOT.TLorentzVector()
# following are of LB1: truth, GSC, onemu, ptreco
LT1=ROOT.TLorentzVector()
LG1=ROOT.TLorentzVector()
LM1=ROOT.TLorentzVector()
LP1=ROOT.TLorentzVector()
# 2 tags of the truth jets
LTB1=ROOT.TLorentzVector()
LTB2=ROOT.TLorentzVector()

#main
def fill_histos(hist, histW, t, calls): #numJets, pTV, mu, pTB1, PtRecoJ1, TruthJ1, GSC, OneMu, PtReco, Truth, mBB, pTBB, EW, LB1, LG1, LM1, LP1, LT1):
    nSigJets = t.nSigJets #t.nJets if args.channel == "2Lep" else t.nJ
    pTV = t.pTV if args.channel == "2Lep" else t.MET
    NumBTags = t.nbJets if args.channel == "2Lep" else t.nTags
    mu = t.ActualMu
    pTB1 = t.pTB1
    OneMuJ1 = t.OneMuptJ1
    GSCJ1 = t.GSCptJ1
    PtRecoJ1 = t.PtRecoptJ1
    TruthJ1 = t.TruthWZptJ1
    GSC = t.GSCMbb
    OneMu = t.OneMuMbb
    PtReco = t.PtRecoMbb
    Truth = t.TruthWZMbb
    mBB = t.mBB
    #EW = t.EventWeight
    EW = t.MCEventWeight*t.LumiWeight

    LB1.SetPtEtaPhiM(t.pTB1,t.etaB1,t.phiB1,t.mB1)
    LB2.SetPtEtaPhiM(t.pTB2,t.etaB2,t.phiB2,t.mB2)
    LT1.SetPtEtaPhiM(t.TruthWZptJ1,t.TruthWZetaJ1,t.TruthWZphiJ1,t.TruthWZmJ1)
    LG1.SetPtEtaPhiM(t.GSCptJ1,t.GSCetaJ1,t.GSCphiJ1,t.GSCmJ1)
    LM1.SetPtEtaPhiM(t.OneMuptJ1,t.OneMuetaJ1,t.OneMuphiJ1,t.OneMumJ1)
    LP1.SetPtEtaPhiM(t.PtRecoptJ1,t.PtRecoetaJ1,t.PtRecophiJ1,t.PtRecomJ1)
    LBt1.SetPtEtaPhiM(t.TrackTaggedptJ1, t.TrackTaggedetaJ1, t.TrackTaggedphiJ1, t.TrackTaggedmJ1)
    LBt2.SetPtEtaPhiM(t.TrackTaggedptJ2, t.TrackTaggedetaJ2, t.TrackTaggedphiJ2, t.TrackTaggedmJ2)
    pTBB = (LB1+LB2).Pt()


    SignalTagged = t.SignalTagged #boolean vector
    TrackTagged = t.TrackTagged #boolean vector. when applying track jet selections, no btagged track jet must be left unmatched to a reco jet
    TruthTagged = t.TruthTagged #boolean vector. when applying track jet selections, no btagged track jet must be left unmatched to a reco jet
    passVROR = t.passVROR # number of jets that passed. if 0, veto event. only valid for track jet selections
    nMatchedTruthJets = t.nMatchedTruthJets

    LTB1,LTB2 = getTrueHCand(TruthTagged)
    TrackJ2pT = 0 if len(Tracks)<2 else Tracks[1].Pt()

    # per jet variables
    # loop through sig jets
    for i in range(len(TrMatchJet)):
        histW["nTrackMatchToAReco"].Fill(len(TrMatchJet[i]), EW)
        #hist["nTrackMatchToAReco"].Fill(len(TrMatchJet[i]))
        #print("nTrackMatchToAReco {}".format(len(TrMatchJet[i])))
        nBTrackMatchToAReco = 0
        # loop through tracks matched to this sig jet
        for j in TrMatchJet[i]:
            dR = Signals[i].DeltaR(Tracks[j])
            histW["dRMatchedSigTracks"].Fill(dR, EW)
            #hist["dRMatchedSigTracks"].Fill(dR)
            if TrackTagged[j]:
                histW["dRMatchedSigBTracks"].Fill(dR, EW)
                #hist["dRMatchedSigBTracks"].Fill(dR)
                nBTrackMatchToAReco+=1
            
        histW["nBTrackMatchToAReco"].Fill(nBTrackMatchToAReco, EW)
        #hist["nBTrackMatchToAReco"].Fill(nBTrackMatchToAReco)
        #print("nBTrackMatchToAReco {}".format(nBTrackMatchToAReco))
    # loop through track jets    
    for i in range(len(JetMatchTr)):
        histW["nRecoMatchToATrack"].Fill(len(JetMatchTr[i]), EW)
        #hist["nRecoMatchToATrack"].Fill(len(JetMatchTr[i]))
        #print("nRecoMatchToATrack {}".format(len(JetMatchTr[i])))
        if len(JetMatchTr[i])>1:
            JetMatchTr[i]=[JetMatchTr[i][0]]
            #print(JetMatchTr[i])


    higgsPT = HCand.Pt()
    higgsM = HCand.M()

    if calls==0:
        logging.debug("old pT1 {} eta1 {} phi1 {} m1 {}".format(LB1.Pt(), LB1.Eta(), LB1.Phi(), LB1.M()))
        logging.debug("old pT2 {} eta2 {} phi2 {} m2 {}".format(LB2.Pt(), LB2.Eta(), LB2.Phi(), LB2.M()))
        logging.debug("old pTBB {} mBB {} PhiBB {} dPhiBB {}".format(pTBB,mBB, (LB1+LB2).Phi(), t.dPhiBB))
        logging.debug("new pT1 {} eta1 {} phi1 {} m1 {}".format(LBt1.Pt(), LBt1.Eta(), LBt1.Phi(), LBt1.M()))
        logging.debug("new pT2 {} eta2 {} phi2 {} m2 {}".format(LBt2.Pt(), LBt2.Eta(), LBt2.Phi(), LBt2.M()))
        logging.debug("new pTBB {} mBB {} PhiBB {} dPhiBB {}".format((LBt1+LBt2).Pt(), (LBt1+LBt2).M(), (LBt1+LBt2).Phi(), LBt1.DeltaPhi(LBt2)))
        logging.debug("HCand pTBB {} mBB {} PhiBB {} dPhiBB {}".format(HCand.Pt(), HCand.M(), HCand.Phi(), LBt1.DeltaPhi(LBt2)))
        logging.debug("tru pT1 {} eta1 {} phi1 {} m1 {}".format(LTB1.Pt(), LTB1.Eta(), LTB1.Phi(), LTB1.M()))
        logging.debug("tru pT2 {} eta2 {} phi2 {} m2 {}".format(LTB2.Pt(), LTB2.Eta(), LTB2.Phi(), LTB2.M()))
        logging.debug("tru pTBB {} mBB {} PhiBB {} dPhiBB {}".format((LTB1+LTB2).Pt(), (LTB1+LTB2).M(), (LTB1+LTB2).Phi(), LTB1.DeltaPhi(LTB2)))
        logging.debug("GSC pt1 {} eta1 {} phi1 {} m1 {}".format(LG1.Pt(), LG1.Eta(), LG1.Phi(), LG1.M()))



    #print("GSCMbb {} TrackGSCMbb {}".format(GSC, HCandGSC.M()))
    histW["nBSigMatchedBTrack"].Fill(nBSigMatchedBTrack, EW)
    histW["nSigMatchedBTracks"].Fill(nSigMatchedBTracks, EW)
    histW["nBTrackJets"].Fill(nBTrackJets,EW)
    histW["nBTags"].Fill(NumBTags,EW)
    histW["nNewBTags"].Fill(len(newBTags),EW)
    histW["nBTrackJetsDIFFnewBTags"].Fill(nBTrackJets-len(newBTags),EW)
    histW["nTrueBTags"].Fill(len(truthBTags),EW)
    histW["nMu"].Fill(mu,EW)
    histW["sumnSigJets_mu"].Fill(mu,nSigJets*EW)
    histW["nSigJets"].Fill(nSigJets, EW)
    histW["nTrackJets"].Fill(nTrackJets, EW)
    histW["nMatchedTruthJets"].Fill(nMatchedTruthJets, EW)
    histW["nTracksNotMatched"].Fill(nTracksNotMatched, EW)
    histW["nSigNotMatched"].Fill(nSigNotMatched, EW)
    histW["nBTracksNotMatched"].Fill(nBTracksNotMatched, EW)
    histW["nBSigNotMatched"].Fill(nBSigNotMatched, EW)
    histW["nBJetsNotMatchedBTracks"].Fill(nBJetsNotMatchedBTracks, EW)
    histW["nBTracksNotMatchedBJets"].Fill(nBTracksNotMatchedBJets, EW)
    histW["nTrueTagsNotTagged"].Fill(len(truthBTags)-nBJetsAreTrueTags, EW)
    histW["nTrueTagsNotNewTagged"].Fill(len(truthBTags)-nNewBJetsAreTrueTags, EW)
    histW["sumGSCMbb_mu"].Fill(mu,GSC*EW)
    histW["GSCMbb"].Fill(GSC, EW)
    histW["Mbb"].Fill(mBB, EW)
    histW["pTBB"].Fill(pTBB, EW)
    histW["dPhiBB"].Fill(t.dPhiBB, EW)
    histW["dRBB"].Fill(t.dRBB, EW)
    histW["dEtaBB"].Fill(t.dEtaBB, EW)
    if args.channel == "0Lep":
        histW["dPhiMETdijet"].Fill(t.dPhiMETdijet,EW)
        histW["TrackdPhiMETdijet"].Fill(t.dPhiMETtdijet,EW)
    histW["TrackMbb"].Fill(higgsM, EW)
    histW["TrackpTBB"].Fill(higgsPT, EW)
    histW["TrackdPhiBB"].Fill(t.dPhitBB, EW)
    #histW["TrackdEtaBB"].Fill(t.dEtatBB, EW)
    #histW["TrackdRBB"].Fill(t.dRtBB, EW)
    histW["TrackGSCMbb"].Fill(HCandGSC.M(), EW)
    histW["PtRecoMbb"].Fill(PtReco, EW)
    histW["OneMuMbb"].Fill(OneMu, EW)
    histW["TruthWZMbb"].Fill(Truth, EW)
    histW["TrueBMbb"].Fill((LTB1+LTB2).M(), EW)
    histW["pTV"].Fill(pTV, EW)
    histW["pTJ1"].Fill(LB1.Pt(), EW)
    histW["pTJ2"].Fill(LB2.Pt(), EW)
    histW["pTJt1"].Fill(LBt1.Pt(), EW)
    histW["pTJt2"].Fill(LBt2.Pt(), EW)                            
    histW["pTTrackJ2"].Fill(TrackJ2pT, EW)
    histW["EtaJ1"].Fill(LB1.Eta(), EW)
    histW["EtaJ2"].Fill(LB2.Eta(), EW)
    histW["EtaJt1"].Fill(LBt1.Eta(), EW)
    histW["EtaJt2"].Fill(LBt2.Eta(), EW)
    histW["dRJ1TB1"].Fill(LTB1.DeltaR(LB1),EW)
    histW["dRJ2TB2"].Fill(LTB2.DeltaR(LB2),EW)
    histW["dRJt1TB1"].Fill(LTB1.DeltaR(LBt1),EW)
    histW["dRJt2TB2"].Fill(LTB2.DeltaR(LBt2),EW)
    histW["dRTruth_GSC"].Fill(LT1.DeltaR(LG1), EW)
    histW["dRTruth_OneMu"].Fill(LT1.DeltaR(LM1), EW)
    histW["dRTruth_PtReco"].Fill(LT1.DeltaR(LP1), EW)
    histW["dRTruth_Final"].Fill(LT1.DeltaR(LB1), EW)
    histW["dPtTruth_GSC"].Fill(LT1.Pt()-LG1.Pt(), EW)
    histW["dPtTruth_Final"].Fill(LT1.Pt()-LB1.Pt(), EW)
    histW["dPtJ1TB1"].Fill(LTB1.Pt()-LB1.Pt(),EW)
    histW["dPtJ2TB2"].Fill(LTB2.Pt()-LB2.Pt(),EW)
    histW["dPtJt1TB1"].Fill(LTB1.Pt()-LBt1.Pt(),EW)
    histW["dPtJt2TB2"].Fill(LTB2.Pt()-LBt2.Pt(),EW)

    if int(pTB1) <=400:
        histW["nPtTrue_Final"].Fill(pTB1,EW)
        histW["sumnPtTrue_Final"].Fill(pTB1, EW*TruthJ1/pTB1)
    if int(PtRecoJ1) <=400:
        histW["nPtTrue_PtReco"].Fill(PtRecoJ1,EW)
        histW["sumnPtTrue_PtReco"].Fill(PtRecoJ1, EW*TruthJ1/PtRecoJ1)
    if int(LM1.Pt()) <=400:
        histW["nPtTrue_OneMu"].Fill(LM1.Pt(),EW)
        histW["sumnPtTrue_OneMu"].Fill(LM1.Pt(), EW*TruthJ1/LM1.Pt())
    if int(LG1.Pt()) <=400:
        histW["nPtTrue_GSC"].Fill(LG1.Pt(),EW)
        histW["sumnPtTrue_GSC"].Fill(LG1.Pt(), EW*TruthJ1/LG1.Pt())
        
    '''
    hist["nBSigMatchedBTrack"].Fill(nBSigMatchedBTrack)
    hist["nSigMatchedBTracks"].Fill(nSigMatchedBTracks)
    hist["nMu"].Fill(mu)
    hist["sumnSigJets_mu"].Fill(mu,nSigJets)
    hist["nSigJets"].Fill(nSigJets)
    hist["nTrackJets"].Fill(nTrackJets)
    hist["nBTrackJets"].Fill(nBTrackJets)
    hist["nBTags"].Fill(NumBTags)
    hist["nNewBTags"].Fill(len(newBTags))
    hist["nTrueBTags"].Fill(len(truthBTags))
    hist["nMatchedTruthJets"].Fill(nMatchedTruthJets)
    hist["nTracksNotMatched"].Fill(nTracksNotMatched)
    hist["nSigNotMatched"].Fill(nSigNotMatched)
    hist["nBTracksNotMatched"].Fill(nBTracksNotMatched)
    hist["nBSigNotMatched"].Fill(nBSigNotMatched)
    hist["nBJetsNotMatchedBTracks"].Fill(nBJetsNotMatchedBTracks)
    hist["nBTracksNotMatchedBJets"].Fill(nBTracksNotMatchedBJets)
    '''
    # end
