#!/usr/bin/env python2

import os
import sys
import ROOT
import collections
import math
import copy
import logging

logging.basicConfig(level=logging.INFO)
sys.argv.append("-b")

# python JetPt.py /eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_a_Resolved_D_D/fetch/data-MVATree/qqZvvHbbJ_PwPy8MINLO-0.root JetOpt_a_0.root 20J2_20J3
# python JetPt.py /eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_a_Resolved_D_D/fetch/data-MVATree/ttbar_nonallhad_PwPy8-0.root JetOpt_a_0.root 20J2_20J3
# python JetPt.py /eos/user/r/ratkin/Reader_output/tag_33-05/JetPtOptimisation45J120J220J3/JetPtOptimisation45J120J220J3/Reader_0L_33-05_a_Resolved_D_D/fetch/data-MVATree/Znunu_Sh221-0.root JetOpt_a_0.root 20J2_20J3
samples_by_type = {"data" : ["data"], "ZJets" : ["Zmumu","Zee","Znunu","Ztautau"], "WJets" : ["Wenu","Wmunu","Wtaunu"],
                       "ttbar" : ["ttbar"], "stop" : ["stop"], "diboson" : ["Wlv","Wqq","Zbb","Zqq","ggWqq","ggZqq"],
                       "signal" : ["ggZll","ggZvv","qq"]}

def getSample(inputFile):
    for sampl in samples_by_type:
        for samp in samples_by_type[sampl]:
            if inputFile.startswith(samp):
                return sampl
    return ""
    
def main(inputFile,per,outputFile,cuts):
    logging.info("period: "+per)
    logging.info("inputFile: "+inputFile)
    logging.info("outputFile: "+outputFile)
    
    
    T = ROOT.TChain("Nominal")
    T.Add(inputFile)
    F = ROOT.TFile(outputFile,"RECREATE")
    
    if "/" in inputFile:
        inputFile = inputFile.split("/")[-1]
    
    sample=getSample(inputFile)

    NUMJETS={"nJets==2":"2jet","nJets==3":"3jet","nJets==4":"4jet","nJets>=2":"2pjet"}
    METCUTS={"MET>150&&MET<250":"150_250ptv","MET>400":"400ptv","MET>250&&MET<400":"250_400ptv","MET>150":"150ptv","MET>250":"250ptv"}
    REGIONS={"Description==\"SR\"" : "SR", "Description==\"CRHigh\"" : "CRHigh","Description==\"CRLow\"" : "CRLow",}
    LEADCUTS={"pTB1>=45":"45J1","pTB1>=60":"60J1"}
    #signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    
    HISTNAMES = {"mBB" : "40,0,400", "GSCMbb" : "40,0,400", "GSCptJ1" : "112,40,600", "GSCptJ2" : "76,20,400", 
                 "dRBB" : "50,0,5", "MET" : "85,150,1000","TruthWZptJ1" : "112,40,600", 
                 "TruthWZptJ2" : "76,20,400", "TruthWZptJ3" : "76,20,400", "TruthWZMbb" : "40,0,400", 
                 "mBBJ" : "100,0,1000","pTB1" : "112,40,600", "pTB2" : "76,20,400", "pTJ3" : "76,20,400",
                 "pTJ4" : "76,20,400", "nMatchedTruthSigJets" : "15,0,15", "softMET":"20,0,100",
                 "sumPtJets":"75,0,750", "MindPhiMETJet":"32,0,3.2", "MindPhiMETJet4J":"32,0,3.2", 
                 "HT" : "125,250,1500","HT4J" : "125,250,1500","nJets" : "15,0,15", "nSigJets" : "15,0,15",
                 "nFwdJets" : "15,0,15","nbJets" : "15,0,15","nTrkJets" : "15,0,15", 
                 "Njets_truth_pTjet30" : "15,0,15","mva" : "2000,-1,1", "ActualMu" : "20,0,100", 
                 "TTBarDecay" : "10,0,10","nJetMigration" : "40,20,60", "nTagMigration" : "40,20,60" }
    
    #NUMJETS={"nJets==2":"2jet"}
    #METCUTS={"MET>150":"150ptv"}
    #REGIONS={"Description==\"SR\"" : "SR"}
    #LEADCUTS={"pTB1>=45":"45J1"}
    #HISTNAMES = {"mBB" : "40,0,400"}
    #HISTNAMES = {"nJetMigration" : "40,20,60"}
    ROOT.gROOT.SetBatch(True)
    numEntries = T.GetEntries()
    print(numEntries)
    if not numEntries > 0:
        print("This file is empty. Exiting now")
        exit()
    #main
    for NumJet in NUMJETS:
        for METCut in METCUTS:
            for Reg in REGIONS:
                for hName in HISTNAMES:
                    if "J3" in hName or hName == "mBBJ":
                        if NumJet == "nJets==2":
                            continue
                    elif hName == "pTJ4" or hName == "MindPhiMETJet4J" or hName == "HT4J":
                        if NumJet == "nJets==2" or NumJet == "nJets==3":
                            continue
                    elif "Migration" in hName:
                        if NumJet != "nJets>=2":
                            continue
                    elif hName == "TTBarDecay":
                        if sample != "ttbar":
                            continue
                    for LeadCut in LEADCUTS:
                        
                        histName = sample+"_"+NUMJETS[NumJet]+"_"+METCUTS[METCut]+"_"+LEADCUTS[LeadCut]+"_"+cuts+"_"+REGIONS[Reg]+"_"+hName
                        histCreation = hName+">>"+histName+"("+HISTNAMES[hName]+")"
                        logging.debug("Creating  "+histCreation)
                        eventSelector = "EventWeight*(nSigJets>=2&&nbJets==2&&"+NumJet+"&&"+METCut+"&&"+Reg+"&&"+LeadCut+"&&PassNonJetCountCuts==1"
                        
                        if sample == "ZJets" or sample == "WJets" or sample == "ttbar":
                            for EF in ["bb","bc","bl","cc","cl","l"]:
                                eventSelector2=eventSelector+"&&EventFlavor==\""+EF+"\")"
                                histName2 = sample+EF+"_"+NUMJETS[NumJet]+"_"+METCUTS[METCut]+"_"+LEADCUTS[LeadCut]+"_"+cuts+"_"+REGIONS[Reg]+"_"+hName
                                logging.debug("  "+histName2)
                                logging.debug("  "+eventSelector2)
                                T.Draw(hName+">>"+histName2+"("+HISTNAMES[hName]+")",eventSelector2)
                                h = ROOT.gDirectory.Get(histName2)
                                F.cd()
                                #h.Sumw2()
                                h.Write()
                                
                                if hName == "nSigJets":
                                    histName3 = histName2 + "_2D"
                                    T.Draw(hName+":ActualMu>>"+histName3+"("+HISTNAMES["ActualMu"]+","+HISTNAMES[hName]+")",eventSelector2)
                                    h1 = ROOT.gDirectory.Get(histName3)
                                    F.cd()
                                    h1.Write()
                                    
                                    histName3 = histName2 + "-nMatchedTruthSigJets_2D"
                                    T.Draw("(nSigJets-nMatchedTruthSigJets):ActualMu>>"+histName3+"("+HISTNAMES["ActualMu"]+",20,-5,15)",eventSelector2)
                                    h2 = ROOT.gDirectory.Get(histName3)
                                    F.cd()
                                    h2.Write()
                                    
                                    histName3 = histName2 + "-nMatchedTruthSigJets"
                                    T.Draw("(nSigJets-nMatchedTruthSigJets)>>"+histName3+"(20,-5,15)",eventSelector2)
                                    h3 = ROOT.gDirectory.Get(histName3)
                                    F.cd()
                                    h3.Write()
                                elif hName == "mBB":
                                    if NumJet == "nJets==2":
                                        eventSelector3=eventSelector+"&&hasFSR==1&&EventFlavor==\""+EF+"\")"
                                        histName4 = histName2 + "_FSR"
                                        T.Draw(hName+">>"+histName4+"("+HISTNAMES[hName]+")",eventSelector3)
                                        h4 = ROOT.gDirectory.Get(histName4)
                                        F.cd()
                                        #h4.Sumw2()
                                        h4.Write()
                        else:
                            eventSelector2=eventSelector+")"
                            logging.debug("  "+eventSelector2)
                            T.Draw(histCreation,eventSelector2)
                            h = ROOT.gDirectory.Get(histName)
                            F.cd()
                            #h.Sumw2()
                            h.Write()

                            if hName == "nSigJets":
                                histName2 = histName + "_2D"
                                T.Draw(hName+":ActualMu>>"+histName2+"("+HISTNAMES["ActualMu"]+","+HISTNAMES[hName]+")",eventSelector2)
                                h1 = ROOT.gDirectory.Get(histName2)
                                F.cd()
                                h1.Write()
                                
                                histName2 = histName + "-nMatchedTruthSigJets_2D"
                                T.Draw("(nSigJets-nMatchedTruthSigJets):ActualMu>>"+histName2+"("+HISTNAMES["ActualMu"]+",20,-5,15)",eventSelector2)
                                h2 = ROOT.gDirectory.Get(histName2)
                                F.cd()
                                h2.Write()

                                histName2 = histName + "-nMatchedTruthSigJets"
                                T.Draw("(nSigJets-nMatchedTruthSigJets)>>"+histName2+"(20,-5,15)",eventSelector2)
                                h3 = ROOT.gDirectory.Get(histName2)
                                F.cd()
                                #h3.Sumw2()
                                h3.Write()
                            elif hName == "mBB":
                                if NumJet == "nJets==2":
                                    eventSelector3=eventSelector+"&&hasFSR==1)"
                                    histName2 = histName + "_FSR"
                                    T.Draw(hName+">>"+histName2+"("+HISTNAMES[hName]+")",eventSelector3)
                                    h4 = ROOT.gDirectory.Get(histName2)
                                    F.cd()
                                    #h4.Sumw2()
                                    h4.Write()
                                
    
    F.Close()

    logging.info("\n Output file "+outputFile+" created")
    logging.info("\n\n All Finished !!!")
    

if __name__ == "__main__":
    inputFile = sys.argv[1]
    per = sys.argv[2].split("_")[1]
    outputFile = sys.argv[2]
    cuts = sys.argv[3]
    main(inputFile,per,outputFile,cuts)




'''
*Br    0 :nJets     : nJets/I                                                *
*Br    1 :nTaus     : nTaus/I                                                *
*Br    2 :flavB1    : flavB1/I                                               *
*Br    3 :flavB2    : flavB2/I                                               *
*Br    4 :flavJ3    : flavJ3/I                                               *
*Br    5 :flavJ4    : flavJ4/I                                               *
*Br    6 :hasFSR    : hasFSR/I                                               *
*Br    7 :nbJets    : nbJets/I                                               *
*Br    8 :isMerged  : isMerged/I                                             *
*Br    9 :nFwdJets  : nFwdJets/I                                             *
*Br   10 :nSigJets  : nSigJets/I                                             *
*Br   11 :nTrkJets  : nTrkJets/I                                             *
*Br   12 :RunNumber : RunNumber/I                                            *
*Br   13 :isOverlap : isOverlap/I                                            *
*Br   14 :isSLEvent : isSLEvent/I                                            *
*Br   15 :nBTrkJets : nBTrkJets/I                                            *
*Br   16 :isResolved : isResolved/I                                          *
*Br   17 :nJetMigration : nJetMigration/I                                    *
*Br   18 :nTagMigration : nTagMigration/I                                    *
*Br   19 :MCChannelNumber : MCChannelNumber/I                                *
*Br   20 :passResoSel_noTag : passResoSel_noTag/I                            *
*Br   21 :passBoostSel_noTag : passBoostSel_noTag/I                          *
*Br   22 :Njets_truth_pTjet30 : Njets_truth_pTjet30/I                        *
*Br   23 :PassNonJetCountCuts : PassNonJetCountCuts/I                        *
*Br   24 :nMatchedTruthFwdJets : nMatchedTruthFwdJets/I                      *
*Br   25 :nMatchedTruthSigJets : nMatchedTruthSigJets/I                      *
*Br   26 :EventNumber : EventNumber/l                                        *
*Br   27 :MET       : MET/F                                                  *
*Br   28 :mB1       : mB1/F                                                  *
*Br   29 :mB2       : mB2/F                                                  *
*Br   30 :mBB       : mBB/F                                                  *
*Br   31 :mJ3       : mJ3/F                                                  *
*Br   32 :mJ4       : mJ4/F                                                  *
*Br   33 :pTV       : pTV/F                                                  *
*Br   34 :dRBB      : dRBB/F                                                 *
*Br   35 :mBBJ      : mBBJ/F                                                 *
*Br   36 :pTB1      : pTB1/F                                                 *
*Br   37 :pTB2      : pTB2/F                                                 *
*Br   38 :pTBB      : pTBB/F                                                 *
*Br   39 :pTJ3      : pTJ3/F                                                 *
*Br   40 :pTJ4      : pTJ4/F                                                 *
*Br   41 :etaB1     : etaB1/F                                                *
*Br   42 :etaB2     : etaB2/F                                                *
*Br   43 :etaBB     : etaBB/F                                                *
*Br   44 :etaJ3     : etaJ3/F                                                *
*Br   45 :etaJ4     : etaJ4/F                                                *
*Br   46 :pTBBJ     : pTBBJ/F                                                *
*Br   47 :phiB1     : phiB1/F                                                *
*Br   48 :phiB2     : phiB2/F                                                *
*Br   49 :phiBB     : phiBB/F                                                *
*Br   50 :phiJ3     : phiJ3/F                                                *
*Br   51 :phiJ4     : phiJ4/F                                                *
*Br   52 :BTagSF    : BTagSF/F                                               *
*Br   53 :GSCMbb    : GSCMbb/F                                               *
*Br   54 :GSCmJ1    : GSCmJ1/F                                               *
*Br   55 :GSCmJ2    : GSCmJ2/F                                               *
*Br   56 :METPhi    : METPhi/F                                               *
*Br   57 :dEtaBB    : dEtaBB/F                                               *
*Br   58 :dPhiBB    : dPhiBB/F                                               *
*Br   59 :metSig    : metSig/F                                               *
*Br   60 :GSCptJ1   : GSCptJ1/F                                              *
*Br   61 :GSCptJ2   : GSCptJ2/F                                              *
*Br   62 :softMET   : softMET/F                                              *
*Br   63 :ActualMu  : ActualMu/F                                             *
*Br   64 :GSCetaJ1  : GSCetaJ1/F                                             *
*Br   65 :GSCetaJ2  : GSCetaJ2/F                                             *
*Br   66 :GSCphiJ1  : GSCphiJ1/F                                             *
*Br   67 :GSCphiJ2  : GSCphiJ2/F                                             *
*Br   68 :MV2c10B1  : MV2c10B1/F                                             *
*Br   69 :MV2c10B2  : MV2c10B2/F                                             *
*Br   70 :OneMuMbb  : OneMuMbb/F                                             *
*Br   71 :OneMumJ1  : OneMumJ1/F                                             *
*Br   72 :OneMumJ2  : OneMumJ2/F                                             *
*Br   73 :PUWeight  : PUWeight/F                                             *
*Br   74 :mH_truth  : mH_truth/F                                             *
*Br   75 :mV_truth  : mV_truth/F                                             *
*Br   76 :AverageMu : AverageMu/F                                            *
*Br   77 :OneMuptJ1 : OneMuptJ1/F                                            *
*Br   78 :OneMuptJ2 : OneMuptJ2/F                                            *
*Br   79 :PtRecoMbb : PtRecoMbb/F                                            *
*Br   80 :PtRecomJ1 : PtRecomJ1/F                                            *
*Br   81 :PtRecomJ2 : PtRecomJ2/F                                            *
*Br   82 :TriggerSF : TriggerSF/F                                            *
*Br   83 :pTH_truth : pTH_truth/F                                            *
*Br   84 :pTV_truth : pTV_truth/F                                            *
*Br   85 :sumPtJets : sumPtJets/F                                            *
*Br   86 :LumiWeight : LumiWeight/F                                          *
*Br   87 :OneMuetaJ1 : OneMuetaJ1/F                                          *
*Br   88 :OneMuetaJ2 : OneMuetaJ2/F                                          *
*Br   89 :OneMuphiJ1 : OneMuphiJ1/F                                          *
*Br   90 :OneMuphiJ2 : OneMuphiJ2/F                                          *
*Br   91 :PtRecoptJ1 : PtRecoptJ1/F                                          *
*Br   92 :PtRecoptJ2 : PtRecoptJ2/F                                          *
*Br   93 :TruthWZMbb : TruthWZMbb/F                                          *
*Br   94 :TruthWZmJ1 : TruthWZmJ1/F                                          *
*Br   95 :TruthWZmJ2 : TruthWZmJ2/F                                          *
*Br   96 :TruthWZmJ3 : TruthWZmJ3/F                                          *
*Br   97 :TruthWZmJ4 : TruthWZmJ4/F                                          *
*Br   98 :bin_bTagB1 : bin_bTagB1/F                                          *
*Br   99 :bin_bTagB2 : bin_bTagB2/F                                          *
*Br  100 :bin_bTagJ3 : bin_bTagJ3/F                                          *
*Br  101 :etaH_truth : etaH_truth/F                                          *
*Br  102 :etaV_truth : etaV_truth/F                                          *
*Br  103 :phiH_truth : phiH_truth/F                                          *
*Br  104 :phiV_truth : phiV_truth/F                                          *
*Br  105 :EventWeight : EventWeight/F                                        *
*Br  106 :PtRecoetaJ1 : PtRecoetaJ1/F                                        *
*Br  107 :PtRecoetaJ2 : PtRecoetaJ2/F                                        *
*Br  108 :PtRecophiJ1 : PtRecophiJ1/F                                        *
*Br  109 :PtRecophiJ2 : PtRecophiJ2/F                                        *
*Br  110 :TruthWZptJ1 : TruthWZptJ1/F                                        *
*Br  111 :TruthWZptJ2 : TruthWZptJ2/F                                        *
*Br  112 :TruthWZptJ3 : TruthWZptJ3/F                                        *
*Br  113 :TruthWZptJ4 : TruthWZptJ4/F                                        *
*Br  114 :mVHResolved : mVHResolved/F                                        *
*Br  115 :TruthWZetaJ1 : TruthWZetaJ1/F                                      *
*Br  116 :TruthWZetaJ2 : TruthWZetaJ2/F                                      *
*Br  117 :TruthWZetaJ3 : TruthWZetaJ3/F                                      *
*Br  118 :TruthWZetaJ4 : TruthWZetaJ4/F                                      *
*Br  119 :TruthWZphiJ1 : TruthWZphiJ1/F                                      *
*Br  120 :TruthWZphiJ2 : TruthWZphiJ2/F                                      *
*Br  121 :TruthWZphiJ3 : TruthWZphiJ3/F                                      *
*Br  122 :TruthWZphiJ4 : TruthWZphiJ4/F                                      *
*Br  123 :pTVHResolved : pTVHResolved/F                                      *
*Br  124 :MCEventWeight : MCEventWeight/F                                    *
*Br  125 :ActualMuScaled : ActualMuScaled/F                                  *
*Br  126 :NTruthWZJets20 : NTruthWZJets20/F                                  *
*Br  127 :AverageMuScaled : AverageMuScaled/F                                *
*Br  128 :JVTWeightResolved : JVTWeightResolved/F                            *
*Br  129 :HT        : HT/D                                                   *
*Br  130 :MPT       : MPT/D                                                  *
*Br  131 :mva       : mva/D                                                  *
*Br  132 :dPhiMETMPT : dPhiMETMPT/D                                          *
*Br  133 :mvadiboson : mvadiboson/D                                          *
*Br  134 :dPhiMETdijet : dPhiMETdijet/D                                      *
*Br  135 :MindPhiMETJet : MindPhiMETJet/D                                    *
*Br  136 :MindPhiMETJet4J : MindPhiMETJet4J/D                                *
*Br  137 :dPhiMETdijetResolved : dPhiMETdijetResolved/D                      *
*Br  138 :MindPhiMETJetResolved : MindPhiMETJetResolved/D                    *
*Br  139 :sample    : string                                                 *
*Br  140 :dRBBReg   : string                                                 *
*Br  141 :Description : string                                               *
*Br  142 :EventFlavor : string                                               *
*Br  143 :HT4J      : HT4J/D
'''


