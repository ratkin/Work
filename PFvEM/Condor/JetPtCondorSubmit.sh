#! /bin/bash

#echo "\n 45J120J220J3 \n"
condor_submit per=a dir=45J120J220J3 cuts=20J2_20J3 JetPtCondor.sub
condor_submit per=d dir=45J120J220J3 cuts=20J2_20J3 JetPtCondor.sub
condor_submit per=e dir=45J120J220J3 cuts=20J2_20J3 JetPtCondor.sub

#echo "\n 45J120J225J3 \n"
#condor_submit per=a dir=45J120J225J3 cuts=20J2_25J3 JetPtCondor.sub
#condor_submit per=d dir=45J120J225J3 cuts=20J2_25J3 JetPtCondor.sub
#condor_submit per=e dir=45J120J225J3 cuts=20J2_25J3 JetPtCondor.sub

#echo "\n 45J125J225J3 \n"
#condor_submit per=a dir=45J125J225J3 cuts=25J2_25J3 JetPtCondor.sub
#condor_submit per=d dir=45J125J225J3 cuts=25J2_25J3 JetPtCondor.sub
#condor_submit per=e dir=45J125J225J3 cuts=25J2_25J3 JetPtCondor.sub

#echo "\n 45J120J230J3 \n"
condor_submit per=a dir=45J120J230J3 cuts=20J2_30J3 JetPtCondor.sub
condor_submit per=d dir=45J120J230J3 cuts=20J2_30J3 JetPtCondor.sub
condor_submit per=e dir=45J120J230J3 cuts=20J2_30J3 JetPtCondor.sub

#echo "\n 45J130J230J3 \n"
condor_submit per=a dir=45J130J230J3 cuts=30J2_30J3 JetPtCondor.sub
condor_submit per=d dir=45J130J230J3 cuts=30J2_30J3 JetPtCondor.sub
condor_submit per=e dir=45J130J230J3 cuts=30J2_30J3 JetPtCondor.sub

#####
# FSR
#####

# echo "\n 45J120J220J3_FSR \n"
# condor_submit per=a dir=45J120J220J3_FSR cuts=20J2_20J3 JetPtCondor.sub
# condor_submit per=d dir=45J120J220J3_FSR cuts=20J2_20J3 JetPtCondor.sub
# condor_submit per=e dir=45J120J220J3_FSR cuts=20J2_20J3 JetPtCondor.sub

# echo "\n 45J120J230J3_FSR \n"
# condor_submit per=a dir=45J120J230J3_FSR cuts=20J2_30J3 JetPtCondor.sub
# condor_submit per=d dir=45J120J230J3_FSR cuts=20J2_30J3 JetPtCondor.sub
# condor_submit per=e dir=45J120J230J3_FSR cuts=20J2_30J3 JetPtCondor.sub

# echo "\n 45J130J230J3_FSR \n"
# condor_submit per=a dir=45J130J230J3_FSR cuts=30J2_30J3 JetPtCondor.sub
# condor_submit per=d dir=45J130J230J3_FSR cuts=30J2_30J3 JetPtCondor.sub
# condor_submit per=e dir=45J130J230J3_FSR cuts=30J2_30J3 JetPtCondor.sub

# echo "\n 45J120J230J3_FSR_b4Cuts \n"
# condor_submit per=a dir=45J120J230J3_FSR_b4Cuts cuts=20J2_30J3 JetPtCondor.sub
# condor_submit per=d dir=45J120J230J3_FSR_b4Cuts cuts=20J2_30J3 JetPtCondor.sub
# condor_submit per=e dir=45J120J230J3_FSR_b4Cuts cuts=20J2_30J3 JetPtCondor.sub

# echo "\n 45J130J230J3_FSR_b4Cuts \n"
# condor_submit per=a dir=45J130J230J3_FSR_b4Cuts cuts=30J2_30J3 JetPtCondor.sub
# condor_submit per=d dir=45J130J230J3_FSR_b4Cuts cuts=30J2_30J3 JetPtCondor.sub
# condor_submit per=e dir=45J130J230J3_FSR_b4Cuts cuts=30J2_30J3 JetPtCondor.sub
