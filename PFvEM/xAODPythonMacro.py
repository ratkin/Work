#!/usr/bin/env python

# Set up ROOT and RootCore:
import ROOT
import os
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure: 
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

# Set up the input files:
#tutorialPath = os.environ["ALRB_TutorialData"]

ssWW = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r33-00_pf18/HIGG2D4_13TeV/CxAOD_33-00_d/qqZllHbbJ_PwPy8MINLO/user.ckato.mc16_13TeV.345055.CAOD_HIGG2D4.e5706_s3126_r10201_p4207.200717_pf18_CxAOD.root/user.ckato.21924073._000001.CxAOD.root"
#ssWW = ""
#fullTutorialPath = ROOT.gSystem.ExpandPathName(tutorialPath)
#fileName = fullTutorialPath + "/r9264/data16_13TeV.00311321.physics_Main.merge.AOD.r9264_p3083/AOD.11038614._004726.pool.root.1"

treeName = "CollectionTree" # default when making transient tree anyway

f = ROOT.TFile.Open(ssWW)

# Make the "transient tree":
t = ROOT.xAOD.MakeTransientTree( f, treeName)

# Print some information:
print( "Number of input events: %s" % t.GetEntries() )
for entry in xrange( 0,30): # let's only run over the first 100 events for this example
   t.GetEntry( entry ) #Get access to objects in one event
   #met = t.MET_Reference_AntiKt4EMTopo___Nominal
   #truthJets = t.AntiKt4TruthWZJets___Nominal
   #Jets = t.AntiKt4EMTopoJets___Nominal
   #muons = t.Muons___Nominal
   print( "Processing run #%i, event #%i" % ( t.EventInfo___Nominal.runNumber(), t.EventInfo___Nominal.eventNumber() ) )
   continue
   print( "Number of Jets: %i" % len( Jets ))#t.MET_Reference_AntiKt4EMTopo___Nominal) )
   for jet in Jets:
      L1 = jet.jetP4() 
      L2 = jet.jetP4("TruthWZ")
      print(L1.Pt(), L2.Pt())
      print("jet pt {} eta {} phi {} m {}".format(jet.pt(),jet.eta(),jet.phi(),jet.m()))
   print( "Number of truthJets: %i" % len( truthJets ))
   for i in xrange(len(truthJets)):
      #L1 = jet.jetP4()
      #print("pt eta {} phi {} m {}".format(jet.eta(),jet.phi(),jet.m()))
      #try:
         print(truthJets.at(i).pt())
      #except:
      #   print("hello")
   
   #t.ElectronsAuxDyn.etcone20()
   # loop over electrons in one event
   '''
   for i in xrange( met.size()): 
      Met = met.at(i) #
      MET_x = Met.mpx()
      MET_y = Met.mpy()
      print("{} {}".format(MET_x,MET_y))
      print("{}".format(ROOT.TMath.Sqrt(MET_x**2+MET_y**2)))
      ptc20 = el.auxdata("ptcone20")
      ptc40 = el.auxdata("ptcone40")
      L=el.auxdata("LHLoose")
      print(" Electron: %g" % i)
      print("  LHLoose = %s Loose = %s Medium = %s Tight = %s LHValue = %s" % (L,el.auxdata("Loose"),el.auxdata("Medium"),el.auxdata("Tight"),el.auxdata("LHValue")))
      print( "  Electron trackParticle pt = %g, eta = %g, phi = %g" %  ( el.trackParticle().pt(), el.trackParticle().eta(), el.trackParticle().phi() ) )
      print( "  Electron Particle      pt = %g, eta = %g, phi = %g" %  ( el.pt(), el.eta(), el.phi() ) )
      print( "  ptcone20 = %s ptcone40 = %s" % (ptc20,ptc40))
      pass # end for loop over electron collection
   exit()
   print( "Number of muons: %i" % len( t.HWWMuons ) )
   for i in xrange( t.HWWMuons.size()):
      mu = t.HWWMuons.at(i)
      print(" Muon: %g" % i)
      #print( "  Muon trackParticle pt = %g, eta = %g, phi = %g" %  ( mu.trackParticle().pt(), mu.trackParticle().eta(), mu.trackParticle().phi() ) )
      print( "  Muon Particle      pt = %g, eta = %g, phi = %g" %  ( mu.pt(), mu.eta(), mu.phi() ) )
      print( "  ptcone20 = %s ptcone40 = %s" % (mu.auxdata("ptcone20"),mu.auxdata("ptcone40")))
      pass
   pass # end loop over entries
   '''
# clear transient trees to avoid crash at end of job
ROOT.xAOD.ClearTransientTrees()
