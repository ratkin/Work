import os
import glob
import sys
import numpy as np

sum_processed = 0
sum_passed = 0
sum_skipped = 0

per = "a"
sample = "ttbar_nonallhad_PwPy8"
#sample = "qqZvvHbbJ_PwPy8MINLO"
#sample="ggZvvHbb_PwPy8"
filepath = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/32-15/Reader_0L_32-15_{}_Resolved_D/submit/".format(per)
samplepath = "CxAOD_r32-15/HIGG5D1_13TeV/CxAOD_32-15_{}/{}".format(per,sample)
#filepath="/afs/cern.ch/work/r/ratkin/ReaderHistOutput/33-05/Reader_0L_33-05_{}_Resolved_D/submit/".format(per)
#samplepath = "CxAOD_r33-05_pf19/HIGG5D1_13TeV/CxAOD_33-05_{}/{}".format(per,sample)

print("filepath: "+filepath)
print("samplepath: Opening file root://eosatlas.cern.ch://eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/{}/".format(samplepath))
print("starting for loop")
for filename in glob.glob(os.path.join(filepath, '*.out')):
   f=open(os.path.join(filepath, filename),'r')
   A=f.readlines()
   f.close()

   carryOn=0
   i=0
   while i<50:
      line = A[i]
      if "Opening file root://eosatlas.cern.ch://eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/{}/".format(samplepath) in line:
         carryOn=1
         break
      i+=1
      
   if carryOn == 0:
      continue

   last=len(A)
   print(filename)
   start=last-15
   #print('length of PhysVol file: ',last)
   processed = 0
   passed = 0
   skipped = 0
   while start<last:
      line=A[start].split('=')
      if "Processed events" in A[start]:
         processed = int(line[-1])
      elif "Skipped - weight" in A[start]:
         skipped = int(line[-1])
      elif "Passed nominal selection" in A[start]:
         passed = int(line[-1])
      start+=1

   rat=0
   if processed!=0:
      rat = float(passed)/float(processed)
   print("in file {}, processed: {}, skipped: {}, passed: {}, ratio: {}".format(filename,processed,skipped,passed,rat))
   sum_processed+=processed
   sum_passed+=passed
   sum_skipped+=skipped

rat=0
if sum_processed!=0:
   rat = float(sum_passed)/float(sum_processed)
print("in total, processed: {}, skipped: {}, passed: {}, ratio: {}".format(sum_processed,sum_skipped,sum_passed,rat))
