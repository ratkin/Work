"""
Runs over histograms
"""
import os
import sys
import ROOT
import math
import copy
from array import array
from decimal import Decimal
#import argparse

# run as: a is the period (use all for combination), em18 is the jet type for 33-01 (leave blank if doing special tests)
# python Validation_AllSamples.py all >& ValidationSignificances.txt


sys.argv.append("-b")

samples_by_type = {"data" : ["data*"], "ZJets" : ["Zmumu*","Zee*","Znunu*","Ztautau*"], "WJets" : ["Wenu*","Wmunu*","Wtaunu*"],
                   "ttbar" : ["ttbar*"], "stop" : ["stop*"], "diboson" : ["Wlv*","Wqq*","Zbb*","Zqq*","ggWqq*","ggZqq*"],
                   "signal" : ["ggZll*","ggZvv*","qq*"]}

#print(chains_by_type["ZJets"].GetEntries())
#samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
#           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
           #"ttbar" : ["ttbar"],
#           "ttbar" : ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl"],  #my own change to see difference n ttbar based on flavour
#           "stop" : ["stopWt","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],"data" : ["data"],
#           "signal" : ["qqZvvH125","ggZllH125","ggZllH125cc","ggZvvH125","qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125cc"]}

samples = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], 
           "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], 
           "ttbar" : ["ttbar"],
           "stop" : ["stops","stopt","stopWt"],"diboson" : ["WZ","WW","ZZ","ggZZ","ggWW"],
           "signal" : ["qqWlvH125","qqWlvH125cc","qqZllH125","qqZllH125cc","qqZvvH125","qqZvvH125cc","ggZvvH125","ggZvvH125cc","ggZllH125","ggZllH125cc",]}


colours = {"signal" : ROOT.kRed, 
           "Wbb" : ROOT.kGreen+4, 
           "Wbc" : ROOT.kGreen+3,
           "Wbl" : ROOT.kGreen+2,
           "Wcc" : ROOT.kGreen+1,
           "Wcl" : ROOT.kGreen-6,
           "Wl" : ROOT.kGreen-9,
           "Zbb" : ROOT.kAzure+2, 
           "Zbc" : ROOT.kAzure+1, 
           "Zbl" : ROOT.kAzure-2, 
           "Zcc" : ROOT.kAzure-4, 
           "Zcl" : ROOT.kAzure-8, 
           "Zl" : ROOT.kAzure-9, 
           "ttbarbb" : ROOT.kOrange, 
           "stop" : ROOT.kOrange-1, 
           "diboson" : ROOT.kGray, 
           "data" : ROOT.kBlack}


working_dir = "/afs/cern.ch/work/r/ratkin/WSMaker2/PFvEMTopo_plots/"
if not os.path.isdir(working_dir):
    os.makedirs(working_dir)
if not os.path.isdir(working_dir+"ZeroLepton/"):
    os.makedirs(working_dir+"ZeroLepton/")
if not os.path.isdir(working_dir+"OneLepton/"):
    os.makedirs(working_dir+"OneLepton/")
if not os.path.isdir(working_dir+"TwoLepton/"):
    os.makedirs(working_dir+"TwoLepton/")

files_dir_nom = "/afs/cern.ch/work/r/ratkin/WSMaker2/WSMaker_VHbb/inputs/SMVHVZ_2021_MVA_mc16ade_Nom_STXS/"
files_dir_2030 = "/afs/cern.ch/work/r/ratkin/WSMaker2/WSMaker_VHbb/inputs/SMVHVZ_2021_MVA_mc16ade_2030_STXS/"
files_dir_3030 = "/afs/cern.ch/work/r/ratkin/WSMaker2/WSMaker_VHbb/inputs/SMVHVZ_2021_MVA_mc16ade_3030_STXS/"

Yields={"signal" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "ttbar" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "stop" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "diboson" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, 
        "Wbb" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wbc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wbl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wcc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wcl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, 
        "Zbb" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zbc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zbl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zcc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zcl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, }


W=open(working_dir+"WSstudies_Yields.tex",'w')
W.write(r"\documentclass[aspectratio=916]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"    \usetheme{default}}")
W.write("\n")
W.write(r"    \usepackage{graphicx}")
W.write("\n")
W.write(r"    \usepackage{booktabs}")
W.write("\n")
W.write(r"    \usepackage{caption}")
W.write("\n")
W.write(r"    \usepackage{subcaption}")
W.write("\n")
W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"    \begin{document}")
W.write("\n")
W.write(r"\end{document} % move this to end of file")
W.write("\n")
W.close()

def printYields(yields,canName):
    
    W=open(working_dir+"WSstudies_Yields.tex",'a')
    canName=canName.replace("_","-")

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+canName+"}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.25\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J2\_20J3}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3}} & \multicolumn{1}{c|}{\textbf{30J2\_30J3}}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in ["signal","diboson","stop","ttbar","Zbb","Zbc","Zbl","Zcc","Zcl","Zl","Wbb","Wbc","Wbl","Wcc","Wcl","Wl"]:
        nom = yields[samp]["20J2_20J3"]
        nom1 = nom
        if nom1 ==0.0:
            nom1=1
        W.write(r"    {} & {:.1f} & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) \\ ".format(
            samp,nom,
            yields[samp]["20J2_30J3"],100*(yields[samp]["20J2_30J3"]/nom1-1),
            yields[samp]["30J2_30J3"],100*(yields[samp]["30J2_30J3"]/nom1-1),
        ))
        
        W.write("\n")
        if samp == "ttbar" or samp == "Zl":
             W.write(r"    \midrule")
             W.write("\n")
    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()


def rebinTrafoDHist(histo, bins): 
  
    histoOld = ROOT.TH1F(histo) 
    histo.Reset()
    newNBins = len(bins)-1
    
    nBinEdges = len(bins) # add one more bin for plotting
    binEdges = [None]*nBinEdges
    for iBinEdge in range(0, nBinEdges) :
        iBin = bins[nBinEdges - iBinEdge - 1] # vector is reverse-ordered
        if (iBin == 0): 
            iBin = 1
        if (iBin == histoOld.GetNbinsX() + 2): 
            iBin = histoOld.GetNbinsX() + 1
    
        binEdges[iBinEdge] = histoOld.GetBinLowEdge(iBin)
    
    #    binEdges[nBinEdges - 1] = 1.8; # additional bin for plotting
    binEdges = np.array(binEdges , dtype="d") 
    histo.SetBins(newNBins, binEdges)
  
    for iBinNew in range(1, newNBins+1):
        # vector is reverse-ordered
        iBinLow  = bins[newNBins - iBinNew + 1] 
        iBinHigh = bins[newNBins - iBinNew] - 1 
        
        err = ctypes.c_double(0.)
        integral = histoOld.IntegralAndError(iBinLow, iBinHigh, err)
        histo.SetBinContent(iBinNew, integral)
        histo.SetBinError(iBinNew, err)
  
    oldNbins = histoOld.GetNbinsX()
    diff = 1. - float(histo.Integral(0, newNBins + 1)) / histoOld.Integral(0, oldNbins + 1)
    # double diff = 1 - histo -> GetSumOfWeights() / histoOld -> GetSumOfWeights();
    if (abs(diff) > 1.e-7) :
        print("WARNING: sizeable difference in transformation of {} found. Integrals: (old-new)/old = {}".format(
            histo.GetName()   , diff              ))
        

def applyTrafoDToHist(hist_sig, hist_bkg, vtag):
    Zs=0
    Zb=0
    if "150_250" in vtag:
        Zs=10
        Zb=5
    else:
        Zs=5
        Zb=3
    # rebin histogram based on trafoD binning 
    newBins = getTrafoD(hist_sig, hist_bkg, Zs, Zb)
    
    rebinTrafoDHist(hist_sig, newBins)
    rebinTrafoDHist(hist_bkg, newBins)
    

def getTrafoD(hist_sig, hist_bkg, Zsig, Zbkg, MCstatUpBound = 0.2) : 
    histoSig = ROOT.TH1F(hist_sig) 
    histoBkg = ROOT.TH1F(hist_bkg) 
    
    bins = [] 
    nBins = histoBkg.GetNbinsX()
    iBin = nBins + 1; # start with overflow bin
    bins.append(nBins+2)
    
    nBkgerr = ctypes.c_double(0.)
    
    nBkg = histoBkg.IntegralAndError(0, nBins+1, nBkgerr)
    nSig = histoSig.Integral(0, nBins+1)
    #print("iBin {} nBkg {} nSig {}".format(iBin,nBkg,nSig))
    while (iBin > 0) :
        #print(iBin)
        sumBkg    = 0.
        sumSig    = 0.
        err2Bkg   = 0.
        bool_pass = False
        
        dist = 1e10
        distPrev = 1e10

        while (not bool_pass and iBin >= 0) :
            nBkgBin  = histoBkg.GetBinContent(iBin)
            nSigBin  = histoSig.GetBinContent(iBin)
            sumBkg  += nBkgBin
            sumSig  += nSigBin
            err2Bkg += pow(histoBkg.GetBinError(iBin),2)
            
            #print("  iBin {} sunBkg {} sumSig {} err2Bkg {}".format(iBin,sumBkg,sumSig,err2Bkg))
            err2RelBkg = 1.
            if (sumBkg != 0): 
                err2RelBkg = err2Bkg / pow(sumBkg, 2)
            
            err2Rel = 1.
            # "trafo D"
            if (sumBkg != 0 and sumSig != 0) :
                err2Rel = 1. / (sumBkg / (nBkg / Zbkg) + sumSig / (nSig / Zsig))
            elif (sumBkg != 0) : 
                err2Rel = (nBkg / Zbkg) / sumBkg
            elif (sumSig != 0) : 
                err2Rel = (nSig / Zsig) / sumSig

            bool_pass = (np.sqrt(err2Rel) < 1 and np.sqrt(err2RelBkg) < MCstatUpBound)
            # distance
            dist = np.abs(err2Rel - 1)
            #print("  err2RelBkg {} err2Rel {} bool_pass {} dist {} distPrev {}".format(err2RelBkg,err2Rel,bool_pass,dist,distPrev))
            if (not (bool_pass and dist > distPrev)): 
                iBin-=1
            #else : 
                # use previous bin
            distPrev = dist
        # remove last bin
        if (iBin+1 == 0 and len(bins) > 1 and len(bins) > Zsig + Zbkg + 0.01): 
            # remove last bin if Nbin > Zsig + Zbkg
            # (1% threshold to capture rounding issues)
            bins.pop()
        #print("appending "+str(iBin+1))
        bins.append(iBin+1)
    return bins

def getHistos(sample,f):
    f.cd()
    h = ROOT.TH1F()
    i = 0
    for samp in samples[sample]:
        hist = f.Get(samp)
        if i ==0:
            #print("Getting "+samp)
            try:
                h=hist.Clone()
            except:
                print(samp+" does not exist")
                i-=1
            
            i+=1
        else:
            #print("Getting "+samp)
            try:
                h.Add(hist.Clone())
            except:
                print(samp+" does not exist")
    return h

def cutsComparisonAllSamples(varName,binning,canName,doNjet):
    Yields={"signal" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "ttbar" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "stop" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "diboson" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, 
            "Wbb" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wbc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wbl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wcc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wcl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Wl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, 
            "Zbb" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zbc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zbl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zcc" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zcl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, "Zl" : {"20J2_20J3":0, "20J2_30J3":0, "30J2_30J3":0}, }
    
    for sample in samples:
        if "WJets" in sample or "ZJets" in sample:
            for samp in samples[sample]:
                cutsComparison(varName,binning,canName,samp,doNjet,Yields)
        else:
            cutsComparison(varName,binning,canName,sample,doNjet,Yields)
    if varName == "mva":
        printYields(Yields,canName)

def cutsComparison(varName,binning,canName,sample,doNjet,Yields):
    #print("cutsComparison")
    print("   "+canName+"  "+sample)
    #13TeV_ZeroLepton_2tag2jet_150_250ptv_SR_mva  ttbar
    f_nom = ROOT.TFile(files_dir_nom+canName+".root", "READ")
    f_2030 = ROOT.TFile(files_dir_2030+canName+".root", "READ")
    f_3030 = ROOT.TFile(files_dir_3030+canName+".root", "READ")
    
    h_nom = ROOT.TH1F()
    h_2030 = ROOT.TH1F()
    h_3030 = ROOT.TH1F()

    line=canName.split("_")
    sample2=sample
    histosE = [1,1,1]
    if (sample == "stop" or sample == "diboson" or sample == "signal"):
        h_nom=getHistos(sample,f_nom)
        h_2030=getHistos(sample,f_2030)
        h_3030=getHistos(sample,f_3030)
    else:
        f_nom.cd()
        hist = f_nom.Get(sample)
        try:
            h_nom=hist.Clone()
        except:
            print(sample+" nom does not exist")
            histosE[0]=0
        f_2030.cd()
        hist = f_2030.Get(sample)
        try:
            h_2030=hist.Clone()
        except:
            print(sample+" 2030 does not exist")
            histosE[1]=0
        f_3030.cd()
        hist = f_3030.Get(sample)
        try:
            h_3030=hist.Clone()
        except:
            print(sample+" 3030 does not exist")
            histosE[2]=0
        
    if sum(histosE) == 0:
        print(sample+" no hists found")
        return
    elif histosE[0]==0:
        h_nom = h_2030.Clone()
    elif histosE[1]==0:
        h_2030 = h_nom.Clone()
    elif histosE[2]==0:
        h_3030 = h_nom.Clone()
    #print("{} {} {}".format(h_nom.Integral(),h_2030.Integral(),h_3030.Integral()))
    #return

    if varName == "mva":
        if not histosE[0]==0:
            Yields[sample]["20J2_20J3"]=h_nom.Integral()
        if not histosE[1]==0:
            Yields[sample]["20J2_30J3"]=h_2030.Integral()
        if not histosE[2]==0:
            Yields[sample]["30J2_30J3"]=h_3030.Integral()

    '''
    if doNjet:
        hist_tmp2b = hist_file433FB.Get(histName2)
        hist_tmp3b = hist_file433FB.Get(histName3)
        hist_tmp4b = hist_file433FB.Get(histName4)
        h433FB.Add(hist_tmp2b.Clone())
        h433FB.Add(hist_tmp3b.Clone())
        h433FB.Add(hist_tmp4b.Clone())
        if (sample == "ttbar" or sample == "ZJets" or sample == "WJets") and varName != "nSigJets-nMatchedTruthSigJets":
            for samp in samples[sample]:
                if "bb" in samp:
                    continue
                histName2a=histName2.replace(sample2,samp)
                histName3a=histName3.replace(sample2,samp)
                histName4a=histName4.replace(sample2,samp)
                hist_tmp454b = hist_file433FB.Get(histName2a)
                hist_tmp454c = hist_file433FB.Get(histName3a)
                hist_tmp454d = hist_file433FB.Get(histName4a)
                h433FB.Add(hist_tmp454b.Clone())
                h433FB.Add(hist_tmp454c.Clone())
                h433FB.Add(hist_tmp454d.Clone())
    '''
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h_nom.SetLineColor(ROOT.kBlack)
    h_nom.SetLineWidth(2)
    h_nom.SetLineStyle(1)
    h_2030.SetLineColor(ROOT.kRed)
    h_2030.SetLineWidth(2)
    h_2030.SetLineStyle(1)
    h_3030.SetLineColor(ROOT.kBlue)
    h_3030.SetLineWidth(2)
    h_3030.SetLineStyle(1)
    
    h_2030_sub=h_2030.Clone()
    h_3030_sub=h_3030.Clone()
    h_2030_sub.Add(h_nom,-1)
    h_3030_sub.Add(h_nom,-1)
    h_2030_sub.SetLineStyle(5)
    h_3030_sub.SetLineStyle(5)
            
    rebinning = int(binning.split(",")[0])
    xLow = float(binning.split(",")[1])
    xHigh = float(binning.split(",")[2])

    #print(rebinning)
    if rebinning != 0:
        h_nom.Rebin(rebinning)
        h_2030.Rebin(rebinning)
        h_3030.Rebin(rebinning)
        h_2030_sub.Rebin(rebinning)
        h_3030_sub.Rebin(rebinning)

    if varName == "MET":
        if "150_250ptv" in canName:
            xLow = 100
            xHigh = 300
        elif "250_400ptv" in canName:
            xLow = 200
            xHigh = 450
        elif "400ptv" in canName:
            xLow = 350
            xHigh = 700
    #print("{} {}".format(xLow,xHigh))
    h_nom.GetXaxis().SetRangeUser(xLow,xHigh)
    h_2030.GetXaxis().SetRangeUser(xLow,xHigh)
    h_3030.GetXaxis().SetRangeUser(xLow,xHigh)
    h_2030_sub.GetXaxis().SetRangeUser(xLow,xHigh)
    h_3030_sub.GetXaxis().SetRangeUser(xLow,xHigh)


    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h_nom.GetMaximum(), h_2030.GetMaximum(), h_3030.GetMaximum())*1.4
    y_min = min(h_2030_sub.GetMinimum(), h_3030_sub.GetMinimum())
                    
    print(y_min)
    rat2030 = h_2030.Clone()
    rat2030.SetLineColor(ROOT.kRed)
    rat3030 = h_3030.Clone()
    rat3030.SetLineColor(ROOT.kBlue)
    h_nom.Draw("hist")
    h_nom.SetTitle("")
    h_nom.SetAxisRange(y_min, y_max, "Y")
    h_nom.SetAxisRange(xLow, xHigh, "X")
    h_nom.GetYaxis().SetMaxDigits(3)
    h_nom.GetYaxis().SetTitle("Entries")
    h_nom.GetYaxis().SetTitleSize(0.04)
    h_nom.GetYaxis().SetTitleOffset(1.2)
    h_nom.GetYaxis().SetLabelSize(0.05)
    h_nom.SetYTitle("Entries")
    h_nom.SetLabelOffset(5)
    
    h_2030.Draw("same hist")
    h_3030.Draw("same hist")
    h_2030_sub.Draw("same hist")
    h_3030_sub.Draw("same hist")
    
    '''
    if ("mBB" in varName or "Mbb" in varName) and sample == "signal":
        fit_pars = []
        H = ROOT.TH1F()
        H1 = ROOT.TH1F()
        H2 = ROOT.TH1F()
        H3 = ROOT.TH1F()
        H4 = ROOT.TH1F()
        H5 = ROOT.TH1F()
        H6 = ROOT.TH1F()
        H7 = ROOT.TH1F()
        H,H1,H2,H3,H4,H5,H6,H7,fit_pars=MbbFitter(h422.Clone(),h422F.Clone(),h423.Clone(),h423F.Clone(),h423FB.Clone(),h433.Clone(),h433F.Clone(),h433FB.Clone())

        H.SetLineColor(ROOT.kBlack)
        H1.SetLineColor(ROOT.kGray+2)
        H2.SetLineColor(ROOT.kBlue)
        H3.SetLineColor(ROOT.kRed)
        H4.SetLineColor(ROOT.kGreen+3)
        H5.SetLineColor(ROOT.kViolet-1)
        H6.SetLineColor(ROOT.kOrange+1)
        H7.SetLineColor(ROOT.kYellow-3)
        

        H.Draw('c hist same')
        H1.Draw('c hist same')
        H2.Draw('c hist same')
        H3.Draw('c hist same')
        H4.Draw('c hist same')
        H5.Draw('c hist same')
        H6.Draw('c hist same')
        H7.Draw('c hist same')

        #    legend.SetHeader("    Jet type,  peak,  width,  integral")
        #    legend.AddEntry(h, "EMTopo, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[0],fit_pars[1],h.Integral()), "l")
        #    legend.AddEntry(h1, "PFlow, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[2],fit_pars[3],h1.Integral()), "l")
        legend1 = ROOT.TLegend(0.605, 0.6, 0.93, 0.87)
        legend1.SetHeader("     sample,  peak,  width,   SoW")
        legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[0],fit_pars[1],h422.Integral()), "l")
        legend1.AddEntry(h422F, "20J2-20J3-FSR, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[2],fit_pars[3],h422F.Integral()), "l")
        legend1.AddEntry(h423, "20J2-30J3, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[4],fit_pars[5],h423.Integral()), "l")
        legend1.AddEntry(h423F, "20J2-30J3-FSR, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[6],fit_pars[7],h423F.Integral()), "l")
        legend1.AddEntry(h423FB, "20J2-30J3-FSR-b4Cuts, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[8],fit_pars[9],h423FB.Integral()), "l")
        legend1.AddEntry(h433, "30J2-30J3, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[10],fit_pars[11],h433.Integral()), "l")
        legend1.AddEntry(h433F, "30J2-30J3-FSR, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[12],fit_pars[13],h433F.Integral()), "l")
        legend1.AddEntry(h433FB, "30J2-30J3-FSR-b4Cuts, {:.1f}, {:.1f}, {:.1f}".format(fit_pars[14],fit_pars[15],h433FB.Integral()), "l")
        legend1.SetBorderSize(0)
        legend1.SetFillColor(0)
        legend1.SetFillStyle(0)
        legend1.Draw()
    '''
    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(h_nom, "20J2-20J3, {:.1f}, {:.1f}".format(h_nom.GetEntries(),h_nom.Integral()), "l")
    legend1.AddEntry(h_2030, "20J2-30J3, {:.1f}, {:.1f}".format(h_2030.GetEntries(),h_2030.Integral()), "l")
    legend1.AddEntry(h_3030, "30J2-30J3, {:.1f}, {:.1f}".format(h_3030.GetEntries(),h_3030.Integral()), "l")
    legend1.AddEntry(h_2030_sub, "20J2-30J3-sub, {:.1f}, {:.1f}".format(h_2030_sub.GetEntries(),h_2030_sub.Integral()), "l")
    legend1.AddEntry(h_3030_sub, "30J2-30J3-sub, {:.1f}, {:.1f}".format(h_3030_sub.GetEntries(),h_3030_sub.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, sample)
    t.DrawLatex(0.15, 0.75, "ATLAS #it{#bf{Internal}}")

    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)

    rat2030.Divide(h_nom)
    rat3030.Divide(h_nom)
    rat2030.SetLineStyle(1)
    rat2030.SetLineColor(ROOT.kRed)
    rat2030.SetMarkerColor(ROOT.kRed)
    rat3030.SetLineStyle(1)
    rat3030.SetLineColor(ROOT.kBlue)
    rat3030.SetMarkerColor(ROOT.kBlue)

    rat2030.SetTitle("")
    rat2030.GetXaxis().SetTitle("")
    rat2030.GetXaxis().SetTitleSize(0.09)
    rat2030.GetXaxis().SetTitleOffset(1.05)
    rat2030.SetAxisRange(xLow, xHigh, "X")

    yLow = 0.9
    yHigh = 3
    if "3jet" in canName:
        yHigh = 2
        yLow = 0
    elif "4jet" in canName or "4pjet" in canName:
        yHigh = 1.5
        yLow = 0

    if sample == "ttbar" or sample == "stop":
        yHigh+= 0.5
    rat2030.SetAxisRange(yLow, yHigh, "Y")
    rat2030.GetXaxis().SetLabelSize(0.09)
    rat2030.GetXaxis().SetTitle(varName)
    rat2030.GetYaxis().SetLabelSize(0.07)
    rat2030.GetYaxis().SetTitleOffset(0.7)
    rat2030.GetYaxis().SetTitleSize(0.06)
    rat2030.GetYaxis().SetTitle("Cuts/Orig")
    rat2030.GetYaxis().SetNdivisions(506)
    
    rat2030.Draw("p") 
    rat3030.Draw("same p") 
    
    if doNjet:
        varName+="_AllJets"
        
    canvas_hs.SaveAs(working_dir+line[1]+"/"+canName+"_"+sample+".png")
    canvas_hs.Close()


#13TeV_ZeroLepton_2tag3jet_250_400ptv_SR_mBB.root


NUMJETS={"nJets==2":"2jet","nJets==3":"3jet","nJets==4":"4jet",} #"nJets>=5":"5pjet","nJets>=2":"2pjet"}
# Only STXS histos are split at 600. Recoo histos are only split at 400
METCUT={"MET>150&&MET<250":"150_250ptv","MET>75&&MET<150":"75_150ptv","MET>400":"400ptv","MET>250&&MET<400":"250_400ptv"}
# For flavour, 15=tau, 5=b, 4=c, <4=l
# rebin,start,end
histNames = {"mBB" : "5,30,250", "mva" : "20,-1,1",}#"dRBB" : "0,0.4,2.5", "MET" : "0,150,600"} #,"nSigJets" :ROOT.gROOT.SetBatch(True)
Leptons = ["ZeroLepton","OneLepton","TwoLepton"]


#samples = {"ttbar" : ["ttbar"],}
           #"stop" : ["stops","stopt","stopWt"]} #"diboson" : ["WZ","WW","ZZ","ggZZ","ggWW"]}
#NUMJETS={"nJets==2":"2jet"}
#METCUT={"MET>150&&MET<250":"150_250ptv"} #"MET>400":"400ptv", 
#Leptons = ["TwoLepton"]#,"OneLepton","TwoLepton"]

#main
histNamesOrder = histNames.keys()
histNamesOrder.sort()
print("Making plots")

for hName in histNamesOrder:
    print("VARIABLE "+hName)
    m_sigValsTot={}
    for numJets in NUMJETS:
        if "J3" in hName or "BBJ" in hName:
            if numJets == "nJets==2":
                continue
        if "J4" in hName:
            if numJets != "nJets==4":
                continue
        for METCut in METCUT:
            for Lep in Leptons:
                
                #if not ("dRBB" in hName or "pTB1" in hName):
                #    continue
                sys.argv.append("-b")
                ROOT.gStyle.SetPadTickX(1)
                ROOT.gStyle.SetPadTickY(1)
                
                #13TeV_ZeroLepton_2tag3jet_250_400ptv_SR_mBB.root
                histEx="13TeV_"+Lep+"_2tag"+NUMJETS[numJets]+"_"+METCUT[METCut]+"_SR_"+hName
                #CUTS=numJets+"_"+METCut+"-&&PassNonJetCountCuts==1-&&"+JetCut1
                if Lep == "TwoLepton" and numJets == "nJets==4":
                    histEx="13TeV_"+Lep+"_2tag4pjet_"+METCUT[METCut]+"_SR_"+hName
                    #CUTS="nJets>=4_"+METCut+"-&&PassNonJetCountCuts==1-&&"+JetCut1
                elif Lep == "ZeroLepton" and METCut == "MET>75&&MET<150":
                    continue
                
                    
                #if numJets == "nJets==2":
                #    if ("30" in JetCut1.split("_")[-1]):
                #        continue
                    
                print("Looking at "+hName+" with cuts "+histEx)
                ROOT.gROOT.SetBatch(True)

                binning = histNames[hName]
                cutsComparisonAllSamples(hName,binning,histEx,False)


print("")
print("")
print("")
print("All complete")





















