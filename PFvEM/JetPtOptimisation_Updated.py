"""
Study the different jet pT cuts 
"""
import os
import sys
import ROOT
import collections
import math
import copy
import re
import ctypes
import numpy as np
#import argparse

# python JetPtOptimisation_Updated.py 0L

sys.argv.append("-b")
print ("Opening files")

channel = sys.argv[1]

samples_inFile = {"WJets" : ["Wbb","Wbc","Wbl","Wcc","Wcl","Wl"], #"Wbb" : ["Wbb"], #"Wheavy" : ["Wbb","Wbc","Wbl","Wcc"],"Wlight" : ["Wcl","Wl"],
                  "ZJets" : ["Zbb","Zbc","Zbl","Zcc","Zcl","Zl"], #"Zbb" : ["Zbb"], #"Zheavy" : ["Zbb","Zbc","Zbl","Zcc"],"Zlight" : ["Zcl","Zl"],
                  "ttbar" : ["ttbar"],"data" : ["data"],
                  "stop" : ["stopWtDS","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],
                  "signal" : ["qqZvvH125","ggZvvH125","qqZvvH125cc","ggZvvH125cc","qqZllH125","ggZllH125","qqZllH125cc","ggZllH125cc","qqWlvH125","qqWlvH125cc",]}

samples_plots = {"W+bb" :["Wbb"],"W+bc":["Wbc"],"W+bl":["Wbl"],"W+cc":["Wcc"],"W+cl":["Wcl"],"W+l":["Wl"],
                 "Z+bb" :["Zbb"],"Z+bc":["Zbc"],"Z+bl":["Zbl"],"Z+cc":["Zcc"],"Z+cl":["Zcl"],"Z+l":["Zl"],
                 "ttbar" : ["ttbar"], "data" : ["data"],
                 "Wt":["stopWtDS"],"s+t chan":["stopt","stops"],"ZZ":["ZZ","ggZZ",],"WZ":["WZ"],"WW":["WW","ggWW"],
                 "VH 125" : ["qqZvvH125","ggZvvH125","qqZvvH125cc","ggZvvH125cc","qqZllH125","ggZllH125","qqZllH125cc","ggZllH125cc","qqWlvH125","qqWlvH125cc",]}

samples_yields = {"W+bb" :["Wbb"],"W+bc":["Wbc"],"W+bl":["Wbl"],"W+cc":["Wcc"],"W+cl":["Wcl"],"W+l":["Wl"],
                 "Z+bb" :["Zbb"],"Z+bc":["Zbc"],"Z+bl":["Zbl"],"Z+cc":["Zcc"],"Z+cl":["Zcl"],"Z+l":["Zl"],
                 "ttbar" : ["ttbar"], "data" : ["data"],
                 "stop" : ["stopWtDS","stopt","stops"],"diboson" : ["ZZ","WZ","ggZZ","WW","ggWW"],
                 "signal" : ["qqZvvH125","ggZvvH125","qqZvvH125cc","ggZvvH125cc","qqZllH125","ggZllH125","qqZllH125cc","ggZllH125cc","qqWlvH125","qqWlvH125cc",]}

'''
samples = {"WJets" : ["WJetsbb","WJetsbc","WJetsbl","WJetscc","WJetscl","WJetsl"], 
           "ZJets" : ["ZJetsbb","ZJetsbc","ZJetsbl","ZJetscc","ZJetscl","ZJetsl"], 
           "ttbar" : ["ttbar"],
           "stop" : ["stop"],"diboson" : ["diboson"],"data" : ["data"],
           "signal" : ["signal"]}
'''
colours = {"VH 125" : ROOT.kRed, 
           "signal" : ROOT.kRed,
           "WJets" : ROOT.kGreen+4,
           "W+bb" : ROOT.kGreen+4, 
           "W+bc" : ROOT.kGreen+3,
           "W+bl" : ROOT.kGreen+2,
           "W+cc" : ROOT.kGreen+1,
           "W+cl" : ROOT.kGreen-6,
           "W+l" : ROOT.kGreen-9,
           "ZJets" : ROOT.kAzure+2,
           "Z+bb" : ROOT.kAzure+2, 
           "Z+bc" : ROOT.kAzure+1, 
           "Z+bl" : ROOT.kAzure-2, 
           "Z+cc" : ROOT.kAzure-4, 
           "Z+cl" : ROOT.kAzure-8, 
           "Z+l" : ROOT.kAzure-9, 
           "ttbar" : ROOT.kOrange, 
           "stop" : ROOT.kOrange-1,
           "s+t chan" : ROOT.kOrange-1, 
           "Wt": ROOT.kYellow - 7,
           "diboson" : ROOT.kGray,
           "WW": ROOT.kGray + 3,
           "WZ" : ROOT.kGray, 
           "ZZ": ROOT.kGray + 1,
           "data" : ROOT.kBlack}

scalefactors = {"ttbar2jet" : 0.98, "ttbar3jet" : 0.93, "ttbar4jet" : 0.93, 
                "WHF2jet" : 1.06, "WHF3jet" : 1.15, "WHF4jet" : 1.15, 
                "ZHF2jet" : 1.16, "ZHF3jet" : 1.09, "ZHF4jet" : 1.09,} # 32-15
scalefactors = {"ttbar2jet" : 0.97, "ttbar3jet" : 0.92, "ttbar4jet" : 0.91, 
                "WHF2jet" : 1.18, "WHF3jet" : 1.13, "WHF4jet" : 1.13, 
                "ZHF2jet" : 1.33, "ZHF3jet" : 1.21, "ZHF4jet" : 1.18,} # 33-05
scalefactors25J3 = {"ttbar2jet" : 0.97, "ttbar3jet" : 0.92, "ttbar4jet" : 0.91, 
                "WHF2jet" : 1.18, "WHF3jet" : 1.13, "WHF4jet" : 1.13, 
                "ZHF2jet" : 1.33, "ZHF3jet" : 1.21, "ZHF4jet" : 1.18,} # 33-05
scalefactors30J3 = {"ttbar2jet" : 0.97, "ttbar3jet" : 0.92, "ttbar4jet" : 0.91, 
                "WHF2jet" : 1.18, "WHF3jet" : 1.13, "WHF4jet" : 1.13, 
                "ZHF2jet" : 1.33, "ZHF3jet" : 1.21, "ZHF4jet" : 1.18,} # 33-05

m_sigVals={}
m_sigValsTot={}
m_yieldVals={}
m_SBVals={}
m_sigValsMVA={}
m_yieldValsMVA={}
m_SBValsMVA={}

saveHistos = True
working_dir = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation_Updated/plots/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/"
#working_dir = "/afs/cern.ch/user/r/ratkin/PFvEMTopo/Condor/"

if not os.path.isdir(working_dir):
    os.makedirs(working_dir)
if not os.path.isdir(working_dir+"JetMigrations/"):
    os.makedirs(working_dir+"JetMigrations/")
if not os.path.isdir(working_dir+"DataMC/"):
    os.makedirs(working_dir+"DataMC/")
if not os.path.isdir(working_dir+"compareDataMC/"):
    os.makedirs(working_dir+"compareDataMC/")
if not os.path.isdir(working_dir+"cutsCompare/"):
    os.makedirs(working_dir+"cutsCompare/")
'''
if not os.path.isdir(working_dir+"cutsCompare/signal/"):
    os.makedirs(working_dir+"cutsCompare/signal/")
if not os.path.isdir(working_dir+"cutsCompare/diboson/"):
    os.makedirs(working_dir+"cutsCompare/diboson/")
if not os.path.isdir(working_dir+"cutsCompare/stop/"):
    os.makedirs(working_dir+"cutsCompare/stop/")
if not os.path.isdir(working_dir+"cutsCompare/ttbar/"):
    os.makedirs(working_dir+"cutsCompare/ttbar/")
if not os.path.isdir(working_dir+"cutsCompare/WJets/"):
    os.makedirs(working_dir+"cutsCompare/WJets/")
if not os.path.isdir(working_dir+"cutsCompare/ZJets/"):
    os.makedirs(working_dir+"cutsCompare/ZJets/")
if not os.path.isdir(working_dir+"Significances/"):
    os.makedirs(working_dir+"Significances/")
'''
#hist_file_name = working_dir+"JetPtOptimisation.root"
#hist_file_name = "/afs/cern.ch/work/r/ratkin/ReaderHistOutput/PFvsEMTopo_Output/JetPtOptimisation/histsHadd/"+"JetOpt.root"

FileName = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VHLegacy/statArea/inputs_33-05/ZeroLep/LimitHistograms.VH.vvbb.13TeV.mc16ade.UCT.v01.root"
if channel== "1L":
    FileName = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VHLegacy/statArea/inputs_33-05/OneLep/LimitHistograms.VH.lvbb.13TeV.mc16ade.UCL.v01.root"
elif channel =="2L":
    FileName = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VHLegacy/statArea/inputs_33-05/TwoLep/LimitHistograms.VH.llbb.13TeV.mc16ade.UCT.v01.root"

hist_file = ROOT.TFile(FileName, "READ")
YieldFileName = "JetOptTables_Updated_"+channel+".tex"

assert hist_file.IsOpen()

W=open(working_dir+YieldFileName,'w')
W.write(r"\documentclass[aspectratio=916]{beamer}")
W.write("\n")
W.write(r"\mode<presentation> {")
W.write("\n")
W.write(r"    \usetheme{default}}")
W.write("\n")
W.write(r"    \usepackage{graphicx}")
W.write("\n")
W.write(r"    \usepackage{booktabs}")
W.write("\n")
W.write(r"    \usepackage{caption}")
W.write("\n")
W.write(r"    \usepackage{subcaption}")
W.write("\n")
W.write(r"    \setbeamerfont{page number in head/foot}{size=\tiny}")
W.write("\n")
W.write(r"    \begin{document}")
W.write("\n")        
W.write(r"\end{document} % move this to end of file")
W.write("\n")
W.close()



def getSignificance(S,B,vtag):
    i = S.FindBin(30)
    binMax = S.FindBin(250)
    summ=0
    while i <= binMax:
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)

def getSignificanceMVA(S,B,vtag):
    i = 1
    summ=0
    while i <= S.GetNbinsX():
        s=S.GetBinContent(i)
        b=B.GetBinContent(i)
        if not (b<=0 or s<=0):
            summ+=2*((s+b)*math.log(1+s/b)-s)
        i+=1
    print("Significance for {}: {}".format(vtag,math.sqrt(summ)))
    return math.sqrt(summ)
    
def rebinTrafoDHist(histo, bins): 
    histoOld = ROOT.TH1F(histo) 
    histo.Reset()
    newNBins = len(bins)-1
    
    nBinEdges = len(bins) # add one more bin for plotting
    binEdges = [None]*nBinEdges
    for iBinEdge in range(0, nBinEdges) :
        iBin = bins[nBinEdges - iBinEdge - 1] # vector is reverse-ordered
        if (iBin == 0): 
            iBin = 1
        if (iBin == histoOld.GetNbinsX() + 2): 
            iBin = histoOld.GetNbinsX() + 1
    
        binEdges[iBinEdge] = histoOld.GetBinLowEdge(iBin)
    
    #    binEdges[nBinEdges - 1] = 1.8; # additional bin for plotting
    binEdges = np.array(binEdges , dtype="d") 
    histo.SetBins(newNBins, binEdges)
  
    for iBinNew in range(1, newNBins+1):
        # vector is reverse-ordered
        iBinLow  = bins[newNBins - iBinNew + 1] 
        iBinHigh = bins[newNBins - iBinNew] - 1 
        
        err = ctypes.c_double(0.)
        integral = histoOld.IntegralAndError(iBinLow, iBinHigh, err)
        histo.SetBinContent(iBinNew, integral)
        histo.SetBinError(iBinNew, err.value)
  
    oldNbins = histoOld.GetNbinsX()
    diff = 1. - float(histo.Integral(0, newNBins + 1)) / histoOld.Integral(0, oldNbins + 1)
    # double diff = 1 - histo -> GetSumOfWeights() / histoOld -> GetSumOfWeights();
    if (abs(diff) > 1.e-7) :
        print("WARNING: sizeable difference in transformation of {} found. Integrals: (old-new)/old = {}".format(
            histo.GetName()   , diff              ))
        

def applyTrafoDToHist(hist_sig, hist_bkg, vtag):
    Zs=0
    Zb=0
    if "150_250" in vtag or "75_150" in vtag:
        Zs=10
        Zb=5
    else:
        Zs=5
        Zb=3
    # rebin histogram based on trafoD binning 
    newBins = getTrafoD(hist_sig, hist_bkg, Zs, Zb)
    rebinTrafoDHist(hist_sig, newBins)
    rebinTrafoDHist(hist_bkg, newBins)
    return(newBins)

def getTrafoD(hist_sig, hist_bkg, Zsig, Zbkg, MCstatUpBound = 0.2) : 
    histoSig = ROOT.TH1F(hist_sig) 
    histoBkg = ROOT.TH1F(hist_bkg) 
    
    bins = [] 
    nBins = histoBkg.GetNbinsX()
    iBin = nBins + 1; # start with overflow bin
    bins.append(nBins+2)
    
    nBkgerr = ctypes.c_double(0.)
    
    nBkg = histoBkg.IntegralAndError(0, nBins+1, nBkgerr)
    nSig = histoSig.Integral(0, nBins+1)
    #print("iBin {} nBkg {} nSig {}".format(iBin,nBkg,nSig))
    while (iBin > 0) :
        #print(iBin)
        sumBkg    = 0.
        sumSig    = 0.
        err2Bkg   = 0.
        bool_pass = False
        
        dist = 1e10
        distPrev = 1e10

        while (not bool_pass and iBin >= 0) :
            nBkgBin  = histoBkg.GetBinContent(iBin)
            nSigBin  = histoSig.GetBinContent(iBin)
            sumBkg  += nBkgBin
            sumSig  += nSigBin
            err2Bkg += pow(histoBkg.GetBinError(iBin),2)
            
            #print("  iBin {} sunBkg {} sumSig {} err2Bkg {}".format(iBin,sumBkg,sumSig,err2Bkg))
            err2RelBkg = 1.
            if (sumBkg != 0): 
                err2RelBkg = err2Bkg / pow(sumBkg, 2)
            
            err2Rel = 1.
            # "trafo D"
            if (sumBkg != 0 and sumSig != 0) :
                err2Rel = 1. / (sumBkg / (nBkg / Zbkg) + sumSig / (nSig / Zsig))
            elif (sumBkg != 0) : 
                err2Rel = (nBkg / Zbkg) / sumBkg
            elif (sumSig != 0) : 
                err2Rel = (nSig / Zsig) / sumSig

            bool_pass = (np.sqrt(err2Rel) < 1 and np.sqrt(err2RelBkg) < MCstatUpBound)
            # distance
            dist = np.abs(err2Rel - 1)
            #print("  err2RelBkg {} err2Rel {} bool_pass {} dist {} distPrev {}".format(err2RelBkg,err2Rel,bool_pass,dist,distPrev))
            if (not (bool_pass and dist > distPrev)): 
                iBin-=1
            #else : 
                # use previous bin
            distPrev = dist
        # remove last bin
        if (iBin+1 == 0 and len(bins) > 1 and len(bins) > Zsig + Zbkg + 0.01): 
            # remove last bin if Nbin > Zsig + Zbkg
            # (1% threshold to capture rounding issues)
            bins.pop()
        #print("appending "+str(iBin+1))
        bins.append(iBin+1)
    return bins


def doBlinding(S1,B1,h1,var):
    thresh = 0.1
    if ("mBB" in var or "Mbb" in var) and "J" not in var:
        i = h1.FindBin(80)
        binMax = h1.FindBin(139.9)
        while i <= binMax:
            h1.SetBinContent(i, 0)
            h1.SetBinError(i, 0)
            i+=1
    elif "mva" in var:
        tot_sig = S1.Integral()
        if not tot_sig > 0: 
            return h1
        sum_sig=0
        i=h1.GetNbinsX()
        #while h1.GetNbinsX()-i<5:
        #    h1.SetBinContent(i, 0)
        #    h1.SetBinError(i, 0)
        #    i-=1
        while i>0:
            sum_sig+=S1.GetBinContent(i)
            if sum_sig/tot_sig < 0.7:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
                i-=1
            else:
                break
    else:
        i=1
        while i<=h1.GetNbinsX():
            s=S1.GetBinContent(i)
            b=B1.GetBinContent(i)
            if s+b == 0:
                i+=1
                continue
            elif s/(s+b) > thresh:
                h1.SetBinContent(i, 0)
                h1.SetBinError(i, 0)
            i+=1

    return h1

def MbbFitter(h422,h4225,h423,h432,h433):
    #h.SetLineColor(ROOT.kBlue)
    #h.SetFillColor(ROOT.kBlue)
    
    m = ROOT.RooRealVar("m","m",25,200)
    B_xp = ROOT.RooRealVar("B_xp","B_xp",121.5,100,130)
    B_sig = ROOT.RooRealVar("B_sig","B_sig",15,5,25)
    B_xi = ROOT.RooRealVar("B_xi","B_xi",0,-1,1)
    B_rho1 = ROOT.RooRealVar("B_rho1","B_rho1", 0,-1,1)
    B_rho2 = ROOT.RooRealVar("B_rho2","B_rho2", 0,-1,1)

    B_xp_1 = ROOT.RooRealVar("B_xp_1","B_xp_1",121.5,100,130)
    B_sig_1 = ROOT.RooRealVar("B_sig_1","B_sig_1",15,5,25)
    B_xi_1 = ROOT.RooRealVar("B_xi_1","B_xi_1",0,-1,1)
    B_rho1_1 = ROOT.RooRealVar("B_rho1_1","B_rho1_1", 0,-1,1)
    B_rho2_1 = ROOT.RooRealVar("B_rho2_1","B_rho2_1", 0,-1,1)

    B_xp_2 = ROOT.RooRealVar("B_xp_2","B_xp_2",121.5,100,130)
    B_sig_2 = ROOT.RooRealVar("B_sig_2","B_sig_2",15,5,25)
    B_xi_2 = ROOT.RooRealVar("B_xi_2","B_xi_2",0,-1,1)
    B_rho1_2 = ROOT.RooRealVar("B_rho1_2","B_rho1_2", 0,-1,1)
    B_rho2_2 = ROOT.RooRealVar("B_rho2_2","B_rho2_2", 0,-1,1)

    B_xp_3 = ROOT.RooRealVar("B_xp_3","B_xp_3",121.5,100,130)
    B_sig_3 = ROOT.RooRealVar("B_sig_3","B_sig_3",15,5,25)
    B_xi_3 = ROOT.RooRealVar("B_xi_3","B_xi_3",0,-1,1)
    B_rho1_3 = ROOT.RooRealVar("B_rho1_3","B_rho1_3", 0,-1,1)
    B_rho2_3 = ROOT.RooRealVar("B_rho2_3","B_rho2_3", 0,-1,1)

    B_xp_4 = ROOT.RooRealVar("B_xp_4","B_xp_4",121.5,100,130)
    B_sig_4 = ROOT.RooRealVar("B_sig_4","B_sig_4",15,5,25)
    B_xi_4 = ROOT.RooRealVar("B_xi_4","B_xi_4",0,-1,1)
    B_rho1_4 = ROOT.RooRealVar("B_rho1_4","B_rho1_4", 0,-1,1)
    B_rho2_4 = ROOT.RooRealVar("B_rho2_4","B_rho2_4", 0,-1,1)

    
    bukin = ROOT.RooBukinPdf("bukin","bukin",m,B_xp,B_sig,B_xi,B_rho1,B_rho2)
    bukin1 = ROOT.RooBukinPdf("bukin1","bukin1",m,B_xp_1,B_sig_1,B_xi_1,B_rho1_1,B_rho2_1)
    bukin2 = ROOT.RooBukinPdf("bukin2","bukin2",m,B_xp_2,B_sig_2,B_xi_2,B_rho1_2,B_rho2_2)
    bukin3 = ROOT.RooBukinPdf("bukin3","bukin3",m,B_xp_3,B_sig_3,B_xi_3,B_rho1_3,B_rho2_3)
    bukin4 = ROOT.RooBukinPdf("bukin4","bukin4",m,B_xp_4,B_sig_4,B_xi_4,B_rho1_4,B_rho2_4)

    #h.Rebin(5)
    #h1.Rebin(5)
    dH = ROOT.RooDataHist("dH","dH",ROOT.RooArgList(m),h422)
    dH1 = ROOT.RooDataHist("dH1","dH1",ROOT.RooArgList(m),h4225)
    dH2 = ROOT.RooDataHist("dH2","dH2",ROOT.RooArgList(m),h423)
    dH3 = ROOT.RooDataHist("dH3","dH3",ROOT.RooArgList(m),h432)
    dH4 = ROOT.RooDataHist("dH4","dH4",ROOT.RooArgList(m),h433)
    
    mframe = m.frame()
    
    fit = bukin.fitTo(dH,ROOT.RooFit.Save())
    fit1 = bukin1.fitTo(dH1,ROOT.RooFit.Save())
    fit2 = bukin2.fitTo(dH2,ROOT.RooFit.Save())
    fit3 = bukin3.fitTo(dH3,ROOT.RooFit.Save())
    fit4 = bukin4.fitTo(dH4,ROOT.RooFit.Save())
    #fit2.Print()
    
    #dH.plotOn(mframe,ROOT.RooFit.Name("Data"))
    #dH1.plotOn(mframe,ROOT.RooFit.Name("Data1"))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("Fit"),ROOT.RooFit.LineColor(2))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("Fit1"),ROOT.RooFit.LineColor(3))
    #mframe.Draw()
    #chi2 = mframe.chiSquare("Fit","Data",5)
    #chi21 = mframe.chiSquare("Fit1","Data1",5)
    #print("chi2: {} chi21: {}".format(chi2,chi21))

    HI = bukin.createHistogram('m', 175)
    HI1 = bukin1.createHistogram('m', 175)
    HI2 = bukin2.createHistogram('m', 175)
    HI3 = bukin3.createHistogram('m', 175)
    HI4 = bukin4.createHistogram('m', 175)
    #print(h.Integral("width"), h.GetBinContent(100))
    #print(HI.Integral("width"), HI.GetBinContent(100))
    #print(h.Integral("width")/HI.Integral("width"))
    #print(h.GetBinContent(h.GetXaxis().FindBin(100))/HI.GetBinContent(HI.GetXaxis().FindBin(100)))
    HI.Scale(h422.Integral("width")/HI.Integral("width"))
    HI1.Scale(h4225.Integral("width")/HI1.Integral("width"))
    HI2.Scale(h423.Integral("width")/HI2.Integral("width"))
    HI3.Scale(h432.Integral("width")/HI3.Integral("width"))
    HI4.Scale(h433.Integral("width")/HI4.Integral("width"))
    #HI.Scale(h.Integral()/HI.Integral())
    #HI1.Scale(h1.Integral()/HI1.Integral())
    #print(HI.Integral(), HI.GetBinContent(100))
    HI.SetFillStyle(0)
    HI.SetLineWidth(2)
    HI.SetLineStyle(2)
    HI1.SetFillStyle(0)
    HI1.SetLineWidth(2)
    HI1.SetLineStyle(2)
    HI2.SetFillStyle(0)
    HI2.SetLineWidth(2)
    HI2.SetLineStyle(2)
    HI3.SetFillStyle(0)
    HI3.SetLineWidth(2)
    HI3.SetLineStyle(2)
    HI4.SetFillStyle(0)
    HI4.SetLineWidth(2)
    HI4.SetLineStyle(2)

    mean = B_xp.getVal()
    sigma = B_sig.getVal()
    mean1 = B_xp_1.getVal()
    sigma1 = B_sig_1.getVal()
    mean2 = B_xp_2.getVal()
    sigma2 = B_sig_2.getVal()
    mean3 = B_xp_3.getVal()
    sigma3 = B_sig_3.getVal()
    mean4 = B_xp_4.getVal()
    sigma4 = B_sig_4.getVal()

    return HI,HI1,HI2,HI3,HI4,[mean,sigma,mean1,sigma1,mean2,sigma2,mean3,sigma3,mean4,sigma4]
    
    #dH.plotOn(mframe,ROOT.RooFit.DrawOption("B"),ROOT.RooFit.FillColorAlpha(0,0),ROOT.RooFit.LineColor(ROOT.kBlue), ROOT.RooFit.MarkerColor(ROOT.kBlue))
    #dH1.plotOn(mframe,ROOT.RooFit.LineColor(ROOT.kRed), ROOT.RooFit.MarkerColor(ROOT.kRed))
    #bukin.plotOn(mframe,ROOT.RooFit.Name("bukin"),ROOT.RooFit.LineColor(ROOT.kBlue),ROOT.RooFit.LineStyle(1))
    #bukin1.plotOn(mframe,ROOT.RooFit.Name("bukin1"),ROOT.RooFit.LineColor(ROOT.kRed),ROOT.RooFit.LineStyle(1))
    
    '''
    leg1 = ROOT.TLegend(0.2,0.75,0.6,0.85)
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetTextSize(0.03)
    leg1.SetHeader("   Jet type  peak  width")
    leg1.AddEntry(mframe.findObject("bukin"),"EMTopo {:.2f} {:.2f}".format(mean, sigma), "L")
    leg1.AddEntry(mframe.findObject("bukin1"),"PFlow {:.2f} {:.2f}".format(mean1, sigma1), "L")
    
    can = ROOT.TCanvas("massframe","massframe",750,800)
    can.cd()
    can.SetTickx()
    can.SetTicky()
    h.Draw('hist')
    h1.Draw('hist same')
    HI.Draw('c hist same')
    HI1.Draw('c hist same')
    
    #bukin.Draw('same')
    #fit.Draw('same')
    
    xTitle = ""
    words = histName.split("_")
    for word in words:
        if "Mbb" in word:
            xTitle = "#bf{"+word+"} [GeV]"
    
    mframe.Draw()
    mframe.SetTitle("")
    mframe.GetXaxis().SetTitle(xTitle)
    mframe.GetYaxis().SetTitle("Events")
    mframe.SetMaximum(mframe.GetMaximum()*1.2)
    leg1.Draw("same")
    mframe.GetYaxis().SetTitle("Events")
    
    can.SaveAs(plot_dir+histName+".png")
    '''


def printYields(yields,canName,applyScales):
    
    W=open(working_dir+YieldFileName,'a')
    canName=canName.replace("_","-")
    if applyScales:
        canName += "-scaled"

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{prefit "+canName+"}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.25\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J3}} & \multicolumn{1}{c|}{\textbf{25J3}} & \multicolumn{1}{c}{\textbf{30J3}} \\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for samp in ["data","signal","diboson","stop","ttbar",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",]:
        nom = yields[samp]["20J3"]
        if nom ==0.0:
            nom=1
        W.write(r"    {} & {:.1f} & {:.1f} ({:.0f}$\%$) & {:.1f} ({:.0f}$\%$) \\ ".format(
            samp,nom,
            yields[samp]["25J3"],100*(yields[samp]["25J3"]/nom-1),
            yields[samp]["30J3"],100*(yields[samp]["30J3"]/nom-1)))
        
        W.write("\n")
        if samp == "ttbar" or samp == "Z+l":
             W.write(r"    \midrule")
             W.write("\n")

    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()

def printSignificance(hName):
    
    W=open("JetOptTables.tex",'a')

    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.write(r"\begin{frame}")
    W.write("\n")
    W.write(r"\begin{table}")
    W.write("\n")
    W.write(r"\begin{center}")
    W.write("\n")
    W.write(r"\caption{"+hName+" Significances}")
    W.write("\n")
    W.write(r"\resizebox{!}{0.30\linewidth}{")
    W.write("\n")
    W.write(r"    \begin{tabular}{c|c|c|c|c|c}")
    W.write("\n")
    W.write(r"    \toprule")
    W.write("\n")
    W.write(r"    & \multicolumn{1}{c|}{\textbf{20J2\_20J3}} & \multicolumn{1}{c|}{\textbf{20J2\_25J3}} & \multicolumn{1}{c|}{\textbf{20J2\_30J3}} & \multicolumn{1}{c|}{\textbf{30J2\_20J3}} & \textbf{30J2\_30J3}\\")
    W.write("\n")
    W.write(r"    \midrule")
    W.write("\n")
    for JETS in ["2jet","3jet","4jet","2$+$3jet","2$+$3$+$4jet"]:
        for MET in ["150_250ptv","250ptv","250_400ptv","400ptv","150ptv"]:
            keyName=JETS+"_"+MET
            keyName1=keyName.replace("_","-")

            

            if JETS == "2$+$3$+$4jet":
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\ ".format(
                    keyName1,
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["4jet_"+MET]["20J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_25J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_25J3"]**2+m_sigValsTot["4jet_"+MET]["20J2_25J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["4jet_"+MET]["20J2_30J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_20J3"]**2+m_sigValsTot["4jet_"+MET]["30J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["4jet_"+MET]["30J2_30J3"]**2)))
                W.write("\n")
            elif JETS == "2$+$3jet":
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\ ".format(
                    keyName1,
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_25J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_25J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["20J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["20J2_30J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_20J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_20J3"]**2),
                    math.sqrt(m_sigValsTot["2jet_"+MET]["30J2_30J3"]**2+m_sigValsTot["3jet_"+MET]["30J2_30J3"]**2)))
                W.write("\n")
            else:
                W.write(r"    {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\ ".format(
                    keyName1,m_sigValsTot[keyName]["20J2_20J3"],m_sigValsTot[keyName]["20J2_25J3"],
                    m_sigValsTot[keyName]["20J2_30J3"],m_sigValsTot[keyName]["30J2_20J3"],m_sigValsTot[keyName]["30J2_30J3"]))
                W.write("\n")
        W.write(r"    \midrule")
        W.write("\n")
    W.write(r"    ")
    W.write("\n")
    W.write(r"    \bottomrule")
    W.write("\n")
    W.write(r"    \end{tabular}}")
    W.write("\n")
    W.write(r"\end{center}")
    W.write("\n")
    W.write(r"\end{table}")
    W.write("\n")
    W.write(r"\end{frame}")
    W.write("\n")
    W.write("\n")
    W.write("\n")
    W.close()


def compareDataMC(varName,canName,applyScales):
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    histo452020={}
    histo452025={}
    histo452030={}
    Yields={"W+bb" : {}, "W+bc" : {}, "W+bl" : {}, "W+cc" : {}, "W+cl" : {}, "W+l" : {}, 
            "Z+bb" : {}, "Z+bc" : {}, "Z+bl" : {}, "Z+cc" : {}, "Z+cl" : {}, "Z+l" : {}, 
            "ttbar" : {}, "stop" : {}, "diboson" : {}, "data" : {}, "signal" : {}, }
    line=canName.split("_")
    canName1=canName
    hist_file.cd()
    #print("Looping to Get histograms")
    for sampl in samples_yields:
        for samp in samples_yields[sampl]:
            histName1=samp+"_"+canName1+"_"+varName+"20J3"
            #print("Getting hist "+histName1)
            hist_tmp1 = hist_file.Get(histName1)
            if sampl in histo452020:
                try:
                    histo452020[sampl].Add(hist_tmp1.Clone())
                except:
                    print("no exist")
            else:
                try:
                    histo452020[sampl]=hist_tmp1.Clone()
                except:
                    print("no exist")

            histName1=samp+"_"+canName1+"_"+varName+"25J3"
            #print("Getting hist "+histName1)
            hist_tmp2 = hist_file.Get(histName1)
            if sampl in histo452025:
                try:
                    histo452025[sampl].Add(hist_tmp2.Clone())
                except:
                    print("no exist")
            else:
                try:
                    histo452025[sampl]=hist_tmp2.Clone()
                except:
                    print("no exist")

            histName1=samp+"_"+canName1+"_"+varName+"30J3"
            #print("Getting hist "+histName1)
            hist_tmp3 = hist_file.Get(histName1)
            if sampl in histo452030:
                try:
                    histo452030[sampl].Add(hist_tmp3.Clone())
                except:
                    print("no exist")
            else:
                try:
                    histo452030[sampl]=hist_tmp3.Clone()
                except:
                    print("no exist")

        if sampl in histo452020:
            Yields[sampl]["20J3"]=histo452020[sampl].Integral()
        else:
            Yields[sampl]["20J3"]=0
        if sampl in histo452025:
            Yields[sampl]["25J3"]=histo452025[sampl].Integral()
        else:
            Yields[sampl]["25J3"]=0
        if sampl in histo452030:
            Yields[sampl]["30J3"]=histo452030[sampl].Integral()
        else:
            Yields[sampl]["30J3"]=0
        #print(samp+": {}".format(histograms[samp].Integral()))

    if applyScales:
        jetCat = "2jet"
        if "3jet" in canName.split("_")[0]:
            jetCat = "3jet"
        elif "4jet" in canName.split("_")[0]:
            jetCat = "4jet"
        for samp in ["ttbar"]:
            Yields[samp]["20J3"] = Yields[samp]["20J3"] * scalefactors["ttbar"+jetCat]
            Yields[samp]["25J3"] = Yields[samp]["25J3"] * scalefactors25J3["ttbar"+jetCat]
            Yields[samp]["30J3"] = Yields[samp]["30J3"] * scalefactors30J3["ttbar"+jetCat]
        for samp in ["W+bb","W+bc","W+bl","W+cc"]:
            Yields[samp]["20J3"] = Yields[samp]["20J3"] * scalefactors["WHF"+jetCat]
            Yields[samp]["25J3"] = Yields[samp]["25J3"] * scalefactors25J3["WHF"+jetCat]
            Yields[samp]["30J3"] = Yields[samp]["30J3"] * scalefactors30J3["WHF"+jetCat]
        for samp in ["Z+bb","Z+bc","Z+bl","Z+cc"]:
            Yields[samp]["20J3"] = Yields[samp]["20J3"] * scalefactors["ZHF"+jetCat]
            Yields[samp]["25J3"] = Yields[samp]["25J3"] * scalefactors25J3["ZHF"+jetCat]
            Yields[samp]["30J3"] = Yields[samp]["30J3"] * scalefactors30J3["ZHF"+jetCat]

            
    printYields(Yields,canName,applyScales)
#    for samp in Yields:
#        print("20J3 "+samp+" {}".format(Yields[samp]["20J3"]))
#        print("25J3 "+samp+" {}".format(Yields[samp]["25J3"]))
#        print("30J3 "+samp+" {}".format(Yields[samp]["30J3"]))

    histograms={}
    for samp in samples_yields:
        histograms[samp] = ROOT.TH1F(samp+canName,samp+canName,3,0,3)
        histograms[samp].SetBinContent(1,Yields[samp]["20J3"])
        if samp == "data":
            histograms[samp].SetBinError(1,math.sqrt(Yields[samp]["20J3"]))                

        histograms[samp].SetBinContent(2,Yields[samp]["25J3"])
        if samp == "data":
            histograms[samp].SetBinError(2,math.sqrt(Yields[samp]["25J3"]))                

        histograms[samp].SetBinContent(3,Yields[samp]["30J3"])
        if samp == "data":
            histograms[samp].SetBinError(3,math.sqrt(Yields[samp]["30J3"]))                


        histograms[samp].GetXaxis().SetBinLabel(1, "20J3")
        histograms[samp].GetXaxis().SetBinLabel(2, "25J3")
        histograms[samp].GetXaxis().SetBinLabel(3, "30J3")
            

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    for sampl in samples_yields:
        if sampl in histograms:
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)


    hs = ROOT.THStack("hs","")
    for samp in ["ttbar","stop",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l","diboson","signal",]:
        if samp in histograms:
            hs.Add(histograms[samp])
    
    prevYield=0
    hd = histograms["data"].Clone()
    hb = histograms["stop"].Clone()
    for samp in ["ttbar",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l","diboson"]:
        if samp in histograms:
            hb.Add(histograms[samp])
    hsig = histograms["signal"].Clone()
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["signal"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")
        
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName+"  "+channel)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["signal","diboson","Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "stop","ttbar"]:
        if samp in histograms:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("Cuts Scenario")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.5
    yHigh = 0.5
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("") 

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    yHigh = rat1.GetMaximum()*1.1
    rat1.SetAxisRange(0, yHigh, "Y")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    

    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("Y+")


    plotName = working_dir+"compareDataMC/compareDataMC_"+canName+"_"+varName+"_"+channel
    if applyScales:
        plotName+="_Scaled"
    if saveHistos:
        canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def doDataMC(varName,binning,canName,F,ptCut,applyScales):
    print("doDataMC")
    histograms={}
    # 2btag4pjet_150_250ptv_SR
    line=canName.split("_")
    F.cd()
    #print("Looping to Get histograms")
    for sampl in samples_plots:
        for samp in samples_plots[sampl]:
            histName1=samp+"_"+canName+"_"+varName+ptCut
            #print("Getting hist "+histName1)
            hist_tmp1 = F.Get(histName1)
            if sampl in histograms:
                try:
                    histograms[sampl].Add(hist_tmp1.Clone())
                except:
                    print(histName1+" doesn't exist")
            else:
                try:
                    histograms[sampl]=hist_tmp1.Clone()
                except:
                    print(histName1+" doesn't exist")

    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    if applyScales:
        jetCat = "2jet"
        if "3jet" in canName.split("_")[0]:
            jetCat = "3jet"
        elif "4jet" in canName.split("_")[0]:
            jetCat = "4jet"
        for samp in ["ttbar"]:
            if samp in histograms:
                if ptCut == "20J3":
                    histograms[samp].Scale(scalefactors["ttbar"+jetCat])
                elif ptCut == "25J3":
                    histograms[samp].Scale(scalefactors25J3["ttbar"+jetCat])
                elif ptCut == "30J3":
                    histograms[samp].Scale(scalefactors30J3["ttbar"+jetCat])
        for samp in ["W+bb","W+bc","W+bl","W+cc"]:
            if samp in histograms:
                if ptCut == "20J3":
                    histograms[samp].Scale(scalefactors["WHF"+jetCat])
                elif ptCut == "25J3":
                    histograms[samp].Scale(scalefactors25J3["WHF"+jetCat])
                elif ptCut == "30J3":
                    histograms[samp].Scale(scalefactors30J3["WHF"+jetCat])
        for samp in ["Z+bb","Z+bc","Z+bl","Z+cc"]:
            if samp in histograms:
                if ptCut == "20J3":
                    histograms[samp].Scale(scalefactors["ZHF"+jetCat])
                elif ptCut == "25J3":
                    histograms[samp].Scale(scalefactors25J3["ZHF"+jetCat])
                elif ptCut == "30J3":
                    histograms[samp].Scale(scalefactors30J3["ZHF"+jetCat])

    newMVABins=[]
    if "mva" in varName:
        hb_temp = ROOT.TH1F()
        if channel == "2L":
            hb_temp = histograms["Z+bb"].Clone()
            for samp in ["s+t chan","Wt",
                         "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                         "ttbar","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                         "WW","WZ","ZZ",]:
                if samp in histograms:
                    hb_temp.Add(histograms[samp])
        else:
            hb_temp = histograms["ttbar"].Clone()
            for samp in ["s+t chan","Wt",
                         "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                         "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                         "WW","WZ","ZZ",]:
                if samp in histograms:
                    hb_temp.Add(histograms[samp])
        hsig_temp = histograms["VH 125"].Clone()
        newMVABins = applyTrafoDToHist(hsig_temp,hb_temp, canName)
    
    rebinning = int(binning.split(",")[0])
    xLow = float(binning.split(",")[1])   
    xHigh = float(binning.split(",")[2])   
    print(binning)
    print(xLow)
    print(xHigh)
    if varName == "pTV":
        if "75_150ptv" in canName:
            xLow = 60
            xHigh = 160
        elif "150_250ptv" in canName:
            xLow = 140
            xHigh = 260
        elif "250_400ptv" in canName:
            xLow = 240
            xHigh = 410
        elif "400ptv" in canName:
            xLow = 390

    for sampl in samples_plots:
        if sampl in histograms:
            if not (rebinning == 0 or varName == "mva"):
                histograms[sampl].Rebin(rebinning)
            elif "mva" in varName:
                rebinTrafoDHist(histograms[sampl], newMVABins)
            if sampl != "data":
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetFillColor(colours[sampl])
            else:
                histograms[sampl].SetLineColor(colours[sampl])
                histograms[sampl].SetMarkerColor(colours[sampl])
                histograms[sampl].SetLineWidth(2)
                histograms[sampl].SetMarkerStyle(8)
                histograms[sampl].SetMarkerSize(1)
                #for i in range(histograms[sampl].GetNbinsX()):
                #    if histograms[sampl].GetBinContent(i+1)>0:
                #        histograms[sampl].SetBinError(i+1,math.sqrt(histograms[sampl].GetBinContent(i+1)))
            

    hs = ROOT.THStack("hs","")
    for samp in ["ttbar","s+t chan","Wt",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                 "WW","WZ","ZZ","VH 125",]:
        if samp in histograms:
            hs.Add(histograms[samp])

    if "VH 125" not in histograms:
        return
    hd = ROOT.TH1F()
    try:
        hd = histograms["data"].Clone()
    except:
        hd = histograms["VH 125"].Clone()
        hd.Reset("ICES")
    hb = ROOT.TH1F()
    try:
        hb = histograms["ttbar"].Clone()
    except:
        hb = histograms["VH 125"].Clone()
        hb.Reset("ICES")
    for samp in ["s+t chan","Wt",
                 "W+bb","W+bc","W+bl","W+cc","W+cl","W+l",
                 "Z+bb","Z+bc","Z+bl","Z+cc","Z+cl","Z+l",
                 "WW","WZ","ZZ",]:
        if samp in histograms:
            hb.Add(histograms[samp])
    hsig = histograms["VH 125"].Clone()



    if varName == "GSCptJ1":
        print("background 40-50: {} 50-60: {} total: {} percentage: {}".format(hb.GetBinContent(1),hb.GetBinContent(2),hb.Integral(),100*(hb.GetBinContent(1)+hb.GetBinContent(2))/hb.Integral()))
        print("signal 40-50: {} 50-60: {} total: {} percentage: {}".format(hsig.GetBinContent(1),hsig.GetBinContent(2),hsig.Integral(),100*(hsig.GetBinContent(1)+hsig.GetBinContent(2))/hsig.Integral()))

    #if ("mBB" in varName or "Mbb" in varName) and "60J1" in line[-4]:
    #    getSignificance(hsig,hb,canName+"_"+varName)
    #if ("mva" in varName) and "60J1" in line[-4]:
    #    getSignificanceMVA(hsig,hb,canName+"_"+varName)

    if ("mBB" in varName or "Mbb" in varName): 
        m_sigVals[canName]=getSignificance(hsig,hb,canName+"_"+varName)
        m_yieldVals[canName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBVals[canName]=hsig.Integral()/hb.Integral()
        else:
            m_SBVals[canName]=0

    if ("mva" in varName):
        m_sigValsMVA[canName]=getSignificanceMVA(hsig,hb,canName+"_"+varName)
        m_yieldValsMVA[canName]=hsig.Integral()
        if hb.Integral() != 0:
            m_SBValsMVA[canName]=hsig.Integral()/hb.Integral()
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),hsig.Integral()/hb.Integral()))
        else:
            m_SBValsMVA[canName]=0
            print("Signal yield: {} background yield: {}   S/B: {}".format(hsig.Integral(),hb.Integral(),0))

    
    doBlinding(hsig,hb,hd,varName)
    hsig.Scale(5)
    hsig.SetLineWidth(2)
    hsig.SetFillColor(0)
    #return
    #print("")

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.75, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.75, 0.4)
    right_hs = ROOT.TPad("right_hs", "right_hs", 0.75, 0.025, 0.995, 0.995)
    upper_hs.Draw()
    right_hs.Draw()
    
    lower_hs.Draw()
    right_hs.cd().SetLeftMargin(0)
    right_hs.SetRightMargin(0)
    lower_hs.cd().SetBottomMargin(0.3)
    lower_hs.SetGridy()
    lower_hs.SetTopMargin(0)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(hs.GetMaximum(), hd.GetMaximum())*1.4
                    
    rat = hd.Clone()
    rat1 = histograms["VH 125"].Clone()
    hd.Draw("p")
    hd.SetTitle("")
    hd.SetAxisRange(0.0, y_max, "Y")
    hd.GetYaxis().SetMaxDigits(3)
    hd.GetYaxis().SetTitle("Entries")
    hd.GetYaxis().SetTitleSize(0.04)
    hd.GetYaxis().SetTitleOffset(1.2)
    hd.GetYaxis().SetLabelSize(0.05)
    hd.SetYTitle("Entries")
    hd.SetLabelOffset(5)
    
    hs.Draw("same hist")
    hd.Draw("same p")
    hsig.Draw("same hist")

    hd.GetXaxis().SetRangeUser(xLow,xHigh)
    hb.GetXaxis().SetRangeUser(xLow,xHigh)
    hsig.GetXaxis().SetRangeUser(xLow,xHigh)
    hs.GetXaxis().SetLimits(xLow,xHigh)
        

    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName+"_"+ptCut+"  "+channel)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    right_hs.cd()
    legend1 = ROOT.TLegend(0.01, 0.4, 0.965, 0.95)
    legend1.SetBorderSize(1)
    legend1.SetTextSize(0.08)
    legend1.SetHeader("       sample, SoW")
    if "data" in histograms:
        legend1.AddEntry(histograms["data"], "data, {:.1f}".format(histograms["data"].Integral()), "pl")
    for samp in ["VH 125","ZZ","WZ","WW",
                 "Z+l","Z+cl","Z+cc","Z+bl","Z+bc","Z+bb",
                 "W+l","W+cl","W+cc","W+bl","W+bc","W+bb",
                 "Wt","s+t chan","ttbar",]:
        if samp in histograms:
            legend1.AddEntry(histograms[samp], samp+", {:.1f}".format(histograms[samp].Integral()), "f")
    legend1.AddEntry(hsig,"Signal x 5","l")
    #legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    legend2 = ROOT.TLegend(0.01, 0.23, 0.965, 0.35)
    legend2.SetBorderSize(1)
    legend2.SetTextSize(0.08)
    legend2.AddEntry(rat,"(Data-Bkg)/Bkg","lp")
    legend2.AddEntry(rat1,"S/B","lp")
    legend2.Draw()
    
    
    lower_hs.cd()
    ROOT.gPad.SetTicky(0)

    rat.Add(hb,-1)
    rat.Divide(hb)
    rat.SetTitle("")
    rat.GetXaxis().SetTitle("")
    rat.GetXaxis().SetTitleSize(0.09)
    rat.GetXaxis().SetTitleOffset(1.05)

    yLow = -0.2
    yHigh = 0.8
    rat.SetLineWidth(2)
    rat.SetLineColor(ROOT.kBlack)
    rat.SetAxisRange(yLow, yHigh, "Y")
    rat.GetXaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetRangeUser(yLow,yHigh)
    rat.GetYaxis().SetLabelSize(0.07)
    rat.GetYaxis().SetTitleOffset(0.7)
    rat.GetYaxis().SetTitleSize(0.06)
    rat.GetYaxis().SetTitle("(Data-Bkg)/Bkg")
    rat.GetYaxis().SetNdivisions(506)
    
    rat.Draw("ep") 
    rat.GetXaxis().SetRangeUser(xLow,xHigh)

    #print(rat.GetBinContent(3))
    #print(rat.GetBinContent(4))
    
    yHigh2 = rat1.GetMaximum()*1.1
    rat1.Divide(hb)
    rat1.SetLineColor(ROOT.kRed)
    rat1.SetLineWidth(2)
    rat1.SetFillColor(0)
    rat1.SetTitle("")
    #rat1.SetAxisRange(0, yHigh2, "Y")
    rat1.GetXaxis().SetTitle(varName)
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)
    rat1.GetXaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetLabelSize(0.07)
    rat1.GetYaxis().SetTitleOffset(0.7)
    rat1.GetYaxis().SetTitleSize(0.06)
    rat1.GetYaxis().SetTitle("S/B")
    rat1.GetYaxis().SetNdivisions(506)

    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinError(i+1)>0.075:
            rat1.SetBinContent(i+1,(rat1.GetBinContent(i)+rat1.GetBinContent(i+2))/2)
            rat1.SetBinError(i+1,(rat1.GetBinError(i)+rat1.GetBinError(i+2))/2)
            #print("{} {}".format(rat1.GetBinContent(i+1),rat1.GetBinError(i+1)))
    
    canvas_hs.cd()
    lower_hs1 = ROOT.TPad("lower_hs1", "lower_hs1", 0.025, 0.025, 0.75, 0.4)
    lower_hs1.SetFillStyle(4000)
    lower_hs1.SetFrameFillStyle(0)
    lower_hs1.Draw()
    lower_hs1.cd()
    ROOT.gPad.SetTicky(0)
    lower_hs1.SetBottomMargin(0.3)
    lower_hs1.SetTopMargin(0)
    rat1.Draw("hist Y+")
    rat1.GetXaxis().SetRangeUser(xLow,xHigh)
    

    plotName = working_dir+"DataMC/DataMC_"+canName+"_"+varName+"_"+ptCut+"_"+channel
    if applyScales:
        plotName+="_Scaled"
    if saveHistos:
        canvas_hs.SaveAs(plotName+".png")
    canvas_hs.Close()

def makeSignificancePlots(sigVals,yieldVals,SBVals,JetMET,varName):
    h_sig = ROOT.TH1F("sig","sig",5,0,5)
    h_yield = ROOT.TH1F("yield","yield",5,0,5)
    h_SB = ROOT.TH1F("SB","SB",5,0,5)
    #gRandom = ROOT.TRandom()

    i=0
    print("\n"+JetMET+"  Sig   Yield   S/B")
    sigValsTotTemp={}
    for keyName in ["20J2_20J3","30J2_20J3","20J2_25J3","20J2_30J3","30J2_30J3"]:
        i+=1
        #keyName = JetCut1+"_"+JetCut2
        keyName1 = "20J2_20J3"
        
        #if JetCut1=="45J1" and JetCut2=="20J2" and JetCut3=="20J3":
        h_sig.SetBinContent(i,sigVals[keyName])
        h_sig.GetXaxis().SetBinLabel(i, keyName)
        h_yield.SetBinContent(i,yieldVals[keyName])
        h_yield.GetXaxis().SetBinLabel(i, keyName)
        h_SB.SetBinContent(i,SBVals[keyName])
        h_SB.GetXaxis().SetBinLabel(i, keyName)
        sigValsNom=sigVals[keyName1]
        if sigValsNom==0:
            sigValsNom=1
        yieldValsNom=yieldVals[keyName1]
        if yieldValsNom==0:
            yieldValsNom=1
        SBValsNom=SBVals[keyName1]
        if SBValsNom==0:
            SBValsNom=1
        print(keyName+":  {:.3f}({:.2f})  {:.3f}({:.2f})  {:.3f}({:.2f})".format(
            sigVals[keyName],100*(sigVals[keyName]/sigValsNom-1),
            yieldVals[keyName],100*(yieldVals[keyName]/yieldValsNom-1),
            SBVals[keyName],100*(SBVals[keyName]/SBValsNom-1)))
        sigValsTotTemp[keyName]=sigVals[keyName]
        #else:
        #    h_sig.SetBinContent(i,sigVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
        #    h_yield.SetBinContent(i,yieldVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
        #    h_SB.SetBinContent(i,SBVals[keyName1]*(1+0.5*(gRandom.Uniform()-0.5)))
    print("")
    m_sigValsTot[JetMET]=copy.deepcopy(sigValsTotTemp)
    
    canvasSigs = ROOT.TCanvas("SigYields", "SigYields", 900, 900)
    upperSigs = ROOT.TPad("upperSigs", "upperSigs", 0.025, 0.445, 0.995, 0.995)
    upperSigs.Draw()
    upperSigs.cd()
    #upperSigs.SetGridy()
    #upperSigs.SetBottomMargin(0.3)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gPad.SetTicky(0)
    
    h_sig.SetTitle("")
    h_sig.GetXaxis().SetTitle("")
    h_sig.GetXaxis().SetTitleSize(0.09)
    h_sig.GetXaxis().SetTitleOffset(1.05)

    h_sig.SetLineWidth(2)
    h_sig.SetLineColor(ROOT.kBlack)
    h_sig.SetAxisRange(0, 4.5, "Y")
    h_sig.GetXaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetLabelSize(0.05)
    h_sig.GetYaxis().SetTitleOffset(0.8)
    h_sig.GetYaxis().SetTitleSize(0.05)
    h_sig.GetYaxis().SetTitle("Sig and S/B")
    h_sig.GetYaxis().SetNdivisions(506)
    y_max = h_sig.GetMaximum()*1.4
    h_sig.SetAxisRange(0.0, y_max, "Y")    
    h_sig.Draw("")
    h_SB.SetLineWidth(2)
    h_SB.SetLineColor(ROOT.kBlue)
    h_SB.Scale(100)
    h_SB.Draw("same hist")

    h_yield.SetLineColor(ROOT.kRed)
    h_yield.SetLineWidth(2)
    h_yield.SetTitle("")
    h_yield.GetXaxis().SetTitle("")
    h_yield.GetXaxis().SetTitleSize(0.09)
    h_yield.GetXaxis().SetTitleOffset(1.05)
    h_yield.GetXaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetLabelSize(0.05)
    h_yield.GetYaxis().SetTitleOffset(0.8)
    h_yield.GetYaxis().SetTitleSize(0.05)
    h_yield.GetYaxis().SetTitle("Signal Yield")
    h_yield.GetYaxis().SetNdivisions(506)
    y_max = h_yield.GetMaximum()*1.4
    h_yield.SetAxisRange(0.0, y_max, "Y")    

    
    canvasSigs.cd()
    upperSigs2 = ROOT.TPad("upperSigs2", "upperSigs2", 0.025, 0.445, 0.995, 0.995)
    upperSigs2.SetFillStyle(4000)
    upperSigs2.SetFrameFillStyle(0)
    upperSigs2.Draw()
    upperSigs2.cd()
    ROOT.gPad.SetTicky(0)
    #upperSigs2.SetBottomMargin(0.3)
    h_yield.Draw("Y+")


    legend1 = ROOT.TLegend(0.705, 0.75, 0.93, 0.87)
    legend1.AddEntry(h_sig ,"Significance","l")
    legend1.AddEntry(h_SB ,"S/B x 100", "l")
    legend1.AddEntry(h_yield ,"Signal Yield","l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()

    canvasSigs.cd()
    lowerSigs = ROOT.TPad("lowerSigs", "lowerSigs", 0.025, 0.025, 0.995, 0.495)
    lowerSigs.Draw()
    lowerSigs.SetGridy()
    lowerSigs.cd().SetBottomMargin(0.3)

    rat1=h_sig.Clone()
    rat2=h_SB.Clone()
    rat3=h_yield.Clone()
    for i in range(rat1.GetNbinsX()):
        if rat1.GetBinContent(1)>0:
            rat1.SetBinContent(i+1,h_sig.GetBinContent(i+1)/h_sig.GetBinContent(1))
            rat1.SetBinError(i+1,h_sig.GetBinError(i+1)/h_sig.GetBinContent(1))
        if rat2.GetBinContent(1)>0:
            rat2.SetBinContent(i+1,h_SB.GetBinContent(i+1)/h_SB.GetBinContent(1))
            rat2.SetBinError(i+1,h_SB.GetBinError(i+1)/h_SB.GetBinContent(1))
        if rat3.GetBinContent(1)>0:
            rat3.SetBinContent(i+1,h_yield.GetBinContent(i+1)/h_yield.GetBinContent(1))
            rat3.SetBinError(i+1,h_yield.GetBinError(i+1)/h_yield.GetBinContent(1))

    rat1.SetTitle("")
    rat1.GetXaxis().SetTitle("")
    rat1.GetXaxis().SetTitleSize(0.09)
    rat1.GetXaxis().SetTitleOffset(1.05)

    #rat1.SetLineWidth(2)
    #rat1.SetLineColor(ROOT.kBlack)
    rat1.GetXaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetLabelSize(0.05)
    rat1.GetYaxis().SetTitleOffset(0.8)
    rat1.GetYaxis().SetTitleSize(0.05)
    rat1.GetYaxis().SetTitle("Cuts/Nom")
    rat1.GetYaxis().SetNdivisions(506)
    rat1.SetAxisRange(0.5, 1.5, "Y")  

    rat1.Draw("hist")
    rat2.Draw("same hist")
    rat3.Draw("same hist")



    if saveHistos:
        canvasSigs.SaveAs(working_dir+"Significances/Significances_"+JetMET+"_"+varName+".png")
    canvasSigs.Close()
    
def cutsComparisonAllSamples(varName,binning,canName,doNjet):
    for sample in samples_inFile:
        #if sample != "signal":
        #    continue
        cutsComparison(varName,binning,canName,sample,doNjet)

def cutsComparison(varName,binning,canName,sample,doNjet):
    print("cutsComparison")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
        
    hist_file.cd()
    h422=ROOT.TH1F()
    h423=ROOT.TH1F()
    h425=ROOT.TH1F()
    
    i=0
    j=0
    k=0
    for samp in samples_inFile[sample]:
        histName1=samp+"_"+canName+"_"+varName+"20J3"
        #print("Getting hist "+histName1)
        hist_tmp1 = hist_file.Get(histName1)
        if i != 0:
            try:
                h422.Add(hist_tmp1.Clone())
            except:
                print(histName1+" doesn't exist")
        else:
            try:
                h422=hist_tmp1.Clone()
            except:
                print(histName1+" doesn't exist")
                i-=1
        i+=1
        histName2=samp+"_"+canName+"_"+varName+"25J3"
        #print("Getting hist "+histName1)
        hist_tmp2 = hist_file.Get(histName2)
        if j != 0:
            try:
                h425.Add(hist_tmp2.Clone())
            except:
                print(histName2+" doesn't exist")
        else:
            try:
                h425=hist_tmp2.Clone()
            except:
                print(histName2+" doesn't exist")
                j-=1
        j+=1
        histName3=samp+"_"+canName+"_"+varName+"30J3"
        #print("Getting hist "+histName1)
        hist_tmp3 = hist_file.Get(histName3)
        if k != 0:
            try:
                h423.Add(hist_tmp3.Clone())
            except:
                print(histName3+" doesn't exist")
        else:
            try:
                h423=hist_tmp3.Clone()
            except:
                print(histName3+" doesn't exist")
                k-=1
        k+=1

    if doNjet:
        for samp in samples_inFile[sample]:
            NJs = ["3jet","4jet","5pjet"]
            if channel == "2L":
                NJs = ["3jet","4pjet"]
            for NJ in NJs:
                canName1=canName.replace("2jet",NJ)

                histName1=samp+"_"+canName1+"_"+varName+"20J3"
                #print("Getting hist "+histName1)
                hist_tmp1 = hist_file.Get(histName1)
                try:
                    h422.Add(hist_tmp1.Clone())
                except:
                    print(histName1+" doesn't exist")

                histName2=samp+"_"+canName1+"_"+varName+"25J3"
                #print("Getting hist "+histName1)
                hist_tmp2 = hist_file.Get(histName2)
                try:
                    h425.Add(hist_tmp2.Clone())
                except:
                    print(histName2+" doesn't exist")

                histName3=samp+"_"+canName1+"_"+varName+"30J3"
                #print("Getting hist "+histName1)
                hist_tmp3 = hist_file.Get(histName3)
                try:
                    h423.Add(hist_tmp3.Clone())
                except:
                    print(histName3+" doesn't exist")


    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlack)
    h422.SetLineWidth(2)
    h425.SetLineColor(ROOT.kRed)
    h425.SetLineWidth(2)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)

    rebinning = int(binning.split(",")[0])
    print(rebinning)
    if rebinning != 0:
        h422.Rebin(rebinning)
        h425.Rebin(rebinning)
        h423.Rebin(rebinning)
    xLow = float(binning.split(",")[1])   
    xHigh = float(binning.split(",")[2])   
    if varName == "pTV":
        if "75_150ptv" in canName:
            xLow = 60
            xHigh = 160
        elif "150_250ptv" in canName:
            xLow = 140
            xHigh = 260
        elif "250_400ptv" in canName:
            xLow = 240
            xHigh = 410
        elif "400ptv" in canName:
            xLow = 390
    h422.GetXaxis().SetRangeUser(xLow,xHigh)
    h425.GetXaxis().SetRangeUser(xLow,xHigh)
    h423.GetXaxis().SetRangeUser(xLow,xHigh)
    
    
    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    #upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.025, 0.995, 0.995)
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd() #.SetBottomMargin(0.2)
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(),h425.GetMaximum(),h423.GetMaximum())*1.4 #max(h422.GetMaximum(),h4225.GetMaximum(), h423.GetMaximum(), h432.GetMaximum(), h433.GetMaximum())*1.4
    
    rat423 = h423.Clone()
    rat425 = h425.Clone()

    h422.Draw("hist")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    h422.GetYaxis().SetTitle("Entries")
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05) #0.05
    h422.SetYTitle("Entries")
    h422.SetLabelOffset(5)

    h425.Draw("same hist")
    h423.Draw("same hist")

    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    #legend1 = ROOT.TLegend(0.55, 0.75, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    #legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
    legend1.AddEntry(h422, "Nominal, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
    legend1.AddEntry(h425, "25J3, {:.1f}, {:.1f}".format(h425.GetEntries(),h425.Integral()), "l")
    legend1.AddEntry(h423, "30J3, {:.1f}, {:.1f}".format(h423.GetEntries(),h423.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    #t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    #t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    #t.DrawLatex(0.15, 0.75, "#it{#bf{0L, == 3 Jets, 2 b-tag}}")
    #t.DrawLatex(0.15, 0.70, "#it{#bf{p_{T}^{V} > 150, SR}}")
    t.DrawLatex(0.15, 0.85, canName+"  "+channel)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)
    
    rat423.Divide(h422)
    rat425.Divide(h422)
    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 2
    if "2jet" in canName:
        yHigh = 2.5
    rat423.SetLineWidth(2)
    #rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle(varName)
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("p") 
    rat425.Draw("same p") 
    
    #h422.GetXaxis().SetTitleSize(0.04)
    #h422.GetXaxis().SetTitleOffset(1.04)
    #h422.GetXaxis().SetLabelSize(0.04)
    #h422.GetXaxis().SetTitle("p_{T}(J3) [GeV]")
    #h422.SetAxisRange(0, 250, "X")
    if doNjet:
        varName+="_AllJets"
        
    canvas_hs.SaveAs(working_dir+"CutsCompare_"+sample+"_"+canName+"_"+varName+"_"+channel+".png")
    canvas_hs.Close()

def pileupEstimateAllSamples(varName,canName):
    for sample in samples:
        if sample != "signal":
            continue
        pileupEstimate(varName,canName,sample)

def pileupEstimate(varName,canName,sample):
    print("pileupEstimate")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    
    sample2=sample
    hist_file.cd()
    histName1=sample2+"_"+canName+"_"+varName+"_2D"
    hist_tmp1 = hist_file.Get(histName1)
    g422=hist_tmp1.Clone()
    h422 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)
    
    #hist_file.cd()
    #histName1=histName1.replace("20J3","25J3")
    #hist_tmp1a = hist_file.Get(histName1)
    #g4225=hist_tmp1a.Clone()
    #h4225 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    #hist_file.cd()
    #histName1=histName1.replace("25J3","30J3")
    #hist_tmp1a = hist_file.Get(histName1)
    #g423=hist_tmp1a.Clone()
    #h423 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    hist_file433.cd()
    #histName1=histName1.replace("20J2","30J2")
    histName1=histName1.replace("20J2_20J3","30J2_30J3")
    hist_tmp1c = hist_file433.Get(histName1)
    g433=hist_tmp1c.Clone()
    h433 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    #hist_file432.cd()
    #histName1=histName1.replace("30J3","20J3")
    #hist_tmp1b = hist_file432.Get(histName1)
    #g432=hist_tmp1b.Clone()
    #h432 = ROOT.TH1F(histName1+"_avg",histName1+"_avg",20,0,100)

    
    totnum422=0
    totsum422=0
    totnum4225=0
    totsum4225=0
    totnum423=0
    totsum423=0
    totnum433=0
    totsum433=0
    totnum432=0
    totsum432=0
    for i in range(g422.GetNbinsX()):
        num422=0
        sum422=0
        num4225=0
        sum4225=0
        num423=0
        sum423=0
        num433=0
        sum433=0
        num432=0
        sum432=0
        for j in range(g422.GetNbinsY()):
            num422+=g422.GetBinContent(i+1,j+1)
            sum422+=g422.GetBinContent(i+1,j+1)*g422.GetYaxis().GetBinLowEdge(j+1)
            '''
            num4225+=g4225.GetBinContent(i+1,j+1)
            sum4225+=g4225.GetBinContent(i+1,j+1)*g4225.GetYaxis().GetBinLowEdge(j+1)
            num423+=g423.GetBinContent(i+1,j+1)
            sum423+=g423.GetBinContent(i+1,j+1)*g423.GetYaxis().GetBinLowEdge(j+1)
            num432+=g432.GetBinContent(i+1,j+1)
            sum432+=g432.GetBinContent(i+1,j+1)*g432.GetYaxis().GetBinLowEdge(j+1)
            '''
            num433+=g433.GetBinContent(i+1,j+1)
            sum433+=g433.GetBinContent(i+1,j+1)*g433.GetYaxis().GetBinLowEdge(j+1)
        if num422>0:
            h422.SetBinContent(i+1,sum422/num422)
            totnum422+=num422
            totsum422+=sum422
        '''
        if num4225>0:
            h4225.SetBinContent(i+1,sum4225/num4225)
            totnum4225+=num4225
            totsum4225+=sum4225
        if num423>0:
            h423.SetBinContent(i+1,sum423/num423)
            totnum423+=num423
            totsum423+=sum423
        if num432>0:
            h432.SetBinContent(i+1,sum432/num432)
            totnum432+=num432
            totsum432+=sum432
        '''
        if num433>0:
            h433.SetBinContent(i+1,sum433/num433)
            totnum433+=num433
            totsum433+=sum433
            
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlue) #ROOT.kBlack)
    h422.SetLineWidth(2)
    h422.SetMarkerColor(ROOT.kBlue) #ROOT.kBlack)
    h422.SetMarkerStyle(8)
    h422.SetMarkerSize(1)
    '''
    h4225.SetLineColor(ROOT.kOrange+1)
    h4225.SetLineWidth(2)
    h4225.SetMarkerColor(ROOT.kOrange+1)
    h4225.SetMarkerStyle(8)
    h4225.SetMarkerSize(1)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h423.SetMarkerColor(ROOT.kBlue)
    h423.SetMarkerStyle(8)
    h423.SetMarkerSize(1)
    h432.SetLineColor(ROOT.kRed)
    h432.SetLineWidth(2)
    h432.SetMarkerColor(ROOT.kRed)
    h432.SetMarkerStyle(8)
    h432.SetMarkerSize(1)
    '''
    h433.SetLineColor(ROOT.kRed) #ROOT.kGreen+3)
    h433.SetLineWidth(2)
    h433.SetMarkerColor(ROOT.kRed) #ROOT.kGreen+3)
    h433.SetMarkerStyle(8)
    h433.SetMarkerSize(1)

    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h433.GetMaximum(), )*1.5 #h423.GetMaximum(), h432.GetMaximum(),  h4225.GetMaximum())*1.5
                    
    #rat423 = h423.Clone()
    #rat4225 = h4225.Clone()
    #rat432 = h432.Clone()
    rat433 = h433.Clone()
    h422.Draw("")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    YTitle = "avg. nSigJets"
    if varName == "nSigJets-nMatchedTruthSigJets":
        #YTitle = "avg. (nSigJets-nMatchedTruthSigJets)"
        YTitle = "avg. no. Pileup Jets"
    h422.GetYaxis().SetTitle(YTitle)
    h422.SetYTitle(YTitle)
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetLabelOffset(5)
    
    #h423.Draw("same hist")
    #h4225.Draw("same hist")
    #h432.Draw("same hist")
    h433.Draw("same")
    
    if totnum422==0:
        totnum422=1
    '''
    if totnum4225==0:
        totnum4225=1
    if totnum423==0:
        totnum423=1
    if totnum432==0:
        totnum432=1
    '''
    if totnum433==0:
        totnum433=1
    #legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1 = ROOT.TLegend(0.55, 0.7, 0.93, 0.87)
    legend1.SetHeader("              sample,    total avg.")
    legend1.AddEntry(h422, "Nominal, {:.3f}".format(totsum422/totnum422), "l")
    '''
    legend1.AddEntry(h4225, "20J2-25J3, {:.3f}".format(totsum4225/totnum4225), "l")
    legend1.AddEntry(h423, "20J2-30J3, {:.3f}".format(totsum423/totnum423), "l")
    legend1.AddEntry(h432, "30J2-20J3, {:.3f}".format(totsum432/totnum432), "l")
    '''
    legend1.AddEntry(h433, "30GeV pT cut, {:.3f}".format(totsum433/totnum433), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.04)
    t.SetTextAlign(4)
    #t.DrawLatex(0.15, 0.85, canName)
    #t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")
    t.DrawLatex(0.15, 0.85, "ATLAS #it{#bf{Simulation Preliminary}}")
    t.DrawLatex(0.15, 0.80, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")
    t.DrawLatex(0.15, 0.75, "#it{#bf{0L, 2+ Jets, 2 b-tag}}")
    t.DrawLatex(0.15, 0.70, "#it{#bf{p_{T}^{V} > 150, SR}}")
            
    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)
    '''
    rat423.Divide(h422)
    rat4225.Divide(h422)
    rat432.Divide(h422)
    '''
    rat433.Divide(h422)
    rat433.SetTitle("")
    rat433.GetXaxis().SetTitle("")
    rat433.GetXaxis().SetTitleSize(0.09)
    rat433.GetXaxis().SetTitleOffset(1.05)

    yLow = 0.5
    yHigh = 1.5
    if varName == "nSigJets-nMatchedTruthSigJets":
        yLow=0
        yHigh=1.1
    rat433.SetLineWidth(2)
    rat433.SetLineColor(ROOT.kRed)
    rat433.SetAxisRange(yLow, yHigh, "Y")
    rat433.GetXaxis().SetLabelSize(0.09)
    rat433.GetXaxis().SetTitle("#mu")
    rat433.GetYaxis().SetLabelSize(0.07)
    rat433.GetYaxis().SetTitleOffset(0.7)
    rat433.GetYaxis().SetTitleSize(0.06)
    rat433.GetYaxis().SetTitle("Cuts/Orig")
    rat433.GetYaxis().SetNdivisions(506)
    
    rat433.Draw("hist") 
    '''
    rat423.Draw("same hist") 
    rat4225.Draw("same hist") 
    rat432.Draw("same hist") 
    '''
    if saveHistos:
        #canvas_hs.SaveAs(working_dir+"cutsCompare/"+sample+"/CutsCompare_"+canName+"_"+varName+"_2D.png")
        canvas_hs.SaveAs(working_dir+"CutsCompare_"+canName+"_"+varName+"_2D.png")
    canvas_hs.Close()

def makeTTBarDecay(varName,binning,canName):
    print("TTBarDecay")
    # signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
    line=canName.split("_")
    
    h422=ROOT.TH1F("TTBarDecay422","TTBarDecay422",12,0,12)
    h4225=ROOT.TH1F("TTBarDecay4225","TTBarDecay4225",12,0,12)
    h423=ROOT.TH1F("TTBarDecay423","TTBarDecay423",12,0,12)
    h433=ROOT.TH1F("TTBarDecay433","TTBarDecay433",12,0,12)
    h432=ROOT.TH1F("TTBarDecay432","TTBarDecay432",12,0,12)

    hist_file.cd()
    histName="_"+canName+"_"+varName
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1 = hist_file.Get(histName1)
        h422.Add(hist_tmp1.Clone())

    hist_file.cd()
    histName=histName.replace("20J3","25J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1a = hist_file.Get(histName1)
        h4225.Add(hist_tmp1a.Clone())

    hist_file.cd()
    histName=histName.replace("25J3","30J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1a = hist_file.Get(histName1)
        h423.Add(hist_tmp1a.Clone())

    hist_file433.cd()
    histName=histName.replace("20J2","30J2")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1c = hist_file433.Get(histName1)
        h433.Add(hist_tmp1c.Clone())

    hist_file432.cd()
    histName=histName.replace("30J3","20J3")
    for samp in ["ttbarbb","ttbarbc","ttbarbl","ttbarcc","ttbarcl","ttbarl",]:
        histName1=samp+histName
        hist_tmp1b = hist_file432.Get(histName1)
        h432.Add(hist_tmp1b.Clone())

    h422.SetBinContent(11,h422.GetBinContent(2)+h422.GetBinContent(3)+h422.GetBinContent(4))
    h422.SetBinError(11,math.sqrt(h422.GetBinError(2)**2+h422.GetBinError(3)**2+h422.GetBinError(4)**2))
    h422.SetBinContent(12,h422.GetBinContent(5)+h422.GetBinContent(6)+h422.GetBinContent(7)+h422.GetBinContent(8)+h422.GetBinContent(9)+h422.GetBinContent(10))
    h422.SetBinError(12,math.sqrt(h422.GetBinError(5)**2+h422.GetBinError(6)**2+h422.GetBinError(7)**2+h422.GetBinError(8)**2+h422.GetBinError(9)**2+h422.GetBinError(10)**2))

    h4225.SetBinContent(11,h4225.GetBinContent(2)+h4225.GetBinContent(3)+h4225.GetBinContent(4))
    h4225.SetBinError(11,math.sqrt(h4225.GetBinError(2)**2+h4225.GetBinError(3)**2+h4225.GetBinError(4)**2))
    h4225.SetBinContent(12,h4225.GetBinContent(5)+h4225.GetBinContent(6)+h4225.GetBinContent(7)+h4225.GetBinContent(8)+h4225.GetBinContent(9)+h4225.GetBinContent(10))
    h4225.SetBinError(12,math.sqrt(h4225.GetBinError(5)**2+h4225.GetBinError(6)**2+h4225.GetBinError(7)**2+h4225.GetBinError(8)**2+h4225.GetBinError(9)**2+h4225.GetBinError(10)**2))

    h423.SetBinContent(11,h423.GetBinContent(2)+h423.GetBinContent(3)+h423.GetBinContent(4))
    h423.SetBinError(11,math.sqrt(h423.GetBinError(2)**2+h423.GetBinError(3)**2+h423.GetBinError(4)**2))
    h423.SetBinContent(12,h423.GetBinContent(5)+h423.GetBinContent(6)+h423.GetBinContent(7)+h423.GetBinContent(8)+h423.GetBinContent(9)+h423.GetBinContent(10))
    h423.SetBinError(12,math.sqrt(h423.GetBinError(5)**2+h423.GetBinError(6)**2+h423.GetBinError(7)**2+h423.GetBinError(8)**2+h423.GetBinError(9)**2+h423.GetBinError(10)**2))

    h433.SetBinContent(11,h433.GetBinContent(2)+h433.GetBinContent(3)+h433.GetBinContent(4))
    h433.SetBinError(11,math.sqrt(h433.GetBinError(2)**2+h433.GetBinError(3)**2+h433.GetBinError(4)**2))
    h433.SetBinContent(12,h433.GetBinContent(5)+h433.GetBinContent(6)+h433.GetBinContent(7)+h433.GetBinContent(8)+h433.GetBinContent(9)+h433.GetBinContent(10))
    h433.SetBinError(12,math.sqrt(h433.GetBinError(5)**2+h433.GetBinError(6)**2+h433.GetBinError(7)**2+h433.GetBinError(8)**2+h433.GetBinError(9)**2+h433.GetBinError(10)**2))

    h432.SetBinContent(11,h432.GetBinContent(2)+h432.GetBinContent(3)+h432.GetBinContent(4))
    h432.SetBinError(11,math.sqrt(h432.GetBinError(2)**2+h432.GetBinError(3)**2+h432.GetBinError(4)**2))
    h432.SetBinContent(12,h432.GetBinContent(5)+h432.GetBinContent(6)+h432.GetBinContent(7)+h432.GetBinContent(8)+h432.GetBinContent(9)+h432.GetBinContent(10))
    h432.SetBinError(12,math.sqrt(h432.GetBinError(5)**2+h432.GetBinError(6)**2+h432.GetBinError(7)**2+h432.GetBinError(8)**2+h432.GetBinError(9)**2+h432.GetBinError(10)**2))
    
    i=0
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
     
    h422.SetLineColor(ROOT.kBlack)
    h422.SetLineWidth(2)
    h4225.SetLineColor(ROOT.kOrange+1)
    h4225.SetLineWidth(2)
    h423.SetLineColor(ROOT.kBlue)
    h423.SetLineWidth(2)
    h432.SetLineColor(ROOT.kRed)
    h432.SetLineWidth(2)
    h433.SetLineColor(ROOT.kGreen+3)
    h433.SetLineWidth(2)
            
    rebinning = int(binning.split(",")[0])
    print(rebinning)
    if rebinning != 0:
        h422.Rebin(rebinning)
        h4225.Rebin(rebinning)
        h423.Rebin(rebinning)
        h432.Rebin(rebinning)
        h433.Rebin(rebinning)


    canvas_hs = ROOT.TCanvas("DataMC", "DataMC", 900, 900)
    #(x1,y1,x2,y2) 
    upper_hs = ROOT.TPad("upper_hs", "upper_hs", 0.025, 0.345, 0.995, 0.995)
    lower_hs = ROOT.TPad("lower_hs", "lower_hs", 0.025, 0.025, 0.995, 0.4)
    upper_hs.Draw()
    lower_hs.Draw()
    lower_hs.SetGridy()
    lower_hs.cd().SetBottomMargin(0.3)
    upper_hs.cd()
    ROOT.gStyle.SetOptStat(0)

    y_max = max(h422.GetMaximum(), h4225.GetMaximum(), h423.GetMaximum(), h432.GetMaximum(), h433.GetMaximum())*1.4
                    
    rat423 = h423.Clone()
    rat4225 = h4225.Clone()
    rat432 = h432.Clone()
    rat433 = h433.Clone()
    h422.Draw("hist")
    h422.SetTitle("")
    h422.SetAxisRange(0.0, y_max, "Y")
    h422.GetYaxis().SetMaxDigits(3)
    h422.GetYaxis().SetTitle("Entries")
    h422.GetYaxis().SetTitleSize(0.04)
    h422.GetYaxis().SetTitleOffset(1.2)
    h422.GetYaxis().SetLabelSize(0.05)
    h422.SetYTitle("Entries")
    h422.SetLabelOffset(5)
    
    h423.Draw("same hist")
    h4225.Draw("same hist")
    h432.Draw("same hist")
    h433.Draw("same hist")
    
    legend1 = ROOT.TLegend(0.605, 0.65, 0.93, 0.87)
    legend1.SetHeader("              sample,    NEntries,    SoW")
    legend1.AddEntry(h422, "20J2-20J3, {:.1f}, {:.1f}".format(h422.GetEntries(),h422.Integral()), "l")
    legend1.AddEntry(h4225, "20J2-25J3, {:.1f}, {:.1f}".format(h4225.GetEntries(),h4225.Integral()), "l")
    legend1.AddEntry(h423, "20J2-30J3, {:.1f}, {:.1f}".format(h423.GetEntries(),h423.Integral()), "l")
    legend1.AddEntry(h432, "30J2-20J3, {:.1f}, {:.1f}".format(h432.GetEntries(),h432.Integral()), "l")
    legend1.AddEntry(h433, "30J2-30J3, {:.1f}, {:.1f}".format(h433.GetEntries(),h433.Integral()), "l")
    legend1.SetBorderSize(0)
    legend1.SetFillColor(0)
    legend1.SetFillStyle(0)
    legend1.Draw()
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.03)
    t.SetTextAlign(4)
    t.DrawLatex(0.15, 0.85, canName)
    t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

    
    
    lower_hs.cd()
    #ROOT.gPad.SetTicky(0)

    rat423.Divide(h422)
    rat4225.Divide(h422)
    rat432.Divide(h422)
    rat433.Divide(h422)
    rat423.SetTitle("")
    rat423.GetXaxis().SetTitle("")
    rat423.GetXaxis().SetTitleSize(0.09)
    rat423.GetXaxis().SetTitleOffset(1.05)
    ind=0
    for label in ["j-j","j-e","j-mu","j-tau","e-e","mu-mu","tau-tau","e-mu","e-tau","mu-tau","SemiLep","DiLep"]:
        rat423.GetXaxis().SetBinLabel(ind+1, label)
        ind+=1

    yLow = 0.5
    yHigh = 4
    if "4jet" in canName:
        yHigh=2.2
    elif "5pjet" in canName:
        yLow = 0
        yHigh = 1.2
    elif "4jet" in canName:
        yHigh=2.6
    elif "2pjet" in canName:
        yHigh=1.2
    
    rat423.SetLineWidth(2)
    rat423.SetLineColor(ROOT.kBlue)
    rat423.SetAxisRange(yLow, yHigh, "Y")
    rat423.GetXaxis().SetLabelSize(0.09)
    rat423.GetXaxis().SetTitle(varName)
    rat423.GetYaxis().SetLabelSize(0.07)
    rat423.GetYaxis().SetTitleOffset(0.7)
    rat423.GetYaxis().SetTitleSize(0.06)
    rat423.GetYaxis().SetTitle("Cuts/Orig")
    rat423.GetYaxis().SetNdivisions(506)
    
    rat423.Draw("p") 
    rat433.Draw("same p") 
    rat4225.Draw("same p") 
    rat432.Draw("same p") 
    
    if saveHistos:
        canvas_hs.SaveAs(working_dir+"cutsCompare/CutsCompare_"+canName+"_"+varName+".png")
    canvas_hs.Close()

def doNJetMigration(varName,canName,F,ptCut):
    print("Jet migrations")
    line=canName.split("_")
    F.cd()
    for sampl in samples_inFile:
        histograms={}
        histograms2={"2jet":0,"3jet":0,"4jet":0,"5pjet":0}
        tagJet = ["tag2jet","tag3jet","tag4jet","tag5pjet"]
        if channel == "2L":
            tagJet = ["tag2jet","tag3jet","tag4pjet"]
        histograms[sampl] = ROOT.TH1F(sampl+"_"+canName,sampl+"_"+canName,10,0,10)            
        hist_tmp = ROOT.TH1F()
        i=0
        for samp in samples_inFile[sampl]:
            histName1=samp+"_"+canName+"_"+varName+ptCut
            for NJ in tagJet:
                histName2=re.sub("tag.*?jet",NJ,histName1)
                #print("Getting hist "+histName1)
                hist_tmp111 = F.Get(histName2)
                if i==0:
                    try:
                        hist_tmp = hist_tmp111.Clone()
                    except:
                        print(histName2+" no exist")
                        i-=1
                else:
                    try:
                        hist_tmp.Add(hist_tmp111.Clone())
                    except:
                        print(histName2+" no exist")
                i+=1


        histograms[sampl].SetBinContent(1,hist_tmp.GetBinContent(23))
        histograms[sampl].SetBinContent(2,hist_tmp.GetBinContent(33))
        histograms[sampl].SetBinContent(3,hist_tmp.GetBinContent(43))
        histograms[sampl].SetBinContent(4,hist_tmp.GetBinContent(53)+hist_tmp.GetBinContent(63)+hist_tmp.GetBinContent(73)+hist_tmp.GetBinContent(83)+hist_tmp.GetBinContent(93))
        tot2jet = hist_tmp.GetBinContent(23) + hist_tmp.GetBinContent(33) + hist_tmp.GetBinContent(43) + \
                  hist_tmp.GetBinContent(53)+hist_tmp.GetBinContent(63)+hist_tmp.GetBinContent(73)+hist_tmp.GetBinContent(83)+hist_tmp.GetBinContent(93)
        histograms[sampl].SetBinContent(5,hist_tmp.GetBinContent(34))
        histograms[sampl].SetBinContent(6,hist_tmp.GetBinContent(44))
        histograms[sampl].SetBinContent(7,hist_tmp.GetBinContent(54)+hist_tmp.GetBinContent(64)+hist_tmp.GetBinContent(74)+hist_tmp.GetBinContent(84)+hist_tmp.GetBinContent(94))
        tot3jet = hist_tmp.GetBinContent(34) + hist_tmp.GetBinContent(44) + hist_tmp.GetBinContent(54) + \
                  hist_tmp.GetBinContent(64)+hist_tmp.GetBinContent(74)+hist_tmp.GetBinContent(84)+hist_tmp.GetBinContent(94)
        histograms[sampl].SetBinContent(8,hist_tmp.GetBinContent(45))
        histograms[sampl].SetBinContent(9,hist_tmp.GetBinContent(55)+hist_tmp.GetBinContent(65)+hist_tmp.GetBinContent(75)+hist_tmp.GetBinContent(85)+hist_tmp.GetBinContent(95))
        tot4jet = hist_tmp.GetBinContent(45) + hist_tmp.GetBinContent(55) + hist_tmp.GetBinContent(65) + \
                  hist_tmp.GetBinContent(75)+hist_tmp.GetBinContent(85)+hist_tmp.GetBinContent(95)

        histDiv2=ROOT.TH1F(sampl+"_"+canName+"_Div2",sampl+"_"+canName+"_Div2",10,0,10)            
        histDiv2.SetBinContent(1,tot2jet)
        histDiv2.SetBinContent(2,tot2jet)
        histDiv2.SetBinContent(3,tot2jet)
        histDiv2.SetBinContent(4,tot2jet)
        histDiv2.SetBinContent(5,tot3jet)
        histDiv2.SetBinContent(6,tot3jet)
        histDiv2.SetBinContent(7,tot3jet)
        histDiv2.SetBinContent(8,tot4jet)
        histDiv2.SetBinContent(9,tot4jet)
        

        histDiv=ROOT.TH1F(sampl+"_"+canName+"_Div",sampl+"_"+canName+"_Div",10,0,10)            
        hist_tmp2 = ROOT.TH1F()
        i=0
        for samp in samples_inFile[sampl]:
            histName1=samp+"_"+canName+"_nSigJets20J3"
            for NJ in tagJet:
                histName2=re.sub("tag.*?jet",NJ,histName1)
                #print("histName2 "+histName2)
                hist_tmp1a = F.Get(histName2)
                if i==0:
                    try:
                        hist_tmp2 = hist_tmp1a.Clone()
                    except:
                        print(histName2+" no exist")
                        i-=1
                else:
                    try:
                        hist_tmp2.Add(hist_tmp1a.Clone())
                    except:
                        print(histName2+" no exist")
                i+=1
                
        histograms2["2jet"]=hist_tmp2.GetBinContent(3)
        histograms2["3jet"]=hist_tmp2.GetBinContent(4)
        histograms2["4jet"]=hist_tmp2.GetBinContent(5)
        histograms2["5pjet"]=hist_tmp2.GetBinContent(6)+hist_tmp2.GetBinContent(7)+hist_tmp2.GetBinContent(8)+hist_tmp2.GetBinContent(9)+hist_tmp2.GetBinContent(10)
        
        histDiv.SetBinContent(1,histograms2["2jet"])
        histDiv.SetBinContent(2,histograms2["3jet"])
        histDiv.SetBinContent(3,histograms2["4jet"])
        histDiv.SetBinContent(4,histograms2["5pjet"])
        histDiv.SetBinContent(5,histograms2["3jet"])
        histDiv.SetBinContent(6,histograms2["4jet"])
        histDiv.SetBinContent(7,histograms2["5pjet"])
        histDiv.SetBinContent(8,histograms2["4jet"])
        histDiv.SetBinContent(9,histograms2["5pjet"])

        #if sampl == "signal":
        #    for i in range(9):
        #        print("actual: {} divider: {} ratio: {}".format(histograms["signal"].GetBinContent(i+1),histDiv.GetBinContent(i+1),histograms["signal"].GetBinContent(i+1)/histDiv.GetBinContent(i+1)))
        
        i=0
        ROOT.gStyle.SetPadTickX(1)
        ROOT.gStyle.SetPadTickY(1)

        hs = ROOT.THStack("hs",";;Entries")
        histograms[sampl].SetLineColor(colours[sampl])
        histograms[sampl].SetFillColor(colours[sampl])
        hs.Add(histograms[sampl])
    
        histNum=hs.GetStack().Last().Clone()
        histNum2=histNum.Clone()
        histDraw=hs.GetStack().First().Clone()
        y_max = histNum.GetMaximum()*1.2
        histNum.Divide(histDiv)
        histNum2.Divide(histDiv2)
        
        canvas_hs = ROOT.TCanvas("DataMC_"+sampl, "DataMC_"+sampl, 900, 900)
        #(x1,y1,x2,y2) 
        upper_hs = ROOT.TPad("upper_hs_"+sampl, "upper_hs_"+sampl, 0.025, 0.565, 0.995, 0.995)
        mid_hs = ROOT.TPad("upper_hs_"+sampl, "upper_hs_"+sampl, 0.025, 0.325, 0.995, 0.605)
        lower_hs = ROOT.TPad("lower_hs_"+sampl, "lower_hs_"+sampl, 0.025, 0.025, 0.995, 0.35)
        upper_hs.Draw()
        mid_hs.Draw()
        lower_hs.Draw()
        mid_hs.cd().SetGridy()
        mid_hs.SetTopMargin(0)
        lower_hs.cd().SetBottomMargin(0.3)
        lower_hs.SetGridy()
        lower_hs.SetTopMargin(0)
        upper_hs.cd()
        ROOT.gStyle.SetOptStat(0)
        
        
        histDraw.Draw()
        histDraw.SetFillColor(0)
        histDraw.SetLineColor(0)
        histDraw.SetTitle("")
        histDraw.SetAxisRange(0.0, y_max, "Y")
        histDraw.GetYaxis().SetRangeUser(0.0,y_max)
        histDraw.GetYaxis().SetMaxDigits(3)
        histDraw.GetYaxis().SetTitle("Entries")
        histDraw.GetYaxis().SetTitleSize(0.04)
        histDraw.GetYaxis().SetTitleOffset(1.2)
        histDraw.GetYaxis().SetLabelSize(0.05)
        histDraw.SetYTitle("Entries")
        #histDraw.GetYaxis().SetLabelOffset(5)
        hs.Draw("same")
                
        t = ROOT.TLatex()
        t.SetNDC()
        t.SetTextFont(72)
        t.SetTextColor(1)
        t.SetTextSize(0.03)
        t.SetTextAlign(4)
        t.DrawLatex(0.15, 0.85, canName+"_"+ptCut+"  "+channel)
        t.DrawLatex(0.15, 0.80, "ATLAS #it{#bf{Internal}}")

        '''
        legend1 = ROOT.TLegend(0.805, 0.65, 0.93, 0.87)
        if sampl == "data" or sampl == "stop" or sampl == "diboson" or sampl == "stop": 
            legend1 = ROOT.TLegend(0.805, 0.8, 0.93, 0.87)
        #legend1.SetTextSize(0.08)
        for samp in samples[sampl]:
            if "ZJets" in samp or "WJets" in samp:
                sampleName=samp.replace("Jets","")
                legend1.AddEntry(histograms[samp], sampleName, "f")
            else:
                legend1.AddEntry(histograms[samp], samp, "f")
        '''
        legend1 = ROOT.TLegend(0.805, 0.8, 0.93, 0.87)
        legend1.AddEntry(histograms[sampl], sampl, "f")
        legend1.SetBorderSize(0)
        legend1.SetFillColor(0)
        legend1.SetFillStyle(0)
        legend1.Draw()

        mid_hs.cd()
        #ROOT.gPad.SetTicky(0)

        histNum2.SetTitle("")
        histNum2.GetXaxis().SetTitle("")
        yLow = 0
        yHigh = 1.05
        histNum2.SetLineWidth(2)
        histNum2.SetLineColor(ROOT.kBlack)
        histNum2.SetAxisRange(yLow, yHigh, "Y")
        #histNum2.GetXaxis().SetLabelSize(0.09)
        #histNum2.GetXaxis().SetTitle("nSigJet Migrations")
        #histNum2.GetXaxis().SetTitleSize(0.09)
        #histNum2.GetXaxis().SetTitleOffset(1.05)
        histNum2.GetYaxis().SetRangeUser(yLow,yHigh)
        histNum2.GetYaxis().SetLabelSize(0.07)
        histNum2.GetYaxis().SetTitleOffset(0.7)
        histNum2.GetYaxis().SetTitleSize(0.06)
        histNum2.GetYaxis().SetTitle("perc. of new cat.")
        histNum2.GetYaxis().SetNdivisions(506)
        
        histNum2.Draw("hist") 

        lower_hs.cd()
        #ROOT.gPad.SetTicky(0)

        histNum.SetTitle("")
        histNum.GetXaxis().SetTitle("")
        histNum.GetXaxis().SetTitleSize(0.09)
        histNum.GetXaxis().SetTitleOffset(1.05)
    
        labs=["2-> 2","3-> 2","4-> 2","5p-> 2",
              "3-> 3","4-> 3","5p-> 3","4-> 4","5p-> 4"]
        for i in range(len(labs)): 
            histNum.GetXaxis().SetBinLabel(i+1, labs[i])


        yLow = 0
        yHigh = 1.05
        histNum.SetLineWidth(2)
        histNum.SetLineColor(ROOT.kBlack)
        histNum.SetAxisRange(yLow, yHigh, "Y")
        histNum.GetXaxis().SetLabelSize(0.09)
        histNum.GetXaxis().SetTitle("nSigJet Migrations")
        histNum.GetXaxis().SetTitleSize(0.09)
        histNum.GetXaxis().SetTitleOffset(1.05)
        histNum.GetYaxis().SetRangeUser(yLow,yHigh)
        histNum.GetYaxis().SetLabelSize(0.07)
        histNum.GetYaxis().SetTitleOffset(0.7)
        histNum.GetYaxis().SetTitleSize(0.06)
        histNum.GetYaxis().SetTitle("new cat./old cat.")
        histNum.GetYaxis().SetNdivisions(506)
        
        histNum.Draw("hist") 
        
        plotName = working_dir+"JetMigrations/JetMigrations_"+canName+"_"+ptCut+"_"+sampl+"_"+channel
        if saveHistos:
            canvas_hs.SaveAs(plotName+".png")
        canvas_hs.Close()


doNotCutCompare = False
NUMJETS=["2jet","3jet","4jet"]
if channel == "2L":
    NUMJETS=["2jet","3jet","4pjet"]
#NUMJETS=["2jet","3jet"]
METCUT=["150_250ptv","250_400ptv","400ptv"]
if channel != "0L":
    METCUT=["75_150ptv","150_250ptv","250_400ptv","400ptv",]
#METCUT=["150_250ptv"]
DRs=["CRLow","SR","CRHigh"]
DRs=["SR"]

# rebin,start,end
histNames = {"mva" : "20,-1,1", "pTV" : "0,0,600", "mBB" : "10,0,350", 
             "GSCptJ1" : "2,0,500", "GSCptJ2" : "2,0,300", "GSCptJ3" : "2,0,200",
             }
#histNames = { "mBB" : "10,0,350",}
ROOT.gROOT.SetBatch(True)
# signal_3jet_150_250ptv_60J1_20J2_20J3_SR_pTB2
#main
#sys.argv.append("-b")
histNamesOrder = histNames.keys()
histNamesOrder.sort()

#histNamesOrder=[]

###############################################
###############################################
#  Ideas:
#  make yield tables of SR vs CR, including percentage in each category
###############################################
###############################################


print("Making plots")
for hName in histNamesOrder:
    m_sigValsTot={}
    for numJets in NUMJETS:
        if "J3" in hName:
            if numJets == "2jet":
                continue
        for METCut in METCUT:
            m_sigVals={}
            m_sigValsMVA={}
            m_yieldVals={}
            m_SBVals={}
            for DR in DRs:
                sys.argv.append("-b")
                ROOT.gStyle.SetPadTickX(1)
                ROOT.gStyle.SetPadTickY(1)
                
                # "qqZllH125_2btag4pjet_150_250ptv_SR_mva30J3"
                histEx="2btag"+numJets+"_"+METCut+"_"+DR
                
                print("Looking at "+hName+" in region "+histEx)
                ROOT.gROOT.SetBatch(True)

                binning = histNames[hName]
                
                for ptCut in ["20J3","25J3","30J3"]:
                    #print(binning)
                    #doDataMC(hName,binning,histEx,hist_file,ptCut,False)
                    #doDataMC(hName,binning,histEx,hist_file,ptCut,True)
                    #if ptCut == "20J3":
                    #    cutsComparisonAllSamples(hName,binning,histEx,False)
                    #if hName == "mBB":
                    #    histExTemp=histEx.replace("SR","CRLow")
                    #    doDataMC(hName,binning,histExTemp,hist_file,ptCut,False)
                    #    doDataMC(hName,binning,histExTemp,hist_file,ptCut,True)
                    #    histExTemp=histEx.replace("SR","CRHigh")
                    #    doDataMC(hName,binning,histExTemp,hist_file,ptCut,False)
                    #    doDataMC(hName,binning,histExTemp,hist_file,ptCut,True)                    
                    if hName == "mva":
                        compareDataMC(hName,histEx,False)
                        compareDataMC(hName,histEx,True)
                        histExTemp=histEx.replace("SR","CRLow")
                        compareDataMC(hName,histExTemp,False)
                        compareDataMC(hName,histExTemp,True)
                        histExTemp=histEx.replace("SR","CRHigh")
                        compareDataMC(hName,histExTemp,False)
                        compareDataMC(hName,histExTemp,True)
                        #if numJets == "2jet":
                        #    cutsComparisonAllSamples("nSigJets","0,0,20",histEx,True)
                        #    if ptCut != "20J3":
                        #        doNJetMigration("nSigJetMigration",histEx,hist_file,ptCut)

    #if "mBB" in hName or "Mbb" in hName or "mva" in hName:
    #    printSignificance(hName)

hist_file.Close()

print("")
print("")
print("")
print("All complete")
#exit()

