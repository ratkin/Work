#! /bin/bash

export LCGRELEASES=/cvmfs/sft.cern.ch/lcg/releases
export LCGEXTERNAL=/cvmfs/sft.cern.ch/lcg/external

#gcc
export GCCCOMP=$LCGEXTERNAL/gcc/4.8.1/x86_64-slc6
source $GCCCOMP/setup.sh

#python
export PYTHONDIR=$LCGEXTERNAL/Python/2.7.4/x86_64-slc6-gcc48-opt
export PATH=$PATH:$PYTHONDIR/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PYTHONDIR/lib

#python stuff
export PYANALYSISDIR=$LCGEXTERNAL/pyanalysis/1.4_python2.7/x86_64-slc6-gcc48-opt
export PYTOOLSDIR=$LCGEXTERNAL/pytools/1.8_python2.7/x86_64-slc6-gcc48-opt
export PYTHONPATH=$PYTHONPATH:$PYTOOLSDIR/lib/python2.7/site-packages
export PATH=$PATH:$PYTOOLSDIR/bin
export PYTHONPATH=$PYTHONPATH:$PYANALYSISDIR/lib/python2.7/site-packages

#root
source $LCGRELEASES/ROOT/6.04.02-a6f71/x86_64-slc6-gcc48-opt/bin/thisroot.sh
export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#xerces
export XercesC_HOME=$LCGEXTERNAL/XercesC/3.1.1p1/x86_64-slc6-gcc48-opt
export PATH=$XercesC_HOME/bin:$PATH
export LD_LIBRARY_PATH=$XercesC_HOME/lib:$LD_LIBRARY_PATH


export G4WORKDIR=/home/ratkin/workdir/Allpix
export PATH=$PATH:$G4WORKDIR
export PATH=$PATH:/home/ratkin/workdir/Allpix/allpix-install/bin

source $LCGRELEASES/Geant4/10.01.p02-81fbd/x86_64-slc6-gcc48-opt/share/Geant4-10.1.2/geant4make/geant4make.sh

#cmake ../allpix -DCMAKE_CXX_COMPILER=$GCCCOMP/bin/lcg-g++-4.8.1 -DCMAKE_C_COMPILER=$GCCCOMP/bin/lcg-gcc-4.8.1 -DCMAKE_INSTALL_PREFIX=../allpix-install