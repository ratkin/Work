#! /bin/bash

export LCGRELEASES=/cvmfs/sft.cern.ch/lcg/releases
export LCGEXTERNAL=/cvmfs/sft.cern.ch/lcg/external
export LCG_85=/cvmfs/sft.cern.ch/lcg/views/LCG_85/x86_64-slc6-gcc49-opt
export ILCSOFT=/home/ratkin/workdir/Allpix/ilcsoft
export ANALYSIS_CONF=/home/ratkin/workdir/Allpix/ilcsoft/v01-17-10/Eutelescope/v1.0/jobsub/examples/datura-noDUT
export ANALYSIS=/home/ratkin/workdir/Allpix/EUT-ana
export ANALYSIS_GBL=/home/ratkin/workdir/Allpix/ilcsoft/v01-17-10/Eutelescope/v1.0/jobsub/examples/GBL/noDUTExample
export ANAGBL=/home/ratkin/workdir/Allpix/EUT-GBL

export LCIO=$ILCSOFT/v01-17-10/lcio/v02-07-02
export CLICSOFT=$ILCSOFT/v01-17-10
export PATH=$LCIO/bin:$LCIO/tools:$PATH
export LD_LIBRARY_PATH=${LCIO}/lib:$LCIO/build/rootdict:${LD_LIBRARY_PATH}
export PYTHONPATH=$LCIO/src/python:$PYTHONPATH
alias pylcio='python $LCIO/src/python/pylcio.py'
alias dumpevent='$LCIO/bin/dumpevent'
alias anajob='$LCIO/bin/anajob'
alias jobsub='$ILCSOFT/v01-17-10/Eutelescope/v1.0/jobsub/jobsub.py'

#gcc
#export GCCCOMP=$LCGEXTERNAL/gcc/4.8.1/x86_64-slc6
source $LCG_85/setup.sh

#python
export PYTHONDIR=$LCGEXTERNAL/Python/2.7.4/x86_64-slc6-gcc48-opt
export PATH=$PATH:$PYTHONDIR/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PYTHONDIR/lib

#python stuff
export PYANALYSISDIR=$LCGEXTERNAL/pyanalysis/1.4_python2.7/x86_64-slc6-gcc48-opt
export PYTOOLSDIR=$LCGEXTERNAL/pytools/1.8_python2.7/x86_64-slc6-gcc48-opt
export PYTHONPATH=$PYTHONPATH:$PYTOOLSDIR/lib/python2.7/site-packages
export PATH=$PATH:$PYTOOLSDIR/bin
export PYTHONPATH=$PYTHONPATH:$PYANALYSISDIR/lib/python2.7/site-packages
export LAPACKDIR=$LCGEXTERNAL/lapack/3.4.0/x86_64-slc6-gcc48-opt
export LD_LIBRARY_PATH=$LAPACKDIR/lib:$LD_LIBRARY_PATH
export BLASDIR=$LCGEXTERNAL/blas/20110419/x86_64-slc6-gcc48-opt
export LD_LIBRARY_PATH=$BLASDIR/lib:$LD_LIBRARY_PATH

#root
source $LCGRELEASES/ROOT/6.04.02-a6f71/x86_64-slc6-gcc48-opt/bin/thisroot.sh
export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#xerces
export XercesC_HOME=$LCGEXTERNAL/XercesC/3.1.1p1/x86_64-slc6-gcc48-opt
export PATH=$XercesC_HOME/bin:$PATH
export LD_LIBRARY_PATH=$XercesC_HOME/lib:$LD_LIBRARY_PATH


export G4WORKDIR=/home/ratkin/workdir/Allpix/allpix-install/bin
export PATH=$PATH:$G4WORKDIR

source $LCGRELEASES/Geant4/10.01.p02-81fbd/x86_64-slc6-gcc48-opt/share/Geant4-10.1.2/geant4make/geant4make.sh

#cmake ../allpix -DCMAKE_CXX_COMPILER=$GCCCOMP/bin/lcg-g++-4.8.1 -DCMAKE_C_COMPILER=$GCCCOMP/bin/lcg-gcc-4.8.1 -DCMAKE_INSTALL_PREFIX=../allpix-install

#--------------------------------------------------------------------------------
### ILCUTIL  DONE
#--------------------------------------------------------------------------------
export ILCUTIL_DIR=${CLICSOFT}/ilcutil/v01-02-01
export LD_LIBRARY_PATH=${ILCUTIL_DIR}/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     CLHEP    DONE
#--------------------------------------------------------------------------------
export CLHEP=$CLICSOFT/CLHEP/x86_64-slc6-gcc49-opt
export CLHEP_BASE_DIR=$CLHEP
export CLHEP_INCLUDE_DIR=$CLHEP/include
export PATH=$CLHEP_BASE_DIR/bin:$PATH
export LD_LIBRARY_PATH=$CLHEP_BASE_DIR/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     GEAR   DONE
#--------------------------------------------------------------------------------
export GEAR=$CLICSOFT/gear/v01-06
export PATH=$GEAR/bin:$GEAR/tools:$PATH
export LD_LIBRARY_PATH=$GEAR/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     GSL   DONE
#--------------------------------------------------------------------------------
export GSL_HOME=$CLICSOFT/gsl/x86_64-slc6-gcc49-opt
export PATH=$GSL_HOME/bin:$PATH
export LD_LIBRARY_PATH=$GSL_HOME/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     CED
#--------------------------------------------------------------------------------
export PATH=$CLICSOFT/CED/v01-09-01/bin:$PATH
export LD_LIBRARY_PATH=$CLICSOFT/CED/v01-09-01/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     DD4hep
#--------------------------------------------------------------------------------
export DD4hep_ROOT=$CLICSOFT/DD4hep/v00-16
export DD4hepINSTALL=$CLICSOFT/DD4hep/v00-16
export DD4HEP=$CLICSOFT/DD4hep/v00-16
export DD4hep_DIR=$CLICSOFT/DD4hep/v00-16
export PYTHONPATH=$DD4HEP/python:$DD4HEP/DDCore/python:$PYTHONPATH
export PATH=$DD4HEP/bin:$PATH
export LD_LIBRARY_PATH=$DD4HEP/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     LCCD
#--------------------------------------------------------------------------------
export LCCD=$CLICSOFT/lccd/v01-03-01
#--------------------------------------------------------------------------------
#     MarlinUtil
#--------------------------------------------------------------------------------
export LD_LIBRARY_PATH=$CLICSOFT/MarlinUtil/v01-12/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     RAIDA
#--------------------------------------------------------------------------------
export RAIDA_HOME=$CLICSOFT/RAIDA/v01-07
export PATH=$RAIDA_HOME/bin:$PATH
#--------------------------------------------------------------------------------
#     Eutelescope
#--------------------------------------------------------------------------------
export MILLEPEDEII=$CLICSOFT/Eutelescope/v1.0/external/millepede2/trunk
export MILLEPEDEII_VERSION=trunk
export EUDAQ_VERSION=branches/v1.6-dev
export EUDAQ=$CLICSOFT/Eutelescope/v1.0/external/eudaq/v1.6-dev
export EUTELESCOPE=$CLICSOFT/Eutelescope/v1.0
export ALLPIX=/home/ratkin/workdir/Allpix/allpix-build
export PATH=$MILLEPEDEII:$EUTELESCOPE/bin:$PATH
export LD_LIBRARY_PATH=$EUTELESCOPE/lib:$EUDAQ/lib:$LD_LIBRARY_PATH
#--------------------------------------------------------------------------------
#     Marlin
#--------------------------------------------------------------------------------
export MARLIN=$CLICSOFT/Marlin/v01-09
export PATH=$MARLIN/bin:$PATH
export MARLIN_DLL=$CLICSOFT/Eutelescope/v1.0/lib/libEutelescope.so:$EUDAQ/lib/libNativeReader.so:$MARLIN_DLL