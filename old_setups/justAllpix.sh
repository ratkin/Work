#! /bin/bash

source /cvmfs/sft.cern.ch/lcg/views/LCG_85/x86_64-slc6-gcc49-opt/setup.sh
export G4WORKDIR=/home/ratkin/workdir/cAllpix/allpix-install/bin
export PATH=$PATH:$G4WORKDIR
export G4INSTALL=/cvmfs/sft.cern.ch/lcg/releases/Geant4/10.02.p02-1c9b9/x86_64-slc6-gcc49-opt

#------------------------------------------------------------------------------------
export XercesC_HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_85/XercesC/3.1.3/x86_64-slc6-gcc49-opt"
export PATH="$XercesC_HOME/bin:$PATH"
export LD_LIBRARY_PATH="$XercesC_HOME/lib:$LD_LIBRARY_PATH"

#------------------------------------------------------------------------------------


