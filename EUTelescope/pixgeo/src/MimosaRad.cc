#include "MimosaRad.h"

namespace eutelescope {
namespace geo {

  std::string GetPixName(int x);

  MimosaRad::MimosaRad(): EUTelGenericPixGeoDescr(21.2, 10.6, 0.02,//21.2, 10.6, 0.02,	//size X, Y, Z (size in mm of sensor)
						0, 1151, 0, 4,	//minX maxX minY maxY its two 0's and the number of pixels in x and y(1151x575 pixel matrix) 
						9.3660734 )		//rad length
{

	//Create the material for the sensor
	matSi = new TGeoMaterial( "Si", 28.0855 , 14.0, 2.33, -_radLength, 45.753206 );
	Si = new TGeoMedium("MimosaSilicon",1, matSi);

	//Create a plane for the sensitive area
	plane = _tGeoManager->MakeBox( "sensarea_mimosa", Si, 10.6, 5.3, 0.01 );
	//plane = _tGeoManager->MakeTubs( "sensarea_mimosa", Si, 100, 110.6, 0.02,90-11.5/2,90+11.5/2 );
	//Divide the regions to create pixels
  	TGeoVolume* row = plane->Divide("mimorow", 1 , _maxIndexX+1 , 0 , 1, 0, "N"); 
	row->Divide("mimopixel", 2 ,_maxIndexY , 0 , 1, 0, "N");
	//row->Divide("mimopixel", 2 , 288, 0 , 1, 0, "N");

}

MimosaRad::~ MimosaRad()
{
	//It appears that ROOT will take ownership and delete that stuff! 
	//delete matSi,
	//delete Si;
}

void  MimosaRad::createRootDescr(char const * planeVolume)
{
	//Get the plane as provided by the EUTelGeometryTelescopeGeoDescription
	TGeoVolume* topplane =_tGeoManager->GetVolume(planeVolume);
	//Add the sensitive area to the plane
	topplane->AddNode(plane, 1);
    
}

std::string MimosaRad::getPixName(int x , int y)
{
	char buffer [100];
	//return path to the pixel, don't forget to shift indices by +1+
	snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", x+1, int(floor(y*4/576))+1);
	//snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d", x+1);
	//std::cout<<"(" <<x<< "," <<y<< ")" <<std::endl;
	/*if(y>=432){
	  snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", x+1, 4);
	}
	else if(y>=288){
	  snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", x+1, 3);
	}
	else if(y>=144){
	  snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", x+1, 2);
	}
	else if(y>=0){
	  snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", x+1, 1);
	  }*/

	/*if (x%2 == 0 )
	{
	  if (y%2 == 0 ){
	    snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", (x/2) +1, (y/2) +1);
	  }
	  else{
	    snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", (x/2) +1, ((y-1)/2) +1);
	  }
	}
	else{
	  if (y%2 == 0 ){
	    snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", ((x-1)/2) +1, (y/2) +1);
	  }
	  else{
	    snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", ((x-1)/2) +1, ((y-1)/2) +1);
	  }
	  }*/
	return std::string( buffer ); 
}
  /*std::string GetPixName(int x){

    char buffer [100];

        if(x==0){
            snprintf( buffer, 100, "/left_1");
        }
        else if(x==9){
        snprintf( buffer, 100, "/right_1");
        }
        else{
            snprintf( buffer, 100, "/plane_1/strip_%d",x);
        }

    return std::string( buffer );
    }*/



	/*TODO*/ std::pair<int, int>  MimosaRad::getPixIndex(char const*){return std::make_pair(0,0); }

EUTelGenericPixGeoDescr* maker()
{
	MimosaRad* mPixGeoDescr = new MimosaRad();
	return dynamic_cast<EUTelGenericPixGeoDescr*>(mPixGeoDescr);
}

} //namespace geo
} //namespace eutelescope

