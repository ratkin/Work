#include "MimosaR02.h"

namespace eutelescope {
  namespace geo {
    
    std::string GetPixName(int x, int y);
    
    //size X, Y, Z still needs to be altered to take proper values for the R0 sensor dimensions. Strips are unaffected though
    MimosaR02::MimosaR02(): EUTelGenericPixGeoDescr(97.779, 106.417, 0.00031,//size X, Y, Z (size in mm of sensor) (taken using points Bx & Cx, Ay and rmax-rCentre)
						    0, 1025,0,1153, 0, 3,	//minX1, maxX1, minX2, maxX2, minY, maxY number of pixels in x and y  
						    0.0001932745,0.0001718368,0.02, //pitchPhi1, pitchPhi2, stereo angle (in rads)
						    384.5, 403.481, 427.462, 456.442, 488.423, 438.614, //rmin, r1, r2, r3, rmax, rCentre (in mm)
						    18.981, 23.981, 28.98, 31.981,// the 4 striplengths (in mm)
						    9.3660734 )		//rad length
    {
      
      //Create the material for the sensor
      matSi = new TGeoMaterial( "Si", 28.0855 , 14.0, 2.33, -_radLength, 45.753206 );
      Si = new TGeoMedium("MimosaSilicon",1, matSi);
      double deg=180, PI=3.14159265358979;

      plane = _tGeoManager->MakeBox("sensarea_box",Si,150,500,1);
      //Define the four strip volumes
      row1strip = _tGeoManager->MakeTubs( "sensarea_row1" , Si, _rMin, _r1, _sizeSensitiveAreaZ/2, 90+(-_pitchPhi1/2)*deg,90+(_pitchPhi1/2)*deg);
      row2strip = _tGeoManager->MakeTubs( "sensarea_row2" , Si, _r1, _r2, _sizeSensitiveAreaZ/2, 90+(-_pitchPhi1/2)*deg,90+(_pitchPhi1/2)*deg);
      row3strip = _tGeoManager->MakeTubs( "sensarea_row3" , Si, _r2, _r3, _sizeSensitiveAreaZ/2, 90+(-_pitchPhi2/2)*deg,90+(_pitchPhi2/2)*deg);
      row4strip = _tGeoManager->MakeTubs( "sensarea_row4" , Si, _r3, _rMax, _sizeSensitiveAreaZ/2, 90+(-_pitchPhi2/2)*deg,90+(_pitchPhi2/2)*deg);

      //The formula used for calculating strip position is defined in the ATLAS12EC Technical Specs
      //get angle of first strips in first two rows
      theta=_pitchPhi1*(_maxIndexX1/2.0-0.5)+_stereoAngle+PI/2;
  
      //placement of first two rows
      for( int i = _maxIndexX1; i >=0; i-- ){
	TGeoCombiTrans* transform=new TGeoCombiTrans(0,0,0,new TGeoRotation("rot",0,0,0));
	TGeoCombiTrans* transform1=new TGeoCombiTrans(0,0,0,new TGeoRotation("rot",0,0,0));
	//get position of each strip for first row
	phi_i=(i-_maxIndexX1/2.0)*_pitchPhi1;
	b=-2*(2*_rCentre*sin(_stereoAngle/2))*sin(_stereoAngle/2+phi_i);    
	c=pow((2*_rCentre*sin(_stereoAngle/2)),2)-pow(_rMin,2);
	r=0.5*(-b+sqrt(pow(b,2)-4*c));
	y=r*cos(phi_i+_stereoAngle) - _rCentre*cos(_stereoAngle);
	x=-r*sin(phi_i+_stereoAngle) + _rCentre*sin(_stereoAngle);
	//create first transform
	//rotate to get correct angle of strip
	transform->RotateZ(theta*deg-90);
	transform->SetTranslation(x-_rMin*cos(theta),y-_rMin*sin(theta),0);
	//get each position of the strips for second row 
	c=pow((2*_rCentre*sin(_stereoAngle/2)),2)-pow(_r1,2);
	r=0.5*(-b+sqrt(pow(b,2)-4*c));
	x=r*cos(phi_i+_stereoAngle) - _rCentre*cos(_stereoAngle);
	y=r*sin(phi_i+_stereoAngle) - _rCentre*sin(_stereoAngle);
	//create second transform
	transform1->RotateZ(theta*deg-90);
	transform1->SetTranslation(x-_r1*cos(theta),y-_r1*sin(theta),0);
	//add the nodes
	plane->AddNode(row1strip,i+1,transform);
	plane->AddNode(row2strip,i+1,transform1);
	//get angle of next strip
	theta-=_pitchPhi1;
      }
  
      //get angle of first strips in outer two rows
      theta=_pitchPhi2*(_maxIndexX2/2.0-0.5)+_stereoAngle+PI/2;
  
      //placement of second two rows
      for( int i = _maxIndexX2; i >=0; i-- ){
	TGeoCombiTrans* transform=new TGeoCombiTrans(0,0,0,new TGeoRotation("rot",0,0,0));
	TGeoCombiTrans* transform1=new TGeoCombiTrans(0,0,0,new TGeoRotation("rot",0,0,0));

	//get each position of the strips for first row
	phi_i=(i-_maxIndexX2/2.0)*_pitchPhi2;
	b=-2*(2*_rCentre*sin(_stereoAngle/2))*sin(_stereoAngle/2+phi_i);    
	c=pow((2*_rCentre*sin(_stereoAngle/2)),2)-pow(_r2,2);
	r=0.5*(-b+sqrt(pow(b,2)-4*c));
	y=r*cos(phi_i+_stereoAngle) - _rCentre*cos(_stereoAngle);
	x=-r*sin(phi_i+_stereoAngle) + _rCentre*sin(_stereoAngle);
	//create first transform
	transform->RotateZ(theta*deg-90);
	transform->SetTranslation(-y-_r2*cos(theta),x-_r2*sin(theta),0);
	//get each position of the strips for second row 
	c=pow((2*_rCentre*sin(_stereoAngle/2)),2)-pow(_r3,2);
	r=0.5*(-b+sqrt(pow(b,2)-4*c));
	y=r*cos(phi_i+_stereoAngle) - _rCentre*cos(_stereoAngle);
	x=-r*sin(phi_i+_stereoAngle) + _rCentre*sin(_stereoAngle);
	//create second transform
	transform1->RotateZ(theta*deg-90);
	transform1->SetTranslation(x-_r3*cos(theta),y-_r3*sin(theta),0);
	//add the nodes
	plane->AddNode(row3strip,i+1,transform);
	plane->AddNode(row4strip,i+1,transform1);//transform);
	//get angle of each strip. will be same for both rows
	theta-=_pitchPhi2;
      }
  
    }
  
    MimosaR02::~ MimosaR02()
    {
      //It appears that ROOT will take ownership and delete that stuff! 
      //delete matSi,
      //delete Si;
    }
  
    void  MimosaR02::createRootDescr(char const * planeVolume)
    {
      //Get the plane as provided by the EUTelGeometryTelescopeGeoDescription
      TGeoVolume* topplane =_tGeoManager->GetVolume(planeVolume);
      //Add the sensitive area to the plane
      topplane->AddNode(plane, 1,new TGeoTranslation(0,0,0) );
       
    }
  
    std::string MimosaR02::getPixName(int x , int y){
      char buffer [100];
      //return path to the pixel, don't forget to shift indices by +1+
      snprintf( buffer, 100, "/sensarea_box_1/sensarea_row%d_%d",y+1,x+1);
    
      return std::string( buffer ); 
    }
  
  
  
    /*TODO*/ std::pair<int, int>  MimosaR02::getPixIndex(char const*){return std::make_pair(0,0); }
  
    EUTelGenericPixGeoDescr* maker()
    {
      MimosaR02* mPixGeoDescr = new MimosaR02();
      return dynamic_cast<EUTelGenericPixGeoDescr*>(mPixGeoDescr);
    }
  
  } //namespace geo
} //namespace eutelescope

