#ifndef MIMOSAR02_H
#define	MIMOSAR02_H

  /** @class Mimosa26
	* This class is the implementation of  @class EUTelGenericPixGeoDescr
	* for the Mimosa26 which is the standard telescope reference plane of
	* the DESY pixel telescope.
	* The geoemtry is as following: the 21.2 x 10.6 mm**2 are is divided
	* into a 1151 x 575 pixel matrix. All pixels are of the same dimension. 
    */

//STL
#include <string> //std::string
#include <utility> //std::pair

//EUTELESCOPE
#include "EUTelGenericPixGeoDescr.h"

//ROOT
#include "TGeoMaterial.h"
#include "TGeoMedium.h"
#include "TGeoVolume.h"

namespace eutelescope {
namespace geo {

class MimosaR02 : public EUTelGenericPixGeoDescr {
	
	public:
		MimosaR02();
		~MimosaR02();

		void createRootDescr(char const *);
		std::string getPixName(int, int);
		std::pair<int, int> getPixIndex(char const *);

	protected:
		TGeoMaterial* matSi;
		TGeoMedium* Si;
		TGeoVolume* plane,*row1strip,*row2strip,*row3strip,*row4strip;
		Double_t phi_i,b,r,c,x1,x,y1,y,gradient,theta;
		//corners of sensor if strips are in y direction ie. rotated 90 degrees
		Double_t Ax=39.565,Ay=-56.608,Bx=47.856,By=47.920,Cx=-49.943,Cy=47.711,Dx=-37.479,Dy=-56.398;
		
};

extern "C"
{
	EUTelGenericPixGeoDescr* maker();
}

} //namespace geo
} //namespace eutelescope

#endif	//MIMOSAR02_H
