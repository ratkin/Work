#include "MimosaRad2.h"

namespace eutelescope {
namespace geo {

  std::string GetPixName(int x, int y);

  MimosaRad2::MimosaRad2(): EUTelGenericPixGeoDescr(21.2, 10.6, 0.02,//21.2, 10.6, 0.02,	//size X, Y, Z (size in mm of sensor)
						0, 1151, 0, 4,	//minX maxX minY maxY its two 0's and the number of pixels in x and y(1151x575 pixel matrix) 
						9.3660734 )		//rad length
{

  //Create the material for the sensor
  matSi = new TGeoMaterial( "Si", 28.0855 , 14.0, 2.33, -_radLength, 45.753206 );
  Si = new TGeoMedium("MimosaSilicon",1, matSi);

  Double_t rmin=384.5,rmax=488.4,dz=0.310,dphi=11.459155902616;
  Double_t l1=19,l2=24,l3=28.9,l4=32;
  Double_t rmid=rmin+l1+l2;
  Int_t n_phi=1026;
  Int_t n_phi2=1154;
  Int_t n_r=2;

  plane  = _tGeoManager->MakeTubs( "sensarea_radial", Si, rmin   , rmax    , dz, 90-dphi/2,90+dphi/2);
  TGeoVolume* plane1 = _tGeoManager->MakeTubs( "sensarea_row_1" , Si, rmin   , rmin+l1 , dz, 90-dphi/2,90+dphi/2);
  TGeoVolume* plane2 = _tGeoManager->MakeTubs( "sensarea_row_2" , Si, rmin+l1, rmid    , dz, 90-dphi/2,90+dphi/2);
  TGeoVolume* plane3 = _tGeoManager->MakeTubs( "sensarea_row_3" , Si, rmid   , rmid+l3 , dz, 90-dphi/2,90+dphi/2);
  TGeoVolume* plane4 = _tGeoManager->MakeTubs( "sensarea_row_4" , Si, rmid+l3, rmax    , dz, 90-dphi/2,90+dphi/2);
  TGeoVolume* row1 = plane1->Divide("radialstrip_1", 2 , n_phi , 0 , 1, 0, "N");
  TGeoVolume* row2 = plane2->Divide("radialstrip_2", 2 , n_phi , 0 , 1, 0, "N");
  TGeoVolume* row3 = plane3->Divide("radialstrip_3", 2 , n_phi2, 0 , 1, 0, "N");
  TGeoVolume* row4 = plane4->Divide("radialstrip_4", 2 , n_phi2, 0 , 1, 0, "N");

  plane->AddNode(plane1, 1, new TGeoTranslation( 0, 0, 0));// (0, -(rmin+rmax)/2, 0)
  plane->AddNode(plane2, 1, new TGeoTranslation( 0, 0, 0));
  plane->AddNode(plane3, 1, new TGeoTranslation( 0, 0, 0));
  plane->AddNode(plane4, 1, new TGeoTranslation( 0, 0, 0));

}

MimosaRad2::~ MimosaRad2()
{
	//It appears that ROOT will take ownership and delete that stuff! 
	//delete matSi,
	//delete Si;
}

void  MimosaRad2::createRootDescr(char const * planeVolume)
{
	//Get the plane as provided by the EUTelGeometryTelescopeGeoDescription
	TGeoVolume* topplane =_tGeoManager->GetVolume(planeVolume);
	//Add the sensitive area to the plane
	topplane->AddNode(plane, 1);
    
}

std::string MimosaRad2::getPixName(int x , int y){
  char buffer [100];
  //return path to the pixel, don't forget to shift indices by +1+
  int z=floor(y*4/576);
  
  snprintf( buffer, 100, "/sensarea_radial_1/sensarea_row_%d_1/radialstrip_%d_%d",z+1,z+1,x+1);

  return std::string( buffer ); 
}



	/*TODO*/ std::pair<int, int>  MimosaRad2::getPixIndex(char const*){return std::make_pair(0,0); }

EUTelGenericPixGeoDescr* maker()
{
	MimosaRad2* mPixGeoDescr = new MimosaRad2();
	return dynamic_cast<EUTelGenericPixGeoDescr*>(mPixGeoDescr);
}

} //namespace geo
} //namespace eutelescope

