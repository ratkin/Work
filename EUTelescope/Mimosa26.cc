#include "Mimosa26.h"

namespace eutelescope {
namespace geo {
  //Kenny changed minmax X,Y instead of size X,Y,Z
Mimosa26::Mimosa26(): EUTelGenericPixGeoDescr(	21.2, 10.6, 0.02,	//size X, Y, Z (21.2, 10.6, 0.02)
						0, 1151, 0, 575,	//min max X,Y
						9.3660734 )		//rad length
{
	//Create the material for the sensor
        matSi = new TGeoMaterial( "Si", 28.0855 , 14.0, 2.33, -_radLength, 45.753206 );//(name,Atomic mass,no. protons,rho(g.cm^-3),radLength,interactionLength)
	Si = new TGeoMedium("MimosaSilicon",1, matSi);
	//Create a plane for the sensitive area
	plane = _tGeoManager->MakeBox( "sensarea_mimosa", Si, 0.5*_sizeSensitiveAreaX, 0.5*_sizeSensitiveAreaY, 0.5*_sizeSensitiveAreaZ );//(name,medium,dx,dy,dz) (was just the actual values not using the variable names)
	//Divide the regions to create pixels
  	TGeoVolume* row = plane->Divide("mimorow", 1 , _maxIndexX+1 , 0 , 1, 0, "N"); //(was 1152 instead of variable+1)
	//TGeoVolume * Divide(const char* divname, Int_t iaxis, Int_t ndiv, Double_t start, Double_t step, Int_t numed = 0, Option_t* option = "") if N, real STEP will be computed as the full range of IAXIS divided by NDIV
	row->Divide("mimopixel", 2 , _maxIndexY+1, 0 , 1, 0, "N");//(was 576 instead of variable+1)
}

Mimosa26::~ Mimosa26()
{
	//It appears that ROOT will take ownership and delete that stuff! 
	//delete matSi,
	//delete Si;
}

void  Mimosa26::createRootDescr(char const * planeVolume)
{
	//Get the plane as provided by the EUTelGeometryTelescopeGeoDescription
	TGeoVolume* topplane =_tGeoManager->GetVolume(planeVolume);
	//Add the sensitive area to the plane
	topplane->AddNode(plane, 1);
    
}

std::string Mimosa26::getPixName(int x , int y)
{
	char buffer [100];
	//return path to the pixel, don't forget to shift indices by +1+
	snprintf( buffer, 100, "/sensarea_mimosa_1/mimorow_%d/mimopixel_%d", x+1, y+1);
	return std::string( buffer ); 
}
	/*TODO*/ std::pair<int, int>  Mimosa26::getPixIndex(char const*){return std::make_pair(0,0); }

EUTelGenericPixGeoDescr* maker()
{
	Mimosa26* mPixGeoDescr = new Mimosa26();
	return dynamic_cast<EUTelGenericPixGeoDescr*>(mPixGeoDescr);
}

} //namespace geo
} //namespace eutelescope

